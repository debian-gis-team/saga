#! /usr/bin/env python

'''
Python Interface to SAGA Tools Library
----------
- Category : Import/Export
- Name     : Tables
- ID       : io_table

Description
----------
Tools for the import and export of tables.
'''

from PySAGA.helper import Tool_Wrapper

def Export_Text_Table(TABLE=None, HEADLINE=None, STRQUOTA=None, SEPARATOR=None, SEP_OTHER=None, FILENAME=None, Verbose=2):
    '''
    Export Text Table
    ----------
    [io_table.0]\n
    Export text table with options.\n
    Arguments
    ----------
    - TABLE [`input table`] : Table
    - HEADLINE [`boolean`] : Headline. Default: 1
    - STRQUOTA [`boolean`] : Strings in Quota. Default: 1
    - SEPARATOR [`choice`] : Separator. Available Choices: [0] tabulator [1] ; [2] , [3] space [4] other Default: 0
    - SEP_OTHER [`text`] : other. Default: *
    - FILENAME [`file path`] : File

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('io_table', '0', 'Export Text Table')
    if Tool.is_Okay():
        Tool.Set_Input ('TABLE', TABLE)
        Tool.Set_Option('HEADLINE', HEADLINE)
        Tool.Set_Option('STRQUOTA', STRQUOTA)
        Tool.Set_Option('SEPARATOR', SEPARATOR)
        Tool.Set_Option('SEP_OTHER', SEP_OTHER)
        Tool.Set_Option('FILENAME', FILENAME)
        return Tool.Execute(Verbose)
    return False

def run_tool_io_table_0(TABLE=None, HEADLINE=None, STRQUOTA=None, SEPARATOR=None, SEP_OTHER=None, FILENAME=None, Verbose=2):
    '''
    Export Text Table
    ----------
    [io_table.0]\n
    Export text table with options.\n
    Arguments
    ----------
    - TABLE [`input table`] : Table
    - HEADLINE [`boolean`] : Headline. Default: 1
    - STRQUOTA [`boolean`] : Strings in Quota. Default: 1
    - SEPARATOR [`choice`] : Separator. Available Choices: [0] tabulator [1] ; [2] , [3] space [4] other Default: 0
    - SEP_OTHER [`text`] : other. Default: *
    - FILENAME [`file path`] : File

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('io_table', '0', 'Export Text Table')
    if Tool.is_Okay():
        Tool.Set_Input ('TABLE', TABLE)
        Tool.Set_Option('HEADLINE', HEADLINE)
        Tool.Set_Option('STRQUOTA', STRQUOTA)
        Tool.Set_Option('SEPARATOR', SEPARATOR)
        Tool.Set_Option('SEP_OTHER', SEP_OTHER)
        Tool.Set_Option('FILENAME', FILENAME)
        return Tool.Execute(Verbose)
    return False

def Import_Text_Table(TABLE=None, HEADLINE=None, SEPARATOR=None, ENCODING=None, SEP_OTHER=None, FILENAME=None, Verbose=2):
    '''
    Import Text Table
    ----------
    [io_table.1]\n
    Import a text table with options.\n
    Arguments
    ----------
    - TABLE [`output table`] : Table
    - HEADLINE [`boolean`] : Headline. Default: 1
    - SEPARATOR [`choice`] : Separator. Available Choices: [0] tabulator [1] ; [2] , [3] space [4] other Default: 0
    - ENCODING [`choice`] : Encoding. Available Choices: [0] ANSI [1] UTF-7 [2] UTF-8 [3] UTF-16 (Little Endian) [4] UTF-16 (Big Endian) [5] UTF-32 (Little Endian) [6] UTF-32 (Big Endian) [7] default Default: 0
    - SEP_OTHER [`text`] : Separator (other). Default: *
    - FILENAME [`file path`] : File

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('io_table', '1', 'Import Text Table')
    if Tool.is_Okay():
        Tool.Set_Output('TABLE', TABLE)
        Tool.Set_Option('HEADLINE', HEADLINE)
        Tool.Set_Option('SEPARATOR', SEPARATOR)
        Tool.Set_Option('ENCODING', ENCODING)
        Tool.Set_Option('SEP_OTHER', SEP_OTHER)
        Tool.Set_Option('FILENAME', FILENAME)
        return Tool.Execute(Verbose)
    return False

def run_tool_io_table_1(TABLE=None, HEADLINE=None, SEPARATOR=None, ENCODING=None, SEP_OTHER=None, FILENAME=None, Verbose=2):
    '''
    Import Text Table
    ----------
    [io_table.1]\n
    Import a text table with options.\n
    Arguments
    ----------
    - TABLE [`output table`] : Table
    - HEADLINE [`boolean`] : Headline. Default: 1
    - SEPARATOR [`choice`] : Separator. Available Choices: [0] tabulator [1] ; [2] , [3] space [4] other Default: 0
    - ENCODING [`choice`] : Encoding. Available Choices: [0] ANSI [1] UTF-7 [2] UTF-8 [3] UTF-16 (Little Endian) [4] UTF-16 (Big Endian) [5] UTF-32 (Little Endian) [6] UTF-32 (Big Endian) [7] default Default: 0
    - SEP_OTHER [`text`] : Separator (other). Default: *
    - FILENAME [`file path`] : File

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('io_table', '1', 'Import Text Table')
    if Tool.is_Okay():
        Tool.Set_Output('TABLE', TABLE)
        Tool.Set_Option('HEADLINE', HEADLINE)
        Tool.Set_Option('SEPARATOR', SEPARATOR)
        Tool.Set_Option('ENCODING', ENCODING)
        Tool.Set_Option('SEP_OTHER', SEP_OTHER)
        Tool.Set_Option('FILENAME', FILENAME)
        return Tool.Execute(Verbose)
    return False

def Import_Text_Table_with_Numbers_only(TABLES=None, SKIP=None, HEADLINE=None, SEPARATOR=None, SEP_OTHER=None, FILENAME=None, Verbose=2):
    '''
    Import Text Table with Numbers only
    ----------
    [io_table.2]\n
    Import Text Table with Numbers only\n
    Arguments
    ----------
    - TABLES [`output table list`] : Tables
    - SKIP [`integer number`] : Skip Leading Lines. Minimum: 0 Default: 0
    - HEADLINE [`boolean`] : Headline. Default: 0
    - SEPARATOR [`choice`] : Separator. Available Choices: [0] tabulator [1] ; [2] , [3] space [4] other Default: 0
    - SEP_OTHER [`text`] : other. Default: *
    - FILENAME [`file path`] : File

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('io_table', '2', 'Import Text Table with Numbers only')
    if Tool.is_Okay():
        Tool.Set_Output('TABLES', TABLES)
        Tool.Set_Option('SKIP', SKIP)
        Tool.Set_Option('HEADLINE', HEADLINE)
        Tool.Set_Option('SEPARATOR', SEPARATOR)
        Tool.Set_Option('SEP_OTHER', SEP_OTHER)
        Tool.Set_Option('FILENAME', FILENAME)
        return Tool.Execute(Verbose)
    return False

def run_tool_io_table_2(TABLES=None, SKIP=None, HEADLINE=None, SEPARATOR=None, SEP_OTHER=None, FILENAME=None, Verbose=2):
    '''
    Import Text Table with Numbers only
    ----------
    [io_table.2]\n
    Import Text Table with Numbers only\n
    Arguments
    ----------
    - TABLES [`output table list`] : Tables
    - SKIP [`integer number`] : Skip Leading Lines. Minimum: 0 Default: 0
    - HEADLINE [`boolean`] : Headline. Default: 0
    - SEPARATOR [`choice`] : Separator. Available Choices: [0] tabulator [1] ; [2] , [3] space [4] other Default: 0
    - SEP_OTHER [`text`] : other. Default: *
    - FILENAME [`file path`] : File

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('io_table', '2', 'Import Text Table with Numbers only')
    if Tool.is_Okay():
        Tool.Set_Output('TABLES', TABLES)
        Tool.Set_Option('SKIP', SKIP)
        Tool.Set_Option('HEADLINE', HEADLINE)
        Tool.Set_Option('SEPARATOR', SEPARATOR)
        Tool.Set_Option('SEP_OTHER', SEP_OTHER)
        Tool.Set_Option('FILENAME', FILENAME)
        return Tool.Execute(Verbose)
    return False

def Import_Text_Tables(TABLES=None, FILES=None, HEADLINE=None, SEPARATOR=None, SEP_OTHER=None, Verbose=2):
    '''
    Import Text Tables
    ----------
    [io_table.import_text_tables]\n
    Import Text Tables\n
    Arguments
    ----------
    - TABLES [`output table list`] : Tables
    - FILES [`file path`] : Files
    - HEADLINE [`boolean`] : Headline. Default: 1
    - SEPARATOR [`choice`] : Separator. Available Choices: [0] tabulator [1] ; [2] , [3] space [4] other Default: 0
    - SEP_OTHER [`text`] : Separator (other). Default: *

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('io_table', 'import_text_tables', 'Import Text Tables')
    if Tool.is_Okay():
        Tool.Set_Output('TABLES', TABLES)
        Tool.Set_Option('FILES', FILES)
        Tool.Set_Option('HEADLINE', HEADLINE)
        Tool.Set_Option('SEPARATOR', SEPARATOR)
        Tool.Set_Option('SEP_OTHER', SEP_OTHER)
        return Tool.Execute(Verbose)
    return False

def run_tool_io_table_import_text_tables(TABLES=None, FILES=None, HEADLINE=None, SEPARATOR=None, SEP_OTHER=None, Verbose=2):
    '''
    Import Text Tables
    ----------
    [io_table.import_text_tables]\n
    Import Text Tables\n
    Arguments
    ----------
    - TABLES [`output table list`] : Tables
    - FILES [`file path`] : Files
    - HEADLINE [`boolean`] : Headline. Default: 1
    - SEPARATOR [`choice`] : Separator. Available Choices: [0] tabulator [1] ; [2] , [3] space [4] other Default: 0
    - SEP_OTHER [`text`] : Separator (other). Default: *

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('io_table', 'import_text_tables', 'Import Text Tables')
    if Tool.is_Okay():
        Tool.Set_Output('TABLES', TABLES)
        Tool.Set_Option('FILES', FILES)
        Tool.Set_Option('HEADLINE', HEADLINE)
        Tool.Set_Option('SEPARATOR', SEPARATOR)
        Tool.Set_Option('SEP_OTHER', SEP_OTHER)
        return Tool.Execute(Verbose)
    return False

