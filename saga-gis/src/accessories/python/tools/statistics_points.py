#! /usr/bin/env python

'''
Python Interface to SAGA Tools Library
----------
- Category : Spatial and Geostatistics
- Name     : Points
- ID       : statistics_points

Description
----------
Spatial and geostatistical analyses of point data.
'''

from PySAGA.helper import Tool_Wrapper

def Variogram(POINTS=None, RESULT=None, FIELD=None, DISTCOUNT=None, DISTMAX=None, NSKIP=None, Verbose=2):
    '''
    Variogram
    ----------
    [statistics_points.0]\n
    Variogram\n
    Arguments
    ----------
    - POINTS [`input shapes`] : Points
    - RESULT [`output table`] : Sample Variogram
    - FIELD [`table field`] : Attribute
    - DISTCOUNT [`integer number`] : Initial Number of Distance Classes. Minimum: 1 Default: 100
    - DISTMAX [`floating point number`] : Maximum Distance. Minimum: 0.000000 Default: 0.000000
    - NSKIP [`integer number`] : Skip Number. Minimum: 1 Default: 1

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('statistics_points', '0', 'Variogram')
    if Tool.is_Okay():
        Tool.Set_Input ('POINTS', POINTS)
        Tool.Set_Output('RESULT', RESULT)
        Tool.Set_Option('FIELD', FIELD)
        Tool.Set_Option('DISTCOUNT', DISTCOUNT)
        Tool.Set_Option('DISTMAX', DISTMAX)
        Tool.Set_Option('NSKIP', NSKIP)
        return Tool.Execute(Verbose)
    return False

def run_tool_statistics_points_0(POINTS=None, RESULT=None, FIELD=None, DISTCOUNT=None, DISTMAX=None, NSKIP=None, Verbose=2):
    '''
    Variogram
    ----------
    [statistics_points.0]\n
    Variogram\n
    Arguments
    ----------
    - POINTS [`input shapes`] : Points
    - RESULT [`output table`] : Sample Variogram
    - FIELD [`table field`] : Attribute
    - DISTCOUNT [`integer number`] : Initial Number of Distance Classes. Minimum: 1 Default: 100
    - DISTMAX [`floating point number`] : Maximum Distance. Minimum: 0.000000 Default: 0.000000
    - NSKIP [`integer number`] : Skip Number. Minimum: 1 Default: 1

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('statistics_points', '0', 'Variogram')
    if Tool.is_Okay():
        Tool.Set_Input ('POINTS', POINTS)
        Tool.Set_Output('RESULT', RESULT)
        Tool.Set_Option('FIELD', FIELD)
        Tool.Set_Option('DISTCOUNT', DISTCOUNT)
        Tool.Set_Option('DISTMAX', DISTMAX)
        Tool.Set_Option('NSKIP', NSKIP)
        return Tool.Execute(Verbose)
    return False

def Variogram_Cloud(POINTS=None, RESULT=None, FIELD=None, DISTMAX=None, NSKIP=None, Verbose=2):
    '''
    Variogram Cloud
    ----------
    [statistics_points.1]\n
    Variogram Cloud\n
    Arguments
    ----------
    - POINTS [`input shapes`] : Points
    - RESULT [`output table`] : Variogram Cloud
    - FIELD [`table field`] : Attribute
    - DISTMAX [`floating point number`] : Maximum Distance. Minimum: 0.000000 Default: 0.000000
    - NSKIP [`integer number`] : Skip Number. Minimum: 1 Default: 1

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('statistics_points', '1', 'Variogram Cloud')
    if Tool.is_Okay():
        Tool.Set_Input ('POINTS', POINTS)
        Tool.Set_Output('RESULT', RESULT)
        Tool.Set_Option('FIELD', FIELD)
        Tool.Set_Option('DISTMAX', DISTMAX)
        Tool.Set_Option('NSKIP', NSKIP)
        return Tool.Execute(Verbose)
    return False

def run_tool_statistics_points_1(POINTS=None, RESULT=None, FIELD=None, DISTMAX=None, NSKIP=None, Verbose=2):
    '''
    Variogram Cloud
    ----------
    [statistics_points.1]\n
    Variogram Cloud\n
    Arguments
    ----------
    - POINTS [`input shapes`] : Points
    - RESULT [`output table`] : Variogram Cloud
    - FIELD [`table field`] : Attribute
    - DISTMAX [`floating point number`] : Maximum Distance. Minimum: 0.000000 Default: 0.000000
    - NSKIP [`integer number`] : Skip Number. Minimum: 1 Default: 1

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('statistics_points', '1', 'Variogram Cloud')
    if Tool.is_Okay():
        Tool.Set_Input ('POINTS', POINTS)
        Tool.Set_Output('RESULT', RESULT)
        Tool.Set_Option('FIELD', FIELD)
        Tool.Set_Option('DISTMAX', DISTMAX)
        Tool.Set_Option('NSKIP', NSKIP)
        return Tool.Execute(Verbose)
    return False

def Variogram_Surface(POINTS=None, COUNT=None, VARIANCE=None, COVARIANCE=None, FIELD=None, DISTCOUNT=None, NSKIP=None, Verbose=2):
    '''
    Variogram Surface
    ----------
    [statistics_points.2]\n
    Calculates a variogram surface.\n
    Arguments
    ----------
    - POINTS [`input shapes`] : Points
    - COUNT [`output data object`] : Number of Pairs
    - VARIANCE [`output data object`] : Variogram Surface
    - COVARIANCE [`output data object`] : Covariance Surface
    - FIELD [`table field`] : Attribute
    - DISTCOUNT [`integer number`] : Number of Distance Classes. Minimum: 1 Default: 10
    - NSKIP [`integer number`] : Skip Number. Minimum: 1 Default: 1

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('statistics_points', '2', 'Variogram Surface')
    if Tool.is_Okay():
        Tool.Set_Input ('POINTS', POINTS)
        Tool.Set_Output('COUNT', COUNT)
        Tool.Set_Output('VARIANCE', VARIANCE)
        Tool.Set_Output('COVARIANCE', COVARIANCE)
        Tool.Set_Option('FIELD', FIELD)
        Tool.Set_Option('DISTCOUNT', DISTCOUNT)
        Tool.Set_Option('NSKIP', NSKIP)
        return Tool.Execute(Verbose)
    return False

def run_tool_statistics_points_2(POINTS=None, COUNT=None, VARIANCE=None, COVARIANCE=None, FIELD=None, DISTCOUNT=None, NSKIP=None, Verbose=2):
    '''
    Variogram Surface
    ----------
    [statistics_points.2]\n
    Calculates a variogram surface.\n
    Arguments
    ----------
    - POINTS [`input shapes`] : Points
    - COUNT [`output data object`] : Number of Pairs
    - VARIANCE [`output data object`] : Variogram Surface
    - COVARIANCE [`output data object`] : Covariance Surface
    - FIELD [`table field`] : Attribute
    - DISTCOUNT [`integer number`] : Number of Distance Classes. Minimum: 1 Default: 10
    - NSKIP [`integer number`] : Skip Number. Minimum: 1 Default: 1

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('statistics_points', '2', 'Variogram Surface')
    if Tool.is_Okay():
        Tool.Set_Input ('POINTS', POINTS)
        Tool.Set_Output('COUNT', COUNT)
        Tool.Set_Output('VARIANCE', VARIANCE)
        Tool.Set_Output('COVARIANCE', COVARIANCE)
        Tool.Set_Option('FIELD', FIELD)
        Tool.Set_Option('DISTCOUNT', DISTCOUNT)
        Tool.Set_Option('NSKIP', NSKIP)
        return Tool.Execute(Verbose)
    return False

def Minimum_Distance_Analysis(POINTS=None, TABLE=None, DIMENSION=None, Verbose=2):
    '''
    Minimum Distance Analysis
    ----------
    [statistics_points.3]\n
    Minimum Distance Analysis\n
    Arguments
    ----------
    - POINTS [`input shapes`] : Points
    - TABLE [`output table`] : Minimum Distance Analysis
    - DIMENSION [`choice`] : Distance Measurement. Available Choices: [0] 2-dimensional (planar) [1] 3-dimensional Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('statistics_points', '3', 'Minimum Distance Analysis')
    if Tool.is_Okay():
        Tool.Set_Input ('POINTS', POINTS)
        Tool.Set_Output('TABLE', TABLE)
        Tool.Set_Option('DIMENSION', DIMENSION)
        return Tool.Execute(Verbose)
    return False

def run_tool_statistics_points_3(POINTS=None, TABLE=None, DIMENSION=None, Verbose=2):
    '''
    Minimum Distance Analysis
    ----------
    [statistics_points.3]\n
    Minimum Distance Analysis\n
    Arguments
    ----------
    - POINTS [`input shapes`] : Points
    - TABLE [`output table`] : Minimum Distance Analysis
    - DIMENSION [`choice`] : Distance Measurement. Available Choices: [0] 2-dimensional (planar) [1] 3-dimensional Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('statistics_points', '3', 'Minimum Distance Analysis')
    if Tool.is_Okay():
        Tool.Set_Input ('POINTS', POINTS)
        Tool.Set_Output('TABLE', TABLE)
        Tool.Set_Option('DIMENSION', DIMENSION)
        return Tool.Execute(Verbose)
    return False

def Spatial_Point_Pattern_Analysis(POINTS=None, CENTRE=None, STDDIST=None, BBOX=None, WEIGHT=None, STEP=None, Verbose=2):
    '''
    Spatial Point Pattern Analysis
    ----------
    [statistics_points.4]\n
    Basic measures for spatial point patterns.\n
    Arguments
    ----------
    - POINTS [`input shapes`] : Points
    - CENTRE [`output shapes`] : Mean Centre
    - STDDIST [`output shapes`] : Standard Distance
    - BBOX [`output shapes`] : Bounding Box
    - WEIGHT [`table field`] : Weight
    - STEP [`floating point number`] : Vertex Distance [Degree]. Minimum: 0.100000 Maximum: 20.000000 Default: 5.000000

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('statistics_points', '4', 'Spatial Point Pattern Analysis')
    if Tool.is_Okay():
        Tool.Set_Input ('POINTS', POINTS)
        Tool.Set_Output('CENTRE', CENTRE)
        Tool.Set_Output('STDDIST', STDDIST)
        Tool.Set_Output('BBOX', BBOX)
        Tool.Set_Option('WEIGHT', WEIGHT)
        Tool.Set_Option('STEP', STEP)
        return Tool.Execute(Verbose)
    return False

def run_tool_statistics_points_4(POINTS=None, CENTRE=None, STDDIST=None, BBOX=None, WEIGHT=None, STEP=None, Verbose=2):
    '''
    Spatial Point Pattern Analysis
    ----------
    [statistics_points.4]\n
    Basic measures for spatial point patterns.\n
    Arguments
    ----------
    - POINTS [`input shapes`] : Points
    - CENTRE [`output shapes`] : Mean Centre
    - STDDIST [`output shapes`] : Standard Distance
    - BBOX [`output shapes`] : Bounding Box
    - WEIGHT [`table field`] : Weight
    - STEP [`floating point number`] : Vertex Distance [Degree]. Minimum: 0.100000 Maximum: 20.000000 Default: 5.000000

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('statistics_points', '4', 'Spatial Point Pattern Analysis')
    if Tool.is_Okay():
        Tool.Set_Input ('POINTS', POINTS)
        Tool.Set_Output('CENTRE', CENTRE)
        Tool.Set_Output('STDDIST', STDDIST)
        Tool.Set_Output('BBOX', BBOX)
        Tool.Set_Option('WEIGHT', WEIGHT)
        Tool.Set_Option('STEP', STEP)
        return Tool.Execute(Verbose)
    return False

