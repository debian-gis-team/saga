#! /usr/bin/env python

'''
Python Interface to SAGA Tools Library
----------
- Category : Visualization
- Name     : Grids
- ID       : grid_visualisation

Description
----------
Visualization tools for grids.
'''

from PySAGA.helper import Tool_Wrapper

def Color_Palette_Rotation(GRID=None, COLORS=None, DOWN=None, Verbose=2):
    '''
    Color Palette Rotation
    ----------
    [grid_visualisation.0]\n
    The 'Color Palette Rotator' rotates the grids color palette.\n
    Arguments
    ----------
    - GRID [`input grid`] : Grid
    - COLORS [`colors`] : Colors
    - DOWN [`boolean`] : Down. Default: 1

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_visualisation', '0', 'Color Palette Rotation')
    if Tool.is_Okay():
        Tool.Set_Input ('GRID', GRID)
        Tool.Set_Option('COLORS', COLORS)
        Tool.Set_Option('DOWN', DOWN)
        return Tool.Execute(Verbose)
    return False

def run_tool_grid_visualisation_0(GRID=None, COLORS=None, DOWN=None, Verbose=2):
    '''
    Color Palette Rotation
    ----------
    [grid_visualisation.0]\n
    The 'Color Palette Rotator' rotates the grids color palette.\n
    Arguments
    ----------
    - GRID [`input grid`] : Grid
    - COLORS [`colors`] : Colors
    - DOWN [`boolean`] : Down. Default: 1

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_visualisation', '0', 'Color Palette Rotation')
    if Tool.is_Okay():
        Tool.Set_Input ('GRID', GRID)
        Tool.Set_Option('COLORS', COLORS)
        Tool.Set_Option('DOWN', DOWN)
        return Tool.Execute(Verbose)
    return False

def Grid_Animation(GRIDS=None, GRID=None, FILE=None, FILE_NODATA=None, FILE_BGCOL=None, FILE_DELAY=None, FILE_COLORS=None, COLORS=None, NSTEPS=None, PROGRESS=None, LOOP=None, RANGE=None, RANGE_PERCENT=None, RANGE_STDDEV=None, RANGE_KEEP=None, RANGE_USER=None, Verbose=2):
    '''
    Grid Animation
    ----------
    [grid_visualisation.1]\n
    Creates an animation based on the values of selected grids. Previously known as 'Color Blending'.\n
    Arguments
    ----------
    - GRIDS [`input grid list`] : Grids
    - GRID [`output grid`] : Grid Animation
    - FILE [`file path`] : Save Frames. Store each frame as image file. If GIF format is selected one animated image file will be created.
    - FILE_NODATA [`boolean`] : Set Transparency for No-Data. Default: 1
    - FILE_BGCOL [`color`] : Background Color. Default: 16777215 Background color used for no-data cells when storing frames to file.
    - FILE_DELAY [`integer number`] : Delay. Minimum: 0 Default: 100 Delay, in milliseconds, to wait between each frame. Applies to animated GIF files.
    - FILE_COLORS [`integer number`] : Color Depth. Minimum: 2 Maximum: 256 Default: 236 Number of color entries used when storing animated GIF.
    - COLORS [`colors`] : Colors
    - NSTEPS [`integer number`] : Interpolation Steps. Minimum: 0 Default: 0
    - PROGRESS [`boolean`] : Progress Bar. Default: 0
    - LOOP [`choice`] : Loop. Available Choices: [0] do not loop [1] loop to first grid [2] loop Default: 0 Endless loop (3rd option) is ignored if file output is activated.
    - RANGE [`choice`] : Histogram Stretch. Available Choices: [0] each grid's range [1] each grid's standard deviation [2] overall range [3] overall standard deviation [4] user defined Default: 3
    - RANGE_PERCENT [`floating point number`] : Percent Stretch. Minimum: 0.000000 Maximum: 50.000000 Default: 2.000000
    - RANGE_STDDEV [`floating point number`] : Standard Deviation. Minimum: 0.000000 Default: 2.000000
    - RANGE_KEEP [`boolean`] : Keep in Range. Default: 1
    - RANGE_USER [`value range`] : Range

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_visualisation', '1', 'Grid Animation')
    if Tool.is_Okay():
        Tool.Set_Input ('GRIDS', GRIDS)
        Tool.Set_Output('GRID', GRID)
        Tool.Set_Option('FILE', FILE)
        Tool.Set_Option('FILE_NODATA', FILE_NODATA)
        Tool.Set_Option('FILE_BGCOL', FILE_BGCOL)
        Tool.Set_Option('FILE_DELAY', FILE_DELAY)
        Tool.Set_Option('FILE_COLORS', FILE_COLORS)
        Tool.Set_Option('COLORS', COLORS)
        Tool.Set_Option('NSTEPS', NSTEPS)
        Tool.Set_Option('PROGRESS', PROGRESS)
        Tool.Set_Option('LOOP', LOOP)
        Tool.Set_Option('RANGE', RANGE)
        Tool.Set_Option('RANGE_PERCENT', RANGE_PERCENT)
        Tool.Set_Option('RANGE_STDDEV', RANGE_STDDEV)
        Tool.Set_Option('RANGE_KEEP', RANGE_KEEP)
        Tool.Set_Option('RANGE_USER', RANGE_USER)
        return Tool.Execute(Verbose)
    return False

def run_tool_grid_visualisation_1(GRIDS=None, GRID=None, FILE=None, FILE_NODATA=None, FILE_BGCOL=None, FILE_DELAY=None, FILE_COLORS=None, COLORS=None, NSTEPS=None, PROGRESS=None, LOOP=None, RANGE=None, RANGE_PERCENT=None, RANGE_STDDEV=None, RANGE_KEEP=None, RANGE_USER=None, Verbose=2):
    '''
    Grid Animation
    ----------
    [grid_visualisation.1]\n
    Creates an animation based on the values of selected grids. Previously known as 'Color Blending'.\n
    Arguments
    ----------
    - GRIDS [`input grid list`] : Grids
    - GRID [`output grid`] : Grid Animation
    - FILE [`file path`] : Save Frames. Store each frame as image file. If GIF format is selected one animated image file will be created.
    - FILE_NODATA [`boolean`] : Set Transparency for No-Data. Default: 1
    - FILE_BGCOL [`color`] : Background Color. Default: 16777215 Background color used for no-data cells when storing frames to file.
    - FILE_DELAY [`integer number`] : Delay. Minimum: 0 Default: 100 Delay, in milliseconds, to wait between each frame. Applies to animated GIF files.
    - FILE_COLORS [`integer number`] : Color Depth. Minimum: 2 Maximum: 256 Default: 236 Number of color entries used when storing animated GIF.
    - COLORS [`colors`] : Colors
    - NSTEPS [`integer number`] : Interpolation Steps. Minimum: 0 Default: 0
    - PROGRESS [`boolean`] : Progress Bar. Default: 0
    - LOOP [`choice`] : Loop. Available Choices: [0] do not loop [1] loop to first grid [2] loop Default: 0 Endless loop (3rd option) is ignored if file output is activated.
    - RANGE [`choice`] : Histogram Stretch. Available Choices: [0] each grid's range [1] each grid's standard deviation [2] overall range [3] overall standard deviation [4] user defined Default: 3
    - RANGE_PERCENT [`floating point number`] : Percent Stretch. Minimum: 0.000000 Maximum: 50.000000 Default: 2.000000
    - RANGE_STDDEV [`floating point number`] : Standard Deviation. Minimum: 0.000000 Default: 2.000000
    - RANGE_KEEP [`boolean`] : Keep in Range. Default: 1
    - RANGE_USER [`value range`] : Range

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_visualisation', '1', 'Grid Animation')
    if Tool.is_Okay():
        Tool.Set_Input ('GRIDS', GRIDS)
        Tool.Set_Output('GRID', GRID)
        Tool.Set_Option('FILE', FILE)
        Tool.Set_Option('FILE_NODATA', FILE_NODATA)
        Tool.Set_Option('FILE_BGCOL', FILE_BGCOL)
        Tool.Set_Option('FILE_DELAY', FILE_DELAY)
        Tool.Set_Option('FILE_COLORS', FILE_COLORS)
        Tool.Set_Option('COLORS', COLORS)
        Tool.Set_Option('NSTEPS', NSTEPS)
        Tool.Set_Option('PROGRESS', PROGRESS)
        Tool.Set_Option('LOOP', LOOP)
        Tool.Set_Option('RANGE', RANGE)
        Tool.Set_Option('RANGE_PERCENT', RANGE_PERCENT)
        Tool.Set_Option('RANGE_STDDEV', RANGE_STDDEV)
        Tool.Set_Option('RANGE_KEEP', RANGE_KEEP)
        Tool.Set_Option('RANGE_USER', RANGE_USER)
        return Tool.Execute(Verbose)
    return False

def Fit_Color_Palette_to_Grid_Values(GRID=None, COUNT=None, SCALE=None, RANGE=None, Verbose=2):
    '''
    Fit Color Palette to Grid Values
    ----------
    [grid_visualisation.2]\n
    Fit Color Palette to Grid Values\n
    Arguments
    ----------
    - GRID [`input grid`] : Grid
    - COUNT [`integer number`] : Number of Colors. Minimum: 2 Default: 100
    - SCALE [`choice`] : Scale. Available Choices: [0] Grid range [1] User defined range Default: 0
    - RANGE [`value range`] : User defined range

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_visualisation', '2', 'Fit Color Palette to Grid Values')
    if Tool.is_Okay():
        Tool.Set_Input ('GRID', GRID)
        Tool.Set_Option('COUNT', COUNT)
        Tool.Set_Option('SCALE', SCALE)
        Tool.Set_Option('RANGE', RANGE)
        return Tool.Execute(Verbose)
    return False

def run_tool_grid_visualisation_2(GRID=None, COUNT=None, SCALE=None, RANGE=None, Verbose=2):
    '''
    Fit Color Palette to Grid Values
    ----------
    [grid_visualisation.2]\n
    Fit Color Palette to Grid Values\n
    Arguments
    ----------
    - GRID [`input grid`] : Grid
    - COUNT [`integer number`] : Number of Colors. Minimum: 2 Default: 100
    - SCALE [`choice`] : Scale. Available Choices: [0] Grid range [1] User defined range Default: 0
    - RANGE [`value range`] : User defined range

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_visualisation', '2', 'Fit Color Palette to Grid Values')
    if Tool.is_Okay():
        Tool.Set_Input ('GRID', GRID)
        Tool.Set_Option('COUNT', COUNT)
        Tool.Set_Option('SCALE', SCALE)
        Tool.Set_Option('RANGE', RANGE)
        return Tool.Execute(Verbose)
    return False

def RGB_Composite(R_GRID=None, G_GRID=None, B_GRID=None, A_GRID=None, RGB=None, METHOD=None, RANGE=None, PERCTL=None, STDDEV=None, NODATA=None, Verbose=2):
    '''
    RGB Composite
    ----------
    [grid_visualisation.3]\n
    Create red-green-blue overlays of grids.\n
    Arguments
    ----------
    - R_GRID [`input grid`] : Red
    - G_GRID [`input grid`] : Green
    - B_GRID [`input grid`] : Blue
    - A_GRID [`optional input grid`] : Alpha
    - RGB [`output grid`] : Composite
    - METHOD [`choice`] : Value Preparation. Available Choices: [0] take original value (0 - 255) [1] rescale to 0 - 255 [2] user defined [3] percentiles [4] standard deviation Default: 0
    - RANGE [`value range`] : Rescale Range
    - PERCTL [`value range`] : Percentiles
    - STDDEV [`floating point number`] : Standard Deviation. Minimum: 0.000000 Default: 2.000000
    - NODATA [`boolean`] : Ignore No Data Cells. Default: 1

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_visualisation', '3', 'RGB Composite')
    if Tool.is_Okay():
        Tool.Set_Input ('R_GRID', R_GRID)
        Tool.Set_Input ('G_GRID', G_GRID)
        Tool.Set_Input ('B_GRID', B_GRID)
        Tool.Set_Input ('A_GRID', A_GRID)
        Tool.Set_Output('RGB', RGB)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('RANGE', RANGE)
        Tool.Set_Option('PERCTL', PERCTL)
        Tool.Set_Option('STDDEV', STDDEV)
        Tool.Set_Option('NODATA', NODATA)
        return Tool.Execute(Verbose)
    return False

def run_tool_grid_visualisation_3(R_GRID=None, G_GRID=None, B_GRID=None, A_GRID=None, RGB=None, METHOD=None, RANGE=None, PERCTL=None, STDDEV=None, NODATA=None, Verbose=2):
    '''
    RGB Composite
    ----------
    [grid_visualisation.3]\n
    Create red-green-blue overlays of grids.\n
    Arguments
    ----------
    - R_GRID [`input grid`] : Red
    - G_GRID [`input grid`] : Green
    - B_GRID [`input grid`] : Blue
    - A_GRID [`optional input grid`] : Alpha
    - RGB [`output grid`] : Composite
    - METHOD [`choice`] : Value Preparation. Available Choices: [0] take original value (0 - 255) [1] rescale to 0 - 255 [2] user defined [3] percentiles [4] standard deviation Default: 0
    - RANGE [`value range`] : Rescale Range
    - PERCTL [`value range`] : Percentiles
    - STDDEV [`floating point number`] : Standard Deviation. Minimum: 0.000000 Default: 2.000000
    - NODATA [`boolean`] : Ignore No Data Cells. Default: 1

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_visualisation', '3', 'RGB Composite')
    if Tool.is_Okay():
        Tool.Set_Input ('R_GRID', R_GRID)
        Tool.Set_Input ('G_GRID', G_GRID)
        Tool.Set_Input ('B_GRID', B_GRID)
        Tool.Set_Input ('A_GRID', A_GRID)
        Tool.Set_Output('RGB', RGB)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('RANGE', RANGE)
        Tool.Set_Option('PERCTL', PERCTL)
        Tool.Set_Option('STDDEV', STDDEV)
        Tool.Set_Option('NODATA', NODATA)
        return Tool.Execute(Verbose)
    return False

def Create_3D_Image(DEM=None, IMAGE=None, SHAPES=None, RGB=None, RGB_Z=None, ZEXAGG=None, ZEXAGG_MIN=None, Z_ROTATE=None, X_ROTATE=None, X_ROTATE_LEVEL=None, PANBREAK=None, BKCOLOR=None, PROJECTION=None, NX=None, NY=None, Verbose=2):
    '''
    Create 3D Image
    ----------
    [grid_visualisation.4]\n
    Create 3D Image\n
    Arguments
    ----------
    - DEM [`input grid`] : Elevation
    - IMAGE [`input grid`] : Overlay Image
    - SHAPES [`optional input shapes list`] : Shapes to project
    - RGB [`output grid`] : 3D Image
    - RGB_Z [`output grid`] : Projected Height
    - ZEXAGG [`floating point number`] : Exaggeration. Default: 1.000000
    - ZEXAGG_MIN [`floating point number`] : Minimum Exaggeration [%]. Minimum: 0.000000 Maximum: 100.000000 Default: 10.000000
    - Z_ROTATE [`floating point number`] : Image Rotation [Degree]. Default: 0.000000
    - X_ROTATE [`floating point number`] : Local Rotation [Degree]. Default: 1.000000
    - X_ROTATE_LEVEL [`choice`] : Local Rotation Base Level. Available Choices: [0] Zero [1] Mean Elevation Default: 1
    - PANBREAK [`floating point number`] : Panorama Break [%]. Minimum: 0.000000 Maximum: 100.000000 Default: 70.000000
    - BKCOLOR [`color`] : Background Color. Default: 0
    - PROJECTION [`choice`] : Projection. Available Choices: [0] Panorama [1] Circular Default: 0
    - NX [`integer number`] : 3D Image Width. Minimum: 1 Default: 100
    - NY [`integer number`] : 3D Image Height. Minimum: 1 Default: 100

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_visualisation', '4', 'Create 3D Image')
    if Tool.is_Okay():
        Tool.Set_Input ('DEM', DEM)
        Tool.Set_Input ('IMAGE', IMAGE)
        Tool.Set_Input ('SHAPES', SHAPES)
        Tool.Set_Output('RGB', RGB)
        Tool.Set_Output('RGB_Z', RGB_Z)
        Tool.Set_Option('ZEXAGG', ZEXAGG)
        Tool.Set_Option('ZEXAGG_MIN', ZEXAGG_MIN)
        Tool.Set_Option('Z_ROTATE', Z_ROTATE)
        Tool.Set_Option('X_ROTATE', X_ROTATE)
        Tool.Set_Option('X_ROTATE_LEVEL', X_ROTATE_LEVEL)
        Tool.Set_Option('PANBREAK', PANBREAK)
        Tool.Set_Option('BKCOLOR', BKCOLOR)
        Tool.Set_Option('PROJECTION', PROJECTION)
        Tool.Set_Option('NX', NX)
        Tool.Set_Option('NY', NY)
        return Tool.Execute(Verbose)
    return False

def run_tool_grid_visualisation_4(DEM=None, IMAGE=None, SHAPES=None, RGB=None, RGB_Z=None, ZEXAGG=None, ZEXAGG_MIN=None, Z_ROTATE=None, X_ROTATE=None, X_ROTATE_LEVEL=None, PANBREAK=None, BKCOLOR=None, PROJECTION=None, NX=None, NY=None, Verbose=2):
    '''
    Create 3D Image
    ----------
    [grid_visualisation.4]\n
    Create 3D Image\n
    Arguments
    ----------
    - DEM [`input grid`] : Elevation
    - IMAGE [`input grid`] : Overlay Image
    - SHAPES [`optional input shapes list`] : Shapes to project
    - RGB [`output grid`] : 3D Image
    - RGB_Z [`output grid`] : Projected Height
    - ZEXAGG [`floating point number`] : Exaggeration. Default: 1.000000
    - ZEXAGG_MIN [`floating point number`] : Minimum Exaggeration [%]. Minimum: 0.000000 Maximum: 100.000000 Default: 10.000000
    - Z_ROTATE [`floating point number`] : Image Rotation [Degree]. Default: 0.000000
    - X_ROTATE [`floating point number`] : Local Rotation [Degree]. Default: 1.000000
    - X_ROTATE_LEVEL [`choice`] : Local Rotation Base Level. Available Choices: [0] Zero [1] Mean Elevation Default: 1
    - PANBREAK [`floating point number`] : Panorama Break [%]. Minimum: 0.000000 Maximum: 100.000000 Default: 70.000000
    - BKCOLOR [`color`] : Background Color. Default: 0
    - PROJECTION [`choice`] : Projection. Available Choices: [0] Panorama [1] Circular Default: 0
    - NX [`integer number`] : 3D Image Width. Minimum: 1 Default: 100
    - NY [`integer number`] : 3D Image Height. Minimum: 1 Default: 100

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_visualisation', '4', 'Create 3D Image')
    if Tool.is_Okay():
        Tool.Set_Input ('DEM', DEM)
        Tool.Set_Input ('IMAGE', IMAGE)
        Tool.Set_Input ('SHAPES', SHAPES)
        Tool.Set_Output('RGB', RGB)
        Tool.Set_Output('RGB_Z', RGB_Z)
        Tool.Set_Option('ZEXAGG', ZEXAGG)
        Tool.Set_Option('ZEXAGG_MIN', ZEXAGG_MIN)
        Tool.Set_Option('Z_ROTATE', Z_ROTATE)
        Tool.Set_Option('X_ROTATE', X_ROTATE)
        Tool.Set_Option('X_ROTATE_LEVEL', X_ROTATE_LEVEL)
        Tool.Set_Option('PANBREAK', PANBREAK)
        Tool.Set_Option('BKCOLOR', BKCOLOR)
        Tool.Set_Option('PROJECTION', PROJECTION)
        Tool.Set_Option('NX', NX)
        Tool.Set_Option('NY', NY)
        return Tool.Execute(Verbose)
    return False

def Color_Triangle_Composite(A_GRID=None, B_GRID=None, C_GRID=None, GRID=None, A_COLOR=None, A_METHOD=None, A_RANGE=None, A_PERCTL=None, A_PERCENT=None, B_COLOR=None, B_METHOD=None, B_RANGE=None, B_PERCTL=None, B_PERCENT=None, C_COLOR=None, C_METHOD=None, C_RANGE=None, C_PERCTL=None, C_PERCENT=None, Verbose=2):
    '''
    Color Triangle Composite
    ----------
    [grid_visualisation.5]\n
    Similar to 'RGB Composite', but the three colors representing intensity of each data set can be chosen by user.\n
    Arguments
    ----------
    - A_GRID [`input grid`] : A
    - B_GRID [`input grid`] : B
    - C_GRID [`input grid`] : C
    - GRID [`output grid`] : Composite
    - A_COLOR [`color`] : Color. Default: 255
    - A_METHOD [`choice`] : Value Preparation. Available Choices: [0] 0 - 1 [1] Rescale to 0 - 1 [2] User defined rescale [3] Percentiles [4] Percentage of standard deviation Default: 4
    - A_RANGE [`value range`] : Rescale Range
    - A_PERCTL [`value range`] : Percentiles
    - A_PERCENT [`floating point number`] : Percentage of Standard Deviation. Minimum: 0.000000 Default: 150.000000
    - B_COLOR [`color`] : Color. Default: 65280
    - B_METHOD [`choice`] : Value Preparation. Available Choices: [0] 0 - 1 [1] Rescale to 0 - 1 [2] User defined rescale [3] Percentiles [4] Percentage of standard deviation Default: 4
    - B_RANGE [`value range`] : Rescale Range
    - B_PERCTL [`value range`] : Percentiles
    - B_PERCENT [`floating point number`] : Percentage of standard deviation. Minimum: 0.000000 Default: 150.000000
    - C_COLOR [`color`] : Color. Default: 16711680
    - C_METHOD [`choice`] : Value Preparation. Available Choices: [0] 0 - 1 [1] Rescale to 0 - 1 [2] User defined rescale [3] Percentiles [4] Percentage of standard deviation Default: 4
    - C_RANGE [`value range`] : Rescale Range
    - C_PERCTL [`value range`] : Percentiles
    - C_PERCENT [`floating point number`] : Percentage of Standard Deviation. Minimum: 0.000000 Default: 150.000000

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_visualisation', '5', 'Color Triangle Composite')
    if Tool.is_Okay():
        Tool.Set_Input ('A_GRID', A_GRID)
        Tool.Set_Input ('B_GRID', B_GRID)
        Tool.Set_Input ('C_GRID', C_GRID)
        Tool.Set_Output('GRID', GRID)
        Tool.Set_Option('A_COLOR', A_COLOR)
        Tool.Set_Option('A_METHOD', A_METHOD)
        Tool.Set_Option('A_RANGE', A_RANGE)
        Tool.Set_Option('A_PERCTL', A_PERCTL)
        Tool.Set_Option('A_PERCENT', A_PERCENT)
        Tool.Set_Option('B_COLOR', B_COLOR)
        Tool.Set_Option('B_METHOD', B_METHOD)
        Tool.Set_Option('B_RANGE', B_RANGE)
        Tool.Set_Option('B_PERCTL', B_PERCTL)
        Tool.Set_Option('B_PERCENT', B_PERCENT)
        Tool.Set_Option('C_COLOR', C_COLOR)
        Tool.Set_Option('C_METHOD', C_METHOD)
        Tool.Set_Option('C_RANGE', C_RANGE)
        Tool.Set_Option('C_PERCTL', C_PERCTL)
        Tool.Set_Option('C_PERCENT', C_PERCENT)
        return Tool.Execute(Verbose)
    return False

def run_tool_grid_visualisation_5(A_GRID=None, B_GRID=None, C_GRID=None, GRID=None, A_COLOR=None, A_METHOD=None, A_RANGE=None, A_PERCTL=None, A_PERCENT=None, B_COLOR=None, B_METHOD=None, B_RANGE=None, B_PERCTL=None, B_PERCENT=None, C_COLOR=None, C_METHOD=None, C_RANGE=None, C_PERCTL=None, C_PERCENT=None, Verbose=2):
    '''
    Color Triangle Composite
    ----------
    [grid_visualisation.5]\n
    Similar to 'RGB Composite', but the three colors representing intensity of each data set can be chosen by user.\n
    Arguments
    ----------
    - A_GRID [`input grid`] : A
    - B_GRID [`input grid`] : B
    - C_GRID [`input grid`] : C
    - GRID [`output grid`] : Composite
    - A_COLOR [`color`] : Color. Default: 255
    - A_METHOD [`choice`] : Value Preparation. Available Choices: [0] 0 - 1 [1] Rescale to 0 - 1 [2] User defined rescale [3] Percentiles [4] Percentage of standard deviation Default: 4
    - A_RANGE [`value range`] : Rescale Range
    - A_PERCTL [`value range`] : Percentiles
    - A_PERCENT [`floating point number`] : Percentage of Standard Deviation. Minimum: 0.000000 Default: 150.000000
    - B_COLOR [`color`] : Color. Default: 65280
    - B_METHOD [`choice`] : Value Preparation. Available Choices: [0] 0 - 1 [1] Rescale to 0 - 1 [2] User defined rescale [3] Percentiles [4] Percentage of standard deviation Default: 4
    - B_RANGE [`value range`] : Rescale Range
    - B_PERCTL [`value range`] : Percentiles
    - B_PERCENT [`floating point number`] : Percentage of standard deviation. Minimum: 0.000000 Default: 150.000000
    - C_COLOR [`color`] : Color. Default: 16711680
    - C_METHOD [`choice`] : Value Preparation. Available Choices: [0] 0 - 1 [1] Rescale to 0 - 1 [2] User defined rescale [3] Percentiles [4] Percentage of standard deviation Default: 4
    - C_RANGE [`value range`] : Rescale Range
    - C_PERCTL [`value range`] : Percentiles
    - C_PERCENT [`floating point number`] : Percentage of Standard Deviation. Minimum: 0.000000 Default: 150.000000

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_visualisation', '5', 'Color Triangle Composite')
    if Tool.is_Okay():
        Tool.Set_Input ('A_GRID', A_GRID)
        Tool.Set_Input ('B_GRID', B_GRID)
        Tool.Set_Input ('C_GRID', C_GRID)
        Tool.Set_Output('GRID', GRID)
        Tool.Set_Option('A_COLOR', A_COLOR)
        Tool.Set_Option('A_METHOD', A_METHOD)
        Tool.Set_Option('A_RANGE', A_RANGE)
        Tool.Set_Option('A_PERCTL', A_PERCTL)
        Tool.Set_Option('A_PERCENT', A_PERCENT)
        Tool.Set_Option('B_COLOR', B_COLOR)
        Tool.Set_Option('B_METHOD', B_METHOD)
        Tool.Set_Option('B_RANGE', B_RANGE)
        Tool.Set_Option('B_PERCTL', B_PERCTL)
        Tool.Set_Option('B_PERCENT', B_PERCENT)
        Tool.Set_Option('C_COLOR', C_COLOR)
        Tool.Set_Option('C_METHOD', C_METHOD)
        Tool.Set_Option('C_RANGE', C_RANGE)
        Tool.Set_Option('C_PERCTL', C_PERCTL)
        Tool.Set_Option('C_PERCENT', C_PERCENT)
        return Tool.Execute(Verbose)
    return False

def Histogram_Surface(GRID=None, HIST=None, METHOD=None, Verbose=2):
    '''
    Histogram Surface
    ----------
    [grid_visualisation.6]\n
    Histogram Surface\n
    Arguments
    ----------
    - GRID [`input grid`] : Grid
    - HIST [`output data object`] : Histogram
    - METHOD [`choice`] : Method. Available Choices: [0] rows [1] columns [2] circle Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_visualisation', '6', 'Histogram Surface')
    if Tool.is_Okay():
        Tool.Set_Input ('GRID', GRID)
        Tool.Set_Output('HIST', HIST)
        Tool.Set_Option('METHOD', METHOD)
        return Tool.Execute(Verbose)
    return False

def run_tool_grid_visualisation_6(GRID=None, HIST=None, METHOD=None, Verbose=2):
    '''
    Histogram Surface
    ----------
    [grid_visualisation.6]\n
    Histogram Surface\n
    Arguments
    ----------
    - GRID [`input grid`] : Grid
    - HIST [`output data object`] : Histogram
    - METHOD [`choice`] : Method. Available Choices: [0] rows [1] columns [2] circle Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_visualisation', '6', 'Histogram Surface')
    if Tool.is_Okay():
        Tool.Set_Input ('GRID', GRID)
        Tool.Set_Output('HIST', HIST)
        Tool.Set_Option('METHOD', METHOD)
        return Tool.Execute(Verbose)
    return False

def AspectSlope_Grid(ASPECT=None, SLOPE=None, ASPECT_SLOPE=None, LUT=None, Verbose=2):
    '''
    Aspect-Slope Grid
    ----------
    [grid_visualisation.7]\n
    This tool creates an aspect-slope map which shows both the aspect and the slope of the terrain. Aspect is symbolized by different hues, while slope is mapped with saturation.\n
    References:\n
    Brewer, C.A. & Marlow, K.A. (1993): Color Representation of Aspect and Slope simultaneously. Proceedings, Eleventh International Symposium on Computer-Assisted Cartography (Auto-Carto-11), Minneapolis, October/November 1993, pp. 328-337.\n
    [http://www.personal.psu.edu/cab38/Terrain/AutoCarto.html](http://www.personal.psu.edu/cab38/Terrain/AutoCarto.html)\n
    [http://blogs.esri.com/esri/arcgis/2008/05/23/aspect-slope-map/](http://blogs.esri.com/esri/arcgis/2008/05/23/aspect-slope-map/)\n
    Arguments
    ----------
    - ASPECT [`input grid`] : Aspect. Aspect grid, in radians and 360 degree from north.
    - SLOPE [`input grid`] : Slope. Slope grid, in radians.
    - ASPECT_SLOPE [`output grid`] : Aspect-Slope. Final aspect-slope grid.
    - LUT [`output table`] : Lookup Table. Lookup table.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_visualisation', '7', 'Aspect-Slope Grid')
    if Tool.is_Okay():
        Tool.Set_Input ('ASPECT', ASPECT)
        Tool.Set_Input ('SLOPE', SLOPE)
        Tool.Set_Output('ASPECT_SLOPE', ASPECT_SLOPE)
        Tool.Set_Output('LUT', LUT)
        return Tool.Execute(Verbose)
    return False

def run_tool_grid_visualisation_7(ASPECT=None, SLOPE=None, ASPECT_SLOPE=None, LUT=None, Verbose=2):
    '''
    Aspect-Slope Grid
    ----------
    [grid_visualisation.7]\n
    This tool creates an aspect-slope map which shows both the aspect and the slope of the terrain. Aspect is symbolized by different hues, while slope is mapped with saturation.\n
    References:\n
    Brewer, C.A. & Marlow, K.A. (1993): Color Representation of Aspect and Slope simultaneously. Proceedings, Eleventh International Symposium on Computer-Assisted Cartography (Auto-Carto-11), Minneapolis, October/November 1993, pp. 328-337.\n
    [http://www.personal.psu.edu/cab38/Terrain/AutoCarto.html](http://www.personal.psu.edu/cab38/Terrain/AutoCarto.html)\n
    [http://blogs.esri.com/esri/arcgis/2008/05/23/aspect-slope-map/](http://blogs.esri.com/esri/arcgis/2008/05/23/aspect-slope-map/)\n
    Arguments
    ----------
    - ASPECT [`input grid`] : Aspect. Aspect grid, in radians and 360 degree from north.
    - SLOPE [`input grid`] : Slope. Slope grid, in radians.
    - ASPECT_SLOPE [`output grid`] : Aspect-Slope. Final aspect-slope grid.
    - LUT [`output table`] : Lookup Table. Lookup table.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_visualisation', '7', 'Aspect-Slope Grid')
    if Tool.is_Okay():
        Tool.Set_Input ('ASPECT', ASPECT)
        Tool.Set_Input ('SLOPE', SLOPE)
        Tool.Set_Output('ASPECT_SLOPE', ASPECT_SLOPE)
        Tool.Set_Output('LUT', LUT)
        return Tool.Execute(Verbose)
    return False

def Terrain_Map_View(DEM=None, SHADE=None, OPENNESS=None, SLOPE=None, CONTOURS=None, METHOD=None, RADIUS=None, CONTOUR_LINES=None, EQUIDISTANCE=None, Verbose=2):
    '''
    Terrain Map View
    ----------
    [grid_visualisation.8]\n
    This tool allows one to create different terrain visualisations from an elevation dataset:\n
    * Topography: a simple map with an analytical hillshading of the terrain\n
    * Morphology: a map which visualizes the terrain by combining positive and negative openness (Yokoyama et al. 2002) with terrain slope in a single map. In contrast to conventional shading methods this has the advantage of being independent from the direction of the light source.\n
    Arguments
    ----------
    - DEM [`input grid`] : DEM. Digital elevation model.
    - SHADE [`output grid`] : Shade. The shaded DTM.
    - OPENNESS [`output grid`] : Openness. The difference of positive and negative openness.
    - SLOPE [`output grid`] : Slope. The calculated terrain slope [radians].
    - CONTOURS [`output shapes`] : Contours. The generated contour lines.
    - METHOD [`choice`] : Method. Available Choices: [0] Topography [1] Morphology Default: 0 Choose the map type to generate.
    - RADIUS [`floating point number`] : Radial Limit. Minimum: 0.000000 Default: 1000.000000 Radial search limit for openness calculation.
    - CONTOUR_LINES [`boolean`] : Contour Lines. Default: 1 Derive contour lines.
    - EQUIDISTANCE [`floating point number`] : Equidistance. Minimum: 0.000000 Default: 50.000000 Contour lines equidistance [map units].

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_visualisation', '8', 'Terrain Map View')
    if Tool.is_Okay():
        Tool.Set_Input ('DEM', DEM)
        Tool.Set_Output('SHADE', SHADE)
        Tool.Set_Output('OPENNESS', OPENNESS)
        Tool.Set_Output('SLOPE', SLOPE)
        Tool.Set_Output('CONTOURS', CONTOURS)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('RADIUS', RADIUS)
        Tool.Set_Option('CONTOUR_LINES', CONTOUR_LINES)
        Tool.Set_Option('EQUIDISTANCE', EQUIDISTANCE)
        return Tool.Execute(Verbose)
    return False

def run_tool_grid_visualisation_8(DEM=None, SHADE=None, OPENNESS=None, SLOPE=None, CONTOURS=None, METHOD=None, RADIUS=None, CONTOUR_LINES=None, EQUIDISTANCE=None, Verbose=2):
    '''
    Terrain Map View
    ----------
    [grid_visualisation.8]\n
    This tool allows one to create different terrain visualisations from an elevation dataset:\n
    * Topography: a simple map with an analytical hillshading of the terrain\n
    * Morphology: a map which visualizes the terrain by combining positive and negative openness (Yokoyama et al. 2002) with terrain slope in a single map. In contrast to conventional shading methods this has the advantage of being independent from the direction of the light source.\n
    Arguments
    ----------
    - DEM [`input grid`] : DEM. Digital elevation model.
    - SHADE [`output grid`] : Shade. The shaded DTM.
    - OPENNESS [`output grid`] : Openness. The difference of positive and negative openness.
    - SLOPE [`output grid`] : Slope. The calculated terrain slope [radians].
    - CONTOURS [`output shapes`] : Contours. The generated contour lines.
    - METHOD [`choice`] : Method. Available Choices: [0] Topography [1] Morphology Default: 0 Choose the map type to generate.
    - RADIUS [`floating point number`] : Radial Limit. Minimum: 0.000000 Default: 1000.000000 Radial search limit for openness calculation.
    - CONTOUR_LINES [`boolean`] : Contour Lines. Default: 1 Derive contour lines.
    - EQUIDISTANCE [`floating point number`] : Equidistance. Minimum: 0.000000 Default: 50.000000 Contour lines equidistance [map units].

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_visualisation', '8', 'Terrain Map View')
    if Tool.is_Okay():
        Tool.Set_Input ('DEM', DEM)
        Tool.Set_Output('SHADE', SHADE)
        Tool.Set_Output('OPENNESS', OPENNESS)
        Tool.Set_Output('SLOPE', SLOPE)
        Tool.Set_Output('CONTOURS', CONTOURS)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('RADIUS', RADIUS)
        Tool.Set_Option('CONTOUR_LINES', CONTOUR_LINES)
        Tool.Set_Option('EQUIDISTANCE', EQUIDISTANCE)
        return Tool.Execute(Verbose)
    return False

def Split_RGB_Composite(RGB=None, R=None, G=None, B=None, A=None, NODATA=None, Verbose=2):
    '''
    Split RGB Composite
    ----------
    [grid_visualisation.9]\n
    Split red-green-blue channels of an rgb coded grid.\n
    Arguments
    ----------
    - RGB [`input grid`] : RGB Composite
    - R [`output grid`] : Red
    - G [`output grid`] : Green
    - B [`output grid`] : Blue
    - A [`output grid`] : Alpha
    - NODATA [`boolean`] : Ignore No Data. Default: 1

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_visualisation', '9', 'Split RGB Composite')
    if Tool.is_Okay():
        Tool.Set_Input ('RGB', RGB)
        Tool.Set_Output('R', R)
        Tool.Set_Output('G', G)
        Tool.Set_Output('B', B)
        Tool.Set_Output('A', A)
        Tool.Set_Option('NODATA', NODATA)
        return Tool.Execute(Verbose)
    return False

def run_tool_grid_visualisation_9(RGB=None, R=None, G=None, B=None, A=None, NODATA=None, Verbose=2):
    '''
    Split RGB Composite
    ----------
    [grid_visualisation.9]\n
    Split red-green-blue channels of an rgb coded grid.\n
    Arguments
    ----------
    - RGB [`input grid`] : RGB Composite
    - R [`output grid`] : Red
    - G [`output grid`] : Green
    - B [`output grid`] : Blue
    - A [`output grid`] : Alpha
    - NODATA [`boolean`] : Ignore No Data. Default: 1

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_visualisation', '9', 'Split RGB Composite')
    if Tool.is_Okay():
        Tool.Set_Input ('RGB', RGB)
        Tool.Set_Output('R', R)
        Tool.Set_Output('G', G)
        Tool.Set_Output('B', B)
        Tool.Set_Output('A', A)
        Tool.Set_Option('NODATA', NODATA)
        return Tool.Execute(Verbose)
    return False

def Select_Lookup_Table_for_Grid_Visualization(GRID=None, LUT=None, NAME=None, VALUE=None, VALUE_MAX=None, DESCRIPTION=None, COLOR=None, Verbose=2):
    '''
    Select Look-up Table for Grid Visualization
    ----------
    [grid_visualisation.10]\n
    Select a look-up table for visual classification of a grid. Useful in combination with tool chains.\n
    Arguments
    ----------
    - GRID [`input grid`] : Grid
    - LUT [`input table`] : Look-up Table
    - NAME [`table field`] : Name
    - VALUE [`table field`] : Value
    - VALUE_MAX [`table field`] : Value (Range Maximum)
    - DESCRIPTION [`table field`] : Description
    - COLOR [`table field`] : Color

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_visualisation', '10', 'Select Look-up Table for Grid Visualization')
    if Tool.is_Okay():
        Tool.Set_Input ('GRID', GRID)
        Tool.Set_Input ('LUT', LUT)
        Tool.Set_Option('NAME', NAME)
        Tool.Set_Option('VALUE', VALUE)
        Tool.Set_Option('VALUE_MAX', VALUE_MAX)
        Tool.Set_Option('DESCRIPTION', DESCRIPTION)
        Tool.Set_Option('COLOR', COLOR)
        return Tool.Execute(Verbose)
    return False

def run_tool_grid_visualisation_10(GRID=None, LUT=None, NAME=None, VALUE=None, VALUE_MAX=None, DESCRIPTION=None, COLOR=None, Verbose=2):
    '''
    Select Look-up Table for Grid Visualization
    ----------
    [grid_visualisation.10]\n
    Select a look-up table for visual classification of a grid. Useful in combination with tool chains.\n
    Arguments
    ----------
    - GRID [`input grid`] : Grid
    - LUT [`input table`] : Look-up Table
    - NAME [`table field`] : Name
    - VALUE [`table field`] : Value
    - VALUE_MAX [`table field`] : Value (Range Maximum)
    - DESCRIPTION [`table field`] : Description
    - COLOR [`table field`] : Color

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_visualisation', '10', 'Select Look-up Table for Grid Visualization')
    if Tool.is_Okay():
        Tool.Set_Input ('GRID', GRID)
        Tool.Set_Input ('LUT', LUT)
        Tool.Set_Option('NAME', NAME)
        Tool.Set_Option('VALUE', VALUE)
        Tool.Set_Option('VALUE_MAX', VALUE_MAX)
        Tool.Set_Option('DESCRIPTION', DESCRIPTION)
        Tool.Set_Option('COLOR', COLOR)
        return Tool.Execute(Verbose)
    return False

def Create_a_Table_from_Lookup_Table(TABLE=None, LUT=None, Verbose=2):
    '''
    Create a Table from Look-up Table
    ----------
    [grid_visualisation.11]\n
    Creates a table object from a look-up table for visual data object classifications. Useful in combination with tool chains.\n
    Arguments
    ----------
    - TABLE [`output table`] : Table
    - LUT [`static table`] : Look-up Table. 5 Fields: - 1. [color] COLOR - 2. [string] NAME - 3. [string] DESCRIPTION - 4. [8 byte floating point number] MINIMUM - 5. [8 byte floating point number] MAXIMUM 

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_visualisation', '11', 'Create a Table from Look-up Table')
    if Tool.is_Okay():
        Tool.Set_Output('TABLE', TABLE)
        Tool.Set_Option('LUT', LUT)
        return Tool.Execute(Verbose)
    return False

def run_tool_grid_visualisation_11(TABLE=None, LUT=None, Verbose=2):
    '''
    Create a Table from Look-up Table
    ----------
    [grid_visualisation.11]\n
    Creates a table object from a look-up table for visual data object classifications. Useful in combination with tool chains.\n
    Arguments
    ----------
    - TABLE [`output table`] : Table
    - LUT [`static table`] : Look-up Table. 5 Fields: - 1. [color] COLOR - 2. [string] NAME - 3. [string] DESCRIPTION - 4. [8 byte floating point number] MINIMUM - 5. [8 byte floating point number] MAXIMUM 

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_visualisation', '11', 'Create a Table from Look-up Table')
    if Tool.is_Okay():
        Tool.Set_Output('TABLE', TABLE)
        Tool.Set_Option('LUT', LUT)
        return Tool.Execute(Verbose)
    return False

