#! /usr/bin/env python

'''
Python Interface to SAGA Tools Library
----------
- Category : Projection
- Name     : PROJ
- ID       : pj_proj4

Description
----------
Projection routines make use of the PROJ generic coordinate transformation software.
PROJ Version is 9.4.1
[PROJ Homepage](https://proj.org)
'''

from PySAGA.helper import Tool_Wrapper

def Set_Coordinate_Reference_System(GRIDS=None, SHAPES=None, GRIDS_OUT=None, SHAPES_OUT=None, CRS_STRING=None, CRS_FILE=None, Verbose=2):
    '''
    Set Coordinate Reference System
    ----------
    [pj_proj4.0]\n
    This tool allows you to define the Coordinate Reference System (CRS) for the supplied data sets. The tool applies no transformation to the data sets, it just updates their CRS metadata.\n
    A complete and correct description of the CRS of a dataset is necessary in order to be able to actually apply a projection with one of the 'Coordinate Transformation' tools.\n
    Arguments
    ----------
    - GRIDS [`optional input grid list`] : Grids
    - SHAPES [`optional input shapes list`] : Shapes
    - GRIDS_OUT [`output grid list`] : Grids
    - SHAPES_OUT [`output shapes list`] : Shapes
    - CRS_STRING [`text`] : Definition String. Default: +proj=longlat +datum=WGS84 +no_defs Supported formats comprise PROJ and WKT strings, object codes (e.g. "EPSG:4326").
    - CRS_FILE [`file path`] : Well Known Text File

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('pj_proj4', '0', 'Set Coordinate Reference System')
    if Tool.is_Okay():
        Tool.Set_Input ('GRIDS', GRIDS)
        Tool.Set_Input ('SHAPES', SHAPES)
        Tool.Set_Output('GRIDS_OUT', GRIDS_OUT)
        Tool.Set_Output('SHAPES_OUT', SHAPES_OUT)
        Tool.Set_Option('CRS_STRING', CRS_STRING)
        Tool.Set_Option('CRS_FILE', CRS_FILE)
        return Tool.Execute(Verbose)
    return False

def run_tool_pj_proj4_0(GRIDS=None, SHAPES=None, GRIDS_OUT=None, SHAPES_OUT=None, CRS_STRING=None, CRS_FILE=None, Verbose=2):
    '''
    Set Coordinate Reference System
    ----------
    [pj_proj4.0]\n
    This tool allows you to define the Coordinate Reference System (CRS) for the supplied data sets. The tool applies no transformation to the data sets, it just updates their CRS metadata.\n
    A complete and correct description of the CRS of a dataset is necessary in order to be able to actually apply a projection with one of the 'Coordinate Transformation' tools.\n
    Arguments
    ----------
    - GRIDS [`optional input grid list`] : Grids
    - SHAPES [`optional input shapes list`] : Shapes
    - GRIDS_OUT [`output grid list`] : Grids
    - SHAPES_OUT [`output shapes list`] : Shapes
    - CRS_STRING [`text`] : Definition String. Default: +proj=longlat +datum=WGS84 +no_defs Supported formats comprise PROJ and WKT strings, object codes (e.g. "EPSG:4326").
    - CRS_FILE [`file path`] : Well Known Text File

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('pj_proj4', '0', 'Set Coordinate Reference System')
    if Tool.is_Okay():
        Tool.Set_Input ('GRIDS', GRIDS)
        Tool.Set_Input ('SHAPES', SHAPES)
        Tool.Set_Output('GRIDS_OUT', GRIDS_OUT)
        Tool.Set_Output('SHAPES_OUT', SHAPES_OUT)
        Tool.Set_Option('CRS_STRING', CRS_STRING)
        Tool.Set_Option('CRS_FILE', CRS_FILE)
        return Tool.Execute(Verbose)
    return False

def Coordinate_Transformation_Shapes_List(SOURCE=None, TARGET=None, CRS_STRING=None, CRS_FILE=None, TRANSFORM_Z=None, PARALLEL=None, COPY=None, Verbose=2):
    '''
    Coordinate Transformation (Shapes List)
    ----------
    [pj_proj4.1]\n
    Coordinate transformation for shapes.\n
    Projection routines make use of the PROJ generic coordinate transformation software.\n
    PROJ Version is 9.4.1\n
    [PROJ Homepage](https://proj.org)\n
    Arguments
    ----------
    - SOURCE [`input shapes list`] : Source
    - TARGET [`output shapes list`] : Target
    - CRS_STRING [`text`] : Definition String. Default: +proj=longlat +datum=WGS84 +no_defs Supported formats comprise PROJ and WKT strings, object codes (e.g. "EPSG:4326").
    - CRS_FILE [`file path`] : Well Known Text File
    - TRANSFORM_Z [`boolean`] : Z Transformation. Default: 1 Transform elevation (z) values, if appropriate.
    - PARALLEL [`boolean`] : Parallel Processing. Default: 0
    - COPY [`boolean`] : Copy. Default: 1 If set the projected data will be created as a copy of the original, if not vertices will be projected in place thus reducing memory requirements.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('pj_proj4', '1', 'Coordinate Transformation (Shapes List)')
    if Tool.is_Okay():
        Tool.Set_Input ('SOURCE', SOURCE)
        Tool.Set_Output('TARGET', TARGET)
        Tool.Set_Option('CRS_STRING', CRS_STRING)
        Tool.Set_Option('CRS_FILE', CRS_FILE)
        Tool.Set_Option('TRANSFORM_Z', TRANSFORM_Z)
        Tool.Set_Option('PARALLEL', PARALLEL)
        Tool.Set_Option('COPY', COPY)
        return Tool.Execute(Verbose)
    return False

def run_tool_pj_proj4_1(SOURCE=None, TARGET=None, CRS_STRING=None, CRS_FILE=None, TRANSFORM_Z=None, PARALLEL=None, COPY=None, Verbose=2):
    '''
    Coordinate Transformation (Shapes List)
    ----------
    [pj_proj4.1]\n
    Coordinate transformation for shapes.\n
    Projection routines make use of the PROJ generic coordinate transformation software.\n
    PROJ Version is 9.4.1\n
    [PROJ Homepage](https://proj.org)\n
    Arguments
    ----------
    - SOURCE [`input shapes list`] : Source
    - TARGET [`output shapes list`] : Target
    - CRS_STRING [`text`] : Definition String. Default: +proj=longlat +datum=WGS84 +no_defs Supported formats comprise PROJ and WKT strings, object codes (e.g. "EPSG:4326").
    - CRS_FILE [`file path`] : Well Known Text File
    - TRANSFORM_Z [`boolean`] : Z Transformation. Default: 1 Transform elevation (z) values, if appropriate.
    - PARALLEL [`boolean`] : Parallel Processing. Default: 0
    - COPY [`boolean`] : Copy. Default: 1 If set the projected data will be created as a copy of the original, if not vertices will be projected in place thus reducing memory requirements.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('pj_proj4', '1', 'Coordinate Transformation (Shapes List)')
    if Tool.is_Okay():
        Tool.Set_Input ('SOURCE', SOURCE)
        Tool.Set_Output('TARGET', TARGET)
        Tool.Set_Option('CRS_STRING', CRS_STRING)
        Tool.Set_Option('CRS_FILE', CRS_FILE)
        Tool.Set_Option('TRANSFORM_Z', TRANSFORM_Z)
        Tool.Set_Option('PARALLEL', PARALLEL)
        Tool.Set_Option('COPY', COPY)
        return Tool.Execute(Verbose)
    return False

def Coordinate_Transformation_Shapes(SOURCE=None, TARGET=None, TARGET_PC=None, CRS_STRING=None, CRS_FILE=None, TRANSFORM_Z=None, PARALLEL=None, COPY=None, Verbose=2):
    '''
    Coordinate Transformation (Shapes)
    ----------
    [pj_proj4.2]\n
    Coordinate transformation for shapes.\n
    Projection routines make use of the PROJ generic coordinate transformation software.\n
    PROJ Version is 9.4.1\n
    [PROJ Homepage](https://proj.org)\n
    Arguments
    ----------
    - SOURCE [`input shapes`] : Source
    - TARGET [`output shapes`] : Target
    - TARGET_PC [`output point cloud`] : Target
    - CRS_STRING [`text`] : Definition String. Default: +proj=longlat +datum=WGS84 +no_defs Supported formats comprise PROJ and WKT strings, object codes (e.g. "EPSG:4326").
    - CRS_FILE [`file path`] : Well Known Text File
    - TRANSFORM_Z [`boolean`] : Z Transformation. Default: 1 Transform elevation (z) values, if appropriate.
    - PARALLEL [`boolean`] : Parallel Processing. Default: 0
    - COPY [`boolean`] : Copy. Default: 1 If set the projected data will be created as a copy of the original, if not vertices will be projected in place thus reducing memory requirements.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('pj_proj4', '2', 'Coordinate Transformation (Shapes)')
    if Tool.is_Okay():
        Tool.Set_Input ('SOURCE', SOURCE)
        Tool.Set_Output('TARGET', TARGET)
        Tool.Set_Output('TARGET_PC', TARGET_PC)
        Tool.Set_Option('CRS_STRING', CRS_STRING)
        Tool.Set_Option('CRS_FILE', CRS_FILE)
        Tool.Set_Option('TRANSFORM_Z', TRANSFORM_Z)
        Tool.Set_Option('PARALLEL', PARALLEL)
        Tool.Set_Option('COPY', COPY)
        return Tool.Execute(Verbose)
    return False

def run_tool_pj_proj4_2(SOURCE=None, TARGET=None, TARGET_PC=None, CRS_STRING=None, CRS_FILE=None, TRANSFORM_Z=None, PARALLEL=None, COPY=None, Verbose=2):
    '''
    Coordinate Transformation (Shapes)
    ----------
    [pj_proj4.2]\n
    Coordinate transformation for shapes.\n
    Projection routines make use of the PROJ generic coordinate transformation software.\n
    PROJ Version is 9.4.1\n
    [PROJ Homepage](https://proj.org)\n
    Arguments
    ----------
    - SOURCE [`input shapes`] : Source
    - TARGET [`output shapes`] : Target
    - TARGET_PC [`output point cloud`] : Target
    - CRS_STRING [`text`] : Definition String. Default: +proj=longlat +datum=WGS84 +no_defs Supported formats comprise PROJ and WKT strings, object codes (e.g. "EPSG:4326").
    - CRS_FILE [`file path`] : Well Known Text File
    - TRANSFORM_Z [`boolean`] : Z Transformation. Default: 1 Transform elevation (z) values, if appropriate.
    - PARALLEL [`boolean`] : Parallel Processing. Default: 0
    - COPY [`boolean`] : Copy. Default: 1 If set the projected data will be created as a copy of the original, if not vertices will be projected in place thus reducing memory requirements.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('pj_proj4', '2', 'Coordinate Transformation (Shapes)')
    if Tool.is_Okay():
        Tool.Set_Input ('SOURCE', SOURCE)
        Tool.Set_Output('TARGET', TARGET)
        Tool.Set_Output('TARGET_PC', TARGET_PC)
        Tool.Set_Option('CRS_STRING', CRS_STRING)
        Tool.Set_Option('CRS_FILE', CRS_FILE)
        Tool.Set_Option('TRANSFORM_Z', TRANSFORM_Z)
        Tool.Set_Option('PARALLEL', PARALLEL)
        Tool.Set_Option('COPY', COPY)
        return Tool.Execute(Verbose)
    return False

def Coordinate_Transformation_Grid_List(SOURCE=None, TARGET_TEMPLATE=None, GRIDS=None, OUT_X=None, OUT_Y=None, CRS_STRING=None, CRS_FILE=None, RESAMPLING=None, BYTEWISE=None, DATA_TYPE=None, TARGET_AREA=None, TARGET_DEFINITION=None, TARGET_USER_SIZE=None, TARGET_USER_XMIN=None, TARGET_USER_XMAX=None, TARGET_USER_YMIN=None, TARGET_USER_YMAX=None, TARGET_USER_COLS=None, TARGET_USER_ROWS=None, TARGET_USER_FLAT=None, TARGET_USER_FITS=None, TARGET_SYSTEM=None, Verbose=2):
    '''
    Coordinate Transformation (Grid List)
    ----------
    [pj_proj4.3]\n
    Coordinate transformation for grids.\n
    Projection routines make use of the PROJ generic coordinate transformation software.\n
    PROJ Version is 9.4.1\n
    [PROJ Homepage](https://proj.org)\n
    Arguments
    ----------
    - SOURCE [`input grid list`] : Source
    - TARGET_TEMPLATE [`optional input grid`] : Target System. use this grid's system for output grids
    - GRIDS [`output grid list`] : Target
    - OUT_X [`output grid`] : X Coordinates
    - OUT_Y [`output grid`] : Y Coordinates
    - CRS_STRING [`text`] : Definition String. Default: +proj=longlat +datum=WGS84 +no_defs Supported formats comprise PROJ and WKT strings, object codes (e.g. "EPSG:4326").
    - CRS_FILE [`file path`] : Well Known Text File
    - RESAMPLING [`choice`] : Resampling. Available Choices: [0] Nearest Neighbour [1] Bilinear Interpolation [2] Bicubic Spline Interpolation [3] B-Spline Interpolation Default: 3
    - BYTEWISE [`boolean`] : Bytewise Interpolation. Default: 0 To be used for RGB and CMYK coded values (i.e. images).
    - DATA_TYPE [`data type`] : Data Type. Available Choices: [0] unsigned 1 byte integer [1] signed 1 byte integer [2] unsigned 2 byte integer [3] signed 2 byte integer [4] unsigned 4 byte integer [5] signed 4 byte integer [6] unsigned 8 byte integer [7] signed 8 byte integer [8] 4 byte floating point number [9] 8 byte floating point number [10] Preserve Default: 10
    - TARGET_AREA [`boolean`] : Use Target Area Polygon. Default: 0 Restricts targeted grid cells to area of the projected bounding rectangle. Useful with certain projections for global data.
    - TARGET_DEFINITION [`choice`] : Target Grid System. Available Choices: [0] user defined [1] grid or grid system Default: 0
    - TARGET_USER_SIZE [`floating point number`] : Cellsize. Minimum: 0.000000 Default: 1.000000
    - TARGET_USER_XMIN [`floating point number`] : West. Default: 0.000000
    - TARGET_USER_XMAX [`floating point number`] : East. Default: 100.000000
    - TARGET_USER_YMIN [`floating point number`] : South. Default: 0.000000
    - TARGET_USER_YMAX [`floating point number`] : North. Default: 100.000000
    - TARGET_USER_COLS [`integer number`] : Columns. Minimum: 1 Default: 101 Number of cells in East-West direction.
    - TARGET_USER_ROWS [`integer number`] : Rows. Minimum: 1 Default: 101 Number of cells in North-South direction.
    - TARGET_USER_FLAT [`boolean`] : Rounding. Default: 1 Round bounding coordinates to multiples of cell size. Ignored if cell size has decimal places.
    - TARGET_USER_FITS [`choice`] : Fit. Available Choices: [0] nodes [1] cells Default: 0
    - TARGET_SYSTEM [`grid system`] : Grid System

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('pj_proj4', '3', 'Coordinate Transformation (Grid List)')
    if Tool.is_Okay():
        Tool.Set_Input ('SOURCE', SOURCE)
        Tool.Set_Input ('TARGET_TEMPLATE', TARGET_TEMPLATE)
        Tool.Set_Output('GRIDS', GRIDS)
        Tool.Set_Output('OUT_X', OUT_X)
        Tool.Set_Output('OUT_Y', OUT_Y)
        Tool.Set_Option('CRS_STRING', CRS_STRING)
        Tool.Set_Option('CRS_FILE', CRS_FILE)
        Tool.Set_Option('RESAMPLING', RESAMPLING)
        Tool.Set_Option('BYTEWISE', BYTEWISE)
        Tool.Set_Option('DATA_TYPE', DATA_TYPE)
        Tool.Set_Option('TARGET_AREA', TARGET_AREA)
        Tool.Set_Option('TARGET_DEFINITION', TARGET_DEFINITION)
        Tool.Set_Option('TARGET_USER_SIZE', TARGET_USER_SIZE)
        Tool.Set_Option('TARGET_USER_XMIN', TARGET_USER_XMIN)
        Tool.Set_Option('TARGET_USER_XMAX', TARGET_USER_XMAX)
        Tool.Set_Option('TARGET_USER_YMIN', TARGET_USER_YMIN)
        Tool.Set_Option('TARGET_USER_YMAX', TARGET_USER_YMAX)
        Tool.Set_Option('TARGET_USER_COLS', TARGET_USER_COLS)
        Tool.Set_Option('TARGET_USER_ROWS', TARGET_USER_ROWS)
        Tool.Set_Option('TARGET_USER_FLAT', TARGET_USER_FLAT)
        Tool.Set_Option('TARGET_USER_FITS', TARGET_USER_FITS)
        Tool.Set_Option('TARGET_SYSTEM', TARGET_SYSTEM)
        return Tool.Execute(Verbose)
    return False

def run_tool_pj_proj4_3(SOURCE=None, TARGET_TEMPLATE=None, GRIDS=None, OUT_X=None, OUT_Y=None, CRS_STRING=None, CRS_FILE=None, RESAMPLING=None, BYTEWISE=None, DATA_TYPE=None, TARGET_AREA=None, TARGET_DEFINITION=None, TARGET_USER_SIZE=None, TARGET_USER_XMIN=None, TARGET_USER_XMAX=None, TARGET_USER_YMIN=None, TARGET_USER_YMAX=None, TARGET_USER_COLS=None, TARGET_USER_ROWS=None, TARGET_USER_FLAT=None, TARGET_USER_FITS=None, TARGET_SYSTEM=None, Verbose=2):
    '''
    Coordinate Transformation (Grid List)
    ----------
    [pj_proj4.3]\n
    Coordinate transformation for grids.\n
    Projection routines make use of the PROJ generic coordinate transformation software.\n
    PROJ Version is 9.4.1\n
    [PROJ Homepage](https://proj.org)\n
    Arguments
    ----------
    - SOURCE [`input grid list`] : Source
    - TARGET_TEMPLATE [`optional input grid`] : Target System. use this grid's system for output grids
    - GRIDS [`output grid list`] : Target
    - OUT_X [`output grid`] : X Coordinates
    - OUT_Y [`output grid`] : Y Coordinates
    - CRS_STRING [`text`] : Definition String. Default: +proj=longlat +datum=WGS84 +no_defs Supported formats comprise PROJ and WKT strings, object codes (e.g. "EPSG:4326").
    - CRS_FILE [`file path`] : Well Known Text File
    - RESAMPLING [`choice`] : Resampling. Available Choices: [0] Nearest Neighbour [1] Bilinear Interpolation [2] Bicubic Spline Interpolation [3] B-Spline Interpolation Default: 3
    - BYTEWISE [`boolean`] : Bytewise Interpolation. Default: 0 To be used for RGB and CMYK coded values (i.e. images).
    - DATA_TYPE [`data type`] : Data Type. Available Choices: [0] unsigned 1 byte integer [1] signed 1 byte integer [2] unsigned 2 byte integer [3] signed 2 byte integer [4] unsigned 4 byte integer [5] signed 4 byte integer [6] unsigned 8 byte integer [7] signed 8 byte integer [8] 4 byte floating point number [9] 8 byte floating point number [10] Preserve Default: 10
    - TARGET_AREA [`boolean`] : Use Target Area Polygon. Default: 0 Restricts targeted grid cells to area of the projected bounding rectangle. Useful with certain projections for global data.
    - TARGET_DEFINITION [`choice`] : Target Grid System. Available Choices: [0] user defined [1] grid or grid system Default: 0
    - TARGET_USER_SIZE [`floating point number`] : Cellsize. Minimum: 0.000000 Default: 1.000000
    - TARGET_USER_XMIN [`floating point number`] : West. Default: 0.000000
    - TARGET_USER_XMAX [`floating point number`] : East. Default: 100.000000
    - TARGET_USER_YMIN [`floating point number`] : South. Default: 0.000000
    - TARGET_USER_YMAX [`floating point number`] : North. Default: 100.000000
    - TARGET_USER_COLS [`integer number`] : Columns. Minimum: 1 Default: 101 Number of cells in East-West direction.
    - TARGET_USER_ROWS [`integer number`] : Rows. Minimum: 1 Default: 101 Number of cells in North-South direction.
    - TARGET_USER_FLAT [`boolean`] : Rounding. Default: 1 Round bounding coordinates to multiples of cell size. Ignored if cell size has decimal places.
    - TARGET_USER_FITS [`choice`] : Fit. Available Choices: [0] nodes [1] cells Default: 0
    - TARGET_SYSTEM [`grid system`] : Grid System

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('pj_proj4', '3', 'Coordinate Transformation (Grid List)')
    if Tool.is_Okay():
        Tool.Set_Input ('SOURCE', SOURCE)
        Tool.Set_Input ('TARGET_TEMPLATE', TARGET_TEMPLATE)
        Tool.Set_Output('GRIDS', GRIDS)
        Tool.Set_Output('OUT_X', OUT_X)
        Tool.Set_Output('OUT_Y', OUT_Y)
        Tool.Set_Option('CRS_STRING', CRS_STRING)
        Tool.Set_Option('CRS_FILE', CRS_FILE)
        Tool.Set_Option('RESAMPLING', RESAMPLING)
        Tool.Set_Option('BYTEWISE', BYTEWISE)
        Tool.Set_Option('DATA_TYPE', DATA_TYPE)
        Tool.Set_Option('TARGET_AREA', TARGET_AREA)
        Tool.Set_Option('TARGET_DEFINITION', TARGET_DEFINITION)
        Tool.Set_Option('TARGET_USER_SIZE', TARGET_USER_SIZE)
        Tool.Set_Option('TARGET_USER_XMIN', TARGET_USER_XMIN)
        Tool.Set_Option('TARGET_USER_XMAX', TARGET_USER_XMAX)
        Tool.Set_Option('TARGET_USER_YMIN', TARGET_USER_YMIN)
        Tool.Set_Option('TARGET_USER_YMAX', TARGET_USER_YMAX)
        Tool.Set_Option('TARGET_USER_COLS', TARGET_USER_COLS)
        Tool.Set_Option('TARGET_USER_ROWS', TARGET_USER_ROWS)
        Tool.Set_Option('TARGET_USER_FLAT', TARGET_USER_FLAT)
        Tool.Set_Option('TARGET_USER_FITS', TARGET_USER_FITS)
        Tool.Set_Option('TARGET_SYSTEM', TARGET_SYSTEM)
        return Tool.Execute(Verbose)
    return False

def Coordinate_Transformation_Grid(SOURCE=None, TARGET_TEMPLATE=None, GRID=None, OUT_X=None, OUT_Y=None, CRS_STRING=None, CRS_FILE=None, SOURCE_GRIDSYSTEM=None, RESAMPLING=None, BYTEWISE=None, DATA_TYPE=None, TARGET_AREA=None, TARGET_DEFINITION=None, TARGET_USER_SIZE=None, TARGET_USER_XMIN=None, TARGET_USER_XMAX=None, TARGET_USER_YMIN=None, TARGET_USER_YMAX=None, TARGET_USER_COLS=None, TARGET_USER_ROWS=None, TARGET_USER_FLAT=None, TARGET_USER_FITS=None, TARGET_SYSTEM=None, Verbose=2):
    '''
    Coordinate Transformation (Grid)
    ----------
    [pj_proj4.4]\n
    Coordinate transformation for grids.\n
    Projection routines make use of the PROJ generic coordinate transformation software.\n
    PROJ Version is 9.4.1\n
    [PROJ Homepage](https://proj.org)\n
    Arguments
    ----------
    - SOURCE [`input grid`] : Source
    - TARGET_TEMPLATE [`optional input grid`] : Target System. use this grid's system for output grids
    - GRID [`output grid`] : Target
    - OUT_X [`output grid`] : X Coordinates
    - OUT_Y [`output grid`] : Y Coordinates
    - CRS_STRING [`text`] : Definition String. Default: +proj=longlat +datum=WGS84 +no_defs Supported formats comprise PROJ and WKT strings, object codes (e.g. "EPSG:4326").
    - CRS_FILE [`file path`] : Well Known Text File
    - SOURCE_GRIDSYSTEM [`grid system`] : Grid system
    - RESAMPLING [`choice`] : Resampling. Available Choices: [0] Nearest Neighbour [1] Bilinear Interpolation [2] Bicubic Spline Interpolation [3] B-Spline Interpolation Default: 3
    - BYTEWISE [`boolean`] : Bytewise Interpolation. Default: 0 To be used for RGB and CMYK coded values (i.e. images).
    - DATA_TYPE [`data type`] : Data Type. Available Choices: [0] unsigned 1 byte integer [1] signed 1 byte integer [2] unsigned 2 byte integer [3] signed 2 byte integer [4] unsigned 4 byte integer [5] signed 4 byte integer [6] unsigned 8 byte integer [7] signed 8 byte integer [8] 4 byte floating point number [9] 8 byte floating point number [10] Preserve Default: 10
    - TARGET_AREA [`boolean`] : Use Target Area Polygon. Default: 0 Restricts targeted grid cells to area of the projected bounding rectangle. Useful with certain projections for global data.
    - TARGET_DEFINITION [`choice`] : Target Grid System. Available Choices: [0] user defined [1] grid or grid system Default: 0
    - TARGET_USER_SIZE [`floating point number`] : Cellsize. Minimum: 0.000000 Default: 1.000000
    - TARGET_USER_XMIN [`floating point number`] : West. Default: 0.000000
    - TARGET_USER_XMAX [`floating point number`] : East. Default: 100.000000
    - TARGET_USER_YMIN [`floating point number`] : South. Default: 0.000000
    - TARGET_USER_YMAX [`floating point number`] : North. Default: 100.000000
    - TARGET_USER_COLS [`integer number`] : Columns. Minimum: 1 Default: 101 Number of cells in East-West direction.
    - TARGET_USER_ROWS [`integer number`] : Rows. Minimum: 1 Default: 101 Number of cells in North-South direction.
    - TARGET_USER_FLAT [`boolean`] : Rounding. Default: 1 Round bounding coordinates to multiples of cell size. Ignored if cell size has decimal places.
    - TARGET_USER_FITS [`choice`] : Fit. Available Choices: [0] nodes [1] cells Default: 0
    - TARGET_SYSTEM [`grid system`] : Grid System

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('pj_proj4', '4', 'Coordinate Transformation (Grid)')
    if Tool.is_Okay():
        Tool.Set_Input ('SOURCE', SOURCE)
        Tool.Set_Input ('TARGET_TEMPLATE', TARGET_TEMPLATE)
        Tool.Set_Output('GRID', GRID)
        Tool.Set_Output('OUT_X', OUT_X)
        Tool.Set_Output('OUT_Y', OUT_Y)
        Tool.Set_Option('CRS_STRING', CRS_STRING)
        Tool.Set_Option('CRS_FILE', CRS_FILE)
        Tool.Set_Option('SOURCE_GRIDSYSTEM', SOURCE_GRIDSYSTEM)
        Tool.Set_Option('RESAMPLING', RESAMPLING)
        Tool.Set_Option('BYTEWISE', BYTEWISE)
        Tool.Set_Option('DATA_TYPE', DATA_TYPE)
        Tool.Set_Option('TARGET_AREA', TARGET_AREA)
        Tool.Set_Option('TARGET_DEFINITION', TARGET_DEFINITION)
        Tool.Set_Option('TARGET_USER_SIZE', TARGET_USER_SIZE)
        Tool.Set_Option('TARGET_USER_XMIN', TARGET_USER_XMIN)
        Tool.Set_Option('TARGET_USER_XMAX', TARGET_USER_XMAX)
        Tool.Set_Option('TARGET_USER_YMIN', TARGET_USER_YMIN)
        Tool.Set_Option('TARGET_USER_YMAX', TARGET_USER_YMAX)
        Tool.Set_Option('TARGET_USER_COLS', TARGET_USER_COLS)
        Tool.Set_Option('TARGET_USER_ROWS', TARGET_USER_ROWS)
        Tool.Set_Option('TARGET_USER_FLAT', TARGET_USER_FLAT)
        Tool.Set_Option('TARGET_USER_FITS', TARGET_USER_FITS)
        Tool.Set_Option('TARGET_SYSTEM', TARGET_SYSTEM)
        return Tool.Execute(Verbose)
    return False

def run_tool_pj_proj4_4(SOURCE=None, TARGET_TEMPLATE=None, GRID=None, OUT_X=None, OUT_Y=None, CRS_STRING=None, CRS_FILE=None, SOURCE_GRIDSYSTEM=None, RESAMPLING=None, BYTEWISE=None, DATA_TYPE=None, TARGET_AREA=None, TARGET_DEFINITION=None, TARGET_USER_SIZE=None, TARGET_USER_XMIN=None, TARGET_USER_XMAX=None, TARGET_USER_YMIN=None, TARGET_USER_YMAX=None, TARGET_USER_COLS=None, TARGET_USER_ROWS=None, TARGET_USER_FLAT=None, TARGET_USER_FITS=None, TARGET_SYSTEM=None, Verbose=2):
    '''
    Coordinate Transformation (Grid)
    ----------
    [pj_proj4.4]\n
    Coordinate transformation for grids.\n
    Projection routines make use of the PROJ generic coordinate transformation software.\n
    PROJ Version is 9.4.1\n
    [PROJ Homepage](https://proj.org)\n
    Arguments
    ----------
    - SOURCE [`input grid`] : Source
    - TARGET_TEMPLATE [`optional input grid`] : Target System. use this grid's system for output grids
    - GRID [`output grid`] : Target
    - OUT_X [`output grid`] : X Coordinates
    - OUT_Y [`output grid`] : Y Coordinates
    - CRS_STRING [`text`] : Definition String. Default: +proj=longlat +datum=WGS84 +no_defs Supported formats comprise PROJ and WKT strings, object codes (e.g. "EPSG:4326").
    - CRS_FILE [`file path`] : Well Known Text File
    - SOURCE_GRIDSYSTEM [`grid system`] : Grid system
    - RESAMPLING [`choice`] : Resampling. Available Choices: [0] Nearest Neighbour [1] Bilinear Interpolation [2] Bicubic Spline Interpolation [3] B-Spline Interpolation Default: 3
    - BYTEWISE [`boolean`] : Bytewise Interpolation. Default: 0 To be used for RGB and CMYK coded values (i.e. images).
    - DATA_TYPE [`data type`] : Data Type. Available Choices: [0] unsigned 1 byte integer [1] signed 1 byte integer [2] unsigned 2 byte integer [3] signed 2 byte integer [4] unsigned 4 byte integer [5] signed 4 byte integer [6] unsigned 8 byte integer [7] signed 8 byte integer [8] 4 byte floating point number [9] 8 byte floating point number [10] Preserve Default: 10
    - TARGET_AREA [`boolean`] : Use Target Area Polygon. Default: 0 Restricts targeted grid cells to area of the projected bounding rectangle. Useful with certain projections for global data.
    - TARGET_DEFINITION [`choice`] : Target Grid System. Available Choices: [0] user defined [1] grid or grid system Default: 0
    - TARGET_USER_SIZE [`floating point number`] : Cellsize. Minimum: 0.000000 Default: 1.000000
    - TARGET_USER_XMIN [`floating point number`] : West. Default: 0.000000
    - TARGET_USER_XMAX [`floating point number`] : East. Default: 100.000000
    - TARGET_USER_YMIN [`floating point number`] : South. Default: 0.000000
    - TARGET_USER_YMAX [`floating point number`] : North. Default: 100.000000
    - TARGET_USER_COLS [`integer number`] : Columns. Minimum: 1 Default: 101 Number of cells in East-West direction.
    - TARGET_USER_ROWS [`integer number`] : Rows. Minimum: 1 Default: 101 Number of cells in North-South direction.
    - TARGET_USER_FLAT [`boolean`] : Rounding. Default: 1 Round bounding coordinates to multiples of cell size. Ignored if cell size has decimal places.
    - TARGET_USER_FITS [`choice`] : Fit. Available Choices: [0] nodes [1] cells Default: 0
    - TARGET_SYSTEM [`grid system`] : Grid System

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('pj_proj4', '4', 'Coordinate Transformation (Grid)')
    if Tool.is_Okay():
        Tool.Set_Input ('SOURCE', SOURCE)
        Tool.Set_Input ('TARGET_TEMPLATE', TARGET_TEMPLATE)
        Tool.Set_Output('GRID', GRID)
        Tool.Set_Output('OUT_X', OUT_X)
        Tool.Set_Output('OUT_Y', OUT_Y)
        Tool.Set_Option('CRS_STRING', CRS_STRING)
        Tool.Set_Option('CRS_FILE', CRS_FILE)
        Tool.Set_Option('SOURCE_GRIDSYSTEM', SOURCE_GRIDSYSTEM)
        Tool.Set_Option('RESAMPLING', RESAMPLING)
        Tool.Set_Option('BYTEWISE', BYTEWISE)
        Tool.Set_Option('DATA_TYPE', DATA_TYPE)
        Tool.Set_Option('TARGET_AREA', TARGET_AREA)
        Tool.Set_Option('TARGET_DEFINITION', TARGET_DEFINITION)
        Tool.Set_Option('TARGET_USER_SIZE', TARGET_USER_SIZE)
        Tool.Set_Option('TARGET_USER_XMIN', TARGET_USER_XMIN)
        Tool.Set_Option('TARGET_USER_XMAX', TARGET_USER_XMAX)
        Tool.Set_Option('TARGET_USER_YMIN', TARGET_USER_YMIN)
        Tool.Set_Option('TARGET_USER_YMAX', TARGET_USER_YMAX)
        Tool.Set_Option('TARGET_USER_COLS', TARGET_USER_COLS)
        Tool.Set_Option('TARGET_USER_ROWS', TARGET_USER_ROWS)
        Tool.Set_Option('TARGET_USER_FLAT', TARGET_USER_FLAT)
        Tool.Set_Option('TARGET_USER_FITS', TARGET_USER_FITS)
        Tool.Set_Option('TARGET_SYSTEM', TARGET_SYSTEM)
        return Tool.Execute(Verbose)
    return False

def Change_Longitudinal_Range_for_Grids(INPUT=None, OUTPUT=None, DIRECTION=None, PATCH=None, Verbose=2):
    '''
    Change Longitudinal Range for Grids
    ----------
    [pj_proj4.13]\n
    Change the longitudinal range of grids using geographic coordinates, i.e. from 0 - 360 to -180 - 180 and vice versa.\n
    Arguments
    ----------
    - INPUT [`input grid list`] : Input
    - OUTPUT [`output grid list`] : Output
    - DIRECTION [`choice`] : Direction. Available Choices: [0] 0 - 360 >> -180 - 180 [1] -180 - 180 >> 0 - 360 Default: 0
    - PATCH [`boolean`] : Patch Last Column. Default: 1

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('pj_proj4', '13', 'Change Longitudinal Range for Grids')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Output('OUTPUT', OUTPUT)
        Tool.Set_Option('DIRECTION', DIRECTION)
        Tool.Set_Option('PATCH', PATCH)
        return Tool.Execute(Verbose)
    return False

def run_tool_pj_proj4_13(INPUT=None, OUTPUT=None, DIRECTION=None, PATCH=None, Verbose=2):
    '''
    Change Longitudinal Range for Grids
    ----------
    [pj_proj4.13]\n
    Change the longitudinal range of grids using geographic coordinates, i.e. from 0 - 360 to -180 - 180 and vice versa.\n
    Arguments
    ----------
    - INPUT [`input grid list`] : Input
    - OUTPUT [`output grid list`] : Output
    - DIRECTION [`choice`] : Direction. Available Choices: [0] 0 - 360 >> -180 - 180 [1] -180 - 180 >> 0 - 360 Default: 0
    - PATCH [`boolean`] : Patch Last Column. Default: 1

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('pj_proj4', '13', 'Change Longitudinal Range for Grids')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Output('OUTPUT', OUTPUT)
        Tool.Set_Option('DIRECTION', DIRECTION)
        Tool.Set_Option('PATCH', PATCH)
        return Tool.Execute(Verbose)
    return False

def LatitudeLongitude_Graticule(GRATICULE=None, COORDS=None, CRS_STRING=None, CRS_FILE=None, XMIN=None, XMAX=None, YMIN=None, YMAX=None, INTERVAL=None, FIXED=None, FITTED=None, RESOLUTION=None, Verbose=2):
    '''
    Latitude/Longitude Graticule
    ----------
    [pj_proj4.14]\n
    Creates a longitude/latitude graticule for the extent and projection of the input shapes layer.\n
    Projection routines make use of the PROJ generic coordinate transformation software.\n
    PROJ Version is 9.4.1\n
    [PROJ Homepage](https://proj.org)\n
    Arguments
    ----------
    - GRATICULE [`output shapes`] : Graticule
    - COORDS [`output shapes`] : Frame Coordinates
    - CRS_STRING [`text`] : Definition String. Default: +proj=longlat +datum=WGS84 +no_defs Supported formats comprise PROJ and WKT strings, object codes (e.g. "EPSG:4326").
    - CRS_FILE [`file path`] : Well Known Text File
    - XMIN [`floating point number`] : Minimum. Default: 0.000000
    - XMAX [`floating point number`] : Maximum. Default: 0.000000
    - YMIN [`floating point number`] : Minimum. Default: 0.000000
    - YMAX [`floating point number`] : Maximum. Default: 0.000000
    - INTERVAL [`choice`] : Interval. Available Choices: [0] fixed interval [1] fitted interval Default: 0
    - FIXED [`floating point number`] : Fixed Interval (Degree). Minimum: 0.000000 Default: 1.000000
    - FITTED [`integer number`] : Number of Intervals. Minimum: 1 Default: 10
    - RESOLUTION [`floating point number`] : Minimum Resolution (Degree). Minimum: 0.000000 Default: 0.500000

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('pj_proj4', '14', 'Latitude/Longitude Graticule')
    if Tool.is_Okay():
        Tool.Set_Output('GRATICULE', GRATICULE)
        Tool.Set_Output('COORDS', COORDS)
        Tool.Set_Option('CRS_STRING', CRS_STRING)
        Tool.Set_Option('CRS_FILE', CRS_FILE)
        Tool.Set_Option('XMIN', XMIN)
        Tool.Set_Option('XMAX', XMAX)
        Tool.Set_Option('YMIN', YMIN)
        Tool.Set_Option('YMAX', YMAX)
        Tool.Set_Option('INTERVAL', INTERVAL)
        Tool.Set_Option('FIXED', FIXED)
        Tool.Set_Option('FITTED', FITTED)
        Tool.Set_Option('RESOLUTION', RESOLUTION)
        return Tool.Execute(Verbose)
    return False

def run_tool_pj_proj4_14(GRATICULE=None, COORDS=None, CRS_STRING=None, CRS_FILE=None, XMIN=None, XMAX=None, YMIN=None, YMAX=None, INTERVAL=None, FIXED=None, FITTED=None, RESOLUTION=None, Verbose=2):
    '''
    Latitude/Longitude Graticule
    ----------
    [pj_proj4.14]\n
    Creates a longitude/latitude graticule for the extent and projection of the input shapes layer.\n
    Projection routines make use of the PROJ generic coordinate transformation software.\n
    PROJ Version is 9.4.1\n
    [PROJ Homepage](https://proj.org)\n
    Arguments
    ----------
    - GRATICULE [`output shapes`] : Graticule
    - COORDS [`output shapes`] : Frame Coordinates
    - CRS_STRING [`text`] : Definition String. Default: +proj=longlat +datum=WGS84 +no_defs Supported formats comprise PROJ and WKT strings, object codes (e.g. "EPSG:4326").
    - CRS_FILE [`file path`] : Well Known Text File
    - XMIN [`floating point number`] : Minimum. Default: 0.000000
    - XMAX [`floating point number`] : Maximum. Default: 0.000000
    - YMIN [`floating point number`] : Minimum. Default: 0.000000
    - YMAX [`floating point number`] : Maximum. Default: 0.000000
    - INTERVAL [`choice`] : Interval. Available Choices: [0] fixed interval [1] fitted interval Default: 0
    - FIXED [`floating point number`] : Fixed Interval (Degree). Minimum: 0.000000 Default: 1.000000
    - FITTED [`integer number`] : Number of Intervals. Minimum: 1 Default: 10
    - RESOLUTION [`floating point number`] : Minimum Resolution (Degree). Minimum: 0.000000 Default: 0.500000

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('pj_proj4', '14', 'Latitude/Longitude Graticule')
    if Tool.is_Okay():
        Tool.Set_Output('GRATICULE', GRATICULE)
        Tool.Set_Output('COORDS', COORDS)
        Tool.Set_Option('CRS_STRING', CRS_STRING)
        Tool.Set_Option('CRS_FILE', CRS_FILE)
        Tool.Set_Option('XMIN', XMIN)
        Tool.Set_Option('XMAX', XMAX)
        Tool.Set_Option('YMIN', YMIN)
        Tool.Set_Option('YMAX', YMAX)
        Tool.Set_Option('INTERVAL', INTERVAL)
        Tool.Set_Option('FIXED', FIXED)
        Tool.Set_Option('FITTED', FITTED)
        Tool.Set_Option('RESOLUTION', RESOLUTION)
        return Tool.Execute(Verbose)
    return False

def Coordinate_Reference_System_Picker(CRS_STRING=None, CRS_FILE=None, Verbose=2):
    '''
    Coordinate Reference System Picker
    ----------
    [pj_proj4.15]\n
    Define or pick a Coordinate Reference System (CRS). It is intended to call this tool only from other tools.\n
    Arguments
    ----------
    - CRS_STRING [`text`] : Definition String. Default: +proj=longlat +datum=WGS84 +no_defs Supported formats comprise PROJ and WKT strings, object codes (e.g. "EPSG:4326").
    - CRS_FILE [`file path`] : Well Known Text File

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('pj_proj4', '15', 'Coordinate Reference System Picker')
    if Tool.is_Okay():
        Tool.Set_Option('CRS_STRING', CRS_STRING)
        Tool.Set_Option('CRS_FILE', CRS_FILE)
        return Tool.Execute(Verbose)
    return False

def run_tool_pj_proj4_15(CRS_STRING=None, CRS_FILE=None, Verbose=2):
    '''
    Coordinate Reference System Picker
    ----------
    [pj_proj4.15]\n
    Define or pick a Coordinate Reference System (CRS). It is intended to call this tool only from other tools.\n
    Arguments
    ----------
    - CRS_STRING [`text`] : Definition String. Default: +proj=longlat +datum=WGS84 +no_defs Supported formats comprise PROJ and WKT strings, object codes (e.g. "EPSG:4326").
    - CRS_FILE [`file path`] : Well Known Text File

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('pj_proj4', '15', 'Coordinate Reference System Picker')
    if Tool.is_Okay():
        Tool.Set_Option('CRS_STRING', CRS_STRING)
        Tool.Set_Option('CRS_FILE', CRS_FILE)
        return Tool.Execute(Verbose)
    return False

def Tissots_Indicatrix(TARGET=None, CRS_STRING=None, CRS_FILE=None, NY=None, NX=None, SCALE=None, Verbose=2):
    '''
    Tissot's Indicatrix
    ----------
    [pj_proj4.16]\n
    Creates a shapes layer with Tissot's indicatrices for chosen projection.\n
    Projection routines make use of the PROJ generic coordinate transformation software.\n
    PROJ Version is 9.4.1\n
    [PROJ Homepage](https://proj.org)\n
    Arguments
    ----------
    - TARGET [`output shapes`] : Indicatrix
    - CRS_STRING [`text`] : Definition String. Default: +proj=longlat +datum=WGS84 +no_defs Supported formats comprise PROJ and WKT strings, object codes (e.g. "EPSG:4326").
    - CRS_FILE [`file path`] : Well Known Text File
    - NY [`integer number`] : Number in Latitudinal Direction. Minimum: 1 Default: 5
    - NX [`integer number`] : Number in Meridional Direction. Minimum: 1 Default: 11
    - SCALE [`floating point number`] : Size. Minimum: 1.000000 Default: 25.000000

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('pj_proj4', '16', 'Tissot\'s Indicatrix')
    if Tool.is_Okay():
        Tool.Set_Output('TARGET', TARGET)
        Tool.Set_Option('CRS_STRING', CRS_STRING)
        Tool.Set_Option('CRS_FILE', CRS_FILE)
        Tool.Set_Option('NY', NY)
        Tool.Set_Option('NX', NX)
        Tool.Set_Option('SCALE', SCALE)
        return Tool.Execute(Verbose)
    return False

def run_tool_pj_proj4_16(TARGET=None, CRS_STRING=None, CRS_FILE=None, NY=None, NX=None, SCALE=None, Verbose=2):
    '''
    Tissot's Indicatrix
    ----------
    [pj_proj4.16]\n
    Creates a shapes layer with Tissot's indicatrices for chosen projection.\n
    Projection routines make use of the PROJ generic coordinate transformation software.\n
    PROJ Version is 9.4.1\n
    [PROJ Homepage](https://proj.org)\n
    Arguments
    ----------
    - TARGET [`output shapes`] : Indicatrix
    - CRS_STRING [`text`] : Definition String. Default: +proj=longlat +datum=WGS84 +no_defs Supported formats comprise PROJ and WKT strings, object codes (e.g. "EPSG:4326").
    - CRS_FILE [`file path`] : Well Known Text File
    - NY [`integer number`] : Number in Latitudinal Direction. Minimum: 1 Default: 5
    - NX [`integer number`] : Number in Meridional Direction. Minimum: 1 Default: 11
    - SCALE [`floating point number`] : Size. Minimum: 1.000000 Default: 25.000000

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('pj_proj4', '16', 'Tissot\'s Indicatrix')
    if Tool.is_Okay():
        Tool.Set_Output('TARGET', TARGET)
        Tool.Set_Option('CRS_STRING', CRS_STRING)
        Tool.Set_Option('CRS_FILE', CRS_FILE)
        Tool.Set_Option('NY', NY)
        Tool.Set_Option('NX', NX)
        Tool.Set_Option('SCALE', SCALE)
        return Tool.Execute(Verbose)
    return False

def Geographic_Coordinate_Grids(GRID=None, LON=None, LAT=None, Verbose=2):
    '''
    Geographic Coordinate Grids
    ----------
    [pj_proj4.17]\n
    Creates for a given grid geographic coordinate information, i.e. two grids specifying the longitude and latitude for each cell. The coordinate system of the input grid has to be defined.\n
    Projection routines make use of the PROJ generic coordinate transformation software.\n
    PROJ Version is 9.4.1\n
    [PROJ Homepage](https://proj.org)\n
    Arguments
    ----------
    - GRID [`input grid`] : Grid
    - LON [`output grid`] : Longitude
    - LAT [`output grid`] : Latitude

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('pj_proj4', '17', 'Geographic Coordinate Grids')
    if Tool.is_Okay():
        Tool.Set_Input ('GRID', GRID)
        Tool.Set_Output('LON', LON)
        Tool.Set_Output('LAT', LAT)
        return Tool.Execute(Verbose)
    return False

def run_tool_pj_proj4_17(GRID=None, LON=None, LAT=None, Verbose=2):
    '''
    Geographic Coordinate Grids
    ----------
    [pj_proj4.17]\n
    Creates for a given grid geographic coordinate information, i.e. two grids specifying the longitude and latitude for each cell. The coordinate system of the input grid has to be defined.\n
    Projection routines make use of the PROJ generic coordinate transformation software.\n
    PROJ Version is 9.4.1\n
    [PROJ Homepage](https://proj.org)\n
    Arguments
    ----------
    - GRID [`input grid`] : Grid
    - LON [`output grid`] : Longitude
    - LAT [`output grid`] : Latitude

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('pj_proj4', '17', 'Geographic Coordinate Grids')
    if Tool.is_Okay():
        Tool.Set_Input ('GRID', GRID)
        Tool.Set_Output('LON', LON)
        Tool.Set_Output('LAT', LAT)
        return Tool.Execute(Verbose)
    return False

def Rotated_to_Regular_Grid(SOURCE=None, TARGET_TEMPLATE=None, TARGET=None, EXTENT=None, ROT_POLE_LON=None, ROT_POLE_LAT=None, TARGET_DEFINITION=None, TARGET_USER_SIZE=None, TARGET_USER_XMIN=None, TARGET_USER_XMAX=None, TARGET_USER_YMIN=None, TARGET_USER_YMAX=None, TARGET_USER_COLS=None, TARGET_USER_ROWS=None, TARGET_USER_FLAT=None, TARGET_USER_FITS=None, TARGET_SYSTEM=None, Verbose=2):
    '''
    Rotated to Regular Grid
    ----------
    [pj_proj4.18]\n
    This tool projects grids using rotated-pole coordinates to regular geographic grids.\n
    Rotated-pole coordinates are used by the CORDEX project.\n
    ____________\n
    CORDEX Domains\n
    ============\n
    CORDEX Area	Name	Resolution	N-Pole/Lon.	N-Pole/Lat.\n
    South America   	SAM-44	0.44	  -56.06	 70.60\n
    Central America 	CAM-44	0.44	  113.98	 75.74\n
    North America   	NAM-44	0.44	   83.00	 42.50\n
    Europe          	EUR-44	0.44	 -162.00	 39.25\n
    Africa          	AFR-44	0.44	     N/A	 90.00\n
    South Asia      	WAS-44	0.44	 -123.34	 79.95\n
    East Asia       	EAS-44	0.44	  -64.78	 77.61\n
    Central Asia    	CAS-44	0.44	 -103.39	 43.48\n
    Australasia     	AUS-44	0.44	  141.38	 60.31\n
    Antarctica      	ANT-44	0.44	 -166.92	  6.08\n
    Arctic          	ARC-44	0.44	    0.00	  6.55\n
    Mediterranean   	MED-44	0.44	  198.00	 39.25\n
    M-East, N-Africa	MNA-44	0.44	     N/A	 90.00\n
    M-East, N-Africa	MNA-22	0.22	     N/A	 90.00\n
    Europe          	EUR-11	0.11	 -162.00	 39.25\n
    South East Asia 	SEA-22	0.22	     N/A	 90.00\n
    ============\n
    Arguments
    ----------
    - SOURCE [`input grid list`] : Grids
    - TARGET_TEMPLATE [`optional input grid`] : Target System. use this grid's system for output grids
    - TARGET [`output grid list`] : Grids
    - EXTENT [`output shapes`] : Extent
    - ROT_POLE_LON [`floating point number`] : Longitude. Default: -162.000000
    - ROT_POLE_LAT [`floating point number`] : Latitude. Default: 39.250000
    - TARGET_DEFINITION [`choice`] : Target Grid System. Available Choices: [0] user defined [1] grid or grid system Default: 0
    - TARGET_USER_SIZE [`floating point number`] : Cellsize. Minimum: 0.000000 Default: 1.000000
    - TARGET_USER_XMIN [`floating point number`] : West. Default: 0.000000
    - TARGET_USER_XMAX [`floating point number`] : East. Default: 100.000000
    - TARGET_USER_YMIN [`floating point number`] : South. Default: 0.000000
    - TARGET_USER_YMAX [`floating point number`] : North. Default: 100.000000
    - TARGET_USER_COLS [`integer number`] : Columns. Minimum: 1 Default: 101 Number of cells in East-West direction.
    - TARGET_USER_ROWS [`integer number`] : Rows. Minimum: 1 Default: 101 Number of cells in North-South direction.
    - TARGET_USER_FLAT [`boolean`] : Rounding. Default: 1 Round bounding coordinates to multiples of cell size. Ignored if cell size has decimal places.
    - TARGET_USER_FITS [`choice`] : Fit. Available Choices: [0] nodes [1] cells Default: 0
    - TARGET_SYSTEM [`grid system`] : Grid System

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('pj_proj4', '18', 'Rotated to Regular Grid')
    if Tool.is_Okay():
        Tool.Set_Input ('SOURCE', SOURCE)
        Tool.Set_Input ('TARGET_TEMPLATE', TARGET_TEMPLATE)
        Tool.Set_Output('TARGET', TARGET)
        Tool.Set_Output('EXTENT', EXTENT)
        Tool.Set_Option('ROT_POLE_LON', ROT_POLE_LON)
        Tool.Set_Option('ROT_POLE_LAT', ROT_POLE_LAT)
        Tool.Set_Option('TARGET_DEFINITION', TARGET_DEFINITION)
        Tool.Set_Option('TARGET_USER_SIZE', TARGET_USER_SIZE)
        Tool.Set_Option('TARGET_USER_XMIN', TARGET_USER_XMIN)
        Tool.Set_Option('TARGET_USER_XMAX', TARGET_USER_XMAX)
        Tool.Set_Option('TARGET_USER_YMIN', TARGET_USER_YMIN)
        Tool.Set_Option('TARGET_USER_YMAX', TARGET_USER_YMAX)
        Tool.Set_Option('TARGET_USER_COLS', TARGET_USER_COLS)
        Tool.Set_Option('TARGET_USER_ROWS', TARGET_USER_ROWS)
        Tool.Set_Option('TARGET_USER_FLAT', TARGET_USER_FLAT)
        Tool.Set_Option('TARGET_USER_FITS', TARGET_USER_FITS)
        Tool.Set_Option('TARGET_SYSTEM', TARGET_SYSTEM)
        return Tool.Execute(Verbose)
    return False

def run_tool_pj_proj4_18(SOURCE=None, TARGET_TEMPLATE=None, TARGET=None, EXTENT=None, ROT_POLE_LON=None, ROT_POLE_LAT=None, TARGET_DEFINITION=None, TARGET_USER_SIZE=None, TARGET_USER_XMIN=None, TARGET_USER_XMAX=None, TARGET_USER_YMIN=None, TARGET_USER_YMAX=None, TARGET_USER_COLS=None, TARGET_USER_ROWS=None, TARGET_USER_FLAT=None, TARGET_USER_FITS=None, TARGET_SYSTEM=None, Verbose=2):
    '''
    Rotated to Regular Grid
    ----------
    [pj_proj4.18]\n
    This tool projects grids using rotated-pole coordinates to regular geographic grids.\n
    Rotated-pole coordinates are used by the CORDEX project.\n
    ____________\n
    CORDEX Domains\n
    ============\n
    CORDEX Area	Name	Resolution	N-Pole/Lon.	N-Pole/Lat.\n
    South America   	SAM-44	0.44	  -56.06	 70.60\n
    Central America 	CAM-44	0.44	  113.98	 75.74\n
    North America   	NAM-44	0.44	   83.00	 42.50\n
    Europe          	EUR-44	0.44	 -162.00	 39.25\n
    Africa          	AFR-44	0.44	     N/A	 90.00\n
    South Asia      	WAS-44	0.44	 -123.34	 79.95\n
    East Asia       	EAS-44	0.44	  -64.78	 77.61\n
    Central Asia    	CAS-44	0.44	 -103.39	 43.48\n
    Australasia     	AUS-44	0.44	  141.38	 60.31\n
    Antarctica      	ANT-44	0.44	 -166.92	  6.08\n
    Arctic          	ARC-44	0.44	    0.00	  6.55\n
    Mediterranean   	MED-44	0.44	  198.00	 39.25\n
    M-East, N-Africa	MNA-44	0.44	     N/A	 90.00\n
    M-East, N-Africa	MNA-22	0.22	     N/A	 90.00\n
    Europe          	EUR-11	0.11	 -162.00	 39.25\n
    South East Asia 	SEA-22	0.22	     N/A	 90.00\n
    ============\n
    Arguments
    ----------
    - SOURCE [`input grid list`] : Grids
    - TARGET_TEMPLATE [`optional input grid`] : Target System. use this grid's system for output grids
    - TARGET [`output grid list`] : Grids
    - EXTENT [`output shapes`] : Extent
    - ROT_POLE_LON [`floating point number`] : Longitude. Default: -162.000000
    - ROT_POLE_LAT [`floating point number`] : Latitude. Default: 39.250000
    - TARGET_DEFINITION [`choice`] : Target Grid System. Available Choices: [0] user defined [1] grid or grid system Default: 0
    - TARGET_USER_SIZE [`floating point number`] : Cellsize. Minimum: 0.000000 Default: 1.000000
    - TARGET_USER_XMIN [`floating point number`] : West. Default: 0.000000
    - TARGET_USER_XMAX [`floating point number`] : East. Default: 100.000000
    - TARGET_USER_YMIN [`floating point number`] : South. Default: 0.000000
    - TARGET_USER_YMAX [`floating point number`] : North. Default: 100.000000
    - TARGET_USER_COLS [`integer number`] : Columns. Minimum: 1 Default: 101 Number of cells in East-West direction.
    - TARGET_USER_ROWS [`integer number`] : Rows. Minimum: 1 Default: 101 Number of cells in North-South direction.
    - TARGET_USER_FLAT [`boolean`] : Rounding. Default: 1 Round bounding coordinates to multiples of cell size. Ignored if cell size has decimal places.
    - TARGET_USER_FITS [`choice`] : Fit. Available Choices: [0] nodes [1] cells Default: 0
    - TARGET_SYSTEM [`grid system`] : Grid System

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('pj_proj4', '18', 'Rotated to Regular Grid')
    if Tool.is_Okay():
        Tool.Set_Input ('SOURCE', SOURCE)
        Tool.Set_Input ('TARGET_TEMPLATE', TARGET_TEMPLATE)
        Tool.Set_Output('TARGET', TARGET)
        Tool.Set_Output('EXTENT', EXTENT)
        Tool.Set_Option('ROT_POLE_LON', ROT_POLE_LON)
        Tool.Set_Option('ROT_POLE_LAT', ROT_POLE_LAT)
        Tool.Set_Option('TARGET_DEFINITION', TARGET_DEFINITION)
        Tool.Set_Option('TARGET_USER_SIZE', TARGET_USER_SIZE)
        Tool.Set_Option('TARGET_USER_XMIN', TARGET_USER_XMIN)
        Tool.Set_Option('TARGET_USER_XMAX', TARGET_USER_XMAX)
        Tool.Set_Option('TARGET_USER_YMIN', TARGET_USER_YMIN)
        Tool.Set_Option('TARGET_USER_YMAX', TARGET_USER_YMAX)
        Tool.Set_Option('TARGET_USER_COLS', TARGET_USER_COLS)
        Tool.Set_Option('TARGET_USER_ROWS', TARGET_USER_ROWS)
        Tool.Set_Option('TARGET_USER_FLAT', TARGET_USER_FLAT)
        Tool.Set_Option('TARGET_USER_FITS', TARGET_USER_FITS)
        Tool.Set_Option('TARGET_SYSTEM', TARGET_SYSTEM)
        return Tool.Execute(Verbose)
    return False

def Coordinate_Reference_System_Format_Conversion(FORMATS=None, DEFINITION=None, FORMAT=None, MULTILINE=None, SIMPLIFIED=None, Verbose=2):
    '''
    Coordinate Reference System Format Conversion
    ----------
    [pj_proj4.19]\n
    Type in a Coordinate Reference System (CRS) definition and find its representation in various formats. Supported input formats are:\n
    (-) proj strings\n
    (-) WKT strings\n
    (-) object codes (e.g. "EPSG:4326", "ESRI:31493", "urn:ogc:def:crs:EPSG::4326", "urn:ogc:def:coordinateOperation:EPSG::1671")\n
    (-) object names (e.g. "WGS 84", "WGS 84 / UTM zone 31N", "Germany_Zone_3". In this case as uniqueness is not guaranteed, heuristics are applied to determine the appropriate best match.\n
    (-) OGC URN combining references for compound CRS (e.g "urn:ogc:def:crs,crs:EPSG::2393,crs:EPSG::5717" or custom abbreviated syntax "EPSG:2393+5717")\n
    (-) OGC URN combining references for concatenated operations (e.g. "urn:ogc:def:coordinateOperation,coordinateOperation:EPSG::3895,coordinateOperation:EPSG::1618")\n
    (-) PROJJSON strings (find the jsonschema at [proj.org](https://proj.org/schemas/v0.4/projjson.schema.json))\n
    (-) compound CRS made from two object names separated with " + " (e.g. "WGS 84 + EGM96 height")\n
    Arguments
    ----------
    - FORMATS [`output table`] : Formats
    - DEFINITION [`text`] : Definition
    - FORMAT [`choice`] : Format. Available Choices: [0] PROJ [1] WKT-1 [2] WKT-2 [3] JSON [4] ESRI [5] all [6] PROJ + WKT-2 Default: 6
    - MULTILINE [`boolean`] : Multiline. Default: 1 applies to JSON and WKT
    - SIMPLIFIED [`boolean`] : Simplified. Default: 1 applies to WKT-2

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('pj_proj4', '19', 'Coordinate Reference System Format Conversion')
    if Tool.is_Okay():
        Tool.Set_Output('FORMATS', FORMATS)
        Tool.Set_Option('DEFINITION', DEFINITION)
        Tool.Set_Option('FORMAT', FORMAT)
        Tool.Set_Option('MULTILINE', MULTILINE)
        Tool.Set_Option('SIMPLIFIED', SIMPLIFIED)
        return Tool.Execute(Verbose)
    return False

def run_tool_pj_proj4_19(FORMATS=None, DEFINITION=None, FORMAT=None, MULTILINE=None, SIMPLIFIED=None, Verbose=2):
    '''
    Coordinate Reference System Format Conversion
    ----------
    [pj_proj4.19]\n
    Type in a Coordinate Reference System (CRS) definition and find its representation in various formats. Supported input formats are:\n
    (-) proj strings\n
    (-) WKT strings\n
    (-) object codes (e.g. "EPSG:4326", "ESRI:31493", "urn:ogc:def:crs:EPSG::4326", "urn:ogc:def:coordinateOperation:EPSG::1671")\n
    (-) object names (e.g. "WGS 84", "WGS 84 / UTM zone 31N", "Germany_Zone_3". In this case as uniqueness is not guaranteed, heuristics are applied to determine the appropriate best match.\n
    (-) OGC URN combining references for compound CRS (e.g "urn:ogc:def:crs,crs:EPSG::2393,crs:EPSG::5717" or custom abbreviated syntax "EPSG:2393+5717")\n
    (-) OGC URN combining references for concatenated operations (e.g. "urn:ogc:def:coordinateOperation,coordinateOperation:EPSG::3895,coordinateOperation:EPSG::1618")\n
    (-) PROJJSON strings (find the jsonschema at [proj.org](https://proj.org/schemas/v0.4/projjson.schema.json))\n
    (-) compound CRS made from two object names separated with " + " (e.g. "WGS 84 + EGM96 height")\n
    Arguments
    ----------
    - FORMATS [`output table`] : Formats
    - DEFINITION [`text`] : Definition
    - FORMAT [`choice`] : Format. Available Choices: [0] PROJ [1] WKT-1 [2] WKT-2 [3] JSON [4] ESRI [5] all [6] PROJ + WKT-2 Default: 6
    - MULTILINE [`boolean`] : Multiline. Default: 1 applies to JSON and WKT
    - SIMPLIFIED [`boolean`] : Simplified. Default: 1 applies to WKT-2

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('pj_proj4', '19', 'Coordinate Reference System Format Conversion')
    if Tool.is_Okay():
        Tool.Set_Output('FORMATS', FORMATS)
        Tool.Set_Option('DEFINITION', DEFINITION)
        Tool.Set_Option('FORMAT', FORMAT)
        Tool.Set_Option('MULTILINE', MULTILINE)
        Tool.Set_Option('SIMPLIFIED', SIMPLIFIED)
        return Tool.Execute(Verbose)
    return False

def Geographic_Distances(PLANAR=None, ORTHODROME=None, LOXODROME=None, EPSILON=None, Verbose=2):
    '''
    Geographic Distances
    ----------
    [pj_proj4.20]\n
    Calculates for all segments of the input lines the planar, great elliptic, and loxodrome distance and re-projects the latter two to the projection of the input lines.\n
    Projection routines make use of the PROJ generic coordinate transformation software.\n
    PROJ Version is 9.4.1\n
    [PROJ Homepage](https://proj.org)\n
    Arguments
    ----------
    - PLANAR [`input shapes`] : Segments
    - ORTHODROME [`output shapes`] : Great Elliptic
    - LOXODROME [`output shapes`] : Loxodrome
    - EPSILON [`floating point number`] : Epsilon. Minimum: 1.000000 Default: 100.000000 defines the maximum resolution [km] for the re-projected distance segments

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('pj_proj4', '20', 'Geographic Distances')
    if Tool.is_Okay():
        Tool.Set_Input ('PLANAR', PLANAR)
        Tool.Set_Output('ORTHODROME', ORTHODROME)
        Tool.Set_Output('LOXODROME', LOXODROME)
        Tool.Set_Option('EPSILON', EPSILON)
        return Tool.Execute(Verbose)
    return False

def run_tool_pj_proj4_20(PLANAR=None, ORTHODROME=None, LOXODROME=None, EPSILON=None, Verbose=2):
    '''
    Geographic Distances
    ----------
    [pj_proj4.20]\n
    Calculates for all segments of the input lines the planar, great elliptic, and loxodrome distance and re-projects the latter two to the projection of the input lines.\n
    Projection routines make use of the PROJ generic coordinate transformation software.\n
    PROJ Version is 9.4.1\n
    [PROJ Homepage](https://proj.org)\n
    Arguments
    ----------
    - PLANAR [`input shapes`] : Segments
    - ORTHODROME [`output shapes`] : Great Elliptic
    - LOXODROME [`output shapes`] : Loxodrome
    - EPSILON [`floating point number`] : Epsilon. Minimum: 1.000000 Default: 100.000000 defines the maximum resolution [km] for the re-projected distance segments

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('pj_proj4', '20', 'Geographic Distances')
    if Tool.is_Okay():
        Tool.Set_Input ('PLANAR', PLANAR)
        Tool.Set_Output('ORTHODROME', ORTHODROME)
        Tool.Set_Output('LOXODROME', LOXODROME)
        Tool.Set_Option('EPSILON', EPSILON)
        return Tool.Execute(Verbose)
    return False

def Geographic_Distances_Pair_of_Coordinates(DISTANCES=None, CRS_STRING=None, CRS_FILE=None, COORD_X1=None, COORD_Y1=None, COORD_X2=None, COORD_Y2=None, EPSILON=None, Verbose=2):
    '''
    Geographic Distances (Pair of Coordinates)
    ----------
    [pj_proj4.21]\n
    Calculates for all segments of the input lines the planar, great elliptic, and loxodrome distance and re-projects the latter two to the projection of the input lines.\n
    Arguments
    ----------
    - DISTANCES [`output shapes`] : Geographic Distances
    - CRS_STRING [`text`] : Definition String. Default: +proj=longlat +datum=WGS84 +no_defs Supported formats comprise PROJ and WKT strings, object codes (e.g. "EPSG:4326").
    - CRS_FILE [`file path`] : Well Known Text File
    - COORD_X1 [`floating point number`] : X. Default: 10.000000
    - COORD_Y1 [`floating point number`] : Y. Default: 53.500000
    - COORD_X2 [`floating point number`] : X. Default: 116.500000
    - COORD_Y2 [`floating point number`] : Y. Default: 6.400000
    - EPSILON [`floating point number`] : Epsilon. Minimum: 1.000000 Default: 100.000000 defines the maximum resolution [km] for the re-projected distance segments

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('pj_proj4', '21', 'Geographic Distances (Pair of Coordinates)')
    if Tool.is_Okay():
        Tool.Set_Output('DISTANCES', DISTANCES)
        Tool.Set_Option('CRS_STRING', CRS_STRING)
        Tool.Set_Option('CRS_FILE', CRS_FILE)
        Tool.Set_Option('COORD_X1', COORD_X1)
        Tool.Set_Option('COORD_Y1', COORD_Y1)
        Tool.Set_Option('COORD_X2', COORD_X2)
        Tool.Set_Option('COORD_Y2', COORD_Y2)
        Tool.Set_Option('EPSILON', EPSILON)
        return Tool.Execute(Verbose)
    return False

def run_tool_pj_proj4_21(DISTANCES=None, CRS_STRING=None, CRS_FILE=None, COORD_X1=None, COORD_Y1=None, COORD_X2=None, COORD_Y2=None, EPSILON=None, Verbose=2):
    '''
    Geographic Distances (Pair of Coordinates)
    ----------
    [pj_proj4.21]\n
    Calculates for all segments of the input lines the planar, great elliptic, and loxodrome distance and re-projects the latter two to the projection of the input lines.\n
    Arguments
    ----------
    - DISTANCES [`output shapes`] : Geographic Distances
    - CRS_STRING [`text`] : Definition String. Default: +proj=longlat +datum=WGS84 +no_defs Supported formats comprise PROJ and WKT strings, object codes (e.g. "EPSG:4326").
    - CRS_FILE [`file path`] : Well Known Text File
    - COORD_X1 [`floating point number`] : X. Default: 10.000000
    - COORD_Y1 [`floating point number`] : Y. Default: 53.500000
    - COORD_X2 [`floating point number`] : X. Default: 116.500000
    - COORD_Y2 [`floating point number`] : Y. Default: 6.400000
    - EPSILON [`floating point number`] : Epsilon. Minimum: 1.000000 Default: 100.000000 defines the maximum resolution [km] for the re-projected distance segments

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('pj_proj4', '21', 'Geographic Distances (Pair of Coordinates)')
    if Tool.is_Okay():
        Tool.Set_Output('DISTANCES', DISTANCES)
        Tool.Set_Option('CRS_STRING', CRS_STRING)
        Tool.Set_Option('CRS_FILE', CRS_FILE)
        Tool.Set_Option('COORD_X1', COORD_X1)
        Tool.Set_Option('COORD_Y1', COORD_Y1)
        Tool.Set_Option('COORD_X2', COORD_X2)
        Tool.Set_Option('COORD_Y2', COORD_Y2)
        Tool.Set_Option('EPSILON', EPSILON)
        return Tool.Execute(Verbose)
    return False

def UTM_Projection_Grid_List(SOURCE=None, TARGET_TEMPLATE=None, GRIDS=None, OUT_X=None, OUT_Y=None, CRS_STRING=None, CRS_FILE=None, RESAMPLING=None, BYTEWISE=None, DATA_TYPE=None, TARGET_AREA=None, TARGET_DEFINITION=None, TARGET_USER_SIZE=None, TARGET_USER_XMIN=None, TARGET_USER_XMAX=None, TARGET_USER_YMIN=None, TARGET_USER_YMAX=None, TARGET_USER_COLS=None, TARGET_USER_ROWS=None, TARGET_USER_FLAT=None, TARGET_USER_FITS=None, TARGET_SYSTEM=None, UTM_ZONE=None, UTM_SOUTH=None, Verbose=2):
    '''
    UTM Projection (Grid List)
    ----------
    [pj_proj4.23]\n
    Project grids into UTM coordinates.\n
    Projection routines make use of the PROJ generic coordinate transformation software.\n
    PROJ Version is 9.4.1\n
    [PROJ Homepage](https://proj.org)\n
    Arguments
    ----------
    - SOURCE [`input grid list`] : Source
    - TARGET_TEMPLATE [`optional input grid`] : Target System. use this grid's system for output grids
    - GRIDS [`output grid list`] : Target
    - OUT_X [`output grid`] : X Coordinates
    - OUT_Y [`output grid`] : Y Coordinates
    - CRS_STRING [`text`] : Definition String. Default: +proj=longlat +datum=WGS84 +no_defs Supported formats comprise PROJ and WKT strings, object codes (e.g. "EPSG:4326").
    - CRS_FILE [`file path`] : Well Known Text File
    - RESAMPLING [`choice`] : Resampling. Available Choices: [0] Nearest Neighbour [1] Bilinear Interpolation [2] Bicubic Spline Interpolation [3] B-Spline Interpolation Default: 3
    - BYTEWISE [`boolean`] : Bytewise Interpolation. Default: 0 To be used for RGB and CMYK coded values (i.e. images).
    - DATA_TYPE [`data type`] : Data Type. Available Choices: [0] unsigned 1 byte integer [1] signed 1 byte integer [2] unsigned 2 byte integer [3] signed 2 byte integer [4] unsigned 4 byte integer [5] signed 4 byte integer [6] unsigned 8 byte integer [7] signed 8 byte integer [8] 4 byte floating point number [9] 8 byte floating point number [10] Preserve Default: 10
    - TARGET_AREA [`boolean`] : Use Target Area Polygon. Default: 0 Restricts targeted grid cells to area of the projected bounding rectangle. Useful with certain projections for global data.
    - TARGET_DEFINITION [`choice`] : Target Grid System. Available Choices: [0] user defined [1] grid or grid system Default: 0
    - TARGET_USER_SIZE [`floating point number`] : Cellsize. Minimum: 0.000000 Default: 1.000000
    - TARGET_USER_XMIN [`floating point number`] : West. Default: 0.000000
    - TARGET_USER_XMAX [`floating point number`] : East. Default: 100.000000
    - TARGET_USER_YMIN [`floating point number`] : South. Default: 0.000000
    - TARGET_USER_YMAX [`floating point number`] : North. Default: 100.000000
    - TARGET_USER_COLS [`integer number`] : Columns. Minimum: 1 Default: 101 Number of cells in East-West direction.
    - TARGET_USER_ROWS [`integer number`] : Rows. Minimum: 1 Default: 101 Number of cells in North-South direction.
    - TARGET_USER_FLAT [`boolean`] : Rounding. Default: 1 Round bounding coordinates to multiples of cell size. Ignored if cell size has decimal places.
    - TARGET_USER_FITS [`choice`] : Fit. Available Choices: [0] nodes [1] cells Default: 0
    - TARGET_SYSTEM [`grid system`] : Grid System
    - UTM_ZONE [`integer number`] : Zone. Minimum: 1 Maximum: 60 Default: 1
    - UTM_SOUTH [`boolean`] : South. Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('pj_proj4', '23', 'UTM Projection (Grid List)')
    if Tool.is_Okay():
        Tool.Set_Input ('SOURCE', SOURCE)
        Tool.Set_Input ('TARGET_TEMPLATE', TARGET_TEMPLATE)
        Tool.Set_Output('GRIDS', GRIDS)
        Tool.Set_Output('OUT_X', OUT_X)
        Tool.Set_Output('OUT_Y', OUT_Y)
        Tool.Set_Option('CRS_STRING', CRS_STRING)
        Tool.Set_Option('CRS_FILE', CRS_FILE)
        Tool.Set_Option('RESAMPLING', RESAMPLING)
        Tool.Set_Option('BYTEWISE', BYTEWISE)
        Tool.Set_Option('DATA_TYPE', DATA_TYPE)
        Tool.Set_Option('TARGET_AREA', TARGET_AREA)
        Tool.Set_Option('TARGET_DEFINITION', TARGET_DEFINITION)
        Tool.Set_Option('TARGET_USER_SIZE', TARGET_USER_SIZE)
        Tool.Set_Option('TARGET_USER_XMIN', TARGET_USER_XMIN)
        Tool.Set_Option('TARGET_USER_XMAX', TARGET_USER_XMAX)
        Tool.Set_Option('TARGET_USER_YMIN', TARGET_USER_YMIN)
        Tool.Set_Option('TARGET_USER_YMAX', TARGET_USER_YMAX)
        Tool.Set_Option('TARGET_USER_COLS', TARGET_USER_COLS)
        Tool.Set_Option('TARGET_USER_ROWS', TARGET_USER_ROWS)
        Tool.Set_Option('TARGET_USER_FLAT', TARGET_USER_FLAT)
        Tool.Set_Option('TARGET_USER_FITS', TARGET_USER_FITS)
        Tool.Set_Option('TARGET_SYSTEM', TARGET_SYSTEM)
        Tool.Set_Option('UTM_ZONE', UTM_ZONE)
        Tool.Set_Option('UTM_SOUTH', UTM_SOUTH)
        return Tool.Execute(Verbose)
    return False

def run_tool_pj_proj4_23(SOURCE=None, TARGET_TEMPLATE=None, GRIDS=None, OUT_X=None, OUT_Y=None, CRS_STRING=None, CRS_FILE=None, RESAMPLING=None, BYTEWISE=None, DATA_TYPE=None, TARGET_AREA=None, TARGET_DEFINITION=None, TARGET_USER_SIZE=None, TARGET_USER_XMIN=None, TARGET_USER_XMAX=None, TARGET_USER_YMIN=None, TARGET_USER_YMAX=None, TARGET_USER_COLS=None, TARGET_USER_ROWS=None, TARGET_USER_FLAT=None, TARGET_USER_FITS=None, TARGET_SYSTEM=None, UTM_ZONE=None, UTM_SOUTH=None, Verbose=2):
    '''
    UTM Projection (Grid List)
    ----------
    [pj_proj4.23]\n
    Project grids into UTM coordinates.\n
    Projection routines make use of the PROJ generic coordinate transformation software.\n
    PROJ Version is 9.4.1\n
    [PROJ Homepage](https://proj.org)\n
    Arguments
    ----------
    - SOURCE [`input grid list`] : Source
    - TARGET_TEMPLATE [`optional input grid`] : Target System. use this grid's system for output grids
    - GRIDS [`output grid list`] : Target
    - OUT_X [`output grid`] : X Coordinates
    - OUT_Y [`output grid`] : Y Coordinates
    - CRS_STRING [`text`] : Definition String. Default: +proj=longlat +datum=WGS84 +no_defs Supported formats comprise PROJ and WKT strings, object codes (e.g. "EPSG:4326").
    - CRS_FILE [`file path`] : Well Known Text File
    - RESAMPLING [`choice`] : Resampling. Available Choices: [0] Nearest Neighbour [1] Bilinear Interpolation [2] Bicubic Spline Interpolation [3] B-Spline Interpolation Default: 3
    - BYTEWISE [`boolean`] : Bytewise Interpolation. Default: 0 To be used for RGB and CMYK coded values (i.e. images).
    - DATA_TYPE [`data type`] : Data Type. Available Choices: [0] unsigned 1 byte integer [1] signed 1 byte integer [2] unsigned 2 byte integer [3] signed 2 byte integer [4] unsigned 4 byte integer [5] signed 4 byte integer [6] unsigned 8 byte integer [7] signed 8 byte integer [8] 4 byte floating point number [9] 8 byte floating point number [10] Preserve Default: 10
    - TARGET_AREA [`boolean`] : Use Target Area Polygon. Default: 0 Restricts targeted grid cells to area of the projected bounding rectangle. Useful with certain projections for global data.
    - TARGET_DEFINITION [`choice`] : Target Grid System. Available Choices: [0] user defined [1] grid or grid system Default: 0
    - TARGET_USER_SIZE [`floating point number`] : Cellsize. Minimum: 0.000000 Default: 1.000000
    - TARGET_USER_XMIN [`floating point number`] : West. Default: 0.000000
    - TARGET_USER_XMAX [`floating point number`] : East. Default: 100.000000
    - TARGET_USER_YMIN [`floating point number`] : South. Default: 0.000000
    - TARGET_USER_YMAX [`floating point number`] : North. Default: 100.000000
    - TARGET_USER_COLS [`integer number`] : Columns. Minimum: 1 Default: 101 Number of cells in East-West direction.
    - TARGET_USER_ROWS [`integer number`] : Rows. Minimum: 1 Default: 101 Number of cells in North-South direction.
    - TARGET_USER_FLAT [`boolean`] : Rounding. Default: 1 Round bounding coordinates to multiples of cell size. Ignored if cell size has decimal places.
    - TARGET_USER_FITS [`choice`] : Fit. Available Choices: [0] nodes [1] cells Default: 0
    - TARGET_SYSTEM [`grid system`] : Grid System
    - UTM_ZONE [`integer number`] : Zone. Minimum: 1 Maximum: 60 Default: 1
    - UTM_SOUTH [`boolean`] : South. Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('pj_proj4', '23', 'UTM Projection (Grid List)')
    if Tool.is_Okay():
        Tool.Set_Input ('SOURCE', SOURCE)
        Tool.Set_Input ('TARGET_TEMPLATE', TARGET_TEMPLATE)
        Tool.Set_Output('GRIDS', GRIDS)
        Tool.Set_Output('OUT_X', OUT_X)
        Tool.Set_Output('OUT_Y', OUT_Y)
        Tool.Set_Option('CRS_STRING', CRS_STRING)
        Tool.Set_Option('CRS_FILE', CRS_FILE)
        Tool.Set_Option('RESAMPLING', RESAMPLING)
        Tool.Set_Option('BYTEWISE', BYTEWISE)
        Tool.Set_Option('DATA_TYPE', DATA_TYPE)
        Tool.Set_Option('TARGET_AREA', TARGET_AREA)
        Tool.Set_Option('TARGET_DEFINITION', TARGET_DEFINITION)
        Tool.Set_Option('TARGET_USER_SIZE', TARGET_USER_SIZE)
        Tool.Set_Option('TARGET_USER_XMIN', TARGET_USER_XMIN)
        Tool.Set_Option('TARGET_USER_XMAX', TARGET_USER_XMAX)
        Tool.Set_Option('TARGET_USER_YMIN', TARGET_USER_YMIN)
        Tool.Set_Option('TARGET_USER_YMAX', TARGET_USER_YMAX)
        Tool.Set_Option('TARGET_USER_COLS', TARGET_USER_COLS)
        Tool.Set_Option('TARGET_USER_ROWS', TARGET_USER_ROWS)
        Tool.Set_Option('TARGET_USER_FLAT', TARGET_USER_FLAT)
        Tool.Set_Option('TARGET_USER_FITS', TARGET_USER_FITS)
        Tool.Set_Option('TARGET_SYSTEM', TARGET_SYSTEM)
        Tool.Set_Option('UTM_ZONE', UTM_ZONE)
        Tool.Set_Option('UTM_SOUTH', UTM_SOUTH)
        return Tool.Execute(Verbose)
    return False

def UTM_Projection_Grid(SOURCE=None, TARGET_TEMPLATE=None, GRID=None, OUT_X=None, OUT_Y=None, CRS_STRING=None, CRS_FILE=None, SOURCE_GRIDSYSTEM=None, RESAMPLING=None, BYTEWISE=None, DATA_TYPE=None, TARGET_AREA=None, TARGET_DEFINITION=None, TARGET_USER_SIZE=None, TARGET_USER_XMIN=None, TARGET_USER_XMAX=None, TARGET_USER_YMIN=None, TARGET_USER_YMAX=None, TARGET_USER_COLS=None, TARGET_USER_ROWS=None, TARGET_USER_FLAT=None, TARGET_USER_FITS=None, TARGET_SYSTEM=None, UTM_ZONE=None, UTM_SOUTH=None, Verbose=2):
    '''
    UTM Projection (Grid)
    ----------
    [pj_proj4.24]\n
    Project grids into UTM coordinates.\n
    Projection routines make use of the PROJ generic coordinate transformation software.\n
    PROJ Version is 9.4.1\n
    [PROJ Homepage](https://proj.org)\n
    Arguments
    ----------
    - SOURCE [`input grid`] : Source
    - TARGET_TEMPLATE [`optional input grid`] : Target System. use this grid's system for output grids
    - GRID [`output grid`] : Target
    - OUT_X [`output grid`] : X Coordinates
    - OUT_Y [`output grid`] : Y Coordinates
    - CRS_STRING [`text`] : Definition String. Default: +proj=longlat +datum=WGS84 +no_defs Supported formats comprise PROJ and WKT strings, object codes (e.g. "EPSG:4326").
    - CRS_FILE [`file path`] : Well Known Text File
    - SOURCE_GRIDSYSTEM [`grid system`] : Grid system
    - RESAMPLING [`choice`] : Resampling. Available Choices: [0] Nearest Neighbour [1] Bilinear Interpolation [2] Bicubic Spline Interpolation [3] B-Spline Interpolation Default: 3
    - BYTEWISE [`boolean`] : Bytewise Interpolation. Default: 0 To be used for RGB and CMYK coded values (i.e. images).
    - DATA_TYPE [`data type`] : Data Type. Available Choices: [0] unsigned 1 byte integer [1] signed 1 byte integer [2] unsigned 2 byte integer [3] signed 2 byte integer [4] unsigned 4 byte integer [5] signed 4 byte integer [6] unsigned 8 byte integer [7] signed 8 byte integer [8] 4 byte floating point number [9] 8 byte floating point number [10] Preserve Default: 10
    - TARGET_AREA [`boolean`] : Use Target Area Polygon. Default: 0 Restricts targeted grid cells to area of the projected bounding rectangle. Useful with certain projections for global data.
    - TARGET_DEFINITION [`choice`] : Target Grid System. Available Choices: [0] user defined [1] grid or grid system Default: 0
    - TARGET_USER_SIZE [`floating point number`] : Cellsize. Minimum: 0.000000 Default: 1.000000
    - TARGET_USER_XMIN [`floating point number`] : West. Default: 0.000000
    - TARGET_USER_XMAX [`floating point number`] : East. Default: 100.000000
    - TARGET_USER_YMIN [`floating point number`] : South. Default: 0.000000
    - TARGET_USER_YMAX [`floating point number`] : North. Default: 100.000000
    - TARGET_USER_COLS [`integer number`] : Columns. Minimum: 1 Default: 101 Number of cells in East-West direction.
    - TARGET_USER_ROWS [`integer number`] : Rows. Minimum: 1 Default: 101 Number of cells in North-South direction.
    - TARGET_USER_FLAT [`boolean`] : Rounding. Default: 1 Round bounding coordinates to multiples of cell size. Ignored if cell size has decimal places.
    - TARGET_USER_FITS [`choice`] : Fit. Available Choices: [0] nodes [1] cells Default: 0
    - TARGET_SYSTEM [`grid system`] : Grid System
    - UTM_ZONE [`integer number`] : Zone. Minimum: 1 Maximum: 60 Default: 1
    - UTM_SOUTH [`boolean`] : South. Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('pj_proj4', '24', 'UTM Projection (Grid)')
    if Tool.is_Okay():
        Tool.Set_Input ('SOURCE', SOURCE)
        Tool.Set_Input ('TARGET_TEMPLATE', TARGET_TEMPLATE)
        Tool.Set_Output('GRID', GRID)
        Tool.Set_Output('OUT_X', OUT_X)
        Tool.Set_Output('OUT_Y', OUT_Y)
        Tool.Set_Option('CRS_STRING', CRS_STRING)
        Tool.Set_Option('CRS_FILE', CRS_FILE)
        Tool.Set_Option('SOURCE_GRIDSYSTEM', SOURCE_GRIDSYSTEM)
        Tool.Set_Option('RESAMPLING', RESAMPLING)
        Tool.Set_Option('BYTEWISE', BYTEWISE)
        Tool.Set_Option('DATA_TYPE', DATA_TYPE)
        Tool.Set_Option('TARGET_AREA', TARGET_AREA)
        Tool.Set_Option('TARGET_DEFINITION', TARGET_DEFINITION)
        Tool.Set_Option('TARGET_USER_SIZE', TARGET_USER_SIZE)
        Tool.Set_Option('TARGET_USER_XMIN', TARGET_USER_XMIN)
        Tool.Set_Option('TARGET_USER_XMAX', TARGET_USER_XMAX)
        Tool.Set_Option('TARGET_USER_YMIN', TARGET_USER_YMIN)
        Tool.Set_Option('TARGET_USER_YMAX', TARGET_USER_YMAX)
        Tool.Set_Option('TARGET_USER_COLS', TARGET_USER_COLS)
        Tool.Set_Option('TARGET_USER_ROWS', TARGET_USER_ROWS)
        Tool.Set_Option('TARGET_USER_FLAT', TARGET_USER_FLAT)
        Tool.Set_Option('TARGET_USER_FITS', TARGET_USER_FITS)
        Tool.Set_Option('TARGET_SYSTEM', TARGET_SYSTEM)
        Tool.Set_Option('UTM_ZONE', UTM_ZONE)
        Tool.Set_Option('UTM_SOUTH', UTM_SOUTH)
        return Tool.Execute(Verbose)
    return False

def run_tool_pj_proj4_24(SOURCE=None, TARGET_TEMPLATE=None, GRID=None, OUT_X=None, OUT_Y=None, CRS_STRING=None, CRS_FILE=None, SOURCE_GRIDSYSTEM=None, RESAMPLING=None, BYTEWISE=None, DATA_TYPE=None, TARGET_AREA=None, TARGET_DEFINITION=None, TARGET_USER_SIZE=None, TARGET_USER_XMIN=None, TARGET_USER_XMAX=None, TARGET_USER_YMIN=None, TARGET_USER_YMAX=None, TARGET_USER_COLS=None, TARGET_USER_ROWS=None, TARGET_USER_FLAT=None, TARGET_USER_FITS=None, TARGET_SYSTEM=None, UTM_ZONE=None, UTM_SOUTH=None, Verbose=2):
    '''
    UTM Projection (Grid)
    ----------
    [pj_proj4.24]\n
    Project grids into UTM coordinates.\n
    Projection routines make use of the PROJ generic coordinate transformation software.\n
    PROJ Version is 9.4.1\n
    [PROJ Homepage](https://proj.org)\n
    Arguments
    ----------
    - SOURCE [`input grid`] : Source
    - TARGET_TEMPLATE [`optional input grid`] : Target System. use this grid's system for output grids
    - GRID [`output grid`] : Target
    - OUT_X [`output grid`] : X Coordinates
    - OUT_Y [`output grid`] : Y Coordinates
    - CRS_STRING [`text`] : Definition String. Default: +proj=longlat +datum=WGS84 +no_defs Supported formats comprise PROJ and WKT strings, object codes (e.g. "EPSG:4326").
    - CRS_FILE [`file path`] : Well Known Text File
    - SOURCE_GRIDSYSTEM [`grid system`] : Grid system
    - RESAMPLING [`choice`] : Resampling. Available Choices: [0] Nearest Neighbour [1] Bilinear Interpolation [2] Bicubic Spline Interpolation [3] B-Spline Interpolation Default: 3
    - BYTEWISE [`boolean`] : Bytewise Interpolation. Default: 0 To be used for RGB and CMYK coded values (i.e. images).
    - DATA_TYPE [`data type`] : Data Type. Available Choices: [0] unsigned 1 byte integer [1] signed 1 byte integer [2] unsigned 2 byte integer [3] signed 2 byte integer [4] unsigned 4 byte integer [5] signed 4 byte integer [6] unsigned 8 byte integer [7] signed 8 byte integer [8] 4 byte floating point number [9] 8 byte floating point number [10] Preserve Default: 10
    - TARGET_AREA [`boolean`] : Use Target Area Polygon. Default: 0 Restricts targeted grid cells to area of the projected bounding rectangle. Useful with certain projections for global data.
    - TARGET_DEFINITION [`choice`] : Target Grid System. Available Choices: [0] user defined [1] grid or grid system Default: 0
    - TARGET_USER_SIZE [`floating point number`] : Cellsize. Minimum: 0.000000 Default: 1.000000
    - TARGET_USER_XMIN [`floating point number`] : West. Default: 0.000000
    - TARGET_USER_XMAX [`floating point number`] : East. Default: 100.000000
    - TARGET_USER_YMIN [`floating point number`] : South. Default: 0.000000
    - TARGET_USER_YMAX [`floating point number`] : North. Default: 100.000000
    - TARGET_USER_COLS [`integer number`] : Columns. Minimum: 1 Default: 101 Number of cells in East-West direction.
    - TARGET_USER_ROWS [`integer number`] : Rows. Minimum: 1 Default: 101 Number of cells in North-South direction.
    - TARGET_USER_FLAT [`boolean`] : Rounding. Default: 1 Round bounding coordinates to multiples of cell size. Ignored if cell size has decimal places.
    - TARGET_USER_FITS [`choice`] : Fit. Available Choices: [0] nodes [1] cells Default: 0
    - TARGET_SYSTEM [`grid system`] : Grid System
    - UTM_ZONE [`integer number`] : Zone. Minimum: 1 Maximum: 60 Default: 1
    - UTM_SOUTH [`boolean`] : South. Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('pj_proj4', '24', 'UTM Projection (Grid)')
    if Tool.is_Okay():
        Tool.Set_Input ('SOURCE', SOURCE)
        Tool.Set_Input ('TARGET_TEMPLATE', TARGET_TEMPLATE)
        Tool.Set_Output('GRID', GRID)
        Tool.Set_Output('OUT_X', OUT_X)
        Tool.Set_Output('OUT_Y', OUT_Y)
        Tool.Set_Option('CRS_STRING', CRS_STRING)
        Tool.Set_Option('CRS_FILE', CRS_FILE)
        Tool.Set_Option('SOURCE_GRIDSYSTEM', SOURCE_GRIDSYSTEM)
        Tool.Set_Option('RESAMPLING', RESAMPLING)
        Tool.Set_Option('BYTEWISE', BYTEWISE)
        Tool.Set_Option('DATA_TYPE', DATA_TYPE)
        Tool.Set_Option('TARGET_AREA', TARGET_AREA)
        Tool.Set_Option('TARGET_DEFINITION', TARGET_DEFINITION)
        Tool.Set_Option('TARGET_USER_SIZE', TARGET_USER_SIZE)
        Tool.Set_Option('TARGET_USER_XMIN', TARGET_USER_XMIN)
        Tool.Set_Option('TARGET_USER_XMAX', TARGET_USER_XMAX)
        Tool.Set_Option('TARGET_USER_YMIN', TARGET_USER_YMIN)
        Tool.Set_Option('TARGET_USER_YMAX', TARGET_USER_YMAX)
        Tool.Set_Option('TARGET_USER_COLS', TARGET_USER_COLS)
        Tool.Set_Option('TARGET_USER_ROWS', TARGET_USER_ROWS)
        Tool.Set_Option('TARGET_USER_FLAT', TARGET_USER_FLAT)
        Tool.Set_Option('TARGET_USER_FITS', TARGET_USER_FITS)
        Tool.Set_Option('TARGET_SYSTEM', TARGET_SYSTEM)
        Tool.Set_Option('UTM_ZONE', UTM_ZONE)
        Tool.Set_Option('UTM_SOUTH', UTM_SOUTH)
        return Tool.Execute(Verbose)
    return False

def UTM_Projection_Shapes_List(SOURCE=None, TARGET=None, CRS_STRING=None, CRS_FILE=None, TRANSFORM_Z=None, PARALLEL=None, COPY=None, UTM_ZONE=None, UTM_SOUTH=None, Verbose=2):
    '''
    UTM Projection (Shapes List)
    ----------
    [pj_proj4.25]\n
    Project shapes into UTM coordinates.\n
    Projection routines make use of the PROJ generic coordinate transformation software.\n
    PROJ Version is 9.4.1\n
    [PROJ Homepage](https://proj.org)\n
    Arguments
    ----------
    - SOURCE [`input shapes list`] : Source
    - TARGET [`output shapes list`] : Target
    - CRS_STRING [`text`] : Definition String. Default: +proj=longlat +datum=WGS84 +no_defs Supported formats comprise PROJ and WKT strings, object codes (e.g. "EPSG:4326").
    - CRS_FILE [`file path`] : Well Known Text File
    - TRANSFORM_Z [`boolean`] : Z Transformation. Default: 1 Transform elevation (z) values, if appropriate.
    - PARALLEL [`boolean`] : Parallel Processing. Default: 0
    - COPY [`boolean`] : Copy. Default: 1 If set the projected data will be created as a copy of the original, if not vertices will be projected in place thus reducing memory requirements.
    - UTM_ZONE [`integer number`] : Zone. Minimum: 1 Maximum: 60 Default: 1
    - UTM_SOUTH [`boolean`] : South. Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('pj_proj4', '25', 'UTM Projection (Shapes List)')
    if Tool.is_Okay():
        Tool.Set_Input ('SOURCE', SOURCE)
        Tool.Set_Output('TARGET', TARGET)
        Tool.Set_Option('CRS_STRING', CRS_STRING)
        Tool.Set_Option('CRS_FILE', CRS_FILE)
        Tool.Set_Option('TRANSFORM_Z', TRANSFORM_Z)
        Tool.Set_Option('PARALLEL', PARALLEL)
        Tool.Set_Option('COPY', COPY)
        Tool.Set_Option('UTM_ZONE', UTM_ZONE)
        Tool.Set_Option('UTM_SOUTH', UTM_SOUTH)
        return Tool.Execute(Verbose)
    return False

def run_tool_pj_proj4_25(SOURCE=None, TARGET=None, CRS_STRING=None, CRS_FILE=None, TRANSFORM_Z=None, PARALLEL=None, COPY=None, UTM_ZONE=None, UTM_SOUTH=None, Verbose=2):
    '''
    UTM Projection (Shapes List)
    ----------
    [pj_proj4.25]\n
    Project shapes into UTM coordinates.\n
    Projection routines make use of the PROJ generic coordinate transformation software.\n
    PROJ Version is 9.4.1\n
    [PROJ Homepage](https://proj.org)\n
    Arguments
    ----------
    - SOURCE [`input shapes list`] : Source
    - TARGET [`output shapes list`] : Target
    - CRS_STRING [`text`] : Definition String. Default: +proj=longlat +datum=WGS84 +no_defs Supported formats comprise PROJ and WKT strings, object codes (e.g. "EPSG:4326").
    - CRS_FILE [`file path`] : Well Known Text File
    - TRANSFORM_Z [`boolean`] : Z Transformation. Default: 1 Transform elevation (z) values, if appropriate.
    - PARALLEL [`boolean`] : Parallel Processing. Default: 0
    - COPY [`boolean`] : Copy. Default: 1 If set the projected data will be created as a copy of the original, if not vertices will be projected in place thus reducing memory requirements.
    - UTM_ZONE [`integer number`] : Zone. Minimum: 1 Maximum: 60 Default: 1
    - UTM_SOUTH [`boolean`] : South. Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('pj_proj4', '25', 'UTM Projection (Shapes List)')
    if Tool.is_Okay():
        Tool.Set_Input ('SOURCE', SOURCE)
        Tool.Set_Output('TARGET', TARGET)
        Tool.Set_Option('CRS_STRING', CRS_STRING)
        Tool.Set_Option('CRS_FILE', CRS_FILE)
        Tool.Set_Option('TRANSFORM_Z', TRANSFORM_Z)
        Tool.Set_Option('PARALLEL', PARALLEL)
        Tool.Set_Option('COPY', COPY)
        Tool.Set_Option('UTM_ZONE', UTM_ZONE)
        Tool.Set_Option('UTM_SOUTH', UTM_SOUTH)
        return Tool.Execute(Verbose)
    return False

def UTM_Projection_Shapes(SOURCE=None, TARGET=None, TARGET_PC=None, CRS_STRING=None, CRS_FILE=None, TRANSFORM_Z=None, PARALLEL=None, COPY=None, UTM_ZONE=None, UTM_SOUTH=None, Verbose=2):
    '''
    UTM Projection (Shapes)
    ----------
    [pj_proj4.26]\n
    Project shapes into UTM coordinates.\n
    Projection routines make use of the PROJ generic coordinate transformation software.\n
    PROJ Version is 9.4.1\n
    [PROJ Homepage](https://proj.org)\n
    Arguments
    ----------
    - SOURCE [`input shapes`] : Source
    - TARGET [`output shapes`] : Target
    - TARGET_PC [`output point cloud`] : Target
    - CRS_STRING [`text`] : Definition String. Default: +proj=longlat +datum=WGS84 +no_defs Supported formats comprise PROJ and WKT strings, object codes (e.g. "EPSG:4326").
    - CRS_FILE [`file path`] : Well Known Text File
    - TRANSFORM_Z [`boolean`] : Z Transformation. Default: 1 Transform elevation (z) values, if appropriate.
    - PARALLEL [`boolean`] : Parallel Processing. Default: 0
    - COPY [`boolean`] : Copy. Default: 1 If set the projected data will be created as a copy of the original, if not vertices will be projected in place thus reducing memory requirements.
    - UTM_ZONE [`integer number`] : Zone. Minimum: 1 Maximum: 60 Default: 1
    - UTM_SOUTH [`boolean`] : South. Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('pj_proj4', '26', 'UTM Projection (Shapes)')
    if Tool.is_Okay():
        Tool.Set_Input ('SOURCE', SOURCE)
        Tool.Set_Output('TARGET', TARGET)
        Tool.Set_Output('TARGET_PC', TARGET_PC)
        Tool.Set_Option('CRS_STRING', CRS_STRING)
        Tool.Set_Option('CRS_FILE', CRS_FILE)
        Tool.Set_Option('TRANSFORM_Z', TRANSFORM_Z)
        Tool.Set_Option('PARALLEL', PARALLEL)
        Tool.Set_Option('COPY', COPY)
        Tool.Set_Option('UTM_ZONE', UTM_ZONE)
        Tool.Set_Option('UTM_SOUTH', UTM_SOUTH)
        return Tool.Execute(Verbose)
    return False

def run_tool_pj_proj4_26(SOURCE=None, TARGET=None, TARGET_PC=None, CRS_STRING=None, CRS_FILE=None, TRANSFORM_Z=None, PARALLEL=None, COPY=None, UTM_ZONE=None, UTM_SOUTH=None, Verbose=2):
    '''
    UTM Projection (Shapes)
    ----------
    [pj_proj4.26]\n
    Project shapes into UTM coordinates.\n
    Projection routines make use of the PROJ generic coordinate transformation software.\n
    PROJ Version is 9.4.1\n
    [PROJ Homepage](https://proj.org)\n
    Arguments
    ----------
    - SOURCE [`input shapes`] : Source
    - TARGET [`output shapes`] : Target
    - TARGET_PC [`output point cloud`] : Target
    - CRS_STRING [`text`] : Definition String. Default: +proj=longlat +datum=WGS84 +no_defs Supported formats comprise PROJ and WKT strings, object codes (e.g. "EPSG:4326").
    - CRS_FILE [`file path`] : Well Known Text File
    - TRANSFORM_Z [`boolean`] : Z Transformation. Default: 1 Transform elevation (z) values, if appropriate.
    - PARALLEL [`boolean`] : Parallel Processing. Default: 0
    - COPY [`boolean`] : Copy. Default: 1 If set the projected data will be created as a copy of the original, if not vertices will be projected in place thus reducing memory requirements.
    - UTM_ZONE [`integer number`] : Zone. Minimum: 1 Maximum: 60 Default: 1
    - UTM_SOUTH [`boolean`] : South. Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('pj_proj4', '26', 'UTM Projection (Shapes)')
    if Tool.is_Okay():
        Tool.Set_Input ('SOURCE', SOURCE)
        Tool.Set_Output('TARGET', TARGET)
        Tool.Set_Output('TARGET_PC', TARGET_PC)
        Tool.Set_Option('CRS_STRING', CRS_STRING)
        Tool.Set_Option('CRS_FILE', CRS_FILE)
        Tool.Set_Option('TRANSFORM_Z', TRANSFORM_Z)
        Tool.Set_Option('PARALLEL', PARALLEL)
        Tool.Set_Option('COPY', COPY)
        Tool.Set_Option('UTM_ZONE', UTM_ZONE)
        Tool.Set_Option('UTM_SOUTH', UTM_SOUTH)
        return Tool.Execute(Verbose)
    return False

def Single_Coordinate_Transformation(SOURCE_CRS=None, SOURCE_X=None, SOURCE_Y=None, TARGET_CRS=None, TARGET_X=None, TARGET_Y=None, Verbose=2):
    '''
    Single Coordinate Transformation
    ----------
    [pj_proj4.29]\n
    Transformation of a single coordinate. Projections can be defined in different formats. Supported formats are:\n
    (-) proj strings\n
    (-) WKT strings\n
    (-) object codes (e.g. "EPSG:4326", "ESRI:31493", "urn:ogc:def:crs:EPSG::4326", "urn:ogc:def:coordinateOperation:EPSG::1671")\n
    (-) object names (e.g. "WGS 84", "WGS 84 / UTM zone 31N", "Germany_Zone_3". In this case as uniqueness is not guaranteed, heuristics are applied to determine the appropriate best match.\n
    (-) OGC URN combining references for compound CRS (e.g "urn:ogc:def:crs,crs:EPSG::2393,crs:EPSG::5717" or custom abbreviated syntax "EPSG:2393+5717")\n
    (-) OGC URN combining references for concatenated operations (e.g. "urn:ogc:def:coordinateOperation,coordinateOperation:EPSG::3895,coordinateOperation:EPSG::1618")\n
    (-) PROJJSON strings (find the jsonschema at [proj.org](https://proj.org/schemas/v0.4/projjson.schema.json))\n
    (-) compound CRS made from two object names separated with " + " (e.g. "WGS 84 + EGM96 height")\n
    Projection routines make use of the PROJ generic coordinate transformation software.\n
    PROJ Version is 9.4.1\n
    [PROJ Homepage](https://proj.org)\n
    Arguments
    ----------
    - SOURCE_CRS [`text`] : Projection. Default: EPSG:4326
    - SOURCE_X [`floating point number`] : X. Default: 0.000000
    - SOURCE_Y [`floating point number`] : Y. Default: 0.000000
    - TARGET_CRS [`text`] : Projection. Default: EPSG:4326
    - TARGET_X [`floating point number`] : X. Default: 0.000000
    - TARGET_Y [`floating point number`] : Y. Default: 0.000000

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('pj_proj4', '29', 'Single Coordinate Transformation')
    if Tool.is_Okay():
        Tool.Set_Option('SOURCE_CRS', SOURCE_CRS)
        Tool.Set_Option('SOURCE_X', SOURCE_X)
        Tool.Set_Option('SOURCE_Y', SOURCE_Y)
        Tool.Set_Option('TARGET_CRS', TARGET_CRS)
        Tool.Set_Option('TARGET_X', TARGET_X)
        Tool.Set_Option('TARGET_Y', TARGET_Y)
        return Tool.Execute(Verbose)
    return False

def run_tool_pj_proj4_29(SOURCE_CRS=None, SOURCE_X=None, SOURCE_Y=None, TARGET_CRS=None, TARGET_X=None, TARGET_Y=None, Verbose=2):
    '''
    Single Coordinate Transformation
    ----------
    [pj_proj4.29]\n
    Transformation of a single coordinate. Projections can be defined in different formats. Supported formats are:\n
    (-) proj strings\n
    (-) WKT strings\n
    (-) object codes (e.g. "EPSG:4326", "ESRI:31493", "urn:ogc:def:crs:EPSG::4326", "urn:ogc:def:coordinateOperation:EPSG::1671")\n
    (-) object names (e.g. "WGS 84", "WGS 84 / UTM zone 31N", "Germany_Zone_3". In this case as uniqueness is not guaranteed, heuristics are applied to determine the appropriate best match.\n
    (-) OGC URN combining references for compound CRS (e.g "urn:ogc:def:crs,crs:EPSG::2393,crs:EPSG::5717" or custom abbreviated syntax "EPSG:2393+5717")\n
    (-) OGC URN combining references for concatenated operations (e.g. "urn:ogc:def:coordinateOperation,coordinateOperation:EPSG::3895,coordinateOperation:EPSG::1618")\n
    (-) PROJJSON strings (find the jsonschema at [proj.org](https://proj.org/schemas/v0.4/projjson.schema.json))\n
    (-) compound CRS made from two object names separated with " + " (e.g. "WGS 84 + EGM96 height")\n
    Projection routines make use of the PROJ generic coordinate transformation software.\n
    PROJ Version is 9.4.1\n
    [PROJ Homepage](https://proj.org)\n
    Arguments
    ----------
    - SOURCE_CRS [`text`] : Projection. Default: EPSG:4326
    - SOURCE_X [`floating point number`] : X. Default: 0.000000
    - SOURCE_Y [`floating point number`] : Y. Default: 0.000000
    - TARGET_CRS [`text`] : Projection. Default: EPSG:4326
    - TARGET_X [`floating point number`] : X. Default: 0.000000
    - TARGET_Y [`floating point number`] : Y. Default: 0.000000

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('pj_proj4', '29', 'Single Coordinate Transformation')
    if Tool.is_Okay():
        Tool.Set_Option('SOURCE_CRS', SOURCE_CRS)
        Tool.Set_Option('SOURCE_X', SOURCE_X)
        Tool.Set_Option('SOURCE_Y', SOURCE_Y)
        Tool.Set_Option('TARGET_CRS', TARGET_CRS)
        Tool.Set_Option('TARGET_X', TARGET_X)
        Tool.Set_Option('TARGET_Y', TARGET_Y)
        return Tool.Execute(Verbose)
    return False

def Coordinate_Conversion_Grids(SOURCE_X=None, SOURCE_Y=None, TARGET_X=None, TARGET_Y=None, SOURCE_CRS_CRS_STRING=None, SOURCE_CRS_CRS_FILE=None, TARGET_CRS_CRS_STRING=None, TARGET_CRS_CRS_FILE=None, Verbose=2):
    '''
    Coordinate Conversion (Grids)
    ----------
    [pj_proj4.30]\n
    This tool projects coordinate tuples. Coordinate tuples have to be provided by the two source coordinate grids.\n
    Projection routines make use of the PROJ generic coordinate transformation software.\n
    PROJ Version is 9.4.1\n
    [PROJ Homepage](https://proj.org)\n
    Arguments
    ----------
    - SOURCE_X [`input grid`] : X Coordinate Source. Grid that provides the source X coordinates.
    - SOURCE_Y [`input grid`] : Y Coordinate Source. Grid that provides the source Y coordinates.
    - TARGET_X [`output grid`] : Projected X Coordinates
    - TARGET_Y [`output grid`] : Projected Y Coordinates
    - SOURCE_CRS_CRS_STRING [`text`] : Definition String. Default: +proj=longlat +datum=WGS84 +no_defs Supported formats comprise PROJ and WKT strings, object codes (e.g. "EPSG:4326").
    - SOURCE_CRS_CRS_FILE [`file path`] : Well Known Text File
    - TARGET_CRS_CRS_STRING [`text`] : Definition String. Default: +proj=longlat +datum=WGS84 +no_defs Supported formats comprise PROJ and WKT strings, object codes (e.g. "EPSG:4326").
    - TARGET_CRS_CRS_FILE [`file path`] : Well Known Text File

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('pj_proj4', '30', 'Coordinate Conversion (Grids)')
    if Tool.is_Okay():
        Tool.Set_Input ('SOURCE_X', SOURCE_X)
        Tool.Set_Input ('SOURCE_Y', SOURCE_Y)
        Tool.Set_Output('TARGET_X', TARGET_X)
        Tool.Set_Output('TARGET_Y', TARGET_Y)
        Tool.Set_Option('SOURCE_CRS.CRS_STRING', SOURCE_CRS_CRS_STRING)
        Tool.Set_Option('SOURCE_CRS.CRS_FILE', SOURCE_CRS_CRS_FILE)
        Tool.Set_Option('TARGET_CRS.CRS_STRING', TARGET_CRS_CRS_STRING)
        Tool.Set_Option('TARGET_CRS.CRS_FILE', TARGET_CRS_CRS_FILE)
        return Tool.Execute(Verbose)
    return False

def run_tool_pj_proj4_30(SOURCE_X=None, SOURCE_Y=None, TARGET_X=None, TARGET_Y=None, SOURCE_CRS_CRS_STRING=None, SOURCE_CRS_CRS_FILE=None, TARGET_CRS_CRS_STRING=None, TARGET_CRS_CRS_FILE=None, Verbose=2):
    '''
    Coordinate Conversion (Grids)
    ----------
    [pj_proj4.30]\n
    This tool projects coordinate tuples. Coordinate tuples have to be provided by the two source coordinate grids.\n
    Projection routines make use of the PROJ generic coordinate transformation software.\n
    PROJ Version is 9.4.1\n
    [PROJ Homepage](https://proj.org)\n
    Arguments
    ----------
    - SOURCE_X [`input grid`] : X Coordinate Source. Grid that provides the source X coordinates.
    - SOURCE_Y [`input grid`] : Y Coordinate Source. Grid that provides the source Y coordinates.
    - TARGET_X [`output grid`] : Projected X Coordinates
    - TARGET_Y [`output grid`] : Projected Y Coordinates
    - SOURCE_CRS_CRS_STRING [`text`] : Definition String. Default: +proj=longlat +datum=WGS84 +no_defs Supported formats comprise PROJ and WKT strings, object codes (e.g. "EPSG:4326").
    - SOURCE_CRS_CRS_FILE [`file path`] : Well Known Text File
    - TARGET_CRS_CRS_STRING [`text`] : Definition String. Default: +proj=longlat +datum=WGS84 +no_defs Supported formats comprise PROJ and WKT strings, object codes (e.g. "EPSG:4326").
    - TARGET_CRS_CRS_FILE [`file path`] : Well Known Text File

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('pj_proj4', '30', 'Coordinate Conversion (Grids)')
    if Tool.is_Okay():
        Tool.Set_Input ('SOURCE_X', SOURCE_X)
        Tool.Set_Input ('SOURCE_Y', SOURCE_Y)
        Tool.Set_Output('TARGET_X', TARGET_X)
        Tool.Set_Output('TARGET_Y', TARGET_Y)
        Tool.Set_Option('SOURCE_CRS.CRS_STRING', SOURCE_CRS_CRS_STRING)
        Tool.Set_Option('SOURCE_CRS.CRS_FILE', SOURCE_CRS_CRS_FILE)
        Tool.Set_Option('TARGET_CRS.CRS_STRING', TARGET_CRS_CRS_STRING)
        Tool.Set_Option('TARGET_CRS.CRS_FILE', TARGET_CRS_CRS_FILE)
        return Tool.Execute(Verbose)
    return False

def Coordinate_Conversion_Table(TABLE=None, SOURCE_X=None, SOURCE_Y=None, TARGET_X=None, TARGET_Y=None, SOURCE_CRS_CRS_STRING=None, SOURCE_CRS_CRS_FILE=None, TARGET_CRS_CRS_STRING=None, TARGET_CRS_CRS_FILE=None, Verbose=2):
    '''
    Coordinate Conversion (Table)
    ----------
    [pj_proj4.31]\n
    This tool projects coordinate tuples. Coordinate tuples have to be provided by the two source coordinate fields.\n
    Projection routines make use of the PROJ generic coordinate transformation software.\n
    PROJ Version is 9.4.1\n
    [PROJ Homepage](https://proj.org)\n
    Arguments
    ----------
    - TABLE [`input table`] : Table
    - SOURCE_X [`table field`] : X Coordinate Source. Table field that provides the source X coordinates.
    - SOURCE_Y [`table field`] : Y Coordinate Source. Table field that provides the source Y coordinates.
    - TARGET_X [`table field`] : Projected X Coordinates
    - TARGET_Y [`table field`] : Projected Y Coordinates
    - SOURCE_CRS_CRS_STRING [`text`] : Definition String. Default: +proj=longlat +datum=WGS84 +no_defs Supported formats comprise PROJ and WKT strings, object codes (e.g. "EPSG:4326").
    - SOURCE_CRS_CRS_FILE [`file path`] : Well Known Text File
    - TARGET_CRS_CRS_STRING [`text`] : Definition String. Default: +proj=longlat +datum=WGS84 +no_defs Supported formats comprise PROJ and WKT strings, object codes (e.g. "EPSG:4326").
    - TARGET_CRS_CRS_FILE [`file path`] : Well Known Text File

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('pj_proj4', '31', 'Coordinate Conversion (Table)')
    if Tool.is_Okay():
        Tool.Set_Input ('TABLE', TABLE)
        Tool.Set_Option('SOURCE_X', SOURCE_X)
        Tool.Set_Option('SOURCE_Y', SOURCE_Y)
        Tool.Set_Option('TARGET_X', TARGET_X)
        Tool.Set_Option('TARGET_Y', TARGET_Y)
        Tool.Set_Option('SOURCE_CRS.CRS_STRING', SOURCE_CRS_CRS_STRING)
        Tool.Set_Option('SOURCE_CRS.CRS_FILE', SOURCE_CRS_CRS_FILE)
        Tool.Set_Option('TARGET_CRS.CRS_STRING', TARGET_CRS_CRS_STRING)
        Tool.Set_Option('TARGET_CRS.CRS_FILE', TARGET_CRS_CRS_FILE)
        return Tool.Execute(Verbose)
    return False

def run_tool_pj_proj4_31(TABLE=None, SOURCE_X=None, SOURCE_Y=None, TARGET_X=None, TARGET_Y=None, SOURCE_CRS_CRS_STRING=None, SOURCE_CRS_CRS_FILE=None, TARGET_CRS_CRS_STRING=None, TARGET_CRS_CRS_FILE=None, Verbose=2):
    '''
    Coordinate Conversion (Table)
    ----------
    [pj_proj4.31]\n
    This tool projects coordinate tuples. Coordinate tuples have to be provided by the two source coordinate fields.\n
    Projection routines make use of the PROJ generic coordinate transformation software.\n
    PROJ Version is 9.4.1\n
    [PROJ Homepage](https://proj.org)\n
    Arguments
    ----------
    - TABLE [`input table`] : Table
    - SOURCE_X [`table field`] : X Coordinate Source. Table field that provides the source X coordinates.
    - SOURCE_Y [`table field`] : Y Coordinate Source. Table field that provides the source Y coordinates.
    - TARGET_X [`table field`] : Projected X Coordinates
    - TARGET_Y [`table field`] : Projected Y Coordinates
    - SOURCE_CRS_CRS_STRING [`text`] : Definition String. Default: +proj=longlat +datum=WGS84 +no_defs Supported formats comprise PROJ and WKT strings, object codes (e.g. "EPSG:4326").
    - SOURCE_CRS_CRS_FILE [`file path`] : Well Known Text File
    - TARGET_CRS_CRS_STRING [`text`] : Definition String. Default: +proj=longlat +datum=WGS84 +no_defs Supported formats comprise PROJ and WKT strings, object codes (e.g. "EPSG:4326").
    - TARGET_CRS_CRS_FILE [`file path`] : Well Known Text File

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('pj_proj4', '31', 'Coordinate Conversion (Table)')
    if Tool.is_Okay():
        Tool.Set_Input ('TABLE', TABLE)
        Tool.Set_Option('SOURCE_X', SOURCE_X)
        Tool.Set_Option('SOURCE_Y', SOURCE_Y)
        Tool.Set_Option('TARGET_X', TARGET_X)
        Tool.Set_Option('TARGET_Y', TARGET_Y)
        Tool.Set_Option('SOURCE_CRS.CRS_STRING', SOURCE_CRS_CRS_STRING)
        Tool.Set_Option('SOURCE_CRS.CRS_FILE', SOURCE_CRS_CRS_FILE)
        Tool.Set_Option('TARGET_CRS.CRS_STRING', TARGET_CRS_CRS_STRING)
        Tool.Set_Option('TARGET_CRS.CRS_FILE', TARGET_CRS_CRS_FILE)
        return Tool.Execute(Verbose)
    return False

def Globe_Gores(GRID=None, GORES=None, NUMBER=None, BLEED=None, RESOLUTION=None, BYTEWISE=None, Verbose=2):
    '''
    Globe Gores
    ----------
    [pj_proj4.32]\n
    With this tool you can easily create globe gores from a (global) grid data set. The tool uses a polyconic projection for each gore and plots all gores side by side into one target grid. Target grid resolution is specified as number of pixels (cells) in meridional direction. The target grid is not suitable for further geoprocessing, but is thought to serve as print template for the creation of globes, i.e. to become glued onto a ball.\n
    Arguments
    ----------
    - GRID [`input grid`] : Grid
    - GORES [`output data object`] : Gores
    - NUMBER [`integer number`] : Number of Gores. Minimum: 2 Maximum: 360 Default: 12
    - BLEED [`floating point number`] : Bleed. Minimum: 0.000000 Maximum: 10.000000 Default: 0.000000 Overlap of gores given as percentage of the gore width.
    - RESOLUTION [`integer number`] : Resolution. Minimum: 100 Default: 1000 Number of cells/pixels from pole to pole (i.e. in North-South direction).
    - BYTEWISE [`boolean`] : Bytewise Interpolation. Default: 0 To be used for RGB and CMYK coded values (i.e. images).

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('pj_proj4', '32', 'Globe Gores')
    if Tool.is_Okay():
        Tool.Set_Input ('GRID', GRID)
        Tool.Set_Output('GORES', GORES)
        Tool.Set_Option('NUMBER', NUMBER)
        Tool.Set_Option('BLEED', BLEED)
        Tool.Set_Option('RESOLUTION', RESOLUTION)
        Tool.Set_Option('BYTEWISE', BYTEWISE)
        return Tool.Execute(Verbose)
    return False

def run_tool_pj_proj4_32(GRID=None, GORES=None, NUMBER=None, BLEED=None, RESOLUTION=None, BYTEWISE=None, Verbose=2):
    '''
    Globe Gores
    ----------
    [pj_proj4.32]\n
    With this tool you can easily create globe gores from a (global) grid data set. The tool uses a polyconic projection for each gore and plots all gores side by side into one target grid. Target grid resolution is specified as number of pixels (cells) in meridional direction. The target grid is not suitable for further geoprocessing, but is thought to serve as print template for the creation of globes, i.e. to become glued onto a ball.\n
    Arguments
    ----------
    - GRID [`input grid`] : Grid
    - GORES [`output data object`] : Gores
    - NUMBER [`integer number`] : Number of Gores. Minimum: 2 Maximum: 360 Default: 12
    - BLEED [`floating point number`] : Bleed. Minimum: 0.000000 Maximum: 10.000000 Default: 0.000000 Overlap of gores given as percentage of the gore width.
    - RESOLUTION [`integer number`] : Resolution. Minimum: 100 Default: 1000 Number of cells/pixels from pole to pole (i.e. in North-South direction).
    - BYTEWISE [`boolean`] : Bytewise Interpolation. Default: 0 To be used for RGB and CMYK coded values (i.e. images).

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('pj_proj4', '32', 'Globe Gores')
    if Tool.is_Okay():
        Tool.Set_Input ('GRID', GRID)
        Tool.Set_Output('GORES', GORES)
        Tool.Set_Option('NUMBER', NUMBER)
        Tool.Set_Option('BLEED', BLEED)
        Tool.Set_Option('RESOLUTION', RESOLUTION)
        Tool.Set_Option('BYTEWISE', BYTEWISE)
        return Tool.Execute(Verbose)
    return False

