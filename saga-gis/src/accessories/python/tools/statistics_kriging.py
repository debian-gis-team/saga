#! /usr/bin/env python

'''
Python Interface to SAGA Tools Library
----------
- Category : Spatial and Geostatistics
- Name     : Kriging
- ID       : statistics_kriging

Description
----------
Kriging - tools for the geostatistical interpolation of irregularly distributed point data.
'''

from PySAGA.helper import Tool_Wrapper

def Ordinary_Kriging(POINTS=None, TARGET_TEMPLATE=None, PREDICTION=None, VARIANCE=None, CV_SUMMARY=None, CV_RESIDUALS=None, FIELD=None, TARGET_DEFINITION=None, TARGET_USER_SIZE=None, TARGET_USER_XMIN=None, TARGET_USER_XMAX=None, TARGET_USER_YMIN=None, TARGET_USER_YMAX=None, TARGET_USER_COLS=None, TARGET_USER_ROWS=None, TARGET_USER_FLAT=None, TARGET_USER_FITS=None, TARGET_SYSTEM=None, TQUALITY=None, VAR_MAXDIST=None, VAR_NCLASSES=None, VAR_NSKIP=None, VAR_MODEL=None, LOG=None, BLOCK=None, DBLOCK=None, CV_METHOD=None, CV_SAMPLES=None, SEARCH_RANGE=None, SEARCH_RADIUS=None, SEARCH_POINTS_ALL=None, SEARCH_POINTS_MIN=None, SEARCH_POINTS_MAX=None, Verbose=2):
    '''
    Ordinary Kriging
    ----------
    [statistics_kriging.0]\n
    Ordinary Kriging for grid interpolation from irregular sample points.\n
    Arguments
    ----------
    - POINTS [`input shapes`] : Points
    - TARGET_TEMPLATE [`optional input grid`] : Target System. use this grid's system for output grids
    - PREDICTION [`output grid`] : Prediction
    - VARIANCE [`output grid`] : Prediction Error
    - CV_SUMMARY [`output table`] : Cross Validation Summary
    - CV_RESIDUALS [`output shapes`] : Cross Validation Residuals
    - FIELD [`table field`] : Attribute
    - TARGET_DEFINITION [`choice`] : Target Grid System. Available Choices: [0] user defined [1] grid or grid system Default: 0
    - TARGET_USER_SIZE [`floating point number`] : Cellsize. Minimum: 0.000000 Default: 1.000000
    - TARGET_USER_XMIN [`floating point number`] : West. Default: 0.000000
    - TARGET_USER_XMAX [`floating point number`] : East. Default: 100.000000
    - TARGET_USER_YMIN [`floating point number`] : South. Default: 0.000000
    - TARGET_USER_YMAX [`floating point number`] : North. Default: 100.000000
    - TARGET_USER_COLS [`integer number`] : Columns. Minimum: 1 Default: 101 Number of cells in East-West direction.
    - TARGET_USER_ROWS [`integer number`] : Rows. Minimum: 1 Default: 101 Number of cells in North-South direction.
    - TARGET_USER_FLAT [`boolean`] : Rounding. Default: 1 Round bounding coordinates to multiples of cell size. Ignored if cell size has decimal places.
    - TARGET_USER_FITS [`choice`] : Fit. Available Choices: [0] nodes [1] cells Default: 0
    - TARGET_SYSTEM [`grid system`] : Grid System
    - TQUALITY [`choice`] : Error Measure. Available Choices: [0] Standard Deviation [1] Variance Default: 1
    - VAR_MAXDIST [`floating point number`] : Maximum Distance. Minimum: 0.000000 Default: 0.000000 maximum distance for variogram estimation, ignored if set to zero
    - VAR_NCLASSES [`integer number`] : Lag Distance Classes. Minimum: 1 Default: 100 initial number of lag distance classes
    - VAR_NSKIP [`integer number`] : Skip. Minimum: 1 Default: 1
    - VAR_MODEL [`text`] : Model. Default: b * x
    - LOG [`boolean`] : Logarithmic Transformation. Default: 0
    - BLOCK [`boolean`] : Block Kriging. Default: 0
    - DBLOCK [`floating point number`] : Block Size. Minimum: 0.000000 Default: 100.000000 Edge length [map units]
    - CV_METHOD [`choice`] : Cross Validation. Available Choices: [0] none [1] leave one out [2] 2-fold [3] k-fold Default: 0
    - CV_SAMPLES [`integer number`] : Cross Validation Subsamples. Minimum: 2 Default: 10 number of subsamples for k-fold cross validation
    - SEARCH_RANGE [`choice`] : Search Range. Available Choices: [0] local [1] global Default: 1
    - SEARCH_RADIUS [`floating point number`] : Maximum Search Distance. Minimum: 0.000000 Default: 1000.000000 local maximum search distance given in map units
    - SEARCH_POINTS_ALL [`choice`] : Number of Points. Available Choices: [0] maximum number of nearest points [1] all points within search distance Default: 1
    - SEARCH_POINTS_MIN [`integer number`] : Minimum. Minimum: 1 Default: 16 minimum number of points to use
    - SEARCH_POINTS_MAX [`integer number`] : Maximum. Minimum: 1 Default: 20 maximum number of nearest points

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('statistics_kriging', '0', 'Ordinary Kriging')
    if Tool.is_Okay():
        Tool.Set_Input ('POINTS', POINTS)
        Tool.Set_Input ('TARGET_TEMPLATE', TARGET_TEMPLATE)
        Tool.Set_Output('PREDICTION', PREDICTION)
        Tool.Set_Output('VARIANCE', VARIANCE)
        Tool.Set_Output('CV_SUMMARY', CV_SUMMARY)
        Tool.Set_Output('CV_RESIDUALS', CV_RESIDUALS)
        Tool.Set_Option('FIELD', FIELD)
        Tool.Set_Option('TARGET_DEFINITION', TARGET_DEFINITION)
        Tool.Set_Option('TARGET_USER_SIZE', TARGET_USER_SIZE)
        Tool.Set_Option('TARGET_USER_XMIN', TARGET_USER_XMIN)
        Tool.Set_Option('TARGET_USER_XMAX', TARGET_USER_XMAX)
        Tool.Set_Option('TARGET_USER_YMIN', TARGET_USER_YMIN)
        Tool.Set_Option('TARGET_USER_YMAX', TARGET_USER_YMAX)
        Tool.Set_Option('TARGET_USER_COLS', TARGET_USER_COLS)
        Tool.Set_Option('TARGET_USER_ROWS', TARGET_USER_ROWS)
        Tool.Set_Option('TARGET_USER_FLAT', TARGET_USER_FLAT)
        Tool.Set_Option('TARGET_USER_FITS', TARGET_USER_FITS)
        Tool.Set_Option('TARGET_SYSTEM', TARGET_SYSTEM)
        Tool.Set_Option('TQUALITY', TQUALITY)
        Tool.Set_Option('VAR_MAXDIST', VAR_MAXDIST)
        Tool.Set_Option('VAR_NCLASSES', VAR_NCLASSES)
        Tool.Set_Option('VAR_NSKIP', VAR_NSKIP)
        Tool.Set_Option('VAR_MODEL', VAR_MODEL)
        Tool.Set_Option('LOG', LOG)
        Tool.Set_Option('BLOCK', BLOCK)
        Tool.Set_Option('DBLOCK', DBLOCK)
        Tool.Set_Option('CV_METHOD', CV_METHOD)
        Tool.Set_Option('CV_SAMPLES', CV_SAMPLES)
        Tool.Set_Option('SEARCH_RANGE', SEARCH_RANGE)
        Tool.Set_Option('SEARCH_RADIUS', SEARCH_RADIUS)
        Tool.Set_Option('SEARCH_POINTS_ALL', SEARCH_POINTS_ALL)
        Tool.Set_Option('SEARCH_POINTS_MIN', SEARCH_POINTS_MIN)
        Tool.Set_Option('SEARCH_POINTS_MAX', SEARCH_POINTS_MAX)
        return Tool.Execute(Verbose)
    return False

def run_tool_statistics_kriging_0(POINTS=None, TARGET_TEMPLATE=None, PREDICTION=None, VARIANCE=None, CV_SUMMARY=None, CV_RESIDUALS=None, FIELD=None, TARGET_DEFINITION=None, TARGET_USER_SIZE=None, TARGET_USER_XMIN=None, TARGET_USER_XMAX=None, TARGET_USER_YMIN=None, TARGET_USER_YMAX=None, TARGET_USER_COLS=None, TARGET_USER_ROWS=None, TARGET_USER_FLAT=None, TARGET_USER_FITS=None, TARGET_SYSTEM=None, TQUALITY=None, VAR_MAXDIST=None, VAR_NCLASSES=None, VAR_NSKIP=None, VAR_MODEL=None, LOG=None, BLOCK=None, DBLOCK=None, CV_METHOD=None, CV_SAMPLES=None, SEARCH_RANGE=None, SEARCH_RADIUS=None, SEARCH_POINTS_ALL=None, SEARCH_POINTS_MIN=None, SEARCH_POINTS_MAX=None, Verbose=2):
    '''
    Ordinary Kriging
    ----------
    [statistics_kriging.0]\n
    Ordinary Kriging for grid interpolation from irregular sample points.\n
    Arguments
    ----------
    - POINTS [`input shapes`] : Points
    - TARGET_TEMPLATE [`optional input grid`] : Target System. use this grid's system for output grids
    - PREDICTION [`output grid`] : Prediction
    - VARIANCE [`output grid`] : Prediction Error
    - CV_SUMMARY [`output table`] : Cross Validation Summary
    - CV_RESIDUALS [`output shapes`] : Cross Validation Residuals
    - FIELD [`table field`] : Attribute
    - TARGET_DEFINITION [`choice`] : Target Grid System. Available Choices: [0] user defined [1] grid or grid system Default: 0
    - TARGET_USER_SIZE [`floating point number`] : Cellsize. Minimum: 0.000000 Default: 1.000000
    - TARGET_USER_XMIN [`floating point number`] : West. Default: 0.000000
    - TARGET_USER_XMAX [`floating point number`] : East. Default: 100.000000
    - TARGET_USER_YMIN [`floating point number`] : South. Default: 0.000000
    - TARGET_USER_YMAX [`floating point number`] : North. Default: 100.000000
    - TARGET_USER_COLS [`integer number`] : Columns. Minimum: 1 Default: 101 Number of cells in East-West direction.
    - TARGET_USER_ROWS [`integer number`] : Rows. Minimum: 1 Default: 101 Number of cells in North-South direction.
    - TARGET_USER_FLAT [`boolean`] : Rounding. Default: 1 Round bounding coordinates to multiples of cell size. Ignored if cell size has decimal places.
    - TARGET_USER_FITS [`choice`] : Fit. Available Choices: [0] nodes [1] cells Default: 0
    - TARGET_SYSTEM [`grid system`] : Grid System
    - TQUALITY [`choice`] : Error Measure. Available Choices: [0] Standard Deviation [1] Variance Default: 1
    - VAR_MAXDIST [`floating point number`] : Maximum Distance. Minimum: 0.000000 Default: 0.000000 maximum distance for variogram estimation, ignored if set to zero
    - VAR_NCLASSES [`integer number`] : Lag Distance Classes. Minimum: 1 Default: 100 initial number of lag distance classes
    - VAR_NSKIP [`integer number`] : Skip. Minimum: 1 Default: 1
    - VAR_MODEL [`text`] : Model. Default: b * x
    - LOG [`boolean`] : Logarithmic Transformation. Default: 0
    - BLOCK [`boolean`] : Block Kriging. Default: 0
    - DBLOCK [`floating point number`] : Block Size. Minimum: 0.000000 Default: 100.000000 Edge length [map units]
    - CV_METHOD [`choice`] : Cross Validation. Available Choices: [0] none [1] leave one out [2] 2-fold [3] k-fold Default: 0
    - CV_SAMPLES [`integer number`] : Cross Validation Subsamples. Minimum: 2 Default: 10 number of subsamples for k-fold cross validation
    - SEARCH_RANGE [`choice`] : Search Range. Available Choices: [0] local [1] global Default: 1
    - SEARCH_RADIUS [`floating point number`] : Maximum Search Distance. Minimum: 0.000000 Default: 1000.000000 local maximum search distance given in map units
    - SEARCH_POINTS_ALL [`choice`] : Number of Points. Available Choices: [0] maximum number of nearest points [1] all points within search distance Default: 1
    - SEARCH_POINTS_MIN [`integer number`] : Minimum. Minimum: 1 Default: 16 minimum number of points to use
    - SEARCH_POINTS_MAX [`integer number`] : Maximum. Minimum: 1 Default: 20 maximum number of nearest points

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('statistics_kriging', '0', 'Ordinary Kriging')
    if Tool.is_Okay():
        Tool.Set_Input ('POINTS', POINTS)
        Tool.Set_Input ('TARGET_TEMPLATE', TARGET_TEMPLATE)
        Tool.Set_Output('PREDICTION', PREDICTION)
        Tool.Set_Output('VARIANCE', VARIANCE)
        Tool.Set_Output('CV_SUMMARY', CV_SUMMARY)
        Tool.Set_Output('CV_RESIDUALS', CV_RESIDUALS)
        Tool.Set_Option('FIELD', FIELD)
        Tool.Set_Option('TARGET_DEFINITION', TARGET_DEFINITION)
        Tool.Set_Option('TARGET_USER_SIZE', TARGET_USER_SIZE)
        Tool.Set_Option('TARGET_USER_XMIN', TARGET_USER_XMIN)
        Tool.Set_Option('TARGET_USER_XMAX', TARGET_USER_XMAX)
        Tool.Set_Option('TARGET_USER_YMIN', TARGET_USER_YMIN)
        Tool.Set_Option('TARGET_USER_YMAX', TARGET_USER_YMAX)
        Tool.Set_Option('TARGET_USER_COLS', TARGET_USER_COLS)
        Tool.Set_Option('TARGET_USER_ROWS', TARGET_USER_ROWS)
        Tool.Set_Option('TARGET_USER_FLAT', TARGET_USER_FLAT)
        Tool.Set_Option('TARGET_USER_FITS', TARGET_USER_FITS)
        Tool.Set_Option('TARGET_SYSTEM', TARGET_SYSTEM)
        Tool.Set_Option('TQUALITY', TQUALITY)
        Tool.Set_Option('VAR_MAXDIST', VAR_MAXDIST)
        Tool.Set_Option('VAR_NCLASSES', VAR_NCLASSES)
        Tool.Set_Option('VAR_NSKIP', VAR_NSKIP)
        Tool.Set_Option('VAR_MODEL', VAR_MODEL)
        Tool.Set_Option('LOG', LOG)
        Tool.Set_Option('BLOCK', BLOCK)
        Tool.Set_Option('DBLOCK', DBLOCK)
        Tool.Set_Option('CV_METHOD', CV_METHOD)
        Tool.Set_Option('CV_SAMPLES', CV_SAMPLES)
        Tool.Set_Option('SEARCH_RANGE', SEARCH_RANGE)
        Tool.Set_Option('SEARCH_RADIUS', SEARCH_RADIUS)
        Tool.Set_Option('SEARCH_POINTS_ALL', SEARCH_POINTS_ALL)
        Tool.Set_Option('SEARCH_POINTS_MIN', SEARCH_POINTS_MIN)
        Tool.Set_Option('SEARCH_POINTS_MAX', SEARCH_POINTS_MAX)
        return Tool.Execute(Verbose)
    return False

def Simple_Kriging(POINTS=None, TARGET_TEMPLATE=None, PREDICTION=None, VARIANCE=None, CV_SUMMARY=None, CV_RESIDUALS=None, FIELD=None, TARGET_DEFINITION=None, TARGET_USER_SIZE=None, TARGET_USER_XMIN=None, TARGET_USER_XMAX=None, TARGET_USER_YMIN=None, TARGET_USER_YMAX=None, TARGET_USER_COLS=None, TARGET_USER_ROWS=None, TARGET_USER_FLAT=None, TARGET_USER_FITS=None, TARGET_SYSTEM=None, TQUALITY=None, VAR_MAXDIST=None, VAR_NCLASSES=None, VAR_NSKIP=None, VAR_MODEL=None, LOG=None, BLOCK=None, DBLOCK=None, CV_METHOD=None, CV_SAMPLES=None, SEARCH_RANGE=None, SEARCH_RADIUS=None, SEARCH_POINTS_ALL=None, SEARCH_POINTS_MIN=None, SEARCH_POINTS_MAX=None, Verbose=2):
    '''
    Simple Kriging
    ----------
    [statistics_kriging.1]\n
    Simple Kriging for grid interpolation from irregular sample points.\n
    Arguments
    ----------
    - POINTS [`input shapes`] : Points
    - TARGET_TEMPLATE [`optional input grid`] : Target System. use this grid's system for output grids
    - PREDICTION [`output grid`] : Prediction
    - VARIANCE [`output grid`] : Prediction Error
    - CV_SUMMARY [`output table`] : Cross Validation Summary
    - CV_RESIDUALS [`output shapes`] : Cross Validation Residuals
    - FIELD [`table field`] : Attribute
    - TARGET_DEFINITION [`choice`] : Target Grid System. Available Choices: [0] user defined [1] grid or grid system Default: 0
    - TARGET_USER_SIZE [`floating point number`] : Cellsize. Minimum: 0.000000 Default: 1.000000
    - TARGET_USER_XMIN [`floating point number`] : West. Default: 0.000000
    - TARGET_USER_XMAX [`floating point number`] : East. Default: 100.000000
    - TARGET_USER_YMIN [`floating point number`] : South. Default: 0.000000
    - TARGET_USER_YMAX [`floating point number`] : North. Default: 100.000000
    - TARGET_USER_COLS [`integer number`] : Columns. Minimum: 1 Default: 101 Number of cells in East-West direction.
    - TARGET_USER_ROWS [`integer number`] : Rows. Minimum: 1 Default: 101 Number of cells in North-South direction.
    - TARGET_USER_FLAT [`boolean`] : Rounding. Default: 1 Round bounding coordinates to multiples of cell size. Ignored if cell size has decimal places.
    - TARGET_USER_FITS [`choice`] : Fit. Available Choices: [0] nodes [1] cells Default: 0
    - TARGET_SYSTEM [`grid system`] : Grid System
    - TQUALITY [`choice`] : Error Measure. Available Choices: [0] Standard Deviation [1] Variance Default: 1
    - VAR_MAXDIST [`floating point number`] : Maximum Distance. Minimum: 0.000000 Default: 0.000000 maximum distance for variogram estimation, ignored if set to zero
    - VAR_NCLASSES [`integer number`] : Lag Distance Classes. Minimum: 1 Default: 100 initial number of lag distance classes
    - VAR_NSKIP [`integer number`] : Skip. Minimum: 1 Default: 1
    - VAR_MODEL [`text`] : Model. Default: b * x
    - LOG [`boolean`] : Logarithmic Transformation. Default: 0
    - BLOCK [`boolean`] : Block Kriging. Default: 0
    - DBLOCK [`floating point number`] : Block Size. Minimum: 0.000000 Default: 100.000000 Edge length [map units]
    - CV_METHOD [`choice`] : Cross Validation. Available Choices: [0] none [1] leave one out [2] 2-fold [3] k-fold Default: 0
    - CV_SAMPLES [`integer number`] : Cross Validation Subsamples. Minimum: 2 Default: 10 number of subsamples for k-fold cross validation
    - SEARCH_RANGE [`choice`] : Search Range. Available Choices: [0] local [1] global Default: 1
    - SEARCH_RADIUS [`floating point number`] : Maximum Search Distance. Minimum: 0.000000 Default: 1000.000000 local maximum search distance given in map units
    - SEARCH_POINTS_ALL [`choice`] : Number of Points. Available Choices: [0] maximum number of nearest points [1] all points within search distance Default: 1
    - SEARCH_POINTS_MIN [`integer number`] : Minimum. Minimum: 1 Default: 16 minimum number of points to use
    - SEARCH_POINTS_MAX [`integer number`] : Maximum. Minimum: 1 Default: 20 maximum number of nearest points

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('statistics_kriging', '1', 'Simple Kriging')
    if Tool.is_Okay():
        Tool.Set_Input ('POINTS', POINTS)
        Tool.Set_Input ('TARGET_TEMPLATE', TARGET_TEMPLATE)
        Tool.Set_Output('PREDICTION', PREDICTION)
        Tool.Set_Output('VARIANCE', VARIANCE)
        Tool.Set_Output('CV_SUMMARY', CV_SUMMARY)
        Tool.Set_Output('CV_RESIDUALS', CV_RESIDUALS)
        Tool.Set_Option('FIELD', FIELD)
        Tool.Set_Option('TARGET_DEFINITION', TARGET_DEFINITION)
        Tool.Set_Option('TARGET_USER_SIZE', TARGET_USER_SIZE)
        Tool.Set_Option('TARGET_USER_XMIN', TARGET_USER_XMIN)
        Tool.Set_Option('TARGET_USER_XMAX', TARGET_USER_XMAX)
        Tool.Set_Option('TARGET_USER_YMIN', TARGET_USER_YMIN)
        Tool.Set_Option('TARGET_USER_YMAX', TARGET_USER_YMAX)
        Tool.Set_Option('TARGET_USER_COLS', TARGET_USER_COLS)
        Tool.Set_Option('TARGET_USER_ROWS', TARGET_USER_ROWS)
        Tool.Set_Option('TARGET_USER_FLAT', TARGET_USER_FLAT)
        Tool.Set_Option('TARGET_USER_FITS', TARGET_USER_FITS)
        Tool.Set_Option('TARGET_SYSTEM', TARGET_SYSTEM)
        Tool.Set_Option('TQUALITY', TQUALITY)
        Tool.Set_Option('VAR_MAXDIST', VAR_MAXDIST)
        Tool.Set_Option('VAR_NCLASSES', VAR_NCLASSES)
        Tool.Set_Option('VAR_NSKIP', VAR_NSKIP)
        Tool.Set_Option('VAR_MODEL', VAR_MODEL)
        Tool.Set_Option('LOG', LOG)
        Tool.Set_Option('BLOCK', BLOCK)
        Tool.Set_Option('DBLOCK', DBLOCK)
        Tool.Set_Option('CV_METHOD', CV_METHOD)
        Tool.Set_Option('CV_SAMPLES', CV_SAMPLES)
        Tool.Set_Option('SEARCH_RANGE', SEARCH_RANGE)
        Tool.Set_Option('SEARCH_RADIUS', SEARCH_RADIUS)
        Tool.Set_Option('SEARCH_POINTS_ALL', SEARCH_POINTS_ALL)
        Tool.Set_Option('SEARCH_POINTS_MIN', SEARCH_POINTS_MIN)
        Tool.Set_Option('SEARCH_POINTS_MAX', SEARCH_POINTS_MAX)
        return Tool.Execute(Verbose)
    return False

def run_tool_statistics_kriging_1(POINTS=None, TARGET_TEMPLATE=None, PREDICTION=None, VARIANCE=None, CV_SUMMARY=None, CV_RESIDUALS=None, FIELD=None, TARGET_DEFINITION=None, TARGET_USER_SIZE=None, TARGET_USER_XMIN=None, TARGET_USER_XMAX=None, TARGET_USER_YMIN=None, TARGET_USER_YMAX=None, TARGET_USER_COLS=None, TARGET_USER_ROWS=None, TARGET_USER_FLAT=None, TARGET_USER_FITS=None, TARGET_SYSTEM=None, TQUALITY=None, VAR_MAXDIST=None, VAR_NCLASSES=None, VAR_NSKIP=None, VAR_MODEL=None, LOG=None, BLOCK=None, DBLOCK=None, CV_METHOD=None, CV_SAMPLES=None, SEARCH_RANGE=None, SEARCH_RADIUS=None, SEARCH_POINTS_ALL=None, SEARCH_POINTS_MIN=None, SEARCH_POINTS_MAX=None, Verbose=2):
    '''
    Simple Kriging
    ----------
    [statistics_kriging.1]\n
    Simple Kriging for grid interpolation from irregular sample points.\n
    Arguments
    ----------
    - POINTS [`input shapes`] : Points
    - TARGET_TEMPLATE [`optional input grid`] : Target System. use this grid's system for output grids
    - PREDICTION [`output grid`] : Prediction
    - VARIANCE [`output grid`] : Prediction Error
    - CV_SUMMARY [`output table`] : Cross Validation Summary
    - CV_RESIDUALS [`output shapes`] : Cross Validation Residuals
    - FIELD [`table field`] : Attribute
    - TARGET_DEFINITION [`choice`] : Target Grid System. Available Choices: [0] user defined [1] grid or grid system Default: 0
    - TARGET_USER_SIZE [`floating point number`] : Cellsize. Minimum: 0.000000 Default: 1.000000
    - TARGET_USER_XMIN [`floating point number`] : West. Default: 0.000000
    - TARGET_USER_XMAX [`floating point number`] : East. Default: 100.000000
    - TARGET_USER_YMIN [`floating point number`] : South. Default: 0.000000
    - TARGET_USER_YMAX [`floating point number`] : North. Default: 100.000000
    - TARGET_USER_COLS [`integer number`] : Columns. Minimum: 1 Default: 101 Number of cells in East-West direction.
    - TARGET_USER_ROWS [`integer number`] : Rows. Minimum: 1 Default: 101 Number of cells in North-South direction.
    - TARGET_USER_FLAT [`boolean`] : Rounding. Default: 1 Round bounding coordinates to multiples of cell size. Ignored if cell size has decimal places.
    - TARGET_USER_FITS [`choice`] : Fit. Available Choices: [0] nodes [1] cells Default: 0
    - TARGET_SYSTEM [`grid system`] : Grid System
    - TQUALITY [`choice`] : Error Measure. Available Choices: [0] Standard Deviation [1] Variance Default: 1
    - VAR_MAXDIST [`floating point number`] : Maximum Distance. Minimum: 0.000000 Default: 0.000000 maximum distance for variogram estimation, ignored if set to zero
    - VAR_NCLASSES [`integer number`] : Lag Distance Classes. Minimum: 1 Default: 100 initial number of lag distance classes
    - VAR_NSKIP [`integer number`] : Skip. Minimum: 1 Default: 1
    - VAR_MODEL [`text`] : Model. Default: b * x
    - LOG [`boolean`] : Logarithmic Transformation. Default: 0
    - BLOCK [`boolean`] : Block Kriging. Default: 0
    - DBLOCK [`floating point number`] : Block Size. Minimum: 0.000000 Default: 100.000000 Edge length [map units]
    - CV_METHOD [`choice`] : Cross Validation. Available Choices: [0] none [1] leave one out [2] 2-fold [3] k-fold Default: 0
    - CV_SAMPLES [`integer number`] : Cross Validation Subsamples. Minimum: 2 Default: 10 number of subsamples for k-fold cross validation
    - SEARCH_RANGE [`choice`] : Search Range. Available Choices: [0] local [1] global Default: 1
    - SEARCH_RADIUS [`floating point number`] : Maximum Search Distance. Minimum: 0.000000 Default: 1000.000000 local maximum search distance given in map units
    - SEARCH_POINTS_ALL [`choice`] : Number of Points. Available Choices: [0] maximum number of nearest points [1] all points within search distance Default: 1
    - SEARCH_POINTS_MIN [`integer number`] : Minimum. Minimum: 1 Default: 16 minimum number of points to use
    - SEARCH_POINTS_MAX [`integer number`] : Maximum. Minimum: 1 Default: 20 maximum number of nearest points

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('statistics_kriging', '1', 'Simple Kriging')
    if Tool.is_Okay():
        Tool.Set_Input ('POINTS', POINTS)
        Tool.Set_Input ('TARGET_TEMPLATE', TARGET_TEMPLATE)
        Tool.Set_Output('PREDICTION', PREDICTION)
        Tool.Set_Output('VARIANCE', VARIANCE)
        Tool.Set_Output('CV_SUMMARY', CV_SUMMARY)
        Tool.Set_Output('CV_RESIDUALS', CV_RESIDUALS)
        Tool.Set_Option('FIELD', FIELD)
        Tool.Set_Option('TARGET_DEFINITION', TARGET_DEFINITION)
        Tool.Set_Option('TARGET_USER_SIZE', TARGET_USER_SIZE)
        Tool.Set_Option('TARGET_USER_XMIN', TARGET_USER_XMIN)
        Tool.Set_Option('TARGET_USER_XMAX', TARGET_USER_XMAX)
        Tool.Set_Option('TARGET_USER_YMIN', TARGET_USER_YMIN)
        Tool.Set_Option('TARGET_USER_YMAX', TARGET_USER_YMAX)
        Tool.Set_Option('TARGET_USER_COLS', TARGET_USER_COLS)
        Tool.Set_Option('TARGET_USER_ROWS', TARGET_USER_ROWS)
        Tool.Set_Option('TARGET_USER_FLAT', TARGET_USER_FLAT)
        Tool.Set_Option('TARGET_USER_FITS', TARGET_USER_FITS)
        Tool.Set_Option('TARGET_SYSTEM', TARGET_SYSTEM)
        Tool.Set_Option('TQUALITY', TQUALITY)
        Tool.Set_Option('VAR_MAXDIST', VAR_MAXDIST)
        Tool.Set_Option('VAR_NCLASSES', VAR_NCLASSES)
        Tool.Set_Option('VAR_NSKIP', VAR_NSKIP)
        Tool.Set_Option('VAR_MODEL', VAR_MODEL)
        Tool.Set_Option('LOG', LOG)
        Tool.Set_Option('BLOCK', BLOCK)
        Tool.Set_Option('DBLOCK', DBLOCK)
        Tool.Set_Option('CV_METHOD', CV_METHOD)
        Tool.Set_Option('CV_SAMPLES', CV_SAMPLES)
        Tool.Set_Option('SEARCH_RANGE', SEARCH_RANGE)
        Tool.Set_Option('SEARCH_RADIUS', SEARCH_RADIUS)
        Tool.Set_Option('SEARCH_POINTS_ALL', SEARCH_POINTS_ALL)
        Tool.Set_Option('SEARCH_POINTS_MIN', SEARCH_POINTS_MIN)
        Tool.Set_Option('SEARCH_POINTS_MAX', SEARCH_POINTS_MAX)
        return Tool.Execute(Verbose)
    return False

def Universal_Kriging(POINTS=None, TARGET_TEMPLATE=None, PREDICTORS=None, PREDICTION=None, VARIANCE=None, CV_SUMMARY=None, CV_RESIDUALS=None, FIELD=None, TARGET_DEFINITION=None, TARGET_USER_SIZE=None, TARGET_USER_XMIN=None, TARGET_USER_XMAX=None, TARGET_USER_YMIN=None, TARGET_USER_YMAX=None, TARGET_USER_COLS=None, TARGET_USER_ROWS=None, TARGET_USER_FLAT=None, TARGET_USER_FITS=None, TARGET_SYSTEM=None, TQUALITY=None, VAR_MAXDIST=None, VAR_NCLASSES=None, VAR_NSKIP=None, VAR_MODEL=None, LOG=None, BLOCK=None, DBLOCK=None, CV_METHOD=None, CV_SAMPLES=None, SEARCH_RANGE=None, SEARCH_RADIUS=None, SEARCH_POINTS_ALL=None, SEARCH_POINTS_MIN=None, SEARCH_POINTS_MAX=None, RESAMPLING=None, COORDS=None, Verbose=2):
    '''
    Universal Kriging
    ----------
    [statistics_kriging.2]\n
    Universal Kriging for grid interpolation from irregular sample points.\n
    Arguments
    ----------
    - POINTS [`input shapes`] : Points
    - TARGET_TEMPLATE [`optional input grid`] : Target System. use this grid's system for output grids
    - PREDICTORS [`optional input grid list`] : Predictors
    - PREDICTION [`output grid`] : Prediction
    - VARIANCE [`output grid`] : Prediction Error
    - CV_SUMMARY [`output table`] : Cross Validation Summary
    - CV_RESIDUALS [`output shapes`] : Cross Validation Residuals
    - FIELD [`table field`] : Attribute
    - TARGET_DEFINITION [`choice`] : Target Grid System. Available Choices: [0] user defined [1] grid or grid system Default: 0
    - TARGET_USER_SIZE [`floating point number`] : Cellsize. Minimum: 0.000000 Default: 1.000000
    - TARGET_USER_XMIN [`floating point number`] : West. Default: 0.000000
    - TARGET_USER_XMAX [`floating point number`] : East. Default: 100.000000
    - TARGET_USER_YMIN [`floating point number`] : South. Default: 0.000000
    - TARGET_USER_YMAX [`floating point number`] : North. Default: 100.000000
    - TARGET_USER_COLS [`integer number`] : Columns. Minimum: 1 Default: 101 Number of cells in East-West direction.
    - TARGET_USER_ROWS [`integer number`] : Rows. Minimum: 1 Default: 101 Number of cells in North-South direction.
    - TARGET_USER_FLAT [`boolean`] : Rounding. Default: 1 Round bounding coordinates to multiples of cell size. Ignored if cell size has decimal places.
    - TARGET_USER_FITS [`choice`] : Fit. Available Choices: [0] nodes [1] cells Default: 0
    - TARGET_SYSTEM [`grid system`] : Grid System
    - TQUALITY [`choice`] : Error Measure. Available Choices: [0] Standard Deviation [1] Variance Default: 1
    - VAR_MAXDIST [`floating point number`] : Maximum Distance. Minimum: 0.000000 Default: 0.000000 maximum distance for variogram estimation, ignored if set to zero
    - VAR_NCLASSES [`integer number`] : Lag Distance Classes. Minimum: 1 Default: 100 initial number of lag distance classes
    - VAR_NSKIP [`integer number`] : Skip. Minimum: 1 Default: 1
    - VAR_MODEL [`text`] : Model. Default: b * x
    - LOG [`boolean`] : Logarithmic Transformation. Default: 0
    - BLOCK [`boolean`] : Block Kriging. Default: 0
    - DBLOCK [`floating point number`] : Block Size. Minimum: 0.000000 Default: 100.000000 Edge length [map units]
    - CV_METHOD [`choice`] : Cross Validation. Available Choices: [0] none [1] leave one out [2] 2-fold [3] k-fold Default: 0
    - CV_SAMPLES [`integer number`] : Cross Validation Subsamples. Minimum: 2 Default: 10 number of subsamples for k-fold cross validation
    - SEARCH_RANGE [`choice`] : Search Range. Available Choices: [0] local [1] global Default: 1
    - SEARCH_RADIUS [`floating point number`] : Maximum Search Distance. Minimum: 0.000000 Default: 1000.000000 local maximum search distance given in map units
    - SEARCH_POINTS_ALL [`choice`] : Number of Points. Available Choices: [0] maximum number of nearest points [1] all points within search distance Default: 1
    - SEARCH_POINTS_MIN [`integer number`] : Minimum. Minimum: 1 Default: 16 minimum number of points to use
    - SEARCH_POINTS_MAX [`integer number`] : Maximum. Minimum: 1 Default: 20 maximum number of nearest points
    - RESAMPLING [`choice`] : Resampling. Available Choices: [0] Nearest Neighbour [1] Bilinear Interpolation [2] Bicubic Spline Interpolation [3] B-Spline Interpolation Default: 3
    - COORDS [`boolean`] : Coordinates. Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('statistics_kriging', '2', 'Universal Kriging')
    if Tool.is_Okay():
        Tool.Set_Input ('POINTS', POINTS)
        Tool.Set_Input ('TARGET_TEMPLATE', TARGET_TEMPLATE)
        Tool.Set_Input ('PREDICTORS', PREDICTORS)
        Tool.Set_Output('PREDICTION', PREDICTION)
        Tool.Set_Output('VARIANCE', VARIANCE)
        Tool.Set_Output('CV_SUMMARY', CV_SUMMARY)
        Tool.Set_Output('CV_RESIDUALS', CV_RESIDUALS)
        Tool.Set_Option('FIELD', FIELD)
        Tool.Set_Option('TARGET_DEFINITION', TARGET_DEFINITION)
        Tool.Set_Option('TARGET_USER_SIZE', TARGET_USER_SIZE)
        Tool.Set_Option('TARGET_USER_XMIN', TARGET_USER_XMIN)
        Tool.Set_Option('TARGET_USER_XMAX', TARGET_USER_XMAX)
        Tool.Set_Option('TARGET_USER_YMIN', TARGET_USER_YMIN)
        Tool.Set_Option('TARGET_USER_YMAX', TARGET_USER_YMAX)
        Tool.Set_Option('TARGET_USER_COLS', TARGET_USER_COLS)
        Tool.Set_Option('TARGET_USER_ROWS', TARGET_USER_ROWS)
        Tool.Set_Option('TARGET_USER_FLAT', TARGET_USER_FLAT)
        Tool.Set_Option('TARGET_USER_FITS', TARGET_USER_FITS)
        Tool.Set_Option('TARGET_SYSTEM', TARGET_SYSTEM)
        Tool.Set_Option('TQUALITY', TQUALITY)
        Tool.Set_Option('VAR_MAXDIST', VAR_MAXDIST)
        Tool.Set_Option('VAR_NCLASSES', VAR_NCLASSES)
        Tool.Set_Option('VAR_NSKIP', VAR_NSKIP)
        Tool.Set_Option('VAR_MODEL', VAR_MODEL)
        Tool.Set_Option('LOG', LOG)
        Tool.Set_Option('BLOCK', BLOCK)
        Tool.Set_Option('DBLOCK', DBLOCK)
        Tool.Set_Option('CV_METHOD', CV_METHOD)
        Tool.Set_Option('CV_SAMPLES', CV_SAMPLES)
        Tool.Set_Option('SEARCH_RANGE', SEARCH_RANGE)
        Tool.Set_Option('SEARCH_RADIUS', SEARCH_RADIUS)
        Tool.Set_Option('SEARCH_POINTS_ALL', SEARCH_POINTS_ALL)
        Tool.Set_Option('SEARCH_POINTS_MIN', SEARCH_POINTS_MIN)
        Tool.Set_Option('SEARCH_POINTS_MAX', SEARCH_POINTS_MAX)
        Tool.Set_Option('RESAMPLING', RESAMPLING)
        Tool.Set_Option('COORDS', COORDS)
        return Tool.Execute(Verbose)
    return False

def run_tool_statistics_kriging_2(POINTS=None, TARGET_TEMPLATE=None, PREDICTORS=None, PREDICTION=None, VARIANCE=None, CV_SUMMARY=None, CV_RESIDUALS=None, FIELD=None, TARGET_DEFINITION=None, TARGET_USER_SIZE=None, TARGET_USER_XMIN=None, TARGET_USER_XMAX=None, TARGET_USER_YMIN=None, TARGET_USER_YMAX=None, TARGET_USER_COLS=None, TARGET_USER_ROWS=None, TARGET_USER_FLAT=None, TARGET_USER_FITS=None, TARGET_SYSTEM=None, TQUALITY=None, VAR_MAXDIST=None, VAR_NCLASSES=None, VAR_NSKIP=None, VAR_MODEL=None, LOG=None, BLOCK=None, DBLOCK=None, CV_METHOD=None, CV_SAMPLES=None, SEARCH_RANGE=None, SEARCH_RADIUS=None, SEARCH_POINTS_ALL=None, SEARCH_POINTS_MIN=None, SEARCH_POINTS_MAX=None, RESAMPLING=None, COORDS=None, Verbose=2):
    '''
    Universal Kriging
    ----------
    [statistics_kriging.2]\n
    Universal Kriging for grid interpolation from irregular sample points.\n
    Arguments
    ----------
    - POINTS [`input shapes`] : Points
    - TARGET_TEMPLATE [`optional input grid`] : Target System. use this grid's system for output grids
    - PREDICTORS [`optional input grid list`] : Predictors
    - PREDICTION [`output grid`] : Prediction
    - VARIANCE [`output grid`] : Prediction Error
    - CV_SUMMARY [`output table`] : Cross Validation Summary
    - CV_RESIDUALS [`output shapes`] : Cross Validation Residuals
    - FIELD [`table field`] : Attribute
    - TARGET_DEFINITION [`choice`] : Target Grid System. Available Choices: [0] user defined [1] grid or grid system Default: 0
    - TARGET_USER_SIZE [`floating point number`] : Cellsize. Minimum: 0.000000 Default: 1.000000
    - TARGET_USER_XMIN [`floating point number`] : West. Default: 0.000000
    - TARGET_USER_XMAX [`floating point number`] : East. Default: 100.000000
    - TARGET_USER_YMIN [`floating point number`] : South. Default: 0.000000
    - TARGET_USER_YMAX [`floating point number`] : North. Default: 100.000000
    - TARGET_USER_COLS [`integer number`] : Columns. Minimum: 1 Default: 101 Number of cells in East-West direction.
    - TARGET_USER_ROWS [`integer number`] : Rows. Minimum: 1 Default: 101 Number of cells in North-South direction.
    - TARGET_USER_FLAT [`boolean`] : Rounding. Default: 1 Round bounding coordinates to multiples of cell size. Ignored if cell size has decimal places.
    - TARGET_USER_FITS [`choice`] : Fit. Available Choices: [0] nodes [1] cells Default: 0
    - TARGET_SYSTEM [`grid system`] : Grid System
    - TQUALITY [`choice`] : Error Measure. Available Choices: [0] Standard Deviation [1] Variance Default: 1
    - VAR_MAXDIST [`floating point number`] : Maximum Distance. Minimum: 0.000000 Default: 0.000000 maximum distance for variogram estimation, ignored if set to zero
    - VAR_NCLASSES [`integer number`] : Lag Distance Classes. Minimum: 1 Default: 100 initial number of lag distance classes
    - VAR_NSKIP [`integer number`] : Skip. Minimum: 1 Default: 1
    - VAR_MODEL [`text`] : Model. Default: b * x
    - LOG [`boolean`] : Logarithmic Transformation. Default: 0
    - BLOCK [`boolean`] : Block Kriging. Default: 0
    - DBLOCK [`floating point number`] : Block Size. Minimum: 0.000000 Default: 100.000000 Edge length [map units]
    - CV_METHOD [`choice`] : Cross Validation. Available Choices: [0] none [1] leave one out [2] 2-fold [3] k-fold Default: 0
    - CV_SAMPLES [`integer number`] : Cross Validation Subsamples. Minimum: 2 Default: 10 number of subsamples for k-fold cross validation
    - SEARCH_RANGE [`choice`] : Search Range. Available Choices: [0] local [1] global Default: 1
    - SEARCH_RADIUS [`floating point number`] : Maximum Search Distance. Minimum: 0.000000 Default: 1000.000000 local maximum search distance given in map units
    - SEARCH_POINTS_ALL [`choice`] : Number of Points. Available Choices: [0] maximum number of nearest points [1] all points within search distance Default: 1
    - SEARCH_POINTS_MIN [`integer number`] : Minimum. Minimum: 1 Default: 16 minimum number of points to use
    - SEARCH_POINTS_MAX [`integer number`] : Maximum. Minimum: 1 Default: 20 maximum number of nearest points
    - RESAMPLING [`choice`] : Resampling. Available Choices: [0] Nearest Neighbour [1] Bilinear Interpolation [2] Bicubic Spline Interpolation [3] B-Spline Interpolation Default: 3
    - COORDS [`boolean`] : Coordinates. Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('statistics_kriging', '2', 'Universal Kriging')
    if Tool.is_Okay():
        Tool.Set_Input ('POINTS', POINTS)
        Tool.Set_Input ('TARGET_TEMPLATE', TARGET_TEMPLATE)
        Tool.Set_Input ('PREDICTORS', PREDICTORS)
        Tool.Set_Output('PREDICTION', PREDICTION)
        Tool.Set_Output('VARIANCE', VARIANCE)
        Tool.Set_Output('CV_SUMMARY', CV_SUMMARY)
        Tool.Set_Output('CV_RESIDUALS', CV_RESIDUALS)
        Tool.Set_Option('FIELD', FIELD)
        Tool.Set_Option('TARGET_DEFINITION', TARGET_DEFINITION)
        Tool.Set_Option('TARGET_USER_SIZE', TARGET_USER_SIZE)
        Tool.Set_Option('TARGET_USER_XMIN', TARGET_USER_XMIN)
        Tool.Set_Option('TARGET_USER_XMAX', TARGET_USER_XMAX)
        Tool.Set_Option('TARGET_USER_YMIN', TARGET_USER_YMIN)
        Tool.Set_Option('TARGET_USER_YMAX', TARGET_USER_YMAX)
        Tool.Set_Option('TARGET_USER_COLS', TARGET_USER_COLS)
        Tool.Set_Option('TARGET_USER_ROWS', TARGET_USER_ROWS)
        Tool.Set_Option('TARGET_USER_FLAT', TARGET_USER_FLAT)
        Tool.Set_Option('TARGET_USER_FITS', TARGET_USER_FITS)
        Tool.Set_Option('TARGET_SYSTEM', TARGET_SYSTEM)
        Tool.Set_Option('TQUALITY', TQUALITY)
        Tool.Set_Option('VAR_MAXDIST', VAR_MAXDIST)
        Tool.Set_Option('VAR_NCLASSES', VAR_NCLASSES)
        Tool.Set_Option('VAR_NSKIP', VAR_NSKIP)
        Tool.Set_Option('VAR_MODEL', VAR_MODEL)
        Tool.Set_Option('LOG', LOG)
        Tool.Set_Option('BLOCK', BLOCK)
        Tool.Set_Option('DBLOCK', DBLOCK)
        Tool.Set_Option('CV_METHOD', CV_METHOD)
        Tool.Set_Option('CV_SAMPLES', CV_SAMPLES)
        Tool.Set_Option('SEARCH_RANGE', SEARCH_RANGE)
        Tool.Set_Option('SEARCH_RADIUS', SEARCH_RADIUS)
        Tool.Set_Option('SEARCH_POINTS_ALL', SEARCH_POINTS_ALL)
        Tool.Set_Option('SEARCH_POINTS_MIN', SEARCH_POINTS_MIN)
        Tool.Set_Option('SEARCH_POINTS_MAX', SEARCH_POINTS_MAX)
        Tool.Set_Option('RESAMPLING', RESAMPLING)
        Tool.Set_Option('COORDS', COORDS)
        return Tool.Execute(Verbose)
    return False

def Regression_Kriging(POINTS=None, PREDICTORS=None, REGRESSION=None, PREDICTION=None, RESIDUALS=None, VARIANCE=None, INFO_COEFF=None, INFO_MODEL=None, INFO_STEPS=None, FIELD=None, TQUALITY=None, COORD_X=None, COORD_Y=None, INTERCEPT=None, METHOD=None, P_VALUE=None, RESAMPLING=None, VAR_MAXDIST=None, VAR_NCLASSES=None, VAR_NSKIP=None, VAR_MODEL=None, KRIGING=None, LOG=None, BLOCK=None, DBLOCK=None, SEARCH_RANGE=None, SEARCH_RADIUS=None, SEARCH_POINTS_ALL=None, SEARCH_POINTS_MIN=None, SEARCH_POINTS_MAX=None, Verbose=2):
    '''
    Regression Kriging
    ----------
    [statistics_kriging.3]\n
    Regression Kriging for grid interpolation from irregular sample points.\n
    Arguments
    ----------
    - POINTS [`input shapes`] : Points
    - PREDICTORS [`input grid list`] : Predictors
    - REGRESSION [`output grid`] : Regression. regression model applied to predictor grids
    - PREDICTION [`output grid`] : Prediction
    - RESIDUALS [`output grid`] : Residuals
    - VARIANCE [`output grid`] : Prediction Error
    - INFO_COEFF [`output table`] : Regression: Coefficients
    - INFO_MODEL [`output table`] : Regression: Model
    - INFO_STEPS [`output table`] : Regression: Steps
    - FIELD [`table field`] : Attribute
    - TQUALITY [`choice`] : Error Measure. Available Choices: [0] standard deviation [1] variance Default: 0
    - COORD_X [`boolean`] : Include X Coordinate. Default: 0
    - COORD_Y [`boolean`] : Include Y Coordinate. Default: 0
    - INTERCEPT [`boolean`] : Intercept. Default: 1
    - METHOD [`choice`] : Method. Available Choices: [0] include all [1] forward [2] backward [3] stepwise Default: 3
    - P_VALUE [`floating point number`] : Significance Level. Minimum: 0.000000 Maximum: 100.000000 Default: 5.000000 Significance level (aka p-value) as threshold for automated predictor selection, given as percentage
    - RESAMPLING [`choice`] : Resampling. Available Choices: [0] Nearest Neighbour [1] Bilinear Interpolation [2] Bicubic Spline Interpolation [3] B-Spline Interpolation Default: 3
    - VAR_MAXDIST [`floating point number`] : Maximum Distance. Minimum: 0.000000 Default: 0.000000 maximum distance for variogram estimation, ignored if set to zero
    - VAR_NCLASSES [`integer number`] : Lag Distance Classes. Minimum: 1 Default: 100 initial number of lag distance classes for variogram estimation
    - VAR_NSKIP [`integer number`] : Skip. Minimum: 1 Default: 1
    - VAR_MODEL [`text`] : Variogram Model. Default: a + b * x
    - KRIGING [`choice`] : Kriging Type. Available Choices: [0] Simple Kriging [1] Ordinary Kriging Default: 0
    - LOG [`boolean`] : Logarithmic Transformation. Default: 0
    - BLOCK [`boolean`] : Block Kriging. Default: 0
    - DBLOCK [`floating point number`] : Block Size. Minimum: 0.000000 Default: 100.000000
    - SEARCH_RANGE [`choice`] : Search Range. Available Choices: [0] local [1] global Default: 1
    - SEARCH_RADIUS [`floating point number`] : Maximum Search Distance. Minimum: 0.000000 Default: 1000.000000 local maximum search distance given in map units
    - SEARCH_POINTS_ALL [`choice`] : Number of Points. Available Choices: [0] maximum number of nearest points [1] all points within search distance Default: 1
    - SEARCH_POINTS_MIN [`integer number`] : Minimum. Minimum: 1 Default: 16 minimum number of points to use
    - SEARCH_POINTS_MAX [`integer number`] : Maximum. Minimum: 1 Default: 20 maximum number of nearest points

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('statistics_kriging', '3', 'Regression Kriging')
    if Tool.is_Okay():
        Tool.Set_Input ('POINTS', POINTS)
        Tool.Set_Input ('PREDICTORS', PREDICTORS)
        Tool.Set_Output('REGRESSION', REGRESSION)
        Tool.Set_Output('PREDICTION', PREDICTION)
        Tool.Set_Output('RESIDUALS', RESIDUALS)
        Tool.Set_Output('VARIANCE', VARIANCE)
        Tool.Set_Output('INFO_COEFF', INFO_COEFF)
        Tool.Set_Output('INFO_MODEL', INFO_MODEL)
        Tool.Set_Output('INFO_STEPS', INFO_STEPS)
        Tool.Set_Option('FIELD', FIELD)
        Tool.Set_Option('TQUALITY', TQUALITY)
        Tool.Set_Option('COORD_X', COORD_X)
        Tool.Set_Option('COORD_Y', COORD_Y)
        Tool.Set_Option('INTERCEPT', INTERCEPT)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('P_VALUE', P_VALUE)
        Tool.Set_Option('RESAMPLING', RESAMPLING)
        Tool.Set_Option('VAR_MAXDIST', VAR_MAXDIST)
        Tool.Set_Option('VAR_NCLASSES', VAR_NCLASSES)
        Tool.Set_Option('VAR_NSKIP', VAR_NSKIP)
        Tool.Set_Option('VAR_MODEL', VAR_MODEL)
        Tool.Set_Option('KRIGING', KRIGING)
        Tool.Set_Option('LOG', LOG)
        Tool.Set_Option('BLOCK', BLOCK)
        Tool.Set_Option('DBLOCK', DBLOCK)
        Tool.Set_Option('SEARCH_RANGE', SEARCH_RANGE)
        Tool.Set_Option('SEARCH_RADIUS', SEARCH_RADIUS)
        Tool.Set_Option('SEARCH_POINTS_ALL', SEARCH_POINTS_ALL)
        Tool.Set_Option('SEARCH_POINTS_MIN', SEARCH_POINTS_MIN)
        Tool.Set_Option('SEARCH_POINTS_MAX', SEARCH_POINTS_MAX)
        return Tool.Execute(Verbose)
    return False

def run_tool_statistics_kriging_3(POINTS=None, PREDICTORS=None, REGRESSION=None, PREDICTION=None, RESIDUALS=None, VARIANCE=None, INFO_COEFF=None, INFO_MODEL=None, INFO_STEPS=None, FIELD=None, TQUALITY=None, COORD_X=None, COORD_Y=None, INTERCEPT=None, METHOD=None, P_VALUE=None, RESAMPLING=None, VAR_MAXDIST=None, VAR_NCLASSES=None, VAR_NSKIP=None, VAR_MODEL=None, KRIGING=None, LOG=None, BLOCK=None, DBLOCK=None, SEARCH_RANGE=None, SEARCH_RADIUS=None, SEARCH_POINTS_ALL=None, SEARCH_POINTS_MIN=None, SEARCH_POINTS_MAX=None, Verbose=2):
    '''
    Regression Kriging
    ----------
    [statistics_kriging.3]\n
    Regression Kriging for grid interpolation from irregular sample points.\n
    Arguments
    ----------
    - POINTS [`input shapes`] : Points
    - PREDICTORS [`input grid list`] : Predictors
    - REGRESSION [`output grid`] : Regression. regression model applied to predictor grids
    - PREDICTION [`output grid`] : Prediction
    - RESIDUALS [`output grid`] : Residuals
    - VARIANCE [`output grid`] : Prediction Error
    - INFO_COEFF [`output table`] : Regression: Coefficients
    - INFO_MODEL [`output table`] : Regression: Model
    - INFO_STEPS [`output table`] : Regression: Steps
    - FIELD [`table field`] : Attribute
    - TQUALITY [`choice`] : Error Measure. Available Choices: [0] standard deviation [1] variance Default: 0
    - COORD_X [`boolean`] : Include X Coordinate. Default: 0
    - COORD_Y [`boolean`] : Include Y Coordinate. Default: 0
    - INTERCEPT [`boolean`] : Intercept. Default: 1
    - METHOD [`choice`] : Method. Available Choices: [0] include all [1] forward [2] backward [3] stepwise Default: 3
    - P_VALUE [`floating point number`] : Significance Level. Minimum: 0.000000 Maximum: 100.000000 Default: 5.000000 Significance level (aka p-value) as threshold for automated predictor selection, given as percentage
    - RESAMPLING [`choice`] : Resampling. Available Choices: [0] Nearest Neighbour [1] Bilinear Interpolation [2] Bicubic Spline Interpolation [3] B-Spline Interpolation Default: 3
    - VAR_MAXDIST [`floating point number`] : Maximum Distance. Minimum: 0.000000 Default: 0.000000 maximum distance for variogram estimation, ignored if set to zero
    - VAR_NCLASSES [`integer number`] : Lag Distance Classes. Minimum: 1 Default: 100 initial number of lag distance classes for variogram estimation
    - VAR_NSKIP [`integer number`] : Skip. Minimum: 1 Default: 1
    - VAR_MODEL [`text`] : Variogram Model. Default: a + b * x
    - KRIGING [`choice`] : Kriging Type. Available Choices: [0] Simple Kriging [1] Ordinary Kriging Default: 0
    - LOG [`boolean`] : Logarithmic Transformation. Default: 0
    - BLOCK [`boolean`] : Block Kriging. Default: 0
    - DBLOCK [`floating point number`] : Block Size. Minimum: 0.000000 Default: 100.000000
    - SEARCH_RANGE [`choice`] : Search Range. Available Choices: [0] local [1] global Default: 1
    - SEARCH_RADIUS [`floating point number`] : Maximum Search Distance. Minimum: 0.000000 Default: 1000.000000 local maximum search distance given in map units
    - SEARCH_POINTS_ALL [`choice`] : Number of Points. Available Choices: [0] maximum number of nearest points [1] all points within search distance Default: 1
    - SEARCH_POINTS_MIN [`integer number`] : Minimum. Minimum: 1 Default: 16 minimum number of points to use
    - SEARCH_POINTS_MAX [`integer number`] : Maximum. Minimum: 1 Default: 20 maximum number of nearest points

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('statistics_kriging', '3', 'Regression Kriging')
    if Tool.is_Okay():
        Tool.Set_Input ('POINTS', POINTS)
        Tool.Set_Input ('PREDICTORS', PREDICTORS)
        Tool.Set_Output('REGRESSION', REGRESSION)
        Tool.Set_Output('PREDICTION', PREDICTION)
        Tool.Set_Output('RESIDUALS', RESIDUALS)
        Tool.Set_Output('VARIANCE', VARIANCE)
        Tool.Set_Output('INFO_COEFF', INFO_COEFF)
        Tool.Set_Output('INFO_MODEL', INFO_MODEL)
        Tool.Set_Output('INFO_STEPS', INFO_STEPS)
        Tool.Set_Option('FIELD', FIELD)
        Tool.Set_Option('TQUALITY', TQUALITY)
        Tool.Set_Option('COORD_X', COORD_X)
        Tool.Set_Option('COORD_Y', COORD_Y)
        Tool.Set_Option('INTERCEPT', INTERCEPT)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('P_VALUE', P_VALUE)
        Tool.Set_Option('RESAMPLING', RESAMPLING)
        Tool.Set_Option('VAR_MAXDIST', VAR_MAXDIST)
        Tool.Set_Option('VAR_NCLASSES', VAR_NCLASSES)
        Tool.Set_Option('VAR_NSKIP', VAR_NSKIP)
        Tool.Set_Option('VAR_MODEL', VAR_MODEL)
        Tool.Set_Option('KRIGING', KRIGING)
        Tool.Set_Option('LOG', LOG)
        Tool.Set_Option('BLOCK', BLOCK)
        Tool.Set_Option('DBLOCK', DBLOCK)
        Tool.Set_Option('SEARCH_RANGE', SEARCH_RANGE)
        Tool.Set_Option('SEARCH_RADIUS', SEARCH_RADIUS)
        Tool.Set_Option('SEARCH_POINTS_ALL', SEARCH_POINTS_ALL)
        Tool.Set_Option('SEARCH_POINTS_MIN', SEARCH_POINTS_MIN)
        Tool.Set_Option('SEARCH_POINTS_MAX', SEARCH_POINTS_MAX)
        return Tool.Execute(Verbose)
    return False

def Variogram_Dialog(POINTS=None, VARIOGRAM=None, ATTRIBUTE=None, LOG=None, VAR_MAXDIST=None, VAR_NCLASSES=None, VAR_NSKIP=None, VAR_MODEL=None, Verbose=2):
    '''
    Variogram (Dialog)
    ----------
    [statistics_kriging.4]\n
    Variogram (Dialog)\n
    Arguments
    ----------
    - POINTS [`input shapes`] : Points
    - VARIOGRAM [`output table`] : Variogram
    - ATTRIBUTE [`table field`] : Attribute
    - LOG [`boolean`] : Logarithmic Transformation. Default: 0
    - VAR_MAXDIST [`floating point number`] : Maximum Distance. Default: -1.000000
    - VAR_NCLASSES [`integer number`] : Lag Distance Classes. Minimum: 1 Default: 100 initial number of lag distance classes
    - VAR_NSKIP [`integer number`] : Skip. Minimum: 1 Default: 1
    - VAR_MODEL [`text`] : Model. Default: a + b * x

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('statistics_kriging', '4', 'Variogram (Dialog)')
    if Tool.is_Okay():
        Tool.Set_Input ('POINTS', POINTS)
        Tool.Set_Output('VARIOGRAM', VARIOGRAM)
        Tool.Set_Option('ATTRIBUTE', ATTRIBUTE)
        Tool.Set_Option('LOG', LOG)
        Tool.Set_Option('VAR_MAXDIST', VAR_MAXDIST)
        Tool.Set_Option('VAR_NCLASSES', VAR_NCLASSES)
        Tool.Set_Option('VAR_NSKIP', VAR_NSKIP)
        Tool.Set_Option('VAR_MODEL', VAR_MODEL)
        return Tool.Execute(Verbose)
    return False

def run_tool_statistics_kriging_4(POINTS=None, VARIOGRAM=None, ATTRIBUTE=None, LOG=None, VAR_MAXDIST=None, VAR_NCLASSES=None, VAR_NSKIP=None, VAR_MODEL=None, Verbose=2):
    '''
    Variogram (Dialog)
    ----------
    [statistics_kriging.4]\n
    Variogram (Dialog)\n
    Arguments
    ----------
    - POINTS [`input shapes`] : Points
    - VARIOGRAM [`output table`] : Variogram
    - ATTRIBUTE [`table field`] : Attribute
    - LOG [`boolean`] : Logarithmic Transformation. Default: 0
    - VAR_MAXDIST [`floating point number`] : Maximum Distance. Default: -1.000000
    - VAR_NCLASSES [`integer number`] : Lag Distance Classes. Minimum: 1 Default: 100 initial number of lag distance classes
    - VAR_NSKIP [`integer number`] : Skip. Minimum: 1 Default: 1
    - VAR_MODEL [`text`] : Model. Default: a + b * x

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('statistics_kriging', '4', 'Variogram (Dialog)')
    if Tool.is_Okay():
        Tool.Set_Input ('POINTS', POINTS)
        Tool.Set_Output('VARIOGRAM', VARIOGRAM)
        Tool.Set_Option('ATTRIBUTE', ATTRIBUTE)
        Tool.Set_Option('LOG', LOG)
        Tool.Set_Option('VAR_MAXDIST', VAR_MAXDIST)
        Tool.Set_Option('VAR_NCLASSES', VAR_NCLASSES)
        Tool.Set_Option('VAR_NSKIP', VAR_NSKIP)
        Tool.Set_Option('VAR_MODEL', VAR_MODEL)
        return Tool.Execute(Verbose)
    return False

def Simple_Kriging_3D(POINTS=None, TARGET_TEMPLATE=None, PREDICTION=None, VARIANCE=None, CV_SUMMARY=None, CV_RESIDUALS=None, Z_FIELD=None, Z_SCALE=None, FIELD=None, TARGET_DEFINITION=None, TARGET_USER_SIZE=None, TARGET_USER_XMIN=None, TARGET_USER_XMAX=None, TARGET_USER_YMIN=None, TARGET_USER_YMAX=None, TARGET_USER_COLS=None, TARGET_USER_ROWS=None, TARGET_USER_FLAT=None, TARGET_USER_FITS=None, TARGET_SYSTEM=None, TARGET_USER_ZSIZE=None, TARGET_USER_ZMIN=None, TARGET_USER_ZMAX=None, TARGET_USER_ZNUM=None, TQUALITY=None, VAR_MAXDIST=None, VAR_NCLASSES=None, VAR_NSKIP=None, VAR_MODEL=None, LOG=None, BLOCK=None, DBLOCK=None, CV_METHOD=None, CV_SAMPLES=None, SEARCH_RANGE=None, SEARCH_RADIUS=None, SEARCH_POINTS_ALL=None, SEARCH_POINTS_MIN=None, SEARCH_POINTS_MAX=None, Verbose=2):
    '''
    Simple Kriging (3D)
    ----------
    [statistics_kriging.5]\n
    Simple Kriging interpolation for 3-dimensional data points. Output will be a grid collection with evenly spaced Z-levels representing the 3rd dimension.\n
    Arguments
    ----------
    - POINTS [`input shapes`] : Points
    - TARGET_TEMPLATE [`optional input grid`] : Target System. use this grid's system for output grids
    - PREDICTION [`output grid collection`] : Prediction
    - VARIANCE [`output grid collection`] : Prediction Error
    - CV_SUMMARY [`output table`] : Cross Validation Summary
    - CV_RESIDUALS [`output shapes`] : Cross Validation Residuals
    - Z_FIELD [`table field`] : Z
    - Z_SCALE [`floating point number`] : Z Factor. Default: 1.000000
    - FIELD [`table field`] : Attribute
    - TARGET_DEFINITION [`choice`] : Target Grid System. Available Choices: [0] user defined [1] grid or grid system Default: 0
    - TARGET_USER_SIZE [`floating point number`] : Cellsize. Minimum: 0.000000 Default: 1.000000
    - TARGET_USER_XMIN [`floating point number`] : West. Default: 0.000000
    - TARGET_USER_XMAX [`floating point number`] : East. Default: 100.000000
    - TARGET_USER_YMIN [`floating point number`] : South. Default: 0.000000
    - TARGET_USER_YMAX [`floating point number`] : North. Default: 100.000000
    - TARGET_USER_COLS [`integer number`] : Columns. Minimum: 1 Default: 101 Number of cells in East-West direction.
    - TARGET_USER_ROWS [`integer number`] : Rows. Minimum: 1 Default: 101 Number of cells in North-South direction.
    - TARGET_USER_FLAT [`boolean`] : Rounding. Default: 1 Round bounding coordinates to multiples of cell size. Ignored if cell size has decimal places.
    - TARGET_USER_FITS [`choice`] : Fit. Available Choices: [0] nodes [1] cells Default: 0
    - TARGET_SYSTEM [`grid system`] : Grid System
    - TARGET_USER_ZSIZE [`floating point number`] : Cellsize. Minimum: 0.000000 Default: 1.000000
    - TARGET_USER_ZMIN [`floating point number`] : Bottom. Default: 0.000000
    - TARGET_USER_ZMAX [`floating point number`] : Top. Default: 100.000000
    - TARGET_USER_ZNUM [`integer number`] : Levels. Minimum: 1 Default: 101
    - TQUALITY [`choice`] : Error Measure. Available Choices: [0] Standard Deviation [1] Variance Default: 1
    - VAR_MAXDIST [`floating point number`] : Maximum Distance. Minimum: 0.000000 Default: 0.000000 maximum distance for variogram estimation, ignored if set to zero
    - VAR_NCLASSES [`integer number`] : Lag Distance Classes. Minimum: 1 Default: 100 initial number of lag distance classes
    - VAR_NSKIP [`integer number`] : Skip. Minimum: 1 Default: 1
    - VAR_MODEL [`text`] : Model. Default: b * x
    - LOG [`boolean`] : Logarithmic Transformation. Default: 0
    - BLOCK [`boolean`] : Block Kriging. Default: 0
    - DBLOCK [`floating point number`] : Block Size. Minimum: 0.000000 Default: 100.000000 Edge length [map units]
    - CV_METHOD [`choice`] : Cross Validation. Available Choices: [0] none [1] leave one out [2] 2-fold [3] k-fold Default: 0
    - CV_SAMPLES [`integer number`] : Cross Validation Subsamples. Minimum: 2 Default: 10 number of subsamples for k-fold cross validation
    - SEARCH_RANGE [`choice`] : Search Range. Available Choices: [0] local [1] global Default: 1
    - SEARCH_RADIUS [`floating point number`] : Maximum Search Distance. Minimum: 0.000000 Default: 1000.000000 local maximum search distance given in map units
    - SEARCH_POINTS_ALL [`choice`] : Number of Points. Available Choices: [0] maximum number of nearest points [1] all points within search distance Default: 1
    - SEARCH_POINTS_MIN [`integer number`] : Minimum. Minimum: 1 Default: 16 minimum number of points to use
    - SEARCH_POINTS_MAX [`integer number`] : Maximum. Minimum: 1 Default: 20 maximum number of nearest points

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('statistics_kriging', '5', 'Simple Kriging (3D)')
    if Tool.is_Okay():
        Tool.Set_Input ('POINTS', POINTS)
        Tool.Set_Input ('TARGET_TEMPLATE', TARGET_TEMPLATE)
        Tool.Set_Output('PREDICTION', PREDICTION)
        Tool.Set_Output('VARIANCE', VARIANCE)
        Tool.Set_Output('CV_SUMMARY', CV_SUMMARY)
        Tool.Set_Output('CV_RESIDUALS', CV_RESIDUALS)
        Tool.Set_Option('Z_FIELD', Z_FIELD)
        Tool.Set_Option('Z_SCALE', Z_SCALE)
        Tool.Set_Option('FIELD', FIELD)
        Tool.Set_Option('TARGET_DEFINITION', TARGET_DEFINITION)
        Tool.Set_Option('TARGET_USER_SIZE', TARGET_USER_SIZE)
        Tool.Set_Option('TARGET_USER_XMIN', TARGET_USER_XMIN)
        Tool.Set_Option('TARGET_USER_XMAX', TARGET_USER_XMAX)
        Tool.Set_Option('TARGET_USER_YMIN', TARGET_USER_YMIN)
        Tool.Set_Option('TARGET_USER_YMAX', TARGET_USER_YMAX)
        Tool.Set_Option('TARGET_USER_COLS', TARGET_USER_COLS)
        Tool.Set_Option('TARGET_USER_ROWS', TARGET_USER_ROWS)
        Tool.Set_Option('TARGET_USER_FLAT', TARGET_USER_FLAT)
        Tool.Set_Option('TARGET_USER_FITS', TARGET_USER_FITS)
        Tool.Set_Option('TARGET_SYSTEM', TARGET_SYSTEM)
        Tool.Set_Option('TARGET_USER_ZSIZE', TARGET_USER_ZSIZE)
        Tool.Set_Option('TARGET_USER_ZMIN', TARGET_USER_ZMIN)
        Tool.Set_Option('TARGET_USER_ZMAX', TARGET_USER_ZMAX)
        Tool.Set_Option('TARGET_USER_ZNUM', TARGET_USER_ZNUM)
        Tool.Set_Option('TQUALITY', TQUALITY)
        Tool.Set_Option('VAR_MAXDIST', VAR_MAXDIST)
        Tool.Set_Option('VAR_NCLASSES', VAR_NCLASSES)
        Tool.Set_Option('VAR_NSKIP', VAR_NSKIP)
        Tool.Set_Option('VAR_MODEL', VAR_MODEL)
        Tool.Set_Option('LOG', LOG)
        Tool.Set_Option('BLOCK', BLOCK)
        Tool.Set_Option('DBLOCK', DBLOCK)
        Tool.Set_Option('CV_METHOD', CV_METHOD)
        Tool.Set_Option('CV_SAMPLES', CV_SAMPLES)
        Tool.Set_Option('SEARCH_RANGE', SEARCH_RANGE)
        Tool.Set_Option('SEARCH_RADIUS', SEARCH_RADIUS)
        Tool.Set_Option('SEARCH_POINTS_ALL', SEARCH_POINTS_ALL)
        Tool.Set_Option('SEARCH_POINTS_MIN', SEARCH_POINTS_MIN)
        Tool.Set_Option('SEARCH_POINTS_MAX', SEARCH_POINTS_MAX)
        return Tool.Execute(Verbose)
    return False

def run_tool_statistics_kriging_5(POINTS=None, TARGET_TEMPLATE=None, PREDICTION=None, VARIANCE=None, CV_SUMMARY=None, CV_RESIDUALS=None, Z_FIELD=None, Z_SCALE=None, FIELD=None, TARGET_DEFINITION=None, TARGET_USER_SIZE=None, TARGET_USER_XMIN=None, TARGET_USER_XMAX=None, TARGET_USER_YMIN=None, TARGET_USER_YMAX=None, TARGET_USER_COLS=None, TARGET_USER_ROWS=None, TARGET_USER_FLAT=None, TARGET_USER_FITS=None, TARGET_SYSTEM=None, TARGET_USER_ZSIZE=None, TARGET_USER_ZMIN=None, TARGET_USER_ZMAX=None, TARGET_USER_ZNUM=None, TQUALITY=None, VAR_MAXDIST=None, VAR_NCLASSES=None, VAR_NSKIP=None, VAR_MODEL=None, LOG=None, BLOCK=None, DBLOCK=None, CV_METHOD=None, CV_SAMPLES=None, SEARCH_RANGE=None, SEARCH_RADIUS=None, SEARCH_POINTS_ALL=None, SEARCH_POINTS_MIN=None, SEARCH_POINTS_MAX=None, Verbose=2):
    '''
    Simple Kriging (3D)
    ----------
    [statistics_kriging.5]\n
    Simple Kriging interpolation for 3-dimensional data points. Output will be a grid collection with evenly spaced Z-levels representing the 3rd dimension.\n
    Arguments
    ----------
    - POINTS [`input shapes`] : Points
    - TARGET_TEMPLATE [`optional input grid`] : Target System. use this grid's system for output grids
    - PREDICTION [`output grid collection`] : Prediction
    - VARIANCE [`output grid collection`] : Prediction Error
    - CV_SUMMARY [`output table`] : Cross Validation Summary
    - CV_RESIDUALS [`output shapes`] : Cross Validation Residuals
    - Z_FIELD [`table field`] : Z
    - Z_SCALE [`floating point number`] : Z Factor. Default: 1.000000
    - FIELD [`table field`] : Attribute
    - TARGET_DEFINITION [`choice`] : Target Grid System. Available Choices: [0] user defined [1] grid or grid system Default: 0
    - TARGET_USER_SIZE [`floating point number`] : Cellsize. Minimum: 0.000000 Default: 1.000000
    - TARGET_USER_XMIN [`floating point number`] : West. Default: 0.000000
    - TARGET_USER_XMAX [`floating point number`] : East. Default: 100.000000
    - TARGET_USER_YMIN [`floating point number`] : South. Default: 0.000000
    - TARGET_USER_YMAX [`floating point number`] : North. Default: 100.000000
    - TARGET_USER_COLS [`integer number`] : Columns. Minimum: 1 Default: 101 Number of cells in East-West direction.
    - TARGET_USER_ROWS [`integer number`] : Rows. Minimum: 1 Default: 101 Number of cells in North-South direction.
    - TARGET_USER_FLAT [`boolean`] : Rounding. Default: 1 Round bounding coordinates to multiples of cell size. Ignored if cell size has decimal places.
    - TARGET_USER_FITS [`choice`] : Fit. Available Choices: [0] nodes [1] cells Default: 0
    - TARGET_SYSTEM [`grid system`] : Grid System
    - TARGET_USER_ZSIZE [`floating point number`] : Cellsize. Minimum: 0.000000 Default: 1.000000
    - TARGET_USER_ZMIN [`floating point number`] : Bottom. Default: 0.000000
    - TARGET_USER_ZMAX [`floating point number`] : Top. Default: 100.000000
    - TARGET_USER_ZNUM [`integer number`] : Levels. Minimum: 1 Default: 101
    - TQUALITY [`choice`] : Error Measure. Available Choices: [0] Standard Deviation [1] Variance Default: 1
    - VAR_MAXDIST [`floating point number`] : Maximum Distance. Minimum: 0.000000 Default: 0.000000 maximum distance for variogram estimation, ignored if set to zero
    - VAR_NCLASSES [`integer number`] : Lag Distance Classes. Minimum: 1 Default: 100 initial number of lag distance classes
    - VAR_NSKIP [`integer number`] : Skip. Minimum: 1 Default: 1
    - VAR_MODEL [`text`] : Model. Default: b * x
    - LOG [`boolean`] : Logarithmic Transformation. Default: 0
    - BLOCK [`boolean`] : Block Kriging. Default: 0
    - DBLOCK [`floating point number`] : Block Size. Minimum: 0.000000 Default: 100.000000 Edge length [map units]
    - CV_METHOD [`choice`] : Cross Validation. Available Choices: [0] none [1] leave one out [2] 2-fold [3] k-fold Default: 0
    - CV_SAMPLES [`integer number`] : Cross Validation Subsamples. Minimum: 2 Default: 10 number of subsamples for k-fold cross validation
    - SEARCH_RANGE [`choice`] : Search Range. Available Choices: [0] local [1] global Default: 1
    - SEARCH_RADIUS [`floating point number`] : Maximum Search Distance. Minimum: 0.000000 Default: 1000.000000 local maximum search distance given in map units
    - SEARCH_POINTS_ALL [`choice`] : Number of Points. Available Choices: [0] maximum number of nearest points [1] all points within search distance Default: 1
    - SEARCH_POINTS_MIN [`integer number`] : Minimum. Minimum: 1 Default: 16 minimum number of points to use
    - SEARCH_POINTS_MAX [`integer number`] : Maximum. Minimum: 1 Default: 20 maximum number of nearest points

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('statistics_kriging', '5', 'Simple Kriging (3D)')
    if Tool.is_Okay():
        Tool.Set_Input ('POINTS', POINTS)
        Tool.Set_Input ('TARGET_TEMPLATE', TARGET_TEMPLATE)
        Tool.Set_Output('PREDICTION', PREDICTION)
        Tool.Set_Output('VARIANCE', VARIANCE)
        Tool.Set_Output('CV_SUMMARY', CV_SUMMARY)
        Tool.Set_Output('CV_RESIDUALS', CV_RESIDUALS)
        Tool.Set_Option('Z_FIELD', Z_FIELD)
        Tool.Set_Option('Z_SCALE', Z_SCALE)
        Tool.Set_Option('FIELD', FIELD)
        Tool.Set_Option('TARGET_DEFINITION', TARGET_DEFINITION)
        Tool.Set_Option('TARGET_USER_SIZE', TARGET_USER_SIZE)
        Tool.Set_Option('TARGET_USER_XMIN', TARGET_USER_XMIN)
        Tool.Set_Option('TARGET_USER_XMAX', TARGET_USER_XMAX)
        Tool.Set_Option('TARGET_USER_YMIN', TARGET_USER_YMIN)
        Tool.Set_Option('TARGET_USER_YMAX', TARGET_USER_YMAX)
        Tool.Set_Option('TARGET_USER_COLS', TARGET_USER_COLS)
        Tool.Set_Option('TARGET_USER_ROWS', TARGET_USER_ROWS)
        Tool.Set_Option('TARGET_USER_FLAT', TARGET_USER_FLAT)
        Tool.Set_Option('TARGET_USER_FITS', TARGET_USER_FITS)
        Tool.Set_Option('TARGET_SYSTEM', TARGET_SYSTEM)
        Tool.Set_Option('TARGET_USER_ZSIZE', TARGET_USER_ZSIZE)
        Tool.Set_Option('TARGET_USER_ZMIN', TARGET_USER_ZMIN)
        Tool.Set_Option('TARGET_USER_ZMAX', TARGET_USER_ZMAX)
        Tool.Set_Option('TARGET_USER_ZNUM', TARGET_USER_ZNUM)
        Tool.Set_Option('TQUALITY', TQUALITY)
        Tool.Set_Option('VAR_MAXDIST', VAR_MAXDIST)
        Tool.Set_Option('VAR_NCLASSES', VAR_NCLASSES)
        Tool.Set_Option('VAR_NSKIP', VAR_NSKIP)
        Tool.Set_Option('VAR_MODEL', VAR_MODEL)
        Tool.Set_Option('LOG', LOG)
        Tool.Set_Option('BLOCK', BLOCK)
        Tool.Set_Option('DBLOCK', DBLOCK)
        Tool.Set_Option('CV_METHOD', CV_METHOD)
        Tool.Set_Option('CV_SAMPLES', CV_SAMPLES)
        Tool.Set_Option('SEARCH_RANGE', SEARCH_RANGE)
        Tool.Set_Option('SEARCH_RADIUS', SEARCH_RADIUS)
        Tool.Set_Option('SEARCH_POINTS_ALL', SEARCH_POINTS_ALL)
        Tool.Set_Option('SEARCH_POINTS_MIN', SEARCH_POINTS_MIN)
        Tool.Set_Option('SEARCH_POINTS_MAX', SEARCH_POINTS_MAX)
        return Tool.Execute(Verbose)
    return False

def Ordinary_Kriging_3D(POINTS=None, TARGET_TEMPLATE=None, PREDICTION=None, VARIANCE=None, CV_SUMMARY=None, CV_RESIDUALS=None, Z_FIELD=None, Z_SCALE=None, FIELD=None, TARGET_DEFINITION=None, TARGET_USER_SIZE=None, TARGET_USER_XMIN=None, TARGET_USER_XMAX=None, TARGET_USER_YMIN=None, TARGET_USER_YMAX=None, TARGET_USER_COLS=None, TARGET_USER_ROWS=None, TARGET_USER_FLAT=None, TARGET_USER_FITS=None, TARGET_SYSTEM=None, TARGET_USER_ZSIZE=None, TARGET_USER_ZMIN=None, TARGET_USER_ZMAX=None, TARGET_USER_ZNUM=None, TQUALITY=None, VAR_MAXDIST=None, VAR_NCLASSES=None, VAR_NSKIP=None, VAR_MODEL=None, LOG=None, BLOCK=None, DBLOCK=None, CV_METHOD=None, CV_SAMPLES=None, SEARCH_RANGE=None, SEARCH_RADIUS=None, SEARCH_POINTS_ALL=None, SEARCH_POINTS_MIN=None, SEARCH_POINTS_MAX=None, Verbose=2):
    '''
    Ordinary Kriging (3D)
    ----------
    [statistics_kriging.6]\n
    Ordinary Kriging interpolation for 3-dimensional data points. Output will be a grid collection with evenly spaced Z-levels representing the 3rd dimension.\n
    Arguments
    ----------
    - POINTS [`input shapes`] : Points
    - TARGET_TEMPLATE [`optional input grid`] : Target System. use this grid's system for output grids
    - PREDICTION [`output grid collection`] : Prediction
    - VARIANCE [`output grid collection`] : Prediction Error
    - CV_SUMMARY [`output table`] : Cross Validation Summary
    - CV_RESIDUALS [`output shapes`] : Cross Validation Residuals
    - Z_FIELD [`table field`] : Z
    - Z_SCALE [`floating point number`] : Z Factor. Default: 1.000000
    - FIELD [`table field`] : Attribute
    - TARGET_DEFINITION [`choice`] : Target Grid System. Available Choices: [0] user defined [1] grid or grid system Default: 0
    - TARGET_USER_SIZE [`floating point number`] : Cellsize. Minimum: 0.000000 Default: 1.000000
    - TARGET_USER_XMIN [`floating point number`] : West. Default: 0.000000
    - TARGET_USER_XMAX [`floating point number`] : East. Default: 100.000000
    - TARGET_USER_YMIN [`floating point number`] : South. Default: 0.000000
    - TARGET_USER_YMAX [`floating point number`] : North. Default: 100.000000
    - TARGET_USER_COLS [`integer number`] : Columns. Minimum: 1 Default: 101 Number of cells in East-West direction.
    - TARGET_USER_ROWS [`integer number`] : Rows. Minimum: 1 Default: 101 Number of cells in North-South direction.
    - TARGET_USER_FLAT [`boolean`] : Rounding. Default: 1 Round bounding coordinates to multiples of cell size. Ignored if cell size has decimal places.
    - TARGET_USER_FITS [`choice`] : Fit. Available Choices: [0] nodes [1] cells Default: 0
    - TARGET_SYSTEM [`grid system`] : Grid System
    - TARGET_USER_ZSIZE [`floating point number`] : Cellsize. Minimum: 0.000000 Default: 1.000000
    - TARGET_USER_ZMIN [`floating point number`] : Bottom. Default: 0.000000
    - TARGET_USER_ZMAX [`floating point number`] : Top. Default: 100.000000
    - TARGET_USER_ZNUM [`integer number`] : Levels. Minimum: 1 Default: 101
    - TQUALITY [`choice`] : Error Measure. Available Choices: [0] Standard Deviation [1] Variance Default: 1
    - VAR_MAXDIST [`floating point number`] : Maximum Distance. Minimum: 0.000000 Default: 0.000000 maximum distance for variogram estimation, ignored if set to zero
    - VAR_NCLASSES [`integer number`] : Lag Distance Classes. Minimum: 1 Default: 100 initial number of lag distance classes
    - VAR_NSKIP [`integer number`] : Skip. Minimum: 1 Default: 1
    - VAR_MODEL [`text`] : Model. Default: b * x
    - LOG [`boolean`] : Logarithmic Transformation. Default: 0
    - BLOCK [`boolean`] : Block Kriging. Default: 0
    - DBLOCK [`floating point number`] : Block Size. Minimum: 0.000000 Default: 100.000000 Edge length [map units]
    - CV_METHOD [`choice`] : Cross Validation. Available Choices: [0] none [1] leave one out [2] 2-fold [3] k-fold Default: 0
    - CV_SAMPLES [`integer number`] : Cross Validation Subsamples. Minimum: 2 Default: 10 number of subsamples for k-fold cross validation
    - SEARCH_RANGE [`choice`] : Search Range. Available Choices: [0] local [1] global Default: 1
    - SEARCH_RADIUS [`floating point number`] : Maximum Search Distance. Minimum: 0.000000 Default: 1000.000000 local maximum search distance given in map units
    - SEARCH_POINTS_ALL [`choice`] : Number of Points. Available Choices: [0] maximum number of nearest points [1] all points within search distance Default: 1
    - SEARCH_POINTS_MIN [`integer number`] : Minimum. Minimum: 1 Default: 16 minimum number of points to use
    - SEARCH_POINTS_MAX [`integer number`] : Maximum. Minimum: 1 Default: 20 maximum number of nearest points

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('statistics_kriging', '6', 'Ordinary Kriging (3D)')
    if Tool.is_Okay():
        Tool.Set_Input ('POINTS', POINTS)
        Tool.Set_Input ('TARGET_TEMPLATE', TARGET_TEMPLATE)
        Tool.Set_Output('PREDICTION', PREDICTION)
        Tool.Set_Output('VARIANCE', VARIANCE)
        Tool.Set_Output('CV_SUMMARY', CV_SUMMARY)
        Tool.Set_Output('CV_RESIDUALS', CV_RESIDUALS)
        Tool.Set_Option('Z_FIELD', Z_FIELD)
        Tool.Set_Option('Z_SCALE', Z_SCALE)
        Tool.Set_Option('FIELD', FIELD)
        Tool.Set_Option('TARGET_DEFINITION', TARGET_DEFINITION)
        Tool.Set_Option('TARGET_USER_SIZE', TARGET_USER_SIZE)
        Tool.Set_Option('TARGET_USER_XMIN', TARGET_USER_XMIN)
        Tool.Set_Option('TARGET_USER_XMAX', TARGET_USER_XMAX)
        Tool.Set_Option('TARGET_USER_YMIN', TARGET_USER_YMIN)
        Tool.Set_Option('TARGET_USER_YMAX', TARGET_USER_YMAX)
        Tool.Set_Option('TARGET_USER_COLS', TARGET_USER_COLS)
        Tool.Set_Option('TARGET_USER_ROWS', TARGET_USER_ROWS)
        Tool.Set_Option('TARGET_USER_FLAT', TARGET_USER_FLAT)
        Tool.Set_Option('TARGET_USER_FITS', TARGET_USER_FITS)
        Tool.Set_Option('TARGET_SYSTEM', TARGET_SYSTEM)
        Tool.Set_Option('TARGET_USER_ZSIZE', TARGET_USER_ZSIZE)
        Tool.Set_Option('TARGET_USER_ZMIN', TARGET_USER_ZMIN)
        Tool.Set_Option('TARGET_USER_ZMAX', TARGET_USER_ZMAX)
        Tool.Set_Option('TARGET_USER_ZNUM', TARGET_USER_ZNUM)
        Tool.Set_Option('TQUALITY', TQUALITY)
        Tool.Set_Option('VAR_MAXDIST', VAR_MAXDIST)
        Tool.Set_Option('VAR_NCLASSES', VAR_NCLASSES)
        Tool.Set_Option('VAR_NSKIP', VAR_NSKIP)
        Tool.Set_Option('VAR_MODEL', VAR_MODEL)
        Tool.Set_Option('LOG', LOG)
        Tool.Set_Option('BLOCK', BLOCK)
        Tool.Set_Option('DBLOCK', DBLOCK)
        Tool.Set_Option('CV_METHOD', CV_METHOD)
        Tool.Set_Option('CV_SAMPLES', CV_SAMPLES)
        Tool.Set_Option('SEARCH_RANGE', SEARCH_RANGE)
        Tool.Set_Option('SEARCH_RADIUS', SEARCH_RADIUS)
        Tool.Set_Option('SEARCH_POINTS_ALL', SEARCH_POINTS_ALL)
        Tool.Set_Option('SEARCH_POINTS_MIN', SEARCH_POINTS_MIN)
        Tool.Set_Option('SEARCH_POINTS_MAX', SEARCH_POINTS_MAX)
        return Tool.Execute(Verbose)
    return False

def run_tool_statistics_kriging_6(POINTS=None, TARGET_TEMPLATE=None, PREDICTION=None, VARIANCE=None, CV_SUMMARY=None, CV_RESIDUALS=None, Z_FIELD=None, Z_SCALE=None, FIELD=None, TARGET_DEFINITION=None, TARGET_USER_SIZE=None, TARGET_USER_XMIN=None, TARGET_USER_XMAX=None, TARGET_USER_YMIN=None, TARGET_USER_YMAX=None, TARGET_USER_COLS=None, TARGET_USER_ROWS=None, TARGET_USER_FLAT=None, TARGET_USER_FITS=None, TARGET_SYSTEM=None, TARGET_USER_ZSIZE=None, TARGET_USER_ZMIN=None, TARGET_USER_ZMAX=None, TARGET_USER_ZNUM=None, TQUALITY=None, VAR_MAXDIST=None, VAR_NCLASSES=None, VAR_NSKIP=None, VAR_MODEL=None, LOG=None, BLOCK=None, DBLOCK=None, CV_METHOD=None, CV_SAMPLES=None, SEARCH_RANGE=None, SEARCH_RADIUS=None, SEARCH_POINTS_ALL=None, SEARCH_POINTS_MIN=None, SEARCH_POINTS_MAX=None, Verbose=2):
    '''
    Ordinary Kriging (3D)
    ----------
    [statistics_kriging.6]\n
    Ordinary Kriging interpolation for 3-dimensional data points. Output will be a grid collection with evenly spaced Z-levels representing the 3rd dimension.\n
    Arguments
    ----------
    - POINTS [`input shapes`] : Points
    - TARGET_TEMPLATE [`optional input grid`] : Target System. use this grid's system for output grids
    - PREDICTION [`output grid collection`] : Prediction
    - VARIANCE [`output grid collection`] : Prediction Error
    - CV_SUMMARY [`output table`] : Cross Validation Summary
    - CV_RESIDUALS [`output shapes`] : Cross Validation Residuals
    - Z_FIELD [`table field`] : Z
    - Z_SCALE [`floating point number`] : Z Factor. Default: 1.000000
    - FIELD [`table field`] : Attribute
    - TARGET_DEFINITION [`choice`] : Target Grid System. Available Choices: [0] user defined [1] grid or grid system Default: 0
    - TARGET_USER_SIZE [`floating point number`] : Cellsize. Minimum: 0.000000 Default: 1.000000
    - TARGET_USER_XMIN [`floating point number`] : West. Default: 0.000000
    - TARGET_USER_XMAX [`floating point number`] : East. Default: 100.000000
    - TARGET_USER_YMIN [`floating point number`] : South. Default: 0.000000
    - TARGET_USER_YMAX [`floating point number`] : North. Default: 100.000000
    - TARGET_USER_COLS [`integer number`] : Columns. Minimum: 1 Default: 101 Number of cells in East-West direction.
    - TARGET_USER_ROWS [`integer number`] : Rows. Minimum: 1 Default: 101 Number of cells in North-South direction.
    - TARGET_USER_FLAT [`boolean`] : Rounding. Default: 1 Round bounding coordinates to multiples of cell size. Ignored if cell size has decimal places.
    - TARGET_USER_FITS [`choice`] : Fit. Available Choices: [0] nodes [1] cells Default: 0
    - TARGET_SYSTEM [`grid system`] : Grid System
    - TARGET_USER_ZSIZE [`floating point number`] : Cellsize. Minimum: 0.000000 Default: 1.000000
    - TARGET_USER_ZMIN [`floating point number`] : Bottom. Default: 0.000000
    - TARGET_USER_ZMAX [`floating point number`] : Top. Default: 100.000000
    - TARGET_USER_ZNUM [`integer number`] : Levels. Minimum: 1 Default: 101
    - TQUALITY [`choice`] : Error Measure. Available Choices: [0] Standard Deviation [1] Variance Default: 1
    - VAR_MAXDIST [`floating point number`] : Maximum Distance. Minimum: 0.000000 Default: 0.000000 maximum distance for variogram estimation, ignored if set to zero
    - VAR_NCLASSES [`integer number`] : Lag Distance Classes. Minimum: 1 Default: 100 initial number of lag distance classes
    - VAR_NSKIP [`integer number`] : Skip. Minimum: 1 Default: 1
    - VAR_MODEL [`text`] : Model. Default: b * x
    - LOG [`boolean`] : Logarithmic Transformation. Default: 0
    - BLOCK [`boolean`] : Block Kriging. Default: 0
    - DBLOCK [`floating point number`] : Block Size. Minimum: 0.000000 Default: 100.000000 Edge length [map units]
    - CV_METHOD [`choice`] : Cross Validation. Available Choices: [0] none [1] leave one out [2] 2-fold [3] k-fold Default: 0
    - CV_SAMPLES [`integer number`] : Cross Validation Subsamples. Minimum: 2 Default: 10 number of subsamples for k-fold cross validation
    - SEARCH_RANGE [`choice`] : Search Range. Available Choices: [0] local [1] global Default: 1
    - SEARCH_RADIUS [`floating point number`] : Maximum Search Distance. Minimum: 0.000000 Default: 1000.000000 local maximum search distance given in map units
    - SEARCH_POINTS_ALL [`choice`] : Number of Points. Available Choices: [0] maximum number of nearest points [1] all points within search distance Default: 1
    - SEARCH_POINTS_MIN [`integer number`] : Minimum. Minimum: 1 Default: 16 minimum number of points to use
    - SEARCH_POINTS_MAX [`integer number`] : Maximum. Minimum: 1 Default: 20 maximum number of nearest points

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('statistics_kriging', '6', 'Ordinary Kriging (3D)')
    if Tool.is_Okay():
        Tool.Set_Input ('POINTS', POINTS)
        Tool.Set_Input ('TARGET_TEMPLATE', TARGET_TEMPLATE)
        Tool.Set_Output('PREDICTION', PREDICTION)
        Tool.Set_Output('VARIANCE', VARIANCE)
        Tool.Set_Output('CV_SUMMARY', CV_SUMMARY)
        Tool.Set_Output('CV_RESIDUALS', CV_RESIDUALS)
        Tool.Set_Option('Z_FIELD', Z_FIELD)
        Tool.Set_Option('Z_SCALE', Z_SCALE)
        Tool.Set_Option('FIELD', FIELD)
        Tool.Set_Option('TARGET_DEFINITION', TARGET_DEFINITION)
        Tool.Set_Option('TARGET_USER_SIZE', TARGET_USER_SIZE)
        Tool.Set_Option('TARGET_USER_XMIN', TARGET_USER_XMIN)
        Tool.Set_Option('TARGET_USER_XMAX', TARGET_USER_XMAX)
        Tool.Set_Option('TARGET_USER_YMIN', TARGET_USER_YMIN)
        Tool.Set_Option('TARGET_USER_YMAX', TARGET_USER_YMAX)
        Tool.Set_Option('TARGET_USER_COLS', TARGET_USER_COLS)
        Tool.Set_Option('TARGET_USER_ROWS', TARGET_USER_ROWS)
        Tool.Set_Option('TARGET_USER_FLAT', TARGET_USER_FLAT)
        Tool.Set_Option('TARGET_USER_FITS', TARGET_USER_FITS)
        Tool.Set_Option('TARGET_SYSTEM', TARGET_SYSTEM)
        Tool.Set_Option('TARGET_USER_ZSIZE', TARGET_USER_ZSIZE)
        Tool.Set_Option('TARGET_USER_ZMIN', TARGET_USER_ZMIN)
        Tool.Set_Option('TARGET_USER_ZMAX', TARGET_USER_ZMAX)
        Tool.Set_Option('TARGET_USER_ZNUM', TARGET_USER_ZNUM)
        Tool.Set_Option('TQUALITY', TQUALITY)
        Tool.Set_Option('VAR_MAXDIST', VAR_MAXDIST)
        Tool.Set_Option('VAR_NCLASSES', VAR_NCLASSES)
        Tool.Set_Option('VAR_NSKIP', VAR_NSKIP)
        Tool.Set_Option('VAR_MODEL', VAR_MODEL)
        Tool.Set_Option('LOG', LOG)
        Tool.Set_Option('BLOCK', BLOCK)
        Tool.Set_Option('DBLOCK', DBLOCK)
        Tool.Set_Option('CV_METHOD', CV_METHOD)
        Tool.Set_Option('CV_SAMPLES', CV_SAMPLES)
        Tool.Set_Option('SEARCH_RANGE', SEARCH_RANGE)
        Tool.Set_Option('SEARCH_RADIUS', SEARCH_RADIUS)
        Tool.Set_Option('SEARCH_POINTS_ALL', SEARCH_POINTS_ALL)
        Tool.Set_Option('SEARCH_POINTS_MIN', SEARCH_POINTS_MIN)
        Tool.Set_Option('SEARCH_POINTS_MAX', SEARCH_POINTS_MAX)
        return Tool.Execute(Verbose)
    return False

