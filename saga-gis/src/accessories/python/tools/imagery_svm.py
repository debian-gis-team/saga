#! /usr/bin/env python

'''
Python Interface to SAGA Tools Library
----------
- Category : Imagery
- Name     : SVM
- ID       : imagery_svm

Description
----------
Interface to LIBSVM - A Library for Support Vector Machines.
Reference:
Chang, C.-C. / Lin, C.-J. (2011): A library for support vector machines. ACM Transactions on Intelligent Systems and Technology, vol.2/3, p.1-27. [LIBSVM Homepage](http://www.csie.ntu.edu.tw/~cjlin/libsvm).

'''

from PySAGA.helper import Tool_Wrapper

def SVM_Classification(GRIDS=None, ROI=None, CLASSES=None, CLASSES_LUT=None, SCALING=None, MESSAGE=None, MODEL_SRC=None, MODEL_LOAD=None, ROI_ID=None, MODEL_SAVE=None, SVM_TYPE=None, KERNEL_TYPE=None, DEGREE=None, GAMMA=None, COEF0=None, COST=None, NU=None, EPS_SVR=None, CACHE_SIZE=None, EPS=None, SHRINKING=None, PROBABILITY=None, CROSSVAL=None, Verbose=2):
    '''
    SVM Classification
    ----------
    [imagery_svm.0]\n
    Support Vector Machine (SVM) based classification for grids.\n
    Arguments
    ----------
    - GRIDS [`input grid list`] : Grids
    - ROI [`input shapes`] : Training Areas
    - CLASSES [`output grid`] : Classification
    - CLASSES_LUT [`output table`] : Look-up Table. A reference list of the grid values that have been assigned to the training classes.
    - SCALING [`choice`] : Scaling. Available Choices: [0] none [1] normalize (0-1) [2] standardize Default: 2
    - MESSAGE [`boolean`] : Verbose Messages. Default: 0
    - MODEL_SRC [`choice`] : Model Source. Available Choices: [0] create from training areas [1] restore from file Default: 0
    - MODEL_LOAD [`file path`] : Restore Model from File
    - ROI_ID [`table field`] : Class Identifier
    - MODEL_SAVE [`file path`] : Store Model to File
    - SVM_TYPE [`choice`] : SVM Type. Available Choices: [0] C-SVC [1] nu-SVC [2] one-class SVM [3] epsilon-SVR [4] nu-SVR Default: 0
    - KERNEL_TYPE [`choice`] : Kernel Type. Available Choices: [0] linear [1] polynomial [2] radial basis function [3] sigmoid Default: 2 linear: u'*v
polynomial: (gamma*u'*v + coef0)^degree
radial basis function: exp(-gamma*|u-v|^2)
sigmoid: tanh(gamma*u'*v + coef0)
    - DEGREE [`integer number`] : Degree. Default: 3 degree in kernel function
    - GAMMA [`floating point number`] : Gamma. Default: 0.000000 gamma in kernel function
    - COEF0 [`floating point number`] : coef0. Default: 0.000000 coef0 in kernel function
    - COST [`floating point number`] : C. Default: 1.000000 parameter C (cost) of C-SVC, epsilon-SVR, and nu-SVR
    - NU [`floating point number`] : nu-SVR. Default: 0.500000 parameter nu of nu-SVC, one-class SVM, and nu-SVR
    - EPS_SVR [`floating point number`] : SVR Epsilon. Default: 0.100000 epsilon in loss function of epsilon-SVR
    - CACHE_SIZE [`floating point number`] : Cache Size. Default: 100.000000 cache memory size in MB
    - EPS [`floating point number`] : Epsilon. Default: 0.001000 tolerance of termination criterion
    - SHRINKING [`boolean`] : Shrinking. Default: 0 whether to use the shrinking heuristics
    - PROBABILITY [`boolean`] : Probability Estimates. Default: 0 whether to train a SVC or SVR model for probability estimates
    - CROSSVAL [`integer number`] : Cross Validation. Minimum: 1 Default: 1 n-fold cross validation: n must > 1

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('imagery_svm', '0', 'SVM Classification')
    if Tool.is_Okay():
        Tool.Set_Input ('GRIDS', GRIDS)
        Tool.Set_Input ('ROI', ROI)
        Tool.Set_Output('CLASSES', CLASSES)
        Tool.Set_Output('CLASSES_LUT', CLASSES_LUT)
        Tool.Set_Option('SCALING', SCALING)
        Tool.Set_Option('MESSAGE', MESSAGE)
        Tool.Set_Option('MODEL_SRC', MODEL_SRC)
        Tool.Set_Option('MODEL_LOAD', MODEL_LOAD)
        Tool.Set_Option('ROI_ID', ROI_ID)
        Tool.Set_Option('MODEL_SAVE', MODEL_SAVE)
        Tool.Set_Option('SVM_TYPE', SVM_TYPE)
        Tool.Set_Option('KERNEL_TYPE', KERNEL_TYPE)
        Tool.Set_Option('DEGREE', DEGREE)
        Tool.Set_Option('GAMMA', GAMMA)
        Tool.Set_Option('COEF0', COEF0)
        Tool.Set_Option('COST', COST)
        Tool.Set_Option('NU', NU)
        Tool.Set_Option('EPS_SVR', EPS_SVR)
        Tool.Set_Option('CACHE_SIZE', CACHE_SIZE)
        Tool.Set_Option('EPS', EPS)
        Tool.Set_Option('SHRINKING', SHRINKING)
        Tool.Set_Option('PROBABILITY', PROBABILITY)
        Tool.Set_Option('CROSSVAL', CROSSVAL)
        return Tool.Execute(Verbose)
    return False

def run_tool_imagery_svm_0(GRIDS=None, ROI=None, CLASSES=None, CLASSES_LUT=None, SCALING=None, MESSAGE=None, MODEL_SRC=None, MODEL_LOAD=None, ROI_ID=None, MODEL_SAVE=None, SVM_TYPE=None, KERNEL_TYPE=None, DEGREE=None, GAMMA=None, COEF0=None, COST=None, NU=None, EPS_SVR=None, CACHE_SIZE=None, EPS=None, SHRINKING=None, PROBABILITY=None, CROSSVAL=None, Verbose=2):
    '''
    SVM Classification
    ----------
    [imagery_svm.0]\n
    Support Vector Machine (SVM) based classification for grids.\n
    Arguments
    ----------
    - GRIDS [`input grid list`] : Grids
    - ROI [`input shapes`] : Training Areas
    - CLASSES [`output grid`] : Classification
    - CLASSES_LUT [`output table`] : Look-up Table. A reference list of the grid values that have been assigned to the training classes.
    - SCALING [`choice`] : Scaling. Available Choices: [0] none [1] normalize (0-1) [2] standardize Default: 2
    - MESSAGE [`boolean`] : Verbose Messages. Default: 0
    - MODEL_SRC [`choice`] : Model Source. Available Choices: [0] create from training areas [1] restore from file Default: 0
    - MODEL_LOAD [`file path`] : Restore Model from File
    - ROI_ID [`table field`] : Class Identifier
    - MODEL_SAVE [`file path`] : Store Model to File
    - SVM_TYPE [`choice`] : SVM Type. Available Choices: [0] C-SVC [1] nu-SVC [2] one-class SVM [3] epsilon-SVR [4] nu-SVR Default: 0
    - KERNEL_TYPE [`choice`] : Kernel Type. Available Choices: [0] linear [1] polynomial [2] radial basis function [3] sigmoid Default: 2 linear: u'*v
polynomial: (gamma*u'*v + coef0)^degree
radial basis function: exp(-gamma*|u-v|^2)
sigmoid: tanh(gamma*u'*v + coef0)
    - DEGREE [`integer number`] : Degree. Default: 3 degree in kernel function
    - GAMMA [`floating point number`] : Gamma. Default: 0.000000 gamma in kernel function
    - COEF0 [`floating point number`] : coef0. Default: 0.000000 coef0 in kernel function
    - COST [`floating point number`] : C. Default: 1.000000 parameter C (cost) of C-SVC, epsilon-SVR, and nu-SVR
    - NU [`floating point number`] : nu-SVR. Default: 0.500000 parameter nu of nu-SVC, one-class SVM, and nu-SVR
    - EPS_SVR [`floating point number`] : SVR Epsilon. Default: 0.100000 epsilon in loss function of epsilon-SVR
    - CACHE_SIZE [`floating point number`] : Cache Size. Default: 100.000000 cache memory size in MB
    - EPS [`floating point number`] : Epsilon. Default: 0.001000 tolerance of termination criterion
    - SHRINKING [`boolean`] : Shrinking. Default: 0 whether to use the shrinking heuristics
    - PROBABILITY [`boolean`] : Probability Estimates. Default: 0 whether to train a SVC or SVR model for probability estimates
    - CROSSVAL [`integer number`] : Cross Validation. Minimum: 1 Default: 1 n-fold cross validation: n must > 1

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('imagery_svm', '0', 'SVM Classification')
    if Tool.is_Okay():
        Tool.Set_Input ('GRIDS', GRIDS)
        Tool.Set_Input ('ROI', ROI)
        Tool.Set_Output('CLASSES', CLASSES)
        Tool.Set_Output('CLASSES_LUT', CLASSES_LUT)
        Tool.Set_Option('SCALING', SCALING)
        Tool.Set_Option('MESSAGE', MESSAGE)
        Tool.Set_Option('MODEL_SRC', MODEL_SRC)
        Tool.Set_Option('MODEL_LOAD', MODEL_LOAD)
        Tool.Set_Option('ROI_ID', ROI_ID)
        Tool.Set_Option('MODEL_SAVE', MODEL_SAVE)
        Tool.Set_Option('SVM_TYPE', SVM_TYPE)
        Tool.Set_Option('KERNEL_TYPE', KERNEL_TYPE)
        Tool.Set_Option('DEGREE', DEGREE)
        Tool.Set_Option('GAMMA', GAMMA)
        Tool.Set_Option('COEF0', COEF0)
        Tool.Set_Option('COST', COST)
        Tool.Set_Option('NU', NU)
        Tool.Set_Option('EPS_SVR', EPS_SVR)
        Tool.Set_Option('CACHE_SIZE', CACHE_SIZE)
        Tool.Set_Option('EPS', EPS)
        Tool.Set_Option('SHRINKING', SHRINKING)
        Tool.Set_Option('PROBABILITY', PROBABILITY)
        Tool.Set_Option('CROSSVAL', CROSSVAL)
        return Tool.Execute(Verbose)
    return False

