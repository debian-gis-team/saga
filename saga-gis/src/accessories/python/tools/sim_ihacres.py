#! /usr/bin/env python

'''
Python Interface to SAGA Tools Library
----------
- Category : Simulation
- Name     : Hydrology: IHACRES
- ID       : sim_ihacres

Description
----------
The metric conceptual rainfall-runoff model [IHACRES] (Identification of unit Hydrographs and Component flows from Rainfall, Evaporation and Streamflow data) has been implemented as a module library. The IHACRES model was developed by: Jakeman, A.J. and G.M. Hornberger (1993); Jakeman et al. (1990).

[Purpose]
The tool can be used to simulate streamflow (catchment runoff) on the basis of daily rainfall and temperature data. No spatial data, such as elevation models, soil or land use maps are required.

IHACRES has been applied to catchments with a wide range of climatologies and sizes (Croke et al., 2004). It has been used to predict streamflow in ungauged catchments (Kokkonen et al., 2003; Post and Jakeman, 1999; Post et al., 1998), to study land cover effects on hydrologic processes (Croke et al., 2004; Kokkonen and Jakeman, 2002), and to investigate dynamic response characteristics and physical catchment descriptors (Kokkonen et al., 2003; Sefton and Howarth, 1998).

More recently, the model has been used to develop a [rainfall-runoff database](http://www.ufz.de/index.php?en=17175) for flood risk assessment and forecasting by Liersch, S. and M. Volk (2008) ([pdf](http://www.iemss.org/iemss2008/uploads/Main/S05-12_Liersch_et_al-IEMSS2008.pdf)).

[References]

(-) Croke, B.F.W., Merritt, W.S., Jakeman, A.J., 2004. A dynamic model for predicting hydrologic response to land cover changes in gauged and ungauged catchments. Journal Of Hydrology 291 (1), 115-31.

(-) Jakeman, A.J., Littlewood, I.G., Whitehead, P.G., 1990. Computation of the instantaneous unit hydrograph and identifiable component flows with application to two small upland catchments. Journal of Hydrology 117 (1-4), 275-300.

(-) Jakeman, A.J. and Hornberger, G.M., 1993. How Much Complexity Is Warranted in a Rainfall-Runoff Model?. Water Resources Research 29 (8), 2637-49.

(-) Kokkonen, T.S., Jakeman, A.J., Young, P.C., Koivusalo, H.J., 2003. Predicting daily flows in ungauged catchments: model regionalization from catchment descriptors at the Coweeta Hydrologic Laboratory. North Carolina Hydrological Processes 17 (11), 2219-38.

(-) Kokkonen, T.S. and Jakeman, A.J., 2002. Structural Effects of Landscape and Land Use on Streamflow Response. In: Environmental Foresight and Models: A Manifesto, 303-321.

(-) Liersch, S. and M. Volk, 2008. A rainfall-runoff database to support flood risk assessment. iEMSs 2008: International Congress on Environmental Modelling and Software. In: M. Sanchez-Marre, J. Bejar, J. Comas, A. Rizzoli and G. Guariso (Eds.): Proceedings of the iEMSs Fourth Biennial Meeting: International Congress on Environmental Modelling and Software (iEMSs 2008). International Environmental Modelling and Software Society, Barcelona, Catalonia, July 2008. Vol. 1: 494-502. ISBN: 978-84-7653-074-0. (PEER reviewed).

(-) Post, D.A. and Jakeman, A.J., 1999. Predicting the daily streamflow of ungauged catchments in S.E. Australia by regionalising the parameters of a lumped conceptual rainfall-runoff model. Ecological Modelling 123 (2-3), 91-104.

(-) Post, D.A., Jones, J.A. and Grant, G.E., 1998. An improved methodology for predicting the daily hydrologic response of ungauged catchments. Environmental Modelling & Software 13 (3-4), 395-403.

(-) Sefton, C.E.M. and Howarth, S.M., 1998. Relationships between dynamic response characteristics and physical descriptors of catchments in England and Wales. Journal of Hydrology 211 (1-4), 1-16.



'''

from PySAGA.helper import Tool_Wrapper

def IHACRES_Calibration_2(TABLE=None, TABLEOUT=None, TABLEPARMS=None, DATE_FIELD=None, DISCHARGE_FIELD=None, PCP_FIELD=None, TMP_FIELD=None, INFLOW_FIELD=None, BUPSTREAM=None, USE_TMP=None, NSIM=None, AREA=None, STORAGE=None, IHACVERS=None, SNOW_TOOL=None, Verbose=2):
    '''
    IHACRES Calibration (2)
    ----------
    [sim_ihacres.0]\n
    Calibration Tool for the Model IHACRES\n
    \n
    \n
    Reference:\n
    \n
    Jakeman, A.J. / Hornberger, G.M. (1993).\n
    How Much Complexity Is Warranted in a Rainfall-Runoff Model?\n
    Water Resources Research, (29), NO. 8 (2637-2649)\n
    \n
    Croke, B. F. W., W. S. Merritt, et al. (2004).\n
    A dynamic model for predicting hydrologic response to land cover changes in gauged and ungauged catchments.\n
    Journal Of Hydrology 291(1-2): 115-131.\n
    Arguments
    ----------
    - TABLE [`input table`] : Table
    - TABLEOUT [`output data object`] : Table
    - TABLEPARMS [`output data object`] : Table
    - DATE_FIELD [`table field`] : Date Column. Select the column containing the Date
    - DISCHARGE_FIELD [`table field`] : Streamflow Column. Select the Column containing Discharge Values
    - PCP_FIELD [`table field`] : Precipitation Column. Select the Column containing precipitation Values
    - TMP_FIELD [`table field`] : Temperature Column. Select the Column containing Temperature Values
    - INFLOW_FIELD [`table field`] : Subbasin Inflow. Select the column containing inflow data to the subbasin
    - BUPSTREAM [`boolean`] : Is the subbasin upstream (no external inflow). Default: 1 If checked, it means there is no external inflow to the subbasin
    - USE_TMP [`boolean`] : Using temperature data?. Default: 1 If checked, then temperature data are used.
    - NSIM [`integer number`] : Number of Simulations. Minimum: 1 Maximum: 10000000 Default: 1000 Number of Simulations for Calibration
    - AREA [`floating point number`] : Area of the Watershed in [km2]. Minimum: 0.000010 Default: 100.000000 Area of the Watershed in [km2] used for unit conversion
    - STORAGE [`choice`] : Storage. Available Choices: [0] Single Storage [1] Two Parallel Storages [2] Two Storages in Series Default: 0
    - IHACVERS [`choice`] : IHACRES Version. Available Choices: [0] Jakeman & Hornberger (1993) [1] Croke et al. (2005) Default: 0
    - SNOW_TOOL [`boolean`] : Snow Tool on/off. Default: 1 If checked the snow module is active

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('sim_ihacres', '0', 'IHACRES Calibration (2)')
    if Tool.is_Okay():
        Tool.Set_Input ('TABLE', TABLE)
        Tool.Set_Output('TABLEout', TABLEOUT)
        Tool.Set_Output('TABLEparms', TABLEPARMS)
        Tool.Set_Option('DATE_Field', DATE_FIELD)
        Tool.Set_Option('DISCHARGE_Field', DISCHARGE_FIELD)
        Tool.Set_Option('PCP_Field', PCP_FIELD)
        Tool.Set_Option('TMP_Field', TMP_FIELD)
        Tool.Set_Option('INFLOW_Field', INFLOW_FIELD)
        Tool.Set_Option('bUPSTREAM', BUPSTREAM)
        Tool.Set_Option('USE_TMP', USE_TMP)
        Tool.Set_Option('NSIM', NSIM)
        Tool.Set_Option('AREA', AREA)
        Tool.Set_Option('STORAGE', STORAGE)
        Tool.Set_Option('IHACVERS', IHACVERS)
        Tool.Set_Option('SNOW_TOOL', SNOW_TOOL)
        return Tool.Execute(Verbose)
    return False

def run_tool_sim_ihacres_0(TABLE=None, TABLEOUT=None, TABLEPARMS=None, DATE_FIELD=None, DISCHARGE_FIELD=None, PCP_FIELD=None, TMP_FIELD=None, INFLOW_FIELD=None, BUPSTREAM=None, USE_TMP=None, NSIM=None, AREA=None, STORAGE=None, IHACVERS=None, SNOW_TOOL=None, Verbose=2):
    '''
    IHACRES Calibration (2)
    ----------
    [sim_ihacres.0]\n
    Calibration Tool for the Model IHACRES\n
    \n
    \n
    Reference:\n
    \n
    Jakeman, A.J. / Hornberger, G.M. (1993).\n
    How Much Complexity Is Warranted in a Rainfall-Runoff Model?\n
    Water Resources Research, (29), NO. 8 (2637-2649)\n
    \n
    Croke, B. F. W., W. S. Merritt, et al. (2004).\n
    A dynamic model for predicting hydrologic response to land cover changes in gauged and ungauged catchments.\n
    Journal Of Hydrology 291(1-2): 115-131.\n
    Arguments
    ----------
    - TABLE [`input table`] : Table
    - TABLEOUT [`output data object`] : Table
    - TABLEPARMS [`output data object`] : Table
    - DATE_FIELD [`table field`] : Date Column. Select the column containing the Date
    - DISCHARGE_FIELD [`table field`] : Streamflow Column. Select the Column containing Discharge Values
    - PCP_FIELD [`table field`] : Precipitation Column. Select the Column containing precipitation Values
    - TMP_FIELD [`table field`] : Temperature Column. Select the Column containing Temperature Values
    - INFLOW_FIELD [`table field`] : Subbasin Inflow. Select the column containing inflow data to the subbasin
    - BUPSTREAM [`boolean`] : Is the subbasin upstream (no external inflow). Default: 1 If checked, it means there is no external inflow to the subbasin
    - USE_TMP [`boolean`] : Using temperature data?. Default: 1 If checked, then temperature data are used.
    - NSIM [`integer number`] : Number of Simulations. Minimum: 1 Maximum: 10000000 Default: 1000 Number of Simulations for Calibration
    - AREA [`floating point number`] : Area of the Watershed in [km2]. Minimum: 0.000010 Default: 100.000000 Area of the Watershed in [km2] used for unit conversion
    - STORAGE [`choice`] : Storage. Available Choices: [0] Single Storage [1] Two Parallel Storages [2] Two Storages in Series Default: 0
    - IHACVERS [`choice`] : IHACRES Version. Available Choices: [0] Jakeman & Hornberger (1993) [1] Croke et al. (2005) Default: 0
    - SNOW_TOOL [`boolean`] : Snow Tool on/off. Default: 1 If checked the snow module is active

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('sim_ihacres', '0', 'IHACRES Calibration (2)')
    if Tool.is_Okay():
        Tool.Set_Input ('TABLE', TABLE)
        Tool.Set_Output('TABLEout', TABLEOUT)
        Tool.Set_Output('TABLEparms', TABLEPARMS)
        Tool.Set_Option('DATE_Field', DATE_FIELD)
        Tool.Set_Option('DISCHARGE_Field', DISCHARGE_FIELD)
        Tool.Set_Option('PCP_Field', PCP_FIELD)
        Tool.Set_Option('TMP_Field', TMP_FIELD)
        Tool.Set_Option('INFLOW_Field', INFLOW_FIELD)
        Tool.Set_Option('bUPSTREAM', BUPSTREAM)
        Tool.Set_Option('USE_TMP', USE_TMP)
        Tool.Set_Option('NSIM', NSIM)
        Tool.Set_Option('AREA', AREA)
        Tool.Set_Option('STORAGE', STORAGE)
        Tool.Set_Option('IHACVERS', IHACVERS)
        Tool.Set_Option('SNOW_TOOL', SNOW_TOOL)
        return Tool.Execute(Verbose)
    return False

def IHACRES_Version_10(TABLE=None, TABLEOUT=None, TABLEPARMS=None, TABLESETTINGS=None, DATE_FIELD=None, DISCHARGE_FIELD=None, PCP_FIELD=None, TMP_FIELD=None, USE_TMP=None, AREA=None, CFAC=None, TWFAC=None, STORAGE=None, IHACVERS=None, SNOW_TOOL=None, WRITEALL_TS=None, Verbose=2):
    '''
    IHACRES Version 1.0
    ----------
    [sim_ihacres.1]\n
    The Rainfall-Runoff Model IHACRES\n
    \n
    \n
    Reference:\n
    \n
    Jakeman, A.J. / Hornberger, G.M. (1993).\n
    How Much Complexity Is Warranted in a Rainfall-Runoff Model?\n
    Water Resources Research, (29), NO. 8 (2637-2649)\n
    \n
    Croke, B. F. W. et al.(2004).\n
    A dynamic model for predicting hydrologic response to land cover changes in gauged and ungauged catchments.\n
    Journal Of Hydrology 291(1-2): 115-131.\n
    Arguments
    ----------
    - TABLE [`input table`] : Table
    - TABLEOUT [`output data object`] : Table
    - TABLEPARMS [`output data object`] : Table
    - TABLESETTINGS [`output data object`] : Table
    - DATE_FIELD [`table field`] : Date Column. Select the column containing the Date
    - DISCHARGE_FIELD [`table field`] : Streamflow Column. Select the Column containing Discharge Values
    - PCP_FIELD [`table field`] : Precipitation Column. Select the Column containing precipitation Values
    - TMP_FIELD [`table field`] : Temperature Column. Select the Column containing Temperature Values
    - USE_TMP [`boolean`] : Using temperature data?. Default: 1 If checked, then temperature data are used.
    - AREA [`floating point number`] : Area of the Watershed in [km2]. Minimum: 0.000010 Default: 100.000000 Area of the Watershed in [km2] used for unit conversion
    - CFAC [`floating point number`] : Parameter (c). Minimum: 0.000000 Maximum: 1.000000 Default: 0.001000 Parameter (c) to fit streamflow volume
    - TWFAC [`floating point number`] : (Tw) wetness decline time constant. Minimum: 0.010000 Maximum: 150.000000 Default: 1.000000 Tw is approximately the time constant, or inversely,the rate at which the catchment wetness declines in the absence of rainfall
    - STORAGE [`choice`] : Storage. Available Choices: [0] Single Storage [1] Two Parallel Storages [2] Two Storages in Series !!! not yet implemented !!! Default: 0
    - IHACVERS [`choice`] : IHACRES Version. Available Choices: [0] Jakeman & Hornberger (1993) [1] Croke et al. (2005) Default: 0
    - SNOW_TOOL [`boolean`] : Using the snow-melt module?. Default: 0 If checked, snow-melt module is used.
    - WRITEALL_TS [`boolean`] : Write all calculated Time Series in a table?. Default: 1 If checked, then a second output table with all Time Series data is created.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('sim_ihacres', '1', 'IHACRES Version 1.0')
    if Tool.is_Okay():
        Tool.Set_Input ('TABLE', TABLE)
        Tool.Set_Output('TABLEout', TABLEOUT)
        Tool.Set_Output('TABLEparms', TABLEPARMS)
        Tool.Set_Output('TABLEsettings', TABLESETTINGS)
        Tool.Set_Option('DATE_Field', DATE_FIELD)
        Tool.Set_Option('DISCHARGE_Field', DISCHARGE_FIELD)
        Tool.Set_Option('PCP_Field', PCP_FIELD)
        Tool.Set_Option('TMP_Field', TMP_FIELD)
        Tool.Set_Option('USE_TMP', USE_TMP)
        Tool.Set_Option('AREA', AREA)
        Tool.Set_Option('CFAC', CFAC)
        Tool.Set_Option('TwFAC', TWFAC)
        Tool.Set_Option('STORAGE', STORAGE)
        Tool.Set_Option('IHACVERS', IHACVERS)
        Tool.Set_Option('SNOW_TOOL', SNOW_TOOL)
        Tool.Set_Option('WRITEALL_TS', WRITEALL_TS)
        return Tool.Execute(Verbose)
    return False

def run_tool_sim_ihacres_1(TABLE=None, TABLEOUT=None, TABLEPARMS=None, TABLESETTINGS=None, DATE_FIELD=None, DISCHARGE_FIELD=None, PCP_FIELD=None, TMP_FIELD=None, USE_TMP=None, AREA=None, CFAC=None, TWFAC=None, STORAGE=None, IHACVERS=None, SNOW_TOOL=None, WRITEALL_TS=None, Verbose=2):
    '''
    IHACRES Version 1.0
    ----------
    [sim_ihacres.1]\n
    The Rainfall-Runoff Model IHACRES\n
    \n
    \n
    Reference:\n
    \n
    Jakeman, A.J. / Hornberger, G.M. (1993).\n
    How Much Complexity Is Warranted in a Rainfall-Runoff Model?\n
    Water Resources Research, (29), NO. 8 (2637-2649)\n
    \n
    Croke, B. F. W. et al.(2004).\n
    A dynamic model for predicting hydrologic response to land cover changes in gauged and ungauged catchments.\n
    Journal Of Hydrology 291(1-2): 115-131.\n
    Arguments
    ----------
    - TABLE [`input table`] : Table
    - TABLEOUT [`output data object`] : Table
    - TABLEPARMS [`output data object`] : Table
    - TABLESETTINGS [`output data object`] : Table
    - DATE_FIELD [`table field`] : Date Column. Select the column containing the Date
    - DISCHARGE_FIELD [`table field`] : Streamflow Column. Select the Column containing Discharge Values
    - PCP_FIELD [`table field`] : Precipitation Column. Select the Column containing precipitation Values
    - TMP_FIELD [`table field`] : Temperature Column. Select the Column containing Temperature Values
    - USE_TMP [`boolean`] : Using temperature data?. Default: 1 If checked, then temperature data are used.
    - AREA [`floating point number`] : Area of the Watershed in [km2]. Minimum: 0.000010 Default: 100.000000 Area of the Watershed in [km2] used for unit conversion
    - CFAC [`floating point number`] : Parameter (c). Minimum: 0.000000 Maximum: 1.000000 Default: 0.001000 Parameter (c) to fit streamflow volume
    - TWFAC [`floating point number`] : (Tw) wetness decline time constant. Minimum: 0.010000 Maximum: 150.000000 Default: 1.000000 Tw is approximately the time constant, or inversely,the rate at which the catchment wetness declines in the absence of rainfall
    - STORAGE [`choice`] : Storage. Available Choices: [0] Single Storage [1] Two Parallel Storages [2] Two Storages in Series !!! not yet implemented !!! Default: 0
    - IHACVERS [`choice`] : IHACRES Version. Available Choices: [0] Jakeman & Hornberger (1993) [1] Croke et al. (2005) Default: 0
    - SNOW_TOOL [`boolean`] : Using the snow-melt module?. Default: 0 If checked, snow-melt module is used.
    - WRITEALL_TS [`boolean`] : Write all calculated Time Series in a table?. Default: 1 If checked, then a second output table with all Time Series data is created.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('sim_ihacres', '1', 'IHACRES Version 1.0')
    if Tool.is_Okay():
        Tool.Set_Input ('TABLE', TABLE)
        Tool.Set_Output('TABLEout', TABLEOUT)
        Tool.Set_Output('TABLEparms', TABLEPARMS)
        Tool.Set_Output('TABLEsettings', TABLESETTINGS)
        Tool.Set_Option('DATE_Field', DATE_FIELD)
        Tool.Set_Option('DISCHARGE_Field', DISCHARGE_FIELD)
        Tool.Set_Option('PCP_Field', PCP_FIELD)
        Tool.Set_Option('TMP_Field', TMP_FIELD)
        Tool.Set_Option('USE_TMP', USE_TMP)
        Tool.Set_Option('AREA', AREA)
        Tool.Set_Option('CFAC', CFAC)
        Tool.Set_Option('TwFAC', TWFAC)
        Tool.Set_Option('STORAGE', STORAGE)
        Tool.Set_Option('IHACVERS', IHACVERS)
        Tool.Set_Option('SNOW_TOOL', SNOW_TOOL)
        Tool.Set_Option('WRITEALL_TS', WRITEALL_TS)
        return Tool.Execute(Verbose)
    return False

def IHACRES_Basin(TABLEOUT=None, NSUBBASINS=None, IHACVERS=None, STORAGE=None, SNOW_TOOL=None, Verbose=2):
    '''
    IHACRES Basin
    ----------
    [sim_ihacres.2]\n
    The Rainfall-Runoff Model IHACRES\n
    \n
    \n
    Reference:\n
    \n
    Jakeman, A.J. / Hornberger, G.M. (1993).\n
    How Much Complexity Is Warranted in a Rainfall-Runoff Model?\n
    Water Resources Research, (29), NO. 8 (2637-2649)\n
    \n
    Croke, B. F. W., W. S. Merritt, et al. (2004).\n
    A dynamic model for predicting hydrologic response to land cover changes in gauged and ungauged catchments.\n
    Journal Of Hydrology 291(1-2): 115-131.\n
    Arguments
    ----------
    - TABLEOUT [`output data object`] : Table
    - NSUBBASINS [`choice`] : Number of sub-basins. Available Choices: [0] 2 [1] 3 [2] 4 [3] 5 [4] 6 [5] 7 [6] 8 [7] 9 [8] 10 Default: 0
    - IHACVERS [`choice`] : IHACRES Version. Available Choices: [0] Jakeman & Hornberger (1993) [1] Croke et al. (2005) !!! not yet implemented !!! Default: 0
    - STORAGE [`choice`] : Storage. Available Choices: [0] Single Storage [1] Two Parallel Storages [2] Two Storages in Series !!! not yet implemented !!! Default: 0
    - SNOW_TOOL [`boolean`] : Using the snow-melt module?. Default: 0 If checked, snow-melt module is used.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('sim_ihacres', '2', 'IHACRES Basin')
    if Tool.is_Okay():
        Tool.Set_Output('TABLEout', TABLEOUT)
        Tool.Set_Option('NSUBBASINS', NSUBBASINS)
        Tool.Set_Option('IHACVERS', IHACVERS)
        Tool.Set_Option('STORAGE', STORAGE)
        Tool.Set_Option('SNOW_TOOL', SNOW_TOOL)
        return Tool.Execute(Verbose)
    return False

def run_tool_sim_ihacres_2(TABLEOUT=None, NSUBBASINS=None, IHACVERS=None, STORAGE=None, SNOW_TOOL=None, Verbose=2):
    '''
    IHACRES Basin
    ----------
    [sim_ihacres.2]\n
    The Rainfall-Runoff Model IHACRES\n
    \n
    \n
    Reference:\n
    \n
    Jakeman, A.J. / Hornberger, G.M. (1993).\n
    How Much Complexity Is Warranted in a Rainfall-Runoff Model?\n
    Water Resources Research, (29), NO. 8 (2637-2649)\n
    \n
    Croke, B. F. W., W. S. Merritt, et al. (2004).\n
    A dynamic model for predicting hydrologic response to land cover changes in gauged and ungauged catchments.\n
    Journal Of Hydrology 291(1-2): 115-131.\n
    Arguments
    ----------
    - TABLEOUT [`output data object`] : Table
    - NSUBBASINS [`choice`] : Number of sub-basins. Available Choices: [0] 2 [1] 3 [2] 4 [3] 5 [4] 6 [5] 7 [6] 8 [7] 9 [8] 10 Default: 0
    - IHACVERS [`choice`] : IHACRES Version. Available Choices: [0] Jakeman & Hornberger (1993) [1] Croke et al. (2005) !!! not yet implemented !!! Default: 0
    - STORAGE [`choice`] : Storage. Available Choices: [0] Single Storage [1] Two Parallel Storages [2] Two Storages in Series !!! not yet implemented !!! Default: 0
    - SNOW_TOOL [`boolean`] : Using the snow-melt module?. Default: 0 If checked, snow-melt module is used.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('sim_ihacres', '2', 'IHACRES Basin')
    if Tool.is_Okay():
        Tool.Set_Output('TABLEout', TABLEOUT)
        Tool.Set_Option('NSUBBASINS', NSUBBASINS)
        Tool.Set_Option('IHACVERS', IHACVERS)
        Tool.Set_Option('STORAGE', STORAGE)
        Tool.Set_Option('SNOW_TOOL', SNOW_TOOL)
        return Tool.Execute(Verbose)
    return False

def IHACRES_Elevation_Bands(TABLEOUT=None, NELEVBANDS=None, AREA_TOT=None, IHACVERS=None, STORAGE=None, SNOW_TOOL=None, Verbose=2):
    '''
    IHACRES Elevation Bands
    ----------
    [sim_ihacres.3]\n
    The Rainfall-Runoff Model IHACRES\n
    \n
    \n
    Reference:\n
    \n
    Jakeman, A.J. / Hornberger, G.M. (1993).\n
    How Much Complexity Is Warranted in a Rainfall-Runoff Model?\n
    Water Resources Research, (29), NO. 8 (2637-2649)\n
    \n
    Croke, B. F. W., W. S. Merritt, et al. (2004).\n
    A dynamic model for predicting hydrologic response to land cover changes in gauged and ungauged catchments.\n
    Journal Of Hydrology 291(1-2): 115-131.\n
    Arguments
    ----------
    - TABLEOUT [`output data object`] : Table
    - NELEVBANDS [`choice`] : Number of elevation bands. Available Choices: [0] 2 [1] 3 [2] 4 [3] 5 [4] 6 [5] 7 [6] 8 [7] 9 [8] 10 Default: 0
    - AREA_TOT [`floating point number`] : Total Catchment Area [km2]. Default: 0.000000
    - IHACVERS [`choice`] : IHACRES Version. Available Choices: [0] Jakeman & Hornberger (1993) [1] Croke et al. (2005) !!! not yet implemented !!! Default: 0
    - STORAGE [`choice`] : Storage. Available Choices: [0] Single Storage [1] Two Parallel Storages [2] Two Storages in Series !!! not yet implemented !!! Default: 0
    - SNOW_TOOL [`boolean`] : Using the snow-melt module?. Default: 0 If checked, snow-melt module is used.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('sim_ihacres', '3', 'IHACRES Elevation Bands')
    if Tool.is_Okay():
        Tool.Set_Output('TABLEout', TABLEOUT)
        Tool.Set_Option('NELEVBANDS', NELEVBANDS)
        Tool.Set_Option('AREA_tot', AREA_TOT)
        Tool.Set_Option('IHACVERS', IHACVERS)
        Tool.Set_Option('STORAGE', STORAGE)
        Tool.Set_Option('SNOW_TOOL', SNOW_TOOL)
        return Tool.Execute(Verbose)
    return False

def run_tool_sim_ihacres_3(TABLEOUT=None, NELEVBANDS=None, AREA_TOT=None, IHACVERS=None, STORAGE=None, SNOW_TOOL=None, Verbose=2):
    '''
    IHACRES Elevation Bands
    ----------
    [sim_ihacres.3]\n
    The Rainfall-Runoff Model IHACRES\n
    \n
    \n
    Reference:\n
    \n
    Jakeman, A.J. / Hornberger, G.M. (1993).\n
    How Much Complexity Is Warranted in a Rainfall-Runoff Model?\n
    Water Resources Research, (29), NO. 8 (2637-2649)\n
    \n
    Croke, B. F. W., W. S. Merritt, et al. (2004).\n
    A dynamic model for predicting hydrologic response to land cover changes in gauged and ungauged catchments.\n
    Journal Of Hydrology 291(1-2): 115-131.\n
    Arguments
    ----------
    - TABLEOUT [`output data object`] : Table
    - NELEVBANDS [`choice`] : Number of elevation bands. Available Choices: [0] 2 [1] 3 [2] 4 [3] 5 [4] 6 [5] 7 [6] 8 [7] 9 [8] 10 Default: 0
    - AREA_TOT [`floating point number`] : Total Catchment Area [km2]. Default: 0.000000
    - IHACVERS [`choice`] : IHACRES Version. Available Choices: [0] Jakeman & Hornberger (1993) [1] Croke et al. (2005) !!! not yet implemented !!! Default: 0
    - STORAGE [`choice`] : Storage. Available Choices: [0] Single Storage [1] Two Parallel Storages [2] Two Storages in Series !!! not yet implemented !!! Default: 0
    - SNOW_TOOL [`boolean`] : Using the snow-melt module?. Default: 0 If checked, snow-melt module is used.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('sim_ihacres', '3', 'IHACRES Elevation Bands')
    if Tool.is_Okay():
        Tool.Set_Output('TABLEout', TABLEOUT)
        Tool.Set_Option('NELEVBANDS', NELEVBANDS)
        Tool.Set_Option('AREA_tot', AREA_TOT)
        Tool.Set_Option('IHACVERS', IHACVERS)
        Tool.Set_Option('STORAGE', STORAGE)
        Tool.Set_Option('SNOW_TOOL', SNOW_TOOL)
        return Tool.Execute(Verbose)
    return False

def IHACRES_Elevation_Bands_Calibration(TABLEOUT=None, TABLEPARMS=None, NELEVBANDS=None, NSIM=None, AREA_TOT=None, IHACVERS=None, STORAGE=None, SNOW_TOOL=None, OBJ_FUNC=None, NSEMIN=None, Verbose=2):
    '''
    IHACRES Elevation Bands Calibration
    ----------
    [sim_ihacres.4]\n
    The Rainfall-Runoff Model IHACRES\n
    \n
    \n
    Reference:\n
    \n
    Jakeman, A.J. / Hornberger, G.M. (1993).\n
    How Much Complexity Is Warranted in a Rainfall-Runoff Model?\n
    Water Resources Research, (29), NO. 8 (2637-2649)\n
    \n
    Kokkonen, T. S. et al. (2003).\n
    Predicting daily flows in ungauged catchments:model regionalization from catchment descriptorsat the Coweeta Hydrologic Laboratory, North Carolina\n
    Hydrological Processes (17), 2219-2238\n
    \n
    Croke, B. F. W., W. S. Merritt, et al. (2004).\n
    A dynamic model for predicting hydrologic responseto land cover changes in gauged andungauged catchments.\n
    Journal Of Hydrology 291(1-2): 115-131.\n
    Arguments
    ----------
    - TABLEOUT [`output data object`] : Table
    - TABLEPARMS [`output data object`] : Table
    - NELEVBANDS [`choice`] : Number of elevation bands. Available Choices: [0] 2 [1] 3 [2] 4 [3] 5 [4] 6 [5] 7 [6] 8 [7] 9 [8] 10 Default: 0
    - NSIM [`integer number`] : Number of Simulations. Minimum: 1 Maximum: 10000000 Default: 1000 Number of Simulations for Calibration
    - AREA_TOT [`floating point number`] : Total Catchment Area [km2]. Default: 0.000000
    - IHACVERS [`choice`] : IHACRES Version. Available Choices: [0] Jakeman & Hornberger (1993) [1] Croke et al. (2005) !!! not yet implemented !!! Default: 0
    - STORAGE [`choice`] : Storage. Available Choices: [0] Single Storage [1] Two Parallel Storages [2] Two Storages in Series !!! not yet implemented !!! Default: 0
    - SNOW_TOOL [`boolean`] : Using the snow-melt module?. Default: 0 If checked, snow-melt module is used.
    - OBJ_FUNC [`choice`] : Objective Function. Available Choices: [0] NSE [1] NSE high flow [2] NSE low flow Default: 0
    - NSEMIN [`floating point number`] : Minimum Nash-Sutcliffe Efficiency. Minimum: 0.100000 Maximum: 1.000000 Default: 0.700000 Minimum Nash-Sutcliffe Efficiency required to print simulation to calibration table

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('sim_ihacres', '4', 'IHACRES Elevation Bands Calibration')
    if Tool.is_Okay():
        Tool.Set_Output('TABLEout', TABLEOUT)
        Tool.Set_Output('TABLEparms', TABLEPARMS)
        Tool.Set_Option('NELEVBANDS', NELEVBANDS)
        Tool.Set_Option('NSIM', NSIM)
        Tool.Set_Option('AREA_tot', AREA_TOT)
        Tool.Set_Option('IHACVERS', IHACVERS)
        Tool.Set_Option('STORAGE', STORAGE)
        Tool.Set_Option('SNOW_TOOL', SNOW_TOOL)
        Tool.Set_Option('OBJ_FUNC', OBJ_FUNC)
        Tool.Set_Option('NSEMIN', NSEMIN)
        return Tool.Execute(Verbose)
    return False

def run_tool_sim_ihacres_4(TABLEOUT=None, TABLEPARMS=None, NELEVBANDS=None, NSIM=None, AREA_TOT=None, IHACVERS=None, STORAGE=None, SNOW_TOOL=None, OBJ_FUNC=None, NSEMIN=None, Verbose=2):
    '''
    IHACRES Elevation Bands Calibration
    ----------
    [sim_ihacres.4]\n
    The Rainfall-Runoff Model IHACRES\n
    \n
    \n
    Reference:\n
    \n
    Jakeman, A.J. / Hornberger, G.M. (1993).\n
    How Much Complexity Is Warranted in a Rainfall-Runoff Model?\n
    Water Resources Research, (29), NO. 8 (2637-2649)\n
    \n
    Kokkonen, T. S. et al. (2003).\n
    Predicting daily flows in ungauged catchments:model regionalization from catchment descriptorsat the Coweeta Hydrologic Laboratory, North Carolina\n
    Hydrological Processes (17), 2219-2238\n
    \n
    Croke, B. F. W., W. S. Merritt, et al. (2004).\n
    A dynamic model for predicting hydrologic responseto land cover changes in gauged andungauged catchments.\n
    Journal Of Hydrology 291(1-2): 115-131.\n
    Arguments
    ----------
    - TABLEOUT [`output data object`] : Table
    - TABLEPARMS [`output data object`] : Table
    - NELEVBANDS [`choice`] : Number of elevation bands. Available Choices: [0] 2 [1] 3 [2] 4 [3] 5 [4] 6 [5] 7 [6] 8 [7] 9 [8] 10 Default: 0
    - NSIM [`integer number`] : Number of Simulations. Minimum: 1 Maximum: 10000000 Default: 1000 Number of Simulations for Calibration
    - AREA_TOT [`floating point number`] : Total Catchment Area [km2]. Default: 0.000000
    - IHACVERS [`choice`] : IHACRES Version. Available Choices: [0] Jakeman & Hornberger (1993) [1] Croke et al. (2005) !!! not yet implemented !!! Default: 0
    - STORAGE [`choice`] : Storage. Available Choices: [0] Single Storage [1] Two Parallel Storages [2] Two Storages in Series !!! not yet implemented !!! Default: 0
    - SNOW_TOOL [`boolean`] : Using the snow-melt module?. Default: 0 If checked, snow-melt module is used.
    - OBJ_FUNC [`choice`] : Objective Function. Available Choices: [0] NSE [1] NSE high flow [2] NSE low flow Default: 0
    - NSEMIN [`floating point number`] : Minimum Nash-Sutcliffe Efficiency. Minimum: 0.100000 Maximum: 1.000000 Default: 0.700000 Minimum Nash-Sutcliffe Efficiency required to print simulation to calibration table

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('sim_ihacres', '4', 'IHACRES Elevation Bands Calibration')
    if Tool.is_Okay():
        Tool.Set_Output('TABLEout', TABLEOUT)
        Tool.Set_Output('TABLEparms', TABLEPARMS)
        Tool.Set_Option('NELEVBANDS', NELEVBANDS)
        Tool.Set_Option('NSIM', NSIM)
        Tool.Set_Option('AREA_tot', AREA_TOT)
        Tool.Set_Option('IHACVERS', IHACVERS)
        Tool.Set_Option('STORAGE', STORAGE)
        Tool.Set_Option('SNOW_TOOL', SNOW_TOOL)
        Tool.Set_Option('OBJ_FUNC', OBJ_FUNC)
        Tool.Set_Option('NSEMIN', NSEMIN)
        return Tool.Execute(Verbose)
    return False

