#! /usr/bin/env python

'''
Python Interface to SAGA Tools Library
----------
- Category : Table
- Name     : Calculus
- ID       : table_calculus

Description
----------
Tools for table based analyses and calculations.
'''

from PySAGA.helper import Tool_Wrapper

def Function_Fit(SOURCE=None, YFIELD=None, USE_X=None, XFIELD=None, FORMEL=None, ITER=None, LAMBDA=None, Verbose=2):
    '''
    Function Fit
    ----------
    [table_calculus.0]\n
    CFit\n
    (created by SAGA Wizard).\n
    Arguments
    ----------
    - SOURCE [`input table`] : Source
    - YFIELD [`table field`] : y - Values
    - USE_X [`choice`] : Use x -Values. Available Choices: [0] No [1] Yes Default: 0
    - XFIELD [`table field`] : x - Values
    - FORMEL [`text`] : Formula. Default: m*x+c The following operators are available for the formula definition:
+ Addition
- Subtraction
* Multiplication
/ Division
^ power
sin(x)
cos(x)
tan(x)
asin(x)
acos(x)
atan(x)
abs(x)
sqrt(x)

For Variogram - Fitting you can use the following Variogram - Models:
NUG(x)
SPH(x,a)
EXP(x,a)
LIN(x,a)
The Fitting variables are single characters like a,b,m .. alphabetical order with the grid list order ('a'= first var, 'b' = second grid, ...)
Example: m*x+a 

    - ITER [`integer number`] : Iterationen. Minimum: 1 Default: 1000
    - LAMBDA [`floating point number`] : Max Lambda. Minimum: 1.000000 Default: 10000.000000

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('table_calculus', '0', 'Function Fit')
    if Tool.is_Okay():
        Tool.Set_Input ('SOURCE', SOURCE)
        Tool.Set_Option('YFIELD', YFIELD)
        Tool.Set_Option('USE_X', USE_X)
        Tool.Set_Option('XFIELD', XFIELD)
        Tool.Set_Option('FORMEL', FORMEL)
        Tool.Set_Option('ITER', ITER)
        Tool.Set_Option('LAMBDA', LAMBDA)
        return Tool.Execute(Verbose)
    return False

def run_tool_table_calculus_0(SOURCE=None, YFIELD=None, USE_X=None, XFIELD=None, FORMEL=None, ITER=None, LAMBDA=None, Verbose=2):
    '''
    Function Fit
    ----------
    [table_calculus.0]\n
    CFit\n
    (created by SAGA Wizard).\n
    Arguments
    ----------
    - SOURCE [`input table`] : Source
    - YFIELD [`table field`] : y - Values
    - USE_X [`choice`] : Use x -Values. Available Choices: [0] No [1] Yes Default: 0
    - XFIELD [`table field`] : x - Values
    - FORMEL [`text`] : Formula. Default: m*x+c The following operators are available for the formula definition:
+ Addition
- Subtraction
* Multiplication
/ Division
^ power
sin(x)
cos(x)
tan(x)
asin(x)
acos(x)
atan(x)
abs(x)
sqrt(x)

For Variogram - Fitting you can use the following Variogram - Models:
NUG(x)
SPH(x,a)
EXP(x,a)
LIN(x,a)
The Fitting variables are single characters like a,b,m .. alphabetical order with the grid list order ('a'= first var, 'b' = second grid, ...)
Example: m*x+a 

    - ITER [`integer number`] : Iterationen. Minimum: 1 Default: 1000
    - LAMBDA [`floating point number`] : Max Lambda. Minimum: 1.000000 Default: 10000.000000

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('table_calculus', '0', 'Function Fit')
    if Tool.is_Okay():
        Tool.Set_Input ('SOURCE', SOURCE)
        Tool.Set_Option('YFIELD', YFIELD)
        Tool.Set_Option('USE_X', USE_X)
        Tool.Set_Option('XFIELD', XFIELD)
        Tool.Set_Option('FORMEL', FORMEL)
        Tool.Set_Option('ITER', ITER)
        Tool.Set_Option('LAMBDA', LAMBDA)
        return Tool.Execute(Verbose)
    return False

def Field_Calculator(TABLE=None, RESULT_TABLE=None, RESULT_SHAPES=None, FIELD=None, NAME=None, FORMULA=None, SELECTION=None, USE_NODATA=None, Verbose=2):
    '''
    Field Calculator
    ----------
    [table_calculus.1]\n
    The table calculator calculates a new attribute from existing attributes based on a mathematical formula. Attributes are addressed by the character 'f' (for 'field') followed by the field number (i.e.: f1, f2, ..., fn) or by the field name in quotation marks or square brackets (e.g.: "Field Name").\n
    Examples:\n
    - sin(f1) * f2 + f3\n
    - "Population" / "Area"\n
    One can also use the drop-down-menu to append fields numbers to the formula.\n
    If the use no-data flag is unchecked and a no-data value appears in a record's input, no calculation is performed for it and the result is set to no-data.\n
    Following operators are available for the formula definition:\n
    ============\n
    [+]	Addition\n
    [-]	Subtraction\n
    [*]	Multiplication\n
    [/]	Division\n
    [abs(x)]	Absolute Value\n
    [mod(x, y)]	Returns the floating point remainder of x/y\n
    [int(x)]	Returns the integer part of floating point value x\n
    [sqr(x)]	Square\n
    [sqrt(x)]	Square Root\n
    [exp(x)]	Exponential\n
    [pow(x, y)]	Returns x raised to the power of y\n
    [x ^ y]	Returns x raised to the power of y\n
    [ln(x)]	Natural Logarithm\n
    [log(x)]	Base 10 Logarithm\n
    [pi()]	Returns the value of Pi\n
    [sin(x)]	Sine, expects radians\n
    [cos(x)]	Cosine, expects radians\n
    [tan(x)]	Tangent, expects radians\n
    [asin(x)]	Arcsine, returns radians\n
    [acos(x)]	Arccosine, returns radians\n
    [atan(x)]	Arctangent, returns radians\n
    [atan2(x, y)]	Arctangent of x/y, returns radians\n
    [min(x, y)]	Returns the minimum of values x and y\n
    [max(x, y)]	Returns the maximum of values x and y\n
    [gt(x, y)]	Returns true (1), if x is greater than y, else false (0)\n
    [x > y]	Returns true (1), if x is greater than y, else false (0)\n
    [lt(x, y)]	Returns true (1), if x is less than y, else false (0)\n
    [x &lt; y]	Returns true (1), if x is less than y, else false (0)\n
    [eq(x, y)]	Returns true (1), if x equals y, else false (0)\n
    [x = y]	Returns true (1), if x equals y, else false (0)\n
    [and(x, y)]	Returns true (1), if both x and y are true (i.e. not 0)\n
    [or(x, y)]	Returns true (1), if at least one of both x and y is true (i.e. not 0)\n
    [ifelse(c, x, y)]	Returns x, if condition c is true (i.e. not 0), else y\n
    [rand_u(x, y)]	Random number, uniform distribution with minimum x and maximum y\n
    [rand_g(x, y)]	Random number, Gaussian distribution with mean x and standard deviation y\n
    [nodata()]	Returns tables's no-data value\n
    [isnodata(x)]	Returns true (1), if x is a no-data value, else false (0)\n
    ============\n
    Arguments
    ----------
    - TABLE [`input table`] : Table
    - RESULT_TABLE [`output table`] : Result
    - RESULT_SHAPES [`output shapes`] : Result
    - FIELD [`table field`] : Result Field. Select a field for the results. If not set a new field for the results will be added.
    - NAME [`text`] : Field Name. Default: Calculation
    - FORMULA [`text`] : Formula. Default: f1 + f2
    - SELECTION [`boolean`] : Selection. Default: 1
    - USE_NODATA [`boolean`] : Use No-Data. Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('table_calculus', '1', 'Field Calculator')
    if Tool.is_Okay():
        Tool.Set_Input ('TABLE', TABLE)
        Tool.Set_Output('RESULT_TABLE', RESULT_TABLE)
        Tool.Set_Output('RESULT_SHAPES', RESULT_SHAPES)
        Tool.Set_Option('FIELD', FIELD)
        Tool.Set_Option('NAME', NAME)
        Tool.Set_Option('FORMULA', FORMULA)
        Tool.Set_Option('SELECTION', SELECTION)
        Tool.Set_Option('USE_NODATA', USE_NODATA)
        return Tool.Execute(Verbose)
    return False

def run_tool_table_calculus_1(TABLE=None, RESULT_TABLE=None, RESULT_SHAPES=None, FIELD=None, NAME=None, FORMULA=None, SELECTION=None, USE_NODATA=None, Verbose=2):
    '''
    Field Calculator
    ----------
    [table_calculus.1]\n
    The table calculator calculates a new attribute from existing attributes based on a mathematical formula. Attributes are addressed by the character 'f' (for 'field') followed by the field number (i.e.: f1, f2, ..., fn) or by the field name in quotation marks or square brackets (e.g.: "Field Name").\n
    Examples:\n
    - sin(f1) * f2 + f3\n
    - "Population" / "Area"\n
    One can also use the drop-down-menu to append fields numbers to the formula.\n
    If the use no-data flag is unchecked and a no-data value appears in a record's input, no calculation is performed for it and the result is set to no-data.\n
    Following operators are available for the formula definition:\n
    ============\n
    [+]	Addition\n
    [-]	Subtraction\n
    [*]	Multiplication\n
    [/]	Division\n
    [abs(x)]	Absolute Value\n
    [mod(x, y)]	Returns the floating point remainder of x/y\n
    [int(x)]	Returns the integer part of floating point value x\n
    [sqr(x)]	Square\n
    [sqrt(x)]	Square Root\n
    [exp(x)]	Exponential\n
    [pow(x, y)]	Returns x raised to the power of y\n
    [x ^ y]	Returns x raised to the power of y\n
    [ln(x)]	Natural Logarithm\n
    [log(x)]	Base 10 Logarithm\n
    [pi()]	Returns the value of Pi\n
    [sin(x)]	Sine, expects radians\n
    [cos(x)]	Cosine, expects radians\n
    [tan(x)]	Tangent, expects radians\n
    [asin(x)]	Arcsine, returns radians\n
    [acos(x)]	Arccosine, returns radians\n
    [atan(x)]	Arctangent, returns radians\n
    [atan2(x, y)]	Arctangent of x/y, returns radians\n
    [min(x, y)]	Returns the minimum of values x and y\n
    [max(x, y)]	Returns the maximum of values x and y\n
    [gt(x, y)]	Returns true (1), if x is greater than y, else false (0)\n
    [x > y]	Returns true (1), if x is greater than y, else false (0)\n
    [lt(x, y)]	Returns true (1), if x is less than y, else false (0)\n
    [x &lt; y]	Returns true (1), if x is less than y, else false (0)\n
    [eq(x, y)]	Returns true (1), if x equals y, else false (0)\n
    [x = y]	Returns true (1), if x equals y, else false (0)\n
    [and(x, y)]	Returns true (1), if both x and y are true (i.e. not 0)\n
    [or(x, y)]	Returns true (1), if at least one of both x and y is true (i.e. not 0)\n
    [ifelse(c, x, y)]	Returns x, if condition c is true (i.e. not 0), else y\n
    [rand_u(x, y)]	Random number, uniform distribution with minimum x and maximum y\n
    [rand_g(x, y)]	Random number, Gaussian distribution with mean x and standard deviation y\n
    [nodata()]	Returns tables's no-data value\n
    [isnodata(x)]	Returns true (1), if x is a no-data value, else false (0)\n
    ============\n
    Arguments
    ----------
    - TABLE [`input table`] : Table
    - RESULT_TABLE [`output table`] : Result
    - RESULT_SHAPES [`output shapes`] : Result
    - FIELD [`table field`] : Result Field. Select a field for the results. If not set a new field for the results will be added.
    - NAME [`text`] : Field Name. Default: Calculation
    - FORMULA [`text`] : Formula. Default: f1 + f2
    - SELECTION [`boolean`] : Selection. Default: 1
    - USE_NODATA [`boolean`] : Use No-Data. Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('table_calculus', '1', 'Field Calculator')
    if Tool.is_Okay():
        Tool.Set_Input ('TABLE', TABLE)
        Tool.Set_Output('RESULT_TABLE', RESULT_TABLE)
        Tool.Set_Output('RESULT_SHAPES', RESULT_SHAPES)
        Tool.Set_Option('FIELD', FIELD)
        Tool.Set_Option('NAME', NAME)
        Tool.Set_Option('FORMULA', FORMULA)
        Tool.Set_Option('SELECTION', SELECTION)
        Tool.Set_Option('USE_NODATA', USE_NODATA)
        return Tool.Execute(Verbose)
    return False

def Moving_Statistics(INPUT=None, OUTPUT=None, FIELD=None, INDEX=None, MISSING=None, OFFSET=None, LENGTH=None, MEAN=None, FIELD_MEAN=None, MEDIAN=None, FIELD_MEDIAN=None, MIN=None, FIELD_MIN=None, MAX=None, FIELD_MAX=None, STDV=None, FIELD_STDV=None, STDV_LO=None, FIELD_STDV_LO=None, STDV_HI=None, FIELD_STDV_HI=None, Verbose=2):
    '''
    Moving Statistics
    ----------
    [table_calculus.5]\n
    For each record the statistics over the selected attribute and the specified number of records is calculated.\n
    Arguments
    ----------
    - INPUT [`input table`] : Input
    - OUTPUT [`output table`] : Output
    - FIELD [`table field`] : Field
    - INDEX [`table field`] : Order by...
    - MISSING [`choice`] : Missing Records. Available Choices: [0] complete records [1] all records in range [2] all records Default: 0 Create output option. If 'complete' specified number of records must be found, 'in range' ignores no-data in found records.
    - OFFSET [`integer number`] : Offset. Default: 5 The number of preceding records to be taken into account for statistics.
    - LENGTH [`integer number`] : Number of Records. Minimum: 1 Default: 11 The total number of records to be taken into account for statistics.
    - MEAN [`boolean`] : Mean. Default: 1
    - FIELD_MEAN [`table field`] : Mean
    - MEDIAN [`boolean`] : Median. Default: 0
    - FIELD_MEDIAN [`table field`] : Median
    - MIN [`boolean`] : Minimum. Default: 0
    - FIELD_MIN [`table field`] : Minimum
    - MAX [`boolean`] : Maximum. Default: 0
    - FIELD_MAX [`table field`] : Maximum
    - STDV [`boolean`] : Standard Deviation. Default: 0
    - FIELD_STDV [`table field`] : Standard Deviation
    - STDV_LO [`boolean`] : Lower Standard Deviation. Default: 0
    - FIELD_STDV_LO [`table field`] : Lower Standard Deviation
    - STDV_HI [`boolean`] : Higher Standard Deviation. Default: 0
    - FIELD_STDV_HI [`table field`] : Higher Standard Deviation

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('table_calculus', '5', 'Moving Statistics')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Output('OUTPUT', OUTPUT)
        Tool.Set_Option('FIELD', FIELD)
        Tool.Set_Option('INDEX', INDEX)
        Tool.Set_Option('MISSING', MISSING)
        Tool.Set_Option('OFFSET', OFFSET)
        Tool.Set_Option('LENGTH', LENGTH)
        Tool.Set_Option('MEAN', MEAN)
        Tool.Set_Option('FIELD_MEAN', FIELD_MEAN)
        Tool.Set_Option('MEDIAN', MEDIAN)
        Tool.Set_Option('FIELD_MEDIAN', FIELD_MEDIAN)
        Tool.Set_Option('MIN', MIN)
        Tool.Set_Option('FIELD_MIN', FIELD_MIN)
        Tool.Set_Option('MAX', MAX)
        Tool.Set_Option('FIELD_MAX', FIELD_MAX)
        Tool.Set_Option('STDV', STDV)
        Tool.Set_Option('FIELD_STDV', FIELD_STDV)
        Tool.Set_Option('STDV_LO', STDV_LO)
        Tool.Set_Option('FIELD_STDV_LO', FIELD_STDV_LO)
        Tool.Set_Option('STDV_HI', STDV_HI)
        Tool.Set_Option('FIELD_STDV_HI', FIELD_STDV_HI)
        return Tool.Execute(Verbose)
    return False

def run_tool_table_calculus_5(INPUT=None, OUTPUT=None, FIELD=None, INDEX=None, MISSING=None, OFFSET=None, LENGTH=None, MEAN=None, FIELD_MEAN=None, MEDIAN=None, FIELD_MEDIAN=None, MIN=None, FIELD_MIN=None, MAX=None, FIELD_MAX=None, STDV=None, FIELD_STDV=None, STDV_LO=None, FIELD_STDV_LO=None, STDV_HI=None, FIELD_STDV_HI=None, Verbose=2):
    '''
    Moving Statistics
    ----------
    [table_calculus.5]\n
    For each record the statistics over the selected attribute and the specified number of records is calculated.\n
    Arguments
    ----------
    - INPUT [`input table`] : Input
    - OUTPUT [`output table`] : Output
    - FIELD [`table field`] : Field
    - INDEX [`table field`] : Order by...
    - MISSING [`choice`] : Missing Records. Available Choices: [0] complete records [1] all records in range [2] all records Default: 0 Create output option. If 'complete' specified number of records must be found, 'in range' ignores no-data in found records.
    - OFFSET [`integer number`] : Offset. Default: 5 The number of preceding records to be taken into account for statistics.
    - LENGTH [`integer number`] : Number of Records. Minimum: 1 Default: 11 The total number of records to be taken into account for statistics.
    - MEAN [`boolean`] : Mean. Default: 1
    - FIELD_MEAN [`table field`] : Mean
    - MEDIAN [`boolean`] : Median. Default: 0
    - FIELD_MEDIAN [`table field`] : Median
    - MIN [`boolean`] : Minimum. Default: 0
    - FIELD_MIN [`table field`] : Minimum
    - MAX [`boolean`] : Maximum. Default: 0
    - FIELD_MAX [`table field`] : Maximum
    - STDV [`boolean`] : Standard Deviation. Default: 0
    - FIELD_STDV [`table field`] : Standard Deviation
    - STDV_LO [`boolean`] : Lower Standard Deviation. Default: 0
    - FIELD_STDV_LO [`table field`] : Lower Standard Deviation
    - STDV_HI [`boolean`] : Higher Standard Deviation. Default: 0
    - FIELD_STDV_HI [`table field`] : Higher Standard Deviation

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('table_calculus', '5', 'Moving Statistics')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Output('OUTPUT', OUTPUT)
        Tool.Set_Option('FIELD', FIELD)
        Tool.Set_Option('INDEX', INDEX)
        Tool.Set_Option('MISSING', MISSING)
        Tool.Set_Option('OFFSET', OFFSET)
        Tool.Set_Option('LENGTH', LENGTH)
        Tool.Set_Option('MEAN', MEAN)
        Tool.Set_Option('FIELD_MEAN', FIELD_MEAN)
        Tool.Set_Option('MEDIAN', MEDIAN)
        Tool.Set_Option('FIELD_MEDIAN', FIELD_MEDIAN)
        Tool.Set_Option('MIN', MIN)
        Tool.Set_Option('FIELD_MIN', FIELD_MIN)
        Tool.Set_Option('MAX', MAX)
        Tool.Set_Option('FIELD_MAX', FIELD_MAX)
        Tool.Set_Option('STDV', STDV)
        Tool.Set_Option('FIELD_STDV', FIELD_STDV)
        Tool.Set_Option('STDV_LO', STDV_LO)
        Tool.Set_Option('FIELD_STDV_LO', FIELD_STDV_LO)
        Tool.Set_Option('STDV_HI', STDV_HI)
        Tool.Set_Option('FIELD_STDV_HI', FIELD_STDV_HI)
        return Tool.Execute(Verbose)
    return False

def Principal_Component_Analysis(TABLE=None, PCA=None, METHOD=None, NFIRST=None, Verbose=2):
    '''
    Principal Component Analysis
    ----------
    [table_calculus.7]\n
    Principal Component Analysis (PCA) for tables.\n
    Arguments
    ----------
    - TABLE [`input table`] : Table
    - PCA [`output table`] : Principal Components
    - METHOD [`choice`] : Method. Available Choices: [0] correlation matrix [1] variance-covariance matrix [2] sums-of-squares-and-cross-products matrix Default: 1
    - NFIRST [`integer number`] : Number of Components. Minimum: 0 Default: 3 maximum number of calculated first components; set to zero to get all

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('table_calculus', '7', 'Principal Component Analysis')
    if Tool.is_Okay():
        Tool.Set_Input ('TABLE', TABLE)
        Tool.Set_Output('PCA', PCA)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('NFIRST', NFIRST)
        return Tool.Execute(Verbose)
    return False

def run_tool_table_calculus_7(TABLE=None, PCA=None, METHOD=None, NFIRST=None, Verbose=2):
    '''
    Principal Component Analysis
    ----------
    [table_calculus.7]\n
    Principal Component Analysis (PCA) for tables.\n
    Arguments
    ----------
    - TABLE [`input table`] : Table
    - PCA [`output table`] : Principal Components
    - METHOD [`choice`] : Method. Available Choices: [0] correlation matrix [1] variance-covariance matrix [2] sums-of-squares-and-cross-products matrix Default: 1
    - NFIRST [`integer number`] : Number of Components. Minimum: 0 Default: 3 maximum number of calculated first components; set to zero to get all

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('table_calculus', '7', 'Principal Component Analysis')
    if Tool.is_Okay():
        Tool.Set_Input ('TABLE', TABLE)
        Tool.Set_Output('PCA', PCA)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('NFIRST', NFIRST)
        return Tool.Execute(Verbose)
    return False

def Fill_Gaps_in_Ordered_Records(TABLE=None, NOGAPS=None, ORDER=None, METHOD=None, Verbose=2):
    '''
    Fill Gaps in Ordered Records
    ----------
    [table_calculus.8]\n
    This tool inserts records where the chosen order field has gaps expecting an increment of one. It is assumed that the order field represents integers.\n
    Arguments
    ----------
    - TABLE [`input table`] : Table
    - NOGAPS [`output table`] : Table without Gaps
    - ORDER [`table field`] : Order
    - METHOD [`choice`] : Interpolation. Available Choices: [0] Nearest Neighbour [1] Linear [2] Spline Default: 1

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('table_calculus', '8', 'Fill Gaps in Ordered Records')
    if Tool.is_Okay():
        Tool.Set_Input ('TABLE', TABLE)
        Tool.Set_Output('NOGAPS', NOGAPS)
        Tool.Set_Option('ORDER', ORDER)
        Tool.Set_Option('METHOD', METHOD)
        return Tool.Execute(Verbose)
    return False

def run_tool_table_calculus_8(TABLE=None, NOGAPS=None, ORDER=None, METHOD=None, Verbose=2):
    '''
    Fill Gaps in Ordered Records
    ----------
    [table_calculus.8]\n
    This tool inserts records where the chosen order field has gaps expecting an increment of one. It is assumed that the order field represents integers.\n
    Arguments
    ----------
    - TABLE [`input table`] : Table
    - NOGAPS [`output table`] : Table without Gaps
    - ORDER [`table field`] : Order
    - METHOD [`choice`] : Interpolation. Available Choices: [0] Nearest Neighbour [1] Linear [2] Spline Default: 1

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('table_calculus', '8', 'Fill Gaps in Ordered Records')
    if Tool.is_Okay():
        Tool.Set_Input ('TABLE', TABLE)
        Tool.Set_Output('NOGAPS', NOGAPS)
        Tool.Set_Option('ORDER', ORDER)
        Tool.Set_Option('METHOD', METHOD)
        return Tool.Execute(Verbose)
    return False

def Fill_Gaps_in_Records(TABLE=None, NOGAPS=None, ORDER=None, FIELD=None, METHOD=None, Verbose=2):
    '''
    Fill Gaps in Records
    ----------
    [table_calculus.9]\n
    This tool fills gaps in the table records. for the chosen attribute field it interpolates values for those records, which have no-data, using existing data from the surrounding records. If no order field is specified, simply the record index is taken as coordinate, for which the interpolation will be performed. Notice: extrapolation is not supported, i.e. only those gaps will be filled that have lower and higher values surrounding them following the record order.\n
    Arguments
    ----------
    - TABLE [`input table`] : Table
    - NOGAPS [`output table`] : Table without Gaps
    - ORDER [`table field`] : Order
    - FIELD [`table field`] : Field
    - METHOD [`choice`] : Interpolation. Available Choices: [0] Nearest Neighbour [1] Linear [2] Spline Default: 1

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('table_calculus', '9', 'Fill Gaps in Records')
    if Tool.is_Okay():
        Tool.Set_Input ('TABLE', TABLE)
        Tool.Set_Output('NOGAPS', NOGAPS)
        Tool.Set_Option('ORDER', ORDER)
        Tool.Set_Option('FIELD', FIELD)
        Tool.Set_Option('METHOD', METHOD)
        return Tool.Execute(Verbose)
    return False

def run_tool_table_calculus_9(TABLE=None, NOGAPS=None, ORDER=None, FIELD=None, METHOD=None, Verbose=2):
    '''
    Fill Gaps in Records
    ----------
    [table_calculus.9]\n
    This tool fills gaps in the table records. for the chosen attribute field it interpolates values for those records, which have no-data, using existing data from the surrounding records. If no order field is specified, simply the record index is taken as coordinate, for which the interpolation will be performed. Notice: extrapolation is not supported, i.e. only those gaps will be filled that have lower and higher values surrounding them following the record order.\n
    Arguments
    ----------
    - TABLE [`input table`] : Table
    - NOGAPS [`output table`] : Table without Gaps
    - ORDER [`table field`] : Order
    - FIELD [`table field`] : Field
    - METHOD [`choice`] : Interpolation. Available Choices: [0] Nearest Neighbour [1] Linear [2] Spline Default: 1

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('table_calculus', '9', 'Fill Gaps in Records')
    if Tool.is_Okay():
        Tool.Set_Input ('TABLE', TABLE)
        Tool.Set_Output('NOGAPS', NOGAPS)
        Tool.Set_Option('ORDER', ORDER)
        Tool.Set_Option('FIELD', FIELD)
        Tool.Set_Option('METHOD', METHOD)
        return Tool.Execute(Verbose)
    return False

def Find_Field_of_Extreme_Value(INPUT=None, OUTPUT=None, FIELDS=None, EXTREME_ID=None, EXTREME_VALUE=None, TYPE=None, IDENTIFY=None, Verbose=2):
    '''
    Find Field of Extreme Value
    ----------
    [table_calculus.11]\n
    Identifies from the selected attributes that one, which has the maximum or respectively the minimum value.\n
    Arguments
    ----------
    - INPUT [`input table`] : Input
    - OUTPUT [`output table`] : Output
    - FIELDS [`table fields`] : Attributes
    - EXTREME_ID [`table field`] : Attribute
    - EXTREME_VALUE [`table field`] : Value
    - TYPE [`choice`] : TYPE. Available Choices: [0] minimum [1] maximum Default: 1
    - IDENTIFY [`choice`] : Attribute Identification. Available Choices: [0] name [1] index Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('table_calculus', '11', 'Find Field of Extreme Value')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Output('OUTPUT', OUTPUT)
        Tool.Set_Option('FIELDS', FIELDS)
        Tool.Set_Option('EXTREME_ID', EXTREME_ID)
        Tool.Set_Option('EXTREME_VALUE', EXTREME_VALUE)
        Tool.Set_Option('TYPE', TYPE)
        Tool.Set_Option('IDENTIFY', IDENTIFY)
        return Tool.Execute(Verbose)
    return False

def run_tool_table_calculus_11(INPUT=None, OUTPUT=None, FIELDS=None, EXTREME_ID=None, EXTREME_VALUE=None, TYPE=None, IDENTIFY=None, Verbose=2):
    '''
    Find Field of Extreme Value
    ----------
    [table_calculus.11]\n
    Identifies from the selected attributes that one, which has the maximum or respectively the minimum value.\n
    Arguments
    ----------
    - INPUT [`input table`] : Input
    - OUTPUT [`output table`] : Output
    - FIELDS [`table fields`] : Attributes
    - EXTREME_ID [`table field`] : Attribute
    - EXTREME_VALUE [`table field`] : Value
    - TYPE [`choice`] : TYPE. Available Choices: [0] minimum [1] maximum Default: 1
    - IDENTIFY [`choice`] : Attribute Identification. Available Choices: [0] name [1] index Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('table_calculus', '11', 'Find Field of Extreme Value')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Output('OUTPUT', OUTPUT)
        Tool.Set_Option('FIELDS', FIELDS)
        Tool.Set_Option('EXTREME_ID', EXTREME_ID)
        Tool.Set_Option('EXTREME_VALUE', EXTREME_VALUE)
        Tool.Set_Option('TYPE', TYPE)
        Tool.Set_Option('IDENTIFY', IDENTIFY)
        return Tool.Execute(Verbose)
    return False

def Minimum_Redundancy_Feature_Selection(DATA=None, SELECTION=None, CLASS=None, VERBOSE=None, MRMR_NFEATURES=None, MRMR_DISCRETIZE=None, MRMR_THRESHOLD=None, MRMR_METHOD=None, Verbose=2):
    '''
    Minimum Redundancy Feature Selection
    ----------
    [table_calculus.12]\n
    Identify the most relevant features for subsequent classification of tabular data.\n
    The minimum Redundancy Maximum Relevance (mRMR) feature selection algorithm has been developed by Hanchuan Peng <hanchuan.peng@gmail.com>.\n
    References:\n
    Feature selection based on mutual information: criteria of max-dependency, max-relevance, and min-redundancy. Hanchuan Peng, Fuhui Long, and Chris Ding, IEEE Transactions on Pattern Analysis and Machine Intelligence, Vol. 27, No. 8, pp.1226-1238, 2005.\n
    Minimum redundancy feature selection from microarray gene expression data,\n
    Chris Ding, and Hanchuan Peng, Journal of Bioinformatics and Computational Biology, Vol. 3, No. 2, pp.185-205, 2005.\n
    Hanchuan Peng's mRMR Homepage at [http://penglab.janelia.org/proj/mRMR/](http://penglab.janelia.org/proj/mRMR/)\n
    Arguments
    ----------
    - DATA [`input table`] : Data
    - SELECTION [`output table`] : Feature Selection
    - CLASS [`table field`] : Class Identifier
    - VERBOSE [`boolean`] : Verbose Output. Default: 1 output of intermediate results to execution message window
    - MRMR_NFEATURES [`integer number`] : Number of Features. Minimum: 1 Default: 50
    - MRMR_DISCRETIZE [`boolean`] : Discretization. Default: 1 uncheck this means no discretizaton (i.e. data is already integer)
    - MRMR_THRESHOLD [`floating point number`] : Discretization Threshold. Minimum: 0.000000 Default: 1.000000 a double number of the discretization threshold; set to 0 to make binarization
    - MRMR_METHOD [`choice`] : Selection Method. Available Choices: [0] Mutual Information Difference (MID) [1] Mutual Information Quotient (MIQ) Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('table_calculus', '12', 'Minimum Redundancy Feature Selection')
    if Tool.is_Okay():
        Tool.Set_Input ('DATA', DATA)
        Tool.Set_Output('SELECTION', SELECTION)
        Tool.Set_Option('CLASS', CLASS)
        Tool.Set_Option('VERBOSE', VERBOSE)
        Tool.Set_Option('mRMR_NFEATURES', MRMR_NFEATURES)
        Tool.Set_Option('mRMR_DISCRETIZE', MRMR_DISCRETIZE)
        Tool.Set_Option('mRMR_THRESHOLD', MRMR_THRESHOLD)
        Tool.Set_Option('mRMR_METHOD', MRMR_METHOD)
        return Tool.Execute(Verbose)
    return False

def run_tool_table_calculus_12(DATA=None, SELECTION=None, CLASS=None, VERBOSE=None, MRMR_NFEATURES=None, MRMR_DISCRETIZE=None, MRMR_THRESHOLD=None, MRMR_METHOD=None, Verbose=2):
    '''
    Minimum Redundancy Feature Selection
    ----------
    [table_calculus.12]\n
    Identify the most relevant features for subsequent classification of tabular data.\n
    The minimum Redundancy Maximum Relevance (mRMR) feature selection algorithm has been developed by Hanchuan Peng <hanchuan.peng@gmail.com>.\n
    References:\n
    Feature selection based on mutual information: criteria of max-dependency, max-relevance, and min-redundancy. Hanchuan Peng, Fuhui Long, and Chris Ding, IEEE Transactions on Pattern Analysis and Machine Intelligence, Vol. 27, No. 8, pp.1226-1238, 2005.\n
    Minimum redundancy feature selection from microarray gene expression data,\n
    Chris Ding, and Hanchuan Peng, Journal of Bioinformatics and Computational Biology, Vol. 3, No. 2, pp.185-205, 2005.\n
    Hanchuan Peng's mRMR Homepage at [http://penglab.janelia.org/proj/mRMR/](http://penglab.janelia.org/proj/mRMR/)\n
    Arguments
    ----------
    - DATA [`input table`] : Data
    - SELECTION [`output table`] : Feature Selection
    - CLASS [`table field`] : Class Identifier
    - VERBOSE [`boolean`] : Verbose Output. Default: 1 output of intermediate results to execution message window
    - MRMR_NFEATURES [`integer number`] : Number of Features. Minimum: 1 Default: 50
    - MRMR_DISCRETIZE [`boolean`] : Discretization. Default: 1 uncheck this means no discretizaton (i.e. data is already integer)
    - MRMR_THRESHOLD [`floating point number`] : Discretization Threshold. Minimum: 0.000000 Default: 1.000000 a double number of the discretization threshold; set to 0 to make binarization
    - MRMR_METHOD [`choice`] : Selection Method. Available Choices: [0] Mutual Information Difference (MID) [1] Mutual Information Quotient (MIQ) Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('table_calculus', '12', 'Minimum Redundancy Feature Selection')
    if Tool.is_Okay():
        Tool.Set_Input ('DATA', DATA)
        Tool.Set_Output('SELECTION', SELECTION)
        Tool.Set_Option('CLASS', CLASS)
        Tool.Set_Option('VERBOSE', VERBOSE)
        Tool.Set_Option('mRMR_NFEATURES', MRMR_NFEATURES)
        Tool.Set_Option('mRMR_DISCRETIZE', MRMR_DISCRETIZE)
        Tool.Set_Option('mRMR_THRESHOLD', MRMR_THRESHOLD)
        Tool.Set_Option('mRMR_METHOD', MRMR_METHOD)
        return Tool.Execute(Verbose)
    return False

def Field_Statistics(TABLE=None, STATISTICS=None, FIELDS=None, Verbose=2):
    '''
    Field Statistics
    ----------
    [table_calculus.15]\n
    The tools allows one to calculate statistics (n, min, max, range, sum, mean, variance and standard deviation) for attribute fields of tables, shapefiles or point clouds.\n
    Arguments
    ----------
    - TABLE [`input table`] : Table. The input table.
    - STATISTICS [`output table`] : Statistics. The calculated statistics.
    - FIELDS [`table fields`] : Attributes. The (numeric) fields to calculate the statistics for.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('table_calculus', '15', 'Field Statistics')
    if Tool.is_Okay():
        Tool.Set_Input ('TABLE', TABLE)
        Tool.Set_Output('STATISTICS', STATISTICS)
        Tool.Set_Option('FIELDS', FIELDS)
        return Tool.Execute(Verbose)
    return False

def run_tool_table_calculus_15(TABLE=None, STATISTICS=None, FIELDS=None, Verbose=2):
    '''
    Field Statistics
    ----------
    [table_calculus.15]\n
    The tools allows one to calculate statistics (n, min, max, range, sum, mean, variance and standard deviation) for attribute fields of tables, shapefiles or point clouds.\n
    Arguments
    ----------
    - TABLE [`input table`] : Table. The input table.
    - STATISTICS [`output table`] : Statistics. The calculated statistics.
    - FIELDS [`table fields`] : Attributes. The (numeric) fields to calculate the statistics for.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('table_calculus', '15', 'Field Statistics')
    if Tool.is_Okay():
        Tool.Set_Input ('TABLE', TABLE)
        Tool.Set_Output('STATISTICS', STATISTICS)
        Tool.Set_Option('FIELDS', FIELDS)
        return Tool.Execute(Verbose)
    return False

def Record_Statistics(TABLE=None, RESULT_TABLE=None, RESULT_SHAPES=None, FIELDS=None, MEAN=None, MIN=None, MAX=None, RANGE=None, SUM=None, NUM=None, VAR=None, STDDEV=None, PCTL=None, PCTL_VAL=None, Verbose=2):
    '''
    Record Statistics
    ----------
    [table_calculus.16]\n
    This tool calculates record-wise the statistics over the selected attribute fields.\n
    Arguments
    ----------
    - TABLE [`input table`] : Table
    - RESULT_TABLE [`output table`] : Result
    - RESULT_SHAPES [`output shapes`] : Result
    - FIELDS [`table fields`] : Attributes. If no field is selected statistics will be built from all numeric fields.
    - MEAN [`boolean`] : Arithmetic Mean. Default: 0
    - MIN [`boolean`] : Minimum. Default: 0
    - MAX [`boolean`] : Maximum. Default: 0
    - RANGE [`boolean`] : Range. Default: 0
    - SUM [`boolean`] : Sum. Default: 0
    - NUM [`boolean`] : Number of Values. Default: 0
    - VAR [`boolean`] : Variance. Default: 0
    - STDDEV [`boolean`] : Standard Deviation. Default: 0
    - PCTL [`boolean`] : Percentile. Default: 0
    - PCTL_VAL [`floating point number`] : Value. Minimum: 0.000000 Maximum: 100.000000 Default: 50.000000

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('table_calculus', '16', 'Record Statistics')
    if Tool.is_Okay():
        Tool.Set_Input ('TABLE', TABLE)
        Tool.Set_Output('RESULT_TABLE', RESULT_TABLE)
        Tool.Set_Output('RESULT_SHAPES', RESULT_SHAPES)
        Tool.Set_Option('FIELDS', FIELDS)
        Tool.Set_Option('MEAN', MEAN)
        Tool.Set_Option('MIN', MIN)
        Tool.Set_Option('MAX', MAX)
        Tool.Set_Option('RANGE', RANGE)
        Tool.Set_Option('SUM', SUM)
        Tool.Set_Option('NUM', NUM)
        Tool.Set_Option('VAR', VAR)
        Tool.Set_Option('STDDEV', STDDEV)
        Tool.Set_Option('PCTL', PCTL)
        Tool.Set_Option('PCTL_VAL', PCTL_VAL)
        return Tool.Execute(Verbose)
    return False

def run_tool_table_calculus_16(TABLE=None, RESULT_TABLE=None, RESULT_SHAPES=None, FIELDS=None, MEAN=None, MIN=None, MAX=None, RANGE=None, SUM=None, NUM=None, VAR=None, STDDEV=None, PCTL=None, PCTL_VAL=None, Verbose=2):
    '''
    Record Statistics
    ----------
    [table_calculus.16]\n
    This tool calculates record-wise the statistics over the selected attribute fields.\n
    Arguments
    ----------
    - TABLE [`input table`] : Table
    - RESULT_TABLE [`output table`] : Result
    - RESULT_SHAPES [`output shapes`] : Result
    - FIELDS [`table fields`] : Attributes. If no field is selected statistics will be built from all numeric fields.
    - MEAN [`boolean`] : Arithmetic Mean. Default: 0
    - MIN [`boolean`] : Minimum. Default: 0
    - MAX [`boolean`] : Maximum. Default: 0
    - RANGE [`boolean`] : Range. Default: 0
    - SUM [`boolean`] : Sum. Default: 0
    - NUM [`boolean`] : Number of Values. Default: 0
    - VAR [`boolean`] : Variance. Default: 0
    - STDDEV [`boolean`] : Standard Deviation. Default: 0
    - PCTL [`boolean`] : Percentile. Default: 0
    - PCTL_VAL [`floating point number`] : Value. Minimum: 0.000000 Maximum: 100.000000 Default: 50.000000

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('table_calculus', '16', 'Record Statistics')
    if Tool.is_Okay():
        Tool.Set_Input ('TABLE', TABLE)
        Tool.Set_Output('RESULT_TABLE', RESULT_TABLE)
        Tool.Set_Output('RESULT_SHAPES', RESULT_SHAPES)
        Tool.Set_Option('FIELDS', FIELDS)
        Tool.Set_Option('MEAN', MEAN)
        Tool.Set_Option('MIN', MIN)
        Tool.Set_Option('MAX', MAX)
        Tool.Set_Option('RANGE', RANGE)
        Tool.Set_Option('SUM', SUM)
        Tool.Set_Option('NUM', NUM)
        Tool.Set_Option('VAR', VAR)
        Tool.Set_Option('STDDEV', STDDEV)
        Tool.Set_Option('PCTL', PCTL)
        Tool.Set_Option('PCTL_VAL', PCTL_VAL)
        return Tool.Execute(Verbose)
    return False

def Aggregate_Values_by_Attributes(TABLE=None, AGGREGATED=None, FIELDS=None, STATISTICS=None, STAT_SUM=None, STAT_AVG=None, STAT_MIN=None, STAT_MAX=None, STAT_RNG=None, STAT_DEV=None, STAT_VAR=None, STAT_LST=None, STAT_NUM=None, STAT_NAMING=None, Verbose=2):
    '''
    Aggregate Values by Attributes
    ----------
    [table_calculus.18]\n
    Aggregate Values by Attributes\n
    Arguments
    ----------
    - TABLE [`input table`] : Table
    - AGGREGATED [`output table`] : Output. The calculated statistics.
    - FIELDS [`table fields`] : Aggregate by Field(s)
    - STATISTICS [`table fields`] : Statistics Field(s)
    - STAT_SUM [`boolean`] : Sum. Default: 0
    - STAT_AVG [`boolean`] : Mean. Default: 1
    - STAT_MIN [`boolean`] : Minimum. Default: 0
    - STAT_MAX [`boolean`] : Maximum. Default: 0
    - STAT_RNG [`boolean`] : Range. Default: 0
    - STAT_DEV [`boolean`] : Deviation. Default: 0
    - STAT_VAR [`boolean`] : Variance. Default: 0
    - STAT_LST [`boolean`] : Listing. Default: 0
    - STAT_NUM [`boolean`] : Count. Default: 0
    - STAT_NAMING [`choice`] : Field Naming. Available Choices: [0] variable type + original name [1] original name + variable type [2] original name [3] variable type Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('table_calculus', '18', 'Aggregate Values by Attributes')
    if Tool.is_Okay():
        Tool.Set_Input ('TABLE', TABLE)
        Tool.Set_Output('AGGREGATED', AGGREGATED)
        Tool.Set_Option('FIELDS', FIELDS)
        Tool.Set_Option('STATISTICS', STATISTICS)
        Tool.Set_Option('STAT_SUM', STAT_SUM)
        Tool.Set_Option('STAT_AVG', STAT_AVG)
        Tool.Set_Option('STAT_MIN', STAT_MIN)
        Tool.Set_Option('STAT_MAX', STAT_MAX)
        Tool.Set_Option('STAT_RNG', STAT_RNG)
        Tool.Set_Option('STAT_DEV', STAT_DEV)
        Tool.Set_Option('STAT_VAR', STAT_VAR)
        Tool.Set_Option('STAT_LST', STAT_LST)
        Tool.Set_Option('STAT_NUM', STAT_NUM)
        Tool.Set_Option('STAT_NAMING', STAT_NAMING)
        return Tool.Execute(Verbose)
    return False

def run_tool_table_calculus_18(TABLE=None, AGGREGATED=None, FIELDS=None, STATISTICS=None, STAT_SUM=None, STAT_AVG=None, STAT_MIN=None, STAT_MAX=None, STAT_RNG=None, STAT_DEV=None, STAT_VAR=None, STAT_LST=None, STAT_NUM=None, STAT_NAMING=None, Verbose=2):
    '''
    Aggregate Values by Attributes
    ----------
    [table_calculus.18]\n
    Aggregate Values by Attributes\n
    Arguments
    ----------
    - TABLE [`input table`] : Table
    - AGGREGATED [`output table`] : Output. The calculated statistics.
    - FIELDS [`table fields`] : Aggregate by Field(s)
    - STATISTICS [`table fields`] : Statistics Field(s)
    - STAT_SUM [`boolean`] : Sum. Default: 0
    - STAT_AVG [`boolean`] : Mean. Default: 1
    - STAT_MIN [`boolean`] : Minimum. Default: 0
    - STAT_MAX [`boolean`] : Maximum. Default: 0
    - STAT_RNG [`boolean`] : Range. Default: 0
    - STAT_DEV [`boolean`] : Deviation. Default: 0
    - STAT_VAR [`boolean`] : Variance. Default: 0
    - STAT_LST [`boolean`] : Listing. Default: 0
    - STAT_NUM [`boolean`] : Count. Default: 0
    - STAT_NAMING [`choice`] : Field Naming. Available Choices: [0] variable type + original name [1] original name + variable type [2] original name [3] variable type Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('table_calculus', '18', 'Aggregate Values by Attributes')
    if Tool.is_Okay():
        Tool.Set_Input ('TABLE', TABLE)
        Tool.Set_Output('AGGREGATED', AGGREGATED)
        Tool.Set_Option('FIELDS', FIELDS)
        Tool.Set_Option('STATISTICS', STATISTICS)
        Tool.Set_Option('STAT_SUM', STAT_SUM)
        Tool.Set_Option('STAT_AVG', STAT_AVG)
        Tool.Set_Option('STAT_MIN', STAT_MIN)
        Tool.Set_Option('STAT_MAX', STAT_MAX)
        Tool.Set_Option('STAT_RNG', STAT_RNG)
        Tool.Set_Option('STAT_DEV', STAT_DEV)
        Tool.Set_Option('STAT_VAR', STAT_VAR)
        Tool.Set_Option('STAT_LST', STAT_LST)
        Tool.Set_Option('STAT_NUM', STAT_NUM)
        Tool.Set_Option('STAT_NAMING', STAT_NAMING)
        return Tool.Execute(Verbose)
    return False

def Confusion_Matrix_Table_Fields(TABLE=None, CONFUSION=None, CLASSES=None, SUMMARY=None, FIELD_1=None, FIELD_2=None, NODATA=None, Verbose=2):
    '''
    Confusion Matrix (Table Fields)
    ----------
    [table_calculus.19]\n
    Compares record-wise the values of two table fields and creates a confusion matrix and derived coefficients. Values are expected to represent categories.\n
    Arguments
    ----------
    - TABLE [`input table`] : Table
    - CONFUSION [`output table`] : Confusion Matrix
    - CLASSES [`output table`] : Categories
    - SUMMARY [`output table`] : Summary
    - FIELD_1 [`table field`] : Categories 1
    - FIELD_2 [`table field`] : Categories 2
    - NODATA [`boolean`] : Include No-Data. Default: 1

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('table_calculus', '19', 'Confusion Matrix (Table Fields)')
    if Tool.is_Okay():
        Tool.Set_Input ('TABLE', TABLE)
        Tool.Set_Output('CONFUSION', CONFUSION)
        Tool.Set_Output('CLASSES', CLASSES)
        Tool.Set_Output('SUMMARY', SUMMARY)
        Tool.Set_Option('FIELD_1', FIELD_1)
        Tool.Set_Option('FIELD_2', FIELD_2)
        Tool.Set_Option('NODATA', NODATA)
        return Tool.Execute(Verbose)
    return False

def run_tool_table_calculus_19(TABLE=None, CONFUSION=None, CLASSES=None, SUMMARY=None, FIELD_1=None, FIELD_2=None, NODATA=None, Verbose=2):
    '''
    Confusion Matrix (Table Fields)
    ----------
    [table_calculus.19]\n
    Compares record-wise the values of two table fields and creates a confusion matrix and derived coefficients. Values are expected to represent categories.\n
    Arguments
    ----------
    - TABLE [`input table`] : Table
    - CONFUSION [`output table`] : Confusion Matrix
    - CLASSES [`output table`] : Categories
    - SUMMARY [`output table`] : Summary
    - FIELD_1 [`table field`] : Categories 1
    - FIELD_2 [`table field`] : Categories 2
    - NODATA [`boolean`] : Include No-Data. Default: 1

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('table_calculus', '19', 'Confusion Matrix (Table Fields)')
    if Tool.is_Okay():
        Tool.Set_Input ('TABLE', TABLE)
        Tool.Set_Output('CONFUSION', CONFUSION)
        Tool.Set_Output('CLASSES', CLASSES)
        Tool.Set_Output('SUMMARY', SUMMARY)
        Tool.Set_Option('FIELD_1', FIELD_1)
        Tool.Set_Option('FIELD_2', FIELD_2)
        Tool.Set_Option('NODATA', NODATA)
        return Tool.Execute(Verbose)
    return False

def Record_Aggregation(INPUT=None, OUTPUT=None, FIELD=None, INDEX=None, IDENTIFIER=None, LENGTH=None, MEAN=None, MEDIAN=None, MIN=None, MAX=None, STDV=None, STDV_LO=None, STDV_HI=None, Verbose=2):
    '''
    Record Aggregation
    ----------
    [table_calculus.20]\n
    This is a simple tool to aggregate the values of the selected attribute by statistical means for the given number of records.\n
    Arguments
    ----------
    - INPUT [`input table`] : Table
    - OUTPUT [`output table`] : Aggregation
    - FIELD [`table field`] : Field
    - INDEX [`table field`] : Order by...
    - IDENTIFIER [`table field`] : Identifier
    - LENGTH [`integer number`] : Number of Records. Minimum: 2 Default: 10 The total number of records to be taken into account for aggregated statistics.
    - MEAN [`boolean`] : Mean. Default: 1
    - MEDIAN [`boolean`] : Median. Default: 0
    - MIN [`boolean`] : Minimum. Default: 0
    - MAX [`boolean`] : Maximum. Default: 0
    - STDV [`boolean`] : Standard Deviation. Default: 0
    - STDV_LO [`boolean`] : Lower Standard Deviation. Default: 0
    - STDV_HI [`boolean`] : Higher Standard Deviation. Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('table_calculus', '20', 'Record Aggregation')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Output('OUTPUT', OUTPUT)
        Tool.Set_Option('FIELD', FIELD)
        Tool.Set_Option('INDEX', INDEX)
        Tool.Set_Option('IDENTIFIER', IDENTIFIER)
        Tool.Set_Option('LENGTH', LENGTH)
        Tool.Set_Option('MEAN', MEAN)
        Tool.Set_Option('MEDIAN', MEDIAN)
        Tool.Set_Option('MIN', MIN)
        Tool.Set_Option('MAX', MAX)
        Tool.Set_Option('STDV', STDV)
        Tool.Set_Option('STDV_LO', STDV_LO)
        Tool.Set_Option('STDV_HI', STDV_HI)
        return Tool.Execute(Verbose)
    return False

def run_tool_table_calculus_20(INPUT=None, OUTPUT=None, FIELD=None, INDEX=None, IDENTIFIER=None, LENGTH=None, MEAN=None, MEDIAN=None, MIN=None, MAX=None, STDV=None, STDV_LO=None, STDV_HI=None, Verbose=2):
    '''
    Record Aggregation
    ----------
    [table_calculus.20]\n
    This is a simple tool to aggregate the values of the selected attribute by statistical means for the given number of records.\n
    Arguments
    ----------
    - INPUT [`input table`] : Table
    - OUTPUT [`output table`] : Aggregation
    - FIELD [`table field`] : Field
    - INDEX [`table field`] : Order by...
    - IDENTIFIER [`table field`] : Identifier
    - LENGTH [`integer number`] : Number of Records. Minimum: 2 Default: 10 The total number of records to be taken into account for aggregated statistics.
    - MEAN [`boolean`] : Mean. Default: 1
    - MEDIAN [`boolean`] : Median. Default: 0
    - MIN [`boolean`] : Minimum. Default: 0
    - MAX [`boolean`] : Maximum. Default: 0
    - STDV [`boolean`] : Standard Deviation. Default: 0
    - STDV_LO [`boolean`] : Lower Standard Deviation. Default: 0
    - STDV_HI [`boolean`] : Higher Standard Deviation. Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('table_calculus', '20', 'Record Aggregation')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Output('OUTPUT', OUTPUT)
        Tool.Set_Option('FIELD', FIELD)
        Tool.Set_Option('INDEX', INDEX)
        Tool.Set_Option('IDENTIFIER', IDENTIFIER)
        Tool.Set_Option('LENGTH', LENGTH)
        Tool.Set_Option('MEAN', MEAN)
        Tool.Set_Option('MEDIAN', MEDIAN)
        Tool.Set_Option('MIN', MIN)
        Tool.Set_Option('MAX', MAX)
        Tool.Set_Option('STDV', STDV)
        Tool.Set_Option('STDV_LO', STDV_LO)
        Tool.Set_Option('STDV_HI', STDV_HI)
        return Tool.Execute(Verbose)
    return False

