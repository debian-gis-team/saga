#! /usr/bin/env python

'''
Python Interface to SAGA Tools Library
----------
- Category : Spatial and Geostatistics
- Name     : Regression
- ID       : statistics_regression

Description
----------
Tools for correlation and regression analyses.
'''

from PySAGA.helper import Tool_Wrapper

def Regression_Analysis_Points_and_Predictor_Grid(PREDICTOR=None, POINTS=None, REGRESSION=None, RESIDUAL=None, ATTRIBUTE=None, RESAMPLING=None, METHOD=None, Verbose=2):
    '''
    Regression Analysis (Points and Predictor Grid)
    ----------
    [statistics_regression.0]\n
    Regression analysis of point attributes with a grid as predictor. The regression function is used to create a new grid with regression based values.\n
    Arguments
    ----------
    - PREDICTOR [`input grid`] : Predictor
    - POINTS [`input shapes`] : Observations
    - REGRESSION [`output grid`] : Regression
    - RESIDUAL [`output shapes`] : Residuals
    - ATTRIBUTE [`table field`] : Dependent Variable
    - RESAMPLING [`choice`] : Resampling. Available Choices: [0] Nearest Neighbour [1] Bilinear Interpolation [2] Bicubic Spline Interpolation [3] B-Spline Interpolation Default: 3
    - METHOD [`choice`] : Regression Function. Available Choices: [0] y = a + b * x (linear) [1] y = a + b / x [2] y = a / (b - x) [3] y = a * x^b (power) [4] y = a e^(b * x) (exponential) [5] y = a + b * ln(x) (logarithmic) Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('statistics_regression', '0', 'Regression Analysis (Points and Predictor Grid)')
    if Tool.is_Okay():
        Tool.Set_Input ('PREDICTOR', PREDICTOR)
        Tool.Set_Input ('POINTS', POINTS)
        Tool.Set_Output('REGRESSION', REGRESSION)
        Tool.Set_Output('RESIDUAL', RESIDUAL)
        Tool.Set_Option('ATTRIBUTE', ATTRIBUTE)
        Tool.Set_Option('RESAMPLING', RESAMPLING)
        Tool.Set_Option('METHOD', METHOD)
        return Tool.Execute(Verbose)
    return False

def run_tool_statistics_regression_0(PREDICTOR=None, POINTS=None, REGRESSION=None, RESIDUAL=None, ATTRIBUTE=None, RESAMPLING=None, METHOD=None, Verbose=2):
    '''
    Regression Analysis (Points and Predictor Grid)
    ----------
    [statistics_regression.0]\n
    Regression analysis of point attributes with a grid as predictor. The regression function is used to create a new grid with regression based values.\n
    Arguments
    ----------
    - PREDICTOR [`input grid`] : Predictor
    - POINTS [`input shapes`] : Observations
    - REGRESSION [`output grid`] : Regression
    - RESIDUAL [`output shapes`] : Residuals
    - ATTRIBUTE [`table field`] : Dependent Variable
    - RESAMPLING [`choice`] : Resampling. Available Choices: [0] Nearest Neighbour [1] Bilinear Interpolation [2] Bicubic Spline Interpolation [3] B-Spline Interpolation Default: 3
    - METHOD [`choice`] : Regression Function. Available Choices: [0] y = a + b * x (linear) [1] y = a + b / x [2] y = a / (b - x) [3] y = a * x^b (power) [4] y = a e^(b * x) (exponential) [5] y = a + b * ln(x) (logarithmic) Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('statistics_regression', '0', 'Regression Analysis (Points and Predictor Grid)')
    if Tool.is_Okay():
        Tool.Set_Input ('PREDICTOR', PREDICTOR)
        Tool.Set_Input ('POINTS', POINTS)
        Tool.Set_Output('REGRESSION', REGRESSION)
        Tool.Set_Output('RESIDUAL', RESIDUAL)
        Tool.Set_Option('ATTRIBUTE', ATTRIBUTE)
        Tool.Set_Option('RESAMPLING', RESAMPLING)
        Tool.Set_Option('METHOD', METHOD)
        return Tool.Execute(Verbose)
    return False

def Multiple_Regression_Analysis_Points_and_Predictor_Grids(PREDICTORS=None, POINTS=None, INFO_COEFF=None, INFO_MODEL=None, INFO_STEPS=None, RESIDUALS=None, REGRESSION=None, REGRESCORR=None, ATTRIBUTE=None, RESAMPLING=None, COORD_X=None, COORD_Y=None, INTERCEPT=None, METHOD=None, P_VALUE=None, CROSSVAL=None, CROSSVAL_K=None, RESIDUAL_COR=None, Verbose=2):
    '''
    Multiple Regression Analysis (Points and Predictor Grids)
    ----------
    [statistics_regression.1]\n
    Linear regression analysis of point attributes with multiple grids. Details of the regression/correlation analysis will be saved to a table. The regression function is used to create a new grid with regression based values. The multiple regression analysis uses a forward selection procedure.\n
    Arguments
    ----------
    - PREDICTORS [`input grid list`] : Predictors
    - POINTS [`input shapes`] : Points
    - INFO_COEFF [`output table`] : Details: Coefficients
    - INFO_MODEL [`output table`] : Details: Model
    - INFO_STEPS [`output table`] : Details: Steps
    - RESIDUALS [`output shapes`] : Residuals
    - REGRESSION [`output grid`] : Regression. regression model applied to predictor grids
    - REGRESCORR [`output grid`] : Regression with Residual Correction. regression model applied to predictor grids with interpolated residuals added
    - ATTRIBUTE [`table field`] : Dependent Variable
    - RESAMPLING [`choice`] : Resampling. Available Choices: [0] Nearest Neighbour [1] Bilinear Interpolation [2] Bicubic Spline Interpolation [3] B-Spline Interpolation Default: 3
    - COORD_X [`boolean`] : Include X Coordinate. Default: 0
    - COORD_Y [`boolean`] : Include Y Coordinate. Default: 0
    - INTERCEPT [`boolean`] : Intercept. Default: 1
    - METHOD [`choice`] : Method. Available Choices: [0] include all [1] forward [2] backward [3] stepwise Default: 3
    - P_VALUE [`floating point number`] : Significance Level. Minimum: 0.000000 Maximum: 100.000000 Default: 5.000000 Significance level (aka p-value) as threshold for automated predictor selection, given as percentage
    - CROSSVAL [`choice`] : Cross Validation. Available Choices: [0] none [1] leave one out [2] 2-fold [3] k-fold Default: 0
    - CROSSVAL_K [`integer number`] : Cross Validation Subsamples. Minimum: 2 Default: 10 number of subsamples for k-fold cross validation
    - RESIDUAL_COR [`choice`] : Residual Interpolation. Available Choices: [0] Multilevel B-Spline Interpolation [1] Inverse Distance Weighted Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('statistics_regression', '1', 'Multiple Regression Analysis (Points and Predictor Grids)')
    if Tool.is_Okay():
        Tool.Set_Input ('PREDICTORS', PREDICTORS)
        Tool.Set_Input ('POINTS', POINTS)
        Tool.Set_Output('INFO_COEFF', INFO_COEFF)
        Tool.Set_Output('INFO_MODEL', INFO_MODEL)
        Tool.Set_Output('INFO_STEPS', INFO_STEPS)
        Tool.Set_Output('RESIDUALS', RESIDUALS)
        Tool.Set_Output('REGRESSION', REGRESSION)
        Tool.Set_Output('REGRESCORR', REGRESCORR)
        Tool.Set_Option('ATTRIBUTE', ATTRIBUTE)
        Tool.Set_Option('RESAMPLING', RESAMPLING)
        Tool.Set_Option('COORD_X', COORD_X)
        Tool.Set_Option('COORD_Y', COORD_Y)
        Tool.Set_Option('INTERCEPT', INTERCEPT)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('P_VALUE', P_VALUE)
        Tool.Set_Option('CROSSVAL', CROSSVAL)
        Tool.Set_Option('CROSSVAL_K', CROSSVAL_K)
        Tool.Set_Option('RESIDUAL_COR', RESIDUAL_COR)
        return Tool.Execute(Verbose)
    return False

def run_tool_statistics_regression_1(PREDICTORS=None, POINTS=None, INFO_COEFF=None, INFO_MODEL=None, INFO_STEPS=None, RESIDUALS=None, REGRESSION=None, REGRESCORR=None, ATTRIBUTE=None, RESAMPLING=None, COORD_X=None, COORD_Y=None, INTERCEPT=None, METHOD=None, P_VALUE=None, CROSSVAL=None, CROSSVAL_K=None, RESIDUAL_COR=None, Verbose=2):
    '''
    Multiple Regression Analysis (Points and Predictor Grids)
    ----------
    [statistics_regression.1]\n
    Linear regression analysis of point attributes with multiple grids. Details of the regression/correlation analysis will be saved to a table. The regression function is used to create a new grid with regression based values. The multiple regression analysis uses a forward selection procedure.\n
    Arguments
    ----------
    - PREDICTORS [`input grid list`] : Predictors
    - POINTS [`input shapes`] : Points
    - INFO_COEFF [`output table`] : Details: Coefficients
    - INFO_MODEL [`output table`] : Details: Model
    - INFO_STEPS [`output table`] : Details: Steps
    - RESIDUALS [`output shapes`] : Residuals
    - REGRESSION [`output grid`] : Regression. regression model applied to predictor grids
    - REGRESCORR [`output grid`] : Regression with Residual Correction. regression model applied to predictor grids with interpolated residuals added
    - ATTRIBUTE [`table field`] : Dependent Variable
    - RESAMPLING [`choice`] : Resampling. Available Choices: [0] Nearest Neighbour [1] Bilinear Interpolation [2] Bicubic Spline Interpolation [3] B-Spline Interpolation Default: 3
    - COORD_X [`boolean`] : Include X Coordinate. Default: 0
    - COORD_Y [`boolean`] : Include Y Coordinate. Default: 0
    - INTERCEPT [`boolean`] : Intercept. Default: 1
    - METHOD [`choice`] : Method. Available Choices: [0] include all [1] forward [2] backward [3] stepwise Default: 3
    - P_VALUE [`floating point number`] : Significance Level. Minimum: 0.000000 Maximum: 100.000000 Default: 5.000000 Significance level (aka p-value) as threshold for automated predictor selection, given as percentage
    - CROSSVAL [`choice`] : Cross Validation. Available Choices: [0] none [1] leave one out [2] 2-fold [3] k-fold Default: 0
    - CROSSVAL_K [`integer number`] : Cross Validation Subsamples. Minimum: 2 Default: 10 number of subsamples for k-fold cross validation
    - RESIDUAL_COR [`choice`] : Residual Interpolation. Available Choices: [0] Multilevel B-Spline Interpolation [1] Inverse Distance Weighted Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('statistics_regression', '1', 'Multiple Regression Analysis (Points and Predictor Grids)')
    if Tool.is_Okay():
        Tool.Set_Input ('PREDICTORS', PREDICTORS)
        Tool.Set_Input ('POINTS', POINTS)
        Tool.Set_Output('INFO_COEFF', INFO_COEFF)
        Tool.Set_Output('INFO_MODEL', INFO_MODEL)
        Tool.Set_Output('INFO_STEPS', INFO_STEPS)
        Tool.Set_Output('RESIDUALS', RESIDUALS)
        Tool.Set_Output('REGRESSION', REGRESSION)
        Tool.Set_Output('REGRESCORR', REGRESCORR)
        Tool.Set_Option('ATTRIBUTE', ATTRIBUTE)
        Tool.Set_Option('RESAMPLING', RESAMPLING)
        Tool.Set_Option('COORD_X', COORD_X)
        Tool.Set_Option('COORD_Y', COORD_Y)
        Tool.Set_Option('INTERCEPT', INTERCEPT)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('P_VALUE', P_VALUE)
        Tool.Set_Option('CROSSVAL', CROSSVAL)
        Tool.Set_Option('CROSSVAL_K', CROSSVAL_K)
        Tool.Set_Option('RESIDUAL_COR', RESIDUAL_COR)
        return Tool.Execute(Verbose)
    return False

def Polynomial_Regression(POINTS=None, TARGET_TEMPLATE=None, RESIDUALS=None, TARGET_OUT_GRID=None, ATTRIBUTE=None, POLYNOM=None, XORDER=None, YORDER=None, TORDER=None, TARGET_DEFINITION=None, TARGET_USER_SIZE=None, TARGET_USER_XMIN=None, TARGET_USER_XMAX=None, TARGET_USER_YMIN=None, TARGET_USER_YMAX=None, TARGET_USER_COLS=None, TARGET_USER_ROWS=None, TARGET_USER_FLAT=None, TARGET_USER_FITS=None, TARGET_SYSTEM=None, Verbose=2):
    '''
    Polynomial Regression
    ----------
    [statistics_regression.2]\n
    Polynomial Regression\n
    Arguments
    ----------
    - POINTS [`input shapes`] : Points
    - TARGET_TEMPLATE [`optional input grid`] : Target System. use this grid's system for output grids
    - RESIDUALS [`output shapes`] : Residuals
    - TARGET_OUT_GRID [`output grid`] : Target Grid
    - ATTRIBUTE [`table field`] : Attribute
    - POLYNOM [`choice`] : Polynom. Available Choices: [0] simple planar surface [1] bi-linear saddle [2] quadratic surface [3] cubic surface [4] user defined Default: 0
    - XORDER [`integer number`] : Maximum X Order. Minimum: 1 Default: 4
    - YORDER [`integer number`] : Maximum Y Order. Minimum: 1 Default: 4
    - TORDER [`integer number`] : Maximum Total Order. Minimum: 0 Default: 4
    - TARGET_DEFINITION [`choice`] : Target Grid System. Available Choices: [0] user defined [1] grid or grid system Default: 0
    - TARGET_USER_SIZE [`floating point number`] : Cellsize. Minimum: 0.000000 Default: 1.000000
    - TARGET_USER_XMIN [`floating point number`] : West. Default: 0.000000
    - TARGET_USER_XMAX [`floating point number`] : East. Default: 100.000000
    - TARGET_USER_YMIN [`floating point number`] : South. Default: 0.000000
    - TARGET_USER_YMAX [`floating point number`] : North. Default: 100.000000
    - TARGET_USER_COLS [`integer number`] : Columns. Minimum: 1 Default: 101 Number of cells in East-West direction.
    - TARGET_USER_ROWS [`integer number`] : Rows. Minimum: 1 Default: 101 Number of cells in North-South direction.
    - TARGET_USER_FLAT [`boolean`] : Rounding. Default: 1 Round bounding coordinates to multiples of cell size. Ignored if cell size has decimal places.
    - TARGET_USER_FITS [`choice`] : Fit. Available Choices: [0] nodes [1] cells Default: 0
    - TARGET_SYSTEM [`grid system`] : Grid System

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('statistics_regression', '2', 'Polynomial Regression')
    if Tool.is_Okay():
        Tool.Set_Input ('POINTS', POINTS)
        Tool.Set_Input ('TARGET_TEMPLATE', TARGET_TEMPLATE)
        Tool.Set_Output('RESIDUALS', RESIDUALS)
        Tool.Set_Output('TARGET_OUT_GRID', TARGET_OUT_GRID)
        Tool.Set_Option('ATTRIBUTE', ATTRIBUTE)
        Tool.Set_Option('POLYNOM', POLYNOM)
        Tool.Set_Option('XORDER', XORDER)
        Tool.Set_Option('YORDER', YORDER)
        Tool.Set_Option('TORDER', TORDER)
        Tool.Set_Option('TARGET_DEFINITION', TARGET_DEFINITION)
        Tool.Set_Option('TARGET_USER_SIZE', TARGET_USER_SIZE)
        Tool.Set_Option('TARGET_USER_XMIN', TARGET_USER_XMIN)
        Tool.Set_Option('TARGET_USER_XMAX', TARGET_USER_XMAX)
        Tool.Set_Option('TARGET_USER_YMIN', TARGET_USER_YMIN)
        Tool.Set_Option('TARGET_USER_YMAX', TARGET_USER_YMAX)
        Tool.Set_Option('TARGET_USER_COLS', TARGET_USER_COLS)
        Tool.Set_Option('TARGET_USER_ROWS', TARGET_USER_ROWS)
        Tool.Set_Option('TARGET_USER_FLAT', TARGET_USER_FLAT)
        Tool.Set_Option('TARGET_USER_FITS', TARGET_USER_FITS)
        Tool.Set_Option('TARGET_SYSTEM', TARGET_SYSTEM)
        return Tool.Execute(Verbose)
    return False

def run_tool_statistics_regression_2(POINTS=None, TARGET_TEMPLATE=None, RESIDUALS=None, TARGET_OUT_GRID=None, ATTRIBUTE=None, POLYNOM=None, XORDER=None, YORDER=None, TORDER=None, TARGET_DEFINITION=None, TARGET_USER_SIZE=None, TARGET_USER_XMIN=None, TARGET_USER_XMAX=None, TARGET_USER_YMIN=None, TARGET_USER_YMAX=None, TARGET_USER_COLS=None, TARGET_USER_ROWS=None, TARGET_USER_FLAT=None, TARGET_USER_FITS=None, TARGET_SYSTEM=None, Verbose=2):
    '''
    Polynomial Regression
    ----------
    [statistics_regression.2]\n
    Polynomial Regression\n
    Arguments
    ----------
    - POINTS [`input shapes`] : Points
    - TARGET_TEMPLATE [`optional input grid`] : Target System. use this grid's system for output grids
    - RESIDUALS [`output shapes`] : Residuals
    - TARGET_OUT_GRID [`output grid`] : Target Grid
    - ATTRIBUTE [`table field`] : Attribute
    - POLYNOM [`choice`] : Polynom. Available Choices: [0] simple planar surface [1] bi-linear saddle [2] quadratic surface [3] cubic surface [4] user defined Default: 0
    - XORDER [`integer number`] : Maximum X Order. Minimum: 1 Default: 4
    - YORDER [`integer number`] : Maximum Y Order. Minimum: 1 Default: 4
    - TORDER [`integer number`] : Maximum Total Order. Minimum: 0 Default: 4
    - TARGET_DEFINITION [`choice`] : Target Grid System. Available Choices: [0] user defined [1] grid or grid system Default: 0
    - TARGET_USER_SIZE [`floating point number`] : Cellsize. Minimum: 0.000000 Default: 1.000000
    - TARGET_USER_XMIN [`floating point number`] : West. Default: 0.000000
    - TARGET_USER_XMAX [`floating point number`] : East. Default: 100.000000
    - TARGET_USER_YMIN [`floating point number`] : South. Default: 0.000000
    - TARGET_USER_YMAX [`floating point number`] : North. Default: 100.000000
    - TARGET_USER_COLS [`integer number`] : Columns. Minimum: 1 Default: 101 Number of cells in East-West direction.
    - TARGET_USER_ROWS [`integer number`] : Rows. Minimum: 1 Default: 101 Number of cells in North-South direction.
    - TARGET_USER_FLAT [`boolean`] : Rounding. Default: 1 Round bounding coordinates to multiples of cell size. Ignored if cell size has decimal places.
    - TARGET_USER_FITS [`choice`] : Fit. Available Choices: [0] nodes [1] cells Default: 0
    - TARGET_SYSTEM [`grid system`] : Grid System

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('statistics_regression', '2', 'Polynomial Regression')
    if Tool.is_Okay():
        Tool.Set_Input ('POINTS', POINTS)
        Tool.Set_Input ('TARGET_TEMPLATE', TARGET_TEMPLATE)
        Tool.Set_Output('RESIDUALS', RESIDUALS)
        Tool.Set_Output('TARGET_OUT_GRID', TARGET_OUT_GRID)
        Tool.Set_Option('ATTRIBUTE', ATTRIBUTE)
        Tool.Set_Option('POLYNOM', POLYNOM)
        Tool.Set_Option('XORDER', XORDER)
        Tool.Set_Option('YORDER', YORDER)
        Tool.Set_Option('TORDER', TORDER)
        Tool.Set_Option('TARGET_DEFINITION', TARGET_DEFINITION)
        Tool.Set_Option('TARGET_USER_SIZE', TARGET_USER_SIZE)
        Tool.Set_Option('TARGET_USER_XMIN', TARGET_USER_XMIN)
        Tool.Set_Option('TARGET_USER_XMAX', TARGET_USER_XMAX)
        Tool.Set_Option('TARGET_USER_YMIN', TARGET_USER_YMIN)
        Tool.Set_Option('TARGET_USER_YMAX', TARGET_USER_YMAX)
        Tool.Set_Option('TARGET_USER_COLS', TARGET_USER_COLS)
        Tool.Set_Option('TARGET_USER_ROWS', TARGET_USER_ROWS)
        Tool.Set_Option('TARGET_USER_FLAT', TARGET_USER_FLAT)
        Tool.Set_Option('TARGET_USER_FITS', TARGET_USER_FITS)
        Tool.Set_Option('TARGET_SYSTEM', TARGET_SYSTEM)
        return Tool.Execute(Verbose)
    return False

def GWR_for_Single_Predictor_Gridded_Model_Output(POINTS=None, TARGET_TEMPLATE=None, TARGET_OUT_GRID=None, INTERCEPT=None, SLOPE=None, QUALITY=None, DEPENDENT=None, PREDICTOR=None, LOGISTIC=None, TARGET_DEFINITION=None, TARGET_USER_SIZE=None, TARGET_USER_XMIN=None, TARGET_USER_XMAX=None, TARGET_USER_YMIN=None, TARGET_USER_YMAX=None, TARGET_USER_COLS=None, TARGET_USER_ROWS=None, TARGET_USER_FLAT=None, TARGET_USER_FITS=None, TARGET_SYSTEM=None, DW_WEIGHTING=None, DW_IDW_POWER=None, DW_BANDWIDTH=None, SEARCH_RANGE=None, SEARCH_RADIUS=None, SEARCH_POINTS_ALL=None, SEARCH_POINTS_MIN=None, SEARCH_POINTS_MAX=None, Verbose=2):
    '''
    GWR for Single Predictor (Gridded Model Output)
    ----------
    [statistics_regression.3]\n
    This Geographically Weighted Regression tool for a single predictor creates gridded model output.\n
    Arguments
    ----------
    - POINTS [`input shapes`] : Points
    - TARGET_TEMPLATE [`optional input grid`] : Target System. use this grid's system for output grids
    - TARGET_OUT_GRID [`output grid`] : Target Grid
    - INTERCEPT [`output grid`] : Intercept
    - SLOPE [`output grid`] : Slope
    - QUALITY [`output grid`] : Quality
    - DEPENDENT [`table field`] : Dependent Variable
    - PREDICTOR [`table field`] : Predictor
    - LOGISTIC [`boolean`] : Logistic Regression. Default: 0
    - TARGET_DEFINITION [`choice`] : Target Grid System. Available Choices: [0] user defined [1] grid or grid system Default: 0
    - TARGET_USER_SIZE [`floating point number`] : Cellsize. Minimum: 0.000000 Default: 1.000000
    - TARGET_USER_XMIN [`floating point number`] : West. Default: 0.000000
    - TARGET_USER_XMAX [`floating point number`] : East. Default: 100.000000
    - TARGET_USER_YMIN [`floating point number`] : South. Default: 0.000000
    - TARGET_USER_YMAX [`floating point number`] : North. Default: 100.000000
    - TARGET_USER_COLS [`integer number`] : Columns. Minimum: 1 Default: 101 Number of cells in East-West direction.
    - TARGET_USER_ROWS [`integer number`] : Rows. Minimum: 1 Default: 101 Number of cells in North-South direction.
    - TARGET_USER_FLAT [`boolean`] : Rounding. Default: 1 Round bounding coordinates to multiples of cell size. Ignored if cell size has decimal places.
    - TARGET_USER_FITS [`choice`] : Fit. Available Choices: [0] nodes [1] cells Default: 0
    - TARGET_SYSTEM [`grid system`] : Grid System
    - DW_WEIGHTING [`choice`] : Weighting Function. Available Choices: [0] no distance weighting [1] inverse distance to a power [2] exponential [3] gaussian Default: 3
    - DW_IDW_POWER [`floating point number`] : Power. Minimum: 0.000000 Default: 2.000000
    - DW_BANDWIDTH [`floating point number`] : Bandwidth. Minimum: 0.000000 Default: 1.000000 Bandwidth for exponential and Gaussian weighting
    - SEARCH_RANGE [`choice`] : Search Range. Available Choices: [0] local [1] global Default: 1
    - SEARCH_RADIUS [`floating point number`] : Maximum Search Distance. Minimum: 0.000000 Default: 1000.000000 local maximum search distance given in map units
    - SEARCH_POINTS_ALL [`choice`] : Number of Points. Available Choices: [0] maximum number of nearest points [1] all points within search distance Default: 1
    - SEARCH_POINTS_MIN [`integer number`] : Minimum. Minimum: 1 Default: 16 minimum number of points to use
    - SEARCH_POINTS_MAX [`integer number`] : Maximum. Minimum: 1 Default: 20 maximum number of nearest points

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('statistics_regression', '3', 'GWR for Single Predictor (Gridded Model Output)')
    if Tool.is_Okay():
        Tool.Set_Input ('POINTS', POINTS)
        Tool.Set_Input ('TARGET_TEMPLATE', TARGET_TEMPLATE)
        Tool.Set_Output('TARGET_OUT_GRID', TARGET_OUT_GRID)
        Tool.Set_Output('INTERCEPT', INTERCEPT)
        Tool.Set_Output('SLOPE', SLOPE)
        Tool.Set_Output('QUALITY', QUALITY)
        Tool.Set_Option('DEPENDENT', DEPENDENT)
        Tool.Set_Option('PREDICTOR', PREDICTOR)
        Tool.Set_Option('LOGISTIC', LOGISTIC)
        Tool.Set_Option('TARGET_DEFINITION', TARGET_DEFINITION)
        Tool.Set_Option('TARGET_USER_SIZE', TARGET_USER_SIZE)
        Tool.Set_Option('TARGET_USER_XMIN', TARGET_USER_XMIN)
        Tool.Set_Option('TARGET_USER_XMAX', TARGET_USER_XMAX)
        Tool.Set_Option('TARGET_USER_YMIN', TARGET_USER_YMIN)
        Tool.Set_Option('TARGET_USER_YMAX', TARGET_USER_YMAX)
        Tool.Set_Option('TARGET_USER_COLS', TARGET_USER_COLS)
        Tool.Set_Option('TARGET_USER_ROWS', TARGET_USER_ROWS)
        Tool.Set_Option('TARGET_USER_FLAT', TARGET_USER_FLAT)
        Tool.Set_Option('TARGET_USER_FITS', TARGET_USER_FITS)
        Tool.Set_Option('TARGET_SYSTEM', TARGET_SYSTEM)
        Tool.Set_Option('DW_WEIGHTING', DW_WEIGHTING)
        Tool.Set_Option('DW_IDW_POWER', DW_IDW_POWER)
        Tool.Set_Option('DW_BANDWIDTH', DW_BANDWIDTH)
        Tool.Set_Option('SEARCH_RANGE', SEARCH_RANGE)
        Tool.Set_Option('SEARCH_RADIUS', SEARCH_RADIUS)
        Tool.Set_Option('SEARCH_POINTS_ALL', SEARCH_POINTS_ALL)
        Tool.Set_Option('SEARCH_POINTS_MIN', SEARCH_POINTS_MIN)
        Tool.Set_Option('SEARCH_POINTS_MAX', SEARCH_POINTS_MAX)
        return Tool.Execute(Verbose)
    return False

def run_tool_statistics_regression_3(POINTS=None, TARGET_TEMPLATE=None, TARGET_OUT_GRID=None, INTERCEPT=None, SLOPE=None, QUALITY=None, DEPENDENT=None, PREDICTOR=None, LOGISTIC=None, TARGET_DEFINITION=None, TARGET_USER_SIZE=None, TARGET_USER_XMIN=None, TARGET_USER_XMAX=None, TARGET_USER_YMIN=None, TARGET_USER_YMAX=None, TARGET_USER_COLS=None, TARGET_USER_ROWS=None, TARGET_USER_FLAT=None, TARGET_USER_FITS=None, TARGET_SYSTEM=None, DW_WEIGHTING=None, DW_IDW_POWER=None, DW_BANDWIDTH=None, SEARCH_RANGE=None, SEARCH_RADIUS=None, SEARCH_POINTS_ALL=None, SEARCH_POINTS_MIN=None, SEARCH_POINTS_MAX=None, Verbose=2):
    '''
    GWR for Single Predictor (Gridded Model Output)
    ----------
    [statistics_regression.3]\n
    This Geographically Weighted Regression tool for a single predictor creates gridded model output.\n
    Arguments
    ----------
    - POINTS [`input shapes`] : Points
    - TARGET_TEMPLATE [`optional input grid`] : Target System. use this grid's system for output grids
    - TARGET_OUT_GRID [`output grid`] : Target Grid
    - INTERCEPT [`output grid`] : Intercept
    - SLOPE [`output grid`] : Slope
    - QUALITY [`output grid`] : Quality
    - DEPENDENT [`table field`] : Dependent Variable
    - PREDICTOR [`table field`] : Predictor
    - LOGISTIC [`boolean`] : Logistic Regression. Default: 0
    - TARGET_DEFINITION [`choice`] : Target Grid System. Available Choices: [0] user defined [1] grid or grid system Default: 0
    - TARGET_USER_SIZE [`floating point number`] : Cellsize. Minimum: 0.000000 Default: 1.000000
    - TARGET_USER_XMIN [`floating point number`] : West. Default: 0.000000
    - TARGET_USER_XMAX [`floating point number`] : East. Default: 100.000000
    - TARGET_USER_YMIN [`floating point number`] : South. Default: 0.000000
    - TARGET_USER_YMAX [`floating point number`] : North. Default: 100.000000
    - TARGET_USER_COLS [`integer number`] : Columns. Minimum: 1 Default: 101 Number of cells in East-West direction.
    - TARGET_USER_ROWS [`integer number`] : Rows. Minimum: 1 Default: 101 Number of cells in North-South direction.
    - TARGET_USER_FLAT [`boolean`] : Rounding. Default: 1 Round bounding coordinates to multiples of cell size. Ignored if cell size has decimal places.
    - TARGET_USER_FITS [`choice`] : Fit. Available Choices: [0] nodes [1] cells Default: 0
    - TARGET_SYSTEM [`grid system`] : Grid System
    - DW_WEIGHTING [`choice`] : Weighting Function. Available Choices: [0] no distance weighting [1] inverse distance to a power [2] exponential [3] gaussian Default: 3
    - DW_IDW_POWER [`floating point number`] : Power. Minimum: 0.000000 Default: 2.000000
    - DW_BANDWIDTH [`floating point number`] : Bandwidth. Minimum: 0.000000 Default: 1.000000 Bandwidth for exponential and Gaussian weighting
    - SEARCH_RANGE [`choice`] : Search Range. Available Choices: [0] local [1] global Default: 1
    - SEARCH_RADIUS [`floating point number`] : Maximum Search Distance. Minimum: 0.000000 Default: 1000.000000 local maximum search distance given in map units
    - SEARCH_POINTS_ALL [`choice`] : Number of Points. Available Choices: [0] maximum number of nearest points [1] all points within search distance Default: 1
    - SEARCH_POINTS_MIN [`integer number`] : Minimum. Minimum: 1 Default: 16 minimum number of points to use
    - SEARCH_POINTS_MAX [`integer number`] : Maximum. Minimum: 1 Default: 20 maximum number of nearest points

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('statistics_regression', '3', 'GWR for Single Predictor (Gridded Model Output)')
    if Tool.is_Okay():
        Tool.Set_Input ('POINTS', POINTS)
        Tool.Set_Input ('TARGET_TEMPLATE', TARGET_TEMPLATE)
        Tool.Set_Output('TARGET_OUT_GRID', TARGET_OUT_GRID)
        Tool.Set_Output('INTERCEPT', INTERCEPT)
        Tool.Set_Output('SLOPE', SLOPE)
        Tool.Set_Output('QUALITY', QUALITY)
        Tool.Set_Option('DEPENDENT', DEPENDENT)
        Tool.Set_Option('PREDICTOR', PREDICTOR)
        Tool.Set_Option('LOGISTIC', LOGISTIC)
        Tool.Set_Option('TARGET_DEFINITION', TARGET_DEFINITION)
        Tool.Set_Option('TARGET_USER_SIZE', TARGET_USER_SIZE)
        Tool.Set_Option('TARGET_USER_XMIN', TARGET_USER_XMIN)
        Tool.Set_Option('TARGET_USER_XMAX', TARGET_USER_XMAX)
        Tool.Set_Option('TARGET_USER_YMIN', TARGET_USER_YMIN)
        Tool.Set_Option('TARGET_USER_YMAX', TARGET_USER_YMAX)
        Tool.Set_Option('TARGET_USER_COLS', TARGET_USER_COLS)
        Tool.Set_Option('TARGET_USER_ROWS', TARGET_USER_ROWS)
        Tool.Set_Option('TARGET_USER_FLAT', TARGET_USER_FLAT)
        Tool.Set_Option('TARGET_USER_FITS', TARGET_USER_FITS)
        Tool.Set_Option('TARGET_SYSTEM', TARGET_SYSTEM)
        Tool.Set_Option('DW_WEIGHTING', DW_WEIGHTING)
        Tool.Set_Option('DW_IDW_POWER', DW_IDW_POWER)
        Tool.Set_Option('DW_BANDWIDTH', DW_BANDWIDTH)
        Tool.Set_Option('SEARCH_RANGE', SEARCH_RANGE)
        Tool.Set_Option('SEARCH_RADIUS', SEARCH_RADIUS)
        Tool.Set_Option('SEARCH_POINTS_ALL', SEARCH_POINTS_ALL)
        Tool.Set_Option('SEARCH_POINTS_MIN', SEARCH_POINTS_MIN)
        Tool.Set_Option('SEARCH_POINTS_MAX', SEARCH_POINTS_MAX)
        return Tool.Execute(Verbose)
    return False

def GWR_for_Single_Predictor_Grid(POINTS=None, PREDICTOR=None, RESIDUALS=None, REGRESSION=None, QUALITY=None, INTERCEPT=None, SLOPE=None, DEPENDENT=None, LOGISTIC=None, DW_WEIGHTING=None, DW_IDW_POWER=None, DW_BANDWIDTH=None, SEARCH_RANGE=None, SEARCH_RADIUS=None, SEARCH_POINTS_ALL=None, SEARCH_POINTS_MIN=None, SEARCH_POINTS_MAX=None, Verbose=2):
    '''
    GWR for Single Predictor Grid
    ----------
    [statistics_regression.4]\n
    Geographically Weighted Regression for a single predictor supplied as grid, to which the regression model is applied. Further details can be stored optionally.\n
    Arguments
    ----------
    - POINTS [`input shapes`] : Points
    - PREDICTOR [`input grid`] : Predictor
    - RESIDUALS [`output shapes`] : Residuals
    - REGRESSION [`output grid`] : Regression
    - QUALITY [`output grid`] : Coefficient of Determination
    - INTERCEPT [`output grid`] : Intercept
    - SLOPE [`output grid`] : Slope
    - DEPENDENT [`table field`] : Dependent Variable
    - LOGISTIC [`boolean`] : Logistic Regression. Default: 0
    - DW_WEIGHTING [`choice`] : Weighting Function. Available Choices: [0] no distance weighting [1] inverse distance to a power [2] exponential [3] gaussian Default: 3
    - DW_IDW_POWER [`floating point number`] : Power. Minimum: 0.000000 Default: 2.000000
    - DW_BANDWIDTH [`floating point number`] : Bandwidth. Minimum: 0.000000 Default: 1.000000 Bandwidth for exponential and Gaussian weighting
    - SEARCH_RANGE [`choice`] : Search Range. Available Choices: [0] local [1] global Default: 1
    - SEARCH_RADIUS [`floating point number`] : Maximum Search Distance. Minimum: 0.000000 Default: 1000.000000 local maximum search distance given in map units
    - SEARCH_POINTS_ALL [`choice`] : Number of Points. Available Choices: [0] maximum number of nearest points [1] all points within search distance Default: 1
    - SEARCH_POINTS_MIN [`integer number`] : Minimum. Minimum: 1 Default: 16 minimum number of points to use
    - SEARCH_POINTS_MAX [`integer number`] : Maximum. Minimum: 1 Default: 20 maximum number of nearest points

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('statistics_regression', '4', 'GWR for Single Predictor Grid')
    if Tool.is_Okay():
        Tool.Set_Input ('POINTS', POINTS)
        Tool.Set_Input ('PREDICTOR', PREDICTOR)
        Tool.Set_Output('RESIDUALS', RESIDUALS)
        Tool.Set_Output('REGRESSION', REGRESSION)
        Tool.Set_Output('QUALITY', QUALITY)
        Tool.Set_Output('INTERCEPT', INTERCEPT)
        Tool.Set_Output('SLOPE', SLOPE)
        Tool.Set_Option('DEPENDENT', DEPENDENT)
        Tool.Set_Option('LOGISTIC', LOGISTIC)
        Tool.Set_Option('DW_WEIGHTING', DW_WEIGHTING)
        Tool.Set_Option('DW_IDW_POWER', DW_IDW_POWER)
        Tool.Set_Option('DW_BANDWIDTH', DW_BANDWIDTH)
        Tool.Set_Option('SEARCH_RANGE', SEARCH_RANGE)
        Tool.Set_Option('SEARCH_RADIUS', SEARCH_RADIUS)
        Tool.Set_Option('SEARCH_POINTS_ALL', SEARCH_POINTS_ALL)
        Tool.Set_Option('SEARCH_POINTS_MIN', SEARCH_POINTS_MIN)
        Tool.Set_Option('SEARCH_POINTS_MAX', SEARCH_POINTS_MAX)
        return Tool.Execute(Verbose)
    return False

def run_tool_statistics_regression_4(POINTS=None, PREDICTOR=None, RESIDUALS=None, REGRESSION=None, QUALITY=None, INTERCEPT=None, SLOPE=None, DEPENDENT=None, LOGISTIC=None, DW_WEIGHTING=None, DW_IDW_POWER=None, DW_BANDWIDTH=None, SEARCH_RANGE=None, SEARCH_RADIUS=None, SEARCH_POINTS_ALL=None, SEARCH_POINTS_MIN=None, SEARCH_POINTS_MAX=None, Verbose=2):
    '''
    GWR for Single Predictor Grid
    ----------
    [statistics_regression.4]\n
    Geographically Weighted Regression for a single predictor supplied as grid, to which the regression model is applied. Further details can be stored optionally.\n
    Arguments
    ----------
    - POINTS [`input shapes`] : Points
    - PREDICTOR [`input grid`] : Predictor
    - RESIDUALS [`output shapes`] : Residuals
    - REGRESSION [`output grid`] : Regression
    - QUALITY [`output grid`] : Coefficient of Determination
    - INTERCEPT [`output grid`] : Intercept
    - SLOPE [`output grid`] : Slope
    - DEPENDENT [`table field`] : Dependent Variable
    - LOGISTIC [`boolean`] : Logistic Regression. Default: 0
    - DW_WEIGHTING [`choice`] : Weighting Function. Available Choices: [0] no distance weighting [1] inverse distance to a power [2] exponential [3] gaussian Default: 3
    - DW_IDW_POWER [`floating point number`] : Power. Minimum: 0.000000 Default: 2.000000
    - DW_BANDWIDTH [`floating point number`] : Bandwidth. Minimum: 0.000000 Default: 1.000000 Bandwidth for exponential and Gaussian weighting
    - SEARCH_RANGE [`choice`] : Search Range. Available Choices: [0] local [1] global Default: 1
    - SEARCH_RADIUS [`floating point number`] : Maximum Search Distance. Minimum: 0.000000 Default: 1000.000000 local maximum search distance given in map units
    - SEARCH_POINTS_ALL [`choice`] : Number of Points. Available Choices: [0] maximum number of nearest points [1] all points within search distance Default: 1
    - SEARCH_POINTS_MIN [`integer number`] : Minimum. Minimum: 1 Default: 16 minimum number of points to use
    - SEARCH_POINTS_MAX [`integer number`] : Maximum. Minimum: 1 Default: 20 maximum number of nearest points

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('statistics_regression', '4', 'GWR for Single Predictor Grid')
    if Tool.is_Okay():
        Tool.Set_Input ('POINTS', POINTS)
        Tool.Set_Input ('PREDICTOR', PREDICTOR)
        Tool.Set_Output('RESIDUALS', RESIDUALS)
        Tool.Set_Output('REGRESSION', REGRESSION)
        Tool.Set_Output('QUALITY', QUALITY)
        Tool.Set_Output('INTERCEPT', INTERCEPT)
        Tool.Set_Output('SLOPE', SLOPE)
        Tool.Set_Option('DEPENDENT', DEPENDENT)
        Tool.Set_Option('LOGISTIC', LOGISTIC)
        Tool.Set_Option('DW_WEIGHTING', DW_WEIGHTING)
        Tool.Set_Option('DW_IDW_POWER', DW_IDW_POWER)
        Tool.Set_Option('DW_BANDWIDTH', DW_BANDWIDTH)
        Tool.Set_Option('SEARCH_RANGE', SEARCH_RANGE)
        Tool.Set_Option('SEARCH_RADIUS', SEARCH_RADIUS)
        Tool.Set_Option('SEARCH_POINTS_ALL', SEARCH_POINTS_ALL)
        Tool.Set_Option('SEARCH_POINTS_MIN', SEARCH_POINTS_MIN)
        Tool.Set_Option('SEARCH_POINTS_MAX', SEARCH_POINTS_MAX)
        return Tool.Execute(Verbose)
    return False

def GWR_for_Multiple_Predictors_Gridded_Model_Output(POINTS=None, TARGET_TEMPLATE=None, REGRESSION=None, QUALITY=None, INTERCEPT=None, SLOPES=None, DEPENDENT=None, PREDICTORS=None, LOGISTIC=None, TARGET_DEFINITION=None, TARGET_USER_SIZE=None, TARGET_USER_XMIN=None, TARGET_USER_XMAX=None, TARGET_USER_YMIN=None, TARGET_USER_YMAX=None, TARGET_USER_COLS=None, TARGET_USER_ROWS=None, TARGET_USER_FLAT=None, TARGET_USER_FITS=None, TARGET_SYSTEM=None, DW_WEIGHTING=None, DW_IDW_POWER=None, DW_BANDWIDTH=None, SEARCH_RANGE=None, SEARCH_RADIUS=None, SEARCH_POINTS_ALL=None, SEARCH_POINTS_MIN=None, SEARCH_POINTS_MAX=None, Verbose=2):
    '''
    GWR for Multiple Predictors (Gridded Model Output)
    ----------
    [statistics_regression.5]\n
    Geographically Weighted Regression for multiple predictors. Predictors have to be supplied as attributes of ingoing points data. Regression model parameters are generated as continuous fields, i.e. as grids.\n
    Arguments
    ----------
    - POINTS [`input shapes`] : Points
    - TARGET_TEMPLATE [`optional input grid`] : Target System. use this grid's system for output grids
    - REGRESSION [`output shapes`] : Regression
    - QUALITY [`output grid`] : Quality
    - INTERCEPT [`output grid`] : Intercept
    - SLOPES [`output grid list`] : Slopes
    - DEPENDENT [`table field`] : Dependent Variable
    - PREDICTORS [`table fields`] : Predictors
    - LOGISTIC [`boolean`] : Logistic Regression. Default: 0
    - TARGET_DEFINITION [`choice`] : Target Grid System. Available Choices: [0] user defined [1] grid or grid system Default: 0
    - TARGET_USER_SIZE [`floating point number`] : Cellsize. Minimum: 0.000000 Default: 1.000000
    - TARGET_USER_XMIN [`floating point number`] : West. Default: 0.000000
    - TARGET_USER_XMAX [`floating point number`] : East. Default: 100.000000
    - TARGET_USER_YMIN [`floating point number`] : South. Default: 0.000000
    - TARGET_USER_YMAX [`floating point number`] : North. Default: 100.000000
    - TARGET_USER_COLS [`integer number`] : Columns. Minimum: 1 Default: 101 Number of cells in East-West direction.
    - TARGET_USER_ROWS [`integer number`] : Rows. Minimum: 1 Default: 101 Number of cells in North-South direction.
    - TARGET_USER_FLAT [`boolean`] : Rounding. Default: 1 Round bounding coordinates to multiples of cell size. Ignored if cell size has decimal places.
    - TARGET_USER_FITS [`choice`] : Fit. Available Choices: [0] nodes [1] cells Default: 0
    - TARGET_SYSTEM [`grid system`] : Grid System
    - DW_WEIGHTING [`choice`] : Weighting Function. Available Choices: [0] no distance weighting [1] inverse distance to a power [2] exponential [3] gaussian Default: 3
    - DW_IDW_POWER [`floating point number`] : Power. Minimum: 0.000000 Default: 2.000000
    - DW_BANDWIDTH [`floating point number`] : Bandwidth. Minimum: 0.000000 Default: 1.000000 Bandwidth for exponential and Gaussian weighting
    - SEARCH_RANGE [`choice`] : Search Range. Available Choices: [0] local [1] global Default: 1
    - SEARCH_RADIUS [`floating point number`] : Maximum Search Distance. Minimum: 0.000000 Default: 1000.000000 local maximum search distance given in map units
    - SEARCH_POINTS_ALL [`choice`] : Number of Points. Available Choices: [0] maximum number of nearest points [1] all points within search distance Default: 1
    - SEARCH_POINTS_MIN [`integer number`] : Minimum. Minimum: 1 Default: 16 minimum number of points to use
    - SEARCH_POINTS_MAX [`integer number`] : Maximum. Minimum: 1 Default: 20 maximum number of nearest points

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('statistics_regression', '5', 'GWR for Multiple Predictors (Gridded Model Output)')
    if Tool.is_Okay():
        Tool.Set_Input ('POINTS', POINTS)
        Tool.Set_Input ('TARGET_TEMPLATE', TARGET_TEMPLATE)
        Tool.Set_Output('REGRESSION', REGRESSION)
        Tool.Set_Output('QUALITY', QUALITY)
        Tool.Set_Output('INTERCEPT', INTERCEPT)
        Tool.Set_Output('SLOPES', SLOPES)
        Tool.Set_Option('DEPENDENT', DEPENDENT)
        Tool.Set_Option('PREDICTORS', PREDICTORS)
        Tool.Set_Option('LOGISTIC', LOGISTIC)
        Tool.Set_Option('TARGET_DEFINITION', TARGET_DEFINITION)
        Tool.Set_Option('TARGET_USER_SIZE', TARGET_USER_SIZE)
        Tool.Set_Option('TARGET_USER_XMIN', TARGET_USER_XMIN)
        Tool.Set_Option('TARGET_USER_XMAX', TARGET_USER_XMAX)
        Tool.Set_Option('TARGET_USER_YMIN', TARGET_USER_YMIN)
        Tool.Set_Option('TARGET_USER_YMAX', TARGET_USER_YMAX)
        Tool.Set_Option('TARGET_USER_COLS', TARGET_USER_COLS)
        Tool.Set_Option('TARGET_USER_ROWS', TARGET_USER_ROWS)
        Tool.Set_Option('TARGET_USER_FLAT', TARGET_USER_FLAT)
        Tool.Set_Option('TARGET_USER_FITS', TARGET_USER_FITS)
        Tool.Set_Option('TARGET_SYSTEM', TARGET_SYSTEM)
        Tool.Set_Option('DW_WEIGHTING', DW_WEIGHTING)
        Tool.Set_Option('DW_IDW_POWER', DW_IDW_POWER)
        Tool.Set_Option('DW_BANDWIDTH', DW_BANDWIDTH)
        Tool.Set_Option('SEARCH_RANGE', SEARCH_RANGE)
        Tool.Set_Option('SEARCH_RADIUS', SEARCH_RADIUS)
        Tool.Set_Option('SEARCH_POINTS_ALL', SEARCH_POINTS_ALL)
        Tool.Set_Option('SEARCH_POINTS_MIN', SEARCH_POINTS_MIN)
        Tool.Set_Option('SEARCH_POINTS_MAX', SEARCH_POINTS_MAX)
        return Tool.Execute(Verbose)
    return False

def run_tool_statistics_regression_5(POINTS=None, TARGET_TEMPLATE=None, REGRESSION=None, QUALITY=None, INTERCEPT=None, SLOPES=None, DEPENDENT=None, PREDICTORS=None, LOGISTIC=None, TARGET_DEFINITION=None, TARGET_USER_SIZE=None, TARGET_USER_XMIN=None, TARGET_USER_XMAX=None, TARGET_USER_YMIN=None, TARGET_USER_YMAX=None, TARGET_USER_COLS=None, TARGET_USER_ROWS=None, TARGET_USER_FLAT=None, TARGET_USER_FITS=None, TARGET_SYSTEM=None, DW_WEIGHTING=None, DW_IDW_POWER=None, DW_BANDWIDTH=None, SEARCH_RANGE=None, SEARCH_RADIUS=None, SEARCH_POINTS_ALL=None, SEARCH_POINTS_MIN=None, SEARCH_POINTS_MAX=None, Verbose=2):
    '''
    GWR for Multiple Predictors (Gridded Model Output)
    ----------
    [statistics_regression.5]\n
    Geographically Weighted Regression for multiple predictors. Predictors have to be supplied as attributes of ingoing points data. Regression model parameters are generated as continuous fields, i.e. as grids.\n
    Arguments
    ----------
    - POINTS [`input shapes`] : Points
    - TARGET_TEMPLATE [`optional input grid`] : Target System. use this grid's system for output grids
    - REGRESSION [`output shapes`] : Regression
    - QUALITY [`output grid`] : Quality
    - INTERCEPT [`output grid`] : Intercept
    - SLOPES [`output grid list`] : Slopes
    - DEPENDENT [`table field`] : Dependent Variable
    - PREDICTORS [`table fields`] : Predictors
    - LOGISTIC [`boolean`] : Logistic Regression. Default: 0
    - TARGET_DEFINITION [`choice`] : Target Grid System. Available Choices: [0] user defined [1] grid or grid system Default: 0
    - TARGET_USER_SIZE [`floating point number`] : Cellsize. Minimum: 0.000000 Default: 1.000000
    - TARGET_USER_XMIN [`floating point number`] : West. Default: 0.000000
    - TARGET_USER_XMAX [`floating point number`] : East. Default: 100.000000
    - TARGET_USER_YMIN [`floating point number`] : South. Default: 0.000000
    - TARGET_USER_YMAX [`floating point number`] : North. Default: 100.000000
    - TARGET_USER_COLS [`integer number`] : Columns. Minimum: 1 Default: 101 Number of cells in East-West direction.
    - TARGET_USER_ROWS [`integer number`] : Rows. Minimum: 1 Default: 101 Number of cells in North-South direction.
    - TARGET_USER_FLAT [`boolean`] : Rounding. Default: 1 Round bounding coordinates to multiples of cell size. Ignored if cell size has decimal places.
    - TARGET_USER_FITS [`choice`] : Fit. Available Choices: [0] nodes [1] cells Default: 0
    - TARGET_SYSTEM [`grid system`] : Grid System
    - DW_WEIGHTING [`choice`] : Weighting Function. Available Choices: [0] no distance weighting [1] inverse distance to a power [2] exponential [3] gaussian Default: 3
    - DW_IDW_POWER [`floating point number`] : Power. Minimum: 0.000000 Default: 2.000000
    - DW_BANDWIDTH [`floating point number`] : Bandwidth. Minimum: 0.000000 Default: 1.000000 Bandwidth for exponential and Gaussian weighting
    - SEARCH_RANGE [`choice`] : Search Range. Available Choices: [0] local [1] global Default: 1
    - SEARCH_RADIUS [`floating point number`] : Maximum Search Distance. Minimum: 0.000000 Default: 1000.000000 local maximum search distance given in map units
    - SEARCH_POINTS_ALL [`choice`] : Number of Points. Available Choices: [0] maximum number of nearest points [1] all points within search distance Default: 1
    - SEARCH_POINTS_MIN [`integer number`] : Minimum. Minimum: 1 Default: 16 minimum number of points to use
    - SEARCH_POINTS_MAX [`integer number`] : Maximum. Minimum: 1 Default: 20 maximum number of nearest points

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('statistics_regression', '5', 'GWR for Multiple Predictors (Gridded Model Output)')
    if Tool.is_Okay():
        Tool.Set_Input ('POINTS', POINTS)
        Tool.Set_Input ('TARGET_TEMPLATE', TARGET_TEMPLATE)
        Tool.Set_Output('REGRESSION', REGRESSION)
        Tool.Set_Output('QUALITY', QUALITY)
        Tool.Set_Output('INTERCEPT', INTERCEPT)
        Tool.Set_Output('SLOPES', SLOPES)
        Tool.Set_Option('DEPENDENT', DEPENDENT)
        Tool.Set_Option('PREDICTORS', PREDICTORS)
        Tool.Set_Option('LOGISTIC', LOGISTIC)
        Tool.Set_Option('TARGET_DEFINITION', TARGET_DEFINITION)
        Tool.Set_Option('TARGET_USER_SIZE', TARGET_USER_SIZE)
        Tool.Set_Option('TARGET_USER_XMIN', TARGET_USER_XMIN)
        Tool.Set_Option('TARGET_USER_XMAX', TARGET_USER_XMAX)
        Tool.Set_Option('TARGET_USER_YMIN', TARGET_USER_YMIN)
        Tool.Set_Option('TARGET_USER_YMAX', TARGET_USER_YMAX)
        Tool.Set_Option('TARGET_USER_COLS', TARGET_USER_COLS)
        Tool.Set_Option('TARGET_USER_ROWS', TARGET_USER_ROWS)
        Tool.Set_Option('TARGET_USER_FLAT', TARGET_USER_FLAT)
        Tool.Set_Option('TARGET_USER_FITS', TARGET_USER_FITS)
        Tool.Set_Option('TARGET_SYSTEM', TARGET_SYSTEM)
        Tool.Set_Option('DW_WEIGHTING', DW_WEIGHTING)
        Tool.Set_Option('DW_IDW_POWER', DW_IDW_POWER)
        Tool.Set_Option('DW_BANDWIDTH', DW_BANDWIDTH)
        Tool.Set_Option('SEARCH_RANGE', SEARCH_RANGE)
        Tool.Set_Option('SEARCH_RADIUS', SEARCH_RADIUS)
        Tool.Set_Option('SEARCH_POINTS_ALL', SEARCH_POINTS_ALL)
        Tool.Set_Option('SEARCH_POINTS_MIN', SEARCH_POINTS_MIN)
        Tool.Set_Option('SEARCH_POINTS_MAX', SEARCH_POINTS_MAX)
        return Tool.Execute(Verbose)
    return False

def GWR_for_Multiple_Predictor_Grids(POINTS=None, PREDICTORS=None, RESIDUALS=None, REGRESSION=None, QUALITY=None, MODEL=None, DEPENDENT=None, LOGISTIC=None, MODEL_OUT=None, RESOLUTION=None, RESOLUTION_VAL=None, DW_WEIGHTING=None, DW_IDW_POWER=None, DW_BANDWIDTH=None, SEARCH_RANGE=None, SEARCH_RADIUS=None, SEARCH_POINTS_ALL=None, SEARCH_POINTS_MIN=None, SEARCH_POINTS_MAX=None, Verbose=2):
    '''
    GWR for Multiple Predictor Grids
    ----------
    [statistics_regression.6]\n
    Geographically Weighted Regression for a multiple predictors supplied as grids, to which the regression model is applied. Further details can be stored optionally.\n
    Arguments
    ----------
    - POINTS [`input shapes`] : Points
    - PREDICTORS [`input grid list`] : Predictors
    - RESIDUALS [`output shapes`] : Residuals
    - REGRESSION [`output grid`] : Regression
    - QUALITY [`output grid`] : Coefficient of Determination
    - MODEL [`output grid list`] : Regression Parameters
    - DEPENDENT [`table field`] : Dependent Variable
    - LOGISTIC [`boolean`] : Logistic Regression. Default: 0
    - MODEL_OUT [`boolean`] : Output of Regression Parameters. Default: 0
    - RESOLUTION [`choice`] : Model Resolution. Available Choices: [0] same as predictors [1] user defined Default: 1
    - RESOLUTION_VAL [`floating point number`] : Resolution. Minimum: 0.000000 Default: 1.000000 map units
    - DW_WEIGHTING [`choice`] : Weighting Function. Available Choices: [0] no distance weighting [1] inverse distance to a power [2] exponential [3] gaussian Default: 3
    - DW_IDW_POWER [`floating point number`] : Power. Minimum: 0.000000 Default: 2.000000
    - DW_BANDWIDTH [`floating point number`] : Bandwidth. Minimum: 0.000000 Default: 1.000000 Bandwidth for exponential and Gaussian weighting
    - SEARCH_RANGE [`choice`] : Search Range. Available Choices: [0] local [1] global Default: 1
    - SEARCH_RADIUS [`floating point number`] : Maximum Search Distance. Minimum: 0.000000 Default: 1000.000000 local maximum search distance given in map units
    - SEARCH_POINTS_ALL [`choice`] : Number of Points. Available Choices: [0] maximum number of nearest points [1] all points within search distance Default: 1
    - SEARCH_POINTS_MIN [`integer number`] : Minimum. Minimum: 1 Default: 16 minimum number of points to use
    - SEARCH_POINTS_MAX [`integer number`] : Maximum. Minimum: 1 Default: 20 maximum number of nearest points

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('statistics_regression', '6', 'GWR for Multiple Predictor Grids')
    if Tool.is_Okay():
        Tool.Set_Input ('POINTS', POINTS)
        Tool.Set_Input ('PREDICTORS', PREDICTORS)
        Tool.Set_Output('RESIDUALS', RESIDUALS)
        Tool.Set_Output('REGRESSION', REGRESSION)
        Tool.Set_Output('QUALITY', QUALITY)
        Tool.Set_Output('MODEL', MODEL)
        Tool.Set_Option('DEPENDENT', DEPENDENT)
        Tool.Set_Option('LOGISTIC', LOGISTIC)
        Tool.Set_Option('MODEL_OUT', MODEL_OUT)
        Tool.Set_Option('RESOLUTION', RESOLUTION)
        Tool.Set_Option('RESOLUTION_VAL', RESOLUTION_VAL)
        Tool.Set_Option('DW_WEIGHTING', DW_WEIGHTING)
        Tool.Set_Option('DW_IDW_POWER', DW_IDW_POWER)
        Tool.Set_Option('DW_BANDWIDTH', DW_BANDWIDTH)
        Tool.Set_Option('SEARCH_RANGE', SEARCH_RANGE)
        Tool.Set_Option('SEARCH_RADIUS', SEARCH_RADIUS)
        Tool.Set_Option('SEARCH_POINTS_ALL', SEARCH_POINTS_ALL)
        Tool.Set_Option('SEARCH_POINTS_MIN', SEARCH_POINTS_MIN)
        Tool.Set_Option('SEARCH_POINTS_MAX', SEARCH_POINTS_MAX)
        return Tool.Execute(Verbose)
    return False

def run_tool_statistics_regression_6(POINTS=None, PREDICTORS=None, RESIDUALS=None, REGRESSION=None, QUALITY=None, MODEL=None, DEPENDENT=None, LOGISTIC=None, MODEL_OUT=None, RESOLUTION=None, RESOLUTION_VAL=None, DW_WEIGHTING=None, DW_IDW_POWER=None, DW_BANDWIDTH=None, SEARCH_RANGE=None, SEARCH_RADIUS=None, SEARCH_POINTS_ALL=None, SEARCH_POINTS_MIN=None, SEARCH_POINTS_MAX=None, Verbose=2):
    '''
    GWR for Multiple Predictor Grids
    ----------
    [statistics_regression.6]\n
    Geographically Weighted Regression for a multiple predictors supplied as grids, to which the regression model is applied. Further details can be stored optionally.\n
    Arguments
    ----------
    - POINTS [`input shapes`] : Points
    - PREDICTORS [`input grid list`] : Predictors
    - RESIDUALS [`output shapes`] : Residuals
    - REGRESSION [`output grid`] : Regression
    - QUALITY [`output grid`] : Coefficient of Determination
    - MODEL [`output grid list`] : Regression Parameters
    - DEPENDENT [`table field`] : Dependent Variable
    - LOGISTIC [`boolean`] : Logistic Regression. Default: 0
    - MODEL_OUT [`boolean`] : Output of Regression Parameters. Default: 0
    - RESOLUTION [`choice`] : Model Resolution. Available Choices: [0] same as predictors [1] user defined Default: 1
    - RESOLUTION_VAL [`floating point number`] : Resolution. Minimum: 0.000000 Default: 1.000000 map units
    - DW_WEIGHTING [`choice`] : Weighting Function. Available Choices: [0] no distance weighting [1] inverse distance to a power [2] exponential [3] gaussian Default: 3
    - DW_IDW_POWER [`floating point number`] : Power. Minimum: 0.000000 Default: 2.000000
    - DW_BANDWIDTH [`floating point number`] : Bandwidth. Minimum: 0.000000 Default: 1.000000 Bandwidth for exponential and Gaussian weighting
    - SEARCH_RANGE [`choice`] : Search Range. Available Choices: [0] local [1] global Default: 1
    - SEARCH_RADIUS [`floating point number`] : Maximum Search Distance. Minimum: 0.000000 Default: 1000.000000 local maximum search distance given in map units
    - SEARCH_POINTS_ALL [`choice`] : Number of Points. Available Choices: [0] maximum number of nearest points [1] all points within search distance Default: 1
    - SEARCH_POINTS_MIN [`integer number`] : Minimum. Minimum: 1 Default: 16 minimum number of points to use
    - SEARCH_POINTS_MAX [`integer number`] : Maximum. Minimum: 1 Default: 20 maximum number of nearest points

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('statistics_regression', '6', 'GWR for Multiple Predictor Grids')
    if Tool.is_Okay():
        Tool.Set_Input ('POINTS', POINTS)
        Tool.Set_Input ('PREDICTORS', PREDICTORS)
        Tool.Set_Output('RESIDUALS', RESIDUALS)
        Tool.Set_Output('REGRESSION', REGRESSION)
        Tool.Set_Output('QUALITY', QUALITY)
        Tool.Set_Output('MODEL', MODEL)
        Tool.Set_Option('DEPENDENT', DEPENDENT)
        Tool.Set_Option('LOGISTIC', LOGISTIC)
        Tool.Set_Option('MODEL_OUT', MODEL_OUT)
        Tool.Set_Option('RESOLUTION', RESOLUTION)
        Tool.Set_Option('RESOLUTION_VAL', RESOLUTION_VAL)
        Tool.Set_Option('DW_WEIGHTING', DW_WEIGHTING)
        Tool.Set_Option('DW_IDW_POWER', DW_IDW_POWER)
        Tool.Set_Option('DW_BANDWIDTH', DW_BANDWIDTH)
        Tool.Set_Option('SEARCH_RANGE', SEARCH_RANGE)
        Tool.Set_Option('SEARCH_RADIUS', SEARCH_RADIUS)
        Tool.Set_Option('SEARCH_POINTS_ALL', SEARCH_POINTS_ALL)
        Tool.Set_Option('SEARCH_POINTS_MIN', SEARCH_POINTS_MIN)
        Tool.Set_Option('SEARCH_POINTS_MAX', SEARCH_POINTS_MAX)
        return Tool.Execute(Verbose)
    return False

def GWR_for_Multiple_Predictors_Shapes(POINTS=None, REGRESSION=None, DEPENDENT=None, PREDICTORS=None, LOGISTIC=None, DW_WEIGHTING=None, DW_IDW_POWER=None, DW_BANDWIDTH=None, SEARCH_RANGE=None, SEARCH_RADIUS=None, SEARCH_POINTS_ALL=None, SEARCH_POINTS_MIN=None, SEARCH_POINTS_MAX=None, Verbose=2):
    '''
    GWR for Multiple Predictors (Shapes)
    ----------
    [statistics_regression.7]\n
    Geographically Weighted Regression for multiple predictors. Regression details are stored in a copy of the input data set. If the input data set is not a point data set, the feature centroids are used as spatial reference.\n
    Arguments
    ----------
    - POINTS [`input shapes`] : Shapes
    - REGRESSION [`output shapes`] : Regression
    - DEPENDENT [`table field`] : Dependent Variable
    - PREDICTORS [`table fields`] : Predictors
    - LOGISTIC [`boolean`] : Logistic Regression. Default: 0
    - DW_WEIGHTING [`choice`] : Weighting Function. Available Choices: [0] no distance weighting [1] inverse distance to a power [2] exponential [3] gaussian Default: 3
    - DW_IDW_POWER [`floating point number`] : Power. Minimum: 0.000000 Default: 2.000000
    - DW_BANDWIDTH [`floating point number`] : Bandwidth. Minimum: 0.000000 Default: 1.000000 Bandwidth for exponential and Gaussian weighting
    - SEARCH_RANGE [`choice`] : Search Range. Available Choices: [0] local [1] global Default: 1
    - SEARCH_RADIUS [`floating point number`] : Maximum Search Distance. Minimum: 0.000000 Default: 1000.000000 local maximum search distance given in map units
    - SEARCH_POINTS_ALL [`choice`] : Number of Points. Available Choices: [0] maximum number of nearest points [1] all points within search distance Default: 1
    - SEARCH_POINTS_MIN [`integer number`] : Minimum. Minimum: 1 Default: 16 minimum number of points to use
    - SEARCH_POINTS_MAX [`integer number`] : Maximum. Minimum: 1 Default: 20 maximum number of nearest points

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('statistics_regression', '7', 'GWR for Multiple Predictors (Shapes)')
    if Tool.is_Okay():
        Tool.Set_Input ('POINTS', POINTS)
        Tool.Set_Output('REGRESSION', REGRESSION)
        Tool.Set_Option('DEPENDENT', DEPENDENT)
        Tool.Set_Option('PREDICTORS', PREDICTORS)
        Tool.Set_Option('LOGISTIC', LOGISTIC)
        Tool.Set_Option('DW_WEIGHTING', DW_WEIGHTING)
        Tool.Set_Option('DW_IDW_POWER', DW_IDW_POWER)
        Tool.Set_Option('DW_BANDWIDTH', DW_BANDWIDTH)
        Tool.Set_Option('SEARCH_RANGE', SEARCH_RANGE)
        Tool.Set_Option('SEARCH_RADIUS', SEARCH_RADIUS)
        Tool.Set_Option('SEARCH_POINTS_ALL', SEARCH_POINTS_ALL)
        Tool.Set_Option('SEARCH_POINTS_MIN', SEARCH_POINTS_MIN)
        Tool.Set_Option('SEARCH_POINTS_MAX', SEARCH_POINTS_MAX)
        return Tool.Execute(Verbose)
    return False

def run_tool_statistics_regression_7(POINTS=None, REGRESSION=None, DEPENDENT=None, PREDICTORS=None, LOGISTIC=None, DW_WEIGHTING=None, DW_IDW_POWER=None, DW_BANDWIDTH=None, SEARCH_RANGE=None, SEARCH_RADIUS=None, SEARCH_POINTS_ALL=None, SEARCH_POINTS_MIN=None, SEARCH_POINTS_MAX=None, Verbose=2):
    '''
    GWR for Multiple Predictors (Shapes)
    ----------
    [statistics_regression.7]\n
    Geographically Weighted Regression for multiple predictors. Regression details are stored in a copy of the input data set. If the input data set is not a point data set, the feature centroids are used as spatial reference.\n
    Arguments
    ----------
    - POINTS [`input shapes`] : Shapes
    - REGRESSION [`output shapes`] : Regression
    - DEPENDENT [`table field`] : Dependent Variable
    - PREDICTORS [`table fields`] : Predictors
    - LOGISTIC [`boolean`] : Logistic Regression. Default: 0
    - DW_WEIGHTING [`choice`] : Weighting Function. Available Choices: [0] no distance weighting [1] inverse distance to a power [2] exponential [3] gaussian Default: 3
    - DW_IDW_POWER [`floating point number`] : Power. Minimum: 0.000000 Default: 2.000000
    - DW_BANDWIDTH [`floating point number`] : Bandwidth. Minimum: 0.000000 Default: 1.000000 Bandwidth for exponential and Gaussian weighting
    - SEARCH_RANGE [`choice`] : Search Range. Available Choices: [0] local [1] global Default: 1
    - SEARCH_RADIUS [`floating point number`] : Maximum Search Distance. Minimum: 0.000000 Default: 1000.000000 local maximum search distance given in map units
    - SEARCH_POINTS_ALL [`choice`] : Number of Points. Available Choices: [0] maximum number of nearest points [1] all points within search distance Default: 1
    - SEARCH_POINTS_MIN [`integer number`] : Minimum. Minimum: 1 Default: 16 minimum number of points to use
    - SEARCH_POINTS_MAX [`integer number`] : Maximum. Minimum: 1 Default: 20 maximum number of nearest points

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('statistics_regression', '7', 'GWR for Multiple Predictors (Shapes)')
    if Tool.is_Okay():
        Tool.Set_Input ('POINTS', POINTS)
        Tool.Set_Output('REGRESSION', REGRESSION)
        Tool.Set_Option('DEPENDENT', DEPENDENT)
        Tool.Set_Option('PREDICTORS', PREDICTORS)
        Tool.Set_Option('LOGISTIC', LOGISTIC)
        Tool.Set_Option('DW_WEIGHTING', DW_WEIGHTING)
        Tool.Set_Option('DW_IDW_POWER', DW_IDW_POWER)
        Tool.Set_Option('DW_BANDWIDTH', DW_BANDWIDTH)
        Tool.Set_Option('SEARCH_RANGE', SEARCH_RANGE)
        Tool.Set_Option('SEARCH_RADIUS', SEARCH_RADIUS)
        Tool.Set_Option('SEARCH_POINTS_ALL', SEARCH_POINTS_ALL)
        Tool.Set_Option('SEARCH_POINTS_MIN', SEARCH_POINTS_MIN)
        Tool.Set_Option('SEARCH_POINTS_MAX', SEARCH_POINTS_MAX)
        return Tool.Execute(Verbose)
    return False

def Multiple_Regression_Analysis_Grid_and_Predictor_Grids(DEPENDENT=None, PREDICTORS=None, REGRESSION=None, RESIDUALS=None, INFO_COEFF=None, INFO_MODEL=None, INFO_STEPS=None, DEPENDENT_GRIDSYSTEM=None, RESAMPLING=None, COORD_X=None, COORD_Y=None, METHOD=None, P_VALUE=None, CROSSVAL=None, CROSSVAL_K=None, Verbose=2):
    '''
    Multiple Regression Analysis (Grid and Predictor Grids)
    ----------
    [statistics_regression.8]\n
    Linear regression analysis of one grid as dependent and multiple grids as independent (predictor) variables. Details of the regression/correlation analysis will be saved to a table. Optionally the regression model is used to create a new grid with regression based values. The multiple regression analysis uses a forward selection procedure.\n
    Arguments
    ----------
    - DEPENDENT [`input grid`] : Dependent Variable
    - PREDICTORS [`input grid list`] : Predictors
    - REGRESSION [`output grid`] : Regression
    - RESIDUALS [`output grid`] : Residuals
    - INFO_COEFF [`output table`] : Details: Coefficients
    - INFO_MODEL [`output table`] : Details: Model
    - INFO_STEPS [`output table`] : Details: Steps
    - DEPENDENT_GRIDSYSTEM [`grid system`] : Grid system
    - RESAMPLING [`choice`] : Resampling. Available Choices: [0] Nearest Neighbour [1] Bilinear Interpolation [2] Bicubic Spline Interpolation [3] B-Spline Interpolation Default: 3
    - COORD_X [`boolean`] : Include X Coordinate. Default: 0
    - COORD_Y [`boolean`] : Include Y Coordinate. Default: 0
    - METHOD [`choice`] : Method. Available Choices: [0] include all [1] forward [2] backward [3] stepwise Default: 3
    - P_VALUE [`floating point number`] : Significance Level. Minimum: 0.000000 Maximum: 100.000000 Default: 5.000000 Significance level (aka p-value) as threshold for automated predictor selection, given as percentage
    - CROSSVAL [`choice`] : Cross Validation. Available Choices: [0] none [1] leave one out [2] 2-fold [3] k-fold Default: 0
    - CROSSVAL_K [`integer number`] : Cross Validation Subsamples. Minimum: 2 Default: 10 number of subsamples for k-fold cross validation

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('statistics_regression', '8', 'Multiple Regression Analysis (Grid and Predictor Grids)')
    if Tool.is_Okay():
        Tool.Set_Input ('DEPENDENT', DEPENDENT)
        Tool.Set_Input ('PREDICTORS', PREDICTORS)
        Tool.Set_Output('REGRESSION', REGRESSION)
        Tool.Set_Output('RESIDUALS', RESIDUALS)
        Tool.Set_Output('INFO_COEFF', INFO_COEFF)
        Tool.Set_Output('INFO_MODEL', INFO_MODEL)
        Tool.Set_Output('INFO_STEPS', INFO_STEPS)
        Tool.Set_Option('DEPENDENT_GRIDSYSTEM', DEPENDENT_GRIDSYSTEM)
        Tool.Set_Option('RESAMPLING', RESAMPLING)
        Tool.Set_Option('COORD_X', COORD_X)
        Tool.Set_Option('COORD_Y', COORD_Y)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('P_VALUE', P_VALUE)
        Tool.Set_Option('CROSSVAL', CROSSVAL)
        Tool.Set_Option('CROSSVAL_K', CROSSVAL_K)
        return Tool.Execute(Verbose)
    return False

def run_tool_statistics_regression_8(DEPENDENT=None, PREDICTORS=None, REGRESSION=None, RESIDUALS=None, INFO_COEFF=None, INFO_MODEL=None, INFO_STEPS=None, DEPENDENT_GRIDSYSTEM=None, RESAMPLING=None, COORD_X=None, COORD_Y=None, METHOD=None, P_VALUE=None, CROSSVAL=None, CROSSVAL_K=None, Verbose=2):
    '''
    Multiple Regression Analysis (Grid and Predictor Grids)
    ----------
    [statistics_regression.8]\n
    Linear regression analysis of one grid as dependent and multiple grids as independent (predictor) variables. Details of the regression/correlation analysis will be saved to a table. Optionally the regression model is used to create a new grid with regression based values. The multiple regression analysis uses a forward selection procedure.\n
    Arguments
    ----------
    - DEPENDENT [`input grid`] : Dependent Variable
    - PREDICTORS [`input grid list`] : Predictors
    - REGRESSION [`output grid`] : Regression
    - RESIDUALS [`output grid`] : Residuals
    - INFO_COEFF [`output table`] : Details: Coefficients
    - INFO_MODEL [`output table`] : Details: Model
    - INFO_STEPS [`output table`] : Details: Steps
    - DEPENDENT_GRIDSYSTEM [`grid system`] : Grid system
    - RESAMPLING [`choice`] : Resampling. Available Choices: [0] Nearest Neighbour [1] Bilinear Interpolation [2] Bicubic Spline Interpolation [3] B-Spline Interpolation Default: 3
    - COORD_X [`boolean`] : Include X Coordinate. Default: 0
    - COORD_Y [`boolean`] : Include Y Coordinate. Default: 0
    - METHOD [`choice`] : Method. Available Choices: [0] include all [1] forward [2] backward [3] stepwise Default: 3
    - P_VALUE [`floating point number`] : Significance Level. Minimum: 0.000000 Maximum: 100.000000 Default: 5.000000 Significance level (aka p-value) as threshold for automated predictor selection, given as percentage
    - CROSSVAL [`choice`] : Cross Validation. Available Choices: [0] none [1] leave one out [2] 2-fold [3] k-fold Default: 0
    - CROSSVAL_K [`integer number`] : Cross Validation Subsamples. Minimum: 2 Default: 10 number of subsamples for k-fold cross validation

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('statistics_regression', '8', 'Multiple Regression Analysis (Grid and Predictor Grids)')
    if Tool.is_Okay():
        Tool.Set_Input ('DEPENDENT', DEPENDENT)
        Tool.Set_Input ('PREDICTORS', PREDICTORS)
        Tool.Set_Output('REGRESSION', REGRESSION)
        Tool.Set_Output('RESIDUALS', RESIDUALS)
        Tool.Set_Output('INFO_COEFF', INFO_COEFF)
        Tool.Set_Output('INFO_MODEL', INFO_MODEL)
        Tool.Set_Output('INFO_STEPS', INFO_STEPS)
        Tool.Set_Option('DEPENDENT_GRIDSYSTEM', DEPENDENT_GRIDSYSTEM)
        Tool.Set_Option('RESAMPLING', RESAMPLING)
        Tool.Set_Option('COORD_X', COORD_X)
        Tool.Set_Option('COORD_Y', COORD_Y)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('P_VALUE', P_VALUE)
        Tool.Set_Option('CROSSVAL', CROSSVAL)
        Tool.Set_Option('CROSSVAL_K', CROSSVAL_K)
        return Tool.Execute(Verbose)
    return False

def Cellwise_Trend_for_Grids(Y_GRIDS=None, X_GRIDS=None, COEFF=None, R=None, R2=None, R2ADJ=None, P=None, STDERR=None, LINEAR=None, ORDER=None, XSOURCE=None, X_TABLE=None, Verbose=2):
    '''
    Cellwise Trend for Grids
    ----------
    [statistics_regression.9]\n
    Fits for each cell a polynomial trend function. Outputs are the polynomial coefficients for the polynomial trend function of chosen order.\n
    Arguments
    ----------
    - Y_GRIDS [`input grid list`] : Dependent Variables
    - X_GRIDS [`optional input grid list`] : Independent Variable (per Grid and Cell)
    - COEFF [`output grid list`] : Polynomial Coefficients
    - R [`output grid`] : Regression Coefficient
    - R2 [`output grid`] : Determination Coefficient
    - R2ADJ [`output grid`] : Adjusted Determination Coefficient
    - P [`output grid`] : Significance Level
    - STDERR [`output grid`] : Standard Error
    - LINEAR [`boolean`] : Linear Trend. Default: 1
    - ORDER [`integer number`] : Polynomial Order. Minimum: 1 Default: 2 Order of the polynomial trend function.
    - XSOURCE [`choice`] : Get Independent Variable from .... Available Choices: [0] list order [1] table [2] grid list Default: 0
    - X_TABLE [`static table`] : Independent Variable (per Grid). 1 Fields: - 1. [8 byte floating point number] Value 

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('statistics_regression', '9', 'Cellwise Trend for Grids')
    if Tool.is_Okay():
        Tool.Set_Input ('Y_GRIDS', Y_GRIDS)
        Tool.Set_Input ('X_GRIDS', X_GRIDS)
        Tool.Set_Output('COEFF', COEFF)
        Tool.Set_Output('R', R)
        Tool.Set_Output('R2', R2)
        Tool.Set_Output('R2ADJ', R2ADJ)
        Tool.Set_Output('P', P)
        Tool.Set_Output('STDERR', STDERR)
        Tool.Set_Option('LINEAR', LINEAR)
        Tool.Set_Option('ORDER', ORDER)
        Tool.Set_Option('XSOURCE', XSOURCE)
        Tool.Set_Option('X_TABLE', X_TABLE)
        return Tool.Execute(Verbose)
    return False

def run_tool_statistics_regression_9(Y_GRIDS=None, X_GRIDS=None, COEFF=None, R=None, R2=None, R2ADJ=None, P=None, STDERR=None, LINEAR=None, ORDER=None, XSOURCE=None, X_TABLE=None, Verbose=2):
    '''
    Cellwise Trend for Grids
    ----------
    [statistics_regression.9]\n
    Fits for each cell a polynomial trend function. Outputs are the polynomial coefficients for the polynomial trend function of chosen order.\n
    Arguments
    ----------
    - Y_GRIDS [`input grid list`] : Dependent Variables
    - X_GRIDS [`optional input grid list`] : Independent Variable (per Grid and Cell)
    - COEFF [`output grid list`] : Polynomial Coefficients
    - R [`output grid`] : Regression Coefficient
    - R2 [`output grid`] : Determination Coefficient
    - R2ADJ [`output grid`] : Adjusted Determination Coefficient
    - P [`output grid`] : Significance Level
    - STDERR [`output grid`] : Standard Error
    - LINEAR [`boolean`] : Linear Trend. Default: 1
    - ORDER [`integer number`] : Polynomial Order. Minimum: 1 Default: 2 Order of the polynomial trend function.
    - XSOURCE [`choice`] : Get Independent Variable from .... Available Choices: [0] list order [1] table [2] grid list Default: 0
    - X_TABLE [`static table`] : Independent Variable (per Grid). 1 Fields: - 1. [8 byte floating point number] Value 

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('statistics_regression', '9', 'Cellwise Trend for Grids')
    if Tool.is_Okay():
        Tool.Set_Input ('Y_GRIDS', Y_GRIDS)
        Tool.Set_Input ('X_GRIDS', X_GRIDS)
        Tool.Set_Output('COEFF', COEFF)
        Tool.Set_Output('R', R)
        Tool.Set_Output('R2', R2)
        Tool.Set_Output('R2ADJ', R2ADJ)
        Tool.Set_Output('P', P)
        Tool.Set_Output('STDERR', STDERR)
        Tool.Set_Option('LINEAR', LINEAR)
        Tool.Set_Option('ORDER', ORDER)
        Tool.Set_Option('XSOURCE', XSOURCE)
        Tool.Set_Option('X_TABLE', X_TABLE)
        return Tool.Execute(Verbose)
    return False

def Trend_Analysis(TABLE=None, TREND=None, FIELD_X=None, FIELD_Y=None, FORMULA=None, FORMULAS=None, Verbose=2):
    '''
    Trend Analysis
    ----------
    [statistics_regression.10]\n
    Trend Analysis\n
    Arguments
    ----------
    - TABLE [`input table`] : Table
    - TREND [`output table`] : Table (with Trend)
    - FIELD_X [`table field`] : X Values
    - FIELD_Y [`table field`] : Y Values
    - FORMULA [`text`] : Formula. Default: m * x + b
    - FORMULAS [`choice`] : Pre-defined Formulas. Available Choices: [0] a + b * x (linear) [1] a + b * x + c * x^2 (quadric) [2] a + b * x + c * x^2 + d * x^3 (cubic) [3] a + b * ln(x) (logarithmic) [4] a + b * x^c (power) [5] a + b / x [6] a + b * (1 - exp(-x / c)) [7] a + b * (1 - exp(-(x / c)^2)) Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('statistics_regression', '10', 'Trend Analysis')
    if Tool.is_Okay():
        Tool.Set_Input ('TABLE', TABLE)
        Tool.Set_Output('TREND', TREND)
        Tool.Set_Option('FIELD_X', FIELD_X)
        Tool.Set_Option('FIELD_Y', FIELD_Y)
        Tool.Set_Option('FORMULA', FORMULA)
        Tool.Set_Option('FORMULAS', FORMULAS)
        return Tool.Execute(Verbose)
    return False

def run_tool_statistics_regression_10(TABLE=None, TREND=None, FIELD_X=None, FIELD_Y=None, FORMULA=None, FORMULAS=None, Verbose=2):
    '''
    Trend Analysis
    ----------
    [statistics_regression.10]\n
    Trend Analysis\n
    Arguments
    ----------
    - TABLE [`input table`] : Table
    - TREND [`output table`] : Table (with Trend)
    - FIELD_X [`table field`] : X Values
    - FIELD_Y [`table field`] : Y Values
    - FORMULA [`text`] : Formula. Default: m * x + b
    - FORMULAS [`choice`] : Pre-defined Formulas. Available Choices: [0] a + b * x (linear) [1] a + b * x + c * x^2 (quadric) [2] a + b * x + c * x^2 + d * x^3 (cubic) [3] a + b * ln(x) (logarithmic) [4] a + b * x^c (power) [5] a + b / x [6] a + b * (1 - exp(-x / c)) [7] a + b * (1 - exp(-(x / c)^2)) Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('statistics_regression', '10', 'Trend Analysis')
    if Tool.is_Okay():
        Tool.Set_Input ('TABLE', TABLE)
        Tool.Set_Output('TREND', TREND)
        Tool.Set_Option('FIELD_X', FIELD_X)
        Tool.Set_Option('FIELD_Y', FIELD_Y)
        Tool.Set_Option('FORMULA', FORMULA)
        Tool.Set_Option('FORMULAS', FORMULAS)
        return Tool.Execute(Verbose)
    return False

def Trend_Analysis_Shapes(TABLE=None, TREND=None, FIELD_X=None, FIELD_Y=None, FORMULA=None, FORMULAS=None, Verbose=2):
    '''
    Trend Analysis (Shapes)
    ----------
    [statistics_regression.11]\n
    Trend Analysis (Shapes)\n
    Arguments
    ----------
    - TABLE [`input shapes`] : Shapes
    - TREND [`output table`] : Table (with Trend)
    - FIELD_X [`table field`] : X Values
    - FIELD_Y [`table field`] : Y Values
    - FORMULA [`text`] : Formula. Default: m * x + b
    - FORMULAS [`choice`] : Pre-defined Formulas. Available Choices: [0] a + b * x (linear) [1] a + b * x + c * x^2 (quadric) [2] a + b * x + c * x^2 + d * x^3 (cubic) [3] a + b * ln(x) (logarithmic) [4] a + b * x^c (power) [5] a + b / x [6] a + b * (1 - exp(-x / c)) [7] a + b * (1 - exp(-(x / c)^2)) Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('statistics_regression', '11', 'Trend Analysis (Shapes)')
    if Tool.is_Okay():
        Tool.Set_Input ('TABLE', TABLE)
        Tool.Set_Output('TREND', TREND)
        Tool.Set_Option('FIELD_X', FIELD_X)
        Tool.Set_Option('FIELD_Y', FIELD_Y)
        Tool.Set_Option('FORMULA', FORMULA)
        Tool.Set_Option('FORMULAS', FORMULAS)
        return Tool.Execute(Verbose)
    return False

def run_tool_statistics_regression_11(TABLE=None, TREND=None, FIELD_X=None, FIELD_Y=None, FORMULA=None, FORMULAS=None, Verbose=2):
    '''
    Trend Analysis (Shapes)
    ----------
    [statistics_regression.11]\n
    Trend Analysis (Shapes)\n
    Arguments
    ----------
    - TABLE [`input shapes`] : Shapes
    - TREND [`output table`] : Table (with Trend)
    - FIELD_X [`table field`] : X Values
    - FIELD_Y [`table field`] : Y Values
    - FORMULA [`text`] : Formula. Default: m * x + b
    - FORMULAS [`choice`] : Pre-defined Formulas. Available Choices: [0] a + b * x (linear) [1] a + b * x + c * x^2 (quadric) [2] a + b * x + c * x^2 + d * x^3 (cubic) [3] a + b * ln(x) (logarithmic) [4] a + b * x^c (power) [5] a + b / x [6] a + b * (1 - exp(-x / c)) [7] a + b * (1 - exp(-(x / c)^2)) Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('statistics_regression', '11', 'Trend Analysis (Shapes)')
    if Tool.is_Okay():
        Tool.Set_Input ('TABLE', TABLE)
        Tool.Set_Output('TREND', TREND)
        Tool.Set_Option('FIELD_X', FIELD_X)
        Tool.Set_Option('FIELD_Y', FIELD_Y)
        Tool.Set_Option('FORMULA', FORMULA)
        Tool.Set_Option('FORMULAS', FORMULAS)
        return Tool.Execute(Verbose)
    return False

def Multiple_Linear_Regression_Analysis_Table(TABLE=None, RESULTS=None, INFO_COEFF=None, INFO_MODEL=None, INFO_STEPS=None, DEPENDENT=None, METHOD=None, P_VALUE=None, CROSSVAL=None, CROSSVAL_K=None, Verbose=2):
    '''
    Multiple Linear Regression Analysis (Table)
    ----------
    [statistics_regression.12]\n
    Multiple linear regression analysis using ordinary least squares.\n
    Arguments
    ----------
    - TABLE [`input table`] : Table
    - RESULTS [`output table`] : Results
    - INFO_COEFF [`output table`] : Details: Coefficients
    - INFO_MODEL [`output table`] : Details: Model
    - INFO_STEPS [`output table`] : Details: Steps
    - DEPENDENT [`table field`] : Dependent Variable
    - METHOD [`choice`] : Method. Available Choices: [0] include all [1] forward [2] backward [3] stepwise Default: 3
    - P_VALUE [`floating point number`] : Significance Level. Minimum: 0.000000 Maximum: 100.000000 Default: 5.000000 Significance level (aka p-value) as threshold for automated predictor selection, given as percentage
    - CROSSVAL [`choice`] : Cross Validation. Available Choices: [0] none [1] leave one out [2] 2-fold [3] k-fold Default: 0
    - CROSSVAL_K [`integer number`] : Cross Validation Subsamples. Minimum: 2 Default: 10 number of subsamples for k-fold cross validation

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('statistics_regression', '12', 'Multiple Linear Regression Analysis (Table)')
    if Tool.is_Okay():
        Tool.Set_Input ('TABLE', TABLE)
        Tool.Set_Output('RESULTS', RESULTS)
        Tool.Set_Output('INFO_COEFF', INFO_COEFF)
        Tool.Set_Output('INFO_MODEL', INFO_MODEL)
        Tool.Set_Output('INFO_STEPS', INFO_STEPS)
        Tool.Set_Option('DEPENDENT', DEPENDENT)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('P_VALUE', P_VALUE)
        Tool.Set_Option('CROSSVAL', CROSSVAL)
        Tool.Set_Option('CROSSVAL_K', CROSSVAL_K)
        return Tool.Execute(Verbose)
    return False

def run_tool_statistics_regression_12(TABLE=None, RESULTS=None, INFO_COEFF=None, INFO_MODEL=None, INFO_STEPS=None, DEPENDENT=None, METHOD=None, P_VALUE=None, CROSSVAL=None, CROSSVAL_K=None, Verbose=2):
    '''
    Multiple Linear Regression Analysis (Table)
    ----------
    [statistics_regression.12]\n
    Multiple linear regression analysis using ordinary least squares.\n
    Arguments
    ----------
    - TABLE [`input table`] : Table
    - RESULTS [`output table`] : Results
    - INFO_COEFF [`output table`] : Details: Coefficients
    - INFO_MODEL [`output table`] : Details: Model
    - INFO_STEPS [`output table`] : Details: Steps
    - DEPENDENT [`table field`] : Dependent Variable
    - METHOD [`choice`] : Method. Available Choices: [0] include all [1] forward [2] backward [3] stepwise Default: 3
    - P_VALUE [`floating point number`] : Significance Level. Minimum: 0.000000 Maximum: 100.000000 Default: 5.000000 Significance level (aka p-value) as threshold for automated predictor selection, given as percentage
    - CROSSVAL [`choice`] : Cross Validation. Available Choices: [0] none [1] leave one out [2] 2-fold [3] k-fold Default: 0
    - CROSSVAL_K [`integer number`] : Cross Validation Subsamples. Minimum: 2 Default: 10 number of subsamples for k-fold cross validation

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('statistics_regression', '12', 'Multiple Linear Regression Analysis (Table)')
    if Tool.is_Okay():
        Tool.Set_Input ('TABLE', TABLE)
        Tool.Set_Output('RESULTS', RESULTS)
        Tool.Set_Output('INFO_COEFF', INFO_COEFF)
        Tool.Set_Output('INFO_MODEL', INFO_MODEL)
        Tool.Set_Output('INFO_STEPS', INFO_STEPS)
        Tool.Set_Option('DEPENDENT', DEPENDENT)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('P_VALUE', P_VALUE)
        Tool.Set_Option('CROSSVAL', CROSSVAL)
        Tool.Set_Option('CROSSVAL_K', CROSSVAL_K)
        return Tool.Execute(Verbose)
    return False

def Multiple_Linear_Regression_Analysis_Shapes(TABLE=None, RESULTS=None, INFO_COEFF=None, INFO_MODEL=None, INFO_STEPS=None, DEPENDENT=None, METHOD=None, P_VALUE=None, CROSSVAL=None, CROSSVAL_K=None, Verbose=2):
    '''
    Multiple Linear Regression Analysis (Shapes)
    ----------
    [statistics_regression.13]\n
    Multiple linear regression analysis using ordinary least squares.\n
    Arguments
    ----------
    - TABLE [`input shapes`] : Shapes
    - RESULTS [`output shapes`] : Results
    - INFO_COEFF [`output table`] : Details: Coefficients
    - INFO_MODEL [`output table`] : Details: Model
    - INFO_STEPS [`output table`] : Details: Steps
    - DEPENDENT [`table field`] : Dependent Variable
    - METHOD [`choice`] : Method. Available Choices: [0] include all [1] forward [2] backward [3] stepwise Default: 3
    - P_VALUE [`floating point number`] : Significance Level. Minimum: 0.000000 Maximum: 100.000000 Default: 5.000000 Significance level (aka p-value) as threshold for automated predictor selection, given as percentage
    - CROSSVAL [`choice`] : Cross Validation. Available Choices: [0] none [1] leave one out [2] 2-fold [3] k-fold Default: 0
    - CROSSVAL_K [`integer number`] : Cross Validation Subsamples. Minimum: 2 Default: 10 number of subsamples for k-fold cross validation

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('statistics_regression', '13', 'Multiple Linear Regression Analysis (Shapes)')
    if Tool.is_Okay():
        Tool.Set_Input ('TABLE', TABLE)
        Tool.Set_Output('RESULTS', RESULTS)
        Tool.Set_Output('INFO_COEFF', INFO_COEFF)
        Tool.Set_Output('INFO_MODEL', INFO_MODEL)
        Tool.Set_Output('INFO_STEPS', INFO_STEPS)
        Tool.Set_Option('DEPENDENT', DEPENDENT)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('P_VALUE', P_VALUE)
        Tool.Set_Option('CROSSVAL', CROSSVAL)
        Tool.Set_Option('CROSSVAL_K', CROSSVAL_K)
        return Tool.Execute(Verbose)
    return False

def run_tool_statistics_regression_13(TABLE=None, RESULTS=None, INFO_COEFF=None, INFO_MODEL=None, INFO_STEPS=None, DEPENDENT=None, METHOD=None, P_VALUE=None, CROSSVAL=None, CROSSVAL_K=None, Verbose=2):
    '''
    Multiple Linear Regression Analysis (Shapes)
    ----------
    [statistics_regression.13]\n
    Multiple linear regression analysis using ordinary least squares.\n
    Arguments
    ----------
    - TABLE [`input shapes`] : Shapes
    - RESULTS [`output shapes`] : Results
    - INFO_COEFF [`output table`] : Details: Coefficients
    - INFO_MODEL [`output table`] : Details: Model
    - INFO_STEPS [`output table`] : Details: Steps
    - DEPENDENT [`table field`] : Dependent Variable
    - METHOD [`choice`] : Method. Available Choices: [0] include all [1] forward [2] backward [3] stepwise Default: 3
    - P_VALUE [`floating point number`] : Significance Level. Minimum: 0.000000 Maximum: 100.000000 Default: 5.000000 Significance level (aka p-value) as threshold for automated predictor selection, given as percentage
    - CROSSVAL [`choice`] : Cross Validation. Available Choices: [0] none [1] leave one out [2] 2-fold [3] k-fold Default: 0
    - CROSSVAL_K [`integer number`] : Cross Validation Subsamples. Minimum: 2 Default: 10 number of subsamples for k-fold cross validation

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('statistics_regression', '13', 'Multiple Linear Regression Analysis (Shapes)')
    if Tool.is_Okay():
        Tool.Set_Input ('TABLE', TABLE)
        Tool.Set_Output('RESULTS', RESULTS)
        Tool.Set_Output('INFO_COEFF', INFO_COEFF)
        Tool.Set_Output('INFO_MODEL', INFO_MODEL)
        Tool.Set_Output('INFO_STEPS', INFO_STEPS)
        Tool.Set_Option('DEPENDENT', DEPENDENT)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('P_VALUE', P_VALUE)
        Tool.Set_Option('CROSSVAL', CROSSVAL)
        Tool.Set_Option('CROSSVAL_K', CROSSVAL_K)
        return Tool.Execute(Verbose)
    return False

def GWR_for_Grid_Downscaling(PREDICTORS=None, DEPENDENT=None, REGRESSION=None, REG_RESCORR=None, QUALITY=None, RESIDUALS=None, MODEL=None, GRID_SYSTEM=None, LOGISTIC=None, MODEL_OUT=None, SEARCH_RANGE=None, SEARCH_RADIUS=None, DW_WEIGHTING=None, DW_IDW_POWER=None, DW_BANDWIDTH=None, Verbose=2):
    '''
    GWR for Grid Downscaling
    ----------
    [statistics_regression.14]\n
    Geographically Weighted Regression for grid downscaling.\n
    Arguments
    ----------
    - PREDICTORS [`input grid list`] : Predictors
    - DEPENDENT [`input grid`] : Dependent Variable
    - REGRESSION [`output grid`] : Regression
    - REG_RESCORR [`output grid`] : Regression with Residual Correction
    - QUALITY [`output grid`] : Coefficient of Determination
    - RESIDUALS [`output grid`] : Residuals
    - MODEL [`output grid list`] : Regression Parameters
    - GRID_SYSTEM [`grid system`] : Grid System
    - LOGISTIC [`boolean`] : Logistic Regression. Default: 0
    - MODEL_OUT [`boolean`] : Output of Model Parameters. Default: 0
    - SEARCH_RANGE [`choice`] : Search Range. Available Choices: [0] local [1] global Default: 0
    - SEARCH_RADIUS [`integer number`] : Search Distance [Cells]. Minimum: 1 Default: 10
    - DW_WEIGHTING [`choice`] : Weighting Function. Available Choices: [0] no distance weighting [1] inverse distance to a power [2] exponential [3] gaussian Default: 3
    - DW_IDW_POWER [`floating point number`] : Power. Minimum: 0.000000 Default: 2.000000
    - DW_BANDWIDTH [`floating point number`] : Bandwidth. Minimum: 0.000000 Default: 7.000000 Bandwidth for exponential and Gaussian weighting

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('statistics_regression', '14', 'GWR for Grid Downscaling')
    if Tool.is_Okay():
        Tool.Set_Input ('PREDICTORS', PREDICTORS)
        Tool.Set_Input ('DEPENDENT', DEPENDENT)
        Tool.Set_Output('REGRESSION', REGRESSION)
        Tool.Set_Output('REG_RESCORR', REG_RESCORR)
        Tool.Set_Output('QUALITY', QUALITY)
        Tool.Set_Output('RESIDUALS', RESIDUALS)
        Tool.Set_Output('MODEL', MODEL)
        Tool.Set_Option('GRID_SYSTEM', GRID_SYSTEM)
        Tool.Set_Option('LOGISTIC', LOGISTIC)
        Tool.Set_Option('MODEL_OUT', MODEL_OUT)
        Tool.Set_Option('SEARCH_RANGE', SEARCH_RANGE)
        Tool.Set_Option('SEARCH_RADIUS', SEARCH_RADIUS)
        Tool.Set_Option('DW_WEIGHTING', DW_WEIGHTING)
        Tool.Set_Option('DW_IDW_POWER', DW_IDW_POWER)
        Tool.Set_Option('DW_BANDWIDTH', DW_BANDWIDTH)
        return Tool.Execute(Verbose)
    return False

def run_tool_statistics_regression_14(PREDICTORS=None, DEPENDENT=None, REGRESSION=None, REG_RESCORR=None, QUALITY=None, RESIDUALS=None, MODEL=None, GRID_SYSTEM=None, LOGISTIC=None, MODEL_OUT=None, SEARCH_RANGE=None, SEARCH_RADIUS=None, DW_WEIGHTING=None, DW_IDW_POWER=None, DW_BANDWIDTH=None, Verbose=2):
    '''
    GWR for Grid Downscaling
    ----------
    [statistics_regression.14]\n
    Geographically Weighted Regression for grid downscaling.\n
    Arguments
    ----------
    - PREDICTORS [`input grid list`] : Predictors
    - DEPENDENT [`input grid`] : Dependent Variable
    - REGRESSION [`output grid`] : Regression
    - REG_RESCORR [`output grid`] : Regression with Residual Correction
    - QUALITY [`output grid`] : Coefficient of Determination
    - RESIDUALS [`output grid`] : Residuals
    - MODEL [`output grid list`] : Regression Parameters
    - GRID_SYSTEM [`grid system`] : Grid System
    - LOGISTIC [`boolean`] : Logistic Regression. Default: 0
    - MODEL_OUT [`boolean`] : Output of Model Parameters. Default: 0
    - SEARCH_RANGE [`choice`] : Search Range. Available Choices: [0] local [1] global Default: 0
    - SEARCH_RADIUS [`integer number`] : Search Distance [Cells]. Minimum: 1 Default: 10
    - DW_WEIGHTING [`choice`] : Weighting Function. Available Choices: [0] no distance weighting [1] inverse distance to a power [2] exponential [3] gaussian Default: 3
    - DW_IDW_POWER [`floating point number`] : Power. Minimum: 0.000000 Default: 2.000000
    - DW_BANDWIDTH [`floating point number`] : Bandwidth. Minimum: 0.000000 Default: 7.000000 Bandwidth for exponential and Gaussian weighting

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('statistics_regression', '14', 'GWR for Grid Downscaling')
    if Tool.is_Okay():
        Tool.Set_Input ('PREDICTORS', PREDICTORS)
        Tool.Set_Input ('DEPENDENT', DEPENDENT)
        Tool.Set_Output('REGRESSION', REGRESSION)
        Tool.Set_Output('REG_RESCORR', REG_RESCORR)
        Tool.Set_Output('QUALITY', QUALITY)
        Tool.Set_Output('RESIDUALS', RESIDUALS)
        Tool.Set_Output('MODEL', MODEL)
        Tool.Set_Option('GRID_SYSTEM', GRID_SYSTEM)
        Tool.Set_Option('LOGISTIC', LOGISTIC)
        Tool.Set_Option('MODEL_OUT', MODEL_OUT)
        Tool.Set_Option('SEARCH_RANGE', SEARCH_RANGE)
        Tool.Set_Option('SEARCH_RADIUS', SEARCH_RADIUS)
        Tool.Set_Option('DW_WEIGHTING', DW_WEIGHTING)
        Tool.Set_Option('DW_IDW_POWER', DW_IDW_POWER)
        Tool.Set_Option('DW_BANDWIDTH', DW_BANDWIDTH)
        return Tool.Execute(Verbose)
    return False

def Zonal_Multiple_Regression_Analysis_Points_and_Predictor_Grids(PREDICTORS=None, ZONES=None, POINTS=None, RESIDUALS=None, REGRESSION=None, ATTRIBUTE=None, RESAMPLING=None, COORD_X=None, COORD_Y=None, INTERCEPT=None, METHOD=None, P_VALUE=None, Verbose=2):
    '''
    Zonal Multiple Regression Analysis (Points and Predictor Grids)
    ----------
    [statistics_regression.15]\n
    Linear regression analysis of point attributes using multiple predictor grids. Details of the regression/correlation analysis will be saved to a table. The regression function is used to create a new grid with regression based values. The multiple regression analysis uses a forward selection procedure. Each polygon in the zones layer is processed as individual zone.\n
    Reference:\n
    - Bahrenberg, G., Giese, E., Nipper, J. (1992): 'Statistische Methoden in der Geographie 2 - Multivariate Statistik', Stuttgart, 415p.\n
    Arguments
    ----------
    - PREDICTORS [`input grid list`] : Predictors
    - ZONES [`input shapes`] : Zones
    - POINTS [`input shapes`] : Points
    - RESIDUALS [`output shapes`] : Residuals
    - REGRESSION [`output grid`] : Regression
    - ATTRIBUTE [`table field`] : Dependent Variable
    - RESAMPLING [`choice`] : Resampling. Available Choices: [0] Nearest Neighbour [1] Bilinear Interpolation [2] Bicubic Spline Interpolation [3] B-Spline Interpolation Default: 3
    - COORD_X [`boolean`] : Include X Coordinate. Default: 0
    - COORD_Y [`boolean`] : Include Y Coordinate. Default: 0
    - INTERCEPT [`boolean`] : Intercept. Default: 1
    - METHOD [`choice`] : Method. Available Choices: [0] include all [1] forward [2] backward [3] stepwise Default: 3
    - P_VALUE [`floating point number`] : Significance Level. Minimum: 0.000000 Maximum: 100.000000 Default: 5.000000 Significance level (aka p-value) as threshold for automated predictor selection, given as percentage

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('statistics_regression', '15', 'Zonal Multiple Regression Analysis (Points and Predictor Grids)')
    if Tool.is_Okay():
        Tool.Set_Input ('PREDICTORS', PREDICTORS)
        Tool.Set_Input ('ZONES', ZONES)
        Tool.Set_Input ('POINTS', POINTS)
        Tool.Set_Output('RESIDUALS', RESIDUALS)
        Tool.Set_Output('REGRESSION', REGRESSION)
        Tool.Set_Option('ATTRIBUTE', ATTRIBUTE)
        Tool.Set_Option('RESAMPLING', RESAMPLING)
        Tool.Set_Option('COORD_X', COORD_X)
        Tool.Set_Option('COORD_Y', COORD_Y)
        Tool.Set_Option('INTERCEPT', INTERCEPT)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('P_VALUE', P_VALUE)
        return Tool.Execute(Verbose)
    return False

def run_tool_statistics_regression_15(PREDICTORS=None, ZONES=None, POINTS=None, RESIDUALS=None, REGRESSION=None, ATTRIBUTE=None, RESAMPLING=None, COORD_X=None, COORD_Y=None, INTERCEPT=None, METHOD=None, P_VALUE=None, Verbose=2):
    '''
    Zonal Multiple Regression Analysis (Points and Predictor Grids)
    ----------
    [statistics_regression.15]\n
    Linear regression analysis of point attributes using multiple predictor grids. Details of the regression/correlation analysis will be saved to a table. The regression function is used to create a new grid with regression based values. The multiple regression analysis uses a forward selection procedure. Each polygon in the zones layer is processed as individual zone.\n
    Reference:\n
    - Bahrenberg, G., Giese, E., Nipper, J. (1992): 'Statistische Methoden in der Geographie 2 - Multivariate Statistik', Stuttgart, 415p.\n
    Arguments
    ----------
    - PREDICTORS [`input grid list`] : Predictors
    - ZONES [`input shapes`] : Zones
    - POINTS [`input shapes`] : Points
    - RESIDUALS [`output shapes`] : Residuals
    - REGRESSION [`output grid`] : Regression
    - ATTRIBUTE [`table field`] : Dependent Variable
    - RESAMPLING [`choice`] : Resampling. Available Choices: [0] Nearest Neighbour [1] Bilinear Interpolation [2] Bicubic Spline Interpolation [3] B-Spline Interpolation Default: 3
    - COORD_X [`boolean`] : Include X Coordinate. Default: 0
    - COORD_Y [`boolean`] : Include Y Coordinate. Default: 0
    - INTERCEPT [`boolean`] : Intercept. Default: 1
    - METHOD [`choice`] : Method. Available Choices: [0] include all [1] forward [2] backward [3] stepwise Default: 3
    - P_VALUE [`floating point number`] : Significance Level. Minimum: 0.000000 Maximum: 100.000000 Default: 5.000000 Significance level (aka p-value) as threshold for automated predictor selection, given as percentage

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('statistics_regression', '15', 'Zonal Multiple Regression Analysis (Points and Predictor Grids)')
    if Tool.is_Okay():
        Tool.Set_Input ('PREDICTORS', PREDICTORS)
        Tool.Set_Input ('ZONES', ZONES)
        Tool.Set_Input ('POINTS', POINTS)
        Tool.Set_Output('RESIDUALS', RESIDUALS)
        Tool.Set_Output('REGRESSION', REGRESSION)
        Tool.Set_Option('ATTRIBUTE', ATTRIBUTE)
        Tool.Set_Option('RESAMPLING', RESAMPLING)
        Tool.Set_Option('COORD_X', COORD_X)
        Tool.Set_Option('COORD_Y', COORD_Y)
        Tool.Set_Option('INTERCEPT', INTERCEPT)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('P_VALUE', P_VALUE)
        return Tool.Execute(Verbose)
    return False

def Global_Spatial_Autocorrelation_for_Shapes(SHAPES=None, SUMMARY=None, VARIABLE=None, DW_WEIGHTING=None, DW_IDW_POWER=None, DW_BANDWIDTH=None, SEARCH_RANGE=None, SEARCH_RADIUS=None, SEARCH_POINTS_ALL=None, SEARCH_POINTS_MAX=None, Verbose=2):
    '''
    Global Spatial Autocorrelation for Shapes
    ----------
    [statistics_regression.16]\n
    Calculates Moran's I and Geary's C for the selected variable of the input shapes. For geometries other than single points (i.e. multi-points, lines, polygons) the centroids are used as location.\n
    Arguments
    ----------
    - SHAPES [`input shapes`] : Shapes
    - SUMMARY [`output table`] : Summary
    - VARIABLE [`table field`] : Variable
    - DW_WEIGHTING [`choice`] : Weighting Function. Available Choices: [0] no distance weighting [1] inverse distance to a power [2] exponential [3] gaussian Default: 3
    - DW_IDW_POWER [`floating point number`] : Power. Minimum: 0.000000 Default: 1.000000
    - DW_BANDWIDTH [`floating point number`] : Bandwidth. Minimum: 0.000000 Default: 1.000000 Bandwidth for exponential and Gaussian weighting
    - SEARCH_RANGE [`choice`] : Search Range. Available Choices: [0] local [1] global Default: 1
    - SEARCH_RADIUS [`floating point number`] : Maximum Search Distance. Minimum: 0.000000 Default: 1000.000000 local maximum search distance given in map units
    - SEARCH_POINTS_ALL [`choice`] : Number of Points. Available Choices: [0] maximum number of nearest points [1] all points within search distance Default: 1
    - SEARCH_POINTS_MAX [`integer number`] : Maximum. Minimum: 1 Default: 20 maximum number of nearest points

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('statistics_regression', '16', 'Global Spatial Autocorrelation for Shapes')
    if Tool.is_Okay():
        Tool.Set_Input ('SHAPES', SHAPES)
        Tool.Set_Output('SUMMARY', SUMMARY)
        Tool.Set_Option('VARIABLE', VARIABLE)
        Tool.Set_Option('DW_WEIGHTING', DW_WEIGHTING)
        Tool.Set_Option('DW_IDW_POWER', DW_IDW_POWER)
        Tool.Set_Option('DW_BANDWIDTH', DW_BANDWIDTH)
        Tool.Set_Option('SEARCH_RANGE', SEARCH_RANGE)
        Tool.Set_Option('SEARCH_RADIUS', SEARCH_RADIUS)
        Tool.Set_Option('SEARCH_POINTS_ALL', SEARCH_POINTS_ALL)
        Tool.Set_Option('SEARCH_POINTS_MAX', SEARCH_POINTS_MAX)
        return Tool.Execute(Verbose)
    return False

def run_tool_statistics_regression_16(SHAPES=None, SUMMARY=None, VARIABLE=None, DW_WEIGHTING=None, DW_IDW_POWER=None, DW_BANDWIDTH=None, SEARCH_RANGE=None, SEARCH_RADIUS=None, SEARCH_POINTS_ALL=None, SEARCH_POINTS_MAX=None, Verbose=2):
    '''
    Global Spatial Autocorrelation for Shapes
    ----------
    [statistics_regression.16]\n
    Calculates Moran's I and Geary's C for the selected variable of the input shapes. For geometries other than single points (i.e. multi-points, lines, polygons) the centroids are used as location.\n
    Arguments
    ----------
    - SHAPES [`input shapes`] : Shapes
    - SUMMARY [`output table`] : Summary
    - VARIABLE [`table field`] : Variable
    - DW_WEIGHTING [`choice`] : Weighting Function. Available Choices: [0] no distance weighting [1] inverse distance to a power [2] exponential [3] gaussian Default: 3
    - DW_IDW_POWER [`floating point number`] : Power. Minimum: 0.000000 Default: 1.000000
    - DW_BANDWIDTH [`floating point number`] : Bandwidth. Minimum: 0.000000 Default: 1.000000 Bandwidth for exponential and Gaussian weighting
    - SEARCH_RANGE [`choice`] : Search Range. Available Choices: [0] local [1] global Default: 1
    - SEARCH_RADIUS [`floating point number`] : Maximum Search Distance. Minimum: 0.000000 Default: 1000.000000 local maximum search distance given in map units
    - SEARCH_POINTS_ALL [`choice`] : Number of Points. Available Choices: [0] maximum number of nearest points [1] all points within search distance Default: 1
    - SEARCH_POINTS_MAX [`integer number`] : Maximum. Minimum: 1 Default: 20 maximum number of nearest points

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('statistics_regression', '16', 'Global Spatial Autocorrelation for Shapes')
    if Tool.is_Okay():
        Tool.Set_Input ('SHAPES', SHAPES)
        Tool.Set_Output('SUMMARY', SUMMARY)
        Tool.Set_Option('VARIABLE', VARIABLE)
        Tool.Set_Option('DW_WEIGHTING', DW_WEIGHTING)
        Tool.Set_Option('DW_IDW_POWER', DW_IDW_POWER)
        Tool.Set_Option('DW_BANDWIDTH', DW_BANDWIDTH)
        Tool.Set_Option('SEARCH_RANGE', SEARCH_RANGE)
        Tool.Set_Option('SEARCH_RADIUS', SEARCH_RADIUS)
        Tool.Set_Option('SEARCH_POINTS_ALL', SEARCH_POINTS_ALL)
        Tool.Set_Option('SEARCH_POINTS_MAX', SEARCH_POINTS_MAX)
        return Tool.Execute(Verbose)
    return False

