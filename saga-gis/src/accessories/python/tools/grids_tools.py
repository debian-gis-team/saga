#! /usr/bin/env python

'''
Python Interface to SAGA Tools Library
----------
- Category : Grid Collection
- Name     : Grid Collection Tools
- ID       : grids_tools

Description
----------
Tools for grid collections.
'''

from PySAGA.helper import Tool_Wrapper

def Create_a_Grid_Collection(LIST=None, TABLE=None, COPY=None, GRIDS=None, NAME=None, DELETE=None, ATTRIBUTES=None, TABLE_Z=None, NFIELDS=None, ZFIELD=None, FIELDS_NAME0=None, FIELDS_TYPE0=None, FIELDS_NAME1=None, FIELDS_TYPE1=None, COPY_GRIDSYSTEM=None, Verbose=2):
    '''
    Create a Grid Collection
    ----------
    [grids_tools.0]\n
    Create a new grid collection from existing grids.\n
    Arguments
    ----------
    - LIST [`input grid list`] : Single Grids
    - TABLE [`input table`] : Attributes
    - COPY [`input grid collection`] : Copy from Grid Collection
    - GRIDS [`output grid collection`] : Grid Collection
    - NAME [`text`] : Name
    - DELETE [`boolean`] : Delete. Default: 1
    - ATTRIBUTES [`choice`] : Attribute Definition. Available Choices: [0] index and name [1] user defined structure [2] table with values [3] copy from other grid collection Default: 0
    - TABLE_Z [`table field`] : Z Attribute
    - NFIELDS [`integer number`] : Number of Attributes. Minimum: 0 Default: 2
    - ZFIELD [`integer number`] : Z Attribute. Minimum: 1 Default: 1
    - FIELDS_NAME0 [`text`] : Name. Default: Value
    - FIELDS_TYPE0 [`data type`] : Type. Available Choices: [0] string [1] date [2] color [3] unsigned 1 byte integer [4] signed 1 byte integer [5] unsigned 2 byte integer [6] signed 2 byte integer [7] unsigned 4 byte integer [8] signed 4 byte integer [9] unsigned 8 byte integer [10] signed 8 byte integer [11] 4 byte floating point number [12] 8 byte floating point number [13] binary Default: 11
    - FIELDS_NAME1 [`text`] : Name. Default: Value
    - FIELDS_TYPE1 [`data type`] : Type. Available Choices: [0] string [1] date [2] color [3] unsigned 1 byte integer [4] signed 1 byte integer [5] unsigned 2 byte integer [6] signed 2 byte integer [7] unsigned 4 byte integer [8] signed 4 byte integer [9] unsigned 8 byte integer [10] signed 8 byte integer [11] 4 byte floating point number [12] 8 byte floating point number [13] binary Default: 11
    - COPY_GRIDSYSTEM [`grid system`] : Grid system

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grids_tools', '0', 'Create a Grid Collection')
    if Tool.is_Okay():
        Tool.Set_Input ('LIST', LIST)
        Tool.Set_Input ('TABLE', TABLE)
        Tool.Set_Input ('COPY', COPY)
        Tool.Set_Output('GRIDS', GRIDS)
        Tool.Set_Option('NAME', NAME)
        Tool.Set_Option('DELETE', DELETE)
        Tool.Set_Option('ATTRIBUTES', ATTRIBUTES)
        Tool.Set_Option('TABLE_Z', TABLE_Z)
        Tool.Set_Option('NFIELDS', NFIELDS)
        Tool.Set_Option('ZFIELD', ZFIELD)
        Tool.Set_Option('FIELDS.NAME0', FIELDS_NAME0)
        Tool.Set_Option('FIELDS.TYPE0', FIELDS_TYPE0)
        Tool.Set_Option('FIELDS.NAME1', FIELDS_NAME1)
        Tool.Set_Option('FIELDS.TYPE1', FIELDS_TYPE1)
        Tool.Set_Option('COPY_GRIDSYSTEM', COPY_GRIDSYSTEM)
        return Tool.Execute(Verbose)
    return False

def run_tool_grids_tools_0(LIST=None, TABLE=None, COPY=None, GRIDS=None, NAME=None, DELETE=None, ATTRIBUTES=None, TABLE_Z=None, NFIELDS=None, ZFIELD=None, FIELDS_NAME0=None, FIELDS_TYPE0=None, FIELDS_NAME1=None, FIELDS_TYPE1=None, COPY_GRIDSYSTEM=None, Verbose=2):
    '''
    Create a Grid Collection
    ----------
    [grids_tools.0]\n
    Create a new grid collection from existing grids.\n
    Arguments
    ----------
    - LIST [`input grid list`] : Single Grids
    - TABLE [`input table`] : Attributes
    - COPY [`input grid collection`] : Copy from Grid Collection
    - GRIDS [`output grid collection`] : Grid Collection
    - NAME [`text`] : Name
    - DELETE [`boolean`] : Delete. Default: 1
    - ATTRIBUTES [`choice`] : Attribute Definition. Available Choices: [0] index and name [1] user defined structure [2] table with values [3] copy from other grid collection Default: 0
    - TABLE_Z [`table field`] : Z Attribute
    - NFIELDS [`integer number`] : Number of Attributes. Minimum: 0 Default: 2
    - ZFIELD [`integer number`] : Z Attribute. Minimum: 1 Default: 1
    - FIELDS_NAME0 [`text`] : Name. Default: Value
    - FIELDS_TYPE0 [`data type`] : Type. Available Choices: [0] string [1] date [2] color [3] unsigned 1 byte integer [4] signed 1 byte integer [5] unsigned 2 byte integer [6] signed 2 byte integer [7] unsigned 4 byte integer [8] signed 4 byte integer [9] unsigned 8 byte integer [10] signed 8 byte integer [11] 4 byte floating point number [12] 8 byte floating point number [13] binary Default: 11
    - FIELDS_NAME1 [`text`] : Name. Default: Value
    - FIELDS_TYPE1 [`data type`] : Type. Available Choices: [0] string [1] date [2] color [3] unsigned 1 byte integer [4] signed 1 byte integer [5] unsigned 2 byte integer [6] signed 2 byte integer [7] unsigned 4 byte integer [8] signed 4 byte integer [9] unsigned 8 byte integer [10] signed 8 byte integer [11] 4 byte floating point number [12] 8 byte floating point number [13] binary Default: 11
    - COPY_GRIDSYSTEM [`grid system`] : Grid system

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grids_tools', '0', 'Create a Grid Collection')
    if Tool.is_Okay():
        Tool.Set_Input ('LIST', LIST)
        Tool.Set_Input ('TABLE', TABLE)
        Tool.Set_Input ('COPY', COPY)
        Tool.Set_Output('GRIDS', GRIDS)
        Tool.Set_Option('NAME', NAME)
        Tool.Set_Option('DELETE', DELETE)
        Tool.Set_Option('ATTRIBUTES', ATTRIBUTES)
        Tool.Set_Option('TABLE_Z', TABLE_Z)
        Tool.Set_Option('NFIELDS', NFIELDS)
        Tool.Set_Option('ZFIELD', ZFIELD)
        Tool.Set_Option('FIELDS.NAME0', FIELDS_NAME0)
        Tool.Set_Option('FIELDS.TYPE0', FIELDS_TYPE0)
        Tool.Set_Option('FIELDS.NAME1', FIELDS_NAME1)
        Tool.Set_Option('FIELDS.TYPE1', FIELDS_TYPE1)
        Tool.Set_Option('COPY_GRIDSYSTEM', COPY_GRIDSYSTEM)
        return Tool.Execute(Verbose)
    return False

def Extract_Grids_from_a_Grid_Collection(GRIDS=None, LIST=None, SELECTION=None, Verbose=2):
    '''
    Extract Grids from a Grid Collection
    ----------
    [grids_tools.1]\n
    Extracts selected z-level grids from a grid collection.\n
    Arguments
    ----------
    - GRIDS [`input grid collection`] : Grid Collection
    - LIST [`output grid list`] : Single Grids
    - SELECTION [`choices`] : Selection. Available Choices:

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grids_tools', '1', 'Extract Grids from a Grid Collection')
    if Tool.is_Okay():
        Tool.Set_Input ('GRIDS', GRIDS)
        Tool.Set_Output('LIST', LIST)
        Tool.Set_Option('SELECTION', SELECTION)
        return Tool.Execute(Verbose)
    return False

def run_tool_grids_tools_1(GRIDS=None, LIST=None, SELECTION=None, Verbose=2):
    '''
    Extract Grids from a Grid Collection
    ----------
    [grids_tools.1]\n
    Extracts selected z-level grids from a grid collection.\n
    Arguments
    ----------
    - GRIDS [`input grid collection`] : Grid Collection
    - LIST [`output grid list`] : Single Grids
    - SELECTION [`choices`] : Selection. Available Choices:

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grids_tools', '1', 'Extract Grids from a Grid Collection')
    if Tool.is_Okay():
        Tool.Set_Input ('GRIDS', GRIDS)
        Tool.Set_Output('LIST', LIST)
        Tool.Set_Option('SELECTION', SELECTION)
        return Tool.Execute(Verbose)
    return False

def Delete_Grids_from_a_Grid_Collection(GRIDS=None, SELECTION=None, Verbose=2):
    '''
    Delete Grids from a Grid Collection
    ----------
    [grids_tools.2]\n
    Delete Grids from a Grid Collection\n
    Arguments
    ----------
    - GRIDS [`input grid collection`] : Grid Collection
    - SELECTION [`choices`] : Selection. Available Choices:

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grids_tools', '2', 'Delete Grids from a Grid Collection')
    if Tool.is_Okay():
        Tool.Set_Input ('GRIDS', GRIDS)
        Tool.Set_Option('SELECTION', SELECTION)
        return Tool.Execute(Verbose)
    return False

def run_tool_grids_tools_2(GRIDS=None, SELECTION=None, Verbose=2):
    '''
    Delete Grids from a Grid Collection
    ----------
    [grids_tools.2]\n
    Delete Grids from a Grid Collection\n
    Arguments
    ----------
    - GRIDS [`input grid collection`] : Grid Collection
    - SELECTION [`choices`] : Selection. Available Choices:

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grids_tools', '2', 'Delete Grids from a Grid Collection')
    if Tool.is_Okay():
        Tool.Set_Input ('GRIDS', GRIDS)
        Tool.Set_Option('SELECTION', SELECTION)
        return Tool.Execute(Verbose)
    return False

def Extract_a_Grid_from_a_Grid_Collection(GRIDS=None, Z_LEVEL=None, GRID=None, Z_LEVEL_DEFAULT=None, RESAMPLING=None, Verbose=2):
    '''
    Extract a Grid from a Grid Collection
    ----------
    [grids_tools.3]\n
    Extracts grid values from the input grid collection using the chosen interpolation either for a constant or a variable z-level as defined by the z-level input grid.\n
    Arguments
    ----------
    - GRIDS [`input grid collection`] : Grid Collection
    - Z_LEVEL [`optional input grid`] : Z
    - GRID [`output grid`] : Grid
    - Z_LEVEL_DEFAULT [`floating point number`] : Default. Default: 0.000000 default value if no grid has been selected
    - RESAMPLING [`choice`] : Resampling. Available Choices: [0] Nearest Neighbour [1] Linear Interpolation [2] Spline Interpolation Default: 1

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grids_tools', '3', 'Extract a Grid from a Grid Collection')
    if Tool.is_Okay():
        Tool.Set_Input ('GRIDS', GRIDS)
        Tool.Set_Input ('Z_LEVEL', Z_LEVEL)
        Tool.Set_Output('GRID', GRID)
        Tool.Set_Option('Z_LEVEL_DEFAULT', Z_LEVEL_DEFAULT)
        Tool.Set_Option('RESAMPLING', RESAMPLING)
        return Tool.Execute(Verbose)
    return False

def run_tool_grids_tools_3(GRIDS=None, Z_LEVEL=None, GRID=None, Z_LEVEL_DEFAULT=None, RESAMPLING=None, Verbose=2):
    '''
    Extract a Grid from a Grid Collection
    ----------
    [grids_tools.3]\n
    Extracts grid values from the input grid collection using the chosen interpolation either for a constant or a variable z-level as defined by the z-level input grid.\n
    Arguments
    ----------
    - GRIDS [`input grid collection`] : Grid Collection
    - Z_LEVEL [`optional input grid`] : Z
    - GRID [`output grid`] : Grid
    - Z_LEVEL_DEFAULT [`floating point number`] : Default. Default: 0.000000 default value if no grid has been selected
    - RESAMPLING [`choice`] : Resampling. Available Choices: [0] Nearest Neighbour [1] Linear Interpolation [2] Spline Interpolation Default: 1

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grids_tools', '3', 'Extract a Grid from a Grid Collection')
    if Tool.is_Okay():
        Tool.Set_Input ('GRIDS', GRIDS)
        Tool.Set_Input ('Z_LEVEL', Z_LEVEL)
        Tool.Set_Output('GRID', GRID)
        Tool.Set_Option('Z_LEVEL_DEFAULT', Z_LEVEL_DEFAULT)
        Tool.Set_Option('RESAMPLING', RESAMPLING)
        return Tool.Execute(Verbose)
    return False

def Add_a_Grid_to_a_Grid_Collection(GRID=None, GRIDS=None, Z_LEVEL=None, DELETE=None, Verbose=2):
    '''
    Add a Grid to a Grid Collection
    ----------
    [grids_tools.4]\n
    Adds a grid at the specified z-level to an existing grid collection. If no grid collection is supplied it will be created according to the input grid's grid system and data type.\n
    Arguments
    ----------
    - GRID [`input grid`] : Grid
    - GRIDS [`output grid collection`] : Grid Collection
    - Z_LEVEL [`floating point number`] : Z. Default: 0.000000
    - DELETE [`boolean`] : Delete. Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grids_tools', '4', 'Add a Grid to a Grid Collection')
    if Tool.is_Okay():
        Tool.Set_Input ('GRID', GRID)
        Tool.Set_Output('GRIDS', GRIDS)
        Tool.Set_Option('Z_LEVEL', Z_LEVEL)
        Tool.Set_Option('DELETE', DELETE)
        return Tool.Execute(Verbose)
    return False

def run_tool_grids_tools_4(GRID=None, GRIDS=None, Z_LEVEL=None, DELETE=None, Verbose=2):
    '''
    Add a Grid to a Grid Collection
    ----------
    [grids_tools.4]\n
    Adds a grid at the specified z-level to an existing grid collection. If no grid collection is supplied it will be created according to the input grid's grid system and data type.\n
    Arguments
    ----------
    - GRID [`input grid`] : Grid
    - GRIDS [`output grid collection`] : Grid Collection
    - Z_LEVEL [`floating point number`] : Z. Default: 0.000000
    - DELETE [`boolean`] : Delete. Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grids_tools', '4', 'Add a Grid to a Grid Collection')
    if Tool.is_Okay():
        Tool.Set_Input ('GRID', GRID)
        Tool.Set_Output('GRIDS', GRIDS)
        Tool.Set_Option('Z_LEVEL', Z_LEVEL)
        Tool.Set_Option('DELETE', DELETE)
        return Tool.Execute(Verbose)
    return False

def Nearest_Neighbour_3D(POINTS=None, TARGET_TEMPLATE=None, GRIDS=None, Z_FIELD=None, Z_SCALE=None, V_FIELD=None, TARGET_DEFINITION=None, TARGET_USER_SIZE=None, TARGET_USER_XMIN=None, TARGET_USER_XMAX=None, TARGET_USER_YMIN=None, TARGET_USER_YMAX=None, TARGET_USER_COLS=None, TARGET_USER_ROWS=None, TARGET_USER_FLAT=None, TARGET_USER_FITS=None, TARGET_SYSTEM=None, TARGET_USER_ZSIZE=None, TARGET_USER_ZMIN=None, TARGET_USER_ZMAX=None, TARGET_USER_ZNUM=None, Verbose=2):
    '''
    Nearest Neighbour (3D)
    ----------
    [grids_tools.5]\n
    Nearest neighbour interpolation for 3-dimensional data points. Output will be a grid collection with evenly spaced Z-levels representing the 3rd dimension.\n
    Arguments
    ----------
    - POINTS [`input shapes`] : Points
    - TARGET_TEMPLATE [`optional input grid`] : Target System. use this grid's system for output grids
    - GRIDS [`output grid collection`] : Grid Collection
    - Z_FIELD [`table field`] : Z
    - Z_SCALE [`floating point number`] : Z Factor. Default: 1.000000
    - V_FIELD [`table field`] : Value
    - TARGET_DEFINITION [`choice`] : Target Grid System. Available Choices: [0] user defined [1] grid or grid system Default: 0
    - TARGET_USER_SIZE [`floating point number`] : Cellsize. Minimum: 0.000000 Default: 1.000000
    - TARGET_USER_XMIN [`floating point number`] : West. Default: 0.000000
    - TARGET_USER_XMAX [`floating point number`] : East. Default: 100.000000
    - TARGET_USER_YMIN [`floating point number`] : South. Default: 0.000000
    - TARGET_USER_YMAX [`floating point number`] : North. Default: 100.000000
    - TARGET_USER_COLS [`integer number`] : Columns. Minimum: 1 Default: 101 Number of cells in East-West direction.
    - TARGET_USER_ROWS [`integer number`] : Rows. Minimum: 1 Default: 101 Number of cells in North-South direction.
    - TARGET_USER_FLAT [`boolean`] : Rounding. Default: 1 Round bounding coordinates to multiples of cell size. Ignored if cell size has decimal places.
    - TARGET_USER_FITS [`choice`] : Fit. Available Choices: [0] nodes [1] cells Default: 0
    - TARGET_SYSTEM [`grid system`] : Grid System
    - TARGET_USER_ZSIZE [`floating point number`] : Cellsize. Minimum: 0.000000 Default: 1.000000
    - TARGET_USER_ZMIN [`floating point number`] : Bottom. Default: 0.000000
    - TARGET_USER_ZMAX [`floating point number`] : Top. Default: 100.000000
    - TARGET_USER_ZNUM [`integer number`] : Levels. Minimum: 1 Default: 101

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grids_tools', '5', 'Nearest Neighbour (3D)')
    if Tool.is_Okay():
        Tool.Set_Input ('POINTS', POINTS)
        Tool.Set_Input ('TARGET_TEMPLATE', TARGET_TEMPLATE)
        Tool.Set_Output('GRIDS', GRIDS)
        Tool.Set_Option('Z_FIELD', Z_FIELD)
        Tool.Set_Option('Z_SCALE', Z_SCALE)
        Tool.Set_Option('V_FIELD', V_FIELD)
        Tool.Set_Option('TARGET_DEFINITION', TARGET_DEFINITION)
        Tool.Set_Option('TARGET_USER_SIZE', TARGET_USER_SIZE)
        Tool.Set_Option('TARGET_USER_XMIN', TARGET_USER_XMIN)
        Tool.Set_Option('TARGET_USER_XMAX', TARGET_USER_XMAX)
        Tool.Set_Option('TARGET_USER_YMIN', TARGET_USER_YMIN)
        Tool.Set_Option('TARGET_USER_YMAX', TARGET_USER_YMAX)
        Tool.Set_Option('TARGET_USER_COLS', TARGET_USER_COLS)
        Tool.Set_Option('TARGET_USER_ROWS', TARGET_USER_ROWS)
        Tool.Set_Option('TARGET_USER_FLAT', TARGET_USER_FLAT)
        Tool.Set_Option('TARGET_USER_FITS', TARGET_USER_FITS)
        Tool.Set_Option('TARGET_SYSTEM', TARGET_SYSTEM)
        Tool.Set_Option('TARGET_USER_ZSIZE', TARGET_USER_ZSIZE)
        Tool.Set_Option('TARGET_USER_ZMIN', TARGET_USER_ZMIN)
        Tool.Set_Option('TARGET_USER_ZMAX', TARGET_USER_ZMAX)
        Tool.Set_Option('TARGET_USER_ZNUM', TARGET_USER_ZNUM)
        return Tool.Execute(Verbose)
    return False

def run_tool_grids_tools_5(POINTS=None, TARGET_TEMPLATE=None, GRIDS=None, Z_FIELD=None, Z_SCALE=None, V_FIELD=None, TARGET_DEFINITION=None, TARGET_USER_SIZE=None, TARGET_USER_XMIN=None, TARGET_USER_XMAX=None, TARGET_USER_YMIN=None, TARGET_USER_YMAX=None, TARGET_USER_COLS=None, TARGET_USER_ROWS=None, TARGET_USER_FLAT=None, TARGET_USER_FITS=None, TARGET_SYSTEM=None, TARGET_USER_ZSIZE=None, TARGET_USER_ZMIN=None, TARGET_USER_ZMAX=None, TARGET_USER_ZNUM=None, Verbose=2):
    '''
    Nearest Neighbour (3D)
    ----------
    [grids_tools.5]\n
    Nearest neighbour interpolation for 3-dimensional data points. Output will be a grid collection with evenly spaced Z-levels representing the 3rd dimension.\n
    Arguments
    ----------
    - POINTS [`input shapes`] : Points
    - TARGET_TEMPLATE [`optional input grid`] : Target System. use this grid's system for output grids
    - GRIDS [`output grid collection`] : Grid Collection
    - Z_FIELD [`table field`] : Z
    - Z_SCALE [`floating point number`] : Z Factor. Default: 1.000000
    - V_FIELD [`table field`] : Value
    - TARGET_DEFINITION [`choice`] : Target Grid System. Available Choices: [0] user defined [1] grid or grid system Default: 0
    - TARGET_USER_SIZE [`floating point number`] : Cellsize. Minimum: 0.000000 Default: 1.000000
    - TARGET_USER_XMIN [`floating point number`] : West. Default: 0.000000
    - TARGET_USER_XMAX [`floating point number`] : East. Default: 100.000000
    - TARGET_USER_YMIN [`floating point number`] : South. Default: 0.000000
    - TARGET_USER_YMAX [`floating point number`] : North. Default: 100.000000
    - TARGET_USER_COLS [`integer number`] : Columns. Minimum: 1 Default: 101 Number of cells in East-West direction.
    - TARGET_USER_ROWS [`integer number`] : Rows. Minimum: 1 Default: 101 Number of cells in North-South direction.
    - TARGET_USER_FLAT [`boolean`] : Rounding. Default: 1 Round bounding coordinates to multiples of cell size. Ignored if cell size has decimal places.
    - TARGET_USER_FITS [`choice`] : Fit. Available Choices: [0] nodes [1] cells Default: 0
    - TARGET_SYSTEM [`grid system`] : Grid System
    - TARGET_USER_ZSIZE [`floating point number`] : Cellsize. Minimum: 0.000000 Default: 1.000000
    - TARGET_USER_ZMIN [`floating point number`] : Bottom. Default: 0.000000
    - TARGET_USER_ZMAX [`floating point number`] : Top. Default: 100.000000
    - TARGET_USER_ZNUM [`integer number`] : Levels. Minimum: 1 Default: 101

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grids_tools', '5', 'Nearest Neighbour (3D)')
    if Tool.is_Okay():
        Tool.Set_Input ('POINTS', POINTS)
        Tool.Set_Input ('TARGET_TEMPLATE', TARGET_TEMPLATE)
        Tool.Set_Output('GRIDS', GRIDS)
        Tool.Set_Option('Z_FIELD', Z_FIELD)
        Tool.Set_Option('Z_SCALE', Z_SCALE)
        Tool.Set_Option('V_FIELD', V_FIELD)
        Tool.Set_Option('TARGET_DEFINITION', TARGET_DEFINITION)
        Tool.Set_Option('TARGET_USER_SIZE', TARGET_USER_SIZE)
        Tool.Set_Option('TARGET_USER_XMIN', TARGET_USER_XMIN)
        Tool.Set_Option('TARGET_USER_XMAX', TARGET_USER_XMAX)
        Tool.Set_Option('TARGET_USER_YMIN', TARGET_USER_YMIN)
        Tool.Set_Option('TARGET_USER_YMAX', TARGET_USER_YMAX)
        Tool.Set_Option('TARGET_USER_COLS', TARGET_USER_COLS)
        Tool.Set_Option('TARGET_USER_ROWS', TARGET_USER_ROWS)
        Tool.Set_Option('TARGET_USER_FLAT', TARGET_USER_FLAT)
        Tool.Set_Option('TARGET_USER_FITS', TARGET_USER_FITS)
        Tool.Set_Option('TARGET_SYSTEM', TARGET_SYSTEM)
        Tool.Set_Option('TARGET_USER_ZSIZE', TARGET_USER_ZSIZE)
        Tool.Set_Option('TARGET_USER_ZMIN', TARGET_USER_ZMIN)
        Tool.Set_Option('TARGET_USER_ZMAX', TARGET_USER_ZMAX)
        Tool.Set_Option('TARGET_USER_ZNUM', TARGET_USER_ZNUM)
        return Tool.Execute(Verbose)
    return False

def Inverse_Distance_Weighted_3D(POINTS=None, TARGET_TEMPLATE=None, GRIDS=None, Z_FIELD=None, Z_SCALE=None, V_FIELD=None, TARGET_DEFINITION=None, TARGET_USER_SIZE=None, TARGET_USER_XMIN=None, TARGET_USER_XMAX=None, TARGET_USER_YMIN=None, TARGET_USER_YMAX=None, TARGET_USER_COLS=None, TARGET_USER_ROWS=None, TARGET_USER_FLAT=None, TARGET_USER_FITS=None, TARGET_SYSTEM=None, TARGET_USER_ZSIZE=None, TARGET_USER_ZMIN=None, TARGET_USER_ZMAX=None, TARGET_USER_ZNUM=None, SEARCH_RANGE=None, SEARCH_RADIUS=None, SEARCH_POINTS_ALL=None, SEARCH_POINTS_MIN=None, SEARCH_POINTS_MAX=None, DW_WEIGHTING=None, DW_IDW_POWER=None, DW_BANDWIDTH=None, Verbose=2):
    '''
    Inverse Distance Weighted (3D)
    ----------
    [grids_tools.6]\n
    Inverse distance weighted interpolation for 3-dimensional data points. Output will be a grid collection with evenly spaced Z-levels representing the 3rd dimension.\n
    Arguments
    ----------
    - POINTS [`input shapes`] : Points
    - TARGET_TEMPLATE [`optional input grid`] : Target System. use this grid's system for output grids
    - GRIDS [`output grid collection`] : Grid Collection
    - Z_FIELD [`table field`] : Z
    - Z_SCALE [`floating point number`] : Z Factor. Default: 1.000000
    - V_FIELD [`table field`] : Value
    - TARGET_DEFINITION [`choice`] : Target Grid System. Available Choices: [0] user defined [1] grid or grid system Default: 0
    - TARGET_USER_SIZE [`floating point number`] : Cellsize. Minimum: 0.000000 Default: 1.000000
    - TARGET_USER_XMIN [`floating point number`] : West. Default: 0.000000
    - TARGET_USER_XMAX [`floating point number`] : East. Default: 100.000000
    - TARGET_USER_YMIN [`floating point number`] : South. Default: 0.000000
    - TARGET_USER_YMAX [`floating point number`] : North. Default: 100.000000
    - TARGET_USER_COLS [`integer number`] : Columns. Minimum: 1 Default: 101 Number of cells in East-West direction.
    - TARGET_USER_ROWS [`integer number`] : Rows. Minimum: 1 Default: 101 Number of cells in North-South direction.
    - TARGET_USER_FLAT [`boolean`] : Rounding. Default: 1 Round bounding coordinates to multiples of cell size. Ignored if cell size has decimal places.
    - TARGET_USER_FITS [`choice`] : Fit. Available Choices: [0] nodes [1] cells Default: 0
    - TARGET_SYSTEM [`grid system`] : Grid System
    - TARGET_USER_ZSIZE [`floating point number`] : Cellsize. Minimum: 0.000000 Default: 1.000000
    - TARGET_USER_ZMIN [`floating point number`] : Bottom. Default: 0.000000
    - TARGET_USER_ZMAX [`floating point number`] : Top. Default: 100.000000
    - TARGET_USER_ZNUM [`integer number`] : Levels. Minimum: 1 Default: 101
    - SEARCH_RANGE [`choice`] : Search Range. Available Choices: [0] local [1] global Default: 1
    - SEARCH_RADIUS [`floating point number`] : Maximum Search Distance. Minimum: 0.000000 Default: 1000.000000 local maximum search distance given in map units
    - SEARCH_POINTS_ALL [`choice`] : Number of Points. Available Choices: [0] maximum number of nearest points [1] all points within search distance Default: 1
    - SEARCH_POINTS_MIN [`integer number`] : Minimum. Minimum: 1 Default: 1 minimum number of points to use
    - SEARCH_POINTS_MAX [`integer number`] : Maximum. Minimum: 1 Default: 20 maximum number of nearest points
    - DW_WEIGHTING [`choice`] : Weighting Function. Available Choices: [0] no distance weighting [1] inverse distance to a power [2] exponential [3] gaussian Default: 1
    - DW_IDW_POWER [`floating point number`] : Power. Minimum: 0.000000 Default: 2.000000
    - DW_BANDWIDTH [`floating point number`] : Bandwidth. Minimum: 0.000000 Default: 1.000000 Bandwidth for exponential and Gaussian weighting

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grids_tools', '6', 'Inverse Distance Weighted (3D)')
    if Tool.is_Okay():
        Tool.Set_Input ('POINTS', POINTS)
        Tool.Set_Input ('TARGET_TEMPLATE', TARGET_TEMPLATE)
        Tool.Set_Output('GRIDS', GRIDS)
        Tool.Set_Option('Z_FIELD', Z_FIELD)
        Tool.Set_Option('Z_SCALE', Z_SCALE)
        Tool.Set_Option('V_FIELD', V_FIELD)
        Tool.Set_Option('TARGET_DEFINITION', TARGET_DEFINITION)
        Tool.Set_Option('TARGET_USER_SIZE', TARGET_USER_SIZE)
        Tool.Set_Option('TARGET_USER_XMIN', TARGET_USER_XMIN)
        Tool.Set_Option('TARGET_USER_XMAX', TARGET_USER_XMAX)
        Tool.Set_Option('TARGET_USER_YMIN', TARGET_USER_YMIN)
        Tool.Set_Option('TARGET_USER_YMAX', TARGET_USER_YMAX)
        Tool.Set_Option('TARGET_USER_COLS', TARGET_USER_COLS)
        Tool.Set_Option('TARGET_USER_ROWS', TARGET_USER_ROWS)
        Tool.Set_Option('TARGET_USER_FLAT', TARGET_USER_FLAT)
        Tool.Set_Option('TARGET_USER_FITS', TARGET_USER_FITS)
        Tool.Set_Option('TARGET_SYSTEM', TARGET_SYSTEM)
        Tool.Set_Option('TARGET_USER_ZSIZE', TARGET_USER_ZSIZE)
        Tool.Set_Option('TARGET_USER_ZMIN', TARGET_USER_ZMIN)
        Tool.Set_Option('TARGET_USER_ZMAX', TARGET_USER_ZMAX)
        Tool.Set_Option('TARGET_USER_ZNUM', TARGET_USER_ZNUM)
        Tool.Set_Option('SEARCH_RANGE', SEARCH_RANGE)
        Tool.Set_Option('SEARCH_RADIUS', SEARCH_RADIUS)
        Tool.Set_Option('SEARCH_POINTS_ALL', SEARCH_POINTS_ALL)
        Tool.Set_Option('SEARCH_POINTS_MIN', SEARCH_POINTS_MIN)
        Tool.Set_Option('SEARCH_POINTS_MAX', SEARCH_POINTS_MAX)
        Tool.Set_Option('DW_WEIGHTING', DW_WEIGHTING)
        Tool.Set_Option('DW_IDW_POWER', DW_IDW_POWER)
        Tool.Set_Option('DW_BANDWIDTH', DW_BANDWIDTH)
        return Tool.Execute(Verbose)
    return False

def run_tool_grids_tools_6(POINTS=None, TARGET_TEMPLATE=None, GRIDS=None, Z_FIELD=None, Z_SCALE=None, V_FIELD=None, TARGET_DEFINITION=None, TARGET_USER_SIZE=None, TARGET_USER_XMIN=None, TARGET_USER_XMAX=None, TARGET_USER_YMIN=None, TARGET_USER_YMAX=None, TARGET_USER_COLS=None, TARGET_USER_ROWS=None, TARGET_USER_FLAT=None, TARGET_USER_FITS=None, TARGET_SYSTEM=None, TARGET_USER_ZSIZE=None, TARGET_USER_ZMIN=None, TARGET_USER_ZMAX=None, TARGET_USER_ZNUM=None, SEARCH_RANGE=None, SEARCH_RADIUS=None, SEARCH_POINTS_ALL=None, SEARCH_POINTS_MIN=None, SEARCH_POINTS_MAX=None, DW_WEIGHTING=None, DW_IDW_POWER=None, DW_BANDWIDTH=None, Verbose=2):
    '''
    Inverse Distance Weighted (3D)
    ----------
    [grids_tools.6]\n
    Inverse distance weighted interpolation for 3-dimensional data points. Output will be a grid collection with evenly spaced Z-levels representing the 3rd dimension.\n
    Arguments
    ----------
    - POINTS [`input shapes`] : Points
    - TARGET_TEMPLATE [`optional input grid`] : Target System. use this grid's system for output grids
    - GRIDS [`output grid collection`] : Grid Collection
    - Z_FIELD [`table field`] : Z
    - Z_SCALE [`floating point number`] : Z Factor. Default: 1.000000
    - V_FIELD [`table field`] : Value
    - TARGET_DEFINITION [`choice`] : Target Grid System. Available Choices: [0] user defined [1] grid or grid system Default: 0
    - TARGET_USER_SIZE [`floating point number`] : Cellsize. Minimum: 0.000000 Default: 1.000000
    - TARGET_USER_XMIN [`floating point number`] : West. Default: 0.000000
    - TARGET_USER_XMAX [`floating point number`] : East. Default: 100.000000
    - TARGET_USER_YMIN [`floating point number`] : South. Default: 0.000000
    - TARGET_USER_YMAX [`floating point number`] : North. Default: 100.000000
    - TARGET_USER_COLS [`integer number`] : Columns. Minimum: 1 Default: 101 Number of cells in East-West direction.
    - TARGET_USER_ROWS [`integer number`] : Rows. Minimum: 1 Default: 101 Number of cells in North-South direction.
    - TARGET_USER_FLAT [`boolean`] : Rounding. Default: 1 Round bounding coordinates to multiples of cell size. Ignored if cell size has decimal places.
    - TARGET_USER_FITS [`choice`] : Fit. Available Choices: [0] nodes [1] cells Default: 0
    - TARGET_SYSTEM [`grid system`] : Grid System
    - TARGET_USER_ZSIZE [`floating point number`] : Cellsize. Minimum: 0.000000 Default: 1.000000
    - TARGET_USER_ZMIN [`floating point number`] : Bottom. Default: 0.000000
    - TARGET_USER_ZMAX [`floating point number`] : Top. Default: 100.000000
    - TARGET_USER_ZNUM [`integer number`] : Levels. Minimum: 1 Default: 101
    - SEARCH_RANGE [`choice`] : Search Range. Available Choices: [0] local [1] global Default: 1
    - SEARCH_RADIUS [`floating point number`] : Maximum Search Distance. Minimum: 0.000000 Default: 1000.000000 local maximum search distance given in map units
    - SEARCH_POINTS_ALL [`choice`] : Number of Points. Available Choices: [0] maximum number of nearest points [1] all points within search distance Default: 1
    - SEARCH_POINTS_MIN [`integer number`] : Minimum. Minimum: 1 Default: 1 minimum number of points to use
    - SEARCH_POINTS_MAX [`integer number`] : Maximum. Minimum: 1 Default: 20 maximum number of nearest points
    - DW_WEIGHTING [`choice`] : Weighting Function. Available Choices: [0] no distance weighting [1] inverse distance to a power [2] exponential [3] gaussian Default: 1
    - DW_IDW_POWER [`floating point number`] : Power. Minimum: 0.000000 Default: 2.000000
    - DW_BANDWIDTH [`floating point number`] : Bandwidth. Minimum: 0.000000 Default: 1.000000 Bandwidth for exponential and Gaussian weighting

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grids_tools', '6', 'Inverse Distance Weighted (3D)')
    if Tool.is_Okay():
        Tool.Set_Input ('POINTS', POINTS)
        Tool.Set_Input ('TARGET_TEMPLATE', TARGET_TEMPLATE)
        Tool.Set_Output('GRIDS', GRIDS)
        Tool.Set_Option('Z_FIELD', Z_FIELD)
        Tool.Set_Option('Z_SCALE', Z_SCALE)
        Tool.Set_Option('V_FIELD', V_FIELD)
        Tool.Set_Option('TARGET_DEFINITION', TARGET_DEFINITION)
        Tool.Set_Option('TARGET_USER_SIZE', TARGET_USER_SIZE)
        Tool.Set_Option('TARGET_USER_XMIN', TARGET_USER_XMIN)
        Tool.Set_Option('TARGET_USER_XMAX', TARGET_USER_XMAX)
        Tool.Set_Option('TARGET_USER_YMIN', TARGET_USER_YMIN)
        Tool.Set_Option('TARGET_USER_YMAX', TARGET_USER_YMAX)
        Tool.Set_Option('TARGET_USER_COLS', TARGET_USER_COLS)
        Tool.Set_Option('TARGET_USER_ROWS', TARGET_USER_ROWS)
        Tool.Set_Option('TARGET_USER_FLAT', TARGET_USER_FLAT)
        Tool.Set_Option('TARGET_USER_FITS', TARGET_USER_FITS)
        Tool.Set_Option('TARGET_SYSTEM', TARGET_SYSTEM)
        Tool.Set_Option('TARGET_USER_ZSIZE', TARGET_USER_ZSIZE)
        Tool.Set_Option('TARGET_USER_ZMIN', TARGET_USER_ZMIN)
        Tool.Set_Option('TARGET_USER_ZMAX', TARGET_USER_ZMAX)
        Tool.Set_Option('TARGET_USER_ZNUM', TARGET_USER_ZNUM)
        Tool.Set_Option('SEARCH_RANGE', SEARCH_RANGE)
        Tool.Set_Option('SEARCH_RADIUS', SEARCH_RADIUS)
        Tool.Set_Option('SEARCH_POINTS_ALL', SEARCH_POINTS_ALL)
        Tool.Set_Option('SEARCH_POINTS_MIN', SEARCH_POINTS_MIN)
        Tool.Set_Option('SEARCH_POINTS_MAX', SEARCH_POINTS_MAX)
        Tool.Set_Option('DW_WEIGHTING', DW_WEIGHTING)
        Tool.Set_Option('DW_IDW_POWER', DW_IDW_POWER)
        Tool.Set_Option('DW_BANDWIDTH', DW_BANDWIDTH)
        return Tool.Execute(Verbose)
    return False

def Grid_Collection_Masking(GRIDS=None, LOWER=None, UPPER=None, MASKED=None, SURFACES=None, MASKING=None, Verbose=2):
    '''
    Grid Collection Masking
    ----------
    [grids_tools.7]\n
    A masking tool for grid collections. Cells of the input grid collection will be set to no-data, depending on the masking option, if their location is either between or not between the lower and upper surface.\n
    Arguments
    ----------
    - GRIDS [`input grid collection`] : Grid Collection
    - LOWER [`input grid`] : Lower Surface
    - UPPER [`input grid`] : Upper Surface
    - MASKED [`output grid collection`] : Masked Grid Collection
    - SURFACES [`grid system`] : Grid System
    - MASKING [`choice`] : Masking. Available Choices: [0] not between upper and lower surface [1] between upper and lower surface Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grids_tools', '7', 'Grid Collection Masking')
    if Tool.is_Okay():
        Tool.Set_Input ('GRIDS', GRIDS)
        Tool.Set_Input ('LOWER', LOWER)
        Tool.Set_Input ('UPPER', UPPER)
        Tool.Set_Output('MASKED', MASKED)
        Tool.Set_Option('SURFACES', SURFACES)
        Tool.Set_Option('MASKING', MASKING)
        return Tool.Execute(Verbose)
    return False

def run_tool_grids_tools_7(GRIDS=None, LOWER=None, UPPER=None, MASKED=None, SURFACES=None, MASKING=None, Verbose=2):
    '''
    Grid Collection Masking
    ----------
    [grids_tools.7]\n
    A masking tool for grid collections. Cells of the input grid collection will be set to no-data, depending on the masking option, if their location is either between or not between the lower and upper surface.\n
    Arguments
    ----------
    - GRIDS [`input grid collection`] : Grid Collection
    - LOWER [`input grid`] : Lower Surface
    - UPPER [`input grid`] : Upper Surface
    - MASKED [`output grid collection`] : Masked Grid Collection
    - SURFACES [`grid system`] : Grid System
    - MASKING [`choice`] : Masking. Available Choices: [0] not between upper and lower surface [1] between upper and lower surface Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grids_tools', '7', 'Grid Collection Masking')
    if Tool.is_Okay():
        Tool.Set_Input ('GRIDS', GRIDS)
        Tool.Set_Input ('LOWER', LOWER)
        Tool.Set_Input ('UPPER', UPPER)
        Tool.Set_Output('MASKED', MASKED)
        Tool.Set_Option('SURFACES', SURFACES)
        Tool.Set_Option('MASKING', MASKING)
        return Tool.Execute(Verbose)
    return False

def Gaussian_Filter(GRIDS=None, FILTERED=None, GRID_SYSTEM=None, SIGMA=None, KERNEL_RADIUS=None, Verbose=2):
    '''
    Gaussian Filter
    ----------
    [grids_tools.filter_gaussian]\n
    Gaussian filter for grid collections.\n
    Arguments
    ----------
    - GRIDS [`input grid collection`] : Grid Collection
    - FILTERED [`output grid collection`] : Filtered
    - GRID_SYSTEM [`grid system`] : Grid System
    - SIGMA [`floating point number`] : Standard Deviation. Default: 50.000000
    - KERNEL_RADIUS [`integer number`] : Kernel Radius. Default: 2

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grids_tools', 'filter_gaussian', 'Gaussian Filter')
    if Tool.is_Okay():
        Tool.Set_Input ('GRIDS', GRIDS)
        Tool.Set_Output('FILTERED', FILTERED)
        Tool.Set_Option('GRID_SYSTEM', GRID_SYSTEM)
        Tool.Set_Option('SIGMA', SIGMA)
        Tool.Set_Option('KERNEL_RADIUS', KERNEL_RADIUS)
        return Tool.Execute(Verbose)
    return False

def run_tool_grids_tools_filter_gaussian(GRIDS=None, FILTERED=None, GRID_SYSTEM=None, SIGMA=None, KERNEL_RADIUS=None, Verbose=2):
    '''
    Gaussian Filter
    ----------
    [grids_tools.filter_gaussian]\n
    Gaussian filter for grid collections.\n
    Arguments
    ----------
    - GRIDS [`input grid collection`] : Grid Collection
    - FILTERED [`output grid collection`] : Filtered
    - GRID_SYSTEM [`grid system`] : Grid System
    - SIGMA [`floating point number`] : Standard Deviation. Default: 50.000000
    - KERNEL_RADIUS [`integer number`] : Kernel Radius. Default: 2

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grids_tools', 'filter_gaussian', 'Gaussian Filter')
    if Tool.is_Okay():
        Tool.Set_Input ('GRIDS', GRIDS)
        Tool.Set_Output('FILTERED', FILTERED)
        Tool.Set_Option('GRID_SYSTEM', GRID_SYSTEM)
        Tool.Set_Option('SIGMA', SIGMA)
        Tool.Set_Option('KERNEL_RADIUS', KERNEL_RADIUS)
        return Tool.Execute(Verbose)
    return False

def Laplacian_Filter(GRIDS=None, FILTERED=None, GRID_SYSTEM=None, METHOD=None, SIGMA=None, KERNEL_RADIUS=None, Verbose=2):
    '''
    Laplacian Filter
    ----------
    [grids_tools.filter_laplacian]\n
    Laplacian filter for grid collections.\n
    Arguments
    ----------
    - GRIDS [`input grid collection`] : Grid Collection
    - FILTERED [`output grid collection`] : Filtered
    - GRID_SYSTEM [`grid system`] : Grid System
    - METHOD [`choice`] : Method. Available Choices: [0] standard kernel 1 [1] standard kernel 2 [2] standard kernel 3 [3] user defined kernel Default: 0
    - SIGMA [`floating point number`] : Standard Deviation. Default: 50.000000
    - KERNEL_RADIUS [`integer number`] : Kernel Radius. Default: 2

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grids_tools', 'filter_laplacian', 'Laplacian Filter')
    if Tool.is_Okay():
        Tool.Set_Input ('GRIDS', GRIDS)
        Tool.Set_Output('FILTERED', FILTERED)
        Tool.Set_Option('GRID_SYSTEM', GRID_SYSTEM)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('SIGMA', SIGMA)
        Tool.Set_Option('KERNEL_RADIUS', KERNEL_RADIUS)
        return Tool.Execute(Verbose)
    return False

def run_tool_grids_tools_filter_laplacian(GRIDS=None, FILTERED=None, GRID_SYSTEM=None, METHOD=None, SIGMA=None, KERNEL_RADIUS=None, Verbose=2):
    '''
    Laplacian Filter
    ----------
    [grids_tools.filter_laplacian]\n
    Laplacian filter for grid collections.\n
    Arguments
    ----------
    - GRIDS [`input grid collection`] : Grid Collection
    - FILTERED [`output grid collection`] : Filtered
    - GRID_SYSTEM [`grid system`] : Grid System
    - METHOD [`choice`] : Method. Available Choices: [0] standard kernel 1 [1] standard kernel 2 [2] standard kernel 3 [3] user defined kernel Default: 0
    - SIGMA [`floating point number`] : Standard Deviation. Default: 50.000000
    - KERNEL_RADIUS [`integer number`] : Kernel Radius. Default: 2

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grids_tools', 'filter_laplacian', 'Laplacian Filter')
    if Tool.is_Okay():
        Tool.Set_Input ('GRIDS', GRIDS)
        Tool.Set_Output('FILTERED', FILTERED)
        Tool.Set_Option('GRID_SYSTEM', GRID_SYSTEM)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('SIGMA', SIGMA)
        Tool.Set_Option('KERNEL_RADIUS', KERNEL_RADIUS)
        return Tool.Execute(Verbose)
    return False

def Rank_Filter(GRIDS=None, FILTERED=None, GRID_SYSTEM=None, RANK=None, KERNEL_TYPE=None, KERNEL_RADIUS=None, Verbose=2):
    '''
    Rank Filter
    ----------
    [grids_tools.filter_rank]\n
    Rank filter for grid collections. Set rank to fifty percent to apply a median filter.\n
    Arguments
    ----------
    - GRIDS [`input grid collection`] : Grid Collection
    - FILTERED [`output grid collection`] : Filtered
    - GRID_SYSTEM [`grid system`] : Grid System
    - RANK [`floating point number`] : Rank. Default: 50.000000
    - KERNEL_TYPE [`choice`] : Kernel Type. Available Choices: [0] Square [1] Circle Default: 0
    - KERNEL_RADIUS [`integer number`] : Kernel Radius. Default: 2

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grids_tools', 'filter_rank', 'Rank Filter')
    if Tool.is_Okay():
        Tool.Set_Input ('GRIDS', GRIDS)
        Tool.Set_Output('FILTERED', FILTERED)
        Tool.Set_Option('GRID_SYSTEM', GRID_SYSTEM)
        Tool.Set_Option('RANK', RANK)
        Tool.Set_Option('KERNEL_TYPE', KERNEL_TYPE)
        Tool.Set_Option('KERNEL_RADIUS', KERNEL_RADIUS)
        return Tool.Execute(Verbose)
    return False

def run_tool_grids_tools_filter_rank(GRIDS=None, FILTERED=None, GRID_SYSTEM=None, RANK=None, KERNEL_TYPE=None, KERNEL_RADIUS=None, Verbose=2):
    '''
    Rank Filter
    ----------
    [grids_tools.filter_rank]\n
    Rank filter for grid collections. Set rank to fifty percent to apply a median filter.\n
    Arguments
    ----------
    - GRIDS [`input grid collection`] : Grid Collection
    - FILTERED [`output grid collection`] : Filtered
    - GRID_SYSTEM [`grid system`] : Grid System
    - RANK [`floating point number`] : Rank. Default: 50.000000
    - KERNEL_TYPE [`choice`] : Kernel Type. Available Choices: [0] Square [1] Circle Default: 0
    - KERNEL_RADIUS [`integer number`] : Kernel Radius. Default: 2

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grids_tools', 'filter_rank', 'Rank Filter')
    if Tool.is_Okay():
        Tool.Set_Input ('GRIDS', GRIDS)
        Tool.Set_Output('FILTERED', FILTERED)
        Tool.Set_Option('GRID_SYSTEM', GRID_SYSTEM)
        Tool.Set_Option('RANK', RANK)
        Tool.Set_Option('KERNEL_TYPE', KERNEL_TYPE)
        Tool.Set_Option('KERNEL_RADIUS', KERNEL_RADIUS)
        return Tool.Execute(Verbose)
    return False

def Simple_Filter(INPUT=None, OUTPUT=None, OUTPUT_FLOAT=None, GRID_SYSTEM=None, KEEP_DEPTH=None, METHOD=None, KERNEL_TYPE=None, KERNEL_RADIUS=None, Verbose=2):
    '''
    Simple Filter
    ----------
    [grids_tools.filter_simple]\n
    Simple standard filters for grid collections.\n
    Arguments
    ----------
    - INPUT [`input grid collection`] : Grid Collection
    - OUTPUT [`output grid collection`] : Filtered. If not set input will be replaced by output.
    - OUTPUT_FLOAT [`output grid collection`] : Filtered
    - GRID_SYSTEM [`grid system`] : Grid System
    - KEEP_DEPTH [`boolean`] : Keep Input Data Depth. Default: 0 Output will have same data depth as input.
    - METHOD [`choice`] : Filter. Available Choices: [0] Smooth [1] Sharpen [2] Edge Default: 0
    - KERNEL_TYPE [`choice`] : Kernel Type. Available Choices: [0] Square [1] Circle Default: 0
    - KERNEL_RADIUS [`integer number`] : Kernel Radius. Default: 2

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grids_tools', 'filter_simple', 'Simple Filter')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Output('OUTPUT', OUTPUT)
        Tool.Set_Output('OUTPUT_FLOAT', OUTPUT_FLOAT)
        Tool.Set_Option('GRID_SYSTEM', GRID_SYSTEM)
        Tool.Set_Option('KEEP_DEPTH', KEEP_DEPTH)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('KERNEL_TYPE', KERNEL_TYPE)
        Tool.Set_Option('KERNEL_RADIUS', KERNEL_RADIUS)
        return Tool.Execute(Verbose)
    return False

def run_tool_grids_tools_filter_simple(INPUT=None, OUTPUT=None, OUTPUT_FLOAT=None, GRID_SYSTEM=None, KEEP_DEPTH=None, METHOD=None, KERNEL_TYPE=None, KERNEL_RADIUS=None, Verbose=2):
    '''
    Simple Filter
    ----------
    [grids_tools.filter_simple]\n
    Simple standard filters for grid collections.\n
    Arguments
    ----------
    - INPUT [`input grid collection`] : Grid Collection
    - OUTPUT [`output grid collection`] : Filtered. If not set input will be replaced by output.
    - OUTPUT_FLOAT [`output grid collection`] : Filtered
    - GRID_SYSTEM [`grid system`] : Grid System
    - KEEP_DEPTH [`boolean`] : Keep Input Data Depth. Default: 0 Output will have same data depth as input.
    - METHOD [`choice`] : Filter. Available Choices: [0] Smooth [1] Sharpen [2] Edge Default: 0
    - KERNEL_TYPE [`choice`] : Kernel Type. Available Choices: [0] Square [1] Circle Default: 0
    - KERNEL_RADIUS [`integer number`] : Kernel Radius. Default: 2

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grids_tools', 'filter_simple', 'Simple Filter')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Output('OUTPUT', OUTPUT)
        Tool.Set_Output('OUTPUT_FLOAT', OUTPUT_FLOAT)
        Tool.Set_Option('GRID_SYSTEM', GRID_SYSTEM)
        Tool.Set_Option('KEEP_DEPTH', KEEP_DEPTH)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('KERNEL_TYPE', KERNEL_TYPE)
        Tool.Set_Option('KERNEL_RADIUS', KERNEL_RADIUS)
        return Tool.Execute(Verbose)
    return False

def Change_Longitudinal_Range_for_a_Grid_Collection(GRIDS=None, CHANGED=None, GRIDS_GRIDSYSTEM=None, DIRECTION=None, Verbose=2):
    '''
    Change Longitudinal Range for a Grid Collection
    ----------
    [grids_tools.longitudinal_range]\n
    Change the longitudinal range of grid collections using geographic coordinates, i.e. from 0 - 360 to -180 - 180 and vice versa.\n
    Arguments
    ----------
    - GRIDS [`input grid collection`] : Grid Collection
    - CHANGED [`output data object`] : Changed
    - GRIDS_GRIDSYSTEM [`grid system`] : Grid system
    - DIRECTION [`choice`] : Direction. Available Choices: [0] 0 - 360 >> -180 - 180 [1] -180 - 180 >> 0 - 360 Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grids_tools', 'longitudinal_range', 'Change Longitudinal Range for a Grid Collection')
    if Tool.is_Okay():
        Tool.Set_Input ('GRIDS', GRIDS)
        Tool.Set_Output('CHANGED', CHANGED)
        Tool.Set_Option('GRIDS_GRIDSYSTEM', GRIDS_GRIDSYSTEM)
        Tool.Set_Option('DIRECTION', DIRECTION)
        return Tool.Execute(Verbose)
    return False

def run_tool_grids_tools_longitudinal_range(GRIDS=None, CHANGED=None, GRIDS_GRIDSYSTEM=None, DIRECTION=None, Verbose=2):
    '''
    Change Longitudinal Range for a Grid Collection
    ----------
    [grids_tools.longitudinal_range]\n
    Change the longitudinal range of grid collections using geographic coordinates, i.e. from 0 - 360 to -180 - 180 and vice versa.\n
    Arguments
    ----------
    - GRIDS [`input grid collection`] : Grid Collection
    - CHANGED [`output data object`] : Changed
    - GRIDS_GRIDSYSTEM [`grid system`] : Grid system
    - DIRECTION [`choice`] : Direction. Available Choices: [0] 0 - 360 >> -180 - 180 [1] -180 - 180 >> 0 - 360 Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grids_tools', 'longitudinal_range', 'Change Longitudinal Range for a Grid Collection')
    if Tool.is_Okay():
        Tool.Set_Input ('GRIDS', GRIDS)
        Tool.Set_Output('CHANGED', CHANGED)
        Tool.Set_Option('GRIDS_GRIDSYSTEM', GRIDS_GRIDSYSTEM)
        Tool.Set_Option('DIRECTION', DIRECTION)
        return Tool.Execute(Verbose)
    return False

