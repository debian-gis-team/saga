#! /usr/bin/env python

'''
Python Interface to SAGA Tools Library
----------
- Category : Imagery
- Name     : OpenCV
- ID       : imagery_opencv

Description
----------
OpenCV - "Open Source Computer Vision Library"
Version: 4.3.0
[OpenCV homepage](http://opencv.org)
'''

from PySAGA.helper import Tool_Wrapper

def Morphological_Filter_OpenCV(INPUT=None, OUTPUT=None, TYPE=None, SHAPE=None, RADIUS=None, ITERATIONS=None, Verbose=2):
    '''
    Morphological Filter (OpenCV)
    ----------
    [imagery_opencv.0]\n
    Morphological Filter.\n
    Arguments
    ----------
    - INPUT [`input grid`] : Input
    - OUTPUT [`output grid`] : Output
    - TYPE [`choice`] : Operation. Available Choices: [0] dilation [1] erosion [2] opening [3] closing [4] morpological gradient [5] top hat [6] black hat Default: 0
    - SHAPE [`choice`] : Element Shape. Available Choices: [0] ellipse [1] rectangle [2] cross Default: 0
    - RADIUS [`integer number`] : Radius (cells). Minimum: 0 Default: 1
    - ITERATIONS [`integer number`] : Iterations. Minimum: 1 Default: 1

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('imagery_opencv', '0', 'Morphological Filter (OpenCV)')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Output('OUTPUT', OUTPUT)
        Tool.Set_Option('TYPE', TYPE)
        Tool.Set_Option('SHAPE', SHAPE)
        Tool.Set_Option('RADIUS', RADIUS)
        Tool.Set_Option('ITERATIONS', ITERATIONS)
        return Tool.Execute(Verbose)
    return False

def run_tool_imagery_opencv_0(INPUT=None, OUTPUT=None, TYPE=None, SHAPE=None, RADIUS=None, ITERATIONS=None, Verbose=2):
    '''
    Morphological Filter (OpenCV)
    ----------
    [imagery_opencv.0]\n
    Morphological Filter.\n
    Arguments
    ----------
    - INPUT [`input grid`] : Input
    - OUTPUT [`output grid`] : Output
    - TYPE [`choice`] : Operation. Available Choices: [0] dilation [1] erosion [2] opening [3] closing [4] morpological gradient [5] top hat [6] black hat Default: 0
    - SHAPE [`choice`] : Element Shape. Available Choices: [0] ellipse [1] rectangle [2] cross Default: 0
    - RADIUS [`integer number`] : Radius (cells). Minimum: 0 Default: 1
    - ITERATIONS [`integer number`] : Iterations. Minimum: 1 Default: 1

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('imagery_opencv', '0', 'Morphological Filter (OpenCV)')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Output('OUTPUT', OUTPUT)
        Tool.Set_Option('TYPE', TYPE)
        Tool.Set_Option('SHAPE', SHAPE)
        Tool.Set_Option('RADIUS', RADIUS)
        Tool.Set_Option('ITERATIONS', ITERATIONS)
        return Tool.Execute(Verbose)
    return False

def Fourier_Transformation(GRID=None, DFT=None, DFT_OPT=None, CENTERED=None, SIZE=None, Verbose=2):
    '''
    Fourier Transformation
    ----------
    [imagery_opencv.1]\n
    Discrete Fourier transformation.\n
    Arguments
    ----------
    - GRID [`input grid`] : Grid
    - DFT [`output grid collection`] : Fourier Transformation
    - DFT_OPT [`output data object`] : Fourier Transformation
    - CENTERED [`boolean`] : Centered. Default: 1
    - SIZE [`choice`] : Output Size. Available Choices: [0] same as input grid [1] optimal DFT size Default: 0 The optimal size for the discrete Fourier transformation might differ from that of the input grid.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('imagery_opencv', '1', 'Fourier Transformation')
    if Tool.is_Okay():
        Tool.Set_Input ('GRID', GRID)
        Tool.Set_Output('DFT', DFT)
        Tool.Set_Output('DFT_OPT', DFT_OPT)
        Tool.Set_Option('CENTERED', CENTERED)
        Tool.Set_Option('SIZE', SIZE)
        return Tool.Execute(Verbose)
    return False

def run_tool_imagery_opencv_1(GRID=None, DFT=None, DFT_OPT=None, CENTERED=None, SIZE=None, Verbose=2):
    '''
    Fourier Transformation
    ----------
    [imagery_opencv.1]\n
    Discrete Fourier transformation.\n
    Arguments
    ----------
    - GRID [`input grid`] : Grid
    - DFT [`output grid collection`] : Fourier Transformation
    - DFT_OPT [`output data object`] : Fourier Transformation
    - CENTERED [`boolean`] : Centered. Default: 1
    - SIZE [`choice`] : Output Size. Available Choices: [0] same as input grid [1] optimal DFT size Default: 0 The optimal size for the discrete Fourier transformation might differ from that of the input grid.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('imagery_opencv', '1', 'Fourier Transformation')
    if Tool.is_Okay():
        Tool.Set_Input ('GRID', GRID)
        Tool.Set_Output('DFT', DFT)
        Tool.Set_Output('DFT_OPT', DFT_OPT)
        Tool.Set_Option('CENTERED', CENTERED)
        Tool.Set_Option('SIZE', SIZE)
        return Tool.Execute(Verbose)
    return False

def Single_Value_Decomposition_OpenCV(INPUT=None, OUTPUT=None, RANGE=None, Verbose=2):
    '''
    Single Value Decomposition (OpenCV)
    ----------
    [imagery_opencv.2]\n
    Single Value Decomposition.\n
    Arguments
    ----------
    - INPUT [`input grid`] : Input
    - OUTPUT [`output grid`] : Output
    - RANGE [`value range`] : Range

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('imagery_opencv', '2', 'Single Value Decomposition (OpenCV)')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Output('OUTPUT', OUTPUT)
        Tool.Set_Option('RANGE', RANGE)
        return Tool.Execute(Verbose)
    return False

def run_tool_imagery_opencv_2(INPUT=None, OUTPUT=None, RANGE=None, Verbose=2):
    '''
    Single Value Decomposition (OpenCV)
    ----------
    [imagery_opencv.2]\n
    Single Value Decomposition.\n
    Arguments
    ----------
    - INPUT [`input grid`] : Input
    - OUTPUT [`output grid`] : Output
    - RANGE [`value range`] : Range

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('imagery_opencv', '2', 'Single Value Decomposition (OpenCV)')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Output('OUTPUT', OUTPUT)
        Tool.Set_Option('RANGE', RANGE)
        return Tool.Execute(Verbose)
    return False

def Stereo_Match_OpenCV(LEFT=None, RIGHT=None, DISPARITY=None, POINTS=None, ALGORITHM=None, DISP_MIN=None, DISP_NUM=None, BLOCKSIZE=None, DIFF_MAX=None, UNIQUENESS=None, SPECKLE_SIZE=None, SPECKLE_RANGE=None, BM_TEXTURE=None, BM_FILTER_CAP=None, SGBM_MODE=None, SGBM_P1=None, SGBM_P2=None, SGBM_FILTER_CAP=None, Verbose=2):
    '''
    Stereo Match (OpenCV)
    ----------
    [imagery_opencv.4]\n
    Stereo Match.\n
    Arguments
    ----------
    - LEFT [`input grid`] : Left Image
    - RIGHT [`input grid`] : Right Image
    - DISPARITY [`output grid`] : Disparity Image
    - POINTS [`output point cloud`] : Points
    - ALGORITHM [`choice`] : Algorithm. Available Choices: [0] Block Matching [1] Modified Hirschmuller Default: 0
    - DISP_MIN [`integer number`] : Minimum Disparity. Minimum: 0 Default: 0 Minimum possible disparity value. Normally, it is zero but sometimes rectification algorithms can shift images, so this parameter needs to be adjusted accordingly.
    - DISP_NUM [`integer number`] : Number of Disparities. Minimum: 1 Default: 1 Maximum disparity minus minimum disparity. The value is always greater than zero.
    - BLOCKSIZE [`integer number`] : Block Size. Minimum: 0 Default: 4 The linear size of the blocks compared by the algorithm. Larger block size implies smoother, though less accurate disparity map. Smaller block size gives more detailed disparity map, but there is higher chance for algorithm to find a wrong correspondence.
    - DIFF_MAX [`integer number`] : Maximum Disparity Difference. Minimum: -1 Default: 1 Maximum allowed difference (in integer pixel units) in the left-right disparity check. Set it to a non-positive value to disable the check.
    - UNIQUENESS [`integer number`] : Uniqueness Ratio. Minimum: 0 Default: 15 Margin in percentage by which the best (minimum) computed cost function value should "win" the second best value to consider the found match correct. Normally, a value within the 5-15 range is good enough.
    - SPECKLE_SIZE [`integer number`] : Speckle Window Size. Minimum: 0 Default: 100 Maximum size of smooth disparity regions to consider their noise speckles and invalidate. Set it to 0 to disable speckle filtering. Otherwise, set it somewhere in the 50-200 range.
    - SPECKLE_RANGE [`integer number`] : Speckle Range. Minimum: 0 Default: 2 Maximum disparity variation within each connected component. If you do speckle filtering, set the parameter to a positive value, it will be implicitly multiplied by 16. Normally, 1 or 2 is good enough.
    - BM_TEXTURE [`integer number`] : Texture Threshold. Minimum: 0 Default: 31
    - BM_FILTER_CAP [`integer number`] : Prefilter Truncation Value. Minimum: 0 Default: 31 Truncation value for the prefiltered image pixels.
    - SGBM_MODE [`choice`] : Mode. Available Choices: [0] Semi-Global Block Matching [1] HH [2] SGBM 3 Way Default: 0
    - SGBM_P1 [`integer number`] : Disparity Smoothness Parameter 1. Minimum: 0 Default: 8 The larger the value, the smoother the disparity. Parameter 1 is the penalty on the disparity change by plus or minus 1 between neighbor pixels. The algorithm requires Parameter 2 > Parameter 1.
    - SGBM_P2 [`integer number`] : Disparity Smoothness Parameter 2. Minimum: 0 Default: 32 The larger the value, the smoother the disparity. Parameter 2 is the penalty on the disparity change by more than 1 between neighbor pixels. The algorithm requires Parameter 2 > Parameter 1.
    - SGBM_FILTER_CAP [`integer number`] : Prefilter Truncation Value. Minimum: 0 Default: 31 Truncation value for the prefiltered image pixels. The algorithm first computes x-derivative at each pixel and clips its value by [-preFilterCap, preFilterCap] interval. The result values are passed to the Birchfield-Tomasi pixel cost function.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('imagery_opencv', '4', 'Stereo Match (OpenCV)')
    if Tool.is_Okay():
        Tool.Set_Input ('LEFT', LEFT)
        Tool.Set_Input ('RIGHT', RIGHT)
        Tool.Set_Output('DISPARITY', DISPARITY)
        Tool.Set_Output('POINTS', POINTS)
        Tool.Set_Option('ALGORITHM', ALGORITHM)
        Tool.Set_Option('DISP_MIN', DISP_MIN)
        Tool.Set_Option('DISP_NUM', DISP_NUM)
        Tool.Set_Option('BLOCKSIZE', BLOCKSIZE)
        Tool.Set_Option('DIFF_MAX', DIFF_MAX)
        Tool.Set_Option('UNIQUENESS', UNIQUENESS)
        Tool.Set_Option('SPECKLE_SIZE', SPECKLE_SIZE)
        Tool.Set_Option('SPECKLE_RANGE', SPECKLE_RANGE)
        Tool.Set_Option('BM_TEXTURE', BM_TEXTURE)
        Tool.Set_Option('BM_FILTER_CAP', BM_FILTER_CAP)
        Tool.Set_Option('SGBM_MODE', SGBM_MODE)
        Tool.Set_Option('SGBM_P1', SGBM_P1)
        Tool.Set_Option('SGBM_P2', SGBM_P2)
        Tool.Set_Option('SGBM_FILTER_CAP', SGBM_FILTER_CAP)
        return Tool.Execute(Verbose)
    return False

def run_tool_imagery_opencv_4(LEFT=None, RIGHT=None, DISPARITY=None, POINTS=None, ALGORITHM=None, DISP_MIN=None, DISP_NUM=None, BLOCKSIZE=None, DIFF_MAX=None, UNIQUENESS=None, SPECKLE_SIZE=None, SPECKLE_RANGE=None, BM_TEXTURE=None, BM_FILTER_CAP=None, SGBM_MODE=None, SGBM_P1=None, SGBM_P2=None, SGBM_FILTER_CAP=None, Verbose=2):
    '''
    Stereo Match (OpenCV)
    ----------
    [imagery_opencv.4]\n
    Stereo Match.\n
    Arguments
    ----------
    - LEFT [`input grid`] : Left Image
    - RIGHT [`input grid`] : Right Image
    - DISPARITY [`output grid`] : Disparity Image
    - POINTS [`output point cloud`] : Points
    - ALGORITHM [`choice`] : Algorithm. Available Choices: [0] Block Matching [1] Modified Hirschmuller Default: 0
    - DISP_MIN [`integer number`] : Minimum Disparity. Minimum: 0 Default: 0 Minimum possible disparity value. Normally, it is zero but sometimes rectification algorithms can shift images, so this parameter needs to be adjusted accordingly.
    - DISP_NUM [`integer number`] : Number of Disparities. Minimum: 1 Default: 1 Maximum disparity minus minimum disparity. The value is always greater than zero.
    - BLOCKSIZE [`integer number`] : Block Size. Minimum: 0 Default: 4 The linear size of the blocks compared by the algorithm. Larger block size implies smoother, though less accurate disparity map. Smaller block size gives more detailed disparity map, but there is higher chance for algorithm to find a wrong correspondence.
    - DIFF_MAX [`integer number`] : Maximum Disparity Difference. Minimum: -1 Default: 1 Maximum allowed difference (in integer pixel units) in the left-right disparity check. Set it to a non-positive value to disable the check.
    - UNIQUENESS [`integer number`] : Uniqueness Ratio. Minimum: 0 Default: 15 Margin in percentage by which the best (minimum) computed cost function value should "win" the second best value to consider the found match correct. Normally, a value within the 5-15 range is good enough.
    - SPECKLE_SIZE [`integer number`] : Speckle Window Size. Minimum: 0 Default: 100 Maximum size of smooth disparity regions to consider their noise speckles and invalidate. Set it to 0 to disable speckle filtering. Otherwise, set it somewhere in the 50-200 range.
    - SPECKLE_RANGE [`integer number`] : Speckle Range. Minimum: 0 Default: 2 Maximum disparity variation within each connected component. If you do speckle filtering, set the parameter to a positive value, it will be implicitly multiplied by 16. Normally, 1 or 2 is good enough.
    - BM_TEXTURE [`integer number`] : Texture Threshold. Minimum: 0 Default: 31
    - BM_FILTER_CAP [`integer number`] : Prefilter Truncation Value. Minimum: 0 Default: 31 Truncation value for the prefiltered image pixels.
    - SGBM_MODE [`choice`] : Mode. Available Choices: [0] Semi-Global Block Matching [1] HH [2] SGBM 3 Way Default: 0
    - SGBM_P1 [`integer number`] : Disparity Smoothness Parameter 1. Minimum: 0 Default: 8 The larger the value, the smoother the disparity. Parameter 1 is the penalty on the disparity change by plus or minus 1 between neighbor pixels. The algorithm requires Parameter 2 > Parameter 1.
    - SGBM_P2 [`integer number`] : Disparity Smoothness Parameter 2. Minimum: 0 Default: 32 The larger the value, the smoother the disparity. Parameter 2 is the penalty on the disparity change by more than 1 between neighbor pixels. The algorithm requires Parameter 2 > Parameter 1.
    - SGBM_FILTER_CAP [`integer number`] : Prefilter Truncation Value. Minimum: 0 Default: 31 Truncation value for the prefiltered image pixels. The algorithm first computes x-derivative at each pixel and clips its value by [-preFilterCap, preFilterCap] interval. The result values are passed to the Birchfield-Tomasi pixel cost function.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('imagery_opencv', '4', 'Stereo Match (OpenCV)')
    if Tool.is_Okay():
        Tool.Set_Input ('LEFT', LEFT)
        Tool.Set_Input ('RIGHT', RIGHT)
        Tool.Set_Output('DISPARITY', DISPARITY)
        Tool.Set_Output('POINTS', POINTS)
        Tool.Set_Option('ALGORITHM', ALGORITHM)
        Tool.Set_Option('DISP_MIN', DISP_MIN)
        Tool.Set_Option('DISP_NUM', DISP_NUM)
        Tool.Set_Option('BLOCKSIZE', BLOCKSIZE)
        Tool.Set_Option('DIFF_MAX', DIFF_MAX)
        Tool.Set_Option('UNIQUENESS', UNIQUENESS)
        Tool.Set_Option('SPECKLE_SIZE', SPECKLE_SIZE)
        Tool.Set_Option('SPECKLE_RANGE', SPECKLE_RANGE)
        Tool.Set_Option('BM_TEXTURE', BM_TEXTURE)
        Tool.Set_Option('BM_FILTER_CAP', BM_FILTER_CAP)
        Tool.Set_Option('SGBM_MODE', SGBM_MODE)
        Tool.Set_Option('SGBM_P1', SGBM_P1)
        Tool.Set_Option('SGBM_P2', SGBM_P2)
        Tool.Set_Option('SGBM_FILTER_CAP', SGBM_FILTER_CAP)
        return Tool.Execute(Verbose)
    return False

def Normal_Bayes_Classification(FEATURES=None, TRAIN_SAMPLES=None, TRAIN_AREAS=None, CLASSES=None, PROBABILITY=None, CLASSES_LUT=None, NORMALIZE=None, GRID_SYSTEM=None, MODEL_TRAIN=None, TRAIN_CLASS=None, TRAIN_BUFFER=None, MODEL_LOAD=None, MODEL_SAVE=None, Verbose=2):
    '''
    Normal Bayes Classification
    ----------
    [imagery_opencv.5]\n
    Integration of the OpenCV Machine Learning library for Normal Bayes classification of gridded features.\n
    Arguments
    ----------
    - FEATURES [`input grid list`] : Features
    - TRAIN_SAMPLES [`input table`] : Training Samples. Provide a class identifier in the first field followed by sample data corresponding to the input feature grids.
    - TRAIN_AREAS [`input shapes`] : Training Areas
    - CLASSES [`output grid`] : Classification
    - PROBABILITY [`output grid`] : Probability
    - CLASSES_LUT [`output table`] : Look-up Table. A reference list of the grid values that have been assigned to the training classes.
    - NORMALIZE [`boolean`] : Normalize. Default: 0
    - GRID_SYSTEM [`grid system`] : Grid System
    - MODEL_TRAIN [`choice`] : Training. Available Choices: [0] training areas [1] training samples [2] load from file Default: 0
    - TRAIN_CLASS [`table field`] : Class Identifier
    - TRAIN_BUFFER [`floating point number`] : Buffer Size. Minimum: 0.000000 Default: 1.000000 For non-polygon type training areas, creates a buffer with a diameter of specified size.
    - MODEL_LOAD [`file path`] : Load Model. Use a model previously stored to file.
    - MODEL_SAVE [`file path`] : Save Model. Stores model to file to be used for subsequent classifications instead of training areas.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('imagery_opencv', '5', 'Normal Bayes Classification')
    if Tool.is_Okay():
        Tool.Set_Input ('FEATURES', FEATURES)
        Tool.Set_Input ('TRAIN_SAMPLES', TRAIN_SAMPLES)
        Tool.Set_Input ('TRAIN_AREAS', TRAIN_AREAS)
        Tool.Set_Output('CLASSES', CLASSES)
        Tool.Set_Output('PROBABILITY', PROBABILITY)
        Tool.Set_Output('CLASSES_LUT', CLASSES_LUT)
        Tool.Set_Option('NORMALIZE', NORMALIZE)
        Tool.Set_Option('GRID_SYSTEM', GRID_SYSTEM)
        Tool.Set_Option('MODEL_TRAIN', MODEL_TRAIN)
        Tool.Set_Option('TRAIN_CLASS', TRAIN_CLASS)
        Tool.Set_Option('TRAIN_BUFFER', TRAIN_BUFFER)
        Tool.Set_Option('MODEL_LOAD', MODEL_LOAD)
        Tool.Set_Option('MODEL_SAVE', MODEL_SAVE)
        return Tool.Execute(Verbose)
    return False

def run_tool_imagery_opencv_5(FEATURES=None, TRAIN_SAMPLES=None, TRAIN_AREAS=None, CLASSES=None, PROBABILITY=None, CLASSES_LUT=None, NORMALIZE=None, GRID_SYSTEM=None, MODEL_TRAIN=None, TRAIN_CLASS=None, TRAIN_BUFFER=None, MODEL_LOAD=None, MODEL_SAVE=None, Verbose=2):
    '''
    Normal Bayes Classification
    ----------
    [imagery_opencv.5]\n
    Integration of the OpenCV Machine Learning library for Normal Bayes classification of gridded features.\n
    Arguments
    ----------
    - FEATURES [`input grid list`] : Features
    - TRAIN_SAMPLES [`input table`] : Training Samples. Provide a class identifier in the first field followed by sample data corresponding to the input feature grids.
    - TRAIN_AREAS [`input shapes`] : Training Areas
    - CLASSES [`output grid`] : Classification
    - PROBABILITY [`output grid`] : Probability
    - CLASSES_LUT [`output table`] : Look-up Table. A reference list of the grid values that have been assigned to the training classes.
    - NORMALIZE [`boolean`] : Normalize. Default: 0
    - GRID_SYSTEM [`grid system`] : Grid System
    - MODEL_TRAIN [`choice`] : Training. Available Choices: [0] training areas [1] training samples [2] load from file Default: 0
    - TRAIN_CLASS [`table field`] : Class Identifier
    - TRAIN_BUFFER [`floating point number`] : Buffer Size. Minimum: 0.000000 Default: 1.000000 For non-polygon type training areas, creates a buffer with a diameter of specified size.
    - MODEL_LOAD [`file path`] : Load Model. Use a model previously stored to file.
    - MODEL_SAVE [`file path`] : Save Model. Stores model to file to be used for subsequent classifications instead of training areas.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('imagery_opencv', '5', 'Normal Bayes Classification')
    if Tool.is_Okay():
        Tool.Set_Input ('FEATURES', FEATURES)
        Tool.Set_Input ('TRAIN_SAMPLES', TRAIN_SAMPLES)
        Tool.Set_Input ('TRAIN_AREAS', TRAIN_AREAS)
        Tool.Set_Output('CLASSES', CLASSES)
        Tool.Set_Output('PROBABILITY', PROBABILITY)
        Tool.Set_Output('CLASSES_LUT', CLASSES_LUT)
        Tool.Set_Option('NORMALIZE', NORMALIZE)
        Tool.Set_Option('GRID_SYSTEM', GRID_SYSTEM)
        Tool.Set_Option('MODEL_TRAIN', MODEL_TRAIN)
        Tool.Set_Option('TRAIN_CLASS', TRAIN_CLASS)
        Tool.Set_Option('TRAIN_BUFFER', TRAIN_BUFFER)
        Tool.Set_Option('MODEL_LOAD', MODEL_LOAD)
        Tool.Set_Option('MODEL_SAVE', MODEL_SAVE)
        return Tool.Execute(Verbose)
    return False

def KNearest_Neighbours_Classification(FEATURES=None, TRAIN_SAMPLES=None, TRAIN_AREAS=None, CLASSES=None, CLASSES_LUT=None, NORMALIZE=None, GRID_SYSTEM=None, MODEL_TRAIN=None, TRAIN_CLASS=None, TRAIN_BUFFER=None, MODEL_LOAD=None, MODEL_SAVE=None, NEIGHBOURS=None, TRAINING=None, ALGORITHM=None, EMAX=None, Verbose=2):
    '''
    K-Nearest Neighbours Classification
    ----------
    [imagery_opencv.6]\n
    Integration of the OpenCV Machine Learning library for K-Nearest Neighbours classification of gridded features.\n
    Arguments
    ----------
    - FEATURES [`input grid list`] : Features
    - TRAIN_SAMPLES [`input table`] : Training Samples. Provide a class identifier in the first field followed by sample data corresponding to the input feature grids.
    - TRAIN_AREAS [`input shapes`] : Training Areas
    - CLASSES [`output grid`] : Classification
    - CLASSES_LUT [`output table`] : Look-up Table. A reference list of the grid values that have been assigned to the training classes.
    - NORMALIZE [`boolean`] : Normalize. Default: 0
    - GRID_SYSTEM [`grid system`] : Grid System
    - MODEL_TRAIN [`choice`] : Training. Available Choices: [0] training areas [1] training samples [2] load from file Default: 0
    - TRAIN_CLASS [`table field`] : Class Identifier
    - TRAIN_BUFFER [`floating point number`] : Buffer Size. Minimum: 0.000000 Default: 1.000000 For non-polygon type training areas, creates a buffer with a diameter of specified size.
    - MODEL_LOAD [`file path`] : Load Model. Use a model previously stored to file.
    - MODEL_SAVE [`file path`] : Save Model. Stores model to file to be used for subsequent classifications instead of training areas.
    - NEIGHBOURS [`integer number`] : Default Number of Neighbours. Minimum: 1 Default: 3
    - TRAINING [`choice`] : Training Method. Available Choices: [0] classification [1] regression model Default: 0
    - ALGORITHM [`choice`] : Algorithm Type. Available Choices: [0] brute force [1] KD Tree Default: 0
    - EMAX [`integer number`] : Parameter for KD Tree implementation. Minimum: 1 Default: 1000

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('imagery_opencv', '6', 'K-Nearest Neighbours Classification')
    if Tool.is_Okay():
        Tool.Set_Input ('FEATURES', FEATURES)
        Tool.Set_Input ('TRAIN_SAMPLES', TRAIN_SAMPLES)
        Tool.Set_Input ('TRAIN_AREAS', TRAIN_AREAS)
        Tool.Set_Output('CLASSES', CLASSES)
        Tool.Set_Output('CLASSES_LUT', CLASSES_LUT)
        Tool.Set_Option('NORMALIZE', NORMALIZE)
        Tool.Set_Option('GRID_SYSTEM', GRID_SYSTEM)
        Tool.Set_Option('MODEL_TRAIN', MODEL_TRAIN)
        Tool.Set_Option('TRAIN_CLASS', TRAIN_CLASS)
        Tool.Set_Option('TRAIN_BUFFER', TRAIN_BUFFER)
        Tool.Set_Option('MODEL_LOAD', MODEL_LOAD)
        Tool.Set_Option('MODEL_SAVE', MODEL_SAVE)
        Tool.Set_Option('NEIGHBOURS', NEIGHBOURS)
        Tool.Set_Option('TRAINING', TRAINING)
        Tool.Set_Option('ALGORITHM', ALGORITHM)
        Tool.Set_Option('EMAX', EMAX)
        return Tool.Execute(Verbose)
    return False

def run_tool_imagery_opencv_6(FEATURES=None, TRAIN_SAMPLES=None, TRAIN_AREAS=None, CLASSES=None, CLASSES_LUT=None, NORMALIZE=None, GRID_SYSTEM=None, MODEL_TRAIN=None, TRAIN_CLASS=None, TRAIN_BUFFER=None, MODEL_LOAD=None, MODEL_SAVE=None, NEIGHBOURS=None, TRAINING=None, ALGORITHM=None, EMAX=None, Verbose=2):
    '''
    K-Nearest Neighbours Classification
    ----------
    [imagery_opencv.6]\n
    Integration of the OpenCV Machine Learning library for K-Nearest Neighbours classification of gridded features.\n
    Arguments
    ----------
    - FEATURES [`input grid list`] : Features
    - TRAIN_SAMPLES [`input table`] : Training Samples. Provide a class identifier in the first field followed by sample data corresponding to the input feature grids.
    - TRAIN_AREAS [`input shapes`] : Training Areas
    - CLASSES [`output grid`] : Classification
    - CLASSES_LUT [`output table`] : Look-up Table. A reference list of the grid values that have been assigned to the training classes.
    - NORMALIZE [`boolean`] : Normalize. Default: 0
    - GRID_SYSTEM [`grid system`] : Grid System
    - MODEL_TRAIN [`choice`] : Training. Available Choices: [0] training areas [1] training samples [2] load from file Default: 0
    - TRAIN_CLASS [`table field`] : Class Identifier
    - TRAIN_BUFFER [`floating point number`] : Buffer Size. Minimum: 0.000000 Default: 1.000000 For non-polygon type training areas, creates a buffer with a diameter of specified size.
    - MODEL_LOAD [`file path`] : Load Model. Use a model previously stored to file.
    - MODEL_SAVE [`file path`] : Save Model. Stores model to file to be used for subsequent classifications instead of training areas.
    - NEIGHBOURS [`integer number`] : Default Number of Neighbours. Minimum: 1 Default: 3
    - TRAINING [`choice`] : Training Method. Available Choices: [0] classification [1] regression model Default: 0
    - ALGORITHM [`choice`] : Algorithm Type. Available Choices: [0] brute force [1] KD Tree Default: 0
    - EMAX [`integer number`] : Parameter for KD Tree implementation. Minimum: 1 Default: 1000

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('imagery_opencv', '6', 'K-Nearest Neighbours Classification')
    if Tool.is_Okay():
        Tool.Set_Input ('FEATURES', FEATURES)
        Tool.Set_Input ('TRAIN_SAMPLES', TRAIN_SAMPLES)
        Tool.Set_Input ('TRAIN_AREAS', TRAIN_AREAS)
        Tool.Set_Output('CLASSES', CLASSES)
        Tool.Set_Output('CLASSES_LUT', CLASSES_LUT)
        Tool.Set_Option('NORMALIZE', NORMALIZE)
        Tool.Set_Option('GRID_SYSTEM', GRID_SYSTEM)
        Tool.Set_Option('MODEL_TRAIN', MODEL_TRAIN)
        Tool.Set_Option('TRAIN_CLASS', TRAIN_CLASS)
        Tool.Set_Option('TRAIN_BUFFER', TRAIN_BUFFER)
        Tool.Set_Option('MODEL_LOAD', MODEL_LOAD)
        Tool.Set_Option('MODEL_SAVE', MODEL_SAVE)
        Tool.Set_Option('NEIGHBOURS', NEIGHBOURS)
        Tool.Set_Option('TRAINING', TRAINING)
        Tool.Set_Option('ALGORITHM', ALGORITHM)
        Tool.Set_Option('EMAX', EMAX)
        return Tool.Execute(Verbose)
    return False

def Support_Vector_Machine_Classification(FEATURES=None, TRAIN_SAMPLES=None, TRAIN_AREAS=None, CLASSES=None, CLASSES_LUT=None, NORMALIZE=None, GRID_SYSTEM=None, MODEL_TRAIN=None, TRAIN_CLASS=None, TRAIN_BUFFER=None, MODEL_LOAD=None, MODEL_SAVE=None, SVM_TYPE=None, C=None, NU=None, P=None, KERNEL=None, COEF0=None, DEGREE=None, GAMMA=None, Verbose=2):
    '''
    Support Vector Machine Classification
    ----------
    [imagery_opencv.7]\n
    Integration of the OpenCV Machine Learning library for Support Vector Machine classification of gridded features.\n
    Arguments
    ----------
    - FEATURES [`input grid list`] : Features
    - TRAIN_SAMPLES [`input table`] : Training Samples. Provide a class identifier in the first field followed by sample data corresponding to the input feature grids.
    - TRAIN_AREAS [`input shapes`] : Training Areas
    - CLASSES [`output grid`] : Classification
    - CLASSES_LUT [`output table`] : Look-up Table. A reference list of the grid values that have been assigned to the training classes.
    - NORMALIZE [`boolean`] : Normalize. Default: 0
    - GRID_SYSTEM [`grid system`] : Grid System
    - MODEL_TRAIN [`choice`] : Training. Available Choices: [0] training areas [1] training samples [2] load from file Default: 0
    - TRAIN_CLASS [`table field`] : Class Identifier
    - TRAIN_BUFFER [`floating point number`] : Buffer Size. Minimum: 0.000000 Default: 1.000000 For non-polygon type training areas, creates a buffer with a diameter of specified size.
    - MODEL_LOAD [`file path`] : Load Model. Use a model previously stored to file.
    - MODEL_SAVE [`file path`] : Save Model. Stores model to file to be used for subsequent classifications instead of training areas.
    - SVM_TYPE [`choice`] : SVM Type. Available Choices: [0] c-support vector classification [1] nu support vector classification [2] distribution estimation (one class) [3] epsilon support vector regression [4] nu support vector regression Default: 0
    - C [`floating point number`] : C. Minimum: 0.000000 Default: 5.000000
    - NU [`floating point number`] : Nu. Minimum: 0.000000 Default: 0.500000
    - P [`floating point number`] : P. Minimum: 0.000000 Default: 0.500000
    - KERNEL [`choice`] : Kernel Type. Available Choices: [0] linear [1] polynomial [2] radial basis function [3] sigmoid [4] exponential chi2 [5] histogram intersection Default: 2
    - COEF0 [`floating point number`] : Coefficient 0. Minimum: 0.000000 Default: 1.000000
    - DEGREE [`floating point number`] : Degree. Minimum: 0.000000 Default: 0.500000
    - GAMMA [`floating point number`] : Gamma. Minimum: 0.000000 Default: 5.000000

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('imagery_opencv', '7', 'Support Vector Machine Classification')
    if Tool.is_Okay():
        Tool.Set_Input ('FEATURES', FEATURES)
        Tool.Set_Input ('TRAIN_SAMPLES', TRAIN_SAMPLES)
        Tool.Set_Input ('TRAIN_AREAS', TRAIN_AREAS)
        Tool.Set_Output('CLASSES', CLASSES)
        Tool.Set_Output('CLASSES_LUT', CLASSES_LUT)
        Tool.Set_Option('NORMALIZE', NORMALIZE)
        Tool.Set_Option('GRID_SYSTEM', GRID_SYSTEM)
        Tool.Set_Option('MODEL_TRAIN', MODEL_TRAIN)
        Tool.Set_Option('TRAIN_CLASS', TRAIN_CLASS)
        Tool.Set_Option('TRAIN_BUFFER', TRAIN_BUFFER)
        Tool.Set_Option('MODEL_LOAD', MODEL_LOAD)
        Tool.Set_Option('MODEL_SAVE', MODEL_SAVE)
        Tool.Set_Option('SVM_TYPE', SVM_TYPE)
        Tool.Set_Option('C', C)
        Tool.Set_Option('NU', NU)
        Tool.Set_Option('P', P)
        Tool.Set_Option('KERNEL', KERNEL)
        Tool.Set_Option('COEF0', COEF0)
        Tool.Set_Option('DEGREE', DEGREE)
        Tool.Set_Option('GAMMA', GAMMA)
        return Tool.Execute(Verbose)
    return False

def run_tool_imagery_opencv_7(FEATURES=None, TRAIN_SAMPLES=None, TRAIN_AREAS=None, CLASSES=None, CLASSES_LUT=None, NORMALIZE=None, GRID_SYSTEM=None, MODEL_TRAIN=None, TRAIN_CLASS=None, TRAIN_BUFFER=None, MODEL_LOAD=None, MODEL_SAVE=None, SVM_TYPE=None, C=None, NU=None, P=None, KERNEL=None, COEF0=None, DEGREE=None, GAMMA=None, Verbose=2):
    '''
    Support Vector Machine Classification
    ----------
    [imagery_opencv.7]\n
    Integration of the OpenCV Machine Learning library for Support Vector Machine classification of gridded features.\n
    Arguments
    ----------
    - FEATURES [`input grid list`] : Features
    - TRAIN_SAMPLES [`input table`] : Training Samples. Provide a class identifier in the first field followed by sample data corresponding to the input feature grids.
    - TRAIN_AREAS [`input shapes`] : Training Areas
    - CLASSES [`output grid`] : Classification
    - CLASSES_LUT [`output table`] : Look-up Table. A reference list of the grid values that have been assigned to the training classes.
    - NORMALIZE [`boolean`] : Normalize. Default: 0
    - GRID_SYSTEM [`grid system`] : Grid System
    - MODEL_TRAIN [`choice`] : Training. Available Choices: [0] training areas [1] training samples [2] load from file Default: 0
    - TRAIN_CLASS [`table field`] : Class Identifier
    - TRAIN_BUFFER [`floating point number`] : Buffer Size. Minimum: 0.000000 Default: 1.000000 For non-polygon type training areas, creates a buffer with a diameter of specified size.
    - MODEL_LOAD [`file path`] : Load Model. Use a model previously stored to file.
    - MODEL_SAVE [`file path`] : Save Model. Stores model to file to be used for subsequent classifications instead of training areas.
    - SVM_TYPE [`choice`] : SVM Type. Available Choices: [0] c-support vector classification [1] nu support vector classification [2] distribution estimation (one class) [3] epsilon support vector regression [4] nu support vector regression Default: 0
    - C [`floating point number`] : C. Minimum: 0.000000 Default: 5.000000
    - NU [`floating point number`] : Nu. Minimum: 0.000000 Default: 0.500000
    - P [`floating point number`] : P. Minimum: 0.000000 Default: 0.500000
    - KERNEL [`choice`] : Kernel Type. Available Choices: [0] linear [1] polynomial [2] radial basis function [3] sigmoid [4] exponential chi2 [5] histogram intersection Default: 2
    - COEF0 [`floating point number`] : Coefficient 0. Minimum: 0.000000 Default: 1.000000
    - DEGREE [`floating point number`] : Degree. Minimum: 0.000000 Default: 0.500000
    - GAMMA [`floating point number`] : Gamma. Minimum: 0.000000 Default: 5.000000

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('imagery_opencv', '7', 'Support Vector Machine Classification')
    if Tool.is_Okay():
        Tool.Set_Input ('FEATURES', FEATURES)
        Tool.Set_Input ('TRAIN_SAMPLES', TRAIN_SAMPLES)
        Tool.Set_Input ('TRAIN_AREAS', TRAIN_AREAS)
        Tool.Set_Output('CLASSES', CLASSES)
        Tool.Set_Output('CLASSES_LUT', CLASSES_LUT)
        Tool.Set_Option('NORMALIZE', NORMALIZE)
        Tool.Set_Option('GRID_SYSTEM', GRID_SYSTEM)
        Tool.Set_Option('MODEL_TRAIN', MODEL_TRAIN)
        Tool.Set_Option('TRAIN_CLASS', TRAIN_CLASS)
        Tool.Set_Option('TRAIN_BUFFER', TRAIN_BUFFER)
        Tool.Set_Option('MODEL_LOAD', MODEL_LOAD)
        Tool.Set_Option('MODEL_SAVE', MODEL_SAVE)
        Tool.Set_Option('SVM_TYPE', SVM_TYPE)
        Tool.Set_Option('C', C)
        Tool.Set_Option('NU', NU)
        Tool.Set_Option('P', P)
        Tool.Set_Option('KERNEL', KERNEL)
        Tool.Set_Option('COEF0', COEF0)
        Tool.Set_Option('DEGREE', DEGREE)
        Tool.Set_Option('GAMMA', GAMMA)
        return Tool.Execute(Verbose)
    return False

def Decision_Tree_Classification(FEATURES=None, TRAIN_SAMPLES=None, TRAIN_AREAS=None, CLASSES=None, CLASSES_LUT=None, NORMALIZE=None, GRID_SYSTEM=None, MODEL_TRAIN=None, TRAIN_CLASS=None, TRAIN_BUFFER=None, MODEL_LOAD=None, MODEL_SAVE=None, MAX_DEPTH=None, MIN_SAMPLES=None, MAX_CATEGRS=None, _1SE_RULE=None, TRUNC_PRUNED=None, REG_ACCURACY=None, Verbose=2):
    '''
    Decision Tree Classification
    ----------
    [imagery_opencv.8]\n
    Integration of the OpenCV Machine Learning library for Decision Tree classification of gridded features.\n
    Arguments
    ----------
    - FEATURES [`input grid list`] : Features
    - TRAIN_SAMPLES [`input table`] : Training Samples. Provide a class identifier in the first field followed by sample data corresponding to the input feature grids.
    - TRAIN_AREAS [`input shapes`] : Training Areas
    - CLASSES [`output grid`] : Classification
    - CLASSES_LUT [`output table`] : Look-up Table. A reference list of the grid values that have been assigned to the training classes.
    - NORMALIZE [`boolean`] : Normalize. Default: 0
    - GRID_SYSTEM [`grid system`] : Grid System
    - MODEL_TRAIN [`choice`] : Training. Available Choices: [0] training areas [1] training samples [2] load from file Default: 0
    - TRAIN_CLASS [`table field`] : Class Identifier
    - TRAIN_BUFFER [`floating point number`] : Buffer Size. Minimum: 0.000000 Default: 1.000000 For non-polygon type training areas, creates a buffer with a diameter of specified size.
    - MODEL_LOAD [`file path`] : Load Model. Use a model previously stored to file.
    - MODEL_SAVE [`file path`] : Save Model. Stores model to file to be used for subsequent classifications instead of training areas.
    - MAX_DEPTH [`integer number`] : Maximum Tree Depth. Minimum: 1 Default: 10 The maximum possible depth of the tree. That is the training algorithms attempts to split a node while its depth is less than maxDepth. The root node has zero depth.
    - MIN_SAMPLES [`integer number`] : Minimum Sample Count. Minimum: 2 Default: 2 If the number of samples in a node is less than this parameter then the node will not be split.
    - MAX_CATEGRS [`integer number`] : Maximum Categories. Minimum: 1 Default: 10 Cluster possible values of a categorical variable into K<=maxCategories clusters to find a suboptimal split.
    - _1SE_RULE [`boolean`] : Use 1SE Rule. Default: 1 If true then a pruning will be harsher. This will make a tree more compact and more resistant to the training data noise but a bit less accurate.
    - TRUNC_PRUNED [`boolean`] : Truncate Pruned Trees. Default: 1 If true then pruned branches are physically removed from the tree. Otherwise they are retained and it is possible to get results from the original unpruned (or pruned less aggressively) tree.
    - REG_ACCURACY [`floating point number`] : Regression Accuracy. Minimum: 0.000000 Default: 0.010000 Termination criteria for regression trees. If all absolute differences between an estimated value in a node and values of train samples in this node are less than this parameter then the node will not be split further.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('imagery_opencv', '8', 'Decision Tree Classification')
    if Tool.is_Okay():
        Tool.Set_Input ('FEATURES', FEATURES)
        Tool.Set_Input ('TRAIN_SAMPLES', TRAIN_SAMPLES)
        Tool.Set_Input ('TRAIN_AREAS', TRAIN_AREAS)
        Tool.Set_Output('CLASSES', CLASSES)
        Tool.Set_Output('CLASSES_LUT', CLASSES_LUT)
        Tool.Set_Option('NORMALIZE', NORMALIZE)
        Tool.Set_Option('GRID_SYSTEM', GRID_SYSTEM)
        Tool.Set_Option('MODEL_TRAIN', MODEL_TRAIN)
        Tool.Set_Option('TRAIN_CLASS', TRAIN_CLASS)
        Tool.Set_Option('TRAIN_BUFFER', TRAIN_BUFFER)
        Tool.Set_Option('MODEL_LOAD', MODEL_LOAD)
        Tool.Set_Option('MODEL_SAVE', MODEL_SAVE)
        Tool.Set_Option('MAX_DEPTH', MAX_DEPTH)
        Tool.Set_Option('MIN_SAMPLES', MIN_SAMPLES)
        Tool.Set_Option('MAX_CATEGRS', MAX_CATEGRS)
        Tool.Set_Option('1SE_RULE', _1SE_RULE)
        Tool.Set_Option('TRUNC_PRUNED', TRUNC_PRUNED)
        Tool.Set_Option('REG_ACCURACY', REG_ACCURACY)
        return Tool.Execute(Verbose)
    return False

def run_tool_imagery_opencv_8(FEATURES=None, TRAIN_SAMPLES=None, TRAIN_AREAS=None, CLASSES=None, CLASSES_LUT=None, NORMALIZE=None, GRID_SYSTEM=None, MODEL_TRAIN=None, TRAIN_CLASS=None, TRAIN_BUFFER=None, MODEL_LOAD=None, MODEL_SAVE=None, MAX_DEPTH=None, MIN_SAMPLES=None, MAX_CATEGRS=None, _1SE_RULE=None, TRUNC_PRUNED=None, REG_ACCURACY=None, Verbose=2):
    '''
    Decision Tree Classification
    ----------
    [imagery_opencv.8]\n
    Integration of the OpenCV Machine Learning library for Decision Tree classification of gridded features.\n
    Arguments
    ----------
    - FEATURES [`input grid list`] : Features
    - TRAIN_SAMPLES [`input table`] : Training Samples. Provide a class identifier in the first field followed by sample data corresponding to the input feature grids.
    - TRAIN_AREAS [`input shapes`] : Training Areas
    - CLASSES [`output grid`] : Classification
    - CLASSES_LUT [`output table`] : Look-up Table. A reference list of the grid values that have been assigned to the training classes.
    - NORMALIZE [`boolean`] : Normalize. Default: 0
    - GRID_SYSTEM [`grid system`] : Grid System
    - MODEL_TRAIN [`choice`] : Training. Available Choices: [0] training areas [1] training samples [2] load from file Default: 0
    - TRAIN_CLASS [`table field`] : Class Identifier
    - TRAIN_BUFFER [`floating point number`] : Buffer Size. Minimum: 0.000000 Default: 1.000000 For non-polygon type training areas, creates a buffer with a diameter of specified size.
    - MODEL_LOAD [`file path`] : Load Model. Use a model previously stored to file.
    - MODEL_SAVE [`file path`] : Save Model. Stores model to file to be used for subsequent classifications instead of training areas.
    - MAX_DEPTH [`integer number`] : Maximum Tree Depth. Minimum: 1 Default: 10 The maximum possible depth of the tree. That is the training algorithms attempts to split a node while its depth is less than maxDepth. The root node has zero depth.
    - MIN_SAMPLES [`integer number`] : Minimum Sample Count. Minimum: 2 Default: 2 If the number of samples in a node is less than this parameter then the node will not be split.
    - MAX_CATEGRS [`integer number`] : Maximum Categories. Minimum: 1 Default: 10 Cluster possible values of a categorical variable into K<=maxCategories clusters to find a suboptimal split.
    - _1SE_RULE [`boolean`] : Use 1SE Rule. Default: 1 If true then a pruning will be harsher. This will make a tree more compact and more resistant to the training data noise but a bit less accurate.
    - TRUNC_PRUNED [`boolean`] : Truncate Pruned Trees. Default: 1 If true then pruned branches are physically removed from the tree. Otherwise they are retained and it is possible to get results from the original unpruned (or pruned less aggressively) tree.
    - REG_ACCURACY [`floating point number`] : Regression Accuracy. Minimum: 0.000000 Default: 0.010000 Termination criteria for regression trees. If all absolute differences between an estimated value in a node and values of train samples in this node are less than this parameter then the node will not be split further.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('imagery_opencv', '8', 'Decision Tree Classification')
    if Tool.is_Okay():
        Tool.Set_Input ('FEATURES', FEATURES)
        Tool.Set_Input ('TRAIN_SAMPLES', TRAIN_SAMPLES)
        Tool.Set_Input ('TRAIN_AREAS', TRAIN_AREAS)
        Tool.Set_Output('CLASSES', CLASSES)
        Tool.Set_Output('CLASSES_LUT', CLASSES_LUT)
        Tool.Set_Option('NORMALIZE', NORMALIZE)
        Tool.Set_Option('GRID_SYSTEM', GRID_SYSTEM)
        Tool.Set_Option('MODEL_TRAIN', MODEL_TRAIN)
        Tool.Set_Option('TRAIN_CLASS', TRAIN_CLASS)
        Tool.Set_Option('TRAIN_BUFFER', TRAIN_BUFFER)
        Tool.Set_Option('MODEL_LOAD', MODEL_LOAD)
        Tool.Set_Option('MODEL_SAVE', MODEL_SAVE)
        Tool.Set_Option('MAX_DEPTH', MAX_DEPTH)
        Tool.Set_Option('MIN_SAMPLES', MIN_SAMPLES)
        Tool.Set_Option('MAX_CATEGRS', MAX_CATEGRS)
        Tool.Set_Option('1SE_RULE', _1SE_RULE)
        Tool.Set_Option('TRUNC_PRUNED', TRUNC_PRUNED)
        Tool.Set_Option('REG_ACCURACY', REG_ACCURACY)
        return Tool.Execute(Verbose)
    return False

def Boosting_Classification(FEATURES=None, TRAIN_SAMPLES=None, TRAIN_AREAS=None, CLASSES=None, CLASSES_LUT=None, NORMALIZE=None, GRID_SYSTEM=None, MODEL_TRAIN=None, TRAIN_CLASS=None, TRAIN_BUFFER=None, MODEL_LOAD=None, MODEL_SAVE=None, MAX_DEPTH=None, MIN_SAMPLES=None, MAX_CATEGRS=None, _1SE_RULE=None, TRUNC_PRUNED=None, REG_ACCURACY=None, WEAK_COUNT=None, WGT_TRIM_RATE=None, BOOST_TYPE=None, Verbose=2):
    '''
    Boosting Classification
    ----------
    [imagery_opencv.9]\n
    Integration of the OpenCV Machine Learning library for Boosted Trees classification of gridded features.\n
    Arguments
    ----------
    - FEATURES [`input grid list`] : Features
    - TRAIN_SAMPLES [`input table`] : Training Samples. Provide a class identifier in the first field followed by sample data corresponding to the input feature grids.
    - TRAIN_AREAS [`input shapes`] : Training Areas
    - CLASSES [`output grid`] : Classification
    - CLASSES_LUT [`output table`] : Look-up Table. A reference list of the grid values that have been assigned to the training classes.
    - NORMALIZE [`boolean`] : Normalize. Default: 0
    - GRID_SYSTEM [`grid system`] : Grid System
    - MODEL_TRAIN [`choice`] : Training. Available Choices: [0] training areas [1] training samples [2] load from file Default: 0
    - TRAIN_CLASS [`table field`] : Class Identifier
    - TRAIN_BUFFER [`floating point number`] : Buffer Size. Minimum: 0.000000 Default: 1.000000 For non-polygon type training areas, creates a buffer with a diameter of specified size.
    - MODEL_LOAD [`file path`] : Load Model. Use a model previously stored to file.
    - MODEL_SAVE [`file path`] : Save Model. Stores model to file to be used for subsequent classifications instead of training areas.
    - MAX_DEPTH [`integer number`] : Maximum Tree Depth. Minimum: 1 Default: 10 The maximum possible depth of the tree. That is the training algorithms attempts to split a node while its depth is less than maxDepth. The root node has zero depth.
    - MIN_SAMPLES [`integer number`] : Minimum Sample Count. Minimum: 2 Default: 2 If the number of samples in a node is less than this parameter then the node will not be split.
    - MAX_CATEGRS [`integer number`] : Maximum Categories. Minimum: 1 Default: 10 Cluster possible values of a categorical variable into K<=maxCategories clusters to find a suboptimal split.
    - _1SE_RULE [`boolean`] : Use 1SE Rule. Default: 1 If true then a pruning will be harsher. This will make a tree more compact and more resistant to the training data noise but a bit less accurate.
    - TRUNC_PRUNED [`boolean`] : Truncate Pruned Trees. Default: 1 If true then pruned branches are physically removed from the tree. Otherwise they are retained and it is possible to get results from the original unpruned (or pruned less aggressively) tree.
    - REG_ACCURACY [`floating point number`] : Regression Accuracy. Minimum: 0.000000 Default: 0.010000 Termination criteria for regression trees. If all absolute differences between an estimated value in a node and values of train samples in this node are less than this parameter then the node will not be split further.
    - WEAK_COUNT [`integer number`] : Weak Count. Minimum: 0 Default: 100 The number of weak classifiers.
    - WGT_TRIM_RATE [`floating point number`] : Weight Trim Rate. Minimum: 0.000000 Maximum: 1.000000 Default: 0.950000 A threshold between 0 and 1 used to save computational time. Set this parameter to 0 to turn off this functionality.
    - BOOST_TYPE [`choice`] : Boost Type. Available Choices: [0] Discrete AdaBoost [1] Real AdaBoost [2] LogitBoost [3] Gentle AdaBoost Default: 1

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('imagery_opencv', '9', 'Boosting Classification')
    if Tool.is_Okay():
        Tool.Set_Input ('FEATURES', FEATURES)
        Tool.Set_Input ('TRAIN_SAMPLES', TRAIN_SAMPLES)
        Tool.Set_Input ('TRAIN_AREAS', TRAIN_AREAS)
        Tool.Set_Output('CLASSES', CLASSES)
        Tool.Set_Output('CLASSES_LUT', CLASSES_LUT)
        Tool.Set_Option('NORMALIZE', NORMALIZE)
        Tool.Set_Option('GRID_SYSTEM', GRID_SYSTEM)
        Tool.Set_Option('MODEL_TRAIN', MODEL_TRAIN)
        Tool.Set_Option('TRAIN_CLASS', TRAIN_CLASS)
        Tool.Set_Option('TRAIN_BUFFER', TRAIN_BUFFER)
        Tool.Set_Option('MODEL_LOAD', MODEL_LOAD)
        Tool.Set_Option('MODEL_SAVE', MODEL_SAVE)
        Tool.Set_Option('MAX_DEPTH', MAX_DEPTH)
        Tool.Set_Option('MIN_SAMPLES', MIN_SAMPLES)
        Tool.Set_Option('MAX_CATEGRS', MAX_CATEGRS)
        Tool.Set_Option('1SE_RULE', _1SE_RULE)
        Tool.Set_Option('TRUNC_PRUNED', TRUNC_PRUNED)
        Tool.Set_Option('REG_ACCURACY', REG_ACCURACY)
        Tool.Set_Option('WEAK_COUNT', WEAK_COUNT)
        Tool.Set_Option('WGT_TRIM_RATE', WGT_TRIM_RATE)
        Tool.Set_Option('BOOST_TYPE', BOOST_TYPE)
        return Tool.Execute(Verbose)
    return False

def run_tool_imagery_opencv_9(FEATURES=None, TRAIN_SAMPLES=None, TRAIN_AREAS=None, CLASSES=None, CLASSES_LUT=None, NORMALIZE=None, GRID_SYSTEM=None, MODEL_TRAIN=None, TRAIN_CLASS=None, TRAIN_BUFFER=None, MODEL_LOAD=None, MODEL_SAVE=None, MAX_DEPTH=None, MIN_SAMPLES=None, MAX_CATEGRS=None, _1SE_RULE=None, TRUNC_PRUNED=None, REG_ACCURACY=None, WEAK_COUNT=None, WGT_TRIM_RATE=None, BOOST_TYPE=None, Verbose=2):
    '''
    Boosting Classification
    ----------
    [imagery_opencv.9]\n
    Integration of the OpenCV Machine Learning library for Boosted Trees classification of gridded features.\n
    Arguments
    ----------
    - FEATURES [`input grid list`] : Features
    - TRAIN_SAMPLES [`input table`] : Training Samples. Provide a class identifier in the first field followed by sample data corresponding to the input feature grids.
    - TRAIN_AREAS [`input shapes`] : Training Areas
    - CLASSES [`output grid`] : Classification
    - CLASSES_LUT [`output table`] : Look-up Table. A reference list of the grid values that have been assigned to the training classes.
    - NORMALIZE [`boolean`] : Normalize. Default: 0
    - GRID_SYSTEM [`grid system`] : Grid System
    - MODEL_TRAIN [`choice`] : Training. Available Choices: [0] training areas [1] training samples [2] load from file Default: 0
    - TRAIN_CLASS [`table field`] : Class Identifier
    - TRAIN_BUFFER [`floating point number`] : Buffer Size. Minimum: 0.000000 Default: 1.000000 For non-polygon type training areas, creates a buffer with a diameter of specified size.
    - MODEL_LOAD [`file path`] : Load Model. Use a model previously stored to file.
    - MODEL_SAVE [`file path`] : Save Model. Stores model to file to be used for subsequent classifications instead of training areas.
    - MAX_DEPTH [`integer number`] : Maximum Tree Depth. Minimum: 1 Default: 10 The maximum possible depth of the tree. That is the training algorithms attempts to split a node while its depth is less than maxDepth. The root node has zero depth.
    - MIN_SAMPLES [`integer number`] : Minimum Sample Count. Minimum: 2 Default: 2 If the number of samples in a node is less than this parameter then the node will not be split.
    - MAX_CATEGRS [`integer number`] : Maximum Categories. Minimum: 1 Default: 10 Cluster possible values of a categorical variable into K<=maxCategories clusters to find a suboptimal split.
    - _1SE_RULE [`boolean`] : Use 1SE Rule. Default: 1 If true then a pruning will be harsher. This will make a tree more compact and more resistant to the training data noise but a bit less accurate.
    - TRUNC_PRUNED [`boolean`] : Truncate Pruned Trees. Default: 1 If true then pruned branches are physically removed from the tree. Otherwise they are retained and it is possible to get results from the original unpruned (or pruned less aggressively) tree.
    - REG_ACCURACY [`floating point number`] : Regression Accuracy. Minimum: 0.000000 Default: 0.010000 Termination criteria for regression trees. If all absolute differences between an estimated value in a node and values of train samples in this node are less than this parameter then the node will not be split further.
    - WEAK_COUNT [`integer number`] : Weak Count. Minimum: 0 Default: 100 The number of weak classifiers.
    - WGT_TRIM_RATE [`floating point number`] : Weight Trim Rate. Minimum: 0.000000 Maximum: 1.000000 Default: 0.950000 A threshold between 0 and 1 used to save computational time. Set this parameter to 0 to turn off this functionality.
    - BOOST_TYPE [`choice`] : Boost Type. Available Choices: [0] Discrete AdaBoost [1] Real AdaBoost [2] LogitBoost [3] Gentle AdaBoost Default: 1

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('imagery_opencv', '9', 'Boosting Classification')
    if Tool.is_Okay():
        Tool.Set_Input ('FEATURES', FEATURES)
        Tool.Set_Input ('TRAIN_SAMPLES', TRAIN_SAMPLES)
        Tool.Set_Input ('TRAIN_AREAS', TRAIN_AREAS)
        Tool.Set_Output('CLASSES', CLASSES)
        Tool.Set_Output('CLASSES_LUT', CLASSES_LUT)
        Tool.Set_Option('NORMALIZE', NORMALIZE)
        Tool.Set_Option('GRID_SYSTEM', GRID_SYSTEM)
        Tool.Set_Option('MODEL_TRAIN', MODEL_TRAIN)
        Tool.Set_Option('TRAIN_CLASS', TRAIN_CLASS)
        Tool.Set_Option('TRAIN_BUFFER', TRAIN_BUFFER)
        Tool.Set_Option('MODEL_LOAD', MODEL_LOAD)
        Tool.Set_Option('MODEL_SAVE', MODEL_SAVE)
        Tool.Set_Option('MAX_DEPTH', MAX_DEPTH)
        Tool.Set_Option('MIN_SAMPLES', MIN_SAMPLES)
        Tool.Set_Option('MAX_CATEGRS', MAX_CATEGRS)
        Tool.Set_Option('1SE_RULE', _1SE_RULE)
        Tool.Set_Option('TRUNC_PRUNED', TRUNC_PRUNED)
        Tool.Set_Option('REG_ACCURACY', REG_ACCURACY)
        Tool.Set_Option('WEAK_COUNT', WEAK_COUNT)
        Tool.Set_Option('WGT_TRIM_RATE', WGT_TRIM_RATE)
        Tool.Set_Option('BOOST_TYPE', BOOST_TYPE)
        return Tool.Execute(Verbose)
    return False

def Random_Forest_Classification(FEATURES=None, TRAIN_SAMPLES=None, TRAIN_AREAS=None, CLASSES=None, CLASSES_LUT=None, NORMALIZE=None, GRID_SYSTEM=None, MODEL_TRAIN=None, TRAIN_CLASS=None, TRAIN_BUFFER=None, MODEL_LOAD=None, MODEL_SAVE=None, MAX_DEPTH=None, MIN_SAMPLES=None, MAX_CATEGRS=None, _1SE_RULE=None, TRUNC_PRUNED=None, REG_ACCURACY=None, ACTIVE_VARS=None, Verbose=2):
    '''
    Random Forest Classification
    ----------
    [imagery_opencv.10]\n
    Integration of the OpenCV Machine Learning library for Random Forest classification of gridded features.\n
    Arguments
    ----------
    - FEATURES [`input grid list`] : Features
    - TRAIN_SAMPLES [`input table`] : Training Samples. Provide a class identifier in the first field followed by sample data corresponding to the input feature grids.
    - TRAIN_AREAS [`input shapes`] : Training Areas
    - CLASSES [`output grid`] : Classification
    - CLASSES_LUT [`output table`] : Look-up Table. A reference list of the grid values that have been assigned to the training classes.
    - NORMALIZE [`boolean`] : Normalize. Default: 0
    - GRID_SYSTEM [`grid system`] : Grid System
    - MODEL_TRAIN [`choice`] : Training. Available Choices: [0] training areas [1] training samples [2] load from file Default: 0
    - TRAIN_CLASS [`table field`] : Class Identifier
    - TRAIN_BUFFER [`floating point number`] : Buffer Size. Minimum: 0.000000 Default: 1.000000 For non-polygon type training areas, creates a buffer with a diameter of specified size.
    - MODEL_LOAD [`file path`] : Load Model. Use a model previously stored to file.
    - MODEL_SAVE [`file path`] : Save Model. Stores model to file to be used for subsequent classifications instead of training areas.
    - MAX_DEPTH [`integer number`] : Maximum Tree Depth. Minimum: 1 Default: 10 The maximum possible depth of the tree. That is the training algorithms attempts to split a node while its depth is less than maxDepth. The root node has zero depth.
    - MIN_SAMPLES [`integer number`] : Minimum Sample Count. Minimum: 2 Default: 2 If the number of samples in a node is less than this parameter then the node will not be split.
    - MAX_CATEGRS [`integer number`] : Maximum Categories. Minimum: 1 Default: 10 Cluster possible values of a categorical variable into K<=maxCategories clusters to find a suboptimal split.
    - _1SE_RULE [`boolean`] : Use 1SE Rule. Default: 1 If true then a pruning will be harsher. This will make a tree more compact and more resistant to the training data noise but a bit less accurate.
    - TRUNC_PRUNED [`boolean`] : Truncate Pruned Trees. Default: 1 If true then pruned branches are physically removed from the tree. Otherwise they are retained and it is possible to get results from the original unpruned (or pruned less aggressively) tree.
    - REG_ACCURACY [`floating point number`] : Regression Accuracy. Minimum: 0.000000 Default: 0.010000 Termination criteria for regression trees. If all absolute differences between an estimated value in a node and values of train samples in this node are less than this parameter then the node will not be split further.
    - ACTIVE_VARS [`integer number`] : Active Variable Count. Minimum: 0 Default: 0 The size of the randomly selected subset of features at each tree node and that are used to find the best split(s). If you set it to 0 then the size will be set to the square root of the total number of features.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('imagery_opencv', '10', 'Random Forest Classification')
    if Tool.is_Okay():
        Tool.Set_Input ('FEATURES', FEATURES)
        Tool.Set_Input ('TRAIN_SAMPLES', TRAIN_SAMPLES)
        Tool.Set_Input ('TRAIN_AREAS', TRAIN_AREAS)
        Tool.Set_Output('CLASSES', CLASSES)
        Tool.Set_Output('CLASSES_LUT', CLASSES_LUT)
        Tool.Set_Option('NORMALIZE', NORMALIZE)
        Tool.Set_Option('GRID_SYSTEM', GRID_SYSTEM)
        Tool.Set_Option('MODEL_TRAIN', MODEL_TRAIN)
        Tool.Set_Option('TRAIN_CLASS', TRAIN_CLASS)
        Tool.Set_Option('TRAIN_BUFFER', TRAIN_BUFFER)
        Tool.Set_Option('MODEL_LOAD', MODEL_LOAD)
        Tool.Set_Option('MODEL_SAVE', MODEL_SAVE)
        Tool.Set_Option('MAX_DEPTH', MAX_DEPTH)
        Tool.Set_Option('MIN_SAMPLES', MIN_SAMPLES)
        Tool.Set_Option('MAX_CATEGRS', MAX_CATEGRS)
        Tool.Set_Option('1SE_RULE', _1SE_RULE)
        Tool.Set_Option('TRUNC_PRUNED', TRUNC_PRUNED)
        Tool.Set_Option('REG_ACCURACY', REG_ACCURACY)
        Tool.Set_Option('ACTIVE_VARS', ACTIVE_VARS)
        return Tool.Execute(Verbose)
    return False

def run_tool_imagery_opencv_10(FEATURES=None, TRAIN_SAMPLES=None, TRAIN_AREAS=None, CLASSES=None, CLASSES_LUT=None, NORMALIZE=None, GRID_SYSTEM=None, MODEL_TRAIN=None, TRAIN_CLASS=None, TRAIN_BUFFER=None, MODEL_LOAD=None, MODEL_SAVE=None, MAX_DEPTH=None, MIN_SAMPLES=None, MAX_CATEGRS=None, _1SE_RULE=None, TRUNC_PRUNED=None, REG_ACCURACY=None, ACTIVE_VARS=None, Verbose=2):
    '''
    Random Forest Classification
    ----------
    [imagery_opencv.10]\n
    Integration of the OpenCV Machine Learning library for Random Forest classification of gridded features.\n
    Arguments
    ----------
    - FEATURES [`input grid list`] : Features
    - TRAIN_SAMPLES [`input table`] : Training Samples. Provide a class identifier in the first field followed by sample data corresponding to the input feature grids.
    - TRAIN_AREAS [`input shapes`] : Training Areas
    - CLASSES [`output grid`] : Classification
    - CLASSES_LUT [`output table`] : Look-up Table. A reference list of the grid values that have been assigned to the training classes.
    - NORMALIZE [`boolean`] : Normalize. Default: 0
    - GRID_SYSTEM [`grid system`] : Grid System
    - MODEL_TRAIN [`choice`] : Training. Available Choices: [0] training areas [1] training samples [2] load from file Default: 0
    - TRAIN_CLASS [`table field`] : Class Identifier
    - TRAIN_BUFFER [`floating point number`] : Buffer Size. Minimum: 0.000000 Default: 1.000000 For non-polygon type training areas, creates a buffer with a diameter of specified size.
    - MODEL_LOAD [`file path`] : Load Model. Use a model previously stored to file.
    - MODEL_SAVE [`file path`] : Save Model. Stores model to file to be used for subsequent classifications instead of training areas.
    - MAX_DEPTH [`integer number`] : Maximum Tree Depth. Minimum: 1 Default: 10 The maximum possible depth of the tree. That is the training algorithms attempts to split a node while its depth is less than maxDepth. The root node has zero depth.
    - MIN_SAMPLES [`integer number`] : Minimum Sample Count. Minimum: 2 Default: 2 If the number of samples in a node is less than this parameter then the node will not be split.
    - MAX_CATEGRS [`integer number`] : Maximum Categories. Minimum: 1 Default: 10 Cluster possible values of a categorical variable into K<=maxCategories clusters to find a suboptimal split.
    - _1SE_RULE [`boolean`] : Use 1SE Rule. Default: 1 If true then a pruning will be harsher. This will make a tree more compact and more resistant to the training data noise but a bit less accurate.
    - TRUNC_PRUNED [`boolean`] : Truncate Pruned Trees. Default: 1 If true then pruned branches are physically removed from the tree. Otherwise they are retained and it is possible to get results from the original unpruned (or pruned less aggressively) tree.
    - REG_ACCURACY [`floating point number`] : Regression Accuracy. Minimum: 0.000000 Default: 0.010000 Termination criteria for regression trees. If all absolute differences between an estimated value in a node and values of train samples in this node are less than this parameter then the node will not be split further.
    - ACTIVE_VARS [`integer number`] : Active Variable Count. Minimum: 0 Default: 0 The size of the randomly selected subset of features at each tree node and that are used to find the best split(s). If you set it to 0 then the size will be set to the square root of the total number of features.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('imagery_opencv', '10', 'Random Forest Classification')
    if Tool.is_Okay():
        Tool.Set_Input ('FEATURES', FEATURES)
        Tool.Set_Input ('TRAIN_SAMPLES', TRAIN_SAMPLES)
        Tool.Set_Input ('TRAIN_AREAS', TRAIN_AREAS)
        Tool.Set_Output('CLASSES', CLASSES)
        Tool.Set_Output('CLASSES_LUT', CLASSES_LUT)
        Tool.Set_Option('NORMALIZE', NORMALIZE)
        Tool.Set_Option('GRID_SYSTEM', GRID_SYSTEM)
        Tool.Set_Option('MODEL_TRAIN', MODEL_TRAIN)
        Tool.Set_Option('TRAIN_CLASS', TRAIN_CLASS)
        Tool.Set_Option('TRAIN_BUFFER', TRAIN_BUFFER)
        Tool.Set_Option('MODEL_LOAD', MODEL_LOAD)
        Tool.Set_Option('MODEL_SAVE', MODEL_SAVE)
        Tool.Set_Option('MAX_DEPTH', MAX_DEPTH)
        Tool.Set_Option('MIN_SAMPLES', MIN_SAMPLES)
        Tool.Set_Option('MAX_CATEGRS', MAX_CATEGRS)
        Tool.Set_Option('1SE_RULE', _1SE_RULE)
        Tool.Set_Option('TRUNC_PRUNED', TRUNC_PRUNED)
        Tool.Set_Option('REG_ACCURACY', REG_ACCURACY)
        Tool.Set_Option('ACTIVE_VARS', ACTIVE_VARS)
        return Tool.Execute(Verbose)
    return False

def Artificial_Neural_Network_Classification(FEATURES=None, TRAIN_SAMPLES=None, TRAIN_AREAS=None, CLASSES=None, CLASSES_LUT=None, NORMALIZE=None, GRID_SYSTEM=None, MODEL_TRAIN=None, TRAIN_CLASS=None, TRAIN_BUFFER=None, MODEL_LOAD=None, MODEL_SAVE=None, ANN_LAYERS=None, ANN_NEURONS=None, ANN_MAXITER=None, ANN_EPSILON=None, ANN_ACTIVATION=None, ANN_ACT_ALPHA=None, ANN_ACT_BETA=None, ANN_PROPAGATION=None, ANN_RP_DW0=None, ANN_RP_DW_PLUS=None, ANN_RP_DW_MINUS=None, ANN_RP_DW_MIN=None, ANN_RP_DW_MAX=None, ANN_BP_DW=None, ANN_BP_MOMENT=None, Verbose=2):
    '''
    Artificial Neural Network Classification
    ----------
    [imagery_opencv.11]\n
    Integration of the OpenCV Machine Learning library for Artificial Neural Network classification of gridded features.\n
    Arguments
    ----------
    - FEATURES [`input grid list`] : Features
    - TRAIN_SAMPLES [`input table`] : Training Samples. Provide a class identifier in the first field followed by sample data corresponding to the input feature grids.
    - TRAIN_AREAS [`input shapes`] : Training Areas
    - CLASSES [`output grid`] : Classification
    - CLASSES_LUT [`output table`] : Look-up Table. A reference list of the grid values that have been assigned to the training classes.
    - NORMALIZE [`boolean`] : Normalize. Default: 0
    - GRID_SYSTEM [`grid system`] : Grid System
    - MODEL_TRAIN [`choice`] : Training. Available Choices: [0] training areas [1] training samples [2] load from file Default: 0
    - TRAIN_CLASS [`table field`] : Class Identifier
    - TRAIN_BUFFER [`floating point number`] : Buffer Size. Minimum: 0.000000 Default: 1.000000 For non-polygon type training areas, creates a buffer with a diameter of specified size.
    - MODEL_LOAD [`file path`] : Load Model. Use a model previously stored to file.
    - MODEL_SAVE [`file path`] : Save Model. Stores model to file to be used for subsequent classifications instead of training areas.
    - ANN_LAYERS [`integer number`] : Number of Layers. Minimum: 1 Default: 3 You can specify the number of layers in the network (not including input and output layer).
    - ANN_NEURONS [`integer number`] : Number of Neurons. Minimum: 1 Default: 5 You can specify the number of neurons in each layer of the network.
    - ANN_MAXITER [`integer number`] : Maximum Number of Iterations. Minimum: 1 Default: 300
    - ANN_EPSILON [`floating point number`] : Error Change (Epsilon). Minimum: 0.000000 Default: 0.000000 Termination criteria of the training algorithm. You can specify how much the error could change between the iterations to make the algorithm continue (epsilon).
    - ANN_ACTIVATION [`choice`] : Activation Function. Available Choices: [0] Identity [1] Sigmoid [2] Gaussian Default: 1
    - ANN_ACT_ALPHA [`floating point number`] : Function's Alpha. Default: 1.000000
    - ANN_ACT_BETA [`floating point number`] : Function's Beta. Default: 1.000000
    - ANN_PROPAGATION [`choice`] : Training Method. Available Choices: [0] resilient propagation [1] back propagation Default: 1
    - ANN_RP_DW0 [`floating point number`] : Initial Update Value. Default: 0.000000
    - ANN_RP_DW_PLUS [`floating point number`] : Increase Factor. Minimum: 1.010000 Default: 1.200000
    - ANN_RP_DW_MINUS [`floating point number`] : Decrease Factor. Minimum: 0.010000 Maximum: 0.990000 Default: 0.500000
    - ANN_RP_DW_MIN [`floating point number`] : Lower Value Update Limit. Minimum: 0.010000 Default: 0.100000
    - ANN_RP_DW_MAX [`floating point number`] : Upper Value Update Limit. Minimum: 1.010000 Default: 1.100000
    - ANN_BP_DW [`floating point number`] : Weight Gradient Term. Default: 0.100000
    - ANN_BP_MOMENT [`floating point number`] : Moment Term. Default: 0.100000

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('imagery_opencv', '11', 'Artificial Neural Network Classification')
    if Tool.is_Okay():
        Tool.Set_Input ('FEATURES', FEATURES)
        Tool.Set_Input ('TRAIN_SAMPLES', TRAIN_SAMPLES)
        Tool.Set_Input ('TRAIN_AREAS', TRAIN_AREAS)
        Tool.Set_Output('CLASSES', CLASSES)
        Tool.Set_Output('CLASSES_LUT', CLASSES_LUT)
        Tool.Set_Option('NORMALIZE', NORMALIZE)
        Tool.Set_Option('GRID_SYSTEM', GRID_SYSTEM)
        Tool.Set_Option('MODEL_TRAIN', MODEL_TRAIN)
        Tool.Set_Option('TRAIN_CLASS', TRAIN_CLASS)
        Tool.Set_Option('TRAIN_BUFFER', TRAIN_BUFFER)
        Tool.Set_Option('MODEL_LOAD', MODEL_LOAD)
        Tool.Set_Option('MODEL_SAVE', MODEL_SAVE)
        Tool.Set_Option('ANN_LAYERS', ANN_LAYERS)
        Tool.Set_Option('ANN_NEURONS', ANN_NEURONS)
        Tool.Set_Option('ANN_MAXITER', ANN_MAXITER)
        Tool.Set_Option('ANN_EPSILON', ANN_EPSILON)
        Tool.Set_Option('ANN_ACTIVATION', ANN_ACTIVATION)
        Tool.Set_Option('ANN_ACT_ALPHA', ANN_ACT_ALPHA)
        Tool.Set_Option('ANN_ACT_BETA', ANN_ACT_BETA)
        Tool.Set_Option('ANN_PROPAGATION', ANN_PROPAGATION)
        Tool.Set_Option('ANN_RP_DW0', ANN_RP_DW0)
        Tool.Set_Option('ANN_RP_DW_PLUS', ANN_RP_DW_PLUS)
        Tool.Set_Option('ANN_RP_DW_MINUS', ANN_RP_DW_MINUS)
        Tool.Set_Option('ANN_RP_DW_MIN', ANN_RP_DW_MIN)
        Tool.Set_Option('ANN_RP_DW_MAX', ANN_RP_DW_MAX)
        Tool.Set_Option('ANN_BP_DW', ANN_BP_DW)
        Tool.Set_Option('ANN_BP_MOMENT', ANN_BP_MOMENT)
        return Tool.Execute(Verbose)
    return False

def run_tool_imagery_opencv_11(FEATURES=None, TRAIN_SAMPLES=None, TRAIN_AREAS=None, CLASSES=None, CLASSES_LUT=None, NORMALIZE=None, GRID_SYSTEM=None, MODEL_TRAIN=None, TRAIN_CLASS=None, TRAIN_BUFFER=None, MODEL_LOAD=None, MODEL_SAVE=None, ANN_LAYERS=None, ANN_NEURONS=None, ANN_MAXITER=None, ANN_EPSILON=None, ANN_ACTIVATION=None, ANN_ACT_ALPHA=None, ANN_ACT_BETA=None, ANN_PROPAGATION=None, ANN_RP_DW0=None, ANN_RP_DW_PLUS=None, ANN_RP_DW_MINUS=None, ANN_RP_DW_MIN=None, ANN_RP_DW_MAX=None, ANN_BP_DW=None, ANN_BP_MOMENT=None, Verbose=2):
    '''
    Artificial Neural Network Classification
    ----------
    [imagery_opencv.11]\n
    Integration of the OpenCV Machine Learning library for Artificial Neural Network classification of gridded features.\n
    Arguments
    ----------
    - FEATURES [`input grid list`] : Features
    - TRAIN_SAMPLES [`input table`] : Training Samples. Provide a class identifier in the first field followed by sample data corresponding to the input feature grids.
    - TRAIN_AREAS [`input shapes`] : Training Areas
    - CLASSES [`output grid`] : Classification
    - CLASSES_LUT [`output table`] : Look-up Table. A reference list of the grid values that have been assigned to the training classes.
    - NORMALIZE [`boolean`] : Normalize. Default: 0
    - GRID_SYSTEM [`grid system`] : Grid System
    - MODEL_TRAIN [`choice`] : Training. Available Choices: [0] training areas [1] training samples [2] load from file Default: 0
    - TRAIN_CLASS [`table field`] : Class Identifier
    - TRAIN_BUFFER [`floating point number`] : Buffer Size. Minimum: 0.000000 Default: 1.000000 For non-polygon type training areas, creates a buffer with a diameter of specified size.
    - MODEL_LOAD [`file path`] : Load Model. Use a model previously stored to file.
    - MODEL_SAVE [`file path`] : Save Model. Stores model to file to be used for subsequent classifications instead of training areas.
    - ANN_LAYERS [`integer number`] : Number of Layers. Minimum: 1 Default: 3 You can specify the number of layers in the network (not including input and output layer).
    - ANN_NEURONS [`integer number`] : Number of Neurons. Minimum: 1 Default: 5 You can specify the number of neurons in each layer of the network.
    - ANN_MAXITER [`integer number`] : Maximum Number of Iterations. Minimum: 1 Default: 300
    - ANN_EPSILON [`floating point number`] : Error Change (Epsilon). Minimum: 0.000000 Default: 0.000000 Termination criteria of the training algorithm. You can specify how much the error could change between the iterations to make the algorithm continue (epsilon).
    - ANN_ACTIVATION [`choice`] : Activation Function. Available Choices: [0] Identity [1] Sigmoid [2] Gaussian Default: 1
    - ANN_ACT_ALPHA [`floating point number`] : Function's Alpha. Default: 1.000000
    - ANN_ACT_BETA [`floating point number`] : Function's Beta. Default: 1.000000
    - ANN_PROPAGATION [`choice`] : Training Method. Available Choices: [0] resilient propagation [1] back propagation Default: 1
    - ANN_RP_DW0 [`floating point number`] : Initial Update Value. Default: 0.000000
    - ANN_RP_DW_PLUS [`floating point number`] : Increase Factor. Minimum: 1.010000 Default: 1.200000
    - ANN_RP_DW_MINUS [`floating point number`] : Decrease Factor. Minimum: 0.010000 Maximum: 0.990000 Default: 0.500000
    - ANN_RP_DW_MIN [`floating point number`] : Lower Value Update Limit. Minimum: 0.010000 Default: 0.100000
    - ANN_RP_DW_MAX [`floating point number`] : Upper Value Update Limit. Minimum: 1.010000 Default: 1.100000
    - ANN_BP_DW [`floating point number`] : Weight Gradient Term. Default: 0.100000
    - ANN_BP_MOMENT [`floating point number`] : Moment Term. Default: 0.100000

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('imagery_opencv', '11', 'Artificial Neural Network Classification')
    if Tool.is_Okay():
        Tool.Set_Input ('FEATURES', FEATURES)
        Tool.Set_Input ('TRAIN_SAMPLES', TRAIN_SAMPLES)
        Tool.Set_Input ('TRAIN_AREAS', TRAIN_AREAS)
        Tool.Set_Output('CLASSES', CLASSES)
        Tool.Set_Output('CLASSES_LUT', CLASSES_LUT)
        Tool.Set_Option('NORMALIZE', NORMALIZE)
        Tool.Set_Option('GRID_SYSTEM', GRID_SYSTEM)
        Tool.Set_Option('MODEL_TRAIN', MODEL_TRAIN)
        Tool.Set_Option('TRAIN_CLASS', TRAIN_CLASS)
        Tool.Set_Option('TRAIN_BUFFER', TRAIN_BUFFER)
        Tool.Set_Option('MODEL_LOAD', MODEL_LOAD)
        Tool.Set_Option('MODEL_SAVE', MODEL_SAVE)
        Tool.Set_Option('ANN_LAYERS', ANN_LAYERS)
        Tool.Set_Option('ANN_NEURONS', ANN_NEURONS)
        Tool.Set_Option('ANN_MAXITER', ANN_MAXITER)
        Tool.Set_Option('ANN_EPSILON', ANN_EPSILON)
        Tool.Set_Option('ANN_ACTIVATION', ANN_ACTIVATION)
        Tool.Set_Option('ANN_ACT_ALPHA', ANN_ACT_ALPHA)
        Tool.Set_Option('ANN_ACT_BETA', ANN_ACT_BETA)
        Tool.Set_Option('ANN_PROPAGATION', ANN_PROPAGATION)
        Tool.Set_Option('ANN_RP_DW0', ANN_RP_DW0)
        Tool.Set_Option('ANN_RP_DW_PLUS', ANN_RP_DW_PLUS)
        Tool.Set_Option('ANN_RP_DW_MINUS', ANN_RP_DW_MINUS)
        Tool.Set_Option('ANN_RP_DW_MIN', ANN_RP_DW_MIN)
        Tool.Set_Option('ANN_RP_DW_MAX', ANN_RP_DW_MAX)
        Tool.Set_Option('ANN_BP_DW', ANN_BP_DW)
        Tool.Set_Option('ANN_BP_MOMENT', ANN_BP_MOMENT)
        return Tool.Execute(Verbose)
    return False

def Logistic_Regression_Classification(FEATURES=None, TRAIN_SAMPLES=None, TRAIN_AREAS=None, CLASSES=None, CLASSES_LUT=None, NORMALIZE=None, GRID_SYSTEM=None, MODEL_TRAIN=None, TRAIN_CLASS=None, TRAIN_BUFFER=None, MODEL_LOAD=None, MODEL_SAVE=None, LOGR_LEARNING_RATE=None, LOGR_ITERATIONS=None, LOGR_REGULARIZATION=None, LOGR_TRAIN_METHOD=None, LOGR_MINIBATCH_SIZE=None, Verbose=2):
    '''
    Logistic Regression Classification
    ----------
    [imagery_opencv.12]\n
    Integration of the OpenCV Machine Learning library for Logistic Regression based classification of gridded features.\n
    Optimization algorithms like 'Batch Gradient Descent' and 'Mini-Batch Gradient Descent' are supported in Logistic Regression. It is important that we mention the number of iterations these optimization algorithms have to run. The number of iterations can be thought as number of steps taken and learning rate specifies if it is a long step or a short step. This and previous parameter define how fast we arrive at a possible solution.\n
    In order to compensate for overfitting regularization can be performed. (L1 or L2 norm).\n
    Logistic regression implementation provides a choice of two training methods with 'Batch Gradient Descent' or the 'Mini-Batch Gradient Descent'.\n
    Arguments
    ----------
    - FEATURES [`input grid list`] : Features
    - TRAIN_SAMPLES [`input table`] : Training Samples. Provide a class identifier in the first field followed by sample data corresponding to the input feature grids.
    - TRAIN_AREAS [`input shapes`] : Training Areas
    - CLASSES [`output grid`] : Classification
    - CLASSES_LUT [`output table`] : Look-up Table. A reference list of the grid values that have been assigned to the training classes.
    - NORMALIZE [`boolean`] : Normalize. Default: 0
    - GRID_SYSTEM [`grid system`] : Grid System
    - MODEL_TRAIN [`choice`] : Training. Available Choices: [0] training areas [1] training samples [2] load from file Default: 0
    - TRAIN_CLASS [`table field`] : Class Identifier
    - TRAIN_BUFFER [`floating point number`] : Buffer Size. Minimum: 0.000000 Default: 1.000000 For non-polygon type training areas, creates a buffer with a diameter of specified size.
    - MODEL_LOAD [`file path`] : Load Model. Use a model previously stored to file.
    - MODEL_SAVE [`file path`] : Save Model. Stores model to file to be used for subsequent classifications instead of training areas.
    - LOGR_LEARNING_RATE [`floating point number`] : Learning Rate. Minimum: 0.000000 Default: 1.000000 The learning rate determines how fast we approach the solution.
    - LOGR_ITERATIONS [`integer number`] : Number of Iterations. Minimum: 1 Default: 300
    - LOGR_REGULARIZATION [`choice`] : Regularization. Available Choices: [0] disabled [1] L1 norm [2] L2 norm Default: 0
    - LOGR_TRAIN_METHOD [`choice`] : Training Method. Available Choices: [0] Batch Gradient Descent [1] Mini-Batch Gradient Descent Default: 0
    - LOGR_MINIBATCH_SIZE [`integer number`] : Mini-Batch Size. Minimum: 1 Default: 1

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('imagery_opencv', '12', 'Logistic Regression Classification')
    if Tool.is_Okay():
        Tool.Set_Input ('FEATURES', FEATURES)
        Tool.Set_Input ('TRAIN_SAMPLES', TRAIN_SAMPLES)
        Tool.Set_Input ('TRAIN_AREAS', TRAIN_AREAS)
        Tool.Set_Output('CLASSES', CLASSES)
        Tool.Set_Output('CLASSES_LUT', CLASSES_LUT)
        Tool.Set_Option('NORMALIZE', NORMALIZE)
        Tool.Set_Option('GRID_SYSTEM', GRID_SYSTEM)
        Tool.Set_Option('MODEL_TRAIN', MODEL_TRAIN)
        Tool.Set_Option('TRAIN_CLASS', TRAIN_CLASS)
        Tool.Set_Option('TRAIN_BUFFER', TRAIN_BUFFER)
        Tool.Set_Option('MODEL_LOAD', MODEL_LOAD)
        Tool.Set_Option('MODEL_SAVE', MODEL_SAVE)
        Tool.Set_Option('LOGR_LEARNING_RATE', LOGR_LEARNING_RATE)
        Tool.Set_Option('LOGR_ITERATIONS', LOGR_ITERATIONS)
        Tool.Set_Option('LOGR_REGULARIZATION', LOGR_REGULARIZATION)
        Tool.Set_Option('LOGR_TRAIN_METHOD', LOGR_TRAIN_METHOD)
        Tool.Set_Option('LOGR_MINIBATCH_SIZE', LOGR_MINIBATCH_SIZE)
        return Tool.Execute(Verbose)
    return False

def run_tool_imagery_opencv_12(FEATURES=None, TRAIN_SAMPLES=None, TRAIN_AREAS=None, CLASSES=None, CLASSES_LUT=None, NORMALIZE=None, GRID_SYSTEM=None, MODEL_TRAIN=None, TRAIN_CLASS=None, TRAIN_BUFFER=None, MODEL_LOAD=None, MODEL_SAVE=None, LOGR_LEARNING_RATE=None, LOGR_ITERATIONS=None, LOGR_REGULARIZATION=None, LOGR_TRAIN_METHOD=None, LOGR_MINIBATCH_SIZE=None, Verbose=2):
    '''
    Logistic Regression Classification
    ----------
    [imagery_opencv.12]\n
    Integration of the OpenCV Machine Learning library for Logistic Regression based classification of gridded features.\n
    Optimization algorithms like 'Batch Gradient Descent' and 'Mini-Batch Gradient Descent' are supported in Logistic Regression. It is important that we mention the number of iterations these optimization algorithms have to run. The number of iterations can be thought as number of steps taken and learning rate specifies if it is a long step or a short step. This and previous parameter define how fast we arrive at a possible solution.\n
    In order to compensate for overfitting regularization can be performed. (L1 or L2 norm).\n
    Logistic regression implementation provides a choice of two training methods with 'Batch Gradient Descent' or the 'Mini-Batch Gradient Descent'.\n
    Arguments
    ----------
    - FEATURES [`input grid list`] : Features
    - TRAIN_SAMPLES [`input table`] : Training Samples. Provide a class identifier in the first field followed by sample data corresponding to the input feature grids.
    - TRAIN_AREAS [`input shapes`] : Training Areas
    - CLASSES [`output grid`] : Classification
    - CLASSES_LUT [`output table`] : Look-up Table. A reference list of the grid values that have been assigned to the training classes.
    - NORMALIZE [`boolean`] : Normalize. Default: 0
    - GRID_SYSTEM [`grid system`] : Grid System
    - MODEL_TRAIN [`choice`] : Training. Available Choices: [0] training areas [1] training samples [2] load from file Default: 0
    - TRAIN_CLASS [`table field`] : Class Identifier
    - TRAIN_BUFFER [`floating point number`] : Buffer Size. Minimum: 0.000000 Default: 1.000000 For non-polygon type training areas, creates a buffer with a diameter of specified size.
    - MODEL_LOAD [`file path`] : Load Model. Use a model previously stored to file.
    - MODEL_SAVE [`file path`] : Save Model. Stores model to file to be used for subsequent classifications instead of training areas.
    - LOGR_LEARNING_RATE [`floating point number`] : Learning Rate. Minimum: 0.000000 Default: 1.000000 The learning rate determines how fast we approach the solution.
    - LOGR_ITERATIONS [`integer number`] : Number of Iterations. Minimum: 1 Default: 300
    - LOGR_REGULARIZATION [`choice`] : Regularization. Available Choices: [0] disabled [1] L1 norm [2] L2 norm Default: 0
    - LOGR_TRAIN_METHOD [`choice`] : Training Method. Available Choices: [0] Batch Gradient Descent [1] Mini-Batch Gradient Descent Default: 0
    - LOGR_MINIBATCH_SIZE [`integer number`] : Mini-Batch Size. Minimum: 1 Default: 1

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('imagery_opencv', '12', 'Logistic Regression Classification')
    if Tool.is_Okay():
        Tool.Set_Input ('FEATURES', FEATURES)
        Tool.Set_Input ('TRAIN_SAMPLES', TRAIN_SAMPLES)
        Tool.Set_Input ('TRAIN_AREAS', TRAIN_AREAS)
        Tool.Set_Output('CLASSES', CLASSES)
        Tool.Set_Output('CLASSES_LUT', CLASSES_LUT)
        Tool.Set_Option('NORMALIZE', NORMALIZE)
        Tool.Set_Option('GRID_SYSTEM', GRID_SYSTEM)
        Tool.Set_Option('MODEL_TRAIN', MODEL_TRAIN)
        Tool.Set_Option('TRAIN_CLASS', TRAIN_CLASS)
        Tool.Set_Option('TRAIN_BUFFER', TRAIN_BUFFER)
        Tool.Set_Option('MODEL_LOAD', MODEL_LOAD)
        Tool.Set_Option('MODEL_SAVE', MODEL_SAVE)
        Tool.Set_Option('LOGR_LEARNING_RATE', LOGR_LEARNING_RATE)
        Tool.Set_Option('LOGR_ITERATIONS', LOGR_ITERATIONS)
        Tool.Set_Option('LOGR_REGULARIZATION', LOGR_REGULARIZATION)
        Tool.Set_Option('LOGR_TRAIN_METHOD', LOGR_TRAIN_METHOD)
        Tool.Set_Option('LOGR_MINIBATCH_SIZE', LOGR_MINIBATCH_SIZE)
        return Tool.Execute(Verbose)
    return False

def Inverse_Fourier_Transformation(REAL=None, IMAG=None, GRID=None, CENTERED=None, RESTORE=None, Verbose=2):
    '''
    Inverse Fourier Transformation
    ----------
    [imagery_opencv.13]\n
    Inverse discrete Fourier transformation.\n
    Arguments
    ----------
    - REAL [`input grid`] : Real
    - IMAG [`input grid`] : Imaginary
    - GRID [`output grid`] : Grid
    - CENTERED [`boolean`] : Centered. Default: 1
    - RESTORE [`boolean`] : Restore. Default: 1 Restore original grid information from metadata.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('imagery_opencv', '13', 'Inverse Fourier Transformation')
    if Tool.is_Okay():
        Tool.Set_Input ('REAL', REAL)
        Tool.Set_Input ('IMAG', IMAG)
        Tool.Set_Output('GRID', GRID)
        Tool.Set_Option('CENTERED', CENTERED)
        Tool.Set_Option('RESTORE', RESTORE)
        return Tool.Execute(Verbose)
    return False

def run_tool_imagery_opencv_13(REAL=None, IMAG=None, GRID=None, CENTERED=None, RESTORE=None, Verbose=2):
    '''
    Inverse Fourier Transformation
    ----------
    [imagery_opencv.13]\n
    Inverse discrete Fourier transformation.\n
    Arguments
    ----------
    - REAL [`input grid`] : Real
    - IMAG [`input grid`] : Imaginary
    - GRID [`output grid`] : Grid
    - CENTERED [`boolean`] : Centered. Default: 1
    - RESTORE [`boolean`] : Restore. Default: 1 Restore original grid information from metadata.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('imagery_opencv', '13', 'Inverse Fourier Transformation')
    if Tool.is_Okay():
        Tool.Set_Input ('REAL', REAL)
        Tool.Set_Input ('IMAG', IMAG)
        Tool.Set_Output('GRID', GRID)
        Tool.Set_Option('CENTERED', CENTERED)
        Tool.Set_Option('RESTORE', RESTORE)
        return Tool.Execute(Verbose)
    return False

def Frequency_Domain_Filter(GRID=None, FILTERED=None, FILTER=None, INVERSE=None, RANGE=None, POWER=None, SCALE=None, Verbose=2):
    '''
    Frequency Domain Filter
    ----------
    [imagery_opencv.14]\n
    The frequency domain filter works on the discrete Fourier transformation.\n
    Arguments
    ----------
    - GRID [`input grid`] : Grid
    - FILTERED [`output grid`] : Filtered Grid
    - FILTER [`choice`] : Filter. Available Choices: [0] range [1] power of distance [2] Hann window [3] Gaussian Default: 2
    - INVERSE [`boolean`] : Inverse. Default: 0
    - RANGE [`value range`] : Range
    - POWER [`floating point number`] : Power. Default: 0.500000
    - SCALE [`floating point number`] : Band Width. Minimum: 0.000000 Default: 2.000000

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('imagery_opencv', '14', 'Frequency Domain Filter')
    if Tool.is_Okay():
        Tool.Set_Input ('GRID', GRID)
        Tool.Set_Output('FILTERED', FILTERED)
        Tool.Set_Option('FILTER', FILTER)
        Tool.Set_Option('INVERSE', INVERSE)
        Tool.Set_Option('RANGE', RANGE)
        Tool.Set_Option('POWER', POWER)
        Tool.Set_Option('SCALE', SCALE)
        return Tool.Execute(Verbose)
    return False

def run_tool_imagery_opencv_14(GRID=None, FILTERED=None, FILTER=None, INVERSE=None, RANGE=None, POWER=None, SCALE=None, Verbose=2):
    '''
    Frequency Domain Filter
    ----------
    [imagery_opencv.14]\n
    The frequency domain filter works on the discrete Fourier transformation.\n
    Arguments
    ----------
    - GRID [`input grid`] : Grid
    - FILTERED [`output grid`] : Filtered Grid
    - FILTER [`choice`] : Filter. Available Choices: [0] range [1] power of distance [2] Hann window [3] Gaussian Default: 2
    - INVERSE [`boolean`] : Inverse. Default: 0
    - RANGE [`value range`] : Range
    - POWER [`floating point number`] : Power. Default: 0.500000
    - SCALE [`floating point number`] : Band Width. Minimum: 0.000000 Default: 2.000000

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('imagery_opencv', '14', 'Frequency Domain Filter')
    if Tool.is_Okay():
        Tool.Set_Input ('GRID', GRID)
        Tool.Set_Output('FILTERED', FILTERED)
        Tool.Set_Option('FILTER', FILTER)
        Tool.Set_Option('INVERSE', INVERSE)
        Tool.Set_Option('RANGE', RANGE)
        Tool.Set_Option('POWER', POWER)
        Tool.Set_Option('SCALE', SCALE)
        return Tool.Execute(Verbose)
    return False

def Canny_Edge_Detection(GRID=None, EDGES=None, EDGE_LINES=None, THRESHOLD=None, RATIO=None, KERNEL_SIZE=None, L2GRADIENT=None, Verbose=2):
    '''
    Canny Edge Detection
    ----------
    [imagery_opencv.15]\n
    Canny edge detection.\n
    Arguments
    ----------
    - GRID [`input grid`] : Grid
    - EDGES [`output grid`] : Edges
    - EDGE_LINES [`output shapes`] : Edge Lines
    - THRESHOLD [`floating point number`] : Threshold. Minimum: 0.000000 Default: 1.000000
    - RATIO [`floating point number`] : Ratio. Minimum: 1.000000 Default: 3.000000
    - KERNEL_SIZE [`integer number`] : Kernel Size. Minimum: 1 Maximum: 3 Default: 1
    - L2GRADIENT [`boolean`] : L2 Gradient. Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('imagery_opencv', '15', 'Canny Edge Detection')
    if Tool.is_Okay():
        Tool.Set_Input ('GRID', GRID)
        Tool.Set_Output('EDGES', EDGES)
        Tool.Set_Output('EDGE_LINES', EDGE_LINES)
        Tool.Set_Option('THRESHOLD', THRESHOLD)
        Tool.Set_Option('RATIO', RATIO)
        Tool.Set_Option('KERNEL_SIZE', KERNEL_SIZE)
        Tool.Set_Option('L2GRADIENT', L2GRADIENT)
        return Tool.Execute(Verbose)
    return False

def run_tool_imagery_opencv_15(GRID=None, EDGES=None, EDGE_LINES=None, THRESHOLD=None, RATIO=None, KERNEL_SIZE=None, L2GRADIENT=None, Verbose=2):
    '''
    Canny Edge Detection
    ----------
    [imagery_opencv.15]\n
    Canny edge detection.\n
    Arguments
    ----------
    - GRID [`input grid`] : Grid
    - EDGES [`output grid`] : Edges
    - EDGE_LINES [`output shapes`] : Edge Lines
    - THRESHOLD [`floating point number`] : Threshold. Minimum: 0.000000 Default: 1.000000
    - RATIO [`floating point number`] : Ratio. Minimum: 1.000000 Default: 3.000000
    - KERNEL_SIZE [`integer number`] : Kernel Size. Minimum: 1 Maximum: 3 Default: 1
    - L2GRADIENT [`boolean`] : L2 Gradient. Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('imagery_opencv', '15', 'Canny Edge Detection')
    if Tool.is_Okay():
        Tool.Set_Input ('GRID', GRID)
        Tool.Set_Output('EDGES', EDGES)
        Tool.Set_Output('EDGE_LINES', EDGE_LINES)
        Tool.Set_Option('THRESHOLD', THRESHOLD)
        Tool.Set_Option('RATIO', RATIO)
        Tool.Set_Option('KERNEL_SIZE', KERNEL_SIZE)
        Tool.Set_Option('L2GRADIENT', L2GRADIENT)
        return Tool.Execute(Verbose)
    return False

def Hough_Circle_Transformation(GRID=None, CIRCLES=None, UNIT=None, RADIUS=None, MIN_DIST=None, METHOD=None, RESOLUTION=None, Verbose=2):
    '''
    Hough Circle Transformation
    ----------
    [imagery_opencv.16]\n
    Hough Circle Transformation.\n
    Arguments
    ----------
    - GRID [`input grid`] : Grid
    - CIRCLES [`output shapes`] : Circles
    - UNIT [`choice`] : Unit. Available Choices: [0] cells [1] map units Default: 0
    - RADIUS [`value range`] : Radius
    - MIN_DIST [`floating point number`] : Minimum Distance. Minimum: 0.000000 Default: 3.000000 Minimum distance between the centers of the detected circles. If the parameter is too small, multiple neighbor circles may be falsely detected in addition to a true one. If it is too large, some circles may be missed.
    - METHOD [`choice`] : Method. Available Choices: [0] Hough gradient [1] Hough gradient (alternative) Default: 0
    - RESOLUTION [`floating point number`] : Accumulator Resolution. Minimum: 1.000000 Default: 3.000000 Inverse ratio of the accumulator resolution to the image resolution. if set to 1, the accumulator has the same resolution as the input image. If set to 2, the accumulator has half as big width and height. For 'Hough gradient (alternative)' the recommended value is 1.5, unless some small very circles need to be detected.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('imagery_opencv', '16', 'Hough Circle Transformation')
    if Tool.is_Okay():
        Tool.Set_Input ('GRID', GRID)
        Tool.Set_Output('CIRCLES', CIRCLES)
        Tool.Set_Option('UNIT', UNIT)
        Tool.Set_Option('RADIUS', RADIUS)
        Tool.Set_Option('MIN_DIST', MIN_DIST)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('RESOLUTION', RESOLUTION)
        return Tool.Execute(Verbose)
    return False

def run_tool_imagery_opencv_16(GRID=None, CIRCLES=None, UNIT=None, RADIUS=None, MIN_DIST=None, METHOD=None, RESOLUTION=None, Verbose=2):
    '''
    Hough Circle Transformation
    ----------
    [imagery_opencv.16]\n
    Hough Circle Transformation.\n
    Arguments
    ----------
    - GRID [`input grid`] : Grid
    - CIRCLES [`output shapes`] : Circles
    - UNIT [`choice`] : Unit. Available Choices: [0] cells [1] map units Default: 0
    - RADIUS [`value range`] : Radius
    - MIN_DIST [`floating point number`] : Minimum Distance. Minimum: 0.000000 Default: 3.000000 Minimum distance between the centers of the detected circles. If the parameter is too small, multiple neighbor circles may be falsely detected in addition to a true one. If it is too large, some circles may be missed.
    - METHOD [`choice`] : Method. Available Choices: [0] Hough gradient [1] Hough gradient (alternative) Default: 0
    - RESOLUTION [`floating point number`] : Accumulator Resolution. Minimum: 1.000000 Default: 3.000000 Inverse ratio of the accumulator resolution to the image resolution. if set to 1, the accumulator has the same resolution as the input image. If set to 2, the accumulator has half as big width and height. For 'Hough gradient (alternative)' the recommended value is 1.5, unless some small very circles need to be detected.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('imagery_opencv', '16', 'Hough Circle Transformation')
    if Tool.is_Okay():
        Tool.Set_Input ('GRID', GRID)
        Tool.Set_Output('CIRCLES', CIRCLES)
        Tool.Set_Option('UNIT', UNIT)
        Tool.Set_Option('RADIUS', RADIUS)
        Tool.Set_Option('MIN_DIST', MIN_DIST)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('RESOLUTION', RESOLUTION)
        return Tool.Execute(Verbose)
    return False

def Watershed_Image_Segmentation(BAND_1=None, SEED_GRID=None, BAND_2=None, BAND_3=None, SEGMENTS=None, POLYGONS=None, SEEDS=None, Verbose=2):
    '''
    Watershed Image Segmentation
    ----------
    [imagery_opencv.17]\n
    Watershed Segmentation.\n
    Arguments
    ----------
    - BAND_1 [`input grid`] : Band 1
    - SEED_GRID [`input grid`] : Seeds
    - BAND_2 [`optional input grid`] : Band 2
    - BAND_3 [`optional input grid`] : Band 3
    - SEGMENTS [`output grid`] : Segments
    - POLYGONS [`output shapes`] : Polygons
    - SEEDS [`choice`] : Get Seeds from.... Available Choices: [0] peaks [1] pits [2] grid Default: 0 Either local maxima (peaks) or minima (pits) of band 1, or provided seeds grid (enumerated values greater zero or not no-data).

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('imagery_opencv', '17', 'Watershed Image Segmentation')
    if Tool.is_Okay():
        Tool.Set_Input ('BAND_1', BAND_1)
        Tool.Set_Input ('SEED_GRID', SEED_GRID)
        Tool.Set_Input ('BAND_2', BAND_2)
        Tool.Set_Input ('BAND_3', BAND_3)
        Tool.Set_Output('SEGMENTS', SEGMENTS)
        Tool.Set_Output('POLYGONS', POLYGONS)
        Tool.Set_Option('SEEDS', SEEDS)
        return Tool.Execute(Verbose)
    return False

def run_tool_imagery_opencv_17(BAND_1=None, SEED_GRID=None, BAND_2=None, BAND_3=None, SEGMENTS=None, POLYGONS=None, SEEDS=None, Verbose=2):
    '''
    Watershed Image Segmentation
    ----------
    [imagery_opencv.17]\n
    Watershed Segmentation.\n
    Arguments
    ----------
    - BAND_1 [`input grid`] : Band 1
    - SEED_GRID [`input grid`] : Seeds
    - BAND_2 [`optional input grid`] : Band 2
    - BAND_3 [`optional input grid`] : Band 3
    - SEGMENTS [`output grid`] : Segments
    - POLYGONS [`output shapes`] : Polygons
    - SEEDS [`choice`] : Get Seeds from.... Available Choices: [0] peaks [1] pits [2] grid Default: 0 Either local maxima (peaks) or minima (pits) of band 1, or provided seeds grid (enumerated values greater zero or not no-data).

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('imagery_opencv', '17', 'Watershed Image Segmentation')
    if Tool.is_Okay():
        Tool.Set_Input ('BAND_1', BAND_1)
        Tool.Set_Input ('SEED_GRID', SEED_GRID)
        Tool.Set_Input ('BAND_2', BAND_2)
        Tool.Set_Input ('BAND_3', BAND_3)
        Tool.Set_Output('SEGMENTS', SEGMENTS)
        Tool.Set_Output('POLYGONS', POLYGONS)
        Tool.Set_Option('SEEDS', SEEDS)
        return Tool.Execute(Verbose)
    return False

