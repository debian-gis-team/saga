#! /usr/bin/env python

'''
Python Interface to SAGA Tools Library
----------
- Category : Import/Export
- Name     : Riegl RDB
- ID       : io_riegl_rdb

Description
----------
Tools to import RDB files.
'''

from PySAGA.helper import Tool_Wrapper

def Import_RDB2_Files(POINTS=None, FILES=None, id=None, ts=None, AMPL=None, REFL=None, DEV=None, pw=None, T_IDX=None, T_CNT=None, CLASS=None, RGBA=None, EPSG=None, Verbose=2):
    '''
    Import RDB2 Files
    ----------
    [io_riegl_rdb.0]\n
    Import a pointcloud from Riegl RDB 2 format. This is a work in progress.\n
    Arguments
    ----------
    - POINTS [`output point cloud list`] : Point Clouds
    - FILES [`file path`] : Input Files
    - id [`boolean`] : Point Source ID. Default: 0
    - ts [`boolean`] : Time Stamp. Default: 0
    - AMPL [`boolean`] : Amplitude. Default: 0
    - REFL [`boolean`] : Reflectance. Default: 0
    - DEV [`boolean`] : Deviation. Default: 0
    - pw [`boolean`] : Pulse Width. Default: 0
    - T_IDX [`boolean`] : Target Index. Default: 0
    - T_CNT [`boolean`] : Target Count. Default: 0
    - CLASS [`boolean`] : Class. Default: 0
    - RGBA [`boolean`] : RGP Color. Default: 0
    - EPSG [`boolean`] : Load EPSG Geo-Tag. Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('io_riegl_rdb', '0', 'Import RDB2 Files')
    if Tool.is_Okay():
        Tool.Set_Output('POINTS', POINTS)
        Tool.Set_Option('FILES', FILES)
        Tool.Set_Option('id', id)
        Tool.Set_Option('ts', ts)
        Tool.Set_Option('ampl', AMPL)
        Tool.Set_Option('refl', REFL)
        Tool.Set_Option('dev', DEV)
        Tool.Set_Option('pw', pw)
        Tool.Set_Option('t_idx', T_IDX)
        Tool.Set_Option('t_cnt', T_CNT)
        Tool.Set_Option('class', CLASS)
        Tool.Set_Option('rgba', RGBA)
        Tool.Set_Option('epsg', EPSG)
        return Tool.Execute(Verbose)
    return False

def run_tool_io_riegl_rdb_0(POINTS=None, FILES=None, id=None, ts=None, AMPL=None, REFL=None, DEV=None, pw=None, T_IDX=None, T_CNT=None, CLASS=None, RGBA=None, EPSG=None, Verbose=2):
    '''
    Import RDB2 Files
    ----------
    [io_riegl_rdb.0]\n
    Import a pointcloud from Riegl RDB 2 format. This is a work in progress.\n
    Arguments
    ----------
    - POINTS [`output point cloud list`] : Point Clouds
    - FILES [`file path`] : Input Files
    - id [`boolean`] : Point Source ID. Default: 0
    - ts [`boolean`] : Time Stamp. Default: 0
    - AMPL [`boolean`] : Amplitude. Default: 0
    - REFL [`boolean`] : Reflectance. Default: 0
    - DEV [`boolean`] : Deviation. Default: 0
    - pw [`boolean`] : Pulse Width. Default: 0
    - T_IDX [`boolean`] : Target Index. Default: 0
    - T_CNT [`boolean`] : Target Count. Default: 0
    - CLASS [`boolean`] : Class. Default: 0
    - RGBA [`boolean`] : RGP Color. Default: 0
    - EPSG [`boolean`] : Load EPSG Geo-Tag. Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('io_riegl_rdb', '0', 'Import RDB2 Files')
    if Tool.is_Okay():
        Tool.Set_Output('POINTS', POINTS)
        Tool.Set_Option('FILES', FILES)
        Tool.Set_Option('id', id)
        Tool.Set_Option('ts', ts)
        Tool.Set_Option('ampl', AMPL)
        Tool.Set_Option('refl', REFL)
        Tool.Set_Option('dev', DEV)
        Tool.Set_Option('pw', pw)
        Tool.Set_Option('t_idx', T_IDX)
        Tool.Set_Option('t_cnt', T_CNT)
        Tool.Set_Option('class', CLASS)
        Tool.Set_Option('rgba', RGBA)
        Tool.Set_Option('epsg', EPSG)
        return Tool.Execute(Verbose)
    return False

def Info_about_RDB2_Files(FILES=None, Verbose=2):
    '''
    Info about RDB2 Files
    ----------
    [io_riegl_rdb.1]\n
    Print info about a Riegl RDB 2 file. This is a work in progress.\n
    Arguments
    ----------
    - FILES [`file path`] : Input Files

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('io_riegl_rdb', '1', 'Info about RDB2 Files')
    if Tool.is_Okay():
        Tool.Set_Option('FILES', FILES)
        return Tool.Execute(Verbose)
    return False

def run_tool_io_riegl_rdb_1(FILES=None, Verbose=2):
    '''
    Info about RDB2 Files
    ----------
    [io_riegl_rdb.1]\n
    Print info about a Riegl RDB 2 file. This is a work in progress.\n
    Arguments
    ----------
    - FILES [`file path`] : Input Files

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('io_riegl_rdb', '1', 'Info about RDB2 Files')
    if Tool.is_Okay():
        Tool.Set_Option('FILES', FILES)
        return Tool.Execute(Verbose)
    return False

