#! /usr/bin/env python

'''
Python Interface to SAGA Tools Library
----------
- Category : Terrain Analysis
- Name     : Slope Stability
- ID       : ta_slope_stability

Description
----------
Tools for slope stability analyses. Developed by Andreas G&uuml;nther, BGR, B2.2
'''

from PySAGA.helper import Tool_Wrapper

def SAFETYFACTOR(A=None, BMIN=None, BMAX=None, CMIN=None, CMAX=None, DMIN=None, DMAX=None, EMIN=None, EMAX=None, FMIN=None, FMAX=None, G=None, H=None, FBMIN=None, FBMAX=None, FCMIN=None, FCMAX=None, FDMIN=None, FDMAX=None, FEMIN=None, FEMAX=None, FFMIN=None, FFMAX=None, fI=None, Verbose=2):
    '''
    SAFETYFACTOR
    ----------
    [ta_slope_stability.0]\n
    This tool computes a slope stability (expressed as a factor-of-safety) raster according to the traditional infinite slope model theory (see cf Selby, 1993) The resulting raster represents the ratio of resisting forces/driving forces (fs) on a potential shear plane with fs lesser 1 unstable, fs greater 1 stable. Except for a slope raster (in radians), all input variables can be specified either globally or distributed (through grids). The tool creates a continuous fs raster (values above 10 are truncated), and a binary stability grid with nodata = stable, 1 = unstable (optional).\n
    Arguments
    ----------
    - A [`input grid`] : Slope grid (rad). A slope angle grid (in radÃ­ans)
    - BMIN [`optional input grid`] : Min thickness grid (m) . A grid representing minimum cell layer thicknesses to potential shear plane (in meters)
    - BMAX [`optional input grid`] : Max thickness grid (m) . A grid representing maximum cell layer thicknesses to potential shear plane (in meters)
    - CMIN [`optional input grid`] : Min saturation grid (-) . A grid representing minimum cell relative water saturation of layer (dimensionless)
    - CMAX [`optional input grid`] : Max saturation grid (-) . A grid representing maximum cell relative water saturation of layer (dimensionless)
    - DMIN [`optional input grid`] : Min friction grid (degree) . A grid representing minimum cell frictional shear strength of layer (in degrees)
    - DMAX [`optional input grid`] : Max friction grid (degree) . A grid representing maximum cell frictional shear strength of layer (in degrees)
    - EMIN [`optional input grid`] : Min density grid (g/cm3). A grid representing minimum cell bulk density of layer (in grams per cubiccentimeters)
    - EMAX [`optional input grid`] : Max density grid (g/cm3). A grid representing maximum cell bulk density of layer (in grams per cubiccentimeters)
    - FMIN [`optional input grid`] : Min cohesion grid (MPa) . A grid representing minimum cell layer cohesion (in Megapascals)
    - FMAX [`optional input grid`] : Max cohesion grid (MPa) . A grid representing maximum cell layer cohesion (in Megapascals)
    - G [`output grid`] : FS values. Resulting factor-of-safety (-) grid
    - H [`output grid`] : FS classes. Resulting stability (0/1) grid
    - FBMIN [`floating point number`] : Min global thickness (m). Default: 1.000000 Constant value if no raster set
    - FBMAX [`floating point number`] : Max global thickness (m). Default: 1.000000 Constant value if no raster set
    - FCMIN [`floating point number`] : Min global saturation (-). Default: 0.000000 Constant value if no raster set
    - FCMAX [`floating point number`] : Max global saturation (-). Default: 0.000000 Constant value if no raster set
    - FDMIN [`floating point number`] : Min global friction (degree). Default: 33.000000 Constant value if no raster set
    - FDMAX [`floating point number`] : Max global friction (degree). Default: 33.000000 Constant value if no raster set
    - FEMIN [`floating point number`] : Min global density (g/cm3). Default: 1.600000 Constant value if no raster set
    - FEMAX [`floating point number`] : Max global density (g/cm3). Default: 1.600000 Constant value if no raster set
    - FFMIN [`floating point number`] : Min global cohesion (MPa). Default: 0.000000 Constant value if no raster set
    - FFMAX [`floating point number`] : Max global cohesion (MPa). Default: 0.000000 Constant value if no raster set
    - fI [`integer number`] : Parameter sampling runs. Default: 1 Number of sampling cycles

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_slope_stability', '0', 'SAFETYFACTOR')
    if Tool.is_Okay():
        Tool.Set_Input ('A', A)
        Tool.Set_Input ('Bmin', BMIN)
        Tool.Set_Input ('Bmax', BMAX)
        Tool.Set_Input ('Cmin', CMIN)
        Tool.Set_Input ('Cmax', CMAX)
        Tool.Set_Input ('Dmin', DMIN)
        Tool.Set_Input ('Dmax', DMAX)
        Tool.Set_Input ('Emin', EMIN)
        Tool.Set_Input ('Emax', EMAX)
        Tool.Set_Input ('Fmin', FMIN)
        Tool.Set_Input ('Fmax', FMAX)
        Tool.Set_Output('G', G)
        Tool.Set_Output('H', H)
        Tool.Set_Option('fBmin', FBMIN)
        Tool.Set_Option('fBmax', FBMAX)
        Tool.Set_Option('fCmin', FCMIN)
        Tool.Set_Option('fCmax', FCMAX)
        Tool.Set_Option('fDmin', FDMIN)
        Tool.Set_Option('fDmax', FDMAX)
        Tool.Set_Option('fEmin', FEMIN)
        Tool.Set_Option('fEmax', FEMAX)
        Tool.Set_Option('fFmin', FFMIN)
        Tool.Set_Option('fFmax', FFMAX)
        Tool.Set_Option('fI', fI)
        return Tool.Execute(Verbose)
    return False

def run_tool_ta_slope_stability_0(A=None, BMIN=None, BMAX=None, CMIN=None, CMAX=None, DMIN=None, DMAX=None, EMIN=None, EMAX=None, FMIN=None, FMAX=None, G=None, H=None, FBMIN=None, FBMAX=None, FCMIN=None, FCMAX=None, FDMIN=None, FDMAX=None, FEMIN=None, FEMAX=None, FFMIN=None, FFMAX=None, fI=None, Verbose=2):
    '''
    SAFETYFACTOR
    ----------
    [ta_slope_stability.0]\n
    This tool computes a slope stability (expressed as a factor-of-safety) raster according to the traditional infinite slope model theory (see cf Selby, 1993) The resulting raster represents the ratio of resisting forces/driving forces (fs) on a potential shear plane with fs lesser 1 unstable, fs greater 1 stable. Except for a slope raster (in radians), all input variables can be specified either globally or distributed (through grids). The tool creates a continuous fs raster (values above 10 are truncated), and a binary stability grid with nodata = stable, 1 = unstable (optional).\n
    Arguments
    ----------
    - A [`input grid`] : Slope grid (rad). A slope angle grid (in radÃ­ans)
    - BMIN [`optional input grid`] : Min thickness grid (m) . A grid representing minimum cell layer thicknesses to potential shear plane (in meters)
    - BMAX [`optional input grid`] : Max thickness grid (m) . A grid representing maximum cell layer thicknesses to potential shear plane (in meters)
    - CMIN [`optional input grid`] : Min saturation grid (-) . A grid representing minimum cell relative water saturation of layer (dimensionless)
    - CMAX [`optional input grid`] : Max saturation grid (-) . A grid representing maximum cell relative water saturation of layer (dimensionless)
    - DMIN [`optional input grid`] : Min friction grid (degree) . A grid representing minimum cell frictional shear strength of layer (in degrees)
    - DMAX [`optional input grid`] : Max friction grid (degree) . A grid representing maximum cell frictional shear strength of layer (in degrees)
    - EMIN [`optional input grid`] : Min density grid (g/cm3). A grid representing minimum cell bulk density of layer (in grams per cubiccentimeters)
    - EMAX [`optional input grid`] : Max density grid (g/cm3). A grid representing maximum cell bulk density of layer (in grams per cubiccentimeters)
    - FMIN [`optional input grid`] : Min cohesion grid (MPa) . A grid representing minimum cell layer cohesion (in Megapascals)
    - FMAX [`optional input grid`] : Max cohesion grid (MPa) . A grid representing maximum cell layer cohesion (in Megapascals)
    - G [`output grid`] : FS values. Resulting factor-of-safety (-) grid
    - H [`output grid`] : FS classes. Resulting stability (0/1) grid
    - FBMIN [`floating point number`] : Min global thickness (m). Default: 1.000000 Constant value if no raster set
    - FBMAX [`floating point number`] : Max global thickness (m). Default: 1.000000 Constant value if no raster set
    - FCMIN [`floating point number`] : Min global saturation (-). Default: 0.000000 Constant value if no raster set
    - FCMAX [`floating point number`] : Max global saturation (-). Default: 0.000000 Constant value if no raster set
    - FDMIN [`floating point number`] : Min global friction (degree). Default: 33.000000 Constant value if no raster set
    - FDMAX [`floating point number`] : Max global friction (degree). Default: 33.000000 Constant value if no raster set
    - FEMIN [`floating point number`] : Min global density (g/cm3). Default: 1.600000 Constant value if no raster set
    - FEMAX [`floating point number`] : Max global density (g/cm3). Default: 1.600000 Constant value if no raster set
    - FFMIN [`floating point number`] : Min global cohesion (MPa). Default: 0.000000 Constant value if no raster set
    - FFMAX [`floating point number`] : Max global cohesion (MPa). Default: 0.000000 Constant value if no raster set
    - fI [`integer number`] : Parameter sampling runs. Default: 1 Number of sampling cycles

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_slope_stability', '0', 'SAFETYFACTOR')
    if Tool.is_Okay():
        Tool.Set_Input ('A', A)
        Tool.Set_Input ('Bmin', BMIN)
        Tool.Set_Input ('Bmax', BMAX)
        Tool.Set_Input ('Cmin', CMIN)
        Tool.Set_Input ('Cmax', CMAX)
        Tool.Set_Input ('Dmin', DMIN)
        Tool.Set_Input ('Dmax', DMAX)
        Tool.Set_Input ('Emin', EMIN)
        Tool.Set_Input ('Emax', EMAX)
        Tool.Set_Input ('Fmin', FMIN)
        Tool.Set_Input ('Fmax', FMAX)
        Tool.Set_Output('G', G)
        Tool.Set_Output('H', H)
        Tool.Set_Option('fBmin', FBMIN)
        Tool.Set_Option('fBmax', FBMAX)
        Tool.Set_Option('fCmin', FCMIN)
        Tool.Set_Option('fCmax', FCMAX)
        Tool.Set_Option('fDmin', FDMIN)
        Tool.Set_Option('fDmax', FDMAX)
        Tool.Set_Option('fEmin', FEMIN)
        Tool.Set_Option('fEmax', FEMAX)
        Tool.Set_Option('fFmin', FFMIN)
        Tool.Set_Option('fFmax', FFMAX)
        Tool.Set_Option('fI', fI)
        return Tool.Execute(Verbose)
    return False

def TOBIA(A=None, B=None, C=None, D=None, E=None, F=None, fB=None, fC=None, Verbose=2):
    '''
    TOBIA
    ----------
    [ta_slope_stability.1]\n
    This tool computes both a continuous and a categorical TOBIA (Topography Bedding Intersection Angle) Index according to Meentemeyer & Moody (2000) For computation, a slope and a aspect raster (both in radians) determining slope face orientations are required. The categorical TOBIA classifies the alignment of a geological structure to Topography into seven classes:\n
    0) Underdip slope\n
    1) Dip slope\n
    2) Overdip slope\n
    3) Steepened escarpmemt\n
    4) Normal escarpment\n
    5) Subdued escarpment\n
    6) Orthoclinal slope\n
    The continuous TOBIA index ranges from -1 to 1 (parallel orientation)\n
    The structure TOBIA should be calculated with can be set either distributed (through dip direction and dip grids, in degrees!), or globally using integers (dip and dip direction, in degrees!). The tool creates a TOBIA class integer grid, and (optionally) a continuous TOBIA index grid.\n
    Reference: [Meentemeyer R. K., Moody A. (2000). Automated mapping of conformity between topographic and geological surfaces. Computers & Geosciences, 26, 815 - 829](http://www.sciencedirect.com/science/article/pii/S009830040000011X).\n
    Arguments
    ----------
    - A [`input grid`] : Slope grid (rad). A slope angle grid (in radians)
    - B [`input grid`] : Aspect grid (rad). A aspect angle grid (in radians)
    - C [`optional input grid`] : Dip grid (degrees) . A grid representing the dip of the structure plane (in degrees)
    - D [`optional input grid`] : Dip direction grid (degrees) . A grid representing the dip direction of the structure plane (in degrees)
    - E [`output grid`] : TOBIA classes. Resulting TOBIA classes (1-7) grid
    - F [`output grid`] : TOBIA index. Resulting TOBIA index (-) grid
    - fB [`floating point number`] : Global structure dip (degrees). Default: 45.000000 Constant value if no raster set
    - fC [`floating point number`] : Global structure dip direction (degrees). Default: 90.000000 Constant value if no raster set

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_slope_stability', '1', 'TOBIA')
    if Tool.is_Okay():
        Tool.Set_Input ('A', A)
        Tool.Set_Input ('B', B)
        Tool.Set_Input ('C', C)
        Tool.Set_Input ('D', D)
        Tool.Set_Output('E', E)
        Tool.Set_Output('F', F)
        Tool.Set_Option('fB', fB)
        Tool.Set_Option('fC', fC)
        return Tool.Execute(Verbose)
    return False

def run_tool_ta_slope_stability_1(A=None, B=None, C=None, D=None, E=None, F=None, fB=None, fC=None, Verbose=2):
    '''
    TOBIA
    ----------
    [ta_slope_stability.1]\n
    This tool computes both a continuous and a categorical TOBIA (Topography Bedding Intersection Angle) Index according to Meentemeyer & Moody (2000) For computation, a slope and a aspect raster (both in radians) determining slope face orientations are required. The categorical TOBIA classifies the alignment of a geological structure to Topography into seven classes:\n
    0) Underdip slope\n
    1) Dip slope\n
    2) Overdip slope\n
    3) Steepened escarpmemt\n
    4) Normal escarpment\n
    5) Subdued escarpment\n
    6) Orthoclinal slope\n
    The continuous TOBIA index ranges from -1 to 1 (parallel orientation)\n
    The structure TOBIA should be calculated with can be set either distributed (through dip direction and dip grids, in degrees!), or globally using integers (dip and dip direction, in degrees!). The tool creates a TOBIA class integer grid, and (optionally) a continuous TOBIA index grid.\n
    Reference: [Meentemeyer R. K., Moody A. (2000). Automated mapping of conformity between topographic and geological surfaces. Computers & Geosciences, 26, 815 - 829](http://www.sciencedirect.com/science/article/pii/S009830040000011X).\n
    Arguments
    ----------
    - A [`input grid`] : Slope grid (rad). A slope angle grid (in radians)
    - B [`input grid`] : Aspect grid (rad). A aspect angle grid (in radians)
    - C [`optional input grid`] : Dip grid (degrees) . A grid representing the dip of the structure plane (in degrees)
    - D [`optional input grid`] : Dip direction grid (degrees) . A grid representing the dip direction of the structure plane (in degrees)
    - E [`output grid`] : TOBIA classes. Resulting TOBIA classes (1-7) grid
    - F [`output grid`] : TOBIA index. Resulting TOBIA index (-) grid
    - fB [`floating point number`] : Global structure dip (degrees). Default: 45.000000 Constant value if no raster set
    - fC [`floating point number`] : Global structure dip direction (degrees). Default: 90.000000 Constant value if no raster set

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_slope_stability', '1', 'TOBIA')
    if Tool.is_Okay():
        Tool.Set_Input ('A', A)
        Tool.Set_Input ('B', B)
        Tool.Set_Input ('C', C)
        Tool.Set_Input ('D', D)
        Tool.Set_Output('E', E)
        Tool.Set_Output('F', F)
        Tool.Set_Option('fB', fB)
        Tool.Set_Option('fC', fC)
        return Tool.Execute(Verbose)
    return False

def SHALSTAB(A=None, B=None, CMIN=None, CMAX=None, DMIN=None, DMAX=None, EMIN=None, EMAX=None, FMIN=None, FMAX=None, JMIN=None, JMAX=None, G=None, H=None, FCMIN=None, FCMAX=None, FDMIN=None, FDMAX=None, FEMIN=None, FEMAX=None, FFMIN=None, FFMAX=None, FJMIN=None, FJMAX=None, fK=None, Verbose=2):
    '''
    SHALSTAB
    ----------
    [ta_slope_stability.2]\n
    This tool is a realization of the SHALSTAB (Shallow Slope Stability) model from Montgomery & Dietrich (1994). The model computes grid cell critical shallow groundwater recharge values (CR in mm/day) as a measure for relative shallow slope stability, utilizing a simple model that combines a steady-state hydrologic model (a topographic wetness index) to predict groundwater pressures with an infinite slope stability model. For computation, a slope (in radians) and a catchment area (in m2) grid are required. Additionally, information on material density (g/cm3), material friction angle (&deg;), material hydraulic conductivity (m/hr), bulk cohesion (MPa) and depth to potential shear plane (m) are required that can be specified either globally or through grids. The minimum and maximum grids (or global values) specify the range of values from which each soil parameter is sampled. The number of sample runs determines how many values for each soil parameter are randomly taken from the range of values found for each grid cell. Finally, the mean value of the samples taken is calculated (per cell) and used as the soil parameter value in the stability equations. This introduces a probabilistic component in addition to the spatial variability of the soil parameters. If you want to work with static values, enter the same value for minimum and maximum and set the number of samples to one.\n
    The tool produces a continuous CR (mm/day) raster with unconditionally stable cells blanked, and unconditionally unstable cells as CR = 0. Optionally, a classified CR grid can be calculated representing seven stability classes.\n
    Arguments
    ----------
    - A [`input grid`] : Slope grid (rad). A slope angle grid (in radians)
    - B [`input grid`] : Catchment area grid (m2). A catchment area grid (in square meters)
    - CMIN [`optional input grid`] : Min Density grid (g/cm3). A grid representing minimum material density (in g/cm3)
    - CMAX [`optional input grid`] : Max Density grid (g/cm3). A grid representing maximum material density (in g/cm3)
    - DMIN [`optional input grid`] : Min Hydraulic conductivity grid (m/hr) . A grid representing minimum material hydraulic conductivity (in m/hr)
    - DMAX [`optional input grid`] : Max Hydraulic conductivity grid (m/hr) . A grid representing maximum material hydraulic conductivity (in m/hr)
    - EMIN [`optional input grid`] : Min Thickness grid (m). A grid representing minimum material thickness (in m)
    - EMAX [`optional input grid`] : Max Thickness grid (m). A grid representing maximum material thickness (in m)
    - FMIN [`optional input grid`] : Min Friction angle grid (degree) . A grid representing minimum material friction angle (in degrees)
    - FMAX [`optional input grid`] : Max Friction angle grid (degree) . A grid representing maximum material friction angle (in degrees)
    - JMIN [`optional input grid`] : Min Bulk cohesion grid (MPa) . A grid representing minimum bulk cohesion
    - JMAX [`optional input grid`] : Max Bulk cohesion grid (MPa) . A grid representing maximum bulk cohesion
    - G [`output grid`] : CR values. Resulting critical recharge (m/day) grid
    - H [`output grid`] : CR classes. Classified critical recharge (-) grid
    - FCMIN [`floating point number`] : Global minimum density (g/cm3). Default: 1.600000 Constant value if no raster set
    - FCMAX [`floating point number`] : Global maximum density (g/cm3). Default: 1.600000 Constant value if no raster set
    - FDMIN [`floating point number`] : Global minimum conductivity (m/hr). Default: 2.700000 Constant value if no raster set
    - FDMAX [`floating point number`] : Global maximum conductivity (m/hr). Default: 2.700000 Constant value if no raster set
    - FEMIN [`floating point number`] : Global minimum thickness (m). Default: 1.000000 Constant value if no raster set
    - FEMAX [`floating point number`] : Global maximum thickness (m). Default: 1.000000 Constant value if no raster set
    - FFMIN [`floating point number`] : Global minimum friction angle (degree). Default: 33.000000 Constant value if no raster set
    - FFMAX [`floating point number`] : Global maximum friction angle (degree). Default: 33.000000 Constant value if no raster set
    - FJMIN [`floating point number`] : Global minimum bulk cohesion (MPa). Default: 0.000000 Constant value if no raster set
    - FJMAX [`floating point number`] : Global maximum bulk cohesion (MPa). Default: 0.000000 Constant value if no raster set
    - fK [`integer number`] : Number of sampling runs. Default: 1 Number of sampling cycles

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_slope_stability', '2', 'SHALSTAB')
    if Tool.is_Okay():
        Tool.Set_Input ('A', A)
        Tool.Set_Input ('B', B)
        Tool.Set_Input ('Cmin', CMIN)
        Tool.Set_Input ('Cmax', CMAX)
        Tool.Set_Input ('Dmin', DMIN)
        Tool.Set_Input ('Dmax', DMAX)
        Tool.Set_Input ('Emin', EMIN)
        Tool.Set_Input ('Emax', EMAX)
        Tool.Set_Input ('Fmin', FMIN)
        Tool.Set_Input ('Fmax', FMAX)
        Tool.Set_Input ('Jmin', JMIN)
        Tool.Set_Input ('Jmax', JMAX)
        Tool.Set_Output('G', G)
        Tool.Set_Output('H', H)
        Tool.Set_Option('fCmin', FCMIN)
        Tool.Set_Option('fCmax', FCMAX)
        Tool.Set_Option('fDmin', FDMIN)
        Tool.Set_Option('fDmax', FDMAX)
        Tool.Set_Option('fEmin', FEMIN)
        Tool.Set_Option('fEmax', FEMAX)
        Tool.Set_Option('fFmin', FFMIN)
        Tool.Set_Option('fFmax', FFMAX)
        Tool.Set_Option('fJmin', FJMIN)
        Tool.Set_Option('fJmax', FJMAX)
        Tool.Set_Option('fK', fK)
        return Tool.Execute(Verbose)
    return False

def run_tool_ta_slope_stability_2(A=None, B=None, CMIN=None, CMAX=None, DMIN=None, DMAX=None, EMIN=None, EMAX=None, FMIN=None, FMAX=None, JMIN=None, JMAX=None, G=None, H=None, FCMIN=None, FCMAX=None, FDMIN=None, FDMAX=None, FEMIN=None, FEMAX=None, FFMIN=None, FFMAX=None, FJMIN=None, FJMAX=None, fK=None, Verbose=2):
    '''
    SHALSTAB
    ----------
    [ta_slope_stability.2]\n
    This tool is a realization of the SHALSTAB (Shallow Slope Stability) model from Montgomery & Dietrich (1994). The model computes grid cell critical shallow groundwater recharge values (CR in mm/day) as a measure for relative shallow slope stability, utilizing a simple model that combines a steady-state hydrologic model (a topographic wetness index) to predict groundwater pressures with an infinite slope stability model. For computation, a slope (in radians) and a catchment area (in m2) grid are required. Additionally, information on material density (g/cm3), material friction angle (&deg;), material hydraulic conductivity (m/hr), bulk cohesion (MPa) and depth to potential shear plane (m) are required that can be specified either globally or through grids. The minimum and maximum grids (or global values) specify the range of values from which each soil parameter is sampled. The number of sample runs determines how many values for each soil parameter are randomly taken from the range of values found for each grid cell. Finally, the mean value of the samples taken is calculated (per cell) and used as the soil parameter value in the stability equations. This introduces a probabilistic component in addition to the spatial variability of the soil parameters. If you want to work with static values, enter the same value for minimum and maximum and set the number of samples to one.\n
    The tool produces a continuous CR (mm/day) raster with unconditionally stable cells blanked, and unconditionally unstable cells as CR = 0. Optionally, a classified CR grid can be calculated representing seven stability classes.\n
    Arguments
    ----------
    - A [`input grid`] : Slope grid (rad). A slope angle grid (in radians)
    - B [`input grid`] : Catchment area grid (m2). A catchment area grid (in square meters)
    - CMIN [`optional input grid`] : Min Density grid (g/cm3). A grid representing minimum material density (in g/cm3)
    - CMAX [`optional input grid`] : Max Density grid (g/cm3). A grid representing maximum material density (in g/cm3)
    - DMIN [`optional input grid`] : Min Hydraulic conductivity grid (m/hr) . A grid representing minimum material hydraulic conductivity (in m/hr)
    - DMAX [`optional input grid`] : Max Hydraulic conductivity grid (m/hr) . A grid representing maximum material hydraulic conductivity (in m/hr)
    - EMIN [`optional input grid`] : Min Thickness grid (m). A grid representing minimum material thickness (in m)
    - EMAX [`optional input grid`] : Max Thickness grid (m). A grid representing maximum material thickness (in m)
    - FMIN [`optional input grid`] : Min Friction angle grid (degree) . A grid representing minimum material friction angle (in degrees)
    - FMAX [`optional input grid`] : Max Friction angle grid (degree) . A grid representing maximum material friction angle (in degrees)
    - JMIN [`optional input grid`] : Min Bulk cohesion grid (MPa) . A grid representing minimum bulk cohesion
    - JMAX [`optional input grid`] : Max Bulk cohesion grid (MPa) . A grid representing maximum bulk cohesion
    - G [`output grid`] : CR values. Resulting critical recharge (m/day) grid
    - H [`output grid`] : CR classes. Classified critical recharge (-) grid
    - FCMIN [`floating point number`] : Global minimum density (g/cm3). Default: 1.600000 Constant value if no raster set
    - FCMAX [`floating point number`] : Global maximum density (g/cm3). Default: 1.600000 Constant value if no raster set
    - FDMIN [`floating point number`] : Global minimum conductivity (m/hr). Default: 2.700000 Constant value if no raster set
    - FDMAX [`floating point number`] : Global maximum conductivity (m/hr). Default: 2.700000 Constant value if no raster set
    - FEMIN [`floating point number`] : Global minimum thickness (m). Default: 1.000000 Constant value if no raster set
    - FEMAX [`floating point number`] : Global maximum thickness (m). Default: 1.000000 Constant value if no raster set
    - FFMIN [`floating point number`] : Global minimum friction angle (degree). Default: 33.000000 Constant value if no raster set
    - FFMAX [`floating point number`] : Global maximum friction angle (degree). Default: 33.000000 Constant value if no raster set
    - FJMIN [`floating point number`] : Global minimum bulk cohesion (MPa). Default: 0.000000 Constant value if no raster set
    - FJMAX [`floating point number`] : Global maximum bulk cohesion (MPa). Default: 0.000000 Constant value if no raster set
    - fK [`integer number`] : Number of sampling runs. Default: 1 Number of sampling cycles

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_slope_stability', '2', 'SHALSTAB')
    if Tool.is_Okay():
        Tool.Set_Input ('A', A)
        Tool.Set_Input ('B', B)
        Tool.Set_Input ('Cmin', CMIN)
        Tool.Set_Input ('Cmax', CMAX)
        Tool.Set_Input ('Dmin', DMIN)
        Tool.Set_Input ('Dmax', DMAX)
        Tool.Set_Input ('Emin', EMIN)
        Tool.Set_Input ('Emax', EMAX)
        Tool.Set_Input ('Fmin', FMIN)
        Tool.Set_Input ('Fmax', FMAX)
        Tool.Set_Input ('Jmin', JMIN)
        Tool.Set_Input ('Jmax', JMAX)
        Tool.Set_Output('G', G)
        Tool.Set_Output('H', H)
        Tool.Set_Option('fCmin', FCMIN)
        Tool.Set_Option('fCmax', FCMAX)
        Tool.Set_Option('fDmin', FDMIN)
        Tool.Set_Option('fDmax', FDMAX)
        Tool.Set_Option('fEmin', FEMIN)
        Tool.Set_Option('fEmax', FEMAX)
        Tool.Set_Option('fFmin', FFMIN)
        Tool.Set_Option('fFmax', FFMAX)
        Tool.Set_Option('fJmin', FJMIN)
        Tool.Set_Option('fJmax', FJMAX)
        Tool.Set_Option('fK', fK)
        return Tool.Execute(Verbose)
    return False

def WETNESS(DEM=None, CMIN=None, CMAX=None, DMIN=None, DMAX=None, EMIN=None, EMAX=None, F=None, G=None, FCMIN=None, FCMAX=None, FDMIN=None, FDMAX=None, FEMIN=None, FEMAX=None, fH=None, METHOD=None, PREPROC=None, Verbose=2):
    '''
    WETNESS
    ----------
    [ta_slope_stability.3]\n
    This tool calculates a topographic wetness index (TWI) following Montgomery & Dietrich (1994) that can be used to estimate the degree of saturation of unconsolidated, permeable materials above (more or less) impermeable bedrock. In contrast to the common TOPMODEL (Beven & Kirkby, 1979) - based TWI, this index differs in such that it considers hydraulic conductivity to be constant in a soil mantle overlying relatively impermeable bedrock. Also, it uses the sine of the slope rather than its tangens, which is more correct and significantly matters for steeper slopes that give raise to landslides. For computation, a slope (in radians) and a catchment area (in m2) grid are required. Additionally, information on groundwater recharge (m/hr), material hydraulic conductivity (m/hr), and depth to potential shear plane (m) are required that can be specified either globally or through grids. The tool produces a continuous wetness index (-) where cells with WI values > 1 (overland flow) set to 1, and optionally creates a classified WI grid rendering three saturation classes:.\n
    0): Low moisture (WI smaller 0.1)\n
    1): Partially wet (0.1 smaller WI smaller 1)\n
    2): Saturation zone (WI larger 1)\n
    References:\n
    [Beven, K.J., Kirkby, M.J. (1979) A physically-based variable contributing area model of basin hydrology. Hydrology Science Bulletin, 24, 43-69.](http://www.tandfonline.com/doi/abs/10.1080/02626667909491834).\n
    [Montgomery D. R., Dietrich, W. E. (1994) A physically based model for the topographic control on shallow landsliding. Water Resources Research, 30, 1153-1171.](http://www.agu.org/pubs/crossref/1994/93WR02979.shtml).\n
    Arguments
    ----------
    - DEM [`input grid`] : DEM. A DEM
    - CMIN [`optional input grid`] : Min hydraulic conductivity grid (m/hr) . A grid representing minimum material hydraulic conductivity (in m/hr)
    - CMAX [`optional input grid`] : Max hydraulic conductivity grid (m/hr) . A grid representing maximum material hydraulic conductivity (in m/hr)
    - DMIN [`optional input grid`] : Min groundwater recharge grid (m/hr) . A grid representing minimum groundwater recharge (in m/hr)
    - DMAX [`optional input grid`] : Max groundwater recharge grid (m/hr) . A grid representing maximum groundwater recharge (in m/hr)
    - EMIN [`optional input grid`] : Min material depth grid (m). A grid representing minimum depth to potential shear plane (in m)
    - EMAX [`optional input grid`] : Max material depth grid (m). A grid representing maximum depth to potential shear plane (in m)
    - F [`output grid`] : WI values. Resulting wetness index (-) grid
    - G [`output grid`] : WI classes. Classified wetness (-) grid
    - FCMIN [`floating point number`] : Min global material conductivity (m/hr). Default: 2.700000 Constant value if no raster set
    - FCMAX [`floating point number`] : Max global material conductivity (m/hr). Default: 2.700000 Constant value if no raster set
    - FDMIN [`floating point number`] : Min global groundwater recharge (m/hr). Default: 0.001000 Constant value if no raster set
    - FDMAX [`floating point number`] : Max global groundwater recharge (m/hr). Default: 0.001000 Constant value if no raster set
    - FEMIN [`floating point number`] : Min global material depth (m). Default: 1.000000 Constant value if no raster set
    - FEMAX [`floating point number`] : Max global material depth (m). Default: 1.000000 Constant value if no raster set
    - fH [`integer number`] : Parameter sampling runs. Default: 1 Number of sampling cycles
    - METHOD [`choice`] : Catchment Area Calculation. Available Choices: [0] Deterministic 8 [1] Rho 8 [2] Braunschweiger Reliefmodell [3] Deterministic Infinity [4] Multiple Flow Direction [5] Multiple Triangular Flow Direction Default: 4
    - PREPROC [`boolean`] : Preprocessing. Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_slope_stability', '3', 'WETNESS')
    if Tool.is_Okay():
        Tool.Set_Input ('DEM', DEM)
        Tool.Set_Input ('Cmin', CMIN)
        Tool.Set_Input ('Cmax', CMAX)
        Tool.Set_Input ('Dmin', DMIN)
        Tool.Set_Input ('Dmax', DMAX)
        Tool.Set_Input ('Emin', EMIN)
        Tool.Set_Input ('Emax', EMAX)
        Tool.Set_Output('F', F)
        Tool.Set_Output('G', G)
        Tool.Set_Option('fCmin', FCMIN)
        Tool.Set_Option('fCmax', FCMAX)
        Tool.Set_Option('fDmin', FDMIN)
        Tool.Set_Option('fDmax', FDMAX)
        Tool.Set_Option('fEmin', FEMIN)
        Tool.Set_Option('fEmax', FEMAX)
        Tool.Set_Option('fH', fH)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('PREPROC', PREPROC)
        return Tool.Execute(Verbose)
    return False

def run_tool_ta_slope_stability_3(DEM=None, CMIN=None, CMAX=None, DMIN=None, DMAX=None, EMIN=None, EMAX=None, F=None, G=None, FCMIN=None, FCMAX=None, FDMIN=None, FDMAX=None, FEMIN=None, FEMAX=None, fH=None, METHOD=None, PREPROC=None, Verbose=2):
    '''
    WETNESS
    ----------
    [ta_slope_stability.3]\n
    This tool calculates a topographic wetness index (TWI) following Montgomery & Dietrich (1994) that can be used to estimate the degree of saturation of unconsolidated, permeable materials above (more or less) impermeable bedrock. In contrast to the common TOPMODEL (Beven & Kirkby, 1979) - based TWI, this index differs in such that it considers hydraulic conductivity to be constant in a soil mantle overlying relatively impermeable bedrock. Also, it uses the sine of the slope rather than its tangens, which is more correct and significantly matters for steeper slopes that give raise to landslides. For computation, a slope (in radians) and a catchment area (in m2) grid are required. Additionally, information on groundwater recharge (m/hr), material hydraulic conductivity (m/hr), and depth to potential shear plane (m) are required that can be specified either globally or through grids. The tool produces a continuous wetness index (-) where cells with WI values > 1 (overland flow) set to 1, and optionally creates a classified WI grid rendering three saturation classes:.\n
    0): Low moisture (WI smaller 0.1)\n
    1): Partially wet (0.1 smaller WI smaller 1)\n
    2): Saturation zone (WI larger 1)\n
    References:\n
    [Beven, K.J., Kirkby, M.J. (1979) A physically-based variable contributing area model of basin hydrology. Hydrology Science Bulletin, 24, 43-69.](http://www.tandfonline.com/doi/abs/10.1080/02626667909491834).\n
    [Montgomery D. R., Dietrich, W. E. (1994) A physically based model for the topographic control on shallow landsliding. Water Resources Research, 30, 1153-1171.](http://www.agu.org/pubs/crossref/1994/93WR02979.shtml).\n
    Arguments
    ----------
    - DEM [`input grid`] : DEM. A DEM
    - CMIN [`optional input grid`] : Min hydraulic conductivity grid (m/hr) . A grid representing minimum material hydraulic conductivity (in m/hr)
    - CMAX [`optional input grid`] : Max hydraulic conductivity grid (m/hr) . A grid representing maximum material hydraulic conductivity (in m/hr)
    - DMIN [`optional input grid`] : Min groundwater recharge grid (m/hr) . A grid representing minimum groundwater recharge (in m/hr)
    - DMAX [`optional input grid`] : Max groundwater recharge grid (m/hr) . A grid representing maximum groundwater recharge (in m/hr)
    - EMIN [`optional input grid`] : Min material depth grid (m). A grid representing minimum depth to potential shear plane (in m)
    - EMAX [`optional input grid`] : Max material depth grid (m). A grid representing maximum depth to potential shear plane (in m)
    - F [`output grid`] : WI values. Resulting wetness index (-) grid
    - G [`output grid`] : WI classes. Classified wetness (-) grid
    - FCMIN [`floating point number`] : Min global material conductivity (m/hr). Default: 2.700000 Constant value if no raster set
    - FCMAX [`floating point number`] : Max global material conductivity (m/hr). Default: 2.700000 Constant value if no raster set
    - FDMIN [`floating point number`] : Min global groundwater recharge (m/hr). Default: 0.001000 Constant value if no raster set
    - FDMAX [`floating point number`] : Max global groundwater recharge (m/hr). Default: 0.001000 Constant value if no raster set
    - FEMIN [`floating point number`] : Min global material depth (m). Default: 1.000000 Constant value if no raster set
    - FEMAX [`floating point number`] : Max global material depth (m). Default: 1.000000 Constant value if no raster set
    - fH [`integer number`] : Parameter sampling runs. Default: 1 Number of sampling cycles
    - METHOD [`choice`] : Catchment Area Calculation. Available Choices: [0] Deterministic 8 [1] Rho 8 [2] Braunschweiger Reliefmodell [3] Deterministic Infinity [4] Multiple Flow Direction [5] Multiple Triangular Flow Direction Default: 4
    - PREPROC [`boolean`] : Preprocessing. Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_slope_stability', '3', 'WETNESS')
    if Tool.is_Okay():
        Tool.Set_Input ('DEM', DEM)
        Tool.Set_Input ('Cmin', CMIN)
        Tool.Set_Input ('Cmax', CMAX)
        Tool.Set_Input ('Dmin', DMIN)
        Tool.Set_Input ('Dmax', DMAX)
        Tool.Set_Input ('Emin', EMIN)
        Tool.Set_Input ('Emax', EMAX)
        Tool.Set_Output('F', F)
        Tool.Set_Output('G', G)
        Tool.Set_Option('fCmin', FCMIN)
        Tool.Set_Option('fCmax', FCMAX)
        Tool.Set_Option('fDmin', FDMIN)
        Tool.Set_Option('fDmax', FDMAX)
        Tool.Set_Option('fEmin', FEMIN)
        Tool.Set_Option('fEmax', FEMAX)
        Tool.Set_Option('fH', fH)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('PREPROC', PREPROC)
        return Tool.Execute(Verbose)
    return False

def WEDGEFAIL(DEM=None, C=None, D=None, EMIN=None, EMAX=None, F=None, fC=None, fD=None, FEMIN=None, FEMAX=None, ff=None, fI=None, METHOD=None, Verbose=2):
    '''
    WEDGEFAIL
    ----------
    [ta_slope_stability.4]\n
    This tool determines terrain elements where failure (slide- or topple movements) on geological discontinuies are kinematically possible through the spatial application of common frictional feasibility criteria (G&uuml;nther et al. 2012 and references therein). Both the orientation of slope elements specified through aspect- and dip grids (in radians) are required together with the orientation of one planar structure defined through global- or grid dip direction and dip data, or two planar structures defined by plunge direction and plunge information of their intersection line (in degrees). The shear strength of the discontinuities is specified using global or grid-based friction angle data. Optionally, a cone value can be set allowing for some variance in discontinuity dip orientations. The tool operates in slide (testing for plane and wedge sliding) or topple (testing for plane and wedge toppling) modes.\n
    Arguments
    ----------
    - DEM [`input grid`] : DEM. A DEM
    - C [`optional input grid`] : Dip/Plunge direction grid (degree) . A dip- or plunge direction grid (in degrees)
    - D [`optional input grid`] : Dip/Plunge grid (degree) . A dip- or plunge grid (in degrees)
    - EMIN [`optional input grid`] : Min friction angle grid (degree) . A minimum discontinuity friction angle grid (in degrees)
    - EMAX [`optional input grid`] : Max friction angle grid (degree) . A maximum discontinuity friction angle grid (in degrees)
    - F [`output grid`] : Failures. Resulting failure cells (-) grid
    - fC [`floating point number`] : Global dip/plunge direction (degree). Default: 0.000000 Constant value if no raster set
    - fD [`floating point number`] : Global dip/plunge (degree). Default: 35.000000 Constant value if no raster set
    - FEMIN [`floating point number`] : Min global friction angle (degree). Default: 35.000000 Constant value if no raster set
    - FEMAX [`floating point number`] : Max global friction angle (degree). Default: 35.000000 Constant value if no raster set
    - ff [`integer number`] : Cone radius (degree). Default: 0 Radius of optional cone variance (in degrees)
    - fI [`integer number`] : Parameter sampling runs. Default: 1 Number of sampling cycles
    - METHOD [`choice`] : Mode. Available Choices: [0] Slide [1] Topple Default: 0 Set failure mode

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_slope_stability', '4', 'WEDGEFAIL')
    if Tool.is_Okay():
        Tool.Set_Input ('DEM', DEM)
        Tool.Set_Input ('C', C)
        Tool.Set_Input ('D', D)
        Tool.Set_Input ('Emin', EMIN)
        Tool.Set_Input ('Emax', EMAX)
        Tool.Set_Output('F', F)
        Tool.Set_Option('fC', fC)
        Tool.Set_Option('fD', fD)
        Tool.Set_Option('fEmin', FEMIN)
        Tool.Set_Option('fEmax', FEMAX)
        Tool.Set_Option('ff', ff)
        Tool.Set_Option('fI', fI)
        Tool.Set_Option('METHOD', METHOD)
        return Tool.Execute(Verbose)
    return False

def run_tool_ta_slope_stability_4(DEM=None, C=None, D=None, EMIN=None, EMAX=None, F=None, fC=None, fD=None, FEMIN=None, FEMAX=None, ff=None, fI=None, METHOD=None, Verbose=2):
    '''
    WEDGEFAIL
    ----------
    [ta_slope_stability.4]\n
    This tool determines terrain elements where failure (slide- or topple movements) on geological discontinuies are kinematically possible through the spatial application of common frictional feasibility criteria (G&uuml;nther et al. 2012 and references therein). Both the orientation of slope elements specified through aspect- and dip grids (in radians) are required together with the orientation of one planar structure defined through global- or grid dip direction and dip data, or two planar structures defined by plunge direction and plunge information of their intersection line (in degrees). The shear strength of the discontinuities is specified using global or grid-based friction angle data. Optionally, a cone value can be set allowing for some variance in discontinuity dip orientations. The tool operates in slide (testing for plane and wedge sliding) or topple (testing for plane and wedge toppling) modes.\n
    Arguments
    ----------
    - DEM [`input grid`] : DEM. A DEM
    - C [`optional input grid`] : Dip/Plunge direction grid (degree) . A dip- or plunge direction grid (in degrees)
    - D [`optional input grid`] : Dip/Plunge grid (degree) . A dip- or plunge grid (in degrees)
    - EMIN [`optional input grid`] : Min friction angle grid (degree) . A minimum discontinuity friction angle grid (in degrees)
    - EMAX [`optional input grid`] : Max friction angle grid (degree) . A maximum discontinuity friction angle grid (in degrees)
    - F [`output grid`] : Failures. Resulting failure cells (-) grid
    - fC [`floating point number`] : Global dip/plunge direction (degree). Default: 0.000000 Constant value if no raster set
    - fD [`floating point number`] : Global dip/plunge (degree). Default: 35.000000 Constant value if no raster set
    - FEMIN [`floating point number`] : Min global friction angle (degree). Default: 35.000000 Constant value if no raster set
    - FEMAX [`floating point number`] : Max global friction angle (degree). Default: 35.000000 Constant value if no raster set
    - ff [`integer number`] : Cone radius (degree). Default: 0 Radius of optional cone variance (in degrees)
    - fI [`integer number`] : Parameter sampling runs. Default: 1 Number of sampling cycles
    - METHOD [`choice`] : Mode. Available Choices: [0] Slide [1] Topple Default: 0 Set failure mode

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_slope_stability', '4', 'WEDGEFAIL')
    if Tool.is_Okay():
        Tool.Set_Input ('DEM', DEM)
        Tool.Set_Input ('C', C)
        Tool.Set_Input ('D', D)
        Tool.Set_Input ('Emin', EMIN)
        Tool.Set_Input ('Emax', EMAX)
        Tool.Set_Output('F', F)
        Tool.Set_Option('fC', fC)
        Tool.Set_Option('fD', fD)
        Tool.Set_Option('fEmin', FEMIN)
        Tool.Set_Option('fEmax', FEMAX)
        Tool.Set_Option('ff', ff)
        Tool.Set_Option('fI', fI)
        Tool.Set_Option('METHOD', METHOD)
        return Tool.Execute(Verbose)
    return False

def ANGMAP(DEM=None, C=None, D=None, E=None, F=None, G=None, fB=None, fC=None, Verbose=2):
    '''
    ANGMAP
    ----------
    [ta_slope_stability.5]\n
    This tool computes the acute angle raster between the topographic surface defined by slope and aspect rasters internally derived from input elevation raster, and a structural plane defined by diop direction- and dip grids. Optionally, the dip direction and dip of the cutting line linears between the two planes can be calculated\n
    Reference: [G&uuml;nther, A. (2003). SLOPEMAP: programs for automated mapping of geometrical and kinematical properties of hard rock hill slopes. Computers & Geosciences, 29, 865 - 875](http://www.sciencedirect.com/science/article/pii/S0098300403000864).\n
    Arguments
    ----------
    - DEM [`input grid`] : Elevation. A Digital Elvation Model (DEM)
    - C [`optional input grid`] : Dip grid (degrees) . A grid representing the dip of the structure plane (in degrees)
    - D [`optional input grid`] : Dip direction grid (degrees) . A grid representing the dip direction of the structure plane (in degrees)
    - E [`output grid`] : Angle. Acute angle (degrees) grid
    - F [`output grid`] : CL dipdir. Dip direction cutting line (degrees)
    - G [`output grid`] : CL dip. Dip cutting line (degrees)
    - fB [`floating point number`] : Global structure dip (degrees). Default: 45.000000 Constant value if no raster set
    - fC [`floating point number`] : Global structure dip direction (degrees). Default: 90.000000 Constant value if no raster set

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_slope_stability', '5', 'ANGMAP')
    if Tool.is_Okay():
        Tool.Set_Input ('DEM', DEM)
        Tool.Set_Input ('C', C)
        Tool.Set_Input ('D', D)
        Tool.Set_Output('E', E)
        Tool.Set_Output('F', F)
        Tool.Set_Output('G', G)
        Tool.Set_Option('fB', fB)
        Tool.Set_Option('fC', fC)
        return Tool.Execute(Verbose)
    return False

def run_tool_ta_slope_stability_5(DEM=None, C=None, D=None, E=None, F=None, G=None, fB=None, fC=None, Verbose=2):
    '''
    ANGMAP
    ----------
    [ta_slope_stability.5]\n
    This tool computes the acute angle raster between the topographic surface defined by slope and aspect rasters internally derived from input elevation raster, and a structural plane defined by diop direction- and dip grids. Optionally, the dip direction and dip of the cutting line linears between the two planes can be calculated\n
    Reference: [G&uuml;nther, A. (2003). SLOPEMAP: programs for automated mapping of geometrical and kinematical properties of hard rock hill slopes. Computers & Geosciences, 29, 865 - 875](http://www.sciencedirect.com/science/article/pii/S0098300403000864).\n
    Arguments
    ----------
    - DEM [`input grid`] : Elevation. A Digital Elvation Model (DEM)
    - C [`optional input grid`] : Dip grid (degrees) . A grid representing the dip of the structure plane (in degrees)
    - D [`optional input grid`] : Dip direction grid (degrees) . A grid representing the dip direction of the structure plane (in degrees)
    - E [`output grid`] : Angle. Acute angle (degrees) grid
    - F [`output grid`] : CL dipdir. Dip direction cutting line (degrees)
    - G [`output grid`] : CL dip. Dip cutting line (degrees)
    - fB [`floating point number`] : Global structure dip (degrees). Default: 45.000000 Constant value if no raster set
    - fC [`floating point number`] : Global structure dip direction (degrees). Default: 90.000000 Constant value if no raster set

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_slope_stability', '5', 'ANGMAP')
    if Tool.is_Okay():
        Tool.Set_Input ('DEM', DEM)
        Tool.Set_Input ('C', C)
        Tool.Set_Input ('D', D)
        Tool.Set_Output('E', E)
        Tool.Set_Output('F', F)
        Tool.Set_Output('G', G)
        Tool.Set_Option('fB', fB)
        Tool.Set_Option('fC', fC)
        return Tool.Execute(Verbose)
    return False

