#! /usr/bin/env python

'''
Python Interface to SAGA Tools Library
----------
- Category : Terrain Analysis
- Name     : Profiles
- ID       : ta_profiles

Description
----------
Simple, flow path and swath profiles.
'''

from PySAGA.helper import Tool_Wrapper

def Cross_Profiles(DEM=None, LINES=None, PROFILES=None, DIST_LINE=None, DIST_PROFILE=None, NUM_PROFILE=None, INTERPOLATION=None, OUTPUT=None, Verbose=2):
    '''
    Cross Profiles
    ----------
    [ta_profiles.3]\n
    Create cross profiles from a grid based DEM for given lines.\n
    Arguments
    ----------
    - DEM [`input grid`] : Elevation
    - LINES [`input shapes`] : Lines
    - PROFILES [`output shapes`] : Cross Profiles
    - DIST_LINE [`floating point number`] : Profile Distance. Minimum: 0.000000 Default: 10.000000 The distance of each cross profile along the lines.
    - DIST_PROFILE [`floating point number`] : Profile Length. Minimum: 0.000000 Default: 10.000000 The length of each cross profile.
    - NUM_PROFILE [`integer number`] : Profile Samples. Minimum: 3 Default: 11 The number of profile points per cross profile.
    - INTERPOLATION [`choice`] : Interpolation. Available Choices: [0] Nearest Neighbour [1] Bilinear Interpolation [2] Bicubic Spline Interpolation [3] B-Spline Interpolation Default: 3
    - OUTPUT [`choice`] : Output. Available Choices: [0] vertices [1] attributes [2] vertices and attributes Default: 2

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_profiles', '3', 'Cross Profiles')
    if Tool.is_Okay():
        Tool.Set_Input ('DEM', DEM)
        Tool.Set_Input ('LINES', LINES)
        Tool.Set_Output('PROFILES', PROFILES)
        Tool.Set_Option('DIST_LINE', DIST_LINE)
        Tool.Set_Option('DIST_PROFILE', DIST_PROFILE)
        Tool.Set_Option('NUM_PROFILE', NUM_PROFILE)
        Tool.Set_Option('INTERPOLATION', INTERPOLATION)
        Tool.Set_Option('OUTPUT', OUTPUT)
        return Tool.Execute(Verbose)
    return False

def run_tool_ta_profiles_3(DEM=None, LINES=None, PROFILES=None, DIST_LINE=None, DIST_PROFILE=None, NUM_PROFILE=None, INTERPOLATION=None, OUTPUT=None, Verbose=2):
    '''
    Cross Profiles
    ----------
    [ta_profiles.3]\n
    Create cross profiles from a grid based DEM for given lines.\n
    Arguments
    ----------
    - DEM [`input grid`] : Elevation
    - LINES [`input shapes`] : Lines
    - PROFILES [`output shapes`] : Cross Profiles
    - DIST_LINE [`floating point number`] : Profile Distance. Minimum: 0.000000 Default: 10.000000 The distance of each cross profile along the lines.
    - DIST_PROFILE [`floating point number`] : Profile Length. Minimum: 0.000000 Default: 10.000000 The length of each cross profile.
    - NUM_PROFILE [`integer number`] : Profile Samples. Minimum: 3 Default: 11 The number of profile points per cross profile.
    - INTERPOLATION [`choice`] : Interpolation. Available Choices: [0] Nearest Neighbour [1] Bilinear Interpolation [2] Bicubic Spline Interpolation [3] B-Spline Interpolation Default: 3
    - OUTPUT [`choice`] : Output. Available Choices: [0] vertices [1] attributes [2] vertices and attributes Default: 2

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_profiles', '3', 'Cross Profiles')
    if Tool.is_Okay():
        Tool.Set_Input ('DEM', DEM)
        Tool.Set_Input ('LINES', LINES)
        Tool.Set_Output('PROFILES', PROFILES)
        Tool.Set_Option('DIST_LINE', DIST_LINE)
        Tool.Set_Option('DIST_PROFILE', DIST_PROFILE)
        Tool.Set_Option('NUM_PROFILE', NUM_PROFILE)
        Tool.Set_Option('INTERPOLATION', INTERPOLATION)
        Tool.Set_Option('OUTPUT', OUTPUT)
        return Tool.Execute(Verbose)
    return False

def Profiles_from_Lines(DEM=None, LINES=None, VALUES=None, PROFILE=None, PROFILES=None, NAME=None, SPLIT=None, Verbose=2):
    '''
    Profiles from Lines
    ----------
    [ta_profiles.4]\n
    Create profiles from a grid based DEM for each line of a lines layer.\n
    Arguments
    ----------
    - DEM [`input grid`] : Elevation
    - LINES [`input shapes`] : Lines
    - VALUES [`optional input grid list`] : Values. Additional values to be collected along profile.
    - PROFILE [`output shapes`] : Profiles
    - PROFILES [`output shapes list`] : Profiles
    - NAME [`table field`] : Name. Attribute to use for split line naming (=> each line as new profile)
    - SPLIT [`boolean`] : Each Line as new Profile. Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_profiles', '4', 'Profiles from Lines')
    if Tool.is_Okay():
        Tool.Set_Input ('DEM', DEM)
        Tool.Set_Input ('LINES', LINES)
        Tool.Set_Input ('VALUES', VALUES)
        Tool.Set_Output('PROFILE', PROFILE)
        Tool.Set_Output('PROFILES', PROFILES)
        Tool.Set_Option('NAME', NAME)
        Tool.Set_Option('SPLIT', SPLIT)
        return Tool.Execute(Verbose)
    return False

def run_tool_ta_profiles_4(DEM=None, LINES=None, VALUES=None, PROFILE=None, PROFILES=None, NAME=None, SPLIT=None, Verbose=2):
    '''
    Profiles from Lines
    ----------
    [ta_profiles.4]\n
    Create profiles from a grid based DEM for each line of a lines layer.\n
    Arguments
    ----------
    - DEM [`input grid`] : Elevation
    - LINES [`input shapes`] : Lines
    - VALUES [`optional input grid list`] : Values. Additional values to be collected along profile.
    - PROFILE [`output shapes`] : Profiles
    - PROFILES [`output shapes list`] : Profiles
    - NAME [`table field`] : Name. Attribute to use for split line naming (=> each line as new profile)
    - SPLIT [`boolean`] : Each Line as new Profile. Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_profiles', '4', 'Profiles from Lines')
    if Tool.is_Okay():
        Tool.Set_Input ('DEM', DEM)
        Tool.Set_Input ('LINES', LINES)
        Tool.Set_Input ('VALUES', VALUES)
        Tool.Set_Output('PROFILE', PROFILE)
        Tool.Set_Output('PROFILES', PROFILES)
        Tool.Set_Option('NAME', NAME)
        Tool.Set_Option('SPLIT', SPLIT)
        return Tool.Execute(Verbose)
    return False

def Profile_from_Points(GRID=None, TABLE=None, VALUES=None, RESULT=None, X=None, Y=None, Verbose=2):
    '''
    Profile from Points
    ----------
    [ta_profiles.5]\n
    The tool allows one to query a profile from an input grid (usually a DEM) for point coordinates stored in a table or shapefile. The profile is traced from one point to the next, sampling the grid values along each line segment. Optionally, additional grids can be queried whose values are added to the profile table.\n
    Arguments
    ----------
    - GRID [`input grid`] : Elevation. The input grid to query.
    - TABLE [`input table`] : Coordinates Table. Table with the point coordinates to query.
    - VALUES [`optional input grid list`] : Values. Additional values to be collected along profile.
    - RESULT [`output table`] : Profile. The output table with the queried profile values.
    - X [`table field`] : X Coordinate
    - Y [`table field`] : Y Coordinate

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_profiles', '5', 'Profile from Points')
    if Tool.is_Okay():
        Tool.Set_Input ('GRID', GRID)
        Tool.Set_Input ('TABLE', TABLE)
        Tool.Set_Input ('VALUES', VALUES)
        Tool.Set_Output('RESULT', RESULT)
        Tool.Set_Option('X', X)
        Tool.Set_Option('Y', Y)
        return Tool.Execute(Verbose)
    return False

def run_tool_ta_profiles_5(GRID=None, TABLE=None, VALUES=None, RESULT=None, X=None, Y=None, Verbose=2):
    '''
    Profile from Points
    ----------
    [ta_profiles.5]\n
    The tool allows one to query a profile from an input grid (usually a DEM) for point coordinates stored in a table or shapefile. The profile is traced from one point to the next, sampling the grid values along each line segment. Optionally, additional grids can be queried whose values are added to the profile table.\n
    Arguments
    ----------
    - GRID [`input grid`] : Elevation. The input grid to query.
    - TABLE [`input table`] : Coordinates Table. Table with the point coordinates to query.
    - VALUES [`optional input grid list`] : Values. Additional values to be collected along profile.
    - RESULT [`output table`] : Profile. The output table with the queried profile values.
    - X [`table field`] : X Coordinate
    - Y [`table field`] : Y Coordinate

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_profiles', '5', 'Profile from Points')
    if Tool.is_Okay():
        Tool.Set_Input ('GRID', GRID)
        Tool.Set_Input ('TABLE', TABLE)
        Tool.Set_Input ('VALUES', VALUES)
        Tool.Set_Output('RESULT', RESULT)
        Tool.Set_Option('X', X)
        Tool.Set_Option('Y', Y)
        return Tool.Execute(Verbose)
    return False

