#! /usr/bin/env python

'''
Python Interface to SAGA Tools Library
----------
- Category : Simulation
- Name     : Erosion
- ID       : sim_erosion

Description
----------
Modelling erosion processes.
'''

from PySAGA.helper import Tool_Wrapper

def MMFSAGA_Soil_Erosion_Model(DTM=None, S=None, PI=None, CC=None, PH=None, ETEO=None, GC=None, D=None, NV=None, MS=None, BD=None, EHD=None, LP=None, PER_C=None, PER_Z=None, PER_S=None, ST=None, CHANNEL=None, RFR=None, TAB_METEO=None, IF=None, Q=None, SL=None, Rf=None, KE=None, Rc=None, TCC=None, TCZ=None, TCS=None, Gc=None, Gz=None, Gs=None, SLC=None, SLZ=None, SLS=None, TCONDC=None, TCONDZ=None, TCONDS=None, W_UP=None, OUT_PATH=None, INTERFLOW=None, T=None, TIMESPAN=None, R=None, I=None, Rn=None, KE_I_METHOD=None, FLOWD_VA=None, CHANNELTRANSPORT=None, Verbose=2):
    '''
    MMF-SAGA Soil Erosion Model
    ----------
    [sim_erosion.0]\n
    Soil erosion modelling with a modified MMF (Morgan-Morgan-Finney) model (Morgan & Duzant 2008).[This tool is called MMF-SAGA because some things have been implemented differently compared to the original publication. The most important are:[[\n
    (-) the extension of the model to support spatially distributed modelling\n
    (-) the introduction of a "Channel Network" layer\n
    (-) the introduction of a "rainfall duration" (time span) parameter\n
    (-) the exposure of the flow depth parameter\n
    [[A more detailed description of the model, its modifications, and model application is provided by Setiawan (2012), chapter 6.[Currently, a number of additional grid datasets are outputted to facilitate model evaluation. This can be easily changed within the source code.[[[References:[[Morgan, R.P.C. (2001)]: A simple approach to soil loss prediction: a revised Morgan-Morgan-Finney model. Catena 44: 305-322.[[[Morgan, R.P.C., Duzant, J.H. (2008)]: Modified MMF (Morgan-Morgan-Finney) model for evaluating effects of crops and vegetation cover on soil erosion. Earth Surf. Process. Landforms 32: 90-106.[[[[Setiawan, M. A. (2012)]: Integrated Soil Erosion Management in the upper Serayu Watershed, Wonosobo District, Central Java Province, Indonesia. Dissertation at the Faculty of Geo- and Atmospheric Sciences of the University of Innsbruck, Austria.](http://sourceforge.net/projects/saga-gis/files/SAGA%20-%20Documentation/Modules/MMF-SAGA_Setiawan.pdf)[[\n
    Arguments
    ----------
    - DTM [`input grid`] : Digital Terrain Model. DTM, digital terrain model [m]
    - S [`input grid`] : Slope. S, slope [rad]
    - PI [`input grid`] : Permanent Interception. PI, permanent interception expressed as the proportion [between 0-1] of rainfall
    - CC [`input grid`] : Canopy Cover. CC, canopy cover expressed as a portion [between 0-1] of the soil surface protected by vegetation or crop
    - PH [`input grid`] : Plant Height. PH, plant height [m], representing the effective height from which raindrops fall from the crop or vegetation
    - ETEO [`input grid`] : Ratio Evapotranspiration. Et/Eo, ratio of actual to potential evapotranspiration
    - GC [`input grid`] : Ground cover. GC, Ground cover expressed as a portion [between 0-1] of the soil surface protected by vegetation or crop cover on the ground
    - D [`input grid`] : Diameter plant elements. D, Average diameter [m] of the individual plants elements (stem, leaves) at the ground surface
    - NV [`input grid`] : Number plant elements. NV, Number of plant elements per unit area [number/unit area] at the ground surface
    - MS [`input grid`] : Soil moisture (at FC). MS, Soil moisture at field capacity [% w/w]
    - BD [`input grid`] : Bulk density top layer. BD, Bulk density of the top layer [Mg/m3]
    - EHD [`input grid`] : Effective hydrological depth. EHD, Effective hydrological depth of the soil [m]
    - LP [`input grid`] : Sat. lateral permeability. LP, Saturated lateral permeability of the soil [m/day]
    - PER_C [`input grid`] : Percentage clays. c, Percentage clays [%]
    - PER_Z [`input grid`] : Percentage silt. z, Percentage silt [%]
    - PER_S [`input grid`] : Percentage sand. s, Percentage sand [%]
    - ST [`input grid`] : Percentage rock fragments. ST, Percentage rock fragments on the soil surface [%]
    - CHANNEL [`optional input grid`] : Channel Network. Channel network, all other cells NoData
    - RFR [`optional input grid`] : Surface roughness. RFR, Surface roughness [cm/m]. In case the surface roughness is not provided as input, v_flow_t is set to 1.0, i.e. natural soil surface roughness is not accounted for.
    - TAB_METEO [`optional input table`] : Meteorological data. Meteorological data for multiple timestep modelling [model step (day); temperature (Celsius); rainfall (mm), rainfall intensity (mm/h); rainfall duration (day); timespan (days)]
    - IF [`output grid`] : Interflow. IF
    - Q [`output grid`] : Mean runoff. Q, estimation of mean runoff [mm]
    - SL [`output grid`] : Mean soil loss. SL, estimation of mean soil loss [kg]
    - Rf [`output grid`] : Effective Rainfall. Rf
    - KE [`output grid`] : Total Kinetic Energy. KE
    - Rc [`output grid`] : Soil moisture storage capacity. Rc
    - TCC [`output grid`] : Transport Capacity Clay. TCc
    - TCZ [`output grid`] : Transport Capacity Silt. TCz
    - TCS [`output grid`] : Transport Capacity Sand. SLs
    - Gc [`output grid`] : Available Clay. Gc
    - Gz [`output grid`] : Available Silt. Gz
    - Gs [`output grid`] : Available Sand. Gs
    - SLC [`output grid`] : Sediment Balance Clay. SLc
    - SLZ [`output grid`] : Sediment Balance Silt. SLz
    - SLS [`output grid`] : Sediment Balance Sand. SLs
    - TCONDC [`output grid`] : Transport Condition Clay. Sediment Limited [0], Transport Limited (SL = TC) [1], Transport Limited (SL = G) [2]
    - TCONDZ [`output grid`] : Transport Condition Silt. Sediment Limited [0], Transport Limited (SL = TC) [1], Transport Limited (SL = G) [2]
    - TCONDS [`output grid`] : Transport Condition Sand. Sediment Limited [0], Transport Limited (SL = TC) [1], Transport Limited (SL = G) [2]
    - W_UP [`output grid`] : Upslope Flow Width. W_up
    - OUT_PATH [`file path`] : Output file path. Full path to the directory for the output grids of each model step
    - INTERFLOW [`boolean`] : Simulate Interflow. Default: 1 Simulate interflow
    - T [`floating point number`] : Mean temperature. Default: 18.000000 T, mean temperature [degree C]
    - TIMESPAN [`integer number`] : Timespan (days). Minimum: 1 Maximum: 365 Default: 30 The number of days to model.
    - R [`floating point number`] : Rainfall. Default: 200.000000 R, height of precipitation in timespan [mm]
    - I [`floating point number`] : Rainfall intensity. Default: 20.000000 I, rainfall intensity [mm/h]
    - Rn [`floating point number`] : Rainfall Duration. Default: 20.000000 Rn, number of rain days in timespan [-]
    - KE_I_METHOD [`choice`] : Relationship KE - I. Available Choices: [0] North America east of Rocky Mountains (Wischmeier & Smith 1978) [1] North-western Europe (Marshall & Palmer) [2] Mediterranean-type climates (Zanchi & Torri 1980) [3] Western Mediterranean (Coutinho & Tomas 1995) [4] Tropical climates (Hudson 1965) [5] Eastern Asia (Onaga et al. 1998) [6] Southern hemisphere climates (Rosewell 1986) [7] Bogor, West-Java, Indonesia (McISaac 1990) Default: 0 Relationship between kinetic energy (KE) and rainfall intensity (I)
    - FLOWD_VA [`floating point number`] : Flow Depth (actual flow velocity). Minimum: 0.000000 Default: 0.005000 The flow depth used to calculate the actual flow velocity [m] (e.g. 0.005 unchannelled flow, 0.01 shallow rills, 0.25 deeper rills.
    - CHANNELTRANSPORT [`boolean`] : Route Soil along Channel Network. Default: 0 Route soil loss along channel network to outlet

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('sim_erosion', '0', 'MMF-SAGA Soil Erosion Model')
    if Tool.is_Okay():
        Tool.Set_Input ('DTM', DTM)
        Tool.Set_Input ('S', S)
        Tool.Set_Input ('PI', PI)
        Tool.Set_Input ('CC', CC)
        Tool.Set_Input ('PH', PH)
        Tool.Set_Input ('EtEo', ETEO)
        Tool.Set_Input ('GC', GC)
        Tool.Set_Input ('D', D)
        Tool.Set_Input ('NV', NV)
        Tool.Set_Input ('MS', MS)
        Tool.Set_Input ('BD', BD)
        Tool.Set_Input ('EHD', EHD)
        Tool.Set_Input ('LP', LP)
        Tool.Set_Input ('PER_C', PER_C)
        Tool.Set_Input ('PER_Z', PER_Z)
        Tool.Set_Input ('PER_S', PER_S)
        Tool.Set_Input ('ST', ST)
        Tool.Set_Input ('CHANNEL', CHANNEL)
        Tool.Set_Input ('RFR', RFR)
        Tool.Set_Input ('TAB_METEO', TAB_METEO)
        Tool.Set_Output('IF', IF)
        Tool.Set_Output('Q', Q)
        Tool.Set_Output('SL', SL)
        Tool.Set_Output('Rf', Rf)
        Tool.Set_Output('KE', KE)
        Tool.Set_Output('Rc', Rc)
        Tool.Set_Output('TCc', TCC)
        Tool.Set_Output('TCz', TCZ)
        Tool.Set_Output('TCs', TCS)
        Tool.Set_Output('Gc', Gc)
        Tool.Set_Output('Gz', Gz)
        Tool.Set_Output('Gs', Gs)
        Tool.Set_Output('SLc', SLC)
        Tool.Set_Output('SLz', SLZ)
        Tool.Set_Output('SLs', SLS)
        Tool.Set_Output('TCONDc', TCONDC)
        Tool.Set_Output('TCONDz', TCONDZ)
        Tool.Set_Output('TCONDs', TCONDS)
        Tool.Set_Output('W_up', W_UP)
        Tool.Set_Option('OUT_PATH', OUT_PATH)
        Tool.Set_Option('INTERFLOW', INTERFLOW)
        Tool.Set_Option('T', T)
        Tool.Set_Option('TIMESPAN', TIMESPAN)
        Tool.Set_Option('R', R)
        Tool.Set_Option('I', I)
        Tool.Set_Option('Rn', Rn)
        Tool.Set_Option('KE_I_METHOD', KE_I_METHOD)
        Tool.Set_Option('FLOWD_VA', FLOWD_VA)
        Tool.Set_Option('CHANNELTRANSPORT', CHANNELTRANSPORT)
        return Tool.Execute(Verbose)
    return False

def run_tool_sim_erosion_0(DTM=None, S=None, PI=None, CC=None, PH=None, ETEO=None, GC=None, D=None, NV=None, MS=None, BD=None, EHD=None, LP=None, PER_C=None, PER_Z=None, PER_S=None, ST=None, CHANNEL=None, RFR=None, TAB_METEO=None, IF=None, Q=None, SL=None, Rf=None, KE=None, Rc=None, TCC=None, TCZ=None, TCS=None, Gc=None, Gz=None, Gs=None, SLC=None, SLZ=None, SLS=None, TCONDC=None, TCONDZ=None, TCONDS=None, W_UP=None, OUT_PATH=None, INTERFLOW=None, T=None, TIMESPAN=None, R=None, I=None, Rn=None, KE_I_METHOD=None, FLOWD_VA=None, CHANNELTRANSPORT=None, Verbose=2):
    '''
    MMF-SAGA Soil Erosion Model
    ----------
    [sim_erosion.0]\n
    Soil erosion modelling with a modified MMF (Morgan-Morgan-Finney) model (Morgan & Duzant 2008).[This tool is called MMF-SAGA because some things have been implemented differently compared to the original publication. The most important are:[[\n
    (-) the extension of the model to support spatially distributed modelling\n
    (-) the introduction of a "Channel Network" layer\n
    (-) the introduction of a "rainfall duration" (time span) parameter\n
    (-) the exposure of the flow depth parameter\n
    [[A more detailed description of the model, its modifications, and model application is provided by Setiawan (2012), chapter 6.[Currently, a number of additional grid datasets are outputted to facilitate model evaluation. This can be easily changed within the source code.[[[References:[[Morgan, R.P.C. (2001)]: A simple approach to soil loss prediction: a revised Morgan-Morgan-Finney model. Catena 44: 305-322.[[[Morgan, R.P.C., Duzant, J.H. (2008)]: Modified MMF (Morgan-Morgan-Finney) model for evaluating effects of crops and vegetation cover on soil erosion. Earth Surf. Process. Landforms 32: 90-106.[[[[Setiawan, M. A. (2012)]: Integrated Soil Erosion Management in the upper Serayu Watershed, Wonosobo District, Central Java Province, Indonesia. Dissertation at the Faculty of Geo- and Atmospheric Sciences of the University of Innsbruck, Austria.](http://sourceforge.net/projects/saga-gis/files/SAGA%20-%20Documentation/Modules/MMF-SAGA_Setiawan.pdf)[[\n
    Arguments
    ----------
    - DTM [`input grid`] : Digital Terrain Model. DTM, digital terrain model [m]
    - S [`input grid`] : Slope. S, slope [rad]
    - PI [`input grid`] : Permanent Interception. PI, permanent interception expressed as the proportion [between 0-1] of rainfall
    - CC [`input grid`] : Canopy Cover. CC, canopy cover expressed as a portion [between 0-1] of the soil surface protected by vegetation or crop
    - PH [`input grid`] : Plant Height. PH, plant height [m], representing the effective height from which raindrops fall from the crop or vegetation
    - ETEO [`input grid`] : Ratio Evapotranspiration. Et/Eo, ratio of actual to potential evapotranspiration
    - GC [`input grid`] : Ground cover. GC, Ground cover expressed as a portion [between 0-1] of the soil surface protected by vegetation or crop cover on the ground
    - D [`input grid`] : Diameter plant elements. D, Average diameter [m] of the individual plants elements (stem, leaves) at the ground surface
    - NV [`input grid`] : Number plant elements. NV, Number of plant elements per unit area [number/unit area] at the ground surface
    - MS [`input grid`] : Soil moisture (at FC). MS, Soil moisture at field capacity [% w/w]
    - BD [`input grid`] : Bulk density top layer. BD, Bulk density of the top layer [Mg/m3]
    - EHD [`input grid`] : Effective hydrological depth. EHD, Effective hydrological depth of the soil [m]
    - LP [`input grid`] : Sat. lateral permeability. LP, Saturated lateral permeability of the soil [m/day]
    - PER_C [`input grid`] : Percentage clays. c, Percentage clays [%]
    - PER_Z [`input grid`] : Percentage silt. z, Percentage silt [%]
    - PER_S [`input grid`] : Percentage sand. s, Percentage sand [%]
    - ST [`input grid`] : Percentage rock fragments. ST, Percentage rock fragments on the soil surface [%]
    - CHANNEL [`optional input grid`] : Channel Network. Channel network, all other cells NoData
    - RFR [`optional input grid`] : Surface roughness. RFR, Surface roughness [cm/m]. In case the surface roughness is not provided as input, v_flow_t is set to 1.0, i.e. natural soil surface roughness is not accounted for.
    - TAB_METEO [`optional input table`] : Meteorological data. Meteorological data for multiple timestep modelling [model step (day); temperature (Celsius); rainfall (mm), rainfall intensity (mm/h); rainfall duration (day); timespan (days)]
    - IF [`output grid`] : Interflow. IF
    - Q [`output grid`] : Mean runoff. Q, estimation of mean runoff [mm]
    - SL [`output grid`] : Mean soil loss. SL, estimation of mean soil loss [kg]
    - Rf [`output grid`] : Effective Rainfall. Rf
    - KE [`output grid`] : Total Kinetic Energy. KE
    - Rc [`output grid`] : Soil moisture storage capacity. Rc
    - TCC [`output grid`] : Transport Capacity Clay. TCc
    - TCZ [`output grid`] : Transport Capacity Silt. TCz
    - TCS [`output grid`] : Transport Capacity Sand. SLs
    - Gc [`output grid`] : Available Clay. Gc
    - Gz [`output grid`] : Available Silt. Gz
    - Gs [`output grid`] : Available Sand. Gs
    - SLC [`output grid`] : Sediment Balance Clay. SLc
    - SLZ [`output grid`] : Sediment Balance Silt. SLz
    - SLS [`output grid`] : Sediment Balance Sand. SLs
    - TCONDC [`output grid`] : Transport Condition Clay. Sediment Limited [0], Transport Limited (SL = TC) [1], Transport Limited (SL = G) [2]
    - TCONDZ [`output grid`] : Transport Condition Silt. Sediment Limited [0], Transport Limited (SL = TC) [1], Transport Limited (SL = G) [2]
    - TCONDS [`output grid`] : Transport Condition Sand. Sediment Limited [0], Transport Limited (SL = TC) [1], Transport Limited (SL = G) [2]
    - W_UP [`output grid`] : Upslope Flow Width. W_up
    - OUT_PATH [`file path`] : Output file path. Full path to the directory for the output grids of each model step
    - INTERFLOW [`boolean`] : Simulate Interflow. Default: 1 Simulate interflow
    - T [`floating point number`] : Mean temperature. Default: 18.000000 T, mean temperature [degree C]
    - TIMESPAN [`integer number`] : Timespan (days). Minimum: 1 Maximum: 365 Default: 30 The number of days to model.
    - R [`floating point number`] : Rainfall. Default: 200.000000 R, height of precipitation in timespan [mm]
    - I [`floating point number`] : Rainfall intensity. Default: 20.000000 I, rainfall intensity [mm/h]
    - Rn [`floating point number`] : Rainfall Duration. Default: 20.000000 Rn, number of rain days in timespan [-]
    - KE_I_METHOD [`choice`] : Relationship KE - I. Available Choices: [0] North America east of Rocky Mountains (Wischmeier & Smith 1978) [1] North-western Europe (Marshall & Palmer) [2] Mediterranean-type climates (Zanchi & Torri 1980) [3] Western Mediterranean (Coutinho & Tomas 1995) [4] Tropical climates (Hudson 1965) [5] Eastern Asia (Onaga et al. 1998) [6] Southern hemisphere climates (Rosewell 1986) [7] Bogor, West-Java, Indonesia (McISaac 1990) Default: 0 Relationship between kinetic energy (KE) and rainfall intensity (I)
    - FLOWD_VA [`floating point number`] : Flow Depth (actual flow velocity). Minimum: 0.000000 Default: 0.005000 The flow depth used to calculate the actual flow velocity [m] (e.g. 0.005 unchannelled flow, 0.01 shallow rills, 0.25 deeper rills.
    - CHANNELTRANSPORT [`boolean`] : Route Soil along Channel Network. Default: 0 Route soil loss along channel network to outlet

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('sim_erosion', '0', 'MMF-SAGA Soil Erosion Model')
    if Tool.is_Okay():
        Tool.Set_Input ('DTM', DTM)
        Tool.Set_Input ('S', S)
        Tool.Set_Input ('PI', PI)
        Tool.Set_Input ('CC', CC)
        Tool.Set_Input ('PH', PH)
        Tool.Set_Input ('EtEo', ETEO)
        Tool.Set_Input ('GC', GC)
        Tool.Set_Input ('D', D)
        Tool.Set_Input ('NV', NV)
        Tool.Set_Input ('MS', MS)
        Tool.Set_Input ('BD', BD)
        Tool.Set_Input ('EHD', EHD)
        Tool.Set_Input ('LP', LP)
        Tool.Set_Input ('PER_C', PER_C)
        Tool.Set_Input ('PER_Z', PER_Z)
        Tool.Set_Input ('PER_S', PER_S)
        Tool.Set_Input ('ST', ST)
        Tool.Set_Input ('CHANNEL', CHANNEL)
        Tool.Set_Input ('RFR', RFR)
        Tool.Set_Input ('TAB_METEO', TAB_METEO)
        Tool.Set_Output('IF', IF)
        Tool.Set_Output('Q', Q)
        Tool.Set_Output('SL', SL)
        Tool.Set_Output('Rf', Rf)
        Tool.Set_Output('KE', KE)
        Tool.Set_Output('Rc', Rc)
        Tool.Set_Output('TCc', TCC)
        Tool.Set_Output('TCz', TCZ)
        Tool.Set_Output('TCs', TCS)
        Tool.Set_Output('Gc', Gc)
        Tool.Set_Output('Gz', Gz)
        Tool.Set_Output('Gs', Gs)
        Tool.Set_Output('SLc', SLC)
        Tool.Set_Output('SLz', SLZ)
        Tool.Set_Output('SLs', SLS)
        Tool.Set_Output('TCONDc', TCONDC)
        Tool.Set_Output('TCONDz', TCONDZ)
        Tool.Set_Output('TCONDs', TCONDS)
        Tool.Set_Output('W_up', W_UP)
        Tool.Set_Option('OUT_PATH', OUT_PATH)
        Tool.Set_Option('INTERFLOW', INTERFLOW)
        Tool.Set_Option('T', T)
        Tool.Set_Option('TIMESPAN', TIMESPAN)
        Tool.Set_Option('R', R)
        Tool.Set_Option('I', I)
        Tool.Set_Option('Rn', Rn)
        Tool.Set_Option('KE_I_METHOD', KE_I_METHOD)
        Tool.Set_Option('FLOWD_VA', FLOWD_VA)
        Tool.Set_Option('CHANNELTRANSPORT', CHANNELTRANSPORT)
        return Tool.Execute(Verbose)
    return False

