#! /usr/bin/env python

'''
Python Interface to SAGA Tools Library
----------
- Category : Shapes
- Name     : Points
- ID       : shapes_points

Description
----------
Tools for the manipulation of point vector data.
'''

from PySAGA.helper import Tool_Wrapper

def Construct_Points_from_Table(TABLE=None, POINTS=None, X=None, Y=None, Z=None, CRS_STRING=None, Verbose=2):
    '''
    Construct Points from Table
    ----------
    [shapes_points.0]\n
    Construct a points layer from coordinates as provided by a table's attributes.\n
    Arguments
    ----------
    - TABLE [`input table`] : Table
    - POINTS [`output shapes`] : Points
    - X [`table field`] : X
    - Y [`table field`] : Y
    - Z [`table field`] : Z
    - CRS_STRING [`text`] : Coordinate System Definition. Supported formats comprise PROJ and WKT strings, object codes (e.g. "EPSG:4326").

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_points', '0', 'Construct Points from Table')
    if Tool.is_Okay():
        Tool.Set_Input ('TABLE', TABLE)
        Tool.Set_Output('POINTS', POINTS)
        Tool.Set_Option('X', X)
        Tool.Set_Option('Y', Y)
        Tool.Set_Option('Z', Z)
        Tool.Set_Option('CRS_STRING', CRS_STRING)
        return Tool.Execute(Verbose)
    return False

def run_tool_shapes_points_0(TABLE=None, POINTS=None, X=None, Y=None, Z=None, CRS_STRING=None, Verbose=2):
    '''
    Construct Points from Table
    ----------
    [shapes_points.0]\n
    Construct a points layer from coordinates as provided by a table's attributes.\n
    Arguments
    ----------
    - TABLE [`input table`] : Table
    - POINTS [`output shapes`] : Points
    - X [`table field`] : X
    - Y [`table field`] : Y
    - Z [`table field`] : Z
    - CRS_STRING [`text`] : Coordinate System Definition. Supported formats comprise PROJ and WKT strings, object codes (e.g. "EPSG:4326").

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_points', '0', 'Construct Points from Table')
    if Tool.is_Okay():
        Tool.Set_Input ('TABLE', TABLE)
        Tool.Set_Output('POINTS', POINTS)
        Tool.Set_Option('X', X)
        Tool.Set_Option('Y', Y)
        Tool.Set_Option('Z', Z)
        Tool.Set_Option('CRS_STRING', CRS_STRING)
        return Tool.Execute(Verbose)
    return False

def Count_Points_in_Polygons(POINTS=None, POLYGONS=None, Verbose=2):
    '''
    Count Points in Polygons
    ----------
    [shapes_points.1]\n
    Count Points in Polygons.\n
    Arguments
    ----------
    - POINTS [`input shapes`] : Points
    - POLYGONS [`input shapes`] : Polygons

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_points', '1', 'Count Points in Polygons')
    if Tool.is_Okay():
        Tool.Set_Input ('POINTS', POINTS)
        Tool.Set_Input ('POLYGONS', POLYGONS)
        return Tool.Execute(Verbose)
    return False

def run_tool_shapes_points_1(POINTS=None, POLYGONS=None, Verbose=2):
    '''
    Count Points in Polygons
    ----------
    [shapes_points.1]\n
    Count Points in Polygons.\n
    Arguments
    ----------
    - POINTS [`input shapes`] : Points
    - POLYGONS [`input shapes`] : Polygons

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_points', '1', 'Count Points in Polygons')
    if Tool.is_Okay():
        Tool.Set_Input ('POINTS', POINTS)
        Tool.Set_Input ('POLYGONS', POLYGONS)
        return Tool.Execute(Verbose)
    return False

def Create_Point_Grid(POINTS=None, X_EXTENT=None, Y_EXTENT=None, DIST=None, Verbose=2):
    '''
    Create Point Grid
    ----------
    [shapes_points.2]\n
    Creates a regular grid of points.\n
    Arguments
    ----------
    - POINTS [`output shapes`] : Points
    - X_EXTENT [`value range`] : X-Extent
    - Y_EXTENT [`value range`] : Y-Extent
    - DIST [`floating point number`] : Distance. Minimum: 0.000000 Default: 1.000000

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_points', '2', 'Create Point Grid')
    if Tool.is_Okay():
        Tool.Set_Output('POINTS', POINTS)
        Tool.Set_Option('X_EXTENT', X_EXTENT)
        Tool.Set_Option('Y_EXTENT', Y_EXTENT)
        Tool.Set_Option('DIST', DIST)
        return Tool.Execute(Verbose)
    return False

def run_tool_shapes_points_2(POINTS=None, X_EXTENT=None, Y_EXTENT=None, DIST=None, Verbose=2):
    '''
    Create Point Grid
    ----------
    [shapes_points.2]\n
    Creates a regular grid of points.\n
    Arguments
    ----------
    - POINTS [`output shapes`] : Points
    - X_EXTENT [`value range`] : X-Extent
    - Y_EXTENT [`value range`] : Y-Extent
    - DIST [`floating point number`] : Distance. Minimum: 0.000000 Default: 1.000000

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_points', '2', 'Create Point Grid')
    if Tool.is_Okay():
        Tool.Set_Output('POINTS', POINTS)
        Tool.Set_Option('X_EXTENT', X_EXTENT)
        Tool.Set_Option('Y_EXTENT', Y_EXTENT)
        Tool.Set_Option('DIST', DIST)
        return Tool.Execute(Verbose)
    return False

def Point_to_Point_Distances(POINTS=None, NEAR=None, DISTANCES=None, LINES=None, ID_POINTS=None, ID_NEAR=None, FORMAT=None, MIN_DIST=None, MAX_DIST=None, Verbose=2):
    '''
    Point to Point Distances
    ----------
    [shapes_points.3]\n
    Computes distances between pairs of points.\n
    Arguments
    ----------
    - POINTS [`input shapes`] : Points
    - NEAR [`optional input shapes`] : Near Points
    - DISTANCES [`output table`] : Distances
    - LINES [`output shapes`] : Distances as Lines
    - ID_POINTS [`table field`] : Identifier
    - ID_NEAR [`table field`] : Identifier
    - FORMAT [`choice`] : Output Format. Available Choices: [0] complete input times near points matrix [1] each pair with a single record [2] find only the nearest point for each input point Default: 1
    - MIN_DIST [`floating point number`] : Minimum Distance. Minimum: 0.000000 Default: 0.000000
    - MAX_DIST [`floating point number`] : Maximum Distance. Minimum: 0.000000 Default: 0.000000 ignored if set to zero (consider all pairs)

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_points', '3', 'Point to Point Distances')
    if Tool.is_Okay():
        Tool.Set_Input ('POINTS', POINTS)
        Tool.Set_Input ('NEAR', NEAR)
        Tool.Set_Output('DISTANCES', DISTANCES)
        Tool.Set_Output('LINES', LINES)
        Tool.Set_Option('ID_POINTS', ID_POINTS)
        Tool.Set_Option('ID_NEAR', ID_NEAR)
        Tool.Set_Option('FORMAT', FORMAT)
        Tool.Set_Option('MIN_DIST', MIN_DIST)
        Tool.Set_Option('MAX_DIST', MAX_DIST)
        return Tool.Execute(Verbose)
    return False

def run_tool_shapes_points_3(POINTS=None, NEAR=None, DISTANCES=None, LINES=None, ID_POINTS=None, ID_NEAR=None, FORMAT=None, MIN_DIST=None, MAX_DIST=None, Verbose=2):
    '''
    Point to Point Distances
    ----------
    [shapes_points.3]\n
    Computes distances between pairs of points.\n
    Arguments
    ----------
    - POINTS [`input shapes`] : Points
    - NEAR [`optional input shapes`] : Near Points
    - DISTANCES [`output table`] : Distances
    - LINES [`output shapes`] : Distances as Lines
    - ID_POINTS [`table field`] : Identifier
    - ID_NEAR [`table field`] : Identifier
    - FORMAT [`choice`] : Output Format. Available Choices: [0] complete input times near points matrix [1] each pair with a single record [2] find only the nearest point for each input point Default: 1
    - MIN_DIST [`floating point number`] : Minimum Distance. Minimum: 0.000000 Default: 0.000000
    - MAX_DIST [`floating point number`] : Maximum Distance. Minimum: 0.000000 Default: 0.000000 ignored if set to zero (consider all pairs)

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_points', '3', 'Point to Point Distances')
    if Tool.is_Okay():
        Tool.Set_Input ('POINTS', POINTS)
        Tool.Set_Input ('NEAR', NEAR)
        Tool.Set_Output('DISTANCES', DISTANCES)
        Tool.Set_Output('LINES', LINES)
        Tool.Set_Option('ID_POINTS', ID_POINTS)
        Tool.Set_Option('ID_NEAR', ID_NEAR)
        Tool.Set_Option('FORMAT', FORMAT)
        Tool.Set_Option('MIN_DIST', MIN_DIST)
        Tool.Set_Option('MAX_DIST', MAX_DIST)
        return Tool.Execute(Verbose)
    return False

def Populate_Polygons_with_Points(POLYGONS=None, POINTS=None, NUMFIELD=None, NUMPOINTS=None, MAXITER=None, Verbose=2):
    '''
    Populate Polygons with Points
    ----------
    [shapes_points.4]\n
    For each selected polygon of the input layer or for all polygons, if none is selected, a multi-point record is created with evenly distributed points trying to meet the specified number of points per polygon.\n
    Arguments
    ----------
    - POLYGONS [`input shapes`] : Polygons
    - POINTS [`output shapes`] : Points
    - NUMFIELD [`table field`] : Number of Points. Desired number of points per polygon.
    - NUMPOINTS [`integer number`] : Number of Points. Minimum: 1 Default: 100 Desired number of points per polygon.
    - MAXITER [`integer number`] : Maximum Iterations. Minimum: 1 Default: 30

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_points', '4', 'Populate Polygons with Points')
    if Tool.is_Okay():
        Tool.Set_Input ('POLYGONS', POLYGONS)
        Tool.Set_Output('POINTS', POINTS)
        Tool.Set_Option('NUMFIELD', NUMFIELD)
        Tool.Set_Option('NUMPOINTS', NUMPOINTS)
        Tool.Set_Option('MAXITER', MAXITER)
        return Tool.Execute(Verbose)
    return False

def run_tool_shapes_points_4(POLYGONS=None, POINTS=None, NUMFIELD=None, NUMPOINTS=None, MAXITER=None, Verbose=2):
    '''
    Populate Polygons with Points
    ----------
    [shapes_points.4]\n
    For each selected polygon of the input layer or for all polygons, if none is selected, a multi-point record is created with evenly distributed points trying to meet the specified number of points per polygon.\n
    Arguments
    ----------
    - POLYGONS [`input shapes`] : Polygons
    - POINTS [`output shapes`] : Points
    - NUMFIELD [`table field`] : Number of Points. Desired number of points per polygon.
    - NUMPOINTS [`integer number`] : Number of Points. Minimum: 1 Default: 100 Desired number of points per polygon.
    - MAXITER [`integer number`] : Maximum Iterations. Minimum: 1 Default: 30

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_points', '4', 'Populate Polygons with Points')
    if Tool.is_Okay():
        Tool.Set_Input ('POLYGONS', POLYGONS)
        Tool.Set_Output('POINTS', POINTS)
        Tool.Set_Option('NUMFIELD', NUMFIELD)
        Tool.Set_Option('NUMPOINTS', NUMPOINTS)
        Tool.Set_Option('MAXITER', MAXITER)
        return Tool.Execute(Verbose)
    return False

def Convert_LinePolygon_Vertices_to_Points(LINES=None, POINTS=None, ADD_POINT_ORDER=None, ADD=None, METHOD_INSERT=None, DIST=None, Verbose=2):
    '''
    Convert Line/Polygon Vertices to Points
    ----------
    [shapes_points.5]\n
    Converts the vertices of lines or polygons data to points. Optionally inserts additional points in user-defined distances.\n
    Arguments
    ----------
    - LINES [`input shapes`] : Lines
    - POINTS [`output shapes`] : Points
    - ADD_POINT_ORDER [`boolean`] : Add Point Order. Default: 0 Add point order as additional attribute.
    - ADD [`boolean`] : Insert Additional Points. Default: 0
    - METHOD_INSERT [`choice`] : Insertion. Available Choices: [0] per line segment [1] per line [2] from line center Default: 0 Choose the method how to insert additional points.
    - DIST [`floating point number`] : Insertion Distance. Minimum: 0.000000 Default: 1.000000 Point insertion distance [map units].

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_points', '5', 'Convert Line/Polygon Vertices to Points')
    if Tool.is_Okay():
        Tool.Set_Input ('LINES', LINES)
        Tool.Set_Output('POINTS', POINTS)
        Tool.Set_Option('ADD_POINT_ORDER', ADD_POINT_ORDER)
        Tool.Set_Option('ADD', ADD)
        Tool.Set_Option('METHOD_INSERT', METHOD_INSERT)
        Tool.Set_Option('DIST', DIST)
        return Tool.Execute(Verbose)
    return False

def run_tool_shapes_points_5(LINES=None, POINTS=None, ADD_POINT_ORDER=None, ADD=None, METHOD_INSERT=None, DIST=None, Verbose=2):
    '''
    Convert Line/Polygon Vertices to Points
    ----------
    [shapes_points.5]\n
    Converts the vertices of lines or polygons data to points. Optionally inserts additional points in user-defined distances.\n
    Arguments
    ----------
    - LINES [`input shapes`] : Lines
    - POINTS [`output shapes`] : Points
    - ADD_POINT_ORDER [`boolean`] : Add Point Order. Default: 0 Add point order as additional attribute.
    - ADD [`boolean`] : Insert Additional Points. Default: 0
    - METHOD_INSERT [`choice`] : Insertion. Available Choices: [0] per line segment [1] per line [2] from line center Default: 0 Choose the method how to insert additional points.
    - DIST [`floating point number`] : Insertion Distance. Minimum: 0.000000 Default: 1.000000 Point insertion distance [map units].

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_points', '5', 'Convert Line/Polygon Vertices to Points')
    if Tool.is_Okay():
        Tool.Set_Input ('LINES', LINES)
        Tool.Set_Output('POINTS', POINTS)
        Tool.Set_Option('ADD_POINT_ORDER', ADD_POINT_ORDER)
        Tool.Set_Option('ADD', ADD)
        Tool.Set_Option('METHOD_INSERT', METHOD_INSERT)
        Tool.Set_Option('DIST', DIST)
        return Tool.Execute(Verbose)
    return False

def Add_Coordinates_to_Points(INPUT=None, OUTPUT=None, X=None, Y=None, Z=None, M=None, LON=None, LAT=None, Verbose=2):
    '''
    Add Coordinates to Points
    ----------
    [shapes_points.6]\n
    The tool attaches the x- and y-coordinates of each point to the attribute table. For 3D shapefiles, also the z/m-coordinates are reported.\n
    Arguments
    ----------
    - INPUT [`input shapes`] : Points
    - OUTPUT [`output shapes`] : Output
    - X [`boolean`] : X. Default: 1
    - Y [`boolean`] : Y. Default: 1
    - Z [`boolean`] : Z. Default: 1
    - M [`boolean`] : M. Default: 1
    - LON [`boolean`] : Longitude. Default: 0
    - LAT [`boolean`] : Latitude. Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_points', '6', 'Add Coordinates to Points')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Output('OUTPUT', OUTPUT)
        Tool.Set_Option('X', X)
        Tool.Set_Option('Y', Y)
        Tool.Set_Option('Z', Z)
        Tool.Set_Option('M', M)
        Tool.Set_Option('LON', LON)
        Tool.Set_Option('LAT', LAT)
        return Tool.Execute(Verbose)
    return False

def run_tool_shapes_points_6(INPUT=None, OUTPUT=None, X=None, Y=None, Z=None, M=None, LON=None, LAT=None, Verbose=2):
    '''
    Add Coordinates to Points
    ----------
    [shapes_points.6]\n
    The tool attaches the x- and y-coordinates of each point to the attribute table. For 3D shapefiles, also the z/m-coordinates are reported.\n
    Arguments
    ----------
    - INPUT [`input shapes`] : Points
    - OUTPUT [`output shapes`] : Output
    - X [`boolean`] : X. Default: 1
    - Y [`boolean`] : Y. Default: 1
    - Z [`boolean`] : Z. Default: 1
    - M [`boolean`] : M. Default: 1
    - LON [`boolean`] : Longitude. Default: 0
    - LAT [`boolean`] : Latitude. Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_points', '6', 'Add Coordinates to Points')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Output('OUTPUT', OUTPUT)
        Tool.Set_Option('X', X)
        Tool.Set_Option('Y', Y)
        Tool.Set_Option('Z', Z)
        Tool.Set_Option('M', M)
        Tool.Set_Option('LON', LON)
        Tool.Set_Option('LAT', LAT)
        return Tool.Execute(Verbose)
    return False

def Remove_Duplicate_Points(POINTS=None, RESULT=None, FIELD=None, NUMERIC=None, METHOD=None, Verbose=2):
    '''
    Remove Duplicate Points
    ----------
    [shapes_points.7]\n
    Removes duplicate points.\n
    Arguments
    ----------
    - POINTS [`input shapes`] : Points
    - RESULT [`output shapes`] : Result
    - FIELD [`table field`] : Attribute
    - NUMERIC [`choice`] : Value Aggregation. Available Choices: [0] take values from the point to be kept [1] minimum values of all duplicates [2] maximum values of all duplicates [3] mean values of all duplicates Default: 0
    - METHOD [`choice`] : Point to Keep. Available Choices: [0] first point [1] last point [2] point with minimum attribute value [3] point with maximum attribute value Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_points', '7', 'Remove Duplicate Points')
    if Tool.is_Okay():
        Tool.Set_Input ('POINTS', POINTS)
        Tool.Set_Output('RESULT', RESULT)
        Tool.Set_Option('FIELD', FIELD)
        Tool.Set_Option('NUMERIC', NUMERIC)
        Tool.Set_Option('METHOD', METHOD)
        return Tool.Execute(Verbose)
    return False

def run_tool_shapes_points_7(POINTS=None, RESULT=None, FIELD=None, NUMERIC=None, METHOD=None, Verbose=2):
    '''
    Remove Duplicate Points
    ----------
    [shapes_points.7]\n
    Removes duplicate points.\n
    Arguments
    ----------
    - POINTS [`input shapes`] : Points
    - RESULT [`output shapes`] : Result
    - FIELD [`table field`] : Attribute
    - NUMERIC [`choice`] : Value Aggregation. Available Choices: [0] take values from the point to be kept [1] minimum values of all duplicates [2] maximum values of all duplicates [3] mean values of all duplicates Default: 0
    - METHOD [`choice`] : Point to Keep. Available Choices: [0] first point [1] last point [2] point with minimum attribute value [3] point with maximum attribute value Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_points', '7', 'Remove Duplicate Points')
    if Tool.is_Okay():
        Tool.Set_Input ('POINTS', POINTS)
        Tool.Set_Output('RESULT', RESULT)
        Tool.Set_Option('FIELD', FIELD)
        Tool.Set_Option('NUMERIC', NUMERIC)
        Tool.Set_Option('METHOD', METHOD)
        return Tool.Execute(Verbose)
    return False

def Clip_Points_with_Polygons(POINTS=None, POLYGONS=None, CLIPS=None, FIELD=None, METHOD=None, Verbose=2):
    '''
    Clip Points with Polygons
    ----------
    [shapes_points.8]\n
    Clip Points with Polygons\n
    Arguments
    ----------
    - POINTS [`input shapes`] : Points
    - POLYGONS [`input shapes`] : Polygons
    - CLIPS [`output shapes list`] : Clipped Points
    - FIELD [`table field`] : Add Attribute to Clipped Points
    - METHOD [`choice`] : Clipping Options. Available Choices: [0] one layer for all points [1] separate layer for each polygon Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_points', '8', 'Clip Points with Polygons')
    if Tool.is_Okay():
        Tool.Set_Input ('POINTS', POINTS)
        Tool.Set_Input ('POLYGONS', POLYGONS)
        Tool.Set_Output('CLIPS', CLIPS)
        Tool.Set_Option('FIELD', FIELD)
        Tool.Set_Option('METHOD', METHOD)
        return Tool.Execute(Verbose)
    return False

def run_tool_shapes_points_8(POINTS=None, POLYGONS=None, CLIPS=None, FIELD=None, METHOD=None, Verbose=2):
    '''
    Clip Points with Polygons
    ----------
    [shapes_points.8]\n
    Clip Points with Polygons\n
    Arguments
    ----------
    - POINTS [`input shapes`] : Points
    - POLYGONS [`input shapes`] : Polygons
    - CLIPS [`output shapes list`] : Clipped Points
    - FIELD [`table field`] : Add Attribute to Clipped Points
    - METHOD [`choice`] : Clipping Options. Available Choices: [0] one layer for all points [1] separate layer for each polygon Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_points', '8', 'Clip Points with Polygons')
    if Tool.is_Okay():
        Tool.Set_Input ('POINTS', POINTS)
        Tool.Set_Input ('POLYGONS', POLYGONS)
        Tool.Set_Output('CLIPS', CLIPS)
        Tool.Set_Option('FIELD', FIELD)
        Tool.Set_Option('METHOD', METHOD)
        return Tool.Execute(Verbose)
    return False

def Separate_points_by_direction(POINTS=None, OUTPUT=None, DIRECTIONS=None, TOLERANCE=None, Verbose=2):
    '''
    Separate points by direction
    ----------
    [shapes_points.9]\n
    Separates points by direction. Direction is determined as average direction of three consecutive points A, B, C. If the angle between the directions of A-B and B-C is higher than given tolerance angle the point is dropped. This tool has been designed to separate GPS tracks recorded by tractors while preparing a field.\n
    Arguments
    ----------
    - POINTS [`input shapes`] : Points
    - OUTPUT [`output shapes list`] : Output
    - DIRECTIONS [`integer number`] : Number of Directions. Minimum: 2 Default: 4
    - TOLERANCE [`floating point number`] : Tolerance (Degree). Minimum: 0.000000 Default: 5.000000

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_points', '9', 'Separate points by direction')
    if Tool.is_Okay():
        Tool.Set_Input ('POINTS', POINTS)
        Tool.Set_Output('OUTPUT', OUTPUT)
        Tool.Set_Option('DIRECTIONS', DIRECTIONS)
        Tool.Set_Option('TOLERANCE', TOLERANCE)
        return Tool.Execute(Verbose)
    return False

def run_tool_shapes_points_9(POINTS=None, OUTPUT=None, DIRECTIONS=None, TOLERANCE=None, Verbose=2):
    '''
    Separate points by direction
    ----------
    [shapes_points.9]\n
    Separates points by direction. Direction is determined as average direction of three consecutive points A, B, C. If the angle between the directions of A-B and B-C is higher than given tolerance angle the point is dropped. This tool has been designed to separate GPS tracks recorded by tractors while preparing a field.\n
    Arguments
    ----------
    - POINTS [`input shapes`] : Points
    - OUTPUT [`output shapes list`] : Output
    - DIRECTIONS [`integer number`] : Number of Directions. Minimum: 2 Default: 4
    - TOLERANCE [`floating point number`] : Tolerance (Degree). Minimum: 0.000000 Default: 5.000000

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_points', '9', 'Separate points by direction')
    if Tool.is_Okay():
        Tool.Set_Input ('POINTS', POINTS)
        Tool.Set_Output('OUTPUT', OUTPUT)
        Tool.Set_Option('DIRECTIONS', DIRECTIONS)
        Tool.Set_Option('TOLERANCE', TOLERANCE)
        return Tool.Execute(Verbose)
    return False

def Add_Polygon_Attributes_to_Points(INPUT=None, POLYGONS=None, OUTPUT=None, FIELDS=None, Verbose=2):
    '''
    Add Polygon Attributes to Points
    ----------
    [shapes_points.10]\n
    Spatial join for points. Retrieves for each point the selected attributes of the polygon that contains the point.\n
    Arguments
    ----------
    - INPUT [`input shapes`] : Points
    - POLYGONS [`input shapes`] : Polygons
    - OUTPUT [`output shapes`] : Result
    - FIELDS [`table fields`] : Attributes. Attributes to add. Select none to add all

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_points', '10', 'Add Polygon Attributes to Points')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Input ('POLYGONS', POLYGONS)
        Tool.Set_Output('OUTPUT', OUTPUT)
        Tool.Set_Option('FIELDS', FIELDS)
        return Tool.Execute(Verbose)
    return False

def run_tool_shapes_points_10(INPUT=None, POLYGONS=None, OUTPUT=None, FIELDS=None, Verbose=2):
    '''
    Add Polygon Attributes to Points
    ----------
    [shapes_points.10]\n
    Spatial join for points. Retrieves for each point the selected attributes of the polygon that contains the point.\n
    Arguments
    ----------
    - INPUT [`input shapes`] : Points
    - POLYGONS [`input shapes`] : Polygons
    - OUTPUT [`output shapes`] : Result
    - FIELDS [`table fields`] : Attributes. Attributes to add. Select none to add all

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_points', '10', 'Add Polygon Attributes to Points')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Input ('POLYGONS', POLYGONS)
        Tool.Set_Output('OUTPUT', OUTPUT)
        Tool.Set_Option('FIELDS', FIELDS)
        return Tool.Execute(Verbose)
    return False

def Points_Filter(POINTS=None, FILTER=None, FIELD=None, RADIUS=None, MINNUM=None, MAXNUM=None, QUADRANTS=None, METHOD=None, TOLERANCE=None, PERCENT=None, Verbose=2):
    '''
    Points Filter
    ----------
    [shapes_points.11]\n
    Points Filter\n
    Arguments
    ----------
    - POINTS [`input shapes`] : Points
    - FILTER [`output shapes`] : Filtered Points
    - FIELD [`table field`] : Attribute
    - RADIUS [`floating point number`] : Radius. Minimum: 0.000000 Default: 1.000000
    - MINNUM [`integer number`] : Minimum Number of Points. Minimum: 0 Default: 0 only points with given minimum number of points in search radius will be processed
    - MAXNUM [`integer number`] : Maximum Number of Points. Minimum: 0 Default: 0 Number of nearest points, which will be evaluated for filtering. Set to zero to investigate all points in search radius.
    - QUADRANTS [`boolean`] : Quadrants. Default: 0
    - METHOD [`choice`] : Filter Criterion. Available Choices: [0] keep maxima (with tolerance) [1] keep minima (with tolerance) [2] remove maxima (with tolerance) [3] remove minima (with tolerance) [4] remove below percentile [5] remove above percentile Default: 0
    - TOLERANCE [`floating point number`] : Tolerance. Minimum: 0.000000 Default: 0.000000
    - PERCENT [`floating point number`] : Percentile. Minimum: 0.000000 Maximum: 100.000000 Default: 50.000000

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_points', '11', 'Points Filter')
    if Tool.is_Okay():
        Tool.Set_Input ('POINTS', POINTS)
        Tool.Set_Output('FILTER', FILTER)
        Tool.Set_Option('FIELD', FIELD)
        Tool.Set_Option('RADIUS', RADIUS)
        Tool.Set_Option('MINNUM', MINNUM)
        Tool.Set_Option('MAXNUM', MAXNUM)
        Tool.Set_Option('QUADRANTS', QUADRANTS)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('TOLERANCE', TOLERANCE)
        Tool.Set_Option('PERCENT', PERCENT)
        return Tool.Execute(Verbose)
    return False

def run_tool_shapes_points_11(POINTS=None, FILTER=None, FIELD=None, RADIUS=None, MINNUM=None, MAXNUM=None, QUADRANTS=None, METHOD=None, TOLERANCE=None, PERCENT=None, Verbose=2):
    '''
    Points Filter
    ----------
    [shapes_points.11]\n
    Points Filter\n
    Arguments
    ----------
    - POINTS [`input shapes`] : Points
    - FILTER [`output shapes`] : Filtered Points
    - FIELD [`table field`] : Attribute
    - RADIUS [`floating point number`] : Radius. Minimum: 0.000000 Default: 1.000000
    - MINNUM [`integer number`] : Minimum Number of Points. Minimum: 0 Default: 0 only points with given minimum number of points in search radius will be processed
    - MAXNUM [`integer number`] : Maximum Number of Points. Minimum: 0 Default: 0 Number of nearest points, which will be evaluated for filtering. Set to zero to investigate all points in search radius.
    - QUADRANTS [`boolean`] : Quadrants. Default: 0
    - METHOD [`choice`] : Filter Criterion. Available Choices: [0] keep maxima (with tolerance) [1] keep minima (with tolerance) [2] remove maxima (with tolerance) [3] remove minima (with tolerance) [4] remove below percentile [5] remove above percentile Default: 0
    - TOLERANCE [`floating point number`] : Tolerance. Minimum: 0.000000 Default: 0.000000
    - PERCENT [`floating point number`] : Percentile. Minimum: 0.000000 Maximum: 100.000000 Default: 50.000000

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_points', '11', 'Points Filter')
    if Tool.is_Okay():
        Tool.Set_Input ('POINTS', POINTS)
        Tool.Set_Output('FILTER', FILTER)
        Tool.Set_Option('FIELD', FIELD)
        Tool.Set_Option('RADIUS', RADIUS)
        Tool.Set_Option('MINNUM', MINNUM)
        Tool.Set_Option('MAXNUM', MAXNUM)
        Tool.Set_Option('QUADRANTS', QUADRANTS)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('TOLERANCE', TOLERANCE)
        Tool.Set_Option('PERCENT', PERCENT)
        return Tool.Execute(Verbose)
    return False

def Convex_Hull(SHAPES=None, HULLS=None, BOXES=None, POLYPOINTS=None, POLYGONCVX=None, Verbose=2):
    '''
    Convex Hull
    ----------
    [shapes_points.12]\n
    Implementation of 'Andrew's Monotone Chain Algorithm' for convex hull construction.\n
    Arguments
    ----------
    - SHAPES [`input shapes`] : Points
    - HULLS [`output shapes`] : Convex Hull
    - BOXES [`output shapes`] : Minimum Bounding Box
    - POLYPOINTS [`choice`] : Hull Construction. Available Choices: [0] one hull for all shapes [1] one hull per shape [2] one hull per shape part Default: 1 This option does not apply to simple point layers.
    - POLYGONCVX [`boolean`] : Polygon Convexity. Default: 0 Describes a polygon's compactness as ratio of its area to its hull's area.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_points', '12', 'Convex Hull')
    if Tool.is_Okay():
        Tool.Set_Input ('SHAPES', SHAPES)
        Tool.Set_Output('HULLS', HULLS)
        Tool.Set_Output('BOXES', BOXES)
        Tool.Set_Option('POLYPOINTS', POLYPOINTS)
        Tool.Set_Option('POLYGONCVX', POLYGONCVX)
        return Tool.Execute(Verbose)
    return False

def run_tool_shapes_points_12(SHAPES=None, HULLS=None, BOXES=None, POLYPOINTS=None, POLYGONCVX=None, Verbose=2):
    '''
    Convex Hull
    ----------
    [shapes_points.12]\n
    Implementation of 'Andrew's Monotone Chain Algorithm' for convex hull construction.\n
    Arguments
    ----------
    - SHAPES [`input shapes`] : Points
    - HULLS [`output shapes`] : Convex Hull
    - BOXES [`output shapes`] : Minimum Bounding Box
    - POLYPOINTS [`choice`] : Hull Construction. Available Choices: [0] one hull for all shapes [1] one hull per shape [2] one hull per shape part Default: 1 This option does not apply to simple point layers.
    - POLYGONCVX [`boolean`] : Polygon Convexity. Default: 0 Describes a polygon's compactness as ratio of its area to its hull's area.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_points', '12', 'Convex Hull')
    if Tool.is_Okay():
        Tool.Set_Input ('SHAPES', SHAPES)
        Tool.Set_Output('HULLS', HULLS)
        Tool.Set_Output('BOXES', BOXES)
        Tool.Set_Option('POLYPOINTS', POLYPOINTS)
        Tool.Set_Option('POLYGONCVX', POLYGONCVX)
        return Tool.Execute(Verbose)
    return False

def Point_Thinning(POINTS=None, THINNED=None, THINNED_PC=None, FIELD=None, OUTPUT_PC=None, RESOLUTION=None, METHOD=None, Verbose=2):
    '''
    Point Thinning
    ----------
    [shapes_points.14]\n
    The Points Thinning tool aggregates points at a level that fits the specified resolution. The information of those points that become aggregated is based on basic statistics, i.e. mean values for coordinates and mean, minimum, maximum, standard deviation for the selected attribute. Due to the underlying spatial structure the quadtree and the raster method lead to differing, though comparable results.\n
    Arguments
    ----------
    - POINTS [`input shapes`] : Points
    - THINNED [`output shapes`] : Thinned Points
    - THINNED_PC [`output point cloud`] : Thinned Points
    - FIELD [`table field`] : Attribute
    - OUTPUT_PC [`boolean`] : Output to Point Cloud. Default: 0
    - RESOLUTION [`floating point number`] : Resolution. Minimum: 0.000000 Default: 1.000000
    - METHOD [`choice`] : Method. Available Choices: [0] quadtree [1] raster Default: 1

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_points', '14', 'Point Thinning')
    if Tool.is_Okay():
        Tool.Set_Input ('POINTS', POINTS)
        Tool.Set_Output('THINNED', THINNED)
        Tool.Set_Output('THINNED_PC', THINNED_PC)
        Tool.Set_Option('FIELD', FIELD)
        Tool.Set_Option('OUTPUT_PC', OUTPUT_PC)
        Tool.Set_Option('RESOLUTION', RESOLUTION)
        Tool.Set_Option('METHOD', METHOD)
        return Tool.Execute(Verbose)
    return False

def run_tool_shapes_points_14(POINTS=None, THINNED=None, THINNED_PC=None, FIELD=None, OUTPUT_PC=None, RESOLUTION=None, METHOD=None, Verbose=2):
    '''
    Point Thinning
    ----------
    [shapes_points.14]\n
    The Points Thinning tool aggregates points at a level that fits the specified resolution. The information of those points that become aggregated is based on basic statistics, i.e. mean values for coordinates and mean, minimum, maximum, standard deviation for the selected attribute. Due to the underlying spatial structure the quadtree and the raster method lead to differing, though comparable results.\n
    Arguments
    ----------
    - POINTS [`input shapes`] : Points
    - THINNED [`output shapes`] : Thinned Points
    - THINNED_PC [`output point cloud`] : Thinned Points
    - FIELD [`table field`] : Attribute
    - OUTPUT_PC [`boolean`] : Output to Point Cloud. Default: 0
    - RESOLUTION [`floating point number`] : Resolution. Minimum: 0.000000 Default: 1.000000
    - METHOD [`choice`] : Method. Available Choices: [0] quadtree [1] raster Default: 1

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_points', '14', 'Point Thinning')
    if Tool.is_Okay():
        Tool.Set_Input ('POINTS', POINTS)
        Tool.Set_Output('THINNED', THINNED)
        Tool.Set_Output('THINNED_PC', THINNED_PC)
        Tool.Set_Option('FIELD', FIELD)
        Tool.Set_Option('OUTPUT_PC', OUTPUT_PC)
        Tool.Set_Option('RESOLUTION', RESOLUTION)
        Tool.Set_Option('METHOD', METHOD)
        return Tool.Execute(Verbose)
    return False

def Convert_Multipoints_to_Points(MULTIPOINTS=None, POINTS=None, ADD_INDEX=None, Verbose=2):
    '''
    Convert Multipoints to Points
    ----------
    [shapes_points.15]\n
    Converts multipoints to points.\n
    Arguments
    ----------
    - MULTIPOINTS [`input shapes`] : Multipoints
    - POINTS [`output shapes`] : Points
    - ADD_INDEX [`boolean`] : Add Part and Point Index. Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_points', '15', 'Convert Multipoints to Points')
    if Tool.is_Okay():
        Tool.Set_Input ('MULTIPOINTS', MULTIPOINTS)
        Tool.Set_Output('POINTS', POINTS)
        Tool.Set_Option('ADD_INDEX', ADD_INDEX)
        return Tool.Execute(Verbose)
    return False

def run_tool_shapes_points_15(MULTIPOINTS=None, POINTS=None, ADD_INDEX=None, Verbose=2):
    '''
    Convert Multipoints to Points
    ----------
    [shapes_points.15]\n
    Converts multipoints to points.\n
    Arguments
    ----------
    - MULTIPOINTS [`input shapes`] : Multipoints
    - POINTS [`output shapes`] : Points
    - ADD_INDEX [`boolean`] : Add Part and Point Index. Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_points', '15', 'Convert Multipoints to Points')
    if Tool.is_Okay():
        Tool.Set_Input ('MULTIPOINTS', MULTIPOINTS)
        Tool.Set_Output('POINTS', POINTS)
        Tool.Set_Option('ADD_INDEX', ADD_INDEX)
        return Tool.Execute(Verbose)
    return False

def Thiessen_Polygons(POINTS=None, POLYGONS=None, FRAME=None, Verbose=2):
    '''
    Thiessen Polygons
    ----------
    [shapes_points.16]\n
    Creates Thiessen or Voronoi polygons for given point data set.\n
    Arguments
    ----------
    - POINTS [`input shapes`] : Points
    - POLYGONS [`output shapes`] : Polygons
    - FRAME [`floating point number`] : Frame Size. Minimum: 0.000000 Default: 10.000000

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_points', '16', 'Thiessen Polygons')
    if Tool.is_Okay():
        Tool.Set_Input ('POINTS', POINTS)
        Tool.Set_Output('POLYGONS', POLYGONS)
        Tool.Set_Option('FRAME', FRAME)
        return Tool.Execute(Verbose)
    return False

def run_tool_shapes_points_16(POINTS=None, POLYGONS=None, FRAME=None, Verbose=2):
    '''
    Thiessen Polygons
    ----------
    [shapes_points.16]\n
    Creates Thiessen or Voronoi polygons for given point data set.\n
    Arguments
    ----------
    - POINTS [`input shapes`] : Points
    - POLYGONS [`output shapes`] : Polygons
    - FRAME [`floating point number`] : Frame Size. Minimum: 0.000000 Default: 10.000000

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_points', '16', 'Thiessen Polygons')
    if Tool.is_Okay():
        Tool.Set_Input ('POINTS', POINTS)
        Tool.Set_Output('POLYGONS', POLYGONS)
        Tool.Set_Option('FRAME', FRAME)
        return Tool.Execute(Verbose)
    return False

def Aggregate_Point_Observations(REFERENCE=None, OBSERVATIONS=None, AGGREGATED=None, REFERENCE_ID=None, X=None, Y=None, TRACK=None, DATE=None, TIME=None, PARAMETER=None, TIME_SPAN=None, FIX_TIME=None, OFF_TIME=None, EPS_TIME=None, EPS_SPACE=None, VERBOSE=None, POLAR=None, Verbose=2):
    '''
    Aggregate Point Observations
    ----------
    [shapes_points.17]\n
    Aggregate Point Observations\n
    Arguments
    ----------
    - REFERENCE [`input shapes`] : Reference Points
    - OBSERVATIONS [`input table`] : Observations
    - AGGREGATED [`output table`] : Aggregated
    - REFERENCE_ID [`table field`] : ID
    - X [`table field`] : X
    - Y [`table field`] : Y
    - TRACK [`table field`] : Track
    - DATE [`table field`] : Date
    - TIME [`table field`] : Time. expected to be the second of day
    - PARAMETER [`table field`] : Parameter
    - TIME_SPAN [`choice`] : Time Span Aggregation. Available Choices: [0] ignore [1] floating [2] fixed Default: 1
    - FIX_TIME [`floating point number`] : Fixed Time Span (minutes). Minimum: 0.000000 Default: 20.000000 ignored if set to zero
    - OFF_TIME [`floating point number`] : Fixed Time Span Offset (minutes). Default: -10.000000 offset in minutes relative to 00:00 (midnight)
    - EPS_TIME [`floating point number`] : Maximum Time Span (Seconds). Minimum: 0.000000 Default: 60.000000 ignored if set to zero
    - EPS_SPACE [`floating point number`] : Maximum Distance. Minimum: 0.000000 Default: 100.000000 given as map units or meters if polar coordinates switch is on; ignored if set to zero
    - VERBOSE [`boolean`] : Verbose. Default: 0
    - POLAR [`boolean`] : Polar Coordinates. Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_points', '17', 'Aggregate Point Observations')
    if Tool.is_Okay():
        Tool.Set_Input ('REFERENCE', REFERENCE)
        Tool.Set_Input ('OBSERVATIONS', OBSERVATIONS)
        Tool.Set_Output('AGGREGATED', AGGREGATED)
        Tool.Set_Option('REFERENCE_ID', REFERENCE_ID)
        Tool.Set_Option('X', X)
        Tool.Set_Option('Y', Y)
        Tool.Set_Option('TRACK', TRACK)
        Tool.Set_Option('DATE', DATE)
        Tool.Set_Option('TIME', TIME)
        Tool.Set_Option('PARAMETER', PARAMETER)
        Tool.Set_Option('TIME_SPAN', TIME_SPAN)
        Tool.Set_Option('FIX_TIME', FIX_TIME)
        Tool.Set_Option('OFF_TIME', OFF_TIME)
        Tool.Set_Option('EPS_TIME', EPS_TIME)
        Tool.Set_Option('EPS_SPACE', EPS_SPACE)
        Tool.Set_Option('VERBOSE', VERBOSE)
        Tool.Set_Option('POLAR', POLAR)
        return Tool.Execute(Verbose)
    return False

def run_tool_shapes_points_17(REFERENCE=None, OBSERVATIONS=None, AGGREGATED=None, REFERENCE_ID=None, X=None, Y=None, TRACK=None, DATE=None, TIME=None, PARAMETER=None, TIME_SPAN=None, FIX_TIME=None, OFF_TIME=None, EPS_TIME=None, EPS_SPACE=None, VERBOSE=None, POLAR=None, Verbose=2):
    '''
    Aggregate Point Observations
    ----------
    [shapes_points.17]\n
    Aggregate Point Observations\n
    Arguments
    ----------
    - REFERENCE [`input shapes`] : Reference Points
    - OBSERVATIONS [`input table`] : Observations
    - AGGREGATED [`output table`] : Aggregated
    - REFERENCE_ID [`table field`] : ID
    - X [`table field`] : X
    - Y [`table field`] : Y
    - TRACK [`table field`] : Track
    - DATE [`table field`] : Date
    - TIME [`table field`] : Time. expected to be the second of day
    - PARAMETER [`table field`] : Parameter
    - TIME_SPAN [`choice`] : Time Span Aggregation. Available Choices: [0] ignore [1] floating [2] fixed Default: 1
    - FIX_TIME [`floating point number`] : Fixed Time Span (minutes). Minimum: 0.000000 Default: 20.000000 ignored if set to zero
    - OFF_TIME [`floating point number`] : Fixed Time Span Offset (minutes). Default: -10.000000 offset in minutes relative to 00:00 (midnight)
    - EPS_TIME [`floating point number`] : Maximum Time Span (Seconds). Minimum: 0.000000 Default: 60.000000 ignored if set to zero
    - EPS_SPACE [`floating point number`] : Maximum Distance. Minimum: 0.000000 Default: 100.000000 given as map units or meters if polar coordinates switch is on; ignored if set to zero
    - VERBOSE [`boolean`] : Verbose. Default: 0
    - POLAR [`boolean`] : Polar Coordinates. Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_points', '17', 'Aggregate Point Observations')
    if Tool.is_Okay():
        Tool.Set_Input ('REFERENCE', REFERENCE)
        Tool.Set_Input ('OBSERVATIONS', OBSERVATIONS)
        Tool.Set_Output('AGGREGATED', AGGREGATED)
        Tool.Set_Option('REFERENCE_ID', REFERENCE_ID)
        Tool.Set_Option('X', X)
        Tool.Set_Option('Y', Y)
        Tool.Set_Option('TRACK', TRACK)
        Tool.Set_Option('DATE', DATE)
        Tool.Set_Option('TIME', TIME)
        Tool.Set_Option('PARAMETER', PARAMETER)
        Tool.Set_Option('TIME_SPAN', TIME_SPAN)
        Tool.Set_Option('FIX_TIME', FIX_TIME)
        Tool.Set_Option('OFF_TIME', OFF_TIME)
        Tool.Set_Option('EPS_TIME', EPS_TIME)
        Tool.Set_Option('EPS_SPACE', EPS_SPACE)
        Tool.Set_Option('VERBOSE', VERBOSE)
        Tool.Set_Option('POLAR', POLAR)
        return Tool.Execute(Verbose)
    return False

def Snap_Points_to_Points(INPUT=None, SNAP=None, OUTPUT=None, MOVES=None, DISTANCE=None, Verbose=2):
    '''
    Snap Points to Points
    ----------
    [shapes_points.18]\n
    Snap Points to Points\n
    Arguments
    ----------
    - INPUT [`input shapes`] : Points
    - SNAP [`input shapes`] : Snap Features
    - OUTPUT [`output shapes`] : Result
    - MOVES [`output shapes`] : Moves
    - DISTANCE [`floating point number`] : Search Distance. Minimum: 0.000000 Default: 0.000000

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_points', '18', 'Snap Points to Points')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Input ('SNAP', SNAP)
        Tool.Set_Output('OUTPUT', OUTPUT)
        Tool.Set_Output('MOVES', MOVES)
        Tool.Set_Option('DISTANCE', DISTANCE)
        return Tool.Execute(Verbose)
    return False

def run_tool_shapes_points_18(INPUT=None, SNAP=None, OUTPUT=None, MOVES=None, DISTANCE=None, Verbose=2):
    '''
    Snap Points to Points
    ----------
    [shapes_points.18]\n
    Snap Points to Points\n
    Arguments
    ----------
    - INPUT [`input shapes`] : Points
    - SNAP [`input shapes`] : Snap Features
    - OUTPUT [`output shapes`] : Result
    - MOVES [`output shapes`] : Moves
    - DISTANCE [`floating point number`] : Search Distance. Minimum: 0.000000 Default: 0.000000

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_points', '18', 'Snap Points to Points')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Input ('SNAP', SNAP)
        Tool.Set_Output('OUTPUT', OUTPUT)
        Tool.Set_Output('MOVES', MOVES)
        Tool.Set_Option('DISTANCE', DISTANCE)
        return Tool.Execute(Verbose)
    return False

def Snap_Points_to_Lines(INPUT=None, SNAP=None, OUTPUT=None, MOVES=None, DISTANCE=None, Verbose=2):
    '''
    Snap Points to Lines
    ----------
    [shapes_points.19]\n
    Snap Points to Lines\n
    Arguments
    ----------
    - INPUT [`input shapes`] : Points
    - SNAP [`input shapes`] : Snap Features
    - OUTPUT [`output shapes`] : Result
    - MOVES [`output shapes`] : Moves
    - DISTANCE [`floating point number`] : Search Distance. Minimum: 0.000000 Default: 0.000000

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_points', '19', 'Snap Points to Lines')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Input ('SNAP', SNAP)
        Tool.Set_Output('OUTPUT', OUTPUT)
        Tool.Set_Output('MOVES', MOVES)
        Tool.Set_Option('DISTANCE', DISTANCE)
        return Tool.Execute(Verbose)
    return False

def run_tool_shapes_points_19(INPUT=None, SNAP=None, OUTPUT=None, MOVES=None, DISTANCE=None, Verbose=2):
    '''
    Snap Points to Lines
    ----------
    [shapes_points.19]\n
    Snap Points to Lines\n
    Arguments
    ----------
    - INPUT [`input shapes`] : Points
    - SNAP [`input shapes`] : Snap Features
    - OUTPUT [`output shapes`] : Result
    - MOVES [`output shapes`] : Moves
    - DISTANCE [`floating point number`] : Search Distance. Minimum: 0.000000 Default: 0.000000

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_points', '19', 'Snap Points to Lines')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Input ('SNAP', SNAP)
        Tool.Set_Output('OUTPUT', OUTPUT)
        Tool.Set_Output('MOVES', MOVES)
        Tool.Set_Option('DISTANCE', DISTANCE)
        return Tool.Execute(Verbose)
    return False

def Snap_Points_to_Grid(INPUT=None, GRID=None, OUTPUT=None, MOVES=None, DISTANCE=None, SHAPE=None, EXTREME=None, Verbose=2):
    '''
    Snap Points to Grid
    ----------
    [shapes_points.20]\n
    Moves all points to grid cell positions that have the highest orlowest value respectively within the given search distance around each point.\n
    Arguments
    ----------
    - INPUT [`input shapes`] : Points
    - GRID [`input grid`] : Grid
    - OUTPUT [`output shapes`] : Result
    - MOVES [`output shapes`] : Moves
    - DISTANCE [`floating point number`] : Search Distance. Minimum: 0.000000 Default: 0.000000 map units
    - SHAPE [`choice`] : Search Shape. Available Choices: [0] circle [1] square Default: 0
    - EXTREME [`choice`] : Extreme. Available Choices: [0] minimum [1] maximum Default: 1

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_points', '20', 'Snap Points to Grid')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Input ('GRID', GRID)
        Tool.Set_Output('OUTPUT', OUTPUT)
        Tool.Set_Output('MOVES', MOVES)
        Tool.Set_Option('DISTANCE', DISTANCE)
        Tool.Set_Option('SHAPE', SHAPE)
        Tool.Set_Option('EXTREME', EXTREME)
        return Tool.Execute(Verbose)
    return False

def run_tool_shapes_points_20(INPUT=None, GRID=None, OUTPUT=None, MOVES=None, DISTANCE=None, SHAPE=None, EXTREME=None, Verbose=2):
    '''
    Snap Points to Grid
    ----------
    [shapes_points.20]\n
    Moves all points to grid cell positions that have the highest orlowest value respectively within the given search distance around each point.\n
    Arguments
    ----------
    - INPUT [`input shapes`] : Points
    - GRID [`input grid`] : Grid
    - OUTPUT [`output shapes`] : Result
    - MOVES [`output shapes`] : Moves
    - DISTANCE [`floating point number`] : Search Distance. Minimum: 0.000000 Default: 0.000000 map units
    - SHAPE [`choice`] : Search Shape. Available Choices: [0] circle [1] square Default: 0
    - EXTREME [`choice`] : Extreme. Available Choices: [0] minimum [1] maximum Default: 1

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_points', '20', 'Snap Points to Grid')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Input ('GRID', GRID)
        Tool.Set_Output('OUTPUT', OUTPUT)
        Tool.Set_Output('MOVES', MOVES)
        Tool.Set_Option('DISTANCE', DISTANCE)
        Tool.Set_Option('SHAPE', SHAPE)
        Tool.Set_Option('EXTREME', EXTREME)
        return Tool.Execute(Verbose)
    return False

def Create_Random_Points(SHAPES=None, POLYGONS=None, POINTS=None, EXTENT=None, GRIDSYSTEM=None, XMIN=None, XMAX=None, YMIN=None, YMAX=None, NX=None, NY=None, BUFFER=None, COUNT=None, DISTRIBUTE=None, ITERATIONS=None, DISTANCE=None, Verbose=2):
    '''
    Create Random Points
    ----------
    [shapes_points.21]\n
    Create a layer with randomly distributed points.\n
    Arguments
    ----------
    - SHAPES [`input shapes`] : Shapes Extent
    - POLYGONS [`input shapes`] : Polygons
    - POINTS [`output shapes`] : Points
    - EXTENT [`choice`] : Target Area. Available Choices: [0] user defined [1] grid system [2] shapes extent [3] polygons Default: 0
    - GRIDSYSTEM [`grid system`] : Grid System Extent
    - XMIN [`floating point number`] : Left. Default: 0.000000
    - XMAX [`floating point number`] : Right. Default: 0.000000
    - YMIN [`floating point number`] : Bottom. Default: 0.000000
    - YMAX [`floating point number`] : Top. Default: 0.000000
    - NX [`integer number`] : Columns. Minimum: 1 Default: 1
    - NY [`integer number`] : Rows. Minimum: 1 Default: 1
    - BUFFER [`floating point number`] : Buffer. Minimum: 0.000000 Default: 0.000000 add buffer (map units) to extent
    - COUNT [`integer number`] : Number of Points. Minimum: 1 Default: 100
    - DISTRIBUTE [`choice`] : Number for.... Available Choices: [0] all polygons [1] each polygon Default: 0
    - ITERATIONS [`integer number`] : Iterations. Minimum: 1 Default: 1000 Maximum number of iterations to find a suitable place for a point.
    - DISTANCE [`floating point number`] : Distance. Minimum: 0.000000 Default: 0.000000 Minimum distance a point should keep to each other.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_points', '21', 'Create Random Points')
    if Tool.is_Okay():
        Tool.Set_Input ('SHAPES', SHAPES)
        Tool.Set_Input ('POLYGONS', POLYGONS)
        Tool.Set_Output('POINTS', POINTS)
        Tool.Set_Option('EXTENT', EXTENT)
        Tool.Set_Option('GRIDSYSTEM', GRIDSYSTEM)
        Tool.Set_Option('XMIN', XMIN)
        Tool.Set_Option('XMAX', XMAX)
        Tool.Set_Option('YMIN', YMIN)
        Tool.Set_Option('YMAX', YMAX)
        Tool.Set_Option('NX', NX)
        Tool.Set_Option('NY', NY)
        Tool.Set_Option('BUFFER', BUFFER)
        Tool.Set_Option('COUNT', COUNT)
        Tool.Set_Option('DISTRIBUTE', DISTRIBUTE)
        Tool.Set_Option('ITERATIONS', ITERATIONS)
        Tool.Set_Option('DISTANCE', DISTANCE)
        return Tool.Execute(Verbose)
    return False

def run_tool_shapes_points_21(SHAPES=None, POLYGONS=None, POINTS=None, EXTENT=None, GRIDSYSTEM=None, XMIN=None, XMAX=None, YMIN=None, YMAX=None, NX=None, NY=None, BUFFER=None, COUNT=None, DISTRIBUTE=None, ITERATIONS=None, DISTANCE=None, Verbose=2):
    '''
    Create Random Points
    ----------
    [shapes_points.21]\n
    Create a layer with randomly distributed points.\n
    Arguments
    ----------
    - SHAPES [`input shapes`] : Shapes Extent
    - POLYGONS [`input shapes`] : Polygons
    - POINTS [`output shapes`] : Points
    - EXTENT [`choice`] : Target Area. Available Choices: [0] user defined [1] grid system [2] shapes extent [3] polygons Default: 0
    - GRIDSYSTEM [`grid system`] : Grid System Extent
    - XMIN [`floating point number`] : Left. Default: 0.000000
    - XMAX [`floating point number`] : Right. Default: 0.000000
    - YMIN [`floating point number`] : Bottom. Default: 0.000000
    - YMAX [`floating point number`] : Top. Default: 0.000000
    - NX [`integer number`] : Columns. Minimum: 1 Default: 1
    - NY [`integer number`] : Rows. Minimum: 1 Default: 1
    - BUFFER [`floating point number`] : Buffer. Minimum: 0.000000 Default: 0.000000 add buffer (map units) to extent
    - COUNT [`integer number`] : Number of Points. Minimum: 1 Default: 100
    - DISTRIBUTE [`choice`] : Number for.... Available Choices: [0] all polygons [1] each polygon Default: 0
    - ITERATIONS [`integer number`] : Iterations. Minimum: 1 Default: 1000 Maximum number of iterations to find a suitable place for a point.
    - DISTANCE [`floating point number`] : Distance. Minimum: 0.000000 Default: 0.000000 Minimum distance a point should keep to each other.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_points', '21', 'Create Random Points')
    if Tool.is_Okay():
        Tool.Set_Input ('SHAPES', SHAPES)
        Tool.Set_Input ('POLYGONS', POLYGONS)
        Tool.Set_Output('POINTS', POINTS)
        Tool.Set_Option('EXTENT', EXTENT)
        Tool.Set_Option('GRIDSYSTEM', GRIDSYSTEM)
        Tool.Set_Option('XMIN', XMIN)
        Tool.Set_Option('XMAX', XMAX)
        Tool.Set_Option('YMIN', YMIN)
        Tool.Set_Option('YMAX', YMAX)
        Tool.Set_Option('NX', NX)
        Tool.Set_Option('NY', NY)
        Tool.Set_Option('BUFFER', BUFFER)
        Tool.Set_Option('COUNT', COUNT)
        Tool.Set_Option('DISTRIBUTE', DISTRIBUTE)
        Tool.Set_Option('ITERATIONS', ITERATIONS)
        Tool.Set_Option('DISTANCE', DISTANCE)
        return Tool.Execute(Verbose)
    return False

def Snap_Points_to_Polygons(INPUT=None, SNAP=None, OUTPUT=None, MOVES=None, DISTANCE=None, Verbose=2):
    '''
    Snap Points to Polygons
    ----------
    [shapes_points.22]\n
    Snap Points to Polygons\n
    Arguments
    ----------
    - INPUT [`input shapes`] : Points
    - SNAP [`input shapes`] : Snap Features
    - OUTPUT [`output shapes`] : Result
    - MOVES [`output shapes`] : Moves
    - DISTANCE [`floating point number`] : Search Distance. Minimum: 0.000000 Default: 0.000000

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_points', '22', 'Snap Points to Polygons')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Input ('SNAP', SNAP)
        Tool.Set_Output('OUTPUT', OUTPUT)
        Tool.Set_Output('MOVES', MOVES)
        Tool.Set_Option('DISTANCE', DISTANCE)
        return Tool.Execute(Verbose)
    return False

def run_tool_shapes_points_22(INPUT=None, SNAP=None, OUTPUT=None, MOVES=None, DISTANCE=None, Verbose=2):
    '''
    Snap Points to Polygons
    ----------
    [shapes_points.22]\n
    Snap Points to Polygons\n
    Arguments
    ----------
    - INPUT [`input shapes`] : Points
    - SNAP [`input shapes`] : Snap Features
    - OUTPUT [`output shapes`] : Result
    - MOVES [`output shapes`] : Moves
    - DISTANCE [`floating point number`] : Search Distance. Minimum: 0.000000 Default: 0.000000

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_points', '22', 'Snap Points to Polygons')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Input ('SNAP', SNAP)
        Tool.Set_Output('OUTPUT', OUTPUT)
        Tool.Set_Output('MOVES', MOVES)
        Tool.Set_Option('DISTANCE', DISTANCE)
        return Tool.Execute(Verbose)
    return False

def _3D_Points_Selection(LOWER=None, UPPER=None, POINTS=None, COPY=None, Z_FIELD=None, Verbose=2):
    '''
    3D Points Selection
    ----------
    [shapes_points.23]\n
    Select points with three dimensional coordinates that fall between a given upper and lower surface, both provided as grids.\n
    Arguments
    ----------
    - LOWER [`input grid`] : Lower Surface
    - UPPER [`input grid`] : Upper Surface
    - POINTS [`input shapes`] : Points
    - COPY [`output shapes`] : Copy Selection
    - Z_FIELD [`table field`] : Z

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_points', '23', '3D Points Selection')
    if Tool.is_Okay():
        Tool.Set_Input ('LOWER', LOWER)
        Tool.Set_Input ('UPPER', UPPER)
        Tool.Set_Input ('POINTS', POINTS)
        Tool.Set_Output('COPY', COPY)
        Tool.Set_Option('Z_FIELD', Z_FIELD)
        return Tool.Execute(Verbose)
    return False

def run_tool_shapes_points_23(LOWER=None, UPPER=None, POINTS=None, COPY=None, Z_FIELD=None, Verbose=2):
    '''
    3D Points Selection
    ----------
    [shapes_points.23]\n
    Select points with three dimensional coordinates that fall between a given upper and lower surface, both provided as grids.\n
    Arguments
    ----------
    - LOWER [`input grid`] : Lower Surface
    - UPPER [`input grid`] : Upper Surface
    - POINTS [`input shapes`] : Points
    - COPY [`output shapes`] : Copy Selection
    - Z_FIELD [`table field`] : Z

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_points', '23', '3D Points Selection')
    if Tool.is_Okay():
        Tool.Set_Input ('LOWER', LOWER)
        Tool.Set_Input ('UPPER', UPPER)
        Tool.Set_Input ('POINTS', POINTS)
        Tool.Set_Output('COPY', COPY)
        Tool.Set_Option('Z_FIELD', Z_FIELD)
        return Tool.Execute(Verbose)
    return False

def Point_to_Line_Distances(POINTS=None, LINES=None, RESULT=None, DISTANCES=None, LINE_ID=None, Verbose=2):
    '''
    Point to Line Distances
    ----------
    [shapes_points.24]\n
    Point to Line Distances\n
    Arguments
    ----------
    - POINTS [`input shapes`] : Points
    - LINES [`input shapes`] : Lines
    - RESULT [`output shapes`] : Result
    - DISTANCES [`output shapes`] : Distances
    - LINE_ID [`table field`] : Identifier

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_points', '24', 'Point to Line Distances')
    if Tool.is_Okay():
        Tool.Set_Input ('POINTS', POINTS)
        Tool.Set_Input ('LINES', LINES)
        Tool.Set_Output('RESULT', RESULT)
        Tool.Set_Output('DISTANCES', DISTANCES)
        Tool.Set_Option('LINE_ID', LINE_ID)
        return Tool.Execute(Verbose)
    return False

def run_tool_shapes_points_24(POINTS=None, LINES=None, RESULT=None, DISTANCES=None, LINE_ID=None, Verbose=2):
    '''
    Point to Line Distances
    ----------
    [shapes_points.24]\n
    Point to Line Distances\n
    Arguments
    ----------
    - POINTS [`input shapes`] : Points
    - LINES [`input shapes`] : Lines
    - RESULT [`output shapes`] : Result
    - DISTANCES [`output shapes`] : Distances
    - LINE_ID [`table field`] : Identifier

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_points', '24', 'Point to Line Distances')
    if Tool.is_Okay():
        Tool.Set_Input ('POINTS', POINTS)
        Tool.Set_Input ('LINES', LINES)
        Tool.Set_Output('RESULT', RESULT)
        Tool.Set_Output('DISTANCES', DISTANCES)
        Tool.Set_Option('LINE_ID', LINE_ID)
        return Tool.Execute(Verbose)
    return False

def Contour_Lines_from_Points(SHAPES=None, CONTOUR=None, ATTRIBUTE=None, CELL_SIZE=None, ZSTEP=None, Verbose=2):
    '''
    Contour Lines from Points
    ----------
    [shapes_points.points_to_contour]\n
    created from history\n
    Arguments
    ----------
    - SHAPES [`input shapes`] : Points
    - CONTOUR [`output shapes`] : Contour Lines
    - ATTRIBUTE [`table field`] : Attribute. attribute to become interpolated
    - CELL_SIZE [`floating point number`] : Precision. Default: 10.000000 this is the internal grid cell size determining the precision of contours
    - ZSTEP [`floating point number`] : Equidistance. Default: 10.000000

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_points', 'points_to_contour', 'Contour Lines from Points')
    if Tool.is_Okay():
        Tool.Set_Input ('SHAPES', SHAPES)
        Tool.Set_Output('CONTOUR', CONTOUR)
        Tool.Set_Option('ATTRIBUTE', ATTRIBUTE)
        Tool.Set_Option('CELL_SIZE', CELL_SIZE)
        Tool.Set_Option('ZSTEP', ZSTEP)
        return Tool.Execute(Verbose)
    return False

def run_tool_shapes_points_points_to_contour(SHAPES=None, CONTOUR=None, ATTRIBUTE=None, CELL_SIZE=None, ZSTEP=None, Verbose=2):
    '''
    Contour Lines from Points
    ----------
    [shapes_points.points_to_contour]\n
    created from history\n
    Arguments
    ----------
    - SHAPES [`input shapes`] : Points
    - CONTOUR [`output shapes`] : Contour Lines
    - ATTRIBUTE [`table field`] : Attribute. attribute to become interpolated
    - CELL_SIZE [`floating point number`] : Precision. Default: 10.000000 this is the internal grid cell size determining the precision of contours
    - ZSTEP [`floating point number`] : Equidistance. Default: 10.000000

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_points', 'points_to_contour', 'Contour Lines from Points')
    if Tool.is_Okay():
        Tool.Set_Input ('SHAPES', SHAPES)
        Tool.Set_Output('CONTOUR', CONTOUR)
        Tool.Set_Option('ATTRIBUTE', ATTRIBUTE)
        Tool.Set_Option('CELL_SIZE', CELL_SIZE)
        Tool.Set_Option('ZSTEP', ZSTEP)
        return Tool.Execute(Verbose)
    return False

