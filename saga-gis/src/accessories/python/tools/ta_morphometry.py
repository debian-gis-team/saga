#! /usr/bin/env python

'''
Python Interface to SAGA Tools Library
----------
- Category : Terrain Analysis
- Name     : Morphometry
- ID       : ta_morphometry

Description
----------
Tools for (grid based) digital terrain analysis.
'''

from PySAGA.helper import Tool_Wrapper

def Slope_Aspect_Curvature(ELEVATION=None, SLOPE=None, ASPECT=None, NORTHNESS=None, EASTNESS=None, C_GENE=None, C_PROF=None, C_PLAN=None, C_TANG=None, C_LONG=None, C_CROS=None, C_MINI=None, C_MAXI=None, C_TOTA=None, C_ROTO=None, METHOD=None, UNIT_SLOPE=None, UNIT_ASPECT=None, Verbose=2):
    '''
    Slope, Aspect, Curvature
    ----------
    [ta_morphometry.0]\n
    Calculates the local morphometric terrain parameters slope, aspect and if supported by the chosen method also the curvature. Besides tangential curvature also its horizontal and vertical components (i.e. plan and profile curvature) can be calculated.\n
    Arguments
    ----------
    - ELEVATION [`input grid`] : Elevation
    - SLOPE [`output grid`] : Slope
    - ASPECT [`output grid`] : Aspect. Starting with 0 for the North direction angles are increasing clockwise.
    - NORTHNESS [`output grid`] : Northness. The aspect's cosine.
    - EASTNESS [`output grid`] : Eastness. The aspect's sine.
    - C_GENE [`output grid`] : General Curvature
    - C_PROF [`output grid`] : Profile Curvature
    - C_PLAN [`output grid`] : Plan Curvature
    - C_TANG [`output grid`] : Tangential Curvature
    - C_LONG [`output grid`] : Longitudinal Curvature. Zevenbergen & Thorne (1987) refer to this as profile curvature
    - C_CROS [`output grid`] : Cross-Sectional Curvature. Zevenbergen & Thorne (1987) refer to this as plan curvature
    - C_MINI [`output grid`] : Minimal Curvature
    - C_MAXI [`output grid`] : Maximal Curvature
    - C_TOTA [`output grid`] : Total Curvature
    - C_ROTO [`output grid`] : Flow Line Curvature
    - METHOD [`choice`] : Method. Available Choices: [0] maximum slope (Travis et al. 1975) [1] maximum triangle slope (Tarboton 1997) [2] least squares fitted plane (Horn 1981, Costa-Cabral & Burgess 1996) [3] 6 parameter 2nd order polynom (Evans 1979) [4] 6 parameter 2nd order polynom (Heerdegen & Beran 1982) [5] 6 parameter 2nd order polynom (Bauer, Rohdenburg, Bork 1985) [6] 9 parameter 2nd order polynom (Zevenbergen & Thorne 1987) [7] 10 parameter 3rd order polynom (Haralick 1983) [8] 10 parameter 3rd order polynom (Florinsky 2009) Default: 6
    - UNIT_SLOPE [`choice`] : Unit. Available Choices: [0] radians [1] degree [2] percent rise Default: 0
    - UNIT_ASPECT [`choice`] : Unit. Available Choices: [0] radians [1] degree Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_morphometry', '0', 'Slope, Aspect, Curvature')
    if Tool.is_Okay():
        Tool.Set_Input ('ELEVATION', ELEVATION)
        Tool.Set_Output('SLOPE', SLOPE)
        Tool.Set_Output('ASPECT', ASPECT)
        Tool.Set_Output('NORTHNESS', NORTHNESS)
        Tool.Set_Output('EASTNESS', EASTNESS)
        Tool.Set_Output('C_GENE', C_GENE)
        Tool.Set_Output('C_PROF', C_PROF)
        Tool.Set_Output('C_PLAN', C_PLAN)
        Tool.Set_Output('C_TANG', C_TANG)
        Tool.Set_Output('C_LONG', C_LONG)
        Tool.Set_Output('C_CROS', C_CROS)
        Tool.Set_Output('C_MINI', C_MINI)
        Tool.Set_Output('C_MAXI', C_MAXI)
        Tool.Set_Output('C_TOTA', C_TOTA)
        Tool.Set_Output('C_ROTO', C_ROTO)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('UNIT_SLOPE', UNIT_SLOPE)
        Tool.Set_Option('UNIT_ASPECT', UNIT_ASPECT)
        return Tool.Execute(Verbose)
    return False

def run_tool_ta_morphometry_0(ELEVATION=None, SLOPE=None, ASPECT=None, NORTHNESS=None, EASTNESS=None, C_GENE=None, C_PROF=None, C_PLAN=None, C_TANG=None, C_LONG=None, C_CROS=None, C_MINI=None, C_MAXI=None, C_TOTA=None, C_ROTO=None, METHOD=None, UNIT_SLOPE=None, UNIT_ASPECT=None, Verbose=2):
    '''
    Slope, Aspect, Curvature
    ----------
    [ta_morphometry.0]\n
    Calculates the local morphometric terrain parameters slope, aspect and if supported by the chosen method also the curvature. Besides tangential curvature also its horizontal and vertical components (i.e. plan and profile curvature) can be calculated.\n
    Arguments
    ----------
    - ELEVATION [`input grid`] : Elevation
    - SLOPE [`output grid`] : Slope
    - ASPECT [`output grid`] : Aspect. Starting with 0 for the North direction angles are increasing clockwise.
    - NORTHNESS [`output grid`] : Northness. The aspect's cosine.
    - EASTNESS [`output grid`] : Eastness. The aspect's sine.
    - C_GENE [`output grid`] : General Curvature
    - C_PROF [`output grid`] : Profile Curvature
    - C_PLAN [`output grid`] : Plan Curvature
    - C_TANG [`output grid`] : Tangential Curvature
    - C_LONG [`output grid`] : Longitudinal Curvature. Zevenbergen & Thorne (1987) refer to this as profile curvature
    - C_CROS [`output grid`] : Cross-Sectional Curvature. Zevenbergen & Thorne (1987) refer to this as plan curvature
    - C_MINI [`output grid`] : Minimal Curvature
    - C_MAXI [`output grid`] : Maximal Curvature
    - C_TOTA [`output grid`] : Total Curvature
    - C_ROTO [`output grid`] : Flow Line Curvature
    - METHOD [`choice`] : Method. Available Choices: [0] maximum slope (Travis et al. 1975) [1] maximum triangle slope (Tarboton 1997) [2] least squares fitted plane (Horn 1981, Costa-Cabral & Burgess 1996) [3] 6 parameter 2nd order polynom (Evans 1979) [4] 6 parameter 2nd order polynom (Heerdegen & Beran 1982) [5] 6 parameter 2nd order polynom (Bauer, Rohdenburg, Bork 1985) [6] 9 parameter 2nd order polynom (Zevenbergen & Thorne 1987) [7] 10 parameter 3rd order polynom (Haralick 1983) [8] 10 parameter 3rd order polynom (Florinsky 2009) Default: 6
    - UNIT_SLOPE [`choice`] : Unit. Available Choices: [0] radians [1] degree [2] percent rise Default: 0
    - UNIT_ASPECT [`choice`] : Unit. Available Choices: [0] radians [1] degree Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_morphometry', '0', 'Slope, Aspect, Curvature')
    if Tool.is_Okay():
        Tool.Set_Input ('ELEVATION', ELEVATION)
        Tool.Set_Output('SLOPE', SLOPE)
        Tool.Set_Output('ASPECT', ASPECT)
        Tool.Set_Output('NORTHNESS', NORTHNESS)
        Tool.Set_Output('EASTNESS', EASTNESS)
        Tool.Set_Output('C_GENE', C_GENE)
        Tool.Set_Output('C_PROF', C_PROF)
        Tool.Set_Output('C_PLAN', C_PLAN)
        Tool.Set_Output('C_TANG', C_TANG)
        Tool.Set_Output('C_LONG', C_LONG)
        Tool.Set_Output('C_CROS', C_CROS)
        Tool.Set_Output('C_MINI', C_MINI)
        Tool.Set_Output('C_MAXI', C_MAXI)
        Tool.Set_Output('C_TOTA', C_TOTA)
        Tool.Set_Output('C_ROTO', C_ROTO)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('UNIT_SLOPE', UNIT_SLOPE)
        Tool.Set_Option('UNIT_ASPECT', UNIT_ASPECT)
        return Tool.Execute(Verbose)
    return False

def Convergence_Index(ELEVATION=None, RESULT=None, METHOD=None, NEIGHBOURS=None, Verbose=2):
    '''
    Convergence Index
    ----------
    [ta_morphometry.1]\n
    The convergence/divergence index.\n
    Arguments
    ----------
    - ELEVATION [`input grid`] : Elevation
    - RESULT [`output grid`] : Convergence Index
    - METHOD [`choice`] : Method. Available Choices: [0] Aspect [1] Gradient Default: 0
    - NEIGHBOURS [`choice`] : Gradient Calculation. Available Choices: [0] 2 x 2 [1] 3 x 3 Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_morphometry', '1', 'Convergence Index')
    if Tool.is_Okay():
        Tool.Set_Input ('ELEVATION', ELEVATION)
        Tool.Set_Output('RESULT', RESULT)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('NEIGHBOURS', NEIGHBOURS)
        return Tool.Execute(Verbose)
    return False

def run_tool_ta_morphometry_1(ELEVATION=None, RESULT=None, METHOD=None, NEIGHBOURS=None, Verbose=2):
    '''
    Convergence Index
    ----------
    [ta_morphometry.1]\n
    The convergence/divergence index.\n
    Arguments
    ----------
    - ELEVATION [`input grid`] : Elevation
    - RESULT [`output grid`] : Convergence Index
    - METHOD [`choice`] : Method. Available Choices: [0] Aspect [1] Gradient Default: 0
    - NEIGHBOURS [`choice`] : Gradient Calculation. Available Choices: [0] 2 x 2 [1] 3 x 3 Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_morphometry', '1', 'Convergence Index')
    if Tool.is_Okay():
        Tool.Set_Input ('ELEVATION', ELEVATION)
        Tool.Set_Output('RESULT', RESULT)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('NEIGHBOURS', NEIGHBOURS)
        return Tool.Execute(Verbose)
    return False

def Convergence_Index_Search_Radius(ELEVATION=None, CONVERGENCE=None, SLOPE=None, DIFFERENCE=None, RADIUS=None, DW_WEIGHTING=None, DW_IDW_POWER=None, DW_BANDWIDTH=None, Verbose=2):
    '''
    Convergence Index (Search Radius)
    ----------
    [ta_morphometry.2]\n
    Convergence Index (Search Radius)\n
    Arguments
    ----------
    - ELEVATION [`input grid`] : Elevation
    - CONVERGENCE [`output grid`] : Convergence Index
    - SLOPE [`boolean`] : Gradient. Default: 0
    - DIFFERENCE [`choice`] : Difference. Available Choices: [0] direction to the center cell [1] center cell's aspect direction Default: 0
    - RADIUS [`floating point number`] : Radius [Cells]. Minimum: 1.000000 Default: 10.000000
    - DW_WEIGHTING [`choice`] : Weighting Function. Available Choices: [0] no distance weighting [1] inverse distance to a power [2] exponential [3] gaussian Default: 0
    - DW_IDW_POWER [`floating point number`] : Power. Minimum: 0.000000 Default: 2.000000
    - DW_BANDWIDTH [`floating point number`] : Bandwidth. Minimum: 0.000000 Default: 1.000000 Bandwidth for exponential and Gaussian weighting

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_morphometry', '2', 'Convergence Index (Search Radius)')
    if Tool.is_Okay():
        Tool.Set_Input ('ELEVATION', ELEVATION)
        Tool.Set_Output('CONVERGENCE', CONVERGENCE)
        Tool.Set_Option('SLOPE', SLOPE)
        Tool.Set_Option('DIFFERENCE', DIFFERENCE)
        Tool.Set_Option('RADIUS', RADIUS)
        Tool.Set_Option('DW_WEIGHTING', DW_WEIGHTING)
        Tool.Set_Option('DW_IDW_POWER', DW_IDW_POWER)
        Tool.Set_Option('DW_BANDWIDTH', DW_BANDWIDTH)
        return Tool.Execute(Verbose)
    return False

def run_tool_ta_morphometry_2(ELEVATION=None, CONVERGENCE=None, SLOPE=None, DIFFERENCE=None, RADIUS=None, DW_WEIGHTING=None, DW_IDW_POWER=None, DW_BANDWIDTH=None, Verbose=2):
    '''
    Convergence Index (Search Radius)
    ----------
    [ta_morphometry.2]\n
    Convergence Index (Search Radius)\n
    Arguments
    ----------
    - ELEVATION [`input grid`] : Elevation
    - CONVERGENCE [`output grid`] : Convergence Index
    - SLOPE [`boolean`] : Gradient. Default: 0
    - DIFFERENCE [`choice`] : Difference. Available Choices: [0] direction to the center cell [1] center cell's aspect direction Default: 0
    - RADIUS [`floating point number`] : Radius [Cells]. Minimum: 1.000000 Default: 10.000000
    - DW_WEIGHTING [`choice`] : Weighting Function. Available Choices: [0] no distance weighting [1] inverse distance to a power [2] exponential [3] gaussian Default: 0
    - DW_IDW_POWER [`floating point number`] : Power. Minimum: 0.000000 Default: 2.000000
    - DW_BANDWIDTH [`floating point number`] : Bandwidth. Minimum: 0.000000 Default: 1.000000 Bandwidth for exponential and Gaussian weighting

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_morphometry', '2', 'Convergence Index (Search Radius)')
    if Tool.is_Okay():
        Tool.Set_Input ('ELEVATION', ELEVATION)
        Tool.Set_Output('CONVERGENCE', CONVERGENCE)
        Tool.Set_Option('SLOPE', SLOPE)
        Tool.Set_Option('DIFFERENCE', DIFFERENCE)
        Tool.Set_Option('RADIUS', RADIUS)
        Tool.Set_Option('DW_WEIGHTING', DW_WEIGHTING)
        Tool.Set_Option('DW_IDW_POWER', DW_IDW_POWER)
        Tool.Set_Option('DW_BANDWIDTH', DW_BANDWIDTH)
        return Tool.Execute(Verbose)
    return False

def Surface_Specific_Points(ELEVATION=None, RESULT=None, METHOD=None, THRESHOLD=None, Verbose=2):
    '''
    Surface Specific Points
    ----------
    [ta_morphometry.3]\n
    Classification of raster cells representing surface-specific points.\n
    Arguments
    ----------
    - ELEVATION [`input grid`] : Elevation
    - RESULT [`output grid`] : Surface Specific Points
    - METHOD [`choice`] : Method. Available Choices: [0] Flagging Neighbours of Highest Elevation [1] Opposite Neighbours [2] Flow Direction (down) [3] Flow Direction (up and down) [4] Peucker & Douglas Default: 1 Algorithm for the detection of Surface Specific Points
    - THRESHOLD [`floating point number`] : Threshold. Default: 1.000000 Threshold for Peucker & Douglas Algorithm

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_morphometry', '3', 'Surface Specific Points')
    if Tool.is_Okay():
        Tool.Set_Input ('ELEVATION', ELEVATION)
        Tool.Set_Output('RESULT', RESULT)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('THRESHOLD', THRESHOLD)
        return Tool.Execute(Verbose)
    return False

def run_tool_ta_morphometry_3(ELEVATION=None, RESULT=None, METHOD=None, THRESHOLD=None, Verbose=2):
    '''
    Surface Specific Points
    ----------
    [ta_morphometry.3]\n
    Classification of raster cells representing surface-specific points.\n
    Arguments
    ----------
    - ELEVATION [`input grid`] : Elevation
    - RESULT [`output grid`] : Surface Specific Points
    - METHOD [`choice`] : Method. Available Choices: [0] Flagging Neighbours of Highest Elevation [1] Opposite Neighbours [2] Flow Direction (down) [3] Flow Direction (up and down) [4] Peucker & Douglas Default: 1 Algorithm for the detection of Surface Specific Points
    - THRESHOLD [`floating point number`] : Threshold. Default: 1.000000 Threshold for Peucker & Douglas Algorithm

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_morphometry', '3', 'Surface Specific Points')
    if Tool.is_Okay():
        Tool.Set_Input ('ELEVATION', ELEVATION)
        Tool.Set_Output('RESULT', RESULT)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('THRESHOLD', THRESHOLD)
        return Tool.Execute(Verbose)
    return False

def Curvature_Classification(DEM=None, CLASSES=None, STRAIGHT=None, VERTICAL=None, HORIZONTAL=None, SMOOTH=None, Verbose=2):
    '''
    Curvature Classification
    ----------
    [ta_morphometry.4]\n
    Landform classification based on the profile and tangential (across slope) curvatures.\n
    Arguments
    ----------
    - DEM [`input grid`] : Elevation
    - CLASSES [`output grid`] : Curvature Classification
    - STRAIGHT [`floating point number`] : Threshold Radius. Minimum: 0.000010 Default: 2000.000000 Curvature radius threshold [map units] to distinct between straight and curved surfaces.
    - VERTICAL [`choice`] : Vertical Curvature. Available Choices: [0] longitudinal curvature [1] profile curvature Default: 1
    - HORIZONTAL [`choice`] : Horizontal Curvature. Available Choices: [0] cross-sectional curvature [1] tangential curvature Default: 1
    - SMOOTH [`integer number`] : Smoothing. Minimum: 0 Default: 0 Smoothing kernel radius. No smoothing will be done, if set to zero.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_morphometry', '4', 'Curvature Classification')
    if Tool.is_Okay():
        Tool.Set_Input ('DEM', DEM)
        Tool.Set_Output('CLASSES', CLASSES)
        Tool.Set_Option('STRAIGHT', STRAIGHT)
        Tool.Set_Option('VERTICAL', VERTICAL)
        Tool.Set_Option('HORIZONTAL', HORIZONTAL)
        Tool.Set_Option('SMOOTH', SMOOTH)
        return Tool.Execute(Verbose)
    return False

def run_tool_ta_morphometry_4(DEM=None, CLASSES=None, STRAIGHT=None, VERTICAL=None, HORIZONTAL=None, SMOOTH=None, Verbose=2):
    '''
    Curvature Classification
    ----------
    [ta_morphometry.4]\n
    Landform classification based on the profile and tangential (across slope) curvatures.\n
    Arguments
    ----------
    - DEM [`input grid`] : Elevation
    - CLASSES [`output grid`] : Curvature Classification
    - STRAIGHT [`floating point number`] : Threshold Radius. Minimum: 0.000010 Default: 2000.000000 Curvature radius threshold [map units] to distinct between straight and curved surfaces.
    - VERTICAL [`choice`] : Vertical Curvature. Available Choices: [0] longitudinal curvature [1] profile curvature Default: 1
    - HORIZONTAL [`choice`] : Horizontal Curvature. Available Choices: [0] cross-sectional curvature [1] tangential curvature Default: 1
    - SMOOTH [`integer number`] : Smoothing. Minimum: 0 Default: 0 Smoothing kernel radius. No smoothing will be done, if set to zero.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_morphometry', '4', 'Curvature Classification')
    if Tool.is_Okay():
        Tool.Set_Input ('DEM', DEM)
        Tool.Set_Output('CLASSES', CLASSES)
        Tool.Set_Option('STRAIGHT', STRAIGHT)
        Tool.Set_Option('VERTICAL', VERTICAL)
        Tool.Set_Option('HORIZONTAL', HORIZONTAL)
        Tool.Set_Option('SMOOTH', SMOOTH)
        return Tool.Execute(Verbose)
    return False

def Hypsometry(ELEVATION=None, TABLE=None, COUNT=None, SORTING=None, METHOD=None, BZRANGE=None, ZRANGE=None, Verbose=2):
    '''
    Hypsometry
    ----------
    [ta_morphometry.5]\n
    Calculates the hypsometric curve for a given DEM.\n
    The hypsometric curve is an empirical cumulative distribution function of elevations in a catchment or of a whole planet. The tool calculates both the relative (scaled from 0 to 100 percent) and absolute (minimum to maximum values) distributions. The former scales elevation and area by the maximum values. Such a non-dimensional curve allows one to asses the similarity of watersheds as differences in hypsometric curves arise from different geomorphic processes shaping a landscape.\n
    In case the hypsometric curve should not be calculated for the whole elevation range of the input dataset, a user-specified elevation range can be specified with the classification constant area.\n
    The output table has two attribute columns with relative height and area values, and two columns with absolute height and area values. In order to plot the non-dimensional hypsometric curve as diagram, use the relative area as x-axis values and the relative height for the y-axis. For a diagram with absolute values, use the absolute area as x-axis values and the absolute height for the y-axis.\n
    Arguments
    ----------
    - ELEVATION [`input grid`] : Elevation
    - TABLE [`output table`] : Hypsometry
    - COUNT [`integer number`] : Number of Classes. Minimum: 1 Default: 100 Number of discrete intervals (bins) used for sampling
    - SORTING [`choice`] : Sort. Available Choices: [0] up [1] down Default: 1 Choose how to sort the elevation dataset before sampling
    - METHOD [`choice`] : Classification Constant. Available Choices: [0] height [1] area Default: 1 Choose the classification constant to use
    - BZRANGE [`boolean`] : Use Z-Range. Default: 0 Use a user-specified elevation range instead of min/max of the input dataset
    - ZRANGE [`value range`] : Z-Range. User specified elevation range

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_morphometry', '5', 'Hypsometry')
    if Tool.is_Okay():
        Tool.Set_Input ('ELEVATION', ELEVATION)
        Tool.Set_Output('TABLE', TABLE)
        Tool.Set_Option('COUNT', COUNT)
        Tool.Set_Option('SORTING', SORTING)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('BZRANGE', BZRANGE)
        Tool.Set_Option('ZRANGE', ZRANGE)
        return Tool.Execute(Verbose)
    return False

def run_tool_ta_morphometry_5(ELEVATION=None, TABLE=None, COUNT=None, SORTING=None, METHOD=None, BZRANGE=None, ZRANGE=None, Verbose=2):
    '''
    Hypsometry
    ----------
    [ta_morphometry.5]\n
    Calculates the hypsometric curve for a given DEM.\n
    The hypsometric curve is an empirical cumulative distribution function of elevations in a catchment or of a whole planet. The tool calculates both the relative (scaled from 0 to 100 percent) and absolute (minimum to maximum values) distributions. The former scales elevation and area by the maximum values. Such a non-dimensional curve allows one to asses the similarity of watersheds as differences in hypsometric curves arise from different geomorphic processes shaping a landscape.\n
    In case the hypsometric curve should not be calculated for the whole elevation range of the input dataset, a user-specified elevation range can be specified with the classification constant area.\n
    The output table has two attribute columns with relative height and area values, and two columns with absolute height and area values. In order to plot the non-dimensional hypsometric curve as diagram, use the relative area as x-axis values and the relative height for the y-axis. For a diagram with absolute values, use the absolute area as x-axis values and the absolute height for the y-axis.\n
    Arguments
    ----------
    - ELEVATION [`input grid`] : Elevation
    - TABLE [`output table`] : Hypsometry
    - COUNT [`integer number`] : Number of Classes. Minimum: 1 Default: 100 Number of discrete intervals (bins) used for sampling
    - SORTING [`choice`] : Sort. Available Choices: [0] up [1] down Default: 1 Choose how to sort the elevation dataset before sampling
    - METHOD [`choice`] : Classification Constant. Available Choices: [0] height [1] area Default: 1 Choose the classification constant to use
    - BZRANGE [`boolean`] : Use Z-Range. Default: 0 Use a user-specified elevation range instead of min/max of the input dataset
    - ZRANGE [`value range`] : Z-Range. User specified elevation range

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_morphometry', '5', 'Hypsometry')
    if Tool.is_Okay():
        Tool.Set_Input ('ELEVATION', ELEVATION)
        Tool.Set_Output('TABLE', TABLE)
        Tool.Set_Option('COUNT', COUNT)
        Tool.Set_Option('SORTING', SORTING)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('BZRANGE', BZRANGE)
        Tool.Set_Option('ZRANGE', ZRANGE)
        return Tool.Execute(Verbose)
    return False

def Real_Surface_Area(DEM=None, AREA=None, Verbose=2):
    '''
    Real Surface Area
    ----------
    [ta_morphometry.6]\n
    Calculates real (not projected) cell area.\n
    Arguments
    ----------
    - DEM [`input grid`] : Elevation
    - AREA [`output grid`] : Surface Area

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_morphometry', '6', 'Real Surface Area')
    if Tool.is_Okay():
        Tool.Set_Input ('DEM', DEM)
        Tool.Set_Output('AREA', AREA)
        return Tool.Execute(Verbose)
    return False

def run_tool_ta_morphometry_6(DEM=None, AREA=None, Verbose=2):
    '''
    Real Surface Area
    ----------
    [ta_morphometry.6]\n
    Calculates real (not projected) cell area.\n
    Arguments
    ----------
    - DEM [`input grid`] : Elevation
    - AREA [`output grid`] : Surface Area

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_morphometry', '6', 'Real Surface Area')
    if Tool.is_Okay():
        Tool.Set_Input ('DEM', DEM)
        Tool.Set_Output('AREA', AREA)
        return Tool.Execute(Verbose)
    return False

def Morphometric_Protection_Index(DEM=None, PROTECTION=None, RADIUS=None, Verbose=2):
    '''
    Morphometric Protection Index
    ----------
    [ta_morphometry.7]\n
    This algorithm analyses the immediate surrounding of each cell up to an given distance and evaluates how the relief protects it. It is equivalent to the positive openness described in Yokoyama (2002).\n
    Arguments
    ----------
    - DEM [`input grid`] : Elevation
    - PROTECTION [`output grid`] : Protection Index
    - RADIUS [`floating point number`] : Radius. Minimum: 0.000000 Default: 2000.000000 The radius in map units

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_morphometry', '7', 'Morphometric Protection Index')
    if Tool.is_Okay():
        Tool.Set_Input ('DEM', DEM)
        Tool.Set_Output('PROTECTION', PROTECTION)
        Tool.Set_Option('RADIUS', RADIUS)
        return Tool.Execute(Verbose)
    return False

def run_tool_ta_morphometry_7(DEM=None, PROTECTION=None, RADIUS=None, Verbose=2):
    '''
    Morphometric Protection Index
    ----------
    [ta_morphometry.7]\n
    This algorithm analyses the immediate surrounding of each cell up to an given distance and evaluates how the relief protects it. It is equivalent to the positive openness described in Yokoyama (2002).\n
    Arguments
    ----------
    - DEM [`input grid`] : Elevation
    - PROTECTION [`output grid`] : Protection Index
    - RADIUS [`floating point number`] : Radius. Minimum: 0.000000 Default: 2000.000000 The radius in map units

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_morphometry', '7', 'Morphometric Protection Index')
    if Tool.is_Okay():
        Tool.Set_Input ('DEM', DEM)
        Tool.Set_Output('PROTECTION', PROTECTION)
        Tool.Set_Option('RADIUS', RADIUS)
        return Tool.Execute(Verbose)
    return False

def Multiresolution_Index_of_Valley_Bottom_Flatness_MRVBF(DEM=None, MRVBF=None, MRRTF=None, T_SLOPE=None, T_PCTL_V=None, T_PCTL_R=None, P_SLOPE=None, P_PCTL=None, UPDATE=None, CLASSIFY=None, MAX_RES=None, Verbose=2):
    '''
    Multiresolution Index of Valley Bottom Flatness (MRVBF)
    ----------
    [ta_morphometry.8]\n
    Calculation of the 'multiresolution index of valley bottom flatness' (MRVBF) and the complementary 'multiresolution index of the ridge top flatness' (MRRTF).\n
    Arguments
    ----------
    - DEM [`input grid`] : Elevation
    - MRVBF [`output grid`] : MRVBF
    - MRRTF [`output grid`] : MRRTF
    - T_SLOPE [`floating point number`] : Initial Threshold for Slope. Minimum: 0.000000 Maximum: 100.000000 Default: 16.000000
    - T_PCTL_V [`floating point number`] : Threshold for Elevation Percentile (Lowness). Minimum: 0.000000 Maximum: 1.000000 Default: 0.400000
    - T_PCTL_R [`floating point number`] : Threshold for Elevation Percentile (Upness). Minimum: 0.000000 Maximum: 1.000000 Default: 0.350000
    - P_SLOPE [`floating point number`] : Shape Parameter for Slope. Default: 4.000000
    - P_PCTL [`floating point number`] : Shape Parameter for Elevation Percentile. Default: 3.000000
    - UPDATE [`boolean`] : Update Views. Default: 1
    - CLASSIFY [`boolean`] : Classify. Default: 0
    - MAX_RES [`floating point number`] : Maximum Resolution (Percentage). Minimum: 0.000000 Maximum: 100.000000 Default: 100.000000 Maximum resolution as percentage of the diameter of the DEM.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_morphometry', '8', 'Multiresolution Index of Valley Bottom Flatness (MRVBF)')
    if Tool.is_Okay():
        Tool.Set_Input ('DEM', DEM)
        Tool.Set_Output('MRVBF', MRVBF)
        Tool.Set_Output('MRRTF', MRRTF)
        Tool.Set_Option('T_SLOPE', T_SLOPE)
        Tool.Set_Option('T_PCTL_V', T_PCTL_V)
        Tool.Set_Option('T_PCTL_R', T_PCTL_R)
        Tool.Set_Option('P_SLOPE', P_SLOPE)
        Tool.Set_Option('P_PCTL', P_PCTL)
        Tool.Set_Option('UPDATE', UPDATE)
        Tool.Set_Option('CLASSIFY', CLASSIFY)
        Tool.Set_Option('MAX_RES', MAX_RES)
        return Tool.Execute(Verbose)
    return False

def run_tool_ta_morphometry_8(DEM=None, MRVBF=None, MRRTF=None, T_SLOPE=None, T_PCTL_V=None, T_PCTL_R=None, P_SLOPE=None, P_PCTL=None, UPDATE=None, CLASSIFY=None, MAX_RES=None, Verbose=2):
    '''
    Multiresolution Index of Valley Bottom Flatness (MRVBF)
    ----------
    [ta_morphometry.8]\n
    Calculation of the 'multiresolution index of valley bottom flatness' (MRVBF) and the complementary 'multiresolution index of the ridge top flatness' (MRRTF).\n
    Arguments
    ----------
    - DEM [`input grid`] : Elevation
    - MRVBF [`output grid`] : MRVBF
    - MRRTF [`output grid`] : MRRTF
    - T_SLOPE [`floating point number`] : Initial Threshold for Slope. Minimum: 0.000000 Maximum: 100.000000 Default: 16.000000
    - T_PCTL_V [`floating point number`] : Threshold for Elevation Percentile (Lowness). Minimum: 0.000000 Maximum: 1.000000 Default: 0.400000
    - T_PCTL_R [`floating point number`] : Threshold for Elevation Percentile (Upness). Minimum: 0.000000 Maximum: 1.000000 Default: 0.350000
    - P_SLOPE [`floating point number`] : Shape Parameter for Slope. Default: 4.000000
    - P_PCTL [`floating point number`] : Shape Parameter for Elevation Percentile. Default: 3.000000
    - UPDATE [`boolean`] : Update Views. Default: 1
    - CLASSIFY [`boolean`] : Classify. Default: 0
    - MAX_RES [`floating point number`] : Maximum Resolution (Percentage). Minimum: 0.000000 Maximum: 100.000000 Default: 100.000000 Maximum resolution as percentage of the diameter of the DEM.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_morphometry', '8', 'Multiresolution Index of Valley Bottom Flatness (MRVBF)')
    if Tool.is_Okay():
        Tool.Set_Input ('DEM', DEM)
        Tool.Set_Output('MRVBF', MRVBF)
        Tool.Set_Output('MRRTF', MRRTF)
        Tool.Set_Option('T_SLOPE', T_SLOPE)
        Tool.Set_Option('T_PCTL_V', T_PCTL_V)
        Tool.Set_Option('T_PCTL_R', T_PCTL_R)
        Tool.Set_Option('P_SLOPE', P_SLOPE)
        Tool.Set_Option('P_PCTL', P_PCTL)
        Tool.Set_Option('UPDATE', UPDATE)
        Tool.Set_Option('CLASSIFY', CLASSIFY)
        Tool.Set_Option('MAX_RES', MAX_RES)
        return Tool.Execute(Verbose)
    return False

def Downslope_Distance_Gradient(DEM=None, GRADIENT=None, DIFFERENCE=None, DISTANCE=None, OUTPUT=None, Verbose=2):
    '''
    Downslope Distance Gradient
    ----------
    [ta_morphometry.9]\n
    Calculation of a new topographic index to quantify downslope controls on local drainage.\n
    Arguments
    ----------
    - DEM [`input grid`] : Elevation
    - GRADIENT [`output grid`] : Gradient
    - DIFFERENCE [`output grid`] : Gradient Difference. Difference to local gradient.
    - DISTANCE [`floating point number`] : Vertical Distance. Minimum: 0.000000 Default: 10.000000
    - OUTPUT [`choice`] : Output. Available Choices: [0] distance [1] gradient (tangens) [2] gradient (degree) Default: 2

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_morphometry', '9', 'Downslope Distance Gradient')
    if Tool.is_Okay():
        Tool.Set_Input ('DEM', DEM)
        Tool.Set_Output('GRADIENT', GRADIENT)
        Tool.Set_Output('DIFFERENCE', DIFFERENCE)
        Tool.Set_Option('DISTANCE', DISTANCE)
        Tool.Set_Option('OUTPUT', OUTPUT)
        return Tool.Execute(Verbose)
    return False

def run_tool_ta_morphometry_9(DEM=None, GRADIENT=None, DIFFERENCE=None, DISTANCE=None, OUTPUT=None, Verbose=2):
    '''
    Downslope Distance Gradient
    ----------
    [ta_morphometry.9]\n
    Calculation of a new topographic index to quantify downslope controls on local drainage.\n
    Arguments
    ----------
    - DEM [`input grid`] : Elevation
    - GRADIENT [`output grid`] : Gradient
    - DIFFERENCE [`output grid`] : Gradient Difference. Difference to local gradient.
    - DISTANCE [`floating point number`] : Vertical Distance. Minimum: 0.000000 Default: 10.000000
    - OUTPUT [`choice`] : Output. Available Choices: [0] distance [1] gradient (tangens) [2] gradient (degree) Default: 2

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_morphometry', '9', 'Downslope Distance Gradient')
    if Tool.is_Okay():
        Tool.Set_Input ('DEM', DEM)
        Tool.Set_Output('GRADIENT', GRADIENT)
        Tool.Set_Output('DIFFERENCE', DIFFERENCE)
        Tool.Set_Option('DISTANCE', DISTANCE)
        Tool.Set_Option('OUTPUT', OUTPUT)
        return Tool.Execute(Verbose)
    return False

def Mass_Balance_Index(DEM=None, HREL=None, MBI=None, TSLOPE=None, TCURVE=None, THREL=None, Verbose=2):
    '''
    Mass Balance Index
    ----------
    [ta_morphometry.10]\n
    A mass balance index.\n
    Arguments
    ----------
    - DEM [`input grid`] : Elevation
    - HREL [`optional input grid`] : Vertical Distance to Channel Network
    - MBI [`output grid`] : Mass Balance Index
    - TSLOPE [`floating point number`] : T Slope. Minimum: 0.000000 Default: 15.000000
    - TCURVE [`floating point number`] : T Curvature. Minimum: 0.000000 Default: 0.010000
    - THREL [`floating point number`] : T Vertical Distance to Channel Network. Minimum: 0.000000 Default: 15.000000

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_morphometry', '10', 'Mass Balance Index')
    if Tool.is_Okay():
        Tool.Set_Input ('DEM', DEM)
        Tool.Set_Input ('HREL', HREL)
        Tool.Set_Output('MBI', MBI)
        Tool.Set_Option('TSLOPE', TSLOPE)
        Tool.Set_Option('TCURVE', TCURVE)
        Tool.Set_Option('THREL', THREL)
        return Tool.Execute(Verbose)
    return False

def run_tool_ta_morphometry_10(DEM=None, HREL=None, MBI=None, TSLOPE=None, TCURVE=None, THREL=None, Verbose=2):
    '''
    Mass Balance Index
    ----------
    [ta_morphometry.10]\n
    A mass balance index.\n
    Arguments
    ----------
    - DEM [`input grid`] : Elevation
    - HREL [`optional input grid`] : Vertical Distance to Channel Network
    - MBI [`output grid`] : Mass Balance Index
    - TSLOPE [`floating point number`] : T Slope. Minimum: 0.000000 Default: 15.000000
    - TCURVE [`floating point number`] : T Curvature. Minimum: 0.000000 Default: 0.010000
    - THREL [`floating point number`] : T Vertical Distance to Channel Network. Minimum: 0.000000 Default: 15.000000

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_morphometry', '10', 'Mass Balance Index')
    if Tool.is_Okay():
        Tool.Set_Input ('DEM', DEM)
        Tool.Set_Input ('HREL', HREL)
        Tool.Set_Output('MBI', MBI)
        Tool.Set_Option('TSLOPE', TSLOPE)
        Tool.Set_Option('TCURVE', TCURVE)
        Tool.Set_Option('THREL', THREL)
        return Tool.Execute(Verbose)
    return False

def Effective_Air_Flow_Heights(DEM=None, DIR=None, LEN=None, AFH=None, DIR_UNITS=None, LEN_SCALE=None, MAXDIST=None, DIR_CONST=None, OLDVER=None, ACCEL=None, PYRAMIDS=None, LEE=None, LUV=None, Verbose=2):
    '''
    Effective Air Flow Heights
    ----------
    [ta_morphometry.11]\n
    Effective Air Flow Heights\n
    Arguments
    ----------
    - DEM [`input grid`] : Elevation
    - DIR [`optional input grid`] : Wind Direction. Direction into which the wind blows, starting with 0 for North and increasing clockwise.
    - LEN [`optional input grid`] : Wind Speed
    - AFH [`output grid`] : Effective Air Flow Heights
    - DIR_UNITS [`choice`] : Wind Direction Units. Available Choices: [0] radians [1] degree Default: 0
    - LEN_SCALE [`floating point number`] : Scaling. Default: 1.000000
    - MAXDIST [`floating point number`] : Search Distance [km]. Minimum: 0.000000 Default: 300.000000
    - DIR_CONST [`floating point number`] : Constant Wind Direction. Default: 135.000000 constant direction into the wind blows, given as degree
    - OLDVER [`boolean`] : Old Version. Default: 0 use old version for constant wind direction (no acceleration and averaging option)
    - ACCEL [`floating point number`] : Acceleration. Minimum: 1.000000 Default: 1.500000
    - PYRAMIDS [`boolean`] : Elevation Averaging. Default: 0 use more averaged elevations when looking at increasing distances
    - LEE [`floating point number`] : Windward Factor. Default: 0.500000
    - LUV [`floating point number`] : Luv Factor. Default: 3.000000

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_morphometry', '11', 'Effective Air Flow Heights')
    if Tool.is_Okay():
        Tool.Set_Input ('DEM', DEM)
        Tool.Set_Input ('DIR', DIR)
        Tool.Set_Input ('LEN', LEN)
        Tool.Set_Output('AFH', AFH)
        Tool.Set_Option('DIR_UNITS', DIR_UNITS)
        Tool.Set_Option('LEN_SCALE', LEN_SCALE)
        Tool.Set_Option('MAXDIST', MAXDIST)
        Tool.Set_Option('DIR_CONST', DIR_CONST)
        Tool.Set_Option('OLDVER', OLDVER)
        Tool.Set_Option('ACCEL', ACCEL)
        Tool.Set_Option('PYRAMIDS', PYRAMIDS)
        Tool.Set_Option('LEE', LEE)
        Tool.Set_Option('LUV', LUV)
        return Tool.Execute(Verbose)
    return False

def run_tool_ta_morphometry_11(DEM=None, DIR=None, LEN=None, AFH=None, DIR_UNITS=None, LEN_SCALE=None, MAXDIST=None, DIR_CONST=None, OLDVER=None, ACCEL=None, PYRAMIDS=None, LEE=None, LUV=None, Verbose=2):
    '''
    Effective Air Flow Heights
    ----------
    [ta_morphometry.11]\n
    Effective Air Flow Heights\n
    Arguments
    ----------
    - DEM [`input grid`] : Elevation
    - DIR [`optional input grid`] : Wind Direction. Direction into which the wind blows, starting with 0 for North and increasing clockwise.
    - LEN [`optional input grid`] : Wind Speed
    - AFH [`output grid`] : Effective Air Flow Heights
    - DIR_UNITS [`choice`] : Wind Direction Units. Available Choices: [0] radians [1] degree Default: 0
    - LEN_SCALE [`floating point number`] : Scaling. Default: 1.000000
    - MAXDIST [`floating point number`] : Search Distance [km]. Minimum: 0.000000 Default: 300.000000
    - DIR_CONST [`floating point number`] : Constant Wind Direction. Default: 135.000000 constant direction into the wind blows, given as degree
    - OLDVER [`boolean`] : Old Version. Default: 0 use old version for constant wind direction (no acceleration and averaging option)
    - ACCEL [`floating point number`] : Acceleration. Minimum: 1.000000 Default: 1.500000
    - PYRAMIDS [`boolean`] : Elevation Averaging. Default: 0 use more averaged elevations when looking at increasing distances
    - LEE [`floating point number`] : Windward Factor. Default: 0.500000
    - LUV [`floating point number`] : Luv Factor. Default: 3.000000

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_morphometry', '11', 'Effective Air Flow Heights')
    if Tool.is_Okay():
        Tool.Set_Input ('DEM', DEM)
        Tool.Set_Input ('DIR', DIR)
        Tool.Set_Input ('LEN', LEN)
        Tool.Set_Output('AFH', AFH)
        Tool.Set_Option('DIR_UNITS', DIR_UNITS)
        Tool.Set_Option('LEN_SCALE', LEN_SCALE)
        Tool.Set_Option('MAXDIST', MAXDIST)
        Tool.Set_Option('DIR_CONST', DIR_CONST)
        Tool.Set_Option('OLDVER', OLDVER)
        Tool.Set_Option('ACCEL', ACCEL)
        Tool.Set_Option('PYRAMIDS', PYRAMIDS)
        Tool.Set_Option('LEE', LEE)
        Tool.Set_Option('LUV', LUV)
        return Tool.Execute(Verbose)
    return False

def Diurnal_Anisotropic_Heat(DEM=None, DAH=None, ALPHA_MAX=None, Verbose=2):
    '''
    Diurnal Anisotropic Heat
    ----------
    [ta_morphometry.12]\n
    This tool calculates a rather simple approximation of the anisotropic diurnal heat (Ha) distribution using the formula:\n
    [Ha = cos(amax - a) * arctan(b)]\n
    where 'amax' defines the aspect with the maximum total heat surplus, 'a' is the slope aspect and 'b' is the slope angle. For more details see Boehner & Antonic (2009).\n
    Arguments
    ----------
    - DEM [`input grid`] : Elevation
    - DAH [`output grid`] : Diurnal Anisotropic Heating
    - ALPHA_MAX [`floating point number`] : Alpha Max (Degree). Minimum: 0.000000 Maximum: 360.000000 Default: 202.500000

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_morphometry', '12', 'Diurnal Anisotropic Heat')
    if Tool.is_Okay():
        Tool.Set_Input ('DEM', DEM)
        Tool.Set_Output('DAH', DAH)
        Tool.Set_Option('ALPHA_MAX', ALPHA_MAX)
        return Tool.Execute(Verbose)
    return False

def run_tool_ta_morphometry_12(DEM=None, DAH=None, ALPHA_MAX=None, Verbose=2):
    '''
    Diurnal Anisotropic Heat
    ----------
    [ta_morphometry.12]\n
    This tool calculates a rather simple approximation of the anisotropic diurnal heat (Ha) distribution using the formula:\n
    [Ha = cos(amax - a) * arctan(b)]\n
    where 'amax' defines the aspect with the maximum total heat surplus, 'a' is the slope aspect and 'b' is the slope angle. For more details see Boehner & Antonic (2009).\n
    Arguments
    ----------
    - DEM [`input grid`] : Elevation
    - DAH [`output grid`] : Diurnal Anisotropic Heating
    - ALPHA_MAX [`floating point number`] : Alpha Max (Degree). Minimum: 0.000000 Maximum: 360.000000 Default: 202.500000

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_morphometry', '12', 'Diurnal Anisotropic Heat')
    if Tool.is_Okay():
        Tool.Set_Input ('DEM', DEM)
        Tool.Set_Output('DAH', DAH)
        Tool.Set_Option('ALPHA_MAX', ALPHA_MAX)
        return Tool.Execute(Verbose)
    return False

def Land_Surface_Temperature_Lapse_Rates(DEM=None, SWR=None, LAI=None, LST=None, LAI_MAX=None, Z_REFERENCE=None, T_REFERENCE=None, T_GRADIENT=None, C_FACTOR=None, Verbose=2):
    '''
    Land Surface Temperature (Lapse Rates)
    ----------
    [ta_morphometry.13]\n
    Temperature estimation at each grid point as a function of temperature, temperature lapse rate and elevation for a reference station. Further optional input is the Leaf Area Index (LAI) and the short-wave radiation ratio, which relates the irradiance including terrain effects to that calculated for a flat, horizontal plane. See Wilson & Gallant (2000) for more details.\n
    Arguments
    ----------
    - DEM [`input grid`] : Elevation
    - SWR [`optional input grid`] : Short Wave Radiation Ratio. The ratio of daily total short-wave irradiance on sloping sites compared to horizontal sites.
    - LAI [`optional input grid`] : Leaf Area Index
    - LST [`output grid`] : Land Surface Temperature
    - LAI_MAX [`floating point number`] : Maximum LAI. Minimum: 0.010000 Default: 8.000000
    - Z_REFERENCE [`floating point number`] : Elevation. Default: 0.000000
    - T_REFERENCE [`floating point number`] : Temperature. Default: 0.000000 Temperature at reference station in degree Celsius.
    - T_GRADIENT [`floating point number`] : Temperature Lapse Rate. Default: 0.650000 Vertical temperature gradient in degree Celsius per 100 meter.
    - C_FACTOR [`floating point number`] : C Factor. Default: 1.000000

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_morphometry', '13', 'Land Surface Temperature (Lapse Rates)')
    if Tool.is_Okay():
        Tool.Set_Input ('DEM', DEM)
        Tool.Set_Input ('SWR', SWR)
        Tool.Set_Input ('LAI', LAI)
        Tool.Set_Output('LST', LST)
        Tool.Set_Option('LAI_MAX', LAI_MAX)
        Tool.Set_Option('Z_REFERENCE', Z_REFERENCE)
        Tool.Set_Option('T_REFERENCE', T_REFERENCE)
        Tool.Set_Option('T_GRADIENT', T_GRADIENT)
        Tool.Set_Option('C_FACTOR', C_FACTOR)
        return Tool.Execute(Verbose)
    return False

def run_tool_ta_morphometry_13(DEM=None, SWR=None, LAI=None, LST=None, LAI_MAX=None, Z_REFERENCE=None, T_REFERENCE=None, T_GRADIENT=None, C_FACTOR=None, Verbose=2):
    '''
    Land Surface Temperature (Lapse Rates)
    ----------
    [ta_morphometry.13]\n
    Temperature estimation at each grid point as a function of temperature, temperature lapse rate and elevation for a reference station. Further optional input is the Leaf Area Index (LAI) and the short-wave radiation ratio, which relates the irradiance including terrain effects to that calculated for a flat, horizontal plane. See Wilson & Gallant (2000) for more details.\n
    Arguments
    ----------
    - DEM [`input grid`] : Elevation
    - SWR [`optional input grid`] : Short Wave Radiation Ratio. The ratio of daily total short-wave irradiance on sloping sites compared to horizontal sites.
    - LAI [`optional input grid`] : Leaf Area Index
    - LST [`output grid`] : Land Surface Temperature
    - LAI_MAX [`floating point number`] : Maximum LAI. Minimum: 0.010000 Default: 8.000000
    - Z_REFERENCE [`floating point number`] : Elevation. Default: 0.000000
    - T_REFERENCE [`floating point number`] : Temperature. Default: 0.000000 Temperature at reference station in degree Celsius.
    - T_GRADIENT [`floating point number`] : Temperature Lapse Rate. Default: 0.650000 Vertical temperature gradient in degree Celsius per 100 meter.
    - C_FACTOR [`floating point number`] : C Factor. Default: 1.000000

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_morphometry', '13', 'Land Surface Temperature (Lapse Rates)')
    if Tool.is_Okay():
        Tool.Set_Input ('DEM', DEM)
        Tool.Set_Input ('SWR', SWR)
        Tool.Set_Input ('LAI', LAI)
        Tool.Set_Output('LST', LST)
        Tool.Set_Option('LAI_MAX', LAI_MAX)
        Tool.Set_Option('Z_REFERENCE', Z_REFERENCE)
        Tool.Set_Option('T_REFERENCE', T_REFERENCE)
        Tool.Set_Option('T_GRADIENT', T_GRADIENT)
        Tool.Set_Option('C_FACTOR', C_FACTOR)
        return Tool.Execute(Verbose)
    return False

def Relative_Heights_and_Slope_Positions(DEM=None, HO=None, HU=None, NH=None, SH=None, MS=None, W=None, T=None, E=None, Verbose=2):
    '''
    Relative Heights and Slope Positions
    ----------
    [ta_morphometry.14]\n
    This tool calculates several terrain indices related to the terrain position from a digital elevation model using an iterative approach. You can control the results with three parameters.\n
    The parameter 'w' weights the influence of catchment size on relative elevation (inversely proportional).\n
    The parameter 't' controls the amount by which a maximum in the neighbourhood of a cell is taken over into the cell (considering the local slope between the cells). The smaller 't' and/or the smaller the slope, the more of the maximum value is taken over into the cell. This results in a greater generalization/smoothing of the result. The greater 't' and/or the higher the slope, the less is taken over into the cell and the result will show a more irregular pattern caused by small changes in elevation between the cells.\n
    The parameter 'e' controls the position of relative height maxima as a function of slope. More details about the computational concept can be found in Boehner & Selige (2006).\n
    Arguments
    ----------
    - DEM [`input grid`] : Elevation
    - HO [`output grid`] : Slope Height
    - HU [`output grid`] : Valley Depth
    - NH [`output grid`] : Normalized Height
    - SH [`output grid`] : Standardized Height
    - MS [`output grid`] : Mid-Slope Position
    - W [`floating point number`] : w. Minimum: 0.000000 Default: 0.500000
    - T [`floating point number`] : t. Minimum: 0.000000 Default: 10.000000
    - E [`floating point number`] : e. Minimum: 0.000000 Default: 2.000000

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_morphometry', '14', 'Relative Heights and Slope Positions')
    if Tool.is_Okay():
        Tool.Set_Input ('DEM', DEM)
        Tool.Set_Output('HO', HO)
        Tool.Set_Output('HU', HU)
        Tool.Set_Output('NH', NH)
        Tool.Set_Output('SH', SH)
        Tool.Set_Output('MS', MS)
        Tool.Set_Option('W', W)
        Tool.Set_Option('T', T)
        Tool.Set_Option('E', E)
        return Tool.Execute(Verbose)
    return False

def run_tool_ta_morphometry_14(DEM=None, HO=None, HU=None, NH=None, SH=None, MS=None, W=None, T=None, E=None, Verbose=2):
    '''
    Relative Heights and Slope Positions
    ----------
    [ta_morphometry.14]\n
    This tool calculates several terrain indices related to the terrain position from a digital elevation model using an iterative approach. You can control the results with three parameters.\n
    The parameter 'w' weights the influence of catchment size on relative elevation (inversely proportional).\n
    The parameter 't' controls the amount by which a maximum in the neighbourhood of a cell is taken over into the cell (considering the local slope between the cells). The smaller 't' and/or the smaller the slope, the more of the maximum value is taken over into the cell. This results in a greater generalization/smoothing of the result. The greater 't' and/or the higher the slope, the less is taken over into the cell and the result will show a more irregular pattern caused by small changes in elevation between the cells.\n
    The parameter 'e' controls the position of relative height maxima as a function of slope. More details about the computational concept can be found in Boehner & Selige (2006).\n
    Arguments
    ----------
    - DEM [`input grid`] : Elevation
    - HO [`output grid`] : Slope Height
    - HU [`output grid`] : Valley Depth
    - NH [`output grid`] : Normalized Height
    - SH [`output grid`] : Standardized Height
    - MS [`output grid`] : Mid-Slope Position
    - W [`floating point number`] : w. Minimum: 0.000000 Default: 0.500000
    - T [`floating point number`] : t. Minimum: 0.000000 Default: 10.000000
    - E [`floating point number`] : e. Minimum: 0.000000 Default: 2.000000

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_morphometry', '14', 'Relative Heights and Slope Positions')
    if Tool.is_Okay():
        Tool.Set_Input ('DEM', DEM)
        Tool.Set_Output('HO', HO)
        Tool.Set_Output('HU', HU)
        Tool.Set_Output('NH', NH)
        Tool.Set_Output('SH', SH)
        Tool.Set_Output('MS', MS)
        Tool.Set_Option('W', W)
        Tool.Set_Option('T', T)
        Tool.Set_Option('E', E)
        return Tool.Execute(Verbose)
    return False

def Wind_Effect_Windward__Leeward_Index(DEM=None, DIR=None, LEN=None, EFFECT=None, AFH=None, DIR_UNITS=None, LEN_SCALE=None, MAXDIST=None, DIR_CONST=None, OLDVER=None, ACCEL=None, PYRAMIDS=None, Verbose=2):
    '''
    Wind Effect (Windward / Leeward Index)
    ----------
    [ta_morphometry.15]\n
    The 'Wind Effect' is a dimensionless index. Values below 1 indicate wind shadowed areas whereas values above 1 indicate areas exposed to wind, all with regard to the specified wind direction. Wind direction, i.e. the direction into which the wind blows, might be either constant or variying in space, if a wind direction grid is supplied.\n
    Arguments
    ----------
    - DEM [`input grid`] : Elevation
    - DIR [`optional input grid`] : Wind Direction. Direction into which the wind blows, starting with 0 for North and increasing clockwise.
    - LEN [`optional input grid`] : Wind Speed
    - EFFECT [`output grid`] : Wind Effect
    - AFH [`output grid`] : Effective Air Flow Heights
    - DIR_UNITS [`choice`] : Wind Direction Units. Available Choices: [0] radians [1] degree Default: 0
    - LEN_SCALE [`floating point number`] : Scaling. Default: 1.000000
    - MAXDIST [`floating point number`] : Search Distance [km]. Minimum: 0.000000 Default: 300.000000
    - DIR_CONST [`floating point number`] : Constant Wind Direction. Default: 135.000000 constant direction into the wind blows, given as degree
    - OLDVER [`boolean`] : Old Version. Default: 0 use old version for constant wind direction (no acceleration and averaging option)
    - ACCEL [`floating point number`] : Acceleration. Minimum: 1.000000 Default: 1.500000
    - PYRAMIDS [`boolean`] : Elevation Averaging. Default: 0 use more averaged elevations when looking at increasing distances

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_morphometry', '15', 'Wind Effect (Windward / Leeward Index)')
    if Tool.is_Okay():
        Tool.Set_Input ('DEM', DEM)
        Tool.Set_Input ('DIR', DIR)
        Tool.Set_Input ('LEN', LEN)
        Tool.Set_Output('EFFECT', EFFECT)
        Tool.Set_Output('AFH', AFH)
        Tool.Set_Option('DIR_UNITS', DIR_UNITS)
        Tool.Set_Option('LEN_SCALE', LEN_SCALE)
        Tool.Set_Option('MAXDIST', MAXDIST)
        Tool.Set_Option('DIR_CONST', DIR_CONST)
        Tool.Set_Option('OLDVER', OLDVER)
        Tool.Set_Option('ACCEL', ACCEL)
        Tool.Set_Option('PYRAMIDS', PYRAMIDS)
        return Tool.Execute(Verbose)
    return False

def run_tool_ta_morphometry_15(DEM=None, DIR=None, LEN=None, EFFECT=None, AFH=None, DIR_UNITS=None, LEN_SCALE=None, MAXDIST=None, DIR_CONST=None, OLDVER=None, ACCEL=None, PYRAMIDS=None, Verbose=2):
    '''
    Wind Effect (Windward / Leeward Index)
    ----------
    [ta_morphometry.15]\n
    The 'Wind Effect' is a dimensionless index. Values below 1 indicate wind shadowed areas whereas values above 1 indicate areas exposed to wind, all with regard to the specified wind direction. Wind direction, i.e. the direction into which the wind blows, might be either constant or variying in space, if a wind direction grid is supplied.\n
    Arguments
    ----------
    - DEM [`input grid`] : Elevation
    - DIR [`optional input grid`] : Wind Direction. Direction into which the wind blows, starting with 0 for North and increasing clockwise.
    - LEN [`optional input grid`] : Wind Speed
    - EFFECT [`output grid`] : Wind Effect
    - AFH [`output grid`] : Effective Air Flow Heights
    - DIR_UNITS [`choice`] : Wind Direction Units. Available Choices: [0] radians [1] degree Default: 0
    - LEN_SCALE [`floating point number`] : Scaling. Default: 1.000000
    - MAXDIST [`floating point number`] : Search Distance [km]. Minimum: 0.000000 Default: 300.000000
    - DIR_CONST [`floating point number`] : Constant Wind Direction. Default: 135.000000 constant direction into the wind blows, given as degree
    - OLDVER [`boolean`] : Old Version. Default: 0 use old version for constant wind direction (no acceleration and averaging option)
    - ACCEL [`floating point number`] : Acceleration. Minimum: 1.000000 Default: 1.500000
    - PYRAMIDS [`boolean`] : Elevation Averaging. Default: 0 use more averaged elevations when looking at increasing distances

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_morphometry', '15', 'Wind Effect (Windward / Leeward Index)')
    if Tool.is_Okay():
        Tool.Set_Input ('DEM', DEM)
        Tool.Set_Input ('DIR', DIR)
        Tool.Set_Input ('LEN', LEN)
        Tool.Set_Output('EFFECT', EFFECT)
        Tool.Set_Output('AFH', AFH)
        Tool.Set_Option('DIR_UNITS', DIR_UNITS)
        Tool.Set_Option('LEN_SCALE', LEN_SCALE)
        Tool.Set_Option('MAXDIST', MAXDIST)
        Tool.Set_Option('DIR_CONST', DIR_CONST)
        Tool.Set_Option('OLDVER', OLDVER)
        Tool.Set_Option('ACCEL', ACCEL)
        Tool.Set_Option('PYRAMIDS', PYRAMIDS)
        return Tool.Execute(Verbose)
    return False

def Terrain_Ruggedness_Index_TRI(DEM=None, TRI=None, MODE=None, RADIUS=None, DW_WEIGHTING=None, DW_IDW_POWER=None, DW_BANDWIDTH=None, Verbose=2):
    '''
    Terrain Ruggedness Index (TRI)
    ----------
    [ta_morphometry.16]\n
    Terrain Ruggedness Index (TRI)\n
    Arguments
    ----------
    - DEM [`input grid`] : Elevation
    - TRI [`output grid`] : Terrain Ruggedness Index (TRI)
    - MODE [`choice`] : Search Mode. Available Choices: [0] Square [1] Circle Default: 1
    - RADIUS [`integer number`] : Search Radius. Minimum: 1 Default: 1 radius in cells
    - DW_WEIGHTING [`choice`] : Weighting Function. Available Choices: [0] no distance weighting [1] inverse distance to a power [2] exponential [3] gaussian Default: 0
    - DW_IDW_POWER [`floating point number`] : Power. Minimum: 0.000000 Default: 2.000000
    - DW_BANDWIDTH [`floating point number`] : Bandwidth. Minimum: 0.000000 Default: 75.000000 Bandwidth for exponential and Gaussian weighting

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_morphometry', '16', 'Terrain Ruggedness Index (TRI)')
    if Tool.is_Okay():
        Tool.Set_Input ('DEM', DEM)
        Tool.Set_Output('TRI', TRI)
        Tool.Set_Option('MODE', MODE)
        Tool.Set_Option('RADIUS', RADIUS)
        Tool.Set_Option('DW_WEIGHTING', DW_WEIGHTING)
        Tool.Set_Option('DW_IDW_POWER', DW_IDW_POWER)
        Tool.Set_Option('DW_BANDWIDTH', DW_BANDWIDTH)
        return Tool.Execute(Verbose)
    return False

def run_tool_ta_morphometry_16(DEM=None, TRI=None, MODE=None, RADIUS=None, DW_WEIGHTING=None, DW_IDW_POWER=None, DW_BANDWIDTH=None, Verbose=2):
    '''
    Terrain Ruggedness Index (TRI)
    ----------
    [ta_morphometry.16]\n
    Terrain Ruggedness Index (TRI)\n
    Arguments
    ----------
    - DEM [`input grid`] : Elevation
    - TRI [`output grid`] : Terrain Ruggedness Index (TRI)
    - MODE [`choice`] : Search Mode. Available Choices: [0] Square [1] Circle Default: 1
    - RADIUS [`integer number`] : Search Radius. Minimum: 1 Default: 1 radius in cells
    - DW_WEIGHTING [`choice`] : Weighting Function. Available Choices: [0] no distance weighting [1] inverse distance to a power [2] exponential [3] gaussian Default: 0
    - DW_IDW_POWER [`floating point number`] : Power. Minimum: 0.000000 Default: 2.000000
    - DW_BANDWIDTH [`floating point number`] : Bandwidth. Minimum: 0.000000 Default: 75.000000 Bandwidth for exponential and Gaussian weighting

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_morphometry', '16', 'Terrain Ruggedness Index (TRI)')
    if Tool.is_Okay():
        Tool.Set_Input ('DEM', DEM)
        Tool.Set_Output('TRI', TRI)
        Tool.Set_Option('MODE', MODE)
        Tool.Set_Option('RADIUS', RADIUS)
        Tool.Set_Option('DW_WEIGHTING', DW_WEIGHTING)
        Tool.Set_Option('DW_IDW_POWER', DW_IDW_POWER)
        Tool.Set_Option('DW_BANDWIDTH', DW_BANDWIDTH)
        return Tool.Execute(Verbose)
    return False

def Vector_Ruggedness_Measure_VRM(DEM=None, VRM=None, MODE=None, RADIUS=None, DW_WEIGHTING=None, DW_IDW_POWER=None, DW_BANDWIDTH=None, Verbose=2):
    '''
    Vector Ruggedness Measure (VRM)
    ----------
    [ta_morphometry.17]\n
    Vector Ruggedness Measure (VRM)\n
    Arguments
    ----------
    - DEM [`input grid`] : Elevation
    - VRM [`output grid`] : Vector Terrain Ruggedness (VRM)
    - MODE [`choice`] : Search Mode. Available Choices: [0] Square [1] Circle Default: 1
    - RADIUS [`integer number`] : Search Radius. Minimum: 1 Default: 1 radius in cells
    - DW_WEIGHTING [`choice`] : Weighting Function. Available Choices: [0] no distance weighting [1] inverse distance to a power [2] exponential [3] gaussian Default: 0
    - DW_IDW_POWER [`floating point number`] : Power. Minimum: 0.000000 Default: 2.000000
    - DW_BANDWIDTH [`floating point number`] : Bandwidth. Minimum: 0.000000 Default: 75.000000 Bandwidth for exponential and Gaussian weighting

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_morphometry', '17', 'Vector Ruggedness Measure (VRM)')
    if Tool.is_Okay():
        Tool.Set_Input ('DEM', DEM)
        Tool.Set_Output('VRM', VRM)
        Tool.Set_Option('MODE', MODE)
        Tool.Set_Option('RADIUS', RADIUS)
        Tool.Set_Option('DW_WEIGHTING', DW_WEIGHTING)
        Tool.Set_Option('DW_IDW_POWER', DW_IDW_POWER)
        Tool.Set_Option('DW_BANDWIDTH', DW_BANDWIDTH)
        return Tool.Execute(Verbose)
    return False

def run_tool_ta_morphometry_17(DEM=None, VRM=None, MODE=None, RADIUS=None, DW_WEIGHTING=None, DW_IDW_POWER=None, DW_BANDWIDTH=None, Verbose=2):
    '''
    Vector Ruggedness Measure (VRM)
    ----------
    [ta_morphometry.17]\n
    Vector Ruggedness Measure (VRM)\n
    Arguments
    ----------
    - DEM [`input grid`] : Elevation
    - VRM [`output grid`] : Vector Terrain Ruggedness (VRM)
    - MODE [`choice`] : Search Mode. Available Choices: [0] Square [1] Circle Default: 1
    - RADIUS [`integer number`] : Search Radius. Minimum: 1 Default: 1 radius in cells
    - DW_WEIGHTING [`choice`] : Weighting Function. Available Choices: [0] no distance weighting [1] inverse distance to a power [2] exponential [3] gaussian Default: 0
    - DW_IDW_POWER [`floating point number`] : Power. Minimum: 0.000000 Default: 2.000000
    - DW_BANDWIDTH [`floating point number`] : Bandwidth. Minimum: 0.000000 Default: 75.000000 Bandwidth for exponential and Gaussian weighting

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_morphometry', '17', 'Vector Ruggedness Measure (VRM)')
    if Tool.is_Okay():
        Tool.Set_Input ('DEM', DEM)
        Tool.Set_Output('VRM', VRM)
        Tool.Set_Option('MODE', MODE)
        Tool.Set_Option('RADIUS', RADIUS)
        Tool.Set_Option('DW_WEIGHTING', DW_WEIGHTING)
        Tool.Set_Option('DW_IDW_POWER', DW_IDW_POWER)
        Tool.Set_Option('DW_BANDWIDTH', DW_BANDWIDTH)
        return Tool.Execute(Verbose)
    return False

def Topographic_Position_Index_TPI(DEM=None, TPI=None, STANDARD=None, RADIUS=None, DW_WEIGHTING=None, DW_IDW_POWER=None, DW_BANDWIDTH=None, Verbose=2):
    '''
    Topographic Position Index (TPI)
    ----------
    [ta_morphometry.18]\n
    Topographic Position Index (TPI) calculation as proposed by Guisan et al. (1999). This is literally the same as the difference to the mean calculation (residual analysis) proposed by Wilson & Gallant (2000). The bandwidth parameter for distance weighting is given as percentage of the (outer) radius.\n
    Arguments
    ----------
    - DEM [`input grid`] : Elevation
    - TPI [`output grid`] : Topographic Position Index
    - STANDARD [`boolean`] : Standardize. Default: 0
    - RADIUS [`value range`] : Scale. kernel radius in map units; defines an annulus if inner radius is greater than zero
    - DW_WEIGHTING [`choice`] : Weighting Function. Available Choices: [0] no distance weighting [1] inverse distance to a power [2] exponential [3] gaussian Default: 0
    - DW_IDW_POWER [`floating point number`] : Power. Minimum: 0.000000 Default: 2.000000
    - DW_BANDWIDTH [`floating point number`] : Bandwidth. Minimum: 0.000000 Default: 75.000000 Bandwidth for exponential and Gaussian weighting

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_morphometry', '18', 'Topographic Position Index (TPI)')
    if Tool.is_Okay():
        Tool.Set_Input ('DEM', DEM)
        Tool.Set_Output('TPI', TPI)
        Tool.Set_Option('STANDARD', STANDARD)
        Tool.Set_Option('RADIUS', RADIUS)
        Tool.Set_Option('DW_WEIGHTING', DW_WEIGHTING)
        Tool.Set_Option('DW_IDW_POWER', DW_IDW_POWER)
        Tool.Set_Option('DW_BANDWIDTH', DW_BANDWIDTH)
        return Tool.Execute(Verbose)
    return False

def run_tool_ta_morphometry_18(DEM=None, TPI=None, STANDARD=None, RADIUS=None, DW_WEIGHTING=None, DW_IDW_POWER=None, DW_BANDWIDTH=None, Verbose=2):
    '''
    Topographic Position Index (TPI)
    ----------
    [ta_morphometry.18]\n
    Topographic Position Index (TPI) calculation as proposed by Guisan et al. (1999). This is literally the same as the difference to the mean calculation (residual analysis) proposed by Wilson & Gallant (2000). The bandwidth parameter for distance weighting is given as percentage of the (outer) radius.\n
    Arguments
    ----------
    - DEM [`input grid`] : Elevation
    - TPI [`output grid`] : Topographic Position Index
    - STANDARD [`boolean`] : Standardize. Default: 0
    - RADIUS [`value range`] : Scale. kernel radius in map units; defines an annulus if inner radius is greater than zero
    - DW_WEIGHTING [`choice`] : Weighting Function. Available Choices: [0] no distance weighting [1] inverse distance to a power [2] exponential [3] gaussian Default: 0
    - DW_IDW_POWER [`floating point number`] : Power. Minimum: 0.000000 Default: 2.000000
    - DW_BANDWIDTH [`floating point number`] : Bandwidth. Minimum: 0.000000 Default: 75.000000 Bandwidth for exponential and Gaussian weighting

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_morphometry', '18', 'Topographic Position Index (TPI)')
    if Tool.is_Okay():
        Tool.Set_Input ('DEM', DEM)
        Tool.Set_Output('TPI', TPI)
        Tool.Set_Option('STANDARD', STANDARD)
        Tool.Set_Option('RADIUS', RADIUS)
        Tool.Set_Option('DW_WEIGHTING', DW_WEIGHTING)
        Tool.Set_Option('DW_IDW_POWER', DW_IDW_POWER)
        Tool.Set_Option('DW_BANDWIDTH', DW_BANDWIDTH)
        return Tool.Execute(Verbose)
    return False

def TPI_Based_Landform_Classification(DEM=None, LANDFORMS=None, RADIUS_A=None, RADIUS_B=None, DW_WEIGHTING=None, DW_IDW_POWER=None, DW_BANDWIDTH=None, Verbose=2):
    '''
    TPI Based Landform Classification
    ----------
    [ta_morphometry.19]\n
    Topographic Position Index (TPI) calculation as proposed by Guisan et al. (1999). This is literally the same as the difference to the mean calculation (residual analysis) proposed by Wilson & Gallant (2000). The bandwidth parameter for distance weighting is given as percentage of the (outer) radius.\n
    Arguments
    ----------
    - DEM [`input grid`] : Elevation
    - LANDFORMS [`output grid`] : Landforms
    - RADIUS_A [`value range`] : Small Scale. kernel radius in map units; defines an annulus if inner radius is greater than zero
    - RADIUS_B [`value range`] : Large Scale. kernel radius in map units; defines an annulus if inner radius is greater than zero
    - DW_WEIGHTING [`choice`] : Weighting Function. Available Choices: [0] no distance weighting [1] inverse distance to a power [2] exponential [3] gaussian Default: 0
    - DW_IDW_POWER [`floating point number`] : Power. Minimum: 0.000000 Default: 2.000000
    - DW_BANDWIDTH [`floating point number`] : Bandwidth. Minimum: 0.000000 Default: 75.000000 Bandwidth for exponential and Gaussian weighting

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_morphometry', '19', 'TPI Based Landform Classification')
    if Tool.is_Okay():
        Tool.Set_Input ('DEM', DEM)
        Tool.Set_Output('LANDFORMS', LANDFORMS)
        Tool.Set_Option('RADIUS_A', RADIUS_A)
        Tool.Set_Option('RADIUS_B', RADIUS_B)
        Tool.Set_Option('DW_WEIGHTING', DW_WEIGHTING)
        Tool.Set_Option('DW_IDW_POWER', DW_IDW_POWER)
        Tool.Set_Option('DW_BANDWIDTH', DW_BANDWIDTH)
        return Tool.Execute(Verbose)
    return False

def run_tool_ta_morphometry_19(DEM=None, LANDFORMS=None, RADIUS_A=None, RADIUS_B=None, DW_WEIGHTING=None, DW_IDW_POWER=None, DW_BANDWIDTH=None, Verbose=2):
    '''
    TPI Based Landform Classification
    ----------
    [ta_morphometry.19]\n
    Topographic Position Index (TPI) calculation as proposed by Guisan et al. (1999). This is literally the same as the difference to the mean calculation (residual analysis) proposed by Wilson & Gallant (2000). The bandwidth parameter for distance weighting is given as percentage of the (outer) radius.\n
    Arguments
    ----------
    - DEM [`input grid`] : Elevation
    - LANDFORMS [`output grid`] : Landforms
    - RADIUS_A [`value range`] : Small Scale. kernel radius in map units; defines an annulus if inner radius is greater than zero
    - RADIUS_B [`value range`] : Large Scale. kernel radius in map units; defines an annulus if inner radius is greater than zero
    - DW_WEIGHTING [`choice`] : Weighting Function. Available Choices: [0] no distance weighting [1] inverse distance to a power [2] exponential [3] gaussian Default: 0
    - DW_IDW_POWER [`floating point number`] : Power. Minimum: 0.000000 Default: 2.000000
    - DW_BANDWIDTH [`floating point number`] : Bandwidth. Minimum: 0.000000 Default: 75.000000 Bandwidth for exponential and Gaussian weighting

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_morphometry', '19', 'TPI Based Landform Classification')
    if Tool.is_Okay():
        Tool.Set_Input ('DEM', DEM)
        Tool.Set_Output('LANDFORMS', LANDFORMS)
        Tool.Set_Option('RADIUS_A', RADIUS_A)
        Tool.Set_Option('RADIUS_B', RADIUS_B)
        Tool.Set_Option('DW_WEIGHTING', DW_WEIGHTING)
        Tool.Set_Option('DW_IDW_POWER', DW_IDW_POWER)
        Tool.Set_Option('DW_BANDWIDTH', DW_BANDWIDTH)
        return Tool.Execute(Verbose)
    return False

def Terrain_Surface_Texture(DEM=None, TEXTURE=None, EPSILON=None, SCALE=None, METHOD=None, DW_WEIGHTING=None, DW_IDW_POWER=None, DW_BANDWIDTH=None, Verbose=2):
    '''
    Terrain Surface Texture
    ----------
    [ta_morphometry.20]\n
    Terrain surface texture as proposed by Iwahashi & Pike (2007) for subsequent terrain classification.\n
    Arguments
    ----------
    - DEM [`input grid`] : Elevation
    - TEXTURE [`output grid`] : Texture
    - EPSILON [`floating point number`] : Flat Area Threshold. Minimum: 0.000000 Default: 1.000000 maximum difference between original and median filtered elevation (3x3 moving window) that still is recognized flat
    - SCALE [`integer number`] : Scale (Cells). Minimum: 1 Default: 10
    - METHOD [`choice`] : Method. Available Choices: [0] counting cells [1] resampling Default: 1
    - DW_WEIGHTING [`choice`] : Weighting Function. Available Choices: [0] no distance weighting [1] inverse distance to a power [2] exponential [3] gaussian Default: 3
    - DW_IDW_POWER [`floating point number`] : Power. Minimum: 0.000000 Default: 2.000000
    - DW_BANDWIDTH [`floating point number`] : Bandwidth. Minimum: 0.000000 Default: 0.700000 Bandwidth for exponential and Gaussian weighting

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_morphometry', '20', 'Terrain Surface Texture')
    if Tool.is_Okay():
        Tool.Set_Input ('DEM', DEM)
        Tool.Set_Output('TEXTURE', TEXTURE)
        Tool.Set_Option('EPSILON', EPSILON)
        Tool.Set_Option('SCALE', SCALE)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('DW_WEIGHTING', DW_WEIGHTING)
        Tool.Set_Option('DW_IDW_POWER', DW_IDW_POWER)
        Tool.Set_Option('DW_BANDWIDTH', DW_BANDWIDTH)
        return Tool.Execute(Verbose)
    return False

def run_tool_ta_morphometry_20(DEM=None, TEXTURE=None, EPSILON=None, SCALE=None, METHOD=None, DW_WEIGHTING=None, DW_IDW_POWER=None, DW_BANDWIDTH=None, Verbose=2):
    '''
    Terrain Surface Texture
    ----------
    [ta_morphometry.20]\n
    Terrain surface texture as proposed by Iwahashi & Pike (2007) for subsequent terrain classification.\n
    Arguments
    ----------
    - DEM [`input grid`] : Elevation
    - TEXTURE [`output grid`] : Texture
    - EPSILON [`floating point number`] : Flat Area Threshold. Minimum: 0.000000 Default: 1.000000 maximum difference between original and median filtered elevation (3x3 moving window) that still is recognized flat
    - SCALE [`integer number`] : Scale (Cells). Minimum: 1 Default: 10
    - METHOD [`choice`] : Method. Available Choices: [0] counting cells [1] resampling Default: 1
    - DW_WEIGHTING [`choice`] : Weighting Function. Available Choices: [0] no distance weighting [1] inverse distance to a power [2] exponential [3] gaussian Default: 3
    - DW_IDW_POWER [`floating point number`] : Power. Minimum: 0.000000 Default: 2.000000
    - DW_BANDWIDTH [`floating point number`] : Bandwidth. Minimum: 0.000000 Default: 0.700000 Bandwidth for exponential and Gaussian weighting

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_morphometry', '20', 'Terrain Surface Texture')
    if Tool.is_Okay():
        Tool.Set_Input ('DEM', DEM)
        Tool.Set_Output('TEXTURE', TEXTURE)
        Tool.Set_Option('EPSILON', EPSILON)
        Tool.Set_Option('SCALE', SCALE)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('DW_WEIGHTING', DW_WEIGHTING)
        Tool.Set_Option('DW_IDW_POWER', DW_IDW_POWER)
        Tool.Set_Option('DW_BANDWIDTH', DW_BANDWIDTH)
        return Tool.Execute(Verbose)
    return False

def Terrain_Surface_Convexity(DEM=None, CONVEXITY=None, KERNEL=None, TYPE=None, EPSILON=None, SCALE=None, METHOD=None, DW_WEIGHTING=None, DW_IDW_POWER=None, DW_BANDWIDTH=None, Verbose=2):
    '''
    Terrain Surface Convexity
    ----------
    [ta_morphometry.21]\n
    Terrain surface convexity as proposed by Iwahashi & Pike (2007) for subsequent terrain classification.\n
    Arguments
    ----------
    - DEM [`input grid`] : Elevation
    - CONVEXITY [`output grid`] : Convexity
    - KERNEL [`choice`] : Laplacian Filter Kernel. Available Choices: [0] conventional four-neighbourhood [1] conventional eight-neihbourhood [2] eight-neihbourhood (distance based weighting) Default: 0
    - TYPE [`choice`] : Type. Available Choices: [0] convexity [1] concavity Default: 0
    - EPSILON [`floating point number`] : Flat Area Threshold. Minimum: 0.000000 Default: 0.000000
    - SCALE [`integer number`] : Scale (Cells). Minimum: 1 Default: 10
    - METHOD [`choice`] : Method. Available Choices: [0] counting cells [1] resampling Default: 1
    - DW_WEIGHTING [`choice`] : Weighting Function. Available Choices: [0] no distance weighting [1] inverse distance to a power [2] exponential [3] gaussian Default: 3
    - DW_IDW_POWER [`floating point number`] : Power. Minimum: 0.000000 Default: 2.000000
    - DW_BANDWIDTH [`floating point number`] : Bandwidth. Minimum: 0.000000 Default: 0.700000 Bandwidth for exponential and Gaussian weighting

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_morphometry', '21', 'Terrain Surface Convexity')
    if Tool.is_Okay():
        Tool.Set_Input ('DEM', DEM)
        Tool.Set_Output('CONVEXITY', CONVEXITY)
        Tool.Set_Option('KERNEL', KERNEL)
        Tool.Set_Option('TYPE', TYPE)
        Tool.Set_Option('EPSILON', EPSILON)
        Tool.Set_Option('SCALE', SCALE)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('DW_WEIGHTING', DW_WEIGHTING)
        Tool.Set_Option('DW_IDW_POWER', DW_IDW_POWER)
        Tool.Set_Option('DW_BANDWIDTH', DW_BANDWIDTH)
        return Tool.Execute(Verbose)
    return False

def run_tool_ta_morphometry_21(DEM=None, CONVEXITY=None, KERNEL=None, TYPE=None, EPSILON=None, SCALE=None, METHOD=None, DW_WEIGHTING=None, DW_IDW_POWER=None, DW_BANDWIDTH=None, Verbose=2):
    '''
    Terrain Surface Convexity
    ----------
    [ta_morphometry.21]\n
    Terrain surface convexity as proposed by Iwahashi & Pike (2007) for subsequent terrain classification.\n
    Arguments
    ----------
    - DEM [`input grid`] : Elevation
    - CONVEXITY [`output grid`] : Convexity
    - KERNEL [`choice`] : Laplacian Filter Kernel. Available Choices: [0] conventional four-neighbourhood [1] conventional eight-neihbourhood [2] eight-neihbourhood (distance based weighting) Default: 0
    - TYPE [`choice`] : Type. Available Choices: [0] convexity [1] concavity Default: 0
    - EPSILON [`floating point number`] : Flat Area Threshold. Minimum: 0.000000 Default: 0.000000
    - SCALE [`integer number`] : Scale (Cells). Minimum: 1 Default: 10
    - METHOD [`choice`] : Method. Available Choices: [0] counting cells [1] resampling Default: 1
    - DW_WEIGHTING [`choice`] : Weighting Function. Available Choices: [0] no distance weighting [1] inverse distance to a power [2] exponential [3] gaussian Default: 3
    - DW_IDW_POWER [`floating point number`] : Power. Minimum: 0.000000 Default: 2.000000
    - DW_BANDWIDTH [`floating point number`] : Bandwidth. Minimum: 0.000000 Default: 0.700000 Bandwidth for exponential and Gaussian weighting

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_morphometry', '21', 'Terrain Surface Convexity')
    if Tool.is_Okay():
        Tool.Set_Input ('DEM', DEM)
        Tool.Set_Output('CONVEXITY', CONVEXITY)
        Tool.Set_Option('KERNEL', KERNEL)
        Tool.Set_Option('TYPE', TYPE)
        Tool.Set_Option('EPSILON', EPSILON)
        Tool.Set_Option('SCALE', SCALE)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('DW_WEIGHTING', DW_WEIGHTING)
        Tool.Set_Option('DW_IDW_POWER', DW_IDW_POWER)
        Tool.Set_Option('DW_BANDWIDTH', DW_BANDWIDTH)
        return Tool.Execute(Verbose)
    return False

def Terrain_Surface_Classification_Iwahashi_and_Pike(DEM=None, SLOPE=None, CONVEXITY=None, TEXTURE=None, LANDFORMS=None, CONV_RECALC=None, TEXT_RECALC=None, TYPE=None, CONV_SCALE=None, CONV_KERNEL=None, CONV_TYPE=None, CONV_EPSILON=None, TEXT_SCALE=None, TEXT_EPSILON=None, Verbose=2):
    '''
    Terrain Surface Classification (Iwahashi and Pike)
    ----------
    [ta_morphometry.22]\n
    Terrain surface classification as proposed by Iwahashi & Pike (2007).\n
    Arguments
    ----------
    - DEM [`input grid`] : Elevation
    - SLOPE [`optional input grid`] : Slope
    - CONVEXITY [`optional input grid`] : Convexity
    - TEXTURE [`optional input grid`] : Texture
    - LANDFORMS [`output grid`] : Landforms
    - CONV_RECALC [`boolean`] : Recalculate. Default: 0
    - TEXT_RECALC [`boolean`] : Recalculate. Default: 0
    - TYPE [`choice`] : Number of Classes. Available Choices: [0] 8 [1] 12 [2] 16 Default: 2
    - CONV_SCALE [`integer number`] : Scale (Cells). Minimum: 1 Default: 10
    - CONV_KERNEL [`choice`] : Laplacian Filter Kernel. Available Choices: [0] four-neighbourhood [1] eight-neihbourhood [2] eight-neihbourhood (distance based weighting) Default: 0
    - CONV_TYPE [`choice`] : Type. Available Choices: [0] convexity [1] concavity Default: 0
    - CONV_EPSILON [`floating point number`] : Flat Area Threshold. Minimum: 0.000000 Default: 0.000000
    - TEXT_SCALE [`integer number`] : Scale (Cells). Minimum: 10 Default: 2
    - TEXT_EPSILON [`floating point number`] : Flat Area Threshold. Minimum: 0.000000 Default: 1.000000 maximum difference between original and median filtered elevation (3x3 moving window) that still is recognized flat

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_morphometry', '22', 'Terrain Surface Classification (Iwahashi and Pike)')
    if Tool.is_Okay():
        Tool.Set_Input ('DEM', DEM)
        Tool.Set_Input ('SLOPE', SLOPE)
        Tool.Set_Input ('CONVEXITY', CONVEXITY)
        Tool.Set_Input ('TEXTURE', TEXTURE)
        Tool.Set_Output('LANDFORMS', LANDFORMS)
        Tool.Set_Option('CONV_RECALC', CONV_RECALC)
        Tool.Set_Option('TEXT_RECALC', TEXT_RECALC)
        Tool.Set_Option('TYPE', TYPE)
        Tool.Set_Option('CONV_SCALE', CONV_SCALE)
        Tool.Set_Option('CONV_KERNEL', CONV_KERNEL)
        Tool.Set_Option('CONV_TYPE', CONV_TYPE)
        Tool.Set_Option('CONV_EPSILON', CONV_EPSILON)
        Tool.Set_Option('TEXT_SCALE', TEXT_SCALE)
        Tool.Set_Option('TEXT_EPSILON', TEXT_EPSILON)
        return Tool.Execute(Verbose)
    return False

def run_tool_ta_morphometry_22(DEM=None, SLOPE=None, CONVEXITY=None, TEXTURE=None, LANDFORMS=None, CONV_RECALC=None, TEXT_RECALC=None, TYPE=None, CONV_SCALE=None, CONV_KERNEL=None, CONV_TYPE=None, CONV_EPSILON=None, TEXT_SCALE=None, TEXT_EPSILON=None, Verbose=2):
    '''
    Terrain Surface Classification (Iwahashi and Pike)
    ----------
    [ta_morphometry.22]\n
    Terrain surface classification as proposed by Iwahashi & Pike (2007).\n
    Arguments
    ----------
    - DEM [`input grid`] : Elevation
    - SLOPE [`optional input grid`] : Slope
    - CONVEXITY [`optional input grid`] : Convexity
    - TEXTURE [`optional input grid`] : Texture
    - LANDFORMS [`output grid`] : Landforms
    - CONV_RECALC [`boolean`] : Recalculate. Default: 0
    - TEXT_RECALC [`boolean`] : Recalculate. Default: 0
    - TYPE [`choice`] : Number of Classes. Available Choices: [0] 8 [1] 12 [2] 16 Default: 2
    - CONV_SCALE [`integer number`] : Scale (Cells). Minimum: 1 Default: 10
    - CONV_KERNEL [`choice`] : Laplacian Filter Kernel. Available Choices: [0] four-neighbourhood [1] eight-neihbourhood [2] eight-neihbourhood (distance based weighting) Default: 0
    - CONV_TYPE [`choice`] : Type. Available Choices: [0] convexity [1] concavity Default: 0
    - CONV_EPSILON [`floating point number`] : Flat Area Threshold. Minimum: 0.000000 Default: 0.000000
    - TEXT_SCALE [`integer number`] : Scale (Cells). Minimum: 10 Default: 2
    - TEXT_EPSILON [`floating point number`] : Flat Area Threshold. Minimum: 0.000000 Default: 1.000000 maximum difference between original and median filtered elevation (3x3 moving window) that still is recognized flat

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_morphometry', '22', 'Terrain Surface Classification (Iwahashi and Pike)')
    if Tool.is_Okay():
        Tool.Set_Input ('DEM', DEM)
        Tool.Set_Input ('SLOPE', SLOPE)
        Tool.Set_Input ('CONVEXITY', CONVEXITY)
        Tool.Set_Input ('TEXTURE', TEXTURE)
        Tool.Set_Output('LANDFORMS', LANDFORMS)
        Tool.Set_Option('CONV_RECALC', CONV_RECALC)
        Tool.Set_Option('TEXT_RECALC', TEXT_RECALC)
        Tool.Set_Option('TYPE', TYPE)
        Tool.Set_Option('CONV_SCALE', CONV_SCALE)
        Tool.Set_Option('CONV_KERNEL', CONV_KERNEL)
        Tool.Set_Option('CONV_TYPE', CONV_TYPE)
        Tool.Set_Option('CONV_EPSILON', CONV_EPSILON)
        Tool.Set_Option('TEXT_SCALE', TEXT_SCALE)
        Tool.Set_Option('TEXT_EPSILON', TEXT_EPSILON)
        return Tool.Execute(Verbose)
    return False

def Morphometric_Features(DEM=None, FEATURES=None, ELEVATION=None, SLOPE=None, ASPECT=None, PROFC=None, PLANC=None, LONGC=None, CROSC=None, MAXIC=None, MINIC=None, SIZE=None, TOL_SLOPE=None, TOL_CURVE=None, EXPONENT=None, ZSCALE=None, CONSTRAIN=None, Verbose=2):
    '''
    Morphometric Features
    ----------
    [ta_morphometry.23]\n
    Uses a multi-scale approach by fitting quadratic parameters to any size window (via least squares) to derive slope, aspect and curvatures (optional output) for subsequent classification of morphometric features (peaks, ridges, passes, channels, pits and planes). This is the method as proposed and implemented by Jo Wood (1996) in LandSerf and GRASS GIS (r.param.scale).\n
    Optional output is described in the following. Generalised elevation is the smoothed input DEM. Slope is the magnitude of maximum gradient. It is given for steepest slope angle and measured in degrees. Aspect is the direction of maximum gradient. Profile curvature is the curvature intersecting with the plane defined by the Z axis and maximum gradient direction. Positive values describe convex profile curvature, negative values concave profile. Plan curvature is the horizontal curvature, intersecting with the XY plane. Longitudinal curvature is the profile curvature intersecting with the plane defined by the surface normal and maximum gradient direction. Cross-sectional curvature is the tangential curvature intersecting with the plane defined by the surface normal and a tangent to the contour - perpendicular to maximum gradient direction. Minimum curvature is measured in direction perpendicular to the direction of of maximum curvature. The maximum curvature is measured in any direction.\n
    References:\n
    Wood, J. (1996): The Geomorphological characterisation of Digital Elevation Models. Diss., Department of Geography, University of Leicester, U.K. [online](http://www.soi.city.ac.uk/~jwo/phd/).\n
    Wood, J. (2009): Geomorphometry in LandSerf. In: Hengl, T. and Reuter, H.I. [Eds.]: Geomorphometry: Concepts, Software, Applications. Developments in Soil Science, Elsevier, Vol.33, 333-349.\n
    [LandSerf Homepage](http://www.landserf.org/).\n
    Arguments
    ----------
    - DEM [`input grid`] : Elevation
    - FEATURES [`output grid`] : Morphometric Features
    - ELEVATION [`output grid`] : Generalized Surface
    - SLOPE [`output grid`] : Slope
    - ASPECT [`output grid`] : Aspect
    - PROFC [`output grid`] : Profile Curvature
    - PLANC [`output grid`] : Plan Curvature
    - LONGC [`output grid`] : Longitudinal Curvature
    - CROSC [`output grid`] : Cross-Sectional Curvature
    - MAXIC [`output grid`] : Maximum Curvature
    - MINIC [`output grid`] : Minimum Curvature
    - SIZE [`integer number`] : Scale Radius (Cells). Minimum: 1 Default: 5 Size of processing window (= 1 + 2 * radius) given as number of cells
    - TOL_SLOPE [`floating point number`] : Slope Tolerance. Default: 1.000000 Slope tolerance that defines a 'flat' surface (degrees)
    - TOL_CURVE [`floating point number`] : Curvature Tolerance. Default: 0.000100 Curvature tolerance that defines 'planar' surface
    - EXPONENT [`floating point number`] : Distance Weighting Exponent. Minimum: 0.000000 Maximum: 4.000000 Default: 0.000000 Exponent for distance weighting (0.0-4.0)
    - ZSCALE [`floating point number`] : Vertical Scaling. Default: 1.000000 Vertical scaling factor
    - CONSTRAIN [`boolean`] : Constrain. Default: 0 Constrain model through central window cell

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_morphometry', '23', 'Morphometric Features')
    if Tool.is_Okay():
        Tool.Set_Input ('DEM', DEM)
        Tool.Set_Output('FEATURES', FEATURES)
        Tool.Set_Output('ELEVATION', ELEVATION)
        Tool.Set_Output('SLOPE', SLOPE)
        Tool.Set_Output('ASPECT', ASPECT)
        Tool.Set_Output('PROFC', PROFC)
        Tool.Set_Output('PLANC', PLANC)
        Tool.Set_Output('LONGC', LONGC)
        Tool.Set_Output('CROSC', CROSC)
        Tool.Set_Output('MAXIC', MAXIC)
        Tool.Set_Output('MINIC', MINIC)
        Tool.Set_Option('SIZE', SIZE)
        Tool.Set_Option('TOL_SLOPE', TOL_SLOPE)
        Tool.Set_Option('TOL_CURVE', TOL_CURVE)
        Tool.Set_Option('EXPONENT', EXPONENT)
        Tool.Set_Option('ZSCALE', ZSCALE)
        Tool.Set_Option('CONSTRAIN', CONSTRAIN)
        return Tool.Execute(Verbose)
    return False

def run_tool_ta_morphometry_23(DEM=None, FEATURES=None, ELEVATION=None, SLOPE=None, ASPECT=None, PROFC=None, PLANC=None, LONGC=None, CROSC=None, MAXIC=None, MINIC=None, SIZE=None, TOL_SLOPE=None, TOL_CURVE=None, EXPONENT=None, ZSCALE=None, CONSTRAIN=None, Verbose=2):
    '''
    Morphometric Features
    ----------
    [ta_morphometry.23]\n
    Uses a multi-scale approach by fitting quadratic parameters to any size window (via least squares) to derive slope, aspect and curvatures (optional output) for subsequent classification of morphometric features (peaks, ridges, passes, channels, pits and planes). This is the method as proposed and implemented by Jo Wood (1996) in LandSerf and GRASS GIS (r.param.scale).\n
    Optional output is described in the following. Generalised elevation is the smoothed input DEM. Slope is the magnitude of maximum gradient. It is given for steepest slope angle and measured in degrees. Aspect is the direction of maximum gradient. Profile curvature is the curvature intersecting with the plane defined by the Z axis and maximum gradient direction. Positive values describe convex profile curvature, negative values concave profile. Plan curvature is the horizontal curvature, intersecting with the XY plane. Longitudinal curvature is the profile curvature intersecting with the plane defined by the surface normal and maximum gradient direction. Cross-sectional curvature is the tangential curvature intersecting with the plane defined by the surface normal and a tangent to the contour - perpendicular to maximum gradient direction. Minimum curvature is measured in direction perpendicular to the direction of of maximum curvature. The maximum curvature is measured in any direction.\n
    References:\n
    Wood, J. (1996): The Geomorphological characterisation of Digital Elevation Models. Diss., Department of Geography, University of Leicester, U.K. [online](http://www.soi.city.ac.uk/~jwo/phd/).\n
    Wood, J. (2009): Geomorphometry in LandSerf. In: Hengl, T. and Reuter, H.I. [Eds.]: Geomorphometry: Concepts, Software, Applications. Developments in Soil Science, Elsevier, Vol.33, 333-349.\n
    [LandSerf Homepage](http://www.landserf.org/).\n
    Arguments
    ----------
    - DEM [`input grid`] : Elevation
    - FEATURES [`output grid`] : Morphometric Features
    - ELEVATION [`output grid`] : Generalized Surface
    - SLOPE [`output grid`] : Slope
    - ASPECT [`output grid`] : Aspect
    - PROFC [`output grid`] : Profile Curvature
    - PLANC [`output grid`] : Plan Curvature
    - LONGC [`output grid`] : Longitudinal Curvature
    - CROSC [`output grid`] : Cross-Sectional Curvature
    - MAXIC [`output grid`] : Maximum Curvature
    - MINIC [`output grid`] : Minimum Curvature
    - SIZE [`integer number`] : Scale Radius (Cells). Minimum: 1 Default: 5 Size of processing window (= 1 + 2 * radius) given as number of cells
    - TOL_SLOPE [`floating point number`] : Slope Tolerance. Default: 1.000000 Slope tolerance that defines a 'flat' surface (degrees)
    - TOL_CURVE [`floating point number`] : Curvature Tolerance. Default: 0.000100 Curvature tolerance that defines 'planar' surface
    - EXPONENT [`floating point number`] : Distance Weighting Exponent. Minimum: 0.000000 Maximum: 4.000000 Default: 0.000000 Exponent for distance weighting (0.0-4.0)
    - ZSCALE [`floating point number`] : Vertical Scaling. Default: 1.000000 Vertical scaling factor
    - CONSTRAIN [`boolean`] : Constrain. Default: 0 Constrain model through central window cell

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_morphometry', '23', 'Morphometric Features')
    if Tool.is_Okay():
        Tool.Set_Input ('DEM', DEM)
        Tool.Set_Output('FEATURES', FEATURES)
        Tool.Set_Output('ELEVATION', ELEVATION)
        Tool.Set_Output('SLOPE', SLOPE)
        Tool.Set_Output('ASPECT', ASPECT)
        Tool.Set_Output('PROFC', PROFC)
        Tool.Set_Output('PLANC', PLANC)
        Tool.Set_Output('LONGC', LONGC)
        Tool.Set_Output('CROSC', CROSC)
        Tool.Set_Output('MAXIC', MAXIC)
        Tool.Set_Output('MINIC', MINIC)
        Tool.Set_Option('SIZE', SIZE)
        Tool.Set_Option('TOL_SLOPE', TOL_SLOPE)
        Tool.Set_Option('TOL_CURVE', TOL_CURVE)
        Tool.Set_Option('EXPONENT', EXPONENT)
        Tool.Set_Option('ZSCALE', ZSCALE)
        Tool.Set_Option('CONSTRAIN', CONSTRAIN)
        return Tool.Execute(Verbose)
    return False

def Valley_and_Ridge_Detection_Top_Hat_Approach(DEM=None, VALLEY=None, HILL=None, VALLEY_IDX=None, HILL_IDX=None, SLOPE_IDX=None, RADIUS_VALLEY=None, RADIUS_HILL=None, THRESHOLD=None, METHOD=None, Verbose=2):
    '''
    Valley and Ridge Detection (Top Hat Approach)
    ----------
    [ta_morphometry.24]\n
    The tool allows one to calculate fuzzy valley and ridge class memberships from a DEM using the Top Hat approach. The mathematical morphology functions "Opening" and "Closing" form the basis of "The Top Hat Transform" function. The function extracts peaks and valleys with a size condition corresponding to the size of the considered structuring element.\n
    Peaks are extracted by the "White Top Hat" (WTH) function:\n
    WTH = DEM - Opening >= t\n
    Valleys are extracted by the "Black Top Hat" (BTH) function:\n
    BTH = Closing - DEM >= t\n
    The threshold value "t" works as an additional cut-off to extract only the highest peaks and deepest valleys. This means that the functions permit extracting peaks and valleys based on width and height criterions. For details see the referenced paper.\n
    The tool is based on the AML script 'tophat' by Jochen Schmidt, Landcare Research.\n
    Arguments
    ----------
    - DEM [`input grid`] : Elevation
    - VALLEY [`output grid`] : Valley Depth
    - HILL [`output grid`] : Hill Height
    - VALLEY_IDX [`output grid`] : Valley Index
    - HILL_IDX [`output grid`] : Hill Index
    - SLOPE_IDX [`output grid`] : Hillslope Index
    - RADIUS_VALLEY [`floating point number`] : Valley Radius. Minimum: 0.000000 Default: 1000.000000 The radius used to fill valleys [map units].
    - RADIUS_HILL [`floating point number`] : Hill Radius. Minimum: 0.000000 Default: 1000.000000 The radius used to cut hills [map units]
    - THRESHOLD [`floating point number`] : Elevation Threshold. Minimum: 0.000000 Default: 100.000000 The elevation threshold used to identify hills/valleys [map units].
    - METHOD [`choice`] : Slope Index. Available Choices: [0] default [1] alternative Default: 0 Choose the method to calculate the slope index.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_morphometry', '24', 'Valley and Ridge Detection (Top Hat Approach)')
    if Tool.is_Okay():
        Tool.Set_Input ('DEM', DEM)
        Tool.Set_Output('VALLEY', VALLEY)
        Tool.Set_Output('HILL', HILL)
        Tool.Set_Output('VALLEY_IDX', VALLEY_IDX)
        Tool.Set_Output('HILL_IDX', HILL_IDX)
        Tool.Set_Output('SLOPE_IDX', SLOPE_IDX)
        Tool.Set_Option('RADIUS_VALLEY', RADIUS_VALLEY)
        Tool.Set_Option('RADIUS_HILL', RADIUS_HILL)
        Tool.Set_Option('THRESHOLD', THRESHOLD)
        Tool.Set_Option('METHOD', METHOD)
        return Tool.Execute(Verbose)
    return False

def run_tool_ta_morphometry_24(DEM=None, VALLEY=None, HILL=None, VALLEY_IDX=None, HILL_IDX=None, SLOPE_IDX=None, RADIUS_VALLEY=None, RADIUS_HILL=None, THRESHOLD=None, METHOD=None, Verbose=2):
    '''
    Valley and Ridge Detection (Top Hat Approach)
    ----------
    [ta_morphometry.24]\n
    The tool allows one to calculate fuzzy valley and ridge class memberships from a DEM using the Top Hat approach. The mathematical morphology functions "Opening" and "Closing" form the basis of "The Top Hat Transform" function. The function extracts peaks and valleys with a size condition corresponding to the size of the considered structuring element.\n
    Peaks are extracted by the "White Top Hat" (WTH) function:\n
    WTH = DEM - Opening >= t\n
    Valleys are extracted by the "Black Top Hat" (BTH) function:\n
    BTH = Closing - DEM >= t\n
    The threshold value "t" works as an additional cut-off to extract only the highest peaks and deepest valleys. This means that the functions permit extracting peaks and valleys based on width and height criterions. For details see the referenced paper.\n
    The tool is based on the AML script 'tophat' by Jochen Schmidt, Landcare Research.\n
    Arguments
    ----------
    - DEM [`input grid`] : Elevation
    - VALLEY [`output grid`] : Valley Depth
    - HILL [`output grid`] : Hill Height
    - VALLEY_IDX [`output grid`] : Valley Index
    - HILL_IDX [`output grid`] : Hill Index
    - SLOPE_IDX [`output grid`] : Hillslope Index
    - RADIUS_VALLEY [`floating point number`] : Valley Radius. Minimum: 0.000000 Default: 1000.000000 The radius used to fill valleys [map units].
    - RADIUS_HILL [`floating point number`] : Hill Radius. Minimum: 0.000000 Default: 1000.000000 The radius used to cut hills [map units]
    - THRESHOLD [`floating point number`] : Elevation Threshold. Minimum: 0.000000 Default: 100.000000 The elevation threshold used to identify hills/valleys [map units].
    - METHOD [`choice`] : Slope Index. Available Choices: [0] default [1] alternative Default: 0 Choose the method to calculate the slope index.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_morphometry', '24', 'Valley and Ridge Detection (Top Hat Approach)')
    if Tool.is_Okay():
        Tool.Set_Input ('DEM', DEM)
        Tool.Set_Output('VALLEY', VALLEY)
        Tool.Set_Output('HILL', HILL)
        Tool.Set_Output('VALLEY_IDX', VALLEY_IDX)
        Tool.Set_Output('HILL_IDX', HILL_IDX)
        Tool.Set_Output('SLOPE_IDX', SLOPE_IDX)
        Tool.Set_Option('RADIUS_VALLEY', RADIUS_VALLEY)
        Tool.Set_Option('RADIUS_HILL', RADIUS_HILL)
        Tool.Set_Option('THRESHOLD', THRESHOLD)
        Tool.Set_Option('METHOD', METHOD)
        return Tool.Execute(Verbose)
    return False

def Fuzzy_Landform_Element_Classification(ELEVATION=None, SLOPE=None, MINCURV=None, MAXCURV=None, PCURV=None, TCURV=None, FORM=None, MEM=None, ENTROPY=None, CI=None, PLAIN=None, PIT=None, PEAK=None, RIDGE=None, CHANNEL=None, SADDLE=None, BSLOPE=None, FSLOPE=None, SSLOPE=None, HOLLOW=None, FHOLLOW=None, SHOLLOW=None, SPUR=None, FSPUR=None, SSPUR=None, INPUT=None, MEMBERSHIP=None, SLOPETODEG=None, T_SLOPE=None, T_CURVE=None, Verbose=2):
    '''
    Fuzzy Landform Element Classification
    ----------
    [ta_morphometry.25]\n
    Algorithm for derivation of form elements according to slope, maximum curvature, minimum curvature, profile curvature, tangential curvature, based on a linear semantic import model for slope and curvature and a fuzzy classification Based on the AML script 'felementf' by Jochen Schmidt, Landcare Research.\n
    Arguments
    ----------
    - ELEVATION [`input grid`] : Elevation
    - SLOPE [`input grid`] : Slope
    - MINCURV [`input grid`] : Minimum Curvature
    - MAXCURV [`input grid`] : Maximum Curvature
    - PCURV [`input grid`] : Profile Curvature
    - TCURV [`input grid`] : Tangential Curvature
    - FORM [`output grid`] : Landform
    - MEM [`output grid`] : Maximum Membership
    - ENTROPY [`output grid`] : Entropy
    - CI [`output grid`] : Confusion Index
    - PLAIN [`output grid`] : Plain
    - PIT [`output grid`] : Pit
    - PEAK [`output grid`] : Peak
    - RIDGE [`output grid`] : Ridge
    - CHANNEL [`output grid`] : Channel
    - SADDLE [`output grid`] : Saddle
    - BSLOPE [`output grid`] : Back Slope
    - FSLOPE [`output grid`] : Foot Slope
    - SSLOPE [`output grid`] : Shoulder Slope
    - HOLLOW [`output grid`] : Hollow
    - FHOLLOW [`output grid`] : Foot Hollow
    - SHOLLOW [`output grid`] : Shoulder Hollow
    - SPUR [`output grid`] : Spur
    - FSPUR [`output grid`] : Foot Spur
    - SSPUR [`output grid`] : Shoulder Spur
    - INPUT [`choice`] : Input. Available Choices: [0] elevation [1] slope and curvatures Default: 0 if elevation is selected, slopes and curvatures will be calculated internally
    - MEMBERSHIP [`boolean`] : Memberships. Default: 0
    - SLOPETODEG [`choice`] : Slope Grid Units. Available Choices: [0] degree [1] radians Default: 0
    - T_SLOPE [`value range`] : Slope Thresholds [Degree]. lower and upper thresholds for semantic import model, planar vs. sloped areas
    - T_CURVE [`value range`] : Curvature Thresholds [1000 / m]. lower and upper thresholds for semantic import model, straight vs. curved areas

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_morphometry', '25', 'Fuzzy Landform Element Classification')
    if Tool.is_Okay():
        Tool.Set_Input ('ELEVATION', ELEVATION)
        Tool.Set_Input ('SLOPE', SLOPE)
        Tool.Set_Input ('MINCURV', MINCURV)
        Tool.Set_Input ('MAXCURV', MAXCURV)
        Tool.Set_Input ('PCURV', PCURV)
        Tool.Set_Input ('TCURV', TCURV)
        Tool.Set_Output('FORM', FORM)
        Tool.Set_Output('MEM', MEM)
        Tool.Set_Output('ENTROPY', ENTROPY)
        Tool.Set_Output('CI', CI)
        Tool.Set_Output('PLAIN', PLAIN)
        Tool.Set_Output('PIT', PIT)
        Tool.Set_Output('PEAK', PEAK)
        Tool.Set_Output('RIDGE', RIDGE)
        Tool.Set_Output('CHANNEL', CHANNEL)
        Tool.Set_Output('SADDLE', SADDLE)
        Tool.Set_Output('BSLOPE', BSLOPE)
        Tool.Set_Output('FSLOPE', FSLOPE)
        Tool.Set_Output('SSLOPE', SSLOPE)
        Tool.Set_Output('HOLLOW', HOLLOW)
        Tool.Set_Output('FHOLLOW', FHOLLOW)
        Tool.Set_Output('SHOLLOW', SHOLLOW)
        Tool.Set_Output('SPUR', SPUR)
        Tool.Set_Output('FSPUR', FSPUR)
        Tool.Set_Output('SSPUR', SSPUR)
        Tool.Set_Option('INPUT', INPUT)
        Tool.Set_Option('MEMBERSHIP', MEMBERSHIP)
        Tool.Set_Option('SLOPETODEG', SLOPETODEG)
        Tool.Set_Option('T_SLOPE', T_SLOPE)
        Tool.Set_Option('T_CURVE', T_CURVE)
        return Tool.Execute(Verbose)
    return False

def run_tool_ta_morphometry_25(ELEVATION=None, SLOPE=None, MINCURV=None, MAXCURV=None, PCURV=None, TCURV=None, FORM=None, MEM=None, ENTROPY=None, CI=None, PLAIN=None, PIT=None, PEAK=None, RIDGE=None, CHANNEL=None, SADDLE=None, BSLOPE=None, FSLOPE=None, SSLOPE=None, HOLLOW=None, FHOLLOW=None, SHOLLOW=None, SPUR=None, FSPUR=None, SSPUR=None, INPUT=None, MEMBERSHIP=None, SLOPETODEG=None, T_SLOPE=None, T_CURVE=None, Verbose=2):
    '''
    Fuzzy Landform Element Classification
    ----------
    [ta_morphometry.25]\n
    Algorithm for derivation of form elements according to slope, maximum curvature, minimum curvature, profile curvature, tangential curvature, based on a linear semantic import model for slope and curvature and a fuzzy classification Based on the AML script 'felementf' by Jochen Schmidt, Landcare Research.\n
    Arguments
    ----------
    - ELEVATION [`input grid`] : Elevation
    - SLOPE [`input grid`] : Slope
    - MINCURV [`input grid`] : Minimum Curvature
    - MAXCURV [`input grid`] : Maximum Curvature
    - PCURV [`input grid`] : Profile Curvature
    - TCURV [`input grid`] : Tangential Curvature
    - FORM [`output grid`] : Landform
    - MEM [`output grid`] : Maximum Membership
    - ENTROPY [`output grid`] : Entropy
    - CI [`output grid`] : Confusion Index
    - PLAIN [`output grid`] : Plain
    - PIT [`output grid`] : Pit
    - PEAK [`output grid`] : Peak
    - RIDGE [`output grid`] : Ridge
    - CHANNEL [`output grid`] : Channel
    - SADDLE [`output grid`] : Saddle
    - BSLOPE [`output grid`] : Back Slope
    - FSLOPE [`output grid`] : Foot Slope
    - SSLOPE [`output grid`] : Shoulder Slope
    - HOLLOW [`output grid`] : Hollow
    - FHOLLOW [`output grid`] : Foot Hollow
    - SHOLLOW [`output grid`] : Shoulder Hollow
    - SPUR [`output grid`] : Spur
    - FSPUR [`output grid`] : Foot Spur
    - SSPUR [`output grid`] : Shoulder Spur
    - INPUT [`choice`] : Input. Available Choices: [0] elevation [1] slope and curvatures Default: 0 if elevation is selected, slopes and curvatures will be calculated internally
    - MEMBERSHIP [`boolean`] : Memberships. Default: 0
    - SLOPETODEG [`choice`] : Slope Grid Units. Available Choices: [0] degree [1] radians Default: 0
    - T_SLOPE [`value range`] : Slope Thresholds [Degree]. lower and upper thresholds for semantic import model, planar vs. sloped areas
    - T_CURVE [`value range`] : Curvature Thresholds [1000 / m]. lower and upper thresholds for semantic import model, straight vs. curved areas

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_morphometry', '25', 'Fuzzy Landform Element Classification')
    if Tool.is_Okay():
        Tool.Set_Input ('ELEVATION', ELEVATION)
        Tool.Set_Input ('SLOPE', SLOPE)
        Tool.Set_Input ('MINCURV', MINCURV)
        Tool.Set_Input ('MAXCURV', MAXCURV)
        Tool.Set_Input ('PCURV', PCURV)
        Tool.Set_Input ('TCURV', TCURV)
        Tool.Set_Output('FORM', FORM)
        Tool.Set_Output('MEM', MEM)
        Tool.Set_Output('ENTROPY', ENTROPY)
        Tool.Set_Output('CI', CI)
        Tool.Set_Output('PLAIN', PLAIN)
        Tool.Set_Output('PIT', PIT)
        Tool.Set_Output('PEAK', PEAK)
        Tool.Set_Output('RIDGE', RIDGE)
        Tool.Set_Output('CHANNEL', CHANNEL)
        Tool.Set_Output('SADDLE', SADDLE)
        Tool.Set_Output('BSLOPE', BSLOPE)
        Tool.Set_Output('FSLOPE', FSLOPE)
        Tool.Set_Output('SSLOPE', SSLOPE)
        Tool.Set_Output('HOLLOW', HOLLOW)
        Tool.Set_Output('FHOLLOW', FHOLLOW)
        Tool.Set_Output('SHOLLOW', SHOLLOW)
        Tool.Set_Output('SPUR', SPUR)
        Tool.Set_Output('FSPUR', FSPUR)
        Tool.Set_Output('SSPUR', SSPUR)
        Tool.Set_Option('INPUT', INPUT)
        Tool.Set_Option('MEMBERSHIP', MEMBERSHIP)
        Tool.Set_Option('SLOPETODEG', SLOPETODEG)
        Tool.Set_Option('T_SLOPE', T_SLOPE)
        Tool.Set_Option('T_CURVE', T_CURVE)
        return Tool.Execute(Verbose)
    return False

def Upslope_and_Downslope_Curvature(DEM=None, C_LOCAL=None, C_UP=None, C_UP_LOCAL=None, C_DOWN=None, C_DOWN_LOCAL=None, WEIGHTING=None, Verbose=2):
    '''
    Upslope and Downslope Curvature
    ----------
    [ta_morphometry.26]\n
    This tool first calculates the local curvature of a cell as sum of the gradients (i.e. tangens of slope) to its neighbour cells. This is a simple estimation of the general curvature and is strongly correlated with general curvatures calculated with other methods (e.g. Zevenbergen & Thorne 1987). Then upslope curvature is calculated as the distance and flow proportional weighted average local curvature over a cell's upslope contributing area following the multiple flow direction algorithm after Freeman (1991). In a similar way the downslope curvature is calculated by summarizing the curvatures of all hydrologically downslope connected cells. The local upslope/downslope curvatures just take the immediately neighboured cells into account.\n
    Arguments
    ----------
    - DEM [`input grid`] : Elevation
    - C_LOCAL [`output grid`] : Local Curvature
    - C_UP [`output grid`] : Upslope Curvature
    - C_UP_LOCAL [`output grid`] : Local Upslope Curvature
    - C_DOWN [`output grid`] : Downslope Curvature
    - C_DOWN_LOCAL [`output grid`] : Local Downslope Curvature
    - WEIGHTING [`floating point number`] : Upslope Weighting. Minimum: 0.000000 Maximum: 1.000000 Default: 0.500000

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_morphometry', '26', 'Upslope and Downslope Curvature')
    if Tool.is_Okay():
        Tool.Set_Input ('DEM', DEM)
        Tool.Set_Output('C_LOCAL', C_LOCAL)
        Tool.Set_Output('C_UP', C_UP)
        Tool.Set_Output('C_UP_LOCAL', C_UP_LOCAL)
        Tool.Set_Output('C_DOWN', C_DOWN)
        Tool.Set_Output('C_DOWN_LOCAL', C_DOWN_LOCAL)
        Tool.Set_Option('WEIGHTING', WEIGHTING)
        return Tool.Execute(Verbose)
    return False

def run_tool_ta_morphometry_26(DEM=None, C_LOCAL=None, C_UP=None, C_UP_LOCAL=None, C_DOWN=None, C_DOWN_LOCAL=None, WEIGHTING=None, Verbose=2):
    '''
    Upslope and Downslope Curvature
    ----------
    [ta_morphometry.26]\n
    This tool first calculates the local curvature of a cell as sum of the gradients (i.e. tangens of slope) to its neighbour cells. This is a simple estimation of the general curvature and is strongly correlated with general curvatures calculated with other methods (e.g. Zevenbergen & Thorne 1987). Then upslope curvature is calculated as the distance and flow proportional weighted average local curvature over a cell's upslope contributing area following the multiple flow direction algorithm after Freeman (1991). In a similar way the downslope curvature is calculated by summarizing the curvatures of all hydrologically downslope connected cells. The local upslope/downslope curvatures just take the immediately neighboured cells into account.\n
    Arguments
    ----------
    - DEM [`input grid`] : Elevation
    - C_LOCAL [`output grid`] : Local Curvature
    - C_UP [`output grid`] : Upslope Curvature
    - C_UP_LOCAL [`output grid`] : Local Upslope Curvature
    - C_DOWN [`output grid`] : Downslope Curvature
    - C_DOWN_LOCAL [`output grid`] : Local Downslope Curvature
    - WEIGHTING [`floating point number`] : Upslope Weighting. Minimum: 0.000000 Maximum: 1.000000 Default: 0.500000

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_morphometry', '26', 'Upslope and Downslope Curvature')
    if Tool.is_Okay():
        Tool.Set_Input ('DEM', DEM)
        Tool.Set_Output('C_LOCAL', C_LOCAL)
        Tool.Set_Output('C_UP', C_UP)
        Tool.Set_Output('C_UP_LOCAL', C_UP_LOCAL)
        Tool.Set_Output('C_DOWN', C_DOWN)
        Tool.Set_Output('C_DOWN_LOCAL', C_DOWN_LOCAL)
        Tool.Set_Option('WEIGHTING', WEIGHTING)
        return Tool.Execute(Verbose)
    return False

def Wind_Exposition_Index(DEM=None, EXPOSITION=None, MAXDIST=None, STEP=None, OLDVER=None, ACCEL=None, PYRAMIDS=None, Verbose=2):
    '''
    Wind Exposition Index
    ----------
    [ta_morphometry.27]\n
    This tool calculates the average 'Wind Effect Index' for all directions using an angular step. Like the 'Wind Effect Index' it is a dimensionless index. Values below 1 indicate wind shadowed areas whereas values above 1 indicate areas exposed to wind.\n
    Arguments
    ----------
    - DEM [`input grid`] : Elevation
    - EXPOSITION [`output grid`] : Wind Exposition
    - MAXDIST [`floating point number`] : Search Distance [km]. Minimum: 0.000000 Default: 300.000000
    - STEP [`floating point number`] : Angular Step Size (Degree). Minimum: 1.000000 Maximum: 45.000000 Default: 15.000000
    - OLDVER [`boolean`] : Old Version. Default: 0 use old version for constant wind direction (no acceleration and averaging option)
    - ACCEL [`floating point number`] : Acceleration. Minimum: 1.000000 Default: 1.500000
    - PYRAMIDS [`boolean`] : Elevation Averaging. Default: 0 use more averaged elevations when looking at increasing distances

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_morphometry', '27', 'Wind Exposition Index')
    if Tool.is_Okay():
        Tool.Set_Input ('DEM', DEM)
        Tool.Set_Output('EXPOSITION', EXPOSITION)
        Tool.Set_Option('MAXDIST', MAXDIST)
        Tool.Set_Option('STEP', STEP)
        Tool.Set_Option('OLDVER', OLDVER)
        Tool.Set_Option('ACCEL', ACCEL)
        Tool.Set_Option('PYRAMIDS', PYRAMIDS)
        return Tool.Execute(Verbose)
    return False

def run_tool_ta_morphometry_27(DEM=None, EXPOSITION=None, MAXDIST=None, STEP=None, OLDVER=None, ACCEL=None, PYRAMIDS=None, Verbose=2):
    '''
    Wind Exposition Index
    ----------
    [ta_morphometry.27]\n
    This tool calculates the average 'Wind Effect Index' for all directions using an angular step. Like the 'Wind Effect Index' it is a dimensionless index. Values below 1 indicate wind shadowed areas whereas values above 1 indicate areas exposed to wind.\n
    Arguments
    ----------
    - DEM [`input grid`] : Elevation
    - EXPOSITION [`output grid`] : Wind Exposition
    - MAXDIST [`floating point number`] : Search Distance [km]. Minimum: 0.000000 Default: 300.000000
    - STEP [`floating point number`] : Angular Step Size (Degree). Minimum: 1.000000 Maximum: 45.000000 Default: 15.000000
    - OLDVER [`boolean`] : Old Version. Default: 0 use old version for constant wind direction (no acceleration and averaging option)
    - ACCEL [`floating point number`] : Acceleration. Minimum: 1.000000 Default: 1.500000
    - PYRAMIDS [`boolean`] : Elevation Averaging. Default: 0 use more averaged elevations when looking at increasing distances

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_morphometry', '27', 'Wind Exposition Index')
    if Tool.is_Okay():
        Tool.Set_Input ('DEM', DEM)
        Tool.Set_Output('EXPOSITION', EXPOSITION)
        Tool.Set_Option('MAXDIST', MAXDIST)
        Tool.Set_Option('STEP', STEP)
        Tool.Set_Option('OLDVER', OLDVER)
        Tool.Set_Option('ACCEL', ACCEL)
        Tool.Set_Option('PYRAMIDS', PYRAMIDS)
        return Tool.Execute(Verbose)
    return False

def MultiScale_Topographic_Position_Index_TPI(DEM=None, TPI=None, SCALE_MIN=None, SCALE_MAX=None, SCALE_NUM=None, Verbose=2):
    '''
    Multi-Scale Topographic Position Index (TPI)
    ----------
    [ta_morphometry.28]\n
    Topographic Position Index (TPI) calculation as proposed by Guisan et al. (1999).\n
    This implementation calculates the TPI for different scales and integrates these into one single grid. The hierarchical integration is achieved by starting with the standardized TPI values of the largest scale, then adding standardized values from smaller scales where the (absolute) values from the smaller scale exceed those from the larger scale. This integration scheme has been proposed by N. Zimmermann.\n
    Arguments
    ----------
    - DEM [`input grid`] : Elevation
    - TPI [`output grid`] : Topographic Position Index
    - SCALE_MIN [`integer number`] : Minimum Scale. Minimum: 1 Default: 1 kernel radius in cells
    - SCALE_MAX [`integer number`] : Maximum Scale. Minimum: 1 Default: 8 kernel radius in cells
    - SCALE_NUM [`integer number`] : Number of Scales. Minimum: 2 Default: 3

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_morphometry', '28', 'Multi-Scale Topographic Position Index (TPI)')
    if Tool.is_Okay():
        Tool.Set_Input ('DEM', DEM)
        Tool.Set_Output('TPI', TPI)
        Tool.Set_Option('SCALE_MIN', SCALE_MIN)
        Tool.Set_Option('SCALE_MAX', SCALE_MAX)
        Tool.Set_Option('SCALE_NUM', SCALE_NUM)
        return Tool.Execute(Verbose)
    return False

def run_tool_ta_morphometry_28(DEM=None, TPI=None, SCALE_MIN=None, SCALE_MAX=None, SCALE_NUM=None, Verbose=2):
    '''
    Multi-Scale Topographic Position Index (TPI)
    ----------
    [ta_morphometry.28]\n
    Topographic Position Index (TPI) calculation as proposed by Guisan et al. (1999).\n
    This implementation calculates the TPI for different scales and integrates these into one single grid. The hierarchical integration is achieved by starting with the standardized TPI values of the largest scale, then adding standardized values from smaller scales where the (absolute) values from the smaller scale exceed those from the larger scale. This integration scheme has been proposed by N. Zimmermann.\n
    Arguments
    ----------
    - DEM [`input grid`] : Elevation
    - TPI [`output grid`] : Topographic Position Index
    - SCALE_MIN [`integer number`] : Minimum Scale. Minimum: 1 Default: 1 kernel radius in cells
    - SCALE_MAX [`integer number`] : Maximum Scale. Minimum: 1 Default: 8 kernel radius in cells
    - SCALE_NUM [`integer number`] : Number of Scales. Minimum: 2 Default: 3

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_morphometry', '28', 'Multi-Scale Topographic Position Index (TPI)')
    if Tool.is_Okay():
        Tool.Set_Input ('DEM', DEM)
        Tool.Set_Output('TPI', TPI)
        Tool.Set_Option('SCALE_MIN', SCALE_MIN)
        Tool.Set_Option('SCALE_MAX', SCALE_MAX)
        Tool.Set_Option('SCALE_NUM', SCALE_NUM)
        return Tool.Execute(Verbose)
    return False

def Wind_Shelter_Index(ELEVATION=None, SHELTER=None, UNIT=None, DIRECTION=None, TOLERANCE=None, DISTANCE=None, QUANTILE=None, NEGATIVES=None, METHOD=None, Verbose=2):
    '''
    Wind Shelter Index
    ----------
    [ta_morphometry.29]\n
    This tool reimplements the Wind Shelter Index proposed by Plattner et al. (2004), that has originally been implemented within the RSAGA package.\n
    Arguments
    ----------
    - ELEVATION [`input grid`] : Elevation
    - SHELTER [`output grid`] : Wind Shelter Index
    - UNIT [`choice`] : Unit. Available Choices: [0] degree [1] radians Default: 0
    - DIRECTION [`floating point number`] : Wind Direction. Default: 135.000000 The direction [degree] into which the wind blows.
    - TOLERANCE [`floating point number`] : Tolerance. Minimum: 0.000000 Maximum: 90.000000 Default: 5.000000 The tolerance angle [degree] to the left and right of the direction angle.
    - DISTANCE [`floating point number`] : Distance. Minimum: 1.000000 Default: 5.000000 [cells]
    - QUANTILE [`floating point number`] : Quantile. Minimum: 0.000000 Maximum: 1.000000 Default: 1.000000 Set quantile to one to get the maximum slope angle.
    - NEGATIVES [`boolean`] : Include Negative Slopes. Default: 0
    - METHOD [`choice`] : Method. Available Choices: [0] windward [1] leeward Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_morphometry', '29', 'Wind Shelter Index')
    if Tool.is_Okay():
        Tool.Set_Input ('ELEVATION', ELEVATION)
        Tool.Set_Output('SHELTER', SHELTER)
        Tool.Set_Option('UNIT', UNIT)
        Tool.Set_Option('DIRECTION', DIRECTION)
        Tool.Set_Option('TOLERANCE', TOLERANCE)
        Tool.Set_Option('DISTANCE', DISTANCE)
        Tool.Set_Option('QUANTILE', QUANTILE)
        Tool.Set_Option('NEGATIVES', NEGATIVES)
        Tool.Set_Option('METHOD', METHOD)
        return Tool.Execute(Verbose)
    return False

def run_tool_ta_morphometry_29(ELEVATION=None, SHELTER=None, UNIT=None, DIRECTION=None, TOLERANCE=None, DISTANCE=None, QUANTILE=None, NEGATIVES=None, METHOD=None, Verbose=2):
    '''
    Wind Shelter Index
    ----------
    [ta_morphometry.29]\n
    This tool reimplements the Wind Shelter Index proposed by Plattner et al. (2004), that has originally been implemented within the RSAGA package.\n
    Arguments
    ----------
    - ELEVATION [`input grid`] : Elevation
    - SHELTER [`output grid`] : Wind Shelter Index
    - UNIT [`choice`] : Unit. Available Choices: [0] degree [1] radians Default: 0
    - DIRECTION [`floating point number`] : Wind Direction. Default: 135.000000 The direction [degree] into which the wind blows.
    - TOLERANCE [`floating point number`] : Tolerance. Minimum: 0.000000 Maximum: 90.000000 Default: 5.000000 The tolerance angle [degree] to the left and right of the direction angle.
    - DISTANCE [`floating point number`] : Distance. Minimum: 1.000000 Default: 5.000000 [cells]
    - QUANTILE [`floating point number`] : Quantile. Minimum: 0.000000 Maximum: 1.000000 Default: 1.000000 Set quantile to one to get the maximum slope angle.
    - NEGATIVES [`boolean`] : Include Negative Slopes. Default: 0
    - METHOD [`choice`] : Method. Available Choices: [0] windward [1] leeward Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_morphometry', '29', 'Wind Shelter Index')
    if Tool.is_Okay():
        Tool.Set_Input ('ELEVATION', ELEVATION)
        Tool.Set_Output('SHELTER', SHELTER)
        Tool.Set_Option('UNIT', UNIT)
        Tool.Set_Option('DIRECTION', DIRECTION)
        Tool.Set_Option('TOLERANCE', TOLERANCE)
        Tool.Set_Option('DISTANCE', DISTANCE)
        Tool.Set_Option('QUANTILE', QUANTILE)
        Tool.Set_Option('NEGATIVES', NEGATIVES)
        Tool.Set_Option('METHOD', METHOD)
        return Tool.Execute(Verbose)
    return False

def Terrain_Clustering(ELEVATION=None, CLUSTER=None, GRID_SYSTEM=None, NCLUSTER=None, MAXITER=None, Verbose=2):
    '''
    Terrain Clustering
    ----------
    [ta_morphometry.clustering]\n
    Terrain Clustering\n
    Arguments
    ----------
    - ELEVATION [`input grid`] : Elevation
    - CLUSTER [`output grid`] : Clusters
    - GRID_SYSTEM [`grid system`] : Grid System
    - NCLUSTER [`integer number`] : Number of Terrain Classes. Default: 5
    - MAXITER [`integer number`] : Maximum Iterations. Default: 25

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_morphometry', 'clustering', 'Terrain Clustering')
    if Tool.is_Okay():
        Tool.Set_Input ('ELEVATION', ELEVATION)
        Tool.Set_Output('CLUSTER', CLUSTER)
        Tool.Set_Option('GRID_SYSTEM', GRID_SYSTEM)
        Tool.Set_Option('NCLUSTER', NCLUSTER)
        Tool.Set_Option('MAXITER', MAXITER)
        return Tool.Execute(Verbose)
    return False

def run_tool_ta_morphometry_clustering(ELEVATION=None, CLUSTER=None, GRID_SYSTEM=None, NCLUSTER=None, MAXITER=None, Verbose=2):
    '''
    Terrain Clustering
    ----------
    [ta_morphometry.clustering]\n
    Terrain Clustering\n
    Arguments
    ----------
    - ELEVATION [`input grid`] : Elevation
    - CLUSTER [`output grid`] : Clusters
    - GRID_SYSTEM [`grid system`] : Grid System
    - NCLUSTER [`integer number`] : Number of Terrain Classes. Default: 5
    - MAXITER [`integer number`] : Maximum Iterations. Default: 25

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_morphometry', 'clustering', 'Terrain Clustering')
    if Tool.is_Okay():
        Tool.Set_Input ('ELEVATION', ELEVATION)
        Tool.Set_Output('CLUSTER', CLUSTER)
        Tool.Set_Option('GRID_SYSTEM', GRID_SYSTEM)
        Tool.Set_Option('NCLUSTER', NCLUSTER)
        Tool.Set_Option('MAXITER', MAXITER)
        return Tool.Execute(Verbose)
    return False

def Terrain_Segmentation(ELEVATION=None, OBJECTS=None, ELEVATION_GRIDSYSTEM=None, TPI_RADIUS=None, BAND_WIDTH=None, NCLUSTER=None, Verbose=2):
    '''
    Terrain Segmentation
    ----------
    [ta_morphometry.segmentation]\n
    Terrain Segmentation\n
    Arguments
    ----------
    - ELEVATION [`input grid`] : Elevation
    - OBJECTS [`output shapes`] : Objects
    - ELEVATION_GRIDSYSTEM [`grid system`] : Grid system
    - TPI_RADIUS [`value range`] : Position Index Radius. position index radius in map units
    - BAND_WIDTH [`floating point number`] : Band Width. Default: 2.000000 increase band width to get less seed points
    - NCLUSTER [`integer number`] : Number of Clusters. Default: 10 number of clusters

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_morphometry', 'segmentation', 'Terrain Segmentation')
    if Tool.is_Okay():
        Tool.Set_Input ('ELEVATION', ELEVATION)
        Tool.Set_Output('OBJECTS', OBJECTS)
        Tool.Set_Option('ELEVATION_GRIDSYSTEM', ELEVATION_GRIDSYSTEM)
        Tool.Set_Option('TPI_RADIUS', TPI_RADIUS)
        Tool.Set_Option('BAND_WIDTH', BAND_WIDTH)
        Tool.Set_Option('NCLUSTER', NCLUSTER)
        return Tool.Execute(Verbose)
    return False

def run_tool_ta_morphometry_segmentation(ELEVATION=None, OBJECTS=None, ELEVATION_GRIDSYSTEM=None, TPI_RADIUS=None, BAND_WIDTH=None, NCLUSTER=None, Verbose=2):
    '''
    Terrain Segmentation
    ----------
    [ta_morphometry.segmentation]\n
    Terrain Segmentation\n
    Arguments
    ----------
    - ELEVATION [`input grid`] : Elevation
    - OBJECTS [`output shapes`] : Objects
    - ELEVATION_GRIDSYSTEM [`grid system`] : Grid system
    - TPI_RADIUS [`value range`] : Position Index Radius. position index radius in map units
    - BAND_WIDTH [`floating point number`] : Band Width. Default: 2.000000 increase band width to get less seed points
    - NCLUSTER [`integer number`] : Number of Clusters. Default: 10 number of clusters

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_morphometry', 'segmentation', 'Terrain Segmentation')
    if Tool.is_Okay():
        Tool.Set_Input ('ELEVATION', ELEVATION)
        Tool.Set_Output('OBJECTS', OBJECTS)
        Tool.Set_Option('ELEVATION_GRIDSYSTEM', ELEVATION_GRIDSYSTEM)
        Tool.Set_Option('TPI_RADIUS', TPI_RADIUS)
        Tool.Set_Option('BAND_WIDTH', BAND_WIDTH)
        Tool.Set_Option('NCLUSTER', NCLUSTER)
        return Tool.Execute(Verbose)
    return False

def Summit_Extraction(SURFACE=None, SUMMITS=None, SURFACE_GRIDSYSTEM=None, METHOD=None, SCALE_FILTER=None, SCALE_TPI=None, THRESHOLD=None, Verbose=2):
    '''
    Summit Extraction
    ----------
    [ta_morphometry.summits]\n
    This is a simple summit detection tool, e.g. for the analysis of Digital Elevation Models. It calculates the distance above a trend surface (using either the grid resampling filter or the topographic position index) to decide, if a local maximum in the input surface grid represents a summit.\n
    Arguments
    ----------
    - SURFACE [`input grid`] : Surface
    - SUMMITS [`output shapes`] : Summits
    - SURFACE_GRIDSYSTEM [`grid system`] : Grid system
    - METHOD [`choice`] : Trend Surface. Available Choices: [0] Resampling Filter [1] Topographic Position Index (TPI) Default: 0
    - SCALE_FILTER [`floating point number`] : Scale. Minimum: 1.000000 Default: 10.000000 The scale (expressed as number of input grid cells) at which the trend surface will be created.
    - SCALE_TPI [`floating point number`] : Scale. Minimum: 1.000000 Default: 300.000000 The scale (expressed as map units) at which the trend surface will be created.
    - THRESHOLD [`floating point number`] : Threshold. Minimum: 0.000000 Default: 10.000000 The minimum distance above the trend surface for summit cells.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_morphometry', 'summits', 'Summit Extraction')
    if Tool.is_Okay():
        Tool.Set_Input ('SURFACE', SURFACE)
        Tool.Set_Output('SUMMITS', SUMMITS)
        Tool.Set_Option('SURFACE_GRIDSYSTEM', SURFACE_GRIDSYSTEM)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('SCALE_FILTER', SCALE_FILTER)
        Tool.Set_Option('SCALE_TPI', SCALE_TPI)
        Tool.Set_Option('THRESHOLD', THRESHOLD)
        return Tool.Execute(Verbose)
    return False

def run_tool_ta_morphometry_summits(SURFACE=None, SUMMITS=None, SURFACE_GRIDSYSTEM=None, METHOD=None, SCALE_FILTER=None, SCALE_TPI=None, THRESHOLD=None, Verbose=2):
    '''
    Summit Extraction
    ----------
    [ta_morphometry.summits]\n
    This is a simple summit detection tool, e.g. for the analysis of Digital Elevation Models. It calculates the distance above a trend surface (using either the grid resampling filter or the topographic position index) to decide, if a local maximum in the input surface grid represents a summit.\n
    Arguments
    ----------
    - SURFACE [`input grid`] : Surface
    - SUMMITS [`output shapes`] : Summits
    - SURFACE_GRIDSYSTEM [`grid system`] : Grid system
    - METHOD [`choice`] : Trend Surface. Available Choices: [0] Resampling Filter [1] Topographic Position Index (TPI) Default: 0
    - SCALE_FILTER [`floating point number`] : Scale. Minimum: 1.000000 Default: 10.000000 The scale (expressed as number of input grid cells) at which the trend surface will be created.
    - SCALE_TPI [`floating point number`] : Scale. Minimum: 1.000000 Default: 300.000000 The scale (expressed as map units) at which the trend surface will be created.
    - THRESHOLD [`floating point number`] : Threshold. Minimum: 0.000000 Default: 10.000000 The minimum distance above the trend surface for summit cells.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_morphometry', 'summits', 'Summit Extraction')
    if Tool.is_Okay():
        Tool.Set_Input ('SURFACE', SURFACE)
        Tool.Set_Output('SUMMITS', SUMMITS)
        Tool.Set_Option('SURFACE_GRIDSYSTEM', SURFACE_GRIDSYSTEM)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('SCALE_FILTER', SCALE_FILTER)
        Tool.Set_Option('SCALE_TPI', SCALE_TPI)
        Tool.Set_Option('THRESHOLD', THRESHOLD)
        return Tool.Execute(Verbose)
    return False

