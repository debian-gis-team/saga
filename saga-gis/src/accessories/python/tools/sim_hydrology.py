#! /usr/bin/env python

'''
Python Interface to SAGA Tools Library
----------
- Category : Simulation
- Name     : Hydrology
- ID       : sim_hydrology

Description
----------
Simulation of hydrological processes.
'''

from PySAGA.helper import Tool_Wrapper

def Top_Soil_Water_Content(STA_FC=None, STA_PWP=None, LANDUSE=None, DYN_W=None, STA_FC_DEFAULT=None, STA_PWP_DEFAULT=None, LANDUSE_DEF=None, DYN_CLIMATE=None, STA_KC=None, Verbose=2):
    '''
    Top Soil Water Content
    ----------
    [sim_hydrology.0]\n
    The WEELS (Wind Erosion on European Light Soils) soil moisture model estimates the top soil water content on a daily basis using the Haude approach for evapotranspiration (cf. DVWK 1996). Input data is\n
    (-) daily weather\n
    (-) precipitation\n
    (-) temperature\n
    (-) air humidity\n
    \n
    (-) soil properties\n
    (-) field capacity\n
    (-) permanent wilting point\n
    \n
    (-) crop type\n
    Arguments
    ----------
    - STA_FC [`optional input grid`] : Field Capacity. [mm]
    - STA_PWP [`optional input grid`] : Permanent Wilting Point. [mm]
    - LANDUSE [`optional input grid`] : Land Use
    - DYN_W [`output grid`] : Soil Moisture
    - STA_FC_DEFAULT [`floating point number`] : Default. Minimum: 0.000000 Maximum: 100.000000 Default: 20.000000 default value if no grid has been selected
    - STA_PWP_DEFAULT [`floating point number`] : Default. Minimum: 0.000000 Maximum: 100.000000 Default: 2.000000 default value if no grid has been selected
    - LANDUSE_DEF [`integer number`] : Default. Default: -1
    - DYN_CLIMATE [`static table`] : Climate Data. 3 Fields: - 1. [8 byte floating point number] Precipitation [mm] - 2. [8 byte floating point number] Temperature [2pm Deg.C] - 3. [8 byte floating point number] Air Humidity [2pm [%] 
    - STA_KC [`static table`] : Crop Coefficients. 14 Fields: - 1. [signed 4 byte integer] Land Use ID - 2. [string] Name - 3. [8 byte floating point number] January - 4. [8 byte floating point number] February - 5. [8 byte floating point number] March - 6. [8 byte floating point number] April - 7. [8 byte floating point number] May - 8. [8 byte floating point number] June - 9. [8 byte floating point number] July - 10. [8 byte floating point number] August - 11. [8 byte floating point number] September - 12. [8 byte floating point number] October - 13. [8 byte floating point number] November - 14. [8 byte floating point number] December 

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('sim_hydrology', '0', 'Top Soil Water Content')
    if Tool.is_Okay():
        Tool.Set_Input ('STA_FC', STA_FC)
        Tool.Set_Input ('STA_PWP', STA_PWP)
        Tool.Set_Input ('LANDUSE', LANDUSE)
        Tool.Set_Output('DYN_W', DYN_W)
        Tool.Set_Option('STA_FC_DEFAULT', STA_FC_DEFAULT)
        Tool.Set_Option('STA_PWP_DEFAULT', STA_PWP_DEFAULT)
        Tool.Set_Option('LANDUSE_DEF', LANDUSE_DEF)
        Tool.Set_Option('DYN_CLIMATE', DYN_CLIMATE)
        Tool.Set_Option('STA_KC', STA_KC)
        return Tool.Execute(Verbose)
    return False

def run_tool_sim_hydrology_0(STA_FC=None, STA_PWP=None, LANDUSE=None, DYN_W=None, STA_FC_DEFAULT=None, STA_PWP_DEFAULT=None, LANDUSE_DEF=None, DYN_CLIMATE=None, STA_KC=None, Verbose=2):
    '''
    Top Soil Water Content
    ----------
    [sim_hydrology.0]\n
    The WEELS (Wind Erosion on European Light Soils) soil moisture model estimates the top soil water content on a daily basis using the Haude approach for evapotranspiration (cf. DVWK 1996). Input data is\n
    (-) daily weather\n
    (-) precipitation\n
    (-) temperature\n
    (-) air humidity\n
    \n
    (-) soil properties\n
    (-) field capacity\n
    (-) permanent wilting point\n
    \n
    (-) crop type\n
    Arguments
    ----------
    - STA_FC [`optional input grid`] : Field Capacity. [mm]
    - STA_PWP [`optional input grid`] : Permanent Wilting Point. [mm]
    - LANDUSE [`optional input grid`] : Land Use
    - DYN_W [`output grid`] : Soil Moisture
    - STA_FC_DEFAULT [`floating point number`] : Default. Minimum: 0.000000 Maximum: 100.000000 Default: 20.000000 default value if no grid has been selected
    - STA_PWP_DEFAULT [`floating point number`] : Default. Minimum: 0.000000 Maximum: 100.000000 Default: 2.000000 default value if no grid has been selected
    - LANDUSE_DEF [`integer number`] : Default. Default: -1
    - DYN_CLIMATE [`static table`] : Climate Data. 3 Fields: - 1. [8 byte floating point number] Precipitation [mm] - 2. [8 byte floating point number] Temperature [2pm Deg.C] - 3. [8 byte floating point number] Air Humidity [2pm [%] 
    - STA_KC [`static table`] : Crop Coefficients. 14 Fields: - 1. [signed 4 byte integer] Land Use ID - 2. [string] Name - 3. [8 byte floating point number] January - 4. [8 byte floating point number] February - 5. [8 byte floating point number] March - 6. [8 byte floating point number] April - 7. [8 byte floating point number] May - 8. [8 byte floating point number] June - 9. [8 byte floating point number] July - 10. [8 byte floating point number] August - 11. [8 byte floating point number] September - 12. [8 byte floating point number] October - 13. [8 byte floating point number] November - 14. [8 byte floating point number] December 

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('sim_hydrology', '0', 'Top Soil Water Content')
    if Tool.is_Okay():
        Tool.Set_Input ('STA_FC', STA_FC)
        Tool.Set_Input ('STA_PWP', STA_PWP)
        Tool.Set_Input ('LANDUSE', LANDUSE)
        Tool.Set_Output('DYN_W', DYN_W)
        Tool.Set_Option('STA_FC_DEFAULT', STA_FC_DEFAULT)
        Tool.Set_Option('STA_PWP_DEFAULT', STA_PWP_DEFAULT)
        Tool.Set_Option('LANDUSE_DEF', LANDUSE_DEF)
        Tool.Set_Option('DYN_CLIMATE', DYN_CLIMATE)
        Tool.Set_Option('STA_KC', STA_KC)
        return Tool.Execute(Verbose)
    return False

def TOPMODEL(ATANB=None, WEATHER=None, MOIST=None, TABLE=None, RECORD_P=None, RECORD_ET=None, RECORD_DATE=None, DTIME=None, NCLASSES=None, P_QS0=None, P_LNTE=None, P_MODEL=None, P_SR0=None, P_SRZMAX=None, P_SUZ_TD=None, P_VCH=None, P_VR=None, P_K0=None, P_PSI=None, P_DTHETA=None, BINF=None, Verbose=2):
    '''
    TOPMODEL
    ----------
    [sim_hydrology.2]\n
    Simple Subcatchment Version of TOPMODEL\n
    Based on the 'TOPMODEL demonstration program v95.02' by Keith Beven (Centre for Research on Environmental Systems and Statistics, Institute of Environmental and Biological Sciences, Lancaster University, Lancaster LA1 4YQ, UK) and the C translation of the Fortran source codes implemented in GRASS.\n
    This program allows single or multiple subcatchment calculations but with single average rainfall and potential evapotranspiration inputs to the whole catchment.  Subcatchment discharges are routed to the catchment outlet using a linear routing algorithm with constant main channel velocity and internal subcatchment routing velocity.  The program requires ln(a/tanB) distributions for each subcatchment.  These may be calculated using the GRIDATB program which requires raster elevation data as input. It is recommended that those data should be 50 m resolution or better.\n
    NOTE that TOPMODEL is not intended to be a traditional model package but is more a collection of concepts that can be used **** where appropriate ****. It is up to the user to verify that the assumptions are appropriate (see discussion in Beven et al.(1994).   This version of the model  will be best suited to catchments with shallow soils and moderate topography which do not suffer from excessively long dry periods.  Ideally predicted contributing areas should be checked against what actually happens in the catchment.\n
    It includes infiltration excess calculations and parameters based on the exponential conductivity Green-Ampt model of Beven (HSJ, 1984) but if infiltration excess does occur it does so over whole area of a subcatchment.  Spatial variability in conductivities can however be handled by specifying Ko parameter values for different subcatchments, even if they have the same ln(a/tanB) and routing parameters, ie. to represent different parts of the area.\n
    Note that time step calculations are explicit ie. SBAR at start of time step is used to determine contributing area. Thus with long (daily) time steps contributing area depends on initial value together with any volume filling effect of daily inputs.  Also baseflow at start of time step is used to update SBAR at end of time step.\n
    Arguments
    ----------
    - ATANB [`input grid`] : Topographic Wetness Index
    - WEATHER [`input table`] : Weather Records
    - MOIST [`output grid`] : Soil Moisture Deficit
    - TABLE [`output table`] : Simulation Output
    - RECORD_P [`table field`] : Precipitation [m / dt]
    - RECORD_ET [`table field`] : Evapotranspiration [m / dt]
    - RECORD_DATE [`table field`] : Date/Time
    - DTIME [`floating point number`] : Time Step [h]. Default: 1.000000
    - NCLASSES [`integer number`] : Number of Classes. Minimum: 1 Default: 30
    - P_QS0 [`floating point number`] : Initial subsurface flow per unit area [m/h]. Default: 0.000033
    - P_LNTE [`floating point number`] : Areal average of ln(T0) = ln(Te) [ln(m²/h)]. Default: 5.000000
    - P_MODEL [`floating point number`] : Model parameter [m]. Default: 0.032000
    - P_SR0 [`floating point number`] : Initial root zone storage deficit [m]. Default: 0.002000
    - P_SRZMAX [`floating point number`] : Maximum root zone storage deficit [m]. Default: 0.050000
    - P_SUZ_TD [`floating point number`] : Unsaturated zone time delay per unit storage deficit [h]. Default: 50.000000
    - P_VCH [`floating point number`] : Main channel routing velocity [m/h]. Default: 3600.000000
    - P_VR [`floating point number`] : Internal subcatchment routing velocity [m/h]. Default: 3600.000000
    - P_K0 [`floating point number`] : Surface hydraulic conductivity [m/h]. Default: 1.000000
    - P_PSI [`floating point number`] : Wetting front suction [m]. Default: 0.020000
    - P_DTHETA [`floating point number`] : Water content change across the wetting front. Default: 0.100000
    - BINF [`boolean`] : Green-Ampt Infiltration. Default: 1

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('sim_hydrology', '2', 'TOPMODEL')
    if Tool.is_Okay():
        Tool.Set_Input ('ATANB', ATANB)
        Tool.Set_Input ('WEATHER', WEATHER)
        Tool.Set_Output('MOIST', MOIST)
        Tool.Set_Output('TABLE', TABLE)
        Tool.Set_Option('RECORD_P', RECORD_P)
        Tool.Set_Option('RECORD_ET', RECORD_ET)
        Tool.Set_Option('RECORD_DATE', RECORD_DATE)
        Tool.Set_Option('DTIME', DTIME)
        Tool.Set_Option('NCLASSES', NCLASSES)
        Tool.Set_Option('P_QS0', P_QS0)
        Tool.Set_Option('P_LNTE', P_LNTE)
        Tool.Set_Option('P_MODEL', P_MODEL)
        Tool.Set_Option('P_SR0', P_SR0)
        Tool.Set_Option('P_SRZMAX', P_SRZMAX)
        Tool.Set_Option('P_SUZ_TD', P_SUZ_TD)
        Tool.Set_Option('P_VCH', P_VCH)
        Tool.Set_Option('P_VR', P_VR)
        Tool.Set_Option('P_K0', P_K0)
        Tool.Set_Option('P_PSI', P_PSI)
        Tool.Set_Option('P_DTHETA', P_DTHETA)
        Tool.Set_Option('BINF', BINF)
        return Tool.Execute(Verbose)
    return False

def run_tool_sim_hydrology_2(ATANB=None, WEATHER=None, MOIST=None, TABLE=None, RECORD_P=None, RECORD_ET=None, RECORD_DATE=None, DTIME=None, NCLASSES=None, P_QS0=None, P_LNTE=None, P_MODEL=None, P_SR0=None, P_SRZMAX=None, P_SUZ_TD=None, P_VCH=None, P_VR=None, P_K0=None, P_PSI=None, P_DTHETA=None, BINF=None, Verbose=2):
    '''
    TOPMODEL
    ----------
    [sim_hydrology.2]\n
    Simple Subcatchment Version of TOPMODEL\n
    Based on the 'TOPMODEL demonstration program v95.02' by Keith Beven (Centre for Research on Environmental Systems and Statistics, Institute of Environmental and Biological Sciences, Lancaster University, Lancaster LA1 4YQ, UK) and the C translation of the Fortran source codes implemented in GRASS.\n
    This program allows single or multiple subcatchment calculations but with single average rainfall and potential evapotranspiration inputs to the whole catchment.  Subcatchment discharges are routed to the catchment outlet using a linear routing algorithm with constant main channel velocity and internal subcatchment routing velocity.  The program requires ln(a/tanB) distributions for each subcatchment.  These may be calculated using the GRIDATB program which requires raster elevation data as input. It is recommended that those data should be 50 m resolution or better.\n
    NOTE that TOPMODEL is not intended to be a traditional model package but is more a collection of concepts that can be used **** where appropriate ****. It is up to the user to verify that the assumptions are appropriate (see discussion in Beven et al.(1994).   This version of the model  will be best suited to catchments with shallow soils and moderate topography which do not suffer from excessively long dry periods.  Ideally predicted contributing areas should be checked against what actually happens in the catchment.\n
    It includes infiltration excess calculations and parameters based on the exponential conductivity Green-Ampt model of Beven (HSJ, 1984) but if infiltration excess does occur it does so over whole area of a subcatchment.  Spatial variability in conductivities can however be handled by specifying Ko parameter values for different subcatchments, even if they have the same ln(a/tanB) and routing parameters, ie. to represent different parts of the area.\n
    Note that time step calculations are explicit ie. SBAR at start of time step is used to determine contributing area. Thus with long (daily) time steps contributing area depends on initial value together with any volume filling effect of daily inputs.  Also baseflow at start of time step is used to update SBAR at end of time step.\n
    Arguments
    ----------
    - ATANB [`input grid`] : Topographic Wetness Index
    - WEATHER [`input table`] : Weather Records
    - MOIST [`output grid`] : Soil Moisture Deficit
    - TABLE [`output table`] : Simulation Output
    - RECORD_P [`table field`] : Precipitation [m / dt]
    - RECORD_ET [`table field`] : Evapotranspiration [m / dt]
    - RECORD_DATE [`table field`] : Date/Time
    - DTIME [`floating point number`] : Time Step [h]. Default: 1.000000
    - NCLASSES [`integer number`] : Number of Classes. Minimum: 1 Default: 30
    - P_QS0 [`floating point number`] : Initial subsurface flow per unit area [m/h]. Default: 0.000033
    - P_LNTE [`floating point number`] : Areal average of ln(T0) = ln(Te) [ln(m²/h)]. Default: 5.000000
    - P_MODEL [`floating point number`] : Model parameter [m]. Default: 0.032000
    - P_SR0 [`floating point number`] : Initial root zone storage deficit [m]. Default: 0.002000
    - P_SRZMAX [`floating point number`] : Maximum root zone storage deficit [m]. Default: 0.050000
    - P_SUZ_TD [`floating point number`] : Unsaturated zone time delay per unit storage deficit [h]. Default: 50.000000
    - P_VCH [`floating point number`] : Main channel routing velocity [m/h]. Default: 3600.000000
    - P_VR [`floating point number`] : Internal subcatchment routing velocity [m/h]. Default: 3600.000000
    - P_K0 [`floating point number`] : Surface hydraulic conductivity [m/h]. Default: 1.000000
    - P_PSI [`floating point number`] : Wetting front suction [m]. Default: 0.020000
    - P_DTHETA [`floating point number`] : Water content change across the wetting front. Default: 0.100000
    - BINF [`boolean`] : Green-Ampt Infiltration. Default: 1

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('sim_hydrology', '2', 'TOPMODEL')
    if Tool.is_Okay():
        Tool.Set_Input ('ATANB', ATANB)
        Tool.Set_Input ('WEATHER', WEATHER)
        Tool.Set_Output('MOIST', MOIST)
        Tool.Set_Output('TABLE', TABLE)
        Tool.Set_Option('RECORD_P', RECORD_P)
        Tool.Set_Option('RECORD_ET', RECORD_ET)
        Tool.Set_Option('RECORD_DATE', RECORD_DATE)
        Tool.Set_Option('DTIME', DTIME)
        Tool.Set_Option('NCLASSES', NCLASSES)
        Tool.Set_Option('P_QS0', P_QS0)
        Tool.Set_Option('P_LNTE', P_LNTE)
        Tool.Set_Option('P_MODEL', P_MODEL)
        Tool.Set_Option('P_SR0', P_SR0)
        Tool.Set_Option('P_SRZMAX', P_SRZMAX)
        Tool.Set_Option('P_SUZ_TD', P_SUZ_TD)
        Tool.Set_Option('P_VCH', P_VCH)
        Tool.Set_Option('P_VR', P_VR)
        Tool.Set_Option('P_K0', P_K0)
        Tool.Set_Option('P_PSI', P_PSI)
        Tool.Set_Option('P_DTHETA', P_DTHETA)
        Tool.Set_Option('BINF', BINF)
        return Tool.Execute(Verbose)
    return False

def Water_Retention_Capacity(SHAPES=None, DEM=None, OUTPUT=None, RETENTION=None, INTERPOL=None, SLOPECORR=None, Verbose=2):
    '''
    Water Retention Capacity
    ----------
    [sim_hydrology.3]\n
    Water Retention Capacity. Plot hole input data must provide five attributes for each soil horizon in the following order and meaning:\n
    horizon depth, TF, L, Ar, Mo.\n
    Arguments
    ----------
    - SHAPES [`input shapes`] : Plot Holes
    - DEM [`input grid`] : Elevation
    - OUTPUT [`output shapes`] : Final Parameters
    - RETENTION [`output grid`] : Water Retention Capacity
    - INTERPOL [`choice`] : Interpolation. Available Choices: [0] Multilevel B-Spline Interpolation [1] Inverse Distance Weighted Default: 0
    - SLOPECORR [`boolean`] : Slope Correction. Default: 1

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('sim_hydrology', '3', 'Water Retention Capacity')
    if Tool.is_Okay():
        Tool.Set_Input ('SHAPES', SHAPES)
        Tool.Set_Input ('DEM', DEM)
        Tool.Set_Output('OUTPUT', OUTPUT)
        Tool.Set_Output('RETENTION', RETENTION)
        Tool.Set_Option('INTERPOL', INTERPOL)
        Tool.Set_Option('SLOPECORR', SLOPECORR)
        return Tool.Execute(Verbose)
    return False

def run_tool_sim_hydrology_3(SHAPES=None, DEM=None, OUTPUT=None, RETENTION=None, INTERPOL=None, SLOPECORR=None, Verbose=2):
    '''
    Water Retention Capacity
    ----------
    [sim_hydrology.3]\n
    Water Retention Capacity. Plot hole input data must provide five attributes for each soil horizon in the following order and meaning:\n
    horizon depth, TF, L, Ar, Mo.\n
    Arguments
    ----------
    - SHAPES [`input shapes`] : Plot Holes
    - DEM [`input grid`] : Elevation
    - OUTPUT [`output shapes`] : Final Parameters
    - RETENTION [`output grid`] : Water Retention Capacity
    - INTERPOL [`choice`] : Interpolation. Available Choices: [0] Multilevel B-Spline Interpolation [1] Inverse Distance Weighted Default: 0
    - SLOPECORR [`boolean`] : Slope Correction. Default: 1

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('sim_hydrology', '3', 'Water Retention Capacity')
    if Tool.is_Okay():
        Tool.Set_Input ('SHAPES', SHAPES)
        Tool.Set_Input ('DEM', DEM)
        Tool.Set_Output('OUTPUT', OUTPUT)
        Tool.Set_Output('RETENTION', RETENTION)
        Tool.Set_Option('INTERPOL', INTERPOL)
        Tool.Set_Option('SLOPECORR', SLOPECORR)
        return Tool.Execute(Verbose)
    return False

def Diffuse_Pollution_Risk(DEM=None, CHANNEL=None, WEIGHT=None, RAIN=None, DELIVERY=None, RISK_POINT=None, RISK_DIFFUSE=None, WEIGHT_DEFAULT=None, RAIN_DEFAULT=None, METHOD=None, CHANNEL_START=None, Verbose=2):
    '''
    Diffuse Pollution Risk
    ----------
    [sim_hydrology.4]\n
    Diffuse Pollution Risk Mapping.\n
    This tool tries to reproduce in parts the methodology of the SCIMAP - Diffuse Pollution Risk Mapping - Framework.\n
    Arguments
    ----------
    - DEM [`input grid`] : Elevation
    - CHANNEL [`optional input grid`] : Channel Network
    - WEIGHT [`optional input grid`] : Land Cover Weights
    - RAIN [`optional input grid`] : Rainfall
    - DELIVERY [`output grid`] : Delivery Index
    - RISK_POINT [`output grid`] : Locational Risk
    - RISK_DIFFUSE [`output grid`] : Diffuse Pollution Risk
    - WEIGHT_DEFAULT [`floating point number`] : Default. Minimum: 0.000000 Default: 1.000000 default value if no grid has been selected
    - RAIN_DEFAULT [`floating point number`] : Default. Minimum: 0.000000 Default: 500.000000 default value if no grid has been selected
    - METHOD [`choice`] : Flow Routing. Available Choices: [0] D8 [1] MFD Default: 1
    - CHANNEL_START [`integer number`] : Channel Initiation Threshold. Minimum: 1 Default: 150 minimum number of upslope contributing cells to start a channel

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('sim_hydrology', '4', 'Diffuse Pollution Risk')
    if Tool.is_Okay():
        Tool.Set_Input ('DEM', DEM)
        Tool.Set_Input ('CHANNEL', CHANNEL)
        Tool.Set_Input ('WEIGHT', WEIGHT)
        Tool.Set_Input ('RAIN', RAIN)
        Tool.Set_Output('DELIVERY', DELIVERY)
        Tool.Set_Output('RISK_POINT', RISK_POINT)
        Tool.Set_Output('RISK_DIFFUSE', RISK_DIFFUSE)
        Tool.Set_Option('WEIGHT_DEFAULT', WEIGHT_DEFAULT)
        Tool.Set_Option('RAIN_DEFAULT', RAIN_DEFAULT)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('CHANNEL_START', CHANNEL_START)
        return Tool.Execute(Verbose)
    return False

def run_tool_sim_hydrology_4(DEM=None, CHANNEL=None, WEIGHT=None, RAIN=None, DELIVERY=None, RISK_POINT=None, RISK_DIFFUSE=None, WEIGHT_DEFAULT=None, RAIN_DEFAULT=None, METHOD=None, CHANNEL_START=None, Verbose=2):
    '''
    Diffuse Pollution Risk
    ----------
    [sim_hydrology.4]\n
    Diffuse Pollution Risk Mapping.\n
    This tool tries to reproduce in parts the methodology of the SCIMAP - Diffuse Pollution Risk Mapping - Framework.\n
    Arguments
    ----------
    - DEM [`input grid`] : Elevation
    - CHANNEL [`optional input grid`] : Channel Network
    - WEIGHT [`optional input grid`] : Land Cover Weights
    - RAIN [`optional input grid`] : Rainfall
    - DELIVERY [`output grid`] : Delivery Index
    - RISK_POINT [`output grid`] : Locational Risk
    - RISK_DIFFUSE [`output grid`] : Diffuse Pollution Risk
    - WEIGHT_DEFAULT [`floating point number`] : Default. Minimum: 0.000000 Default: 1.000000 default value if no grid has been selected
    - RAIN_DEFAULT [`floating point number`] : Default. Minimum: 0.000000 Default: 500.000000 default value if no grid has been selected
    - METHOD [`choice`] : Flow Routing. Available Choices: [0] D8 [1] MFD Default: 1
    - CHANNEL_START [`integer number`] : Channel Initiation Threshold. Minimum: 1 Default: 150 minimum number of upslope contributing cells to start a channel

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('sim_hydrology', '4', 'Diffuse Pollution Risk')
    if Tool.is_Okay():
        Tool.Set_Input ('DEM', DEM)
        Tool.Set_Input ('CHANNEL', CHANNEL)
        Tool.Set_Input ('WEIGHT', WEIGHT)
        Tool.Set_Input ('RAIN', RAIN)
        Tool.Set_Output('DELIVERY', DELIVERY)
        Tool.Set_Output('RISK_POINT', RISK_POINT)
        Tool.Set_Output('RISK_DIFFUSE', RISK_DIFFUSE)
        Tool.Set_Option('WEIGHT_DEFAULT', WEIGHT_DEFAULT)
        Tool.Set_Option('RAIN_DEFAULT', RAIN_DEFAULT)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('CHANNEL_START', CHANNEL_START)
        return Tool.Execute(Verbose)
    return False

def Surface_and_Gradient(MASK=None, SURF=None, GRAD=None, SURF_E=None, Verbose=2):
    '''
    Surface and Gradient
    ----------
    [sim_hydrology.5]\n
    Cellular automata are simple computational operators, but despite their simplicity, they allow the simulation of highly complex processes. This tool has been created to apply the concept of cellular automata to simulate diffusion and flow processes in shallow water bodies with in- and outflow, where monitoring data show concentration growth or decrease between the inflow and the outflow points. Parameters are for example nutrients like nitrate, which is reduced by denitrification process inside the water body.\n
    Values of mask grid are expected to be 1 for water area, 2 for inlet, 3 for outlet and 0 for non water.\n
    Arguments
    ----------
    - MASK [`input grid`] : Mask
    - SURF [`output grid`] : Surface
    - GRAD [`output grid`] : Gradient
    - SURF_E [`floating point number`] : Surface Approximation Threshold. Minimum: 0.000000 Default: 0.001000

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('sim_hydrology', '5', 'Surface and Gradient')
    if Tool.is_Okay():
        Tool.Set_Input ('MASK', MASK)
        Tool.Set_Output('SURF', SURF)
        Tool.Set_Output('GRAD', GRAD)
        Tool.Set_Option('SURF_E', SURF_E)
        return Tool.Execute(Verbose)
    return False

def run_tool_sim_hydrology_5(MASK=None, SURF=None, GRAD=None, SURF_E=None, Verbose=2):
    '''
    Surface and Gradient
    ----------
    [sim_hydrology.5]\n
    Cellular automata are simple computational operators, but despite their simplicity, they allow the simulation of highly complex processes. This tool has been created to apply the concept of cellular automata to simulate diffusion and flow processes in shallow water bodies with in- and outflow, where monitoring data show concentration growth or decrease between the inflow and the outflow points. Parameters are for example nutrients like nitrate, which is reduced by denitrification process inside the water body.\n
    Values of mask grid are expected to be 1 for water area, 2 for inlet, 3 for outlet and 0 for non water.\n
    Arguments
    ----------
    - MASK [`input grid`] : Mask
    - SURF [`output grid`] : Surface
    - GRAD [`output grid`] : Gradient
    - SURF_E [`floating point number`] : Surface Approximation Threshold. Minimum: 0.000000 Default: 0.001000

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('sim_hydrology', '5', 'Surface and Gradient')
    if Tool.is_Okay():
        Tool.Set_Input ('MASK', MASK)
        Tool.Set_Output('SURF', SURF)
        Tool.Set_Output('GRAD', GRAD)
        Tool.Set_Option('SURF_E', SURF_E)
        return Tool.Execute(Verbose)
    return False

def Concentration(MASK=None, GRAD=None, CONC=None, CONC_IN=None, CONC_OUT=None, CONC_E=None, GRAD_MIN=None, NEIGHBOURS=None, Verbose=2):
    '''
    Concentration
    ----------
    [sim_hydrology.6]\n
    Cellular automata are simple computational operators, but despite their simplicity, they allow the simulation of highly complex processes. This tool has been created to apply the concept of cellular automata to simulate diffusion and flow processes in shallow water bodies with in- and outflow, where monitoring data show concentration growth or decrease between the inflow and the outflow points. Parameters are for example nutrients like nitrate, which is reduced by denitrification process inside the water body.\n
    Values of mask grid are expected to be 1 for water area, 2 for inlet, 3 for outlet and 0 for non water.\n
    Arguments
    ----------
    - MASK [`input grid`] : Mask
    - GRAD [`input grid`] : Gradient
    - CONC [`output grid`] : Concentration
    - CONC_IN [`floating point number`] : Inlet Concentration. Minimum: 0.000000 Default: 5.000000
    - CONC_OUT [`floating point number`] : Outlet Concentration. Minimum: 0.000000 Default: 3.000000
    - CONC_E [`floating point number`] : Concentration Approximation Threshold. Minimum: 0.000000 Default: 0.001000
    - GRAD_MIN [`floating point number`] : Minimum Gradient. Minimum: 0.000000 Default: 0.000000
    - NEIGHBOURS [`choice`] : Neighbourhood. Available Choices: [0] Moore (8) [1] Neumann (4) [2] Optimised Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('sim_hydrology', '6', 'Concentration')
    if Tool.is_Okay():
        Tool.Set_Input ('MASK', MASK)
        Tool.Set_Input ('GRAD', GRAD)
        Tool.Set_Output('CONC', CONC)
        Tool.Set_Option('CONC_IN', CONC_IN)
        Tool.Set_Option('CONC_OUT', CONC_OUT)
        Tool.Set_Option('CONC_E', CONC_E)
        Tool.Set_Option('GRAD_MIN', GRAD_MIN)
        Tool.Set_Option('NEIGHBOURS', NEIGHBOURS)
        return Tool.Execute(Verbose)
    return False

def run_tool_sim_hydrology_6(MASK=None, GRAD=None, CONC=None, CONC_IN=None, CONC_OUT=None, CONC_E=None, GRAD_MIN=None, NEIGHBOURS=None, Verbose=2):
    '''
    Concentration
    ----------
    [sim_hydrology.6]\n
    Cellular automata are simple computational operators, but despite their simplicity, they allow the simulation of highly complex processes. This tool has been created to apply the concept of cellular automata to simulate diffusion and flow processes in shallow water bodies with in- and outflow, where monitoring data show concentration growth or decrease between the inflow and the outflow points. Parameters are for example nutrients like nitrate, which is reduced by denitrification process inside the water body.\n
    Values of mask grid are expected to be 1 for water area, 2 for inlet, 3 for outlet and 0 for non water.\n
    Arguments
    ----------
    - MASK [`input grid`] : Mask
    - GRAD [`input grid`] : Gradient
    - CONC [`output grid`] : Concentration
    - CONC_IN [`floating point number`] : Inlet Concentration. Minimum: 0.000000 Default: 5.000000
    - CONC_OUT [`floating point number`] : Outlet Concentration. Minimum: 0.000000 Default: 3.000000
    - CONC_E [`floating point number`] : Concentration Approximation Threshold. Minimum: 0.000000 Default: 0.001000
    - GRAD_MIN [`floating point number`] : Minimum Gradient. Minimum: 0.000000 Default: 0.000000
    - NEIGHBOURS [`choice`] : Neighbourhood. Available Choices: [0] Moore (8) [1] Neumann (4) [2] Optimised Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('sim_hydrology', '6', 'Concentration')
    if Tool.is_Okay():
        Tool.Set_Input ('MASK', MASK)
        Tool.Set_Input ('GRAD', GRAD)
        Tool.Set_Output('CONC', CONC)
        Tool.Set_Option('CONC_IN', CONC_IN)
        Tool.Set_Option('CONC_OUT', CONC_OUT)
        Tool.Set_Option('CONC_E', CONC_E)
        Tool.Set_Option('GRAD_MIN', GRAD_MIN)
        Tool.Set_Option('NEIGHBOURS', NEIGHBOURS)
        return Tool.Execute(Verbose)
    return False

def Surface_Gradient_and_Concentration(MASK=None, SURF=None, GRAD=None, CONC=None, SURF_E=None, CONC_IN=None, CONC_OUT=None, CONC_E=None, GRAD_MIN=None, NEIGHBOURS=None, Verbose=2):
    '''
    Surface, Gradient and Concentration
    ----------
    [sim_hydrology.7]\n
    Cellular automata are simple computational operators, but despite their simplicity, they allow the simulation of highly complex processes. This tool has been created to apply the concept of cellular automata to simulate diffusion and flow processes in shallow water bodies with in- and outflow, where monitoring data show concentration growth or decrease between the inflow and the outflow points. Parameters are for example nutrients like nitrate, which is reduced by denitrification process inside the water body.\n
    Values of mask grid are expected to be 1 for water area, 2 for inlet, 3 for outlet and 0 for non water.\n
    Arguments
    ----------
    - MASK [`input grid`] : Mask
    - SURF [`output grid`] : Surface
    - GRAD [`output grid`] : Gradient
    - CONC [`output grid`] : Concentration
    - SURF_E [`floating point number`] : Surface Approximation Threshold. Minimum: 0.000000 Default: 0.001000
    - CONC_IN [`floating point number`] : Inlet Concentration. Minimum: 0.000000 Default: 5.000000
    - CONC_OUT [`floating point number`] : Outlet Concentration. Minimum: 0.000000 Default: 3.000000
    - CONC_E [`floating point number`] : Concentration Approximation Threshold. Minimum: 0.000000 Default: 0.001000
    - GRAD_MIN [`floating point number`] : Minimum Gradient. Minimum: 0.000000 Default: 0.000000
    - NEIGHBOURS [`choice`] : Neighbourhood. Available Choices: [0] Moore (8) [1] Neumann (4) [2] Optimised Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('sim_hydrology', '7', 'Surface, Gradient and Concentration')
    if Tool.is_Okay():
        Tool.Set_Input ('MASK', MASK)
        Tool.Set_Output('SURF', SURF)
        Tool.Set_Output('GRAD', GRAD)
        Tool.Set_Output('CONC', CONC)
        Tool.Set_Option('SURF_E', SURF_E)
        Tool.Set_Option('CONC_IN', CONC_IN)
        Tool.Set_Option('CONC_OUT', CONC_OUT)
        Tool.Set_Option('CONC_E', CONC_E)
        Tool.Set_Option('GRAD_MIN', GRAD_MIN)
        Tool.Set_Option('NEIGHBOURS', NEIGHBOURS)
        return Tool.Execute(Verbose)
    return False

def run_tool_sim_hydrology_7(MASK=None, SURF=None, GRAD=None, CONC=None, SURF_E=None, CONC_IN=None, CONC_OUT=None, CONC_E=None, GRAD_MIN=None, NEIGHBOURS=None, Verbose=2):
    '''
    Surface, Gradient and Concentration
    ----------
    [sim_hydrology.7]\n
    Cellular automata are simple computational operators, but despite their simplicity, they allow the simulation of highly complex processes. This tool has been created to apply the concept of cellular automata to simulate diffusion and flow processes in shallow water bodies with in- and outflow, where monitoring data show concentration growth or decrease between the inflow and the outflow points. Parameters are for example nutrients like nitrate, which is reduced by denitrification process inside the water body.\n
    Values of mask grid are expected to be 1 for water area, 2 for inlet, 3 for outlet and 0 for non water.\n
    Arguments
    ----------
    - MASK [`input grid`] : Mask
    - SURF [`output grid`] : Surface
    - GRAD [`output grid`] : Gradient
    - CONC [`output grid`] : Concentration
    - SURF_E [`floating point number`] : Surface Approximation Threshold. Minimum: 0.000000 Default: 0.001000
    - CONC_IN [`floating point number`] : Inlet Concentration. Minimum: 0.000000 Default: 5.000000
    - CONC_OUT [`floating point number`] : Outlet Concentration. Minimum: 0.000000 Default: 3.000000
    - CONC_E [`floating point number`] : Concentration Approximation Threshold. Minimum: 0.000000 Default: 0.001000
    - GRAD_MIN [`floating point number`] : Minimum Gradient. Minimum: 0.000000 Default: 0.000000
    - NEIGHBOURS [`choice`] : Neighbourhood. Available Choices: [0] Moore (8) [1] Neumann (4) [2] Optimised Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('sim_hydrology', '7', 'Surface, Gradient and Concentration')
    if Tool.is_Okay():
        Tool.Set_Input ('MASK', MASK)
        Tool.Set_Output('SURF', SURF)
        Tool.Set_Output('GRAD', GRAD)
        Tool.Set_Output('CONC', CONC)
        Tool.Set_Option('SURF_E', SURF_E)
        Tool.Set_Option('CONC_IN', CONC_IN)
        Tool.Set_Option('CONC_OUT', CONC_OUT)
        Tool.Set_Option('CONC_E', CONC_E)
        Tool.Set_Option('GRAD_MIN', GRAD_MIN)
        Tool.Set_Option('NEIGHBOURS', NEIGHBOURS)
        return Tool.Execute(Verbose)
    return False

def QuasiDynamic_Flow_Accumulation(DEM=None, FLOW_K=None, FLOW_ACC=None, FLOW=None, TIME_MEAN=None, TIME_CONC=None, VELOCITY=None, FLOW_K_DEFAULT=None, TIME=None, ROUTING=None, FLOW_DEPTH=None, FLOW_CONST=None, RESET=None, RAIN=None, Verbose=2):
    '''
    Quasi-Dynamic Flow Accumulation
    ----------
    [sim_hydrology.8]\n
    This tool estimates the flow distribution at a specified time after a rainfall event. It is based on a standard flow accumulation calculation (e.g. O'Callaghan & Mark 1984, Freeman 1991) that has an additional function to accumulate the flow travel time. Final flow accumulation becomes that part of the total flow through that corresponds to the portion of flow travel time that exceeds the targeted time. The travel time is estimated with the empirical Manning equation (e.g. Dingman 1994, Freeman et al. 1998, Manning 1891).\n
    Arguments
    ----------
    - DEM [`input grid`] : Elevation. [m]
    - FLOW_K [`optional input grid`] : Manning-Strickler Coefficient. Manning-Strickler coefficient for flow travel time estimation (reciprocal of Manning's Roughness Coefficient)
    - FLOW_ACC [`output grid`] : Flow Accumulation. [mm]
    - FLOW [`output grid`] : Flow Through
    - TIME_MEAN [`output grid`] : Flow Travel Time
    - TIME_CONC [`output grid`] : Flow Concentration
    - VELOCITY [`output grid`] : Flow Velocity
    - FLOW_K_DEFAULT [`floating point number`] : Default. Minimum: 0.000000 Default: 20.000000 default value if no grid has been selected
    - TIME [`floating point number`] : Time. Minimum: 0.001000 Default: 5.000000 minutes
    - ROUTING [`choice`] : Flow Routing. Available Choices: [0] D8 [1] MFD Default: 1
    - FLOW_DEPTH [`choice`] : Flow Depth. Available Choices: [0] constant [1] dynamic Default: 1
    - FLOW_CONST [`floating point number`] : Constant Flow Depth. Minimum: 0.000000 Default: 10.000000 [mm]
    - RESET [`boolean`] : Reset. Default: 1 Resets flow accumulation raster.
    - RAIN [`floating point number`] : Rain. Minimum: 0.000000 Default: 10.000000 The flow portion [mm] added to each raster cell before simulation starts.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('sim_hydrology', '8', 'Quasi-Dynamic Flow Accumulation')
    if Tool.is_Okay():
        Tool.Set_Input ('DEM', DEM)
        Tool.Set_Input ('FLOW_K', FLOW_K)
        Tool.Set_Output('FLOW_ACC', FLOW_ACC)
        Tool.Set_Output('FLOW', FLOW)
        Tool.Set_Output('TIME_MEAN', TIME_MEAN)
        Tool.Set_Output('TIME_CONC', TIME_CONC)
        Tool.Set_Output('VELOCITY', VELOCITY)
        Tool.Set_Option('FLOW_K_DEFAULT', FLOW_K_DEFAULT)
        Tool.Set_Option('TIME', TIME)
        Tool.Set_Option('ROUTING', ROUTING)
        Tool.Set_Option('FLOW_DEPTH', FLOW_DEPTH)
        Tool.Set_Option('FLOW_CONST', FLOW_CONST)
        Tool.Set_Option('RESET', RESET)
        Tool.Set_Option('RAIN', RAIN)
        return Tool.Execute(Verbose)
    return False

def run_tool_sim_hydrology_8(DEM=None, FLOW_K=None, FLOW_ACC=None, FLOW=None, TIME_MEAN=None, TIME_CONC=None, VELOCITY=None, FLOW_K_DEFAULT=None, TIME=None, ROUTING=None, FLOW_DEPTH=None, FLOW_CONST=None, RESET=None, RAIN=None, Verbose=2):
    '''
    Quasi-Dynamic Flow Accumulation
    ----------
    [sim_hydrology.8]\n
    This tool estimates the flow distribution at a specified time after a rainfall event. It is based on a standard flow accumulation calculation (e.g. O'Callaghan & Mark 1984, Freeman 1991) that has an additional function to accumulate the flow travel time. Final flow accumulation becomes that part of the total flow through that corresponds to the portion of flow travel time that exceeds the targeted time. The travel time is estimated with the empirical Manning equation (e.g. Dingman 1994, Freeman et al. 1998, Manning 1891).\n
    Arguments
    ----------
    - DEM [`input grid`] : Elevation. [m]
    - FLOW_K [`optional input grid`] : Manning-Strickler Coefficient. Manning-Strickler coefficient for flow travel time estimation (reciprocal of Manning's Roughness Coefficient)
    - FLOW_ACC [`output grid`] : Flow Accumulation. [mm]
    - FLOW [`output grid`] : Flow Through
    - TIME_MEAN [`output grid`] : Flow Travel Time
    - TIME_CONC [`output grid`] : Flow Concentration
    - VELOCITY [`output grid`] : Flow Velocity
    - FLOW_K_DEFAULT [`floating point number`] : Default. Minimum: 0.000000 Default: 20.000000 default value if no grid has been selected
    - TIME [`floating point number`] : Time. Minimum: 0.001000 Default: 5.000000 minutes
    - ROUTING [`choice`] : Flow Routing. Available Choices: [0] D8 [1] MFD Default: 1
    - FLOW_DEPTH [`choice`] : Flow Depth. Available Choices: [0] constant [1] dynamic Default: 1
    - FLOW_CONST [`floating point number`] : Constant Flow Depth. Minimum: 0.000000 Default: 10.000000 [mm]
    - RESET [`boolean`] : Reset. Default: 1 Resets flow accumulation raster.
    - RAIN [`floating point number`] : Rain. Minimum: 0.000000 Default: 10.000000 The flow portion [mm] added to each raster cell before simulation starts.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('sim_hydrology', '8', 'Quasi-Dynamic Flow Accumulation')
    if Tool.is_Okay():
        Tool.Set_Input ('DEM', DEM)
        Tool.Set_Input ('FLOW_K', FLOW_K)
        Tool.Set_Output('FLOW_ACC', FLOW_ACC)
        Tool.Set_Output('FLOW', FLOW)
        Tool.Set_Output('TIME_MEAN', TIME_MEAN)
        Tool.Set_Output('TIME_CONC', TIME_CONC)
        Tool.Set_Output('VELOCITY', VELOCITY)
        Tool.Set_Option('FLOW_K_DEFAULT', FLOW_K_DEFAULT)
        Tool.Set_Option('TIME', TIME)
        Tool.Set_Option('ROUTING', ROUTING)
        Tool.Set_Option('FLOW_DEPTH', FLOW_DEPTH)
        Tool.Set_Option('FLOW_CONST', FLOW_CONST)
        Tool.Set_Option('RESET', RESET)
        Tool.Set_Option('RAIN', RAIN)
        return Tool.Execute(Verbose)
    return False

def Overland_Flow(DEM=None, ROUGHNESS=None, INTER_MAX=None, POND_MAX=None, INFIL_MAX=None, FLOW=None, PRECIP=None, ET_POT=None, MONITOR_POINTS=None, INTERCEPT=None, PONDING=None, INFILTRAT=None, VELOCITY=None, SUMMARY=None, MONITOR_SERIES=None, ROUGHNESS_DEFAULT=None, STRICKLER=None, INTER_MAX_DEFAULT=None, POND_MAX_DEFAULT=None, INFIL_MAX_DEFAULT=None, WEATHER=None, PRECIP_DEFAULT=None, ET_POT_DEFAULT=None, RESET=None, TIME_STOP=None, TIME_STEP=None, FLOW_OUT=None, TIME_UPDATE=None, UPDATE_FLOW_FIXED=None, UPDATE_FLOW_RANGE=None, UPDATE_VELO_FIXED=None, UPDATE_VELO_RANGE=None, MONITOR_NAME=None, MONITOR_UPDATE=None, MONITOR_RESET=None, Verbose=2):
    '''
    Overland Flow
    ----------
    [sim_hydrology.9]\n
    A simple overland flow simulation.\n
    Arguments
    ----------
    - DEM [`input grid`] : Elevation
    - ROUGHNESS [`optional input grid`] : Roughness. Ks Strickler = 1/n Manning
    - INTER_MAX [`optional input grid`] : Interception Capacity [mm]
    - POND_MAX [`optional input grid`] : Ponding Capacity [mm]
    - INFIL_MAX [`optional input grid`] : Infiltration Capacity [mm/h]
    - FLOW [`optional input grid`] : Flow [mm]
    - PRECIP [`optional input grid`] : Precipitation [mm/h]
    - ET_POT [`optional input grid`] : Potential Evapotranspiration [mm/h]
    - MONITOR_POINTS [`optional input shapes`] : Monitors
    - INTERCEPT [`output grid`] : Interception [mm]
    - PONDING [`output grid`] : Ponding [mm]
    - INFILTRAT [`output grid`] : Infiltration [mm]
    - VELOCITY [`output grid`] : Velocity [m/h]
    - SUMMARY [`output table`] : Overland Flow Summary
    - MONITOR_SERIES [`output table`] : Time Series. Flow accumulation values collected for each monitor point.
    - ROUGHNESS_DEFAULT [`floating point number`] : Default. Minimum: 0.000000 Default: 20.000000 default value if no grid has been selected
    - STRICKLER [`choice`] : Type. Available Choices: [0] Strickler Ks, [m^1/3 / s] [1] Gauckler-Manning n, [s / m^1/3] Default: 0 Ks Strickler = 1/n Gauckler-Manning
    - INTER_MAX_DEFAULT [`floating point number`] : Default. Minimum: 0.000000 Default: 0.000000 default value if no grid has been selected
    - POND_MAX_DEFAULT [`floating point number`] : Default. Minimum: 0.000000 Default: 0.000000 default value if no grid has been selected
    - INFIL_MAX_DEFAULT [`floating point number`] : Default. Minimum: 0.000000 Default: 0.000000 default value if no grid has been selected
    - WEATHER [`grid system`] : Weather Grid System
    - PRECIP_DEFAULT [`floating point number`] : Default. Minimum: 0.000000 Default: 0.000000 default value if no grid has been selected
    - ET_POT_DEFAULT [`floating point number`] : Default. Minimum: 0.000000 Default: 0.000000 default value if no grid has been selected
    - RESET [`boolean`] : Reset. Default: 1 If checked storages (flow, ponding, interception) and sinks (infiltration) will be set to zero.
    - TIME_STOP [`floating point number`] : Simulation Time [h]. Minimum: 0.000000 Default: 6.000000 Simulation time in hours.
    - TIME_STEP [`floating point number`] : Time Step Adjustment. Minimum: 0.010000 Maximum: 1.000000 Default: 0.500000 Choosing a lower value will result in a better numerical precision but also in a longer calculation time.
    - FLOW_OUT [`boolean`] : Overland Flow Summary. Default: 0 Report the amount of overland flow that left the covered area.
    - TIME_UPDATE [`floating point number`] : Map Update Frequency. Minimum: 0.000000 Default: 1.000000 Map update frequency in minutes. Set to zero to update each simulation time step.
    - UPDATE_FLOW_FIXED [`boolean`] : Fixed Color Stretch for Flow. Default: 0
    - UPDATE_FLOW_RANGE [`value range`] : Fixed Color Stretch
    - UPDATE_VELO_FIXED [`boolean`] : Fixed Color Stretch for Velocity. Default: 1
    - UPDATE_VELO_RANGE [`value range`] : Fixed Color Stretch
    - MONITOR_NAME [`table field`] : Name. Used to name time series fields. Will simply be enumerated if not set.
    - MONITOR_UPDATE [`floating point number`] : Monitor Update Frequency. Minimum: 0.000000 Default: 1.000000 Monitor update frequency in minutes. Set to zero to update each simulation time step.
    - MONITOR_RESET [`boolean`] : Reset. Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('sim_hydrology', '9', 'Overland Flow')
    if Tool.is_Okay():
        Tool.Set_Input ('DEM', DEM)
        Tool.Set_Input ('ROUGHNESS', ROUGHNESS)
        Tool.Set_Input ('INTER_MAX', INTER_MAX)
        Tool.Set_Input ('POND_MAX', POND_MAX)
        Tool.Set_Input ('INFIL_MAX', INFIL_MAX)
        Tool.Set_Input ('FLOW', FLOW)
        Tool.Set_Input ('PRECIP', PRECIP)
        Tool.Set_Input ('ET_POT', ET_POT)
        Tool.Set_Input ('MONITOR_POINTS', MONITOR_POINTS)
        Tool.Set_Output('INTERCEPT', INTERCEPT)
        Tool.Set_Output('PONDING', PONDING)
        Tool.Set_Output('INFILTRAT', INFILTRAT)
        Tool.Set_Output('VELOCITY', VELOCITY)
        Tool.Set_Output('SUMMARY', SUMMARY)
        Tool.Set_Output('MONITOR_SERIES', MONITOR_SERIES)
        Tool.Set_Option('ROUGHNESS_DEFAULT', ROUGHNESS_DEFAULT)
        Tool.Set_Option('STRICKLER', STRICKLER)
        Tool.Set_Option('INTER_MAX_DEFAULT', INTER_MAX_DEFAULT)
        Tool.Set_Option('POND_MAX_DEFAULT', POND_MAX_DEFAULT)
        Tool.Set_Option('INFIL_MAX_DEFAULT', INFIL_MAX_DEFAULT)
        Tool.Set_Option('WEATHER', WEATHER)
        Tool.Set_Option('PRECIP_DEFAULT', PRECIP_DEFAULT)
        Tool.Set_Option('ET_POT_DEFAULT', ET_POT_DEFAULT)
        Tool.Set_Option('RESET', RESET)
        Tool.Set_Option('TIME_STOP', TIME_STOP)
        Tool.Set_Option('TIME_STEP', TIME_STEP)
        Tool.Set_Option('FLOW_OUT', FLOW_OUT)
        Tool.Set_Option('TIME_UPDATE', TIME_UPDATE)
        Tool.Set_Option('UPDATE_FLOW_FIXED', UPDATE_FLOW_FIXED)
        Tool.Set_Option('UPDATE_FLOW_RANGE', UPDATE_FLOW_RANGE)
        Tool.Set_Option('UPDATE_VELO_FIXED', UPDATE_VELO_FIXED)
        Tool.Set_Option('UPDATE_VELO_RANGE', UPDATE_VELO_RANGE)
        Tool.Set_Option('MONITOR_NAME', MONITOR_NAME)
        Tool.Set_Option('MONITOR_UPDATE', MONITOR_UPDATE)
        Tool.Set_Option('MONITOR_RESET', MONITOR_RESET)
        return Tool.Execute(Verbose)
    return False

def run_tool_sim_hydrology_9(DEM=None, ROUGHNESS=None, INTER_MAX=None, POND_MAX=None, INFIL_MAX=None, FLOW=None, PRECIP=None, ET_POT=None, MONITOR_POINTS=None, INTERCEPT=None, PONDING=None, INFILTRAT=None, VELOCITY=None, SUMMARY=None, MONITOR_SERIES=None, ROUGHNESS_DEFAULT=None, STRICKLER=None, INTER_MAX_DEFAULT=None, POND_MAX_DEFAULT=None, INFIL_MAX_DEFAULT=None, WEATHER=None, PRECIP_DEFAULT=None, ET_POT_DEFAULT=None, RESET=None, TIME_STOP=None, TIME_STEP=None, FLOW_OUT=None, TIME_UPDATE=None, UPDATE_FLOW_FIXED=None, UPDATE_FLOW_RANGE=None, UPDATE_VELO_FIXED=None, UPDATE_VELO_RANGE=None, MONITOR_NAME=None, MONITOR_UPDATE=None, MONITOR_RESET=None, Verbose=2):
    '''
    Overland Flow
    ----------
    [sim_hydrology.9]\n
    A simple overland flow simulation.\n
    Arguments
    ----------
    - DEM [`input grid`] : Elevation
    - ROUGHNESS [`optional input grid`] : Roughness. Ks Strickler = 1/n Manning
    - INTER_MAX [`optional input grid`] : Interception Capacity [mm]
    - POND_MAX [`optional input grid`] : Ponding Capacity [mm]
    - INFIL_MAX [`optional input grid`] : Infiltration Capacity [mm/h]
    - FLOW [`optional input grid`] : Flow [mm]
    - PRECIP [`optional input grid`] : Precipitation [mm/h]
    - ET_POT [`optional input grid`] : Potential Evapotranspiration [mm/h]
    - MONITOR_POINTS [`optional input shapes`] : Monitors
    - INTERCEPT [`output grid`] : Interception [mm]
    - PONDING [`output grid`] : Ponding [mm]
    - INFILTRAT [`output grid`] : Infiltration [mm]
    - VELOCITY [`output grid`] : Velocity [m/h]
    - SUMMARY [`output table`] : Overland Flow Summary
    - MONITOR_SERIES [`output table`] : Time Series. Flow accumulation values collected for each monitor point.
    - ROUGHNESS_DEFAULT [`floating point number`] : Default. Minimum: 0.000000 Default: 20.000000 default value if no grid has been selected
    - STRICKLER [`choice`] : Type. Available Choices: [0] Strickler Ks, [m^1/3 / s] [1] Gauckler-Manning n, [s / m^1/3] Default: 0 Ks Strickler = 1/n Gauckler-Manning
    - INTER_MAX_DEFAULT [`floating point number`] : Default. Minimum: 0.000000 Default: 0.000000 default value if no grid has been selected
    - POND_MAX_DEFAULT [`floating point number`] : Default. Minimum: 0.000000 Default: 0.000000 default value if no grid has been selected
    - INFIL_MAX_DEFAULT [`floating point number`] : Default. Minimum: 0.000000 Default: 0.000000 default value if no grid has been selected
    - WEATHER [`grid system`] : Weather Grid System
    - PRECIP_DEFAULT [`floating point number`] : Default. Minimum: 0.000000 Default: 0.000000 default value if no grid has been selected
    - ET_POT_DEFAULT [`floating point number`] : Default. Minimum: 0.000000 Default: 0.000000 default value if no grid has been selected
    - RESET [`boolean`] : Reset. Default: 1 If checked storages (flow, ponding, interception) and sinks (infiltration) will be set to zero.
    - TIME_STOP [`floating point number`] : Simulation Time [h]. Minimum: 0.000000 Default: 6.000000 Simulation time in hours.
    - TIME_STEP [`floating point number`] : Time Step Adjustment. Minimum: 0.010000 Maximum: 1.000000 Default: 0.500000 Choosing a lower value will result in a better numerical precision but also in a longer calculation time.
    - FLOW_OUT [`boolean`] : Overland Flow Summary. Default: 0 Report the amount of overland flow that left the covered area.
    - TIME_UPDATE [`floating point number`] : Map Update Frequency. Minimum: 0.000000 Default: 1.000000 Map update frequency in minutes. Set to zero to update each simulation time step.
    - UPDATE_FLOW_FIXED [`boolean`] : Fixed Color Stretch for Flow. Default: 0
    - UPDATE_FLOW_RANGE [`value range`] : Fixed Color Stretch
    - UPDATE_VELO_FIXED [`boolean`] : Fixed Color Stretch for Velocity. Default: 1
    - UPDATE_VELO_RANGE [`value range`] : Fixed Color Stretch
    - MONITOR_NAME [`table field`] : Name. Used to name time series fields. Will simply be enumerated if not set.
    - MONITOR_UPDATE [`floating point number`] : Monitor Update Frequency. Minimum: 0.000000 Default: 1.000000 Monitor update frequency in minutes. Set to zero to update each simulation time step.
    - MONITOR_RESET [`boolean`] : Reset. Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('sim_hydrology', '9', 'Overland Flow')
    if Tool.is_Okay():
        Tool.Set_Input ('DEM', DEM)
        Tool.Set_Input ('ROUGHNESS', ROUGHNESS)
        Tool.Set_Input ('INTER_MAX', INTER_MAX)
        Tool.Set_Input ('POND_MAX', POND_MAX)
        Tool.Set_Input ('INFIL_MAX', INFIL_MAX)
        Tool.Set_Input ('FLOW', FLOW)
        Tool.Set_Input ('PRECIP', PRECIP)
        Tool.Set_Input ('ET_POT', ET_POT)
        Tool.Set_Input ('MONITOR_POINTS', MONITOR_POINTS)
        Tool.Set_Output('INTERCEPT', INTERCEPT)
        Tool.Set_Output('PONDING', PONDING)
        Tool.Set_Output('INFILTRAT', INFILTRAT)
        Tool.Set_Output('VELOCITY', VELOCITY)
        Tool.Set_Output('SUMMARY', SUMMARY)
        Tool.Set_Output('MONITOR_SERIES', MONITOR_SERIES)
        Tool.Set_Option('ROUGHNESS_DEFAULT', ROUGHNESS_DEFAULT)
        Tool.Set_Option('STRICKLER', STRICKLER)
        Tool.Set_Option('INTER_MAX_DEFAULT', INTER_MAX_DEFAULT)
        Tool.Set_Option('POND_MAX_DEFAULT', POND_MAX_DEFAULT)
        Tool.Set_Option('INFIL_MAX_DEFAULT', INFIL_MAX_DEFAULT)
        Tool.Set_Option('WEATHER', WEATHER)
        Tool.Set_Option('PRECIP_DEFAULT', PRECIP_DEFAULT)
        Tool.Set_Option('ET_POT_DEFAULT', ET_POT_DEFAULT)
        Tool.Set_Option('RESET', RESET)
        Tool.Set_Option('TIME_STOP', TIME_STOP)
        Tool.Set_Option('TIME_STEP', TIME_STEP)
        Tool.Set_Option('FLOW_OUT', FLOW_OUT)
        Tool.Set_Option('TIME_UPDATE', TIME_UPDATE)
        Tool.Set_Option('UPDATE_FLOW_FIXED', UPDATE_FLOW_FIXED)
        Tool.Set_Option('UPDATE_FLOW_RANGE', UPDATE_FLOW_RANGE)
        Tool.Set_Option('UPDATE_VELO_FIXED', UPDATE_VELO_FIXED)
        Tool.Set_Option('UPDATE_VELO_RANGE', UPDATE_VELO_RANGE)
        Tool.Set_Option('MONITOR_NAME', MONITOR_NAME)
        Tool.Set_Option('MONITOR_UPDATE', MONITOR_UPDATE)
        Tool.Set_Option('MONITOR_RESET', MONITOR_RESET)
        return Tool.Execute(Verbose)
    return False

def Soil_Water_Simulation_after_Glugla_Table(INPUT=None, SIMULATION=None, INPUT_DAY=None, INPUT_P=None, INPUT_ETP=None, INPUT_LAI=None, INPUT_LAI_DEFAULT=None, I_MAX=None, LAI_MIN=None, LAI_MAX=None, LITTER_MAX=None, LITTER_CF=None, LITTER_0=None, GLUGLA=None, DO_ROOTING=None, OUTPUT_UNIT=None, SOIL_LAYERS=None, Verbose=2):
    '''
    Soil Water Simulation after Glugla (Table)
    ----------
    [sim_hydrology.10]\n
    A simple model for daily soil water simulation based on the approach of Glugla (1969).\n
    This is a re-implementation of the formulas used in the 'Simpel' model developed by Hörmann (1998), "...covering the lowest, serious end of hydrologic computing".\n
    Arguments
    ----------
    - INPUT [`input table`] : Input
    - SIMULATION [`output table`] : Simulation
    - INPUT_DAY [`table field`] : Day Identifier. Day specifier, e.g. a date field.
    - INPUT_P [`table field`] : Precipitation. [mm]
    - INPUT_ETP [`table field`] : Potential Evapotranspiration. [mm]
    - INPUT_LAI [`table field`] : Leaf Area Index
    - INPUT_LAI_DEFAULT [`floating point number`] : Default. Default: 0.000000 default value if no attribute has been selected
    - I_MAX [`floating point number`] : Interception Capacity. Minimum: 0.000000 Default: 2.000000 Leaf interception capacity [mm] at maximum LAI.
    - LAI_MIN [`floating point number`] : Minimum LAI. Minimum: 0.000000 Default: 0.100000 Minimum leaf area index.
    - LAI_MAX [`floating point number`] : Maximum LAI. Minimum: 0.000000 Default: 4.000000 Maximum leaf area index.
    - LITTER_MAX [`floating point number`] : Litter Capacity. Minimum: 0.000000 Default: 0.000000 [mm]
    - LITTER_CF [`floating point number`] : Litter Drying Factor. Minimum: 0.000000 Default: 3.000000 Curvature factor determining the drying of the litter storage.
    - LITTER_0 [`floating point number`] : Initial Litter Water Content. Minimum: 0.000000 Default: 0.000000 [mm]
    - GLUGLA [`floating point number`] : Glugla Coefficient. Minimum: 0.000000 Default: 150.000000 Empirical parameter [mm/day], depends on soil texture and structure.
    - DO_ROOTING [`boolean`] : Rooting. Default: 0
    - OUTPUT_UNIT [`choice`] : Output Unit. Available Choices: [0] mm [1] vol.% [2] percent of field capacity Default: 0
    - SOIL_LAYERS [`static table`] : Soil Layers. 6 Fields: - 1. [8 byte floating point number] Size - 2. [8 byte floating point number] FC - 3. [8 byte floating point number] PWP - 4. [8 byte floating point number] ETmax - 5. [8 byte floating point number] Rooting - 6. [8 byte floating point number] Water_0  Provide a row for each soil layer: depth [cm], field capacity [vol.%], permanent wilting point [vol.%], maximum ET [vol.%/day], rooting [% of total], initial water content [vol.%].

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('sim_hydrology', '10', 'Soil Water Simulation after Glugla (Table)')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Output('SIMULATION', SIMULATION)
        Tool.Set_Option('INPUT_DAY', INPUT_DAY)
        Tool.Set_Option('INPUT_P', INPUT_P)
        Tool.Set_Option('INPUT_ETP', INPUT_ETP)
        Tool.Set_Option('INPUT_LAI', INPUT_LAI)
        Tool.Set_Option('INPUT_LAI_DEFAULT', INPUT_LAI_DEFAULT)
        Tool.Set_Option('I_MAX', I_MAX)
        Tool.Set_Option('LAI_MIN', LAI_MIN)
        Tool.Set_Option('LAI_MAX', LAI_MAX)
        Tool.Set_Option('LITTER_MAX', LITTER_MAX)
        Tool.Set_Option('LITTER_CF', LITTER_CF)
        Tool.Set_Option('LITTER_0', LITTER_0)
        Tool.Set_Option('GLUGLA', GLUGLA)
        Tool.Set_Option('DO_ROOTING', DO_ROOTING)
        Tool.Set_Option('OUTPUT_UNIT', OUTPUT_UNIT)
        Tool.Set_Option('SOIL_LAYERS', SOIL_LAYERS)
        return Tool.Execute(Verbose)
    return False

def run_tool_sim_hydrology_10(INPUT=None, SIMULATION=None, INPUT_DAY=None, INPUT_P=None, INPUT_ETP=None, INPUT_LAI=None, INPUT_LAI_DEFAULT=None, I_MAX=None, LAI_MIN=None, LAI_MAX=None, LITTER_MAX=None, LITTER_CF=None, LITTER_0=None, GLUGLA=None, DO_ROOTING=None, OUTPUT_UNIT=None, SOIL_LAYERS=None, Verbose=2):
    '''
    Soil Water Simulation after Glugla (Table)
    ----------
    [sim_hydrology.10]\n
    A simple model for daily soil water simulation based on the approach of Glugla (1969).\n
    This is a re-implementation of the formulas used in the 'Simpel' model developed by Hörmann (1998), "...covering the lowest, serious end of hydrologic computing".\n
    Arguments
    ----------
    - INPUT [`input table`] : Input
    - SIMULATION [`output table`] : Simulation
    - INPUT_DAY [`table field`] : Day Identifier. Day specifier, e.g. a date field.
    - INPUT_P [`table field`] : Precipitation. [mm]
    - INPUT_ETP [`table field`] : Potential Evapotranspiration. [mm]
    - INPUT_LAI [`table field`] : Leaf Area Index
    - INPUT_LAI_DEFAULT [`floating point number`] : Default. Default: 0.000000 default value if no attribute has been selected
    - I_MAX [`floating point number`] : Interception Capacity. Minimum: 0.000000 Default: 2.000000 Leaf interception capacity [mm] at maximum LAI.
    - LAI_MIN [`floating point number`] : Minimum LAI. Minimum: 0.000000 Default: 0.100000 Minimum leaf area index.
    - LAI_MAX [`floating point number`] : Maximum LAI. Minimum: 0.000000 Default: 4.000000 Maximum leaf area index.
    - LITTER_MAX [`floating point number`] : Litter Capacity. Minimum: 0.000000 Default: 0.000000 [mm]
    - LITTER_CF [`floating point number`] : Litter Drying Factor. Minimum: 0.000000 Default: 3.000000 Curvature factor determining the drying of the litter storage.
    - LITTER_0 [`floating point number`] : Initial Litter Water Content. Minimum: 0.000000 Default: 0.000000 [mm]
    - GLUGLA [`floating point number`] : Glugla Coefficient. Minimum: 0.000000 Default: 150.000000 Empirical parameter [mm/day], depends on soil texture and structure.
    - DO_ROOTING [`boolean`] : Rooting. Default: 0
    - OUTPUT_UNIT [`choice`] : Output Unit. Available Choices: [0] mm [1] vol.% [2] percent of field capacity Default: 0
    - SOIL_LAYERS [`static table`] : Soil Layers. 6 Fields: - 1. [8 byte floating point number] Size - 2. [8 byte floating point number] FC - 3. [8 byte floating point number] PWP - 4. [8 byte floating point number] ETmax - 5. [8 byte floating point number] Rooting - 6. [8 byte floating point number] Water_0  Provide a row for each soil layer: depth [cm], field capacity [vol.%], permanent wilting point [vol.%], maximum ET [vol.%/day], rooting [% of total], initial water content [vol.%].

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('sim_hydrology', '10', 'Soil Water Simulation after Glugla (Table)')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Output('SIMULATION', SIMULATION)
        Tool.Set_Option('INPUT_DAY', INPUT_DAY)
        Tool.Set_Option('INPUT_P', INPUT_P)
        Tool.Set_Option('INPUT_ETP', INPUT_ETP)
        Tool.Set_Option('INPUT_LAI', INPUT_LAI)
        Tool.Set_Option('INPUT_LAI_DEFAULT', INPUT_LAI_DEFAULT)
        Tool.Set_Option('I_MAX', I_MAX)
        Tool.Set_Option('LAI_MIN', LAI_MIN)
        Tool.Set_Option('LAI_MAX', LAI_MAX)
        Tool.Set_Option('LITTER_MAX', LITTER_MAX)
        Tool.Set_Option('LITTER_CF', LITTER_CF)
        Tool.Set_Option('LITTER_0', LITTER_0)
        Tool.Set_Option('GLUGLA', GLUGLA)
        Tool.Set_Option('DO_ROOTING', DO_ROOTING)
        Tool.Set_Option('OUTPUT_UNIT', OUTPUT_UNIT)
        Tool.Set_Option('SOIL_LAYERS', SOIL_LAYERS)
        return Tool.Execute(Verbose)
    return False

def Soil_Water_Simulation_after_Glugla_Grid(P=None, ETP=None, FC=None, PWP=None, ETMAX=None, LAI=None, GLUGLA=None, SOIL_WATER=None, LITTER=None, RECHARGE=None, RECHARGE_SUM=None, LAI_GRIDSYSTEM=None, LAI_DEFAULT=None, GLUGLA_GRIDSYSTEM=None, GLUGLA_DEFAULT=None, I_MAX=None, LAI_MIN=None, LAI_MAX=None, LITTER_MAX=None, LITTER_CF=None, LITTER_0=None, RESET=None, DO_ROOTING=None, OUTPUT_UNIT=None, SOIL_LAYERS=None, Verbose=2):
    '''
    Soil Water Simulation after Glugla (Grid)
    ----------
    [sim_hydrology.11]\n
    A simple model for daily soil water simulation based on the approach of Glugla (1969).\n
    This is a re-implementation of the formulas used in the 'Simpel' model developed by HÃ¶rmann (1998), "...covering the lowest, serious end of hydrologic computing".\n
    Arguments
    ----------
    - P [`input grid list`] : Precipitation. [mm]
    - ETP [`input grid list`] : Potential Evapotranspiration. [mm]
    - FC [`optional input grid list`] : Field Capacity. [vol.%]
    - PWP [`optional input grid list`] : Permanent Wilting Point. [vol.%]
    - ETMAX [`optional input grid list`] : Maximum Evapotranspiration. [vol.%/day]
    - LAI [`optional input grid`] : Leaf Area Index
    - GLUGLA [`optional input grid`] : Glugla Coefficient. Empirical parameter [mm/day], depends on soil texture and structure.
    - SOIL_WATER [`output grid list`] : Soil Water Content
    - LITTER [`output grid`] : Litter Water Content. [mm]
    - RECHARGE [`output grid`] : Recharge. [mm]
    - RECHARGE_SUM [`output grid`] : Recharge Sum. [mm]
    - LAI_GRIDSYSTEM [`grid system`] : Grid system
    - LAI_DEFAULT [`floating point number`] : Default. Minimum: 0.000000 Default: 0.000000 default value if no grid has been selected
    - GLUGLA_GRIDSYSTEM [`grid system`] : Grid system
    - GLUGLA_DEFAULT [`floating point number`] : Default. Minimum: 0.000000 Default: 150.000000 default value if no grid has been selected
    - I_MAX [`floating point number`] : Interception Capacity. Minimum: 0.000000 Default: 0.000000 Leaf interception capacity [mm] at maximum LAI.
    - LAI_MIN [`floating point number`] : Minimum LAI. Minimum: 0.000000 Default: 0.100000 Minimum leaf area index.
    - LAI_MAX [`floating point number`] : Maximum LAI. Minimum: 0.000000 Default: 4.000000 Maximum leaf area index.
    - LITTER_MAX [`floating point number`] : Litter Capacity. Minimum: 0.000000 Default: 0.000000 [mm]
    - LITTER_CF [`floating point number`] : Litter Drying Factor. Minimum: 0.000000 Default: 3.000000 Curvature factor determining the drying of the litter storage.
    - LITTER_0 [`floating point number`] : Initial Litter Water Content. Minimum: 0.000000 Default: 0.000000 [mm]
    - RESET [`boolean`] : Reset. Default: 1
    - DO_ROOTING [`boolean`] : Rooting. Default: 0
    - OUTPUT_UNIT [`choice`] : Output Unit. Available Choices: [0] mm [1] vol.% [2] percent of field capacity Default: 0
    - SOIL_LAYERS [`static table`] : Soil Layers. 6 Fields: - 1. [8 byte floating point number] Size - 2. [8 byte floating point number] FC - 3. [8 byte floating point number] PWP - 4. [8 byte floating point number] ETmax - 5. [8 byte floating point number] Rooting - 6. [8 byte floating point number] Water_0  Provide a row for each soil layer: size [cm], field capacity [vol.%], permanent wilting point [vol.%], maximum ET [vol.%/day], rooting [% of total], initial water content [vol.%].

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('sim_hydrology', '11', 'Soil Water Simulation after Glugla (Grid)')
    if Tool.is_Okay():
        Tool.Set_Input ('P', P)
        Tool.Set_Input ('ETP', ETP)
        Tool.Set_Input ('FC', FC)
        Tool.Set_Input ('PWP', PWP)
        Tool.Set_Input ('ETMAX', ETMAX)
        Tool.Set_Input ('LAI', LAI)
        Tool.Set_Input ('GLUGLA', GLUGLA)
        Tool.Set_Output('SOIL_WATER', SOIL_WATER)
        Tool.Set_Output('LITTER', LITTER)
        Tool.Set_Output('RECHARGE', RECHARGE)
        Tool.Set_Output('RECHARGE_SUM', RECHARGE_SUM)
        Tool.Set_Option('LAI_GRIDSYSTEM', LAI_GRIDSYSTEM)
        Tool.Set_Option('LAI_DEFAULT', LAI_DEFAULT)
        Tool.Set_Option('GLUGLA_GRIDSYSTEM', GLUGLA_GRIDSYSTEM)
        Tool.Set_Option('GLUGLA_DEFAULT', GLUGLA_DEFAULT)
        Tool.Set_Option('I_MAX', I_MAX)
        Tool.Set_Option('LAI_MIN', LAI_MIN)
        Tool.Set_Option('LAI_MAX', LAI_MAX)
        Tool.Set_Option('LITTER_MAX', LITTER_MAX)
        Tool.Set_Option('LITTER_CF', LITTER_CF)
        Tool.Set_Option('LITTER_0', LITTER_0)
        Tool.Set_Option('RESET', RESET)
        Tool.Set_Option('DO_ROOTING', DO_ROOTING)
        Tool.Set_Option('OUTPUT_UNIT', OUTPUT_UNIT)
        Tool.Set_Option('SOIL_LAYERS', SOIL_LAYERS)
        return Tool.Execute(Verbose)
    return False

def run_tool_sim_hydrology_11(P=None, ETP=None, FC=None, PWP=None, ETMAX=None, LAI=None, GLUGLA=None, SOIL_WATER=None, LITTER=None, RECHARGE=None, RECHARGE_SUM=None, LAI_GRIDSYSTEM=None, LAI_DEFAULT=None, GLUGLA_GRIDSYSTEM=None, GLUGLA_DEFAULT=None, I_MAX=None, LAI_MIN=None, LAI_MAX=None, LITTER_MAX=None, LITTER_CF=None, LITTER_0=None, RESET=None, DO_ROOTING=None, OUTPUT_UNIT=None, SOIL_LAYERS=None, Verbose=2):
    '''
    Soil Water Simulation after Glugla (Grid)
    ----------
    [sim_hydrology.11]\n
    A simple model for daily soil water simulation based on the approach of Glugla (1969).\n
    This is a re-implementation of the formulas used in the 'Simpel' model developed by HÃ¶rmann (1998), "...covering the lowest, serious end of hydrologic computing".\n
    Arguments
    ----------
    - P [`input grid list`] : Precipitation. [mm]
    - ETP [`input grid list`] : Potential Evapotranspiration. [mm]
    - FC [`optional input grid list`] : Field Capacity. [vol.%]
    - PWP [`optional input grid list`] : Permanent Wilting Point. [vol.%]
    - ETMAX [`optional input grid list`] : Maximum Evapotranspiration. [vol.%/day]
    - LAI [`optional input grid`] : Leaf Area Index
    - GLUGLA [`optional input grid`] : Glugla Coefficient. Empirical parameter [mm/day], depends on soil texture and structure.
    - SOIL_WATER [`output grid list`] : Soil Water Content
    - LITTER [`output grid`] : Litter Water Content. [mm]
    - RECHARGE [`output grid`] : Recharge. [mm]
    - RECHARGE_SUM [`output grid`] : Recharge Sum. [mm]
    - LAI_GRIDSYSTEM [`grid system`] : Grid system
    - LAI_DEFAULT [`floating point number`] : Default. Minimum: 0.000000 Default: 0.000000 default value if no grid has been selected
    - GLUGLA_GRIDSYSTEM [`grid system`] : Grid system
    - GLUGLA_DEFAULT [`floating point number`] : Default. Minimum: 0.000000 Default: 150.000000 default value if no grid has been selected
    - I_MAX [`floating point number`] : Interception Capacity. Minimum: 0.000000 Default: 0.000000 Leaf interception capacity [mm] at maximum LAI.
    - LAI_MIN [`floating point number`] : Minimum LAI. Minimum: 0.000000 Default: 0.100000 Minimum leaf area index.
    - LAI_MAX [`floating point number`] : Maximum LAI. Minimum: 0.000000 Default: 4.000000 Maximum leaf area index.
    - LITTER_MAX [`floating point number`] : Litter Capacity. Minimum: 0.000000 Default: 0.000000 [mm]
    - LITTER_CF [`floating point number`] : Litter Drying Factor. Minimum: 0.000000 Default: 3.000000 Curvature factor determining the drying of the litter storage.
    - LITTER_0 [`floating point number`] : Initial Litter Water Content. Minimum: 0.000000 Default: 0.000000 [mm]
    - RESET [`boolean`] : Reset. Default: 1
    - DO_ROOTING [`boolean`] : Rooting. Default: 0
    - OUTPUT_UNIT [`choice`] : Output Unit. Available Choices: [0] mm [1] vol.% [2] percent of field capacity Default: 0
    - SOIL_LAYERS [`static table`] : Soil Layers. 6 Fields: - 1. [8 byte floating point number] Size - 2. [8 byte floating point number] FC - 3. [8 byte floating point number] PWP - 4. [8 byte floating point number] ETmax - 5. [8 byte floating point number] Rooting - 6. [8 byte floating point number] Water_0  Provide a row for each soil layer: size [cm], field capacity [vol.%], permanent wilting point [vol.%], maximum ET [vol.%/day], rooting [% of total], initial water content [vol.%].

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('sim_hydrology', '11', 'Soil Water Simulation after Glugla (Grid)')
    if Tool.is_Okay():
        Tool.Set_Input ('P', P)
        Tool.Set_Input ('ETP', ETP)
        Tool.Set_Input ('FC', FC)
        Tool.Set_Input ('PWP', PWP)
        Tool.Set_Input ('ETMAX', ETMAX)
        Tool.Set_Input ('LAI', LAI)
        Tool.Set_Input ('GLUGLA', GLUGLA)
        Tool.Set_Output('SOIL_WATER', SOIL_WATER)
        Tool.Set_Output('LITTER', LITTER)
        Tool.Set_Output('RECHARGE', RECHARGE)
        Tool.Set_Output('RECHARGE_SUM', RECHARGE_SUM)
        Tool.Set_Option('LAI_GRIDSYSTEM', LAI_GRIDSYSTEM)
        Tool.Set_Option('LAI_DEFAULT', LAI_DEFAULT)
        Tool.Set_Option('GLUGLA_GRIDSYSTEM', GLUGLA_GRIDSYSTEM)
        Tool.Set_Option('GLUGLA_DEFAULT', GLUGLA_DEFAULT)
        Tool.Set_Option('I_MAX', I_MAX)
        Tool.Set_Option('LAI_MIN', LAI_MIN)
        Tool.Set_Option('LAI_MAX', LAI_MAX)
        Tool.Set_Option('LITTER_MAX', LITTER_MAX)
        Tool.Set_Option('LITTER_CF', LITTER_CF)
        Tool.Set_Option('LITTER_0', LITTER_0)
        Tool.Set_Option('RESET', RESET)
        Tool.Set_Option('DO_ROOTING', DO_ROOTING)
        Tool.Set_Option('OUTPUT_UNIT', OUTPUT_UNIT)
        Tool.Set_Option('SOIL_LAYERS', SOIL_LAYERS)
        return Tool.Execute(Verbose)
    return False

def Glugla_Coefficient(SAND=None, SILT=None, CLAY=None, AIR=None, GLUGLA=None, Verbose=2):
    '''
    Glugla Coefficient
    ----------
    [sim_hydrology.12]\n
    Derivation of Glugla coefficient and, optionally, air capacitiy from soil texture data using a simple pedotransfer function. If one grain size fraction input is not provided its content is estimated from the contents of the other two fractions.\n
    Arguments
    ----------
    - SAND [`optional input grid`] : Sand. Percent
    - SILT [`optional input grid`] : Silt. Percent
    - CLAY [`optional input grid`] : Clay. Percent
    - AIR [`output grid`] : Air Capacity
    - GLUGLA [`output grid`] : Glugla Coefficient

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('sim_hydrology', '12', 'Glugla Coefficient')
    if Tool.is_Okay():
        Tool.Set_Input ('SAND', SAND)
        Tool.Set_Input ('SILT', SILT)
        Tool.Set_Input ('CLAY', CLAY)
        Tool.Set_Output('AIR', AIR)
        Tool.Set_Output('GLUGLA', GLUGLA)
        return Tool.Execute(Verbose)
    return False

def run_tool_sim_hydrology_12(SAND=None, SILT=None, CLAY=None, AIR=None, GLUGLA=None, Verbose=2):
    '''
    Glugla Coefficient
    ----------
    [sim_hydrology.12]\n
    Derivation of Glugla coefficient and, optionally, air capacitiy from soil texture data using a simple pedotransfer function. If one grain size fraction input is not provided its content is estimated from the contents of the other two fractions.\n
    Arguments
    ----------
    - SAND [`optional input grid`] : Sand. Percent
    - SILT [`optional input grid`] : Silt. Percent
    - CLAY [`optional input grid`] : Clay. Percent
    - AIR [`output grid`] : Air Capacity
    - GLUGLA [`output grid`] : Glugla Coefficient

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('sim_hydrology', '12', 'Glugla Coefficient')
    if Tool.is_Okay():
        Tool.Set_Input ('SAND', SAND)
        Tool.Set_Input ('SILT', SILT)
        Tool.Set_Input ('CLAY', CLAY)
        Tool.Set_Output('AIR', AIR)
        Tool.Set_Output('GLUGLA', GLUGLA)
        return Tool.Execute(Verbose)
    return False

