#! /usr/bin/env python

'''
Python Interface to SAGA Tools Library
----------
- Category : Simulation
- Name     : Air Flow Simulations
- ID       : sim_air_flow

Description
----------
Air Flow Simulations
'''

from PySAGA.helper import Tool_Wrapper

def Cold_Air_Flow(DEM=None, PRODUCTION=None, FRICTION=None, AIR=None, VELOCITY=None, PRODUCTION_DEFAULT=None, FRICTION_DEFAULT=None, RESET=None, TIME_STOP=None, TIME_UPDATE=None, EDGE=None, DELAY=None, T_AIR=None, T_AIR_COLD=None, Verbose=2):
    '''
    Cold Air Flow
    ----------
    [sim_air_flow.0]\n
    A simple cold air flow simulation.\n
    Arguments
    ----------
    - DEM [`input grid`] : Elevation
    - PRODUCTION [`optional input grid`] : Production. Rate of cold air production [m/h].
    - FRICTION [`optional input grid`] : Surface Friction Coefficient. Surface friction coefficient.
    - AIR [`output grid`] : Cold Air Height
    - VELOCITY [`output grid`] : Velocity
    - PRODUCTION_DEFAULT [`floating point number`] : Default. Minimum: 0.000000 Default: 1.000000 default value if no grid has been selected
    - FRICTION_DEFAULT [`floating point number`] : Default. Minimum: 0.000000 Default: 1.000000 default value if no grid has been selected
    - RESET [`boolean`] : Reset. Default: 1
    - TIME_STOP [`floating point number`] : Simulation Time [h]. Minimum: 0.000000 Default: 6.000000 Simulation time in hours.
    - TIME_UPDATE [`floating point number`] : Map Update Frequency [min]. Minimum: 0.000000 Default: 10.000000
    - EDGE [`choice`] : Edge. Available Choices: [0] closed [1] open Default: 1
    - DELAY [`floating point number`] : Time Step Adjustment. Minimum: 0.010000 Maximum: 1.000000 Default: 0.500000 Choosing a lower value will result in a better numerical precision but also in a longer calculation time.
    - T_AIR [`floating point number`] : Surrounding Air Temperature. Minimum: -273.150000 Default: 15.000000 Surrounding air temperature [degree Celsius].
    - T_AIR_COLD [`floating point number`] : Cold Air Temperature. Minimum: -273.150000 Default: 5.000000 Cold air temperature [degree Celsius].

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('sim_air_flow', '0', 'Cold Air Flow')
    if Tool.is_Okay():
        Tool.Set_Input ('DEM', DEM)
        Tool.Set_Input ('PRODUCTION', PRODUCTION)
        Tool.Set_Input ('FRICTION', FRICTION)
        Tool.Set_Output('AIR', AIR)
        Tool.Set_Output('VELOCITY', VELOCITY)
        Tool.Set_Option('PRODUCTION_DEFAULT', PRODUCTION_DEFAULT)
        Tool.Set_Option('FRICTION_DEFAULT', FRICTION_DEFAULT)
        Tool.Set_Option('RESET', RESET)
        Tool.Set_Option('TIME_STOP', TIME_STOP)
        Tool.Set_Option('TIME_UPDATE', TIME_UPDATE)
        Tool.Set_Option('EDGE', EDGE)
        Tool.Set_Option('DELAY', DELAY)
        Tool.Set_Option('T_AIR', T_AIR)
        Tool.Set_Option('T_AIR_COLD', T_AIR_COLD)
        return Tool.Execute(Verbose)
    return False

def run_tool_sim_air_flow_0(DEM=None, PRODUCTION=None, FRICTION=None, AIR=None, VELOCITY=None, PRODUCTION_DEFAULT=None, FRICTION_DEFAULT=None, RESET=None, TIME_STOP=None, TIME_UPDATE=None, EDGE=None, DELAY=None, T_AIR=None, T_AIR_COLD=None, Verbose=2):
    '''
    Cold Air Flow
    ----------
    [sim_air_flow.0]\n
    A simple cold air flow simulation.\n
    Arguments
    ----------
    - DEM [`input grid`] : Elevation
    - PRODUCTION [`optional input grid`] : Production. Rate of cold air production [m/h].
    - FRICTION [`optional input grid`] : Surface Friction Coefficient. Surface friction coefficient.
    - AIR [`output grid`] : Cold Air Height
    - VELOCITY [`output grid`] : Velocity
    - PRODUCTION_DEFAULT [`floating point number`] : Default. Minimum: 0.000000 Default: 1.000000 default value if no grid has been selected
    - FRICTION_DEFAULT [`floating point number`] : Default. Minimum: 0.000000 Default: 1.000000 default value if no grid has been selected
    - RESET [`boolean`] : Reset. Default: 1
    - TIME_STOP [`floating point number`] : Simulation Time [h]. Minimum: 0.000000 Default: 6.000000 Simulation time in hours.
    - TIME_UPDATE [`floating point number`] : Map Update Frequency [min]. Minimum: 0.000000 Default: 10.000000
    - EDGE [`choice`] : Edge. Available Choices: [0] closed [1] open Default: 1
    - DELAY [`floating point number`] : Time Step Adjustment. Minimum: 0.010000 Maximum: 1.000000 Default: 0.500000 Choosing a lower value will result in a better numerical precision but also in a longer calculation time.
    - T_AIR [`floating point number`] : Surrounding Air Temperature. Minimum: -273.150000 Default: 15.000000 Surrounding air temperature [degree Celsius].
    - T_AIR_COLD [`floating point number`] : Cold Air Temperature. Minimum: -273.150000 Default: 5.000000 Cold air temperature [degree Celsius].

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('sim_air_flow', '0', 'Cold Air Flow')
    if Tool.is_Okay():
        Tool.Set_Input ('DEM', DEM)
        Tool.Set_Input ('PRODUCTION', PRODUCTION)
        Tool.Set_Input ('FRICTION', FRICTION)
        Tool.Set_Output('AIR', AIR)
        Tool.Set_Output('VELOCITY', VELOCITY)
        Tool.Set_Option('PRODUCTION_DEFAULT', PRODUCTION_DEFAULT)
        Tool.Set_Option('FRICTION_DEFAULT', FRICTION_DEFAULT)
        Tool.Set_Option('RESET', RESET)
        Tool.Set_Option('TIME_STOP', TIME_STOP)
        Tool.Set_Option('TIME_UPDATE', TIME_UPDATE)
        Tool.Set_Option('EDGE', EDGE)
        Tool.Set_Option('DELAY', DELAY)
        Tool.Set_Option('T_AIR', T_AIR)
        Tool.Set_Option('T_AIR_COLD', T_AIR_COLD)
        return Tool.Execute(Verbose)
    return False

