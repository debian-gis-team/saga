#! /usr/bin/env python

'''
Python Interface to SAGA Tools Library
----------
- Category : Reports
- Name     : PDF
- ID       : docs_pdf

Description
----------
Create reports in the Portable Document Format (PDF). The export tools are using the free open source library [libHaru](https://github.com/libharu/libharu) version 2.4.2
'''

from PySAGA.helper import Tool_Wrapper

def Shapes_Report(SHAPES=None, SUBTITLE=None, FILENAME=None, PAPER_SIZE=None, COLOR_LINE=None, COLOR_FILL=None, LAYOUT_MODE=None, LAYOUT_BREAK=None, COLUMNS=None, CELL_MODE=None, CELL_HEIGHT=None, Verbose=2):
    '''
    Shapes Report
    ----------
    [docs_pdf.0]\n
    Shapes Report\n
    Arguments
    ----------
    - SHAPES [`input shapes`] : Shapes
    - SUBTITLE [`table field`] : Subtitle
    - FILENAME [`file path`] : PDF File
    - PAPER_SIZE [`choice`] : Paper Format. Available Choices: [0] A4 Portrait [1] A4 Landscape [2] A3 Portrait [3] A3 Landscape Default: 0
    - COLOR_LINE [`color`] : Line Color. Default: 0
    - COLOR_FILL [`color`] : Fill Color. Default: 65280
    - LAYOUT_MODE [`choice`] : Layout. Available Choices: [0] horizontal [1] vertical Default: 1
    - LAYOUT_BREAK [`floating point number`] : Map/Table Size Ratio [%]. Minimum: 0.000000 Maximum: 100.000000 Default: 50.000000
    - COLUMNS [`integer number`] : Columns. Minimum: 1 Default: 2 Number of attribute table columns.
    - CELL_MODE [`choice`] : Cell Sizes. Available Choices: [0] fit to page [1] fixed cell height Default: 0
    - CELL_HEIGHT [`integer number`] : Cell Height. Minimum: 1 Default: 8

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('docs_pdf', '0', 'Shapes Report')
    if Tool.is_Okay():
        Tool.Set_Input ('SHAPES', SHAPES)
        Tool.Set_Option('SUBTITLE', SUBTITLE)
        Tool.Set_Option('FILENAME', FILENAME)
        Tool.Set_Option('PAPER_SIZE', PAPER_SIZE)
        Tool.Set_Option('COLOR_LINE', COLOR_LINE)
        Tool.Set_Option('COLOR_FILL', COLOR_FILL)
        Tool.Set_Option('LAYOUT_MODE', LAYOUT_MODE)
        Tool.Set_Option('LAYOUT_BREAK', LAYOUT_BREAK)
        Tool.Set_Option('COLUMNS', COLUMNS)
        Tool.Set_Option('CELL_MODE', CELL_MODE)
        Tool.Set_Option('CELL_HEIGHT', CELL_HEIGHT)
        return Tool.Execute(Verbose)
    return False

def run_tool_docs_pdf_0(SHAPES=None, SUBTITLE=None, FILENAME=None, PAPER_SIZE=None, COLOR_LINE=None, COLOR_FILL=None, LAYOUT_MODE=None, LAYOUT_BREAK=None, COLUMNS=None, CELL_MODE=None, CELL_HEIGHT=None, Verbose=2):
    '''
    Shapes Report
    ----------
    [docs_pdf.0]\n
    Shapes Report\n
    Arguments
    ----------
    - SHAPES [`input shapes`] : Shapes
    - SUBTITLE [`table field`] : Subtitle
    - FILENAME [`file path`] : PDF File
    - PAPER_SIZE [`choice`] : Paper Format. Available Choices: [0] A4 Portrait [1] A4 Landscape [2] A3 Portrait [3] A3 Landscape Default: 0
    - COLOR_LINE [`color`] : Line Color. Default: 0
    - COLOR_FILL [`color`] : Fill Color. Default: 65280
    - LAYOUT_MODE [`choice`] : Layout. Available Choices: [0] horizontal [1] vertical Default: 1
    - LAYOUT_BREAK [`floating point number`] : Map/Table Size Ratio [%]. Minimum: 0.000000 Maximum: 100.000000 Default: 50.000000
    - COLUMNS [`integer number`] : Columns. Minimum: 1 Default: 2 Number of attribute table columns.
    - CELL_MODE [`choice`] : Cell Sizes. Available Choices: [0] fit to page [1] fixed cell height Default: 0
    - CELL_HEIGHT [`integer number`] : Cell Height. Minimum: 1 Default: 8

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('docs_pdf', '0', 'Shapes Report')
    if Tool.is_Okay():
        Tool.Set_Input ('SHAPES', SHAPES)
        Tool.Set_Option('SUBTITLE', SUBTITLE)
        Tool.Set_Option('FILENAME', FILENAME)
        Tool.Set_Option('PAPER_SIZE', PAPER_SIZE)
        Tool.Set_Option('COLOR_LINE', COLOR_LINE)
        Tool.Set_Option('COLOR_FILL', COLOR_FILL)
        Tool.Set_Option('LAYOUT_MODE', LAYOUT_MODE)
        Tool.Set_Option('LAYOUT_BREAK', LAYOUT_BREAK)
        Tool.Set_Option('COLUMNS', COLUMNS)
        Tool.Set_Option('CELL_MODE', CELL_MODE)
        Tool.Set_Option('CELL_HEIGHT', CELL_HEIGHT)
        return Tool.Execute(Verbose)
    return False

def Shapes_Summary_Report(SHAPES=None, TABLE=None, FIELD=None, PDF=None, OUTPUTPATH=None, Verbose=2):
    '''
    Shapes Summary Report
    ----------
    [docs_pdf.1]\n
    (c) 2004 by Victor Olaya. summary.\n
    Arguments
    ----------
    - SHAPES [`input shapes`] : Shapes
    - TABLE [`output table`] : Summary Table
    - FIELD [`table field`] : Field
    - PDF [`boolean`] : Create PDF Docs. Default: 1 Create PDF Docs
    - OUTPUTPATH [`file path`] : Folder. Folder

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('docs_pdf', '1', 'Shapes Summary Report')
    if Tool.is_Okay():
        Tool.Set_Input ('SHAPES', SHAPES)
        Tool.Set_Output('TABLE', TABLE)
        Tool.Set_Option('FIELD', FIELD)
        Tool.Set_Option('PDF', PDF)
        Tool.Set_Option('OUTPUTPATH', OUTPUTPATH)
        return Tool.Execute(Verbose)
    return False

def run_tool_docs_pdf_1(SHAPES=None, TABLE=None, FIELD=None, PDF=None, OUTPUTPATH=None, Verbose=2):
    '''
    Shapes Summary Report
    ----------
    [docs_pdf.1]\n
    (c) 2004 by Victor Olaya. summary.\n
    Arguments
    ----------
    - SHAPES [`input shapes`] : Shapes
    - TABLE [`output table`] : Summary Table
    - FIELD [`table field`] : Field
    - PDF [`boolean`] : Create PDF Docs. Default: 1 Create PDF Docs
    - OUTPUTPATH [`file path`] : Folder. Folder

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('docs_pdf', '1', 'Shapes Summary Report')
    if Tool.is_Okay():
        Tool.Set_Input ('SHAPES', SHAPES)
        Tool.Set_Output('TABLE', TABLE)
        Tool.Set_Option('FIELD', FIELD)
        Tool.Set_Option('PDF', PDF)
        Tool.Set_Option('OUTPUTPATH', OUTPUTPATH)
        return Tool.Execute(Verbose)
    return False

def Terrain_Path_Cross_Sections(DEM=None, LINES=None, SECTIONS=None, NUMPOINTS=None, INTERVAL=None, STEP=None, PDF=None, OUTPUTPATH=None, WIDTH=None, SLOPE=None, THRESHOLD=None, Verbose=2):
    '''
    Terrain Path Cross Sections
    ----------
    [docs_pdf.2]\n
    (c) 2004 Victor Olaya. Cross Sections\n
    Arguments
    ----------
    - DEM [`input grid`] : DEM. Digital Terrain Model
    - LINES [`input shapes`] : Path
    - SECTIONS [`output shapes`] : Cross Sections
    - NUMPOINTS [`integer number`] : Number of points. Minimum: 1 Default: 10 Number of points on each side of the section
    - INTERVAL [`floating point number`] : Interval. Default: 10.000000 Interval between points (in grid units).
    - STEP [`integer number`] : n. Default: 2 Draw a section each n points
    - PDF [`boolean`] : Create PDF Documents. Default: 1
    - OUTPUTPATH [`file path`] : Folder for PDF Files
    - WIDTH [`floating point number`] : Road Width. Default: 5.000000
    - SLOPE [`floating point number`] : Side Slope. Default: 0.500000
    - THRESHOLD [`floating point number`] : Height Threshold. Default: 0.500000 Height threshold for optimal profile calculation

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('docs_pdf', '2', 'Terrain Path Cross Sections')
    if Tool.is_Okay():
        Tool.Set_Input ('DEM', DEM)
        Tool.Set_Input ('LINES', LINES)
        Tool.Set_Output('SECTIONS', SECTIONS)
        Tool.Set_Option('NUMPOINTS', NUMPOINTS)
        Tool.Set_Option('INTERVAL', INTERVAL)
        Tool.Set_Option('STEP', STEP)
        Tool.Set_Option('PDF', PDF)
        Tool.Set_Option('OUTPUTPATH', OUTPUTPATH)
        Tool.Set_Option('WIDTH', WIDTH)
        Tool.Set_Option('SLOPE', SLOPE)
        Tool.Set_Option('THRESHOLD', THRESHOLD)
        return Tool.Execute(Verbose)
    return False

def run_tool_docs_pdf_2(DEM=None, LINES=None, SECTIONS=None, NUMPOINTS=None, INTERVAL=None, STEP=None, PDF=None, OUTPUTPATH=None, WIDTH=None, SLOPE=None, THRESHOLD=None, Verbose=2):
    '''
    Terrain Path Cross Sections
    ----------
    [docs_pdf.2]\n
    (c) 2004 Victor Olaya. Cross Sections\n
    Arguments
    ----------
    - DEM [`input grid`] : DEM. Digital Terrain Model
    - LINES [`input shapes`] : Path
    - SECTIONS [`output shapes`] : Cross Sections
    - NUMPOINTS [`integer number`] : Number of points. Minimum: 1 Default: 10 Number of points on each side of the section
    - INTERVAL [`floating point number`] : Interval. Default: 10.000000 Interval between points (in grid units).
    - STEP [`integer number`] : n. Default: 2 Draw a section each n points
    - PDF [`boolean`] : Create PDF Documents. Default: 1
    - OUTPUTPATH [`file path`] : Folder for PDF Files
    - WIDTH [`floating point number`] : Road Width. Default: 5.000000
    - SLOPE [`floating point number`] : Side Slope. Default: 0.500000
    - THRESHOLD [`floating point number`] : Height Threshold. Default: 0.500000 Height threshold for optimal profile calculation

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('docs_pdf', '2', 'Terrain Path Cross Sections')
    if Tool.is_Okay():
        Tool.Set_Input ('DEM', DEM)
        Tool.Set_Input ('LINES', LINES)
        Tool.Set_Output('SECTIONS', SECTIONS)
        Tool.Set_Option('NUMPOINTS', NUMPOINTS)
        Tool.Set_Option('INTERVAL', INTERVAL)
        Tool.Set_Option('STEP', STEP)
        Tool.Set_Option('PDF', PDF)
        Tool.Set_Option('OUTPUTPATH', OUTPUTPATH)
        Tool.Set_Option('WIDTH', WIDTH)
        Tool.Set_Option('SLOPE', SLOPE)
        Tool.Set_Option('THRESHOLD', THRESHOLD)
        return Tool.Execute(Verbose)
    return False

