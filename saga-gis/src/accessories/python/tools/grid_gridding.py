#! /usr/bin/env python

'''
Python Interface to SAGA Tools Library
----------
- Category : Grid
- Name     : Gridding
- ID       : grid_gridding

Description
----------
Tools for the gridding of points and other vector data.
'''

from PySAGA.helper import Tool_Wrapper

def Shapes_to_Grid(INPUT=None, TARGET_TEMPLATE=None, GRID=None, COUNT=None, FIELD=None, OUTPUT=None, MULTIPLE=None, LINE_TYPE=None, POLY_TYPE=None, GRID_TYPE=None, TARGET_DEFINITION=None, TARGET_USER_SIZE=None, TARGET_USER_XMIN=None, TARGET_USER_XMAX=None, TARGET_USER_YMIN=None, TARGET_USER_YMAX=None, TARGET_USER_COLS=None, TARGET_USER_ROWS=None, TARGET_USER_FLAT=None, TARGET_USER_FITS=None, TARGET_SYSTEM=None, Verbose=2):
    '''
    Shapes to Grid
    ----------
    [grid_gridding.0]\n
    Gridding of a shapes layer. If some shapes are selected, only these will be gridded.\n
    Arguments
    ----------
    - INPUT [`input shapes`] : Shapes
    - TARGET_TEMPLATE [`optional input grid`] : Target System. use this grid's system for output grids
    - GRID [`output grid`] : Grid
    - COUNT [`output grid`] : Number of Values
    - FIELD [`table field`] : Attribute
    - OUTPUT [`choice`] : Output Values. Available Choices: [0] data / no-data [1] index number [2] attribute Default: 2
    - MULTIPLE [`choice`] : Method for Multiple Values. Available Choices: [0] first [1] last [2] minimum [3] maximum [4] mean Default: 1
    - LINE_TYPE [`choice`] : Lines. Available Choices: [0] thin [1] thick Default: 1
    - POLY_TYPE [`choice`] : Polygon. Available Choices: [0] node [1] cell Default: 1
    - GRID_TYPE [`data type`] : Data Type. Available Choices: [0] bit [1] unsigned 1 byte integer [2] signed 1 byte integer [3] unsigned 2 byte integer [4] signed 2 byte integer [5] unsigned 4 byte integer [6] signed 4 byte integer [7] unsigned 8 byte integer [8] signed 8 byte integer [9] 4 byte floating point number [10] 8 byte floating point number [11] same as attribute Default: 11
    - TARGET_DEFINITION [`choice`] : Target Grid System. Available Choices: [0] user defined [1] grid or grid system Default: 0
    - TARGET_USER_SIZE [`floating point number`] : Cellsize. Minimum: 0.000000 Default: 1.000000
    - TARGET_USER_XMIN [`floating point number`] : West. Default: 0.000000
    - TARGET_USER_XMAX [`floating point number`] : East. Default: 100.000000
    - TARGET_USER_YMIN [`floating point number`] : South. Default: 0.000000
    - TARGET_USER_YMAX [`floating point number`] : North. Default: 100.000000
    - TARGET_USER_COLS [`integer number`] : Columns. Minimum: 1 Default: 101 Number of cells in East-West direction.
    - TARGET_USER_ROWS [`integer number`] : Rows. Minimum: 1 Default: 101 Number of cells in North-South direction.
    - TARGET_USER_FLAT [`boolean`] : Rounding. Default: 1 Round bounding coordinates to multiples of cell size. Ignored if cell size has decimal places.
    - TARGET_USER_FITS [`choice`] : Fit. Available Choices: [0] nodes [1] cells Default: 0
    - TARGET_SYSTEM [`grid system`] : Grid System

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_gridding', '0', 'Shapes to Grid')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Input ('TARGET_TEMPLATE', TARGET_TEMPLATE)
        Tool.Set_Output('GRID', GRID)
        Tool.Set_Output('COUNT', COUNT)
        Tool.Set_Option('FIELD', FIELD)
        Tool.Set_Option('OUTPUT', OUTPUT)
        Tool.Set_Option('MULTIPLE', MULTIPLE)
        Tool.Set_Option('LINE_TYPE', LINE_TYPE)
        Tool.Set_Option('POLY_TYPE', POLY_TYPE)
        Tool.Set_Option('GRID_TYPE', GRID_TYPE)
        Tool.Set_Option('TARGET_DEFINITION', TARGET_DEFINITION)
        Tool.Set_Option('TARGET_USER_SIZE', TARGET_USER_SIZE)
        Tool.Set_Option('TARGET_USER_XMIN', TARGET_USER_XMIN)
        Tool.Set_Option('TARGET_USER_XMAX', TARGET_USER_XMAX)
        Tool.Set_Option('TARGET_USER_YMIN', TARGET_USER_YMIN)
        Tool.Set_Option('TARGET_USER_YMAX', TARGET_USER_YMAX)
        Tool.Set_Option('TARGET_USER_COLS', TARGET_USER_COLS)
        Tool.Set_Option('TARGET_USER_ROWS', TARGET_USER_ROWS)
        Tool.Set_Option('TARGET_USER_FLAT', TARGET_USER_FLAT)
        Tool.Set_Option('TARGET_USER_FITS', TARGET_USER_FITS)
        Tool.Set_Option('TARGET_SYSTEM', TARGET_SYSTEM)
        return Tool.Execute(Verbose)
    return False

def run_tool_grid_gridding_0(INPUT=None, TARGET_TEMPLATE=None, GRID=None, COUNT=None, FIELD=None, OUTPUT=None, MULTIPLE=None, LINE_TYPE=None, POLY_TYPE=None, GRID_TYPE=None, TARGET_DEFINITION=None, TARGET_USER_SIZE=None, TARGET_USER_XMIN=None, TARGET_USER_XMAX=None, TARGET_USER_YMIN=None, TARGET_USER_YMAX=None, TARGET_USER_COLS=None, TARGET_USER_ROWS=None, TARGET_USER_FLAT=None, TARGET_USER_FITS=None, TARGET_SYSTEM=None, Verbose=2):
    '''
    Shapes to Grid
    ----------
    [grid_gridding.0]\n
    Gridding of a shapes layer. If some shapes are selected, only these will be gridded.\n
    Arguments
    ----------
    - INPUT [`input shapes`] : Shapes
    - TARGET_TEMPLATE [`optional input grid`] : Target System. use this grid's system for output grids
    - GRID [`output grid`] : Grid
    - COUNT [`output grid`] : Number of Values
    - FIELD [`table field`] : Attribute
    - OUTPUT [`choice`] : Output Values. Available Choices: [0] data / no-data [1] index number [2] attribute Default: 2
    - MULTIPLE [`choice`] : Method for Multiple Values. Available Choices: [0] first [1] last [2] minimum [3] maximum [4] mean Default: 1
    - LINE_TYPE [`choice`] : Lines. Available Choices: [0] thin [1] thick Default: 1
    - POLY_TYPE [`choice`] : Polygon. Available Choices: [0] node [1] cell Default: 1
    - GRID_TYPE [`data type`] : Data Type. Available Choices: [0] bit [1] unsigned 1 byte integer [2] signed 1 byte integer [3] unsigned 2 byte integer [4] signed 2 byte integer [5] unsigned 4 byte integer [6] signed 4 byte integer [7] unsigned 8 byte integer [8] signed 8 byte integer [9] 4 byte floating point number [10] 8 byte floating point number [11] same as attribute Default: 11
    - TARGET_DEFINITION [`choice`] : Target Grid System. Available Choices: [0] user defined [1] grid or grid system Default: 0
    - TARGET_USER_SIZE [`floating point number`] : Cellsize. Minimum: 0.000000 Default: 1.000000
    - TARGET_USER_XMIN [`floating point number`] : West. Default: 0.000000
    - TARGET_USER_XMAX [`floating point number`] : East. Default: 100.000000
    - TARGET_USER_YMIN [`floating point number`] : South. Default: 0.000000
    - TARGET_USER_YMAX [`floating point number`] : North. Default: 100.000000
    - TARGET_USER_COLS [`integer number`] : Columns. Minimum: 1 Default: 101 Number of cells in East-West direction.
    - TARGET_USER_ROWS [`integer number`] : Rows. Minimum: 1 Default: 101 Number of cells in North-South direction.
    - TARGET_USER_FLAT [`boolean`] : Rounding. Default: 1 Round bounding coordinates to multiples of cell size. Ignored if cell size has decimal places.
    - TARGET_USER_FITS [`choice`] : Fit. Available Choices: [0] nodes [1] cells Default: 0
    - TARGET_SYSTEM [`grid system`] : Grid System

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_gridding', '0', 'Shapes to Grid')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Input ('TARGET_TEMPLATE', TARGET_TEMPLATE)
        Tool.Set_Output('GRID', GRID)
        Tool.Set_Output('COUNT', COUNT)
        Tool.Set_Option('FIELD', FIELD)
        Tool.Set_Option('OUTPUT', OUTPUT)
        Tool.Set_Option('MULTIPLE', MULTIPLE)
        Tool.Set_Option('LINE_TYPE', LINE_TYPE)
        Tool.Set_Option('POLY_TYPE', POLY_TYPE)
        Tool.Set_Option('GRID_TYPE', GRID_TYPE)
        Tool.Set_Option('TARGET_DEFINITION', TARGET_DEFINITION)
        Tool.Set_Option('TARGET_USER_SIZE', TARGET_USER_SIZE)
        Tool.Set_Option('TARGET_USER_XMIN', TARGET_USER_XMIN)
        Tool.Set_Option('TARGET_USER_XMAX', TARGET_USER_XMAX)
        Tool.Set_Option('TARGET_USER_YMIN', TARGET_USER_YMIN)
        Tool.Set_Option('TARGET_USER_YMAX', TARGET_USER_YMAX)
        Tool.Set_Option('TARGET_USER_COLS', TARGET_USER_COLS)
        Tool.Set_Option('TARGET_USER_ROWS', TARGET_USER_ROWS)
        Tool.Set_Option('TARGET_USER_FLAT', TARGET_USER_FLAT)
        Tool.Set_Option('TARGET_USER_FITS', TARGET_USER_FITS)
        Tool.Set_Option('TARGET_SYSTEM', TARGET_SYSTEM)
        return Tool.Execute(Verbose)
    return False

def Inverse_Distance_Weighted(POINTS=None, TARGET_TEMPLATE=None, CV_SUMMARY=None, CV_RESIDUALS=None, TARGET_OUT_GRID=None, FIELD=None, CV_METHOD=None, CV_SAMPLES=None, TARGET_DEFINITION=None, TARGET_USER_SIZE=None, TARGET_USER_XMIN=None, TARGET_USER_XMAX=None, TARGET_USER_YMIN=None, TARGET_USER_YMAX=None, TARGET_USER_COLS=None, TARGET_USER_ROWS=None, TARGET_USER_FLAT=None, TARGET_USER_FITS=None, TARGET_SYSTEM=None, SEARCH_RANGE=None, SEARCH_RADIUS=None, SEARCH_POINTS_ALL=None, SEARCH_POINTS_MIN=None, SEARCH_POINTS_MAX=None, DW_WEIGHTING=None, DW_IDW_POWER=None, DW_BANDWIDTH=None, Verbose=2):
    '''
    Inverse Distance Weighted
    ----------
    [grid_gridding.1]\n
    Inverse distance grid interpolation from irregular distributed points.\n
    Arguments
    ----------
    - POINTS [`input shapes`] : Points
    - TARGET_TEMPLATE [`optional input grid`] : Target System. use this grid's system for output grids
    - CV_SUMMARY [`output table`] : Cross Validation Summary
    - CV_RESIDUALS [`output shapes`] : Cross Validation Residuals
    - TARGET_OUT_GRID [`output grid`] : Target Grid
    - FIELD [`table field`] : Attribute
    - CV_METHOD [`choice`] : Cross Validation. Available Choices: [0] none [1] leave one out [2] 2-fold [3] k-fold Default: 0
    - CV_SAMPLES [`integer number`] : Cross Validation Subsamples. Minimum: 2 Default: 10 number of subsamples for k-fold cross validation
    - TARGET_DEFINITION [`choice`] : Target Grid System. Available Choices: [0] user defined [1] grid or grid system Default: 0
    - TARGET_USER_SIZE [`floating point number`] : Cellsize. Minimum: 0.000000 Default: 1.000000
    - TARGET_USER_XMIN [`floating point number`] : West. Default: 0.000000
    - TARGET_USER_XMAX [`floating point number`] : East. Default: 100.000000
    - TARGET_USER_YMIN [`floating point number`] : South. Default: 0.000000
    - TARGET_USER_YMAX [`floating point number`] : North. Default: 100.000000
    - TARGET_USER_COLS [`integer number`] : Columns. Minimum: 1 Default: 101 Number of cells in East-West direction.
    - TARGET_USER_ROWS [`integer number`] : Rows. Minimum: 1 Default: 101 Number of cells in North-South direction.
    - TARGET_USER_FLAT [`boolean`] : Rounding. Default: 1 Round bounding coordinates to multiples of cell size. Ignored if cell size has decimal places.
    - TARGET_USER_FITS [`choice`] : Fit. Available Choices: [0] nodes [1] cells Default: 0
    - TARGET_SYSTEM [`grid system`] : Grid System
    - SEARCH_RANGE [`choice`] : Search Range. Available Choices: [0] local [1] global Default: 1
    - SEARCH_RADIUS [`floating point number`] : Maximum Search Distance. Minimum: 0.000000 Default: 1000.000000 local maximum search distance given in map units
    - SEARCH_POINTS_ALL [`choice`] : Number of Points. Available Choices: [0] maximum number of nearest points [1] all points within search distance Default: 1
    - SEARCH_POINTS_MIN [`integer number`] : Minimum. Minimum: 1 Default: 1 minimum number of points to use
    - SEARCH_POINTS_MAX [`integer number`] : Maximum. Minimum: 1 Default: 20 maximum number of nearest points
    - DW_WEIGHTING [`choice`] : Weighting Function. Available Choices: [0] no distance weighting [1] inverse distance to a power [2] exponential [3] gaussian Default: 1
    - DW_IDW_POWER [`floating point number`] : Power. Minimum: 0.000000 Default: 2.000000
    - DW_BANDWIDTH [`floating point number`] : Bandwidth. Minimum: 0.000000 Default: 1.000000 Bandwidth for exponential and Gaussian weighting

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_gridding', '1', 'Inverse Distance Weighted')
    if Tool.is_Okay():
        Tool.Set_Input ('POINTS', POINTS)
        Tool.Set_Input ('TARGET_TEMPLATE', TARGET_TEMPLATE)
        Tool.Set_Output('CV_SUMMARY', CV_SUMMARY)
        Tool.Set_Output('CV_RESIDUALS', CV_RESIDUALS)
        Tool.Set_Output('TARGET_OUT_GRID', TARGET_OUT_GRID)
        Tool.Set_Option('FIELD', FIELD)
        Tool.Set_Option('CV_METHOD', CV_METHOD)
        Tool.Set_Option('CV_SAMPLES', CV_SAMPLES)
        Tool.Set_Option('TARGET_DEFINITION', TARGET_DEFINITION)
        Tool.Set_Option('TARGET_USER_SIZE', TARGET_USER_SIZE)
        Tool.Set_Option('TARGET_USER_XMIN', TARGET_USER_XMIN)
        Tool.Set_Option('TARGET_USER_XMAX', TARGET_USER_XMAX)
        Tool.Set_Option('TARGET_USER_YMIN', TARGET_USER_YMIN)
        Tool.Set_Option('TARGET_USER_YMAX', TARGET_USER_YMAX)
        Tool.Set_Option('TARGET_USER_COLS', TARGET_USER_COLS)
        Tool.Set_Option('TARGET_USER_ROWS', TARGET_USER_ROWS)
        Tool.Set_Option('TARGET_USER_FLAT', TARGET_USER_FLAT)
        Tool.Set_Option('TARGET_USER_FITS', TARGET_USER_FITS)
        Tool.Set_Option('TARGET_SYSTEM', TARGET_SYSTEM)
        Tool.Set_Option('SEARCH_RANGE', SEARCH_RANGE)
        Tool.Set_Option('SEARCH_RADIUS', SEARCH_RADIUS)
        Tool.Set_Option('SEARCH_POINTS_ALL', SEARCH_POINTS_ALL)
        Tool.Set_Option('SEARCH_POINTS_MIN', SEARCH_POINTS_MIN)
        Tool.Set_Option('SEARCH_POINTS_MAX', SEARCH_POINTS_MAX)
        Tool.Set_Option('DW_WEIGHTING', DW_WEIGHTING)
        Tool.Set_Option('DW_IDW_POWER', DW_IDW_POWER)
        Tool.Set_Option('DW_BANDWIDTH', DW_BANDWIDTH)
        return Tool.Execute(Verbose)
    return False

def run_tool_grid_gridding_1(POINTS=None, TARGET_TEMPLATE=None, CV_SUMMARY=None, CV_RESIDUALS=None, TARGET_OUT_GRID=None, FIELD=None, CV_METHOD=None, CV_SAMPLES=None, TARGET_DEFINITION=None, TARGET_USER_SIZE=None, TARGET_USER_XMIN=None, TARGET_USER_XMAX=None, TARGET_USER_YMIN=None, TARGET_USER_YMAX=None, TARGET_USER_COLS=None, TARGET_USER_ROWS=None, TARGET_USER_FLAT=None, TARGET_USER_FITS=None, TARGET_SYSTEM=None, SEARCH_RANGE=None, SEARCH_RADIUS=None, SEARCH_POINTS_ALL=None, SEARCH_POINTS_MIN=None, SEARCH_POINTS_MAX=None, DW_WEIGHTING=None, DW_IDW_POWER=None, DW_BANDWIDTH=None, Verbose=2):
    '''
    Inverse Distance Weighted
    ----------
    [grid_gridding.1]\n
    Inverse distance grid interpolation from irregular distributed points.\n
    Arguments
    ----------
    - POINTS [`input shapes`] : Points
    - TARGET_TEMPLATE [`optional input grid`] : Target System. use this grid's system for output grids
    - CV_SUMMARY [`output table`] : Cross Validation Summary
    - CV_RESIDUALS [`output shapes`] : Cross Validation Residuals
    - TARGET_OUT_GRID [`output grid`] : Target Grid
    - FIELD [`table field`] : Attribute
    - CV_METHOD [`choice`] : Cross Validation. Available Choices: [0] none [1] leave one out [2] 2-fold [3] k-fold Default: 0
    - CV_SAMPLES [`integer number`] : Cross Validation Subsamples. Minimum: 2 Default: 10 number of subsamples for k-fold cross validation
    - TARGET_DEFINITION [`choice`] : Target Grid System. Available Choices: [0] user defined [1] grid or grid system Default: 0
    - TARGET_USER_SIZE [`floating point number`] : Cellsize. Minimum: 0.000000 Default: 1.000000
    - TARGET_USER_XMIN [`floating point number`] : West. Default: 0.000000
    - TARGET_USER_XMAX [`floating point number`] : East. Default: 100.000000
    - TARGET_USER_YMIN [`floating point number`] : South. Default: 0.000000
    - TARGET_USER_YMAX [`floating point number`] : North. Default: 100.000000
    - TARGET_USER_COLS [`integer number`] : Columns. Minimum: 1 Default: 101 Number of cells in East-West direction.
    - TARGET_USER_ROWS [`integer number`] : Rows. Minimum: 1 Default: 101 Number of cells in North-South direction.
    - TARGET_USER_FLAT [`boolean`] : Rounding. Default: 1 Round bounding coordinates to multiples of cell size. Ignored if cell size has decimal places.
    - TARGET_USER_FITS [`choice`] : Fit. Available Choices: [0] nodes [1] cells Default: 0
    - TARGET_SYSTEM [`grid system`] : Grid System
    - SEARCH_RANGE [`choice`] : Search Range. Available Choices: [0] local [1] global Default: 1
    - SEARCH_RADIUS [`floating point number`] : Maximum Search Distance. Minimum: 0.000000 Default: 1000.000000 local maximum search distance given in map units
    - SEARCH_POINTS_ALL [`choice`] : Number of Points. Available Choices: [0] maximum number of nearest points [1] all points within search distance Default: 1
    - SEARCH_POINTS_MIN [`integer number`] : Minimum. Minimum: 1 Default: 1 minimum number of points to use
    - SEARCH_POINTS_MAX [`integer number`] : Maximum. Minimum: 1 Default: 20 maximum number of nearest points
    - DW_WEIGHTING [`choice`] : Weighting Function. Available Choices: [0] no distance weighting [1] inverse distance to a power [2] exponential [3] gaussian Default: 1
    - DW_IDW_POWER [`floating point number`] : Power. Minimum: 0.000000 Default: 2.000000
    - DW_BANDWIDTH [`floating point number`] : Bandwidth. Minimum: 0.000000 Default: 1.000000 Bandwidth for exponential and Gaussian weighting

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_gridding', '1', 'Inverse Distance Weighted')
    if Tool.is_Okay():
        Tool.Set_Input ('POINTS', POINTS)
        Tool.Set_Input ('TARGET_TEMPLATE', TARGET_TEMPLATE)
        Tool.Set_Output('CV_SUMMARY', CV_SUMMARY)
        Tool.Set_Output('CV_RESIDUALS', CV_RESIDUALS)
        Tool.Set_Output('TARGET_OUT_GRID', TARGET_OUT_GRID)
        Tool.Set_Option('FIELD', FIELD)
        Tool.Set_Option('CV_METHOD', CV_METHOD)
        Tool.Set_Option('CV_SAMPLES', CV_SAMPLES)
        Tool.Set_Option('TARGET_DEFINITION', TARGET_DEFINITION)
        Tool.Set_Option('TARGET_USER_SIZE', TARGET_USER_SIZE)
        Tool.Set_Option('TARGET_USER_XMIN', TARGET_USER_XMIN)
        Tool.Set_Option('TARGET_USER_XMAX', TARGET_USER_XMAX)
        Tool.Set_Option('TARGET_USER_YMIN', TARGET_USER_YMIN)
        Tool.Set_Option('TARGET_USER_YMAX', TARGET_USER_YMAX)
        Tool.Set_Option('TARGET_USER_COLS', TARGET_USER_COLS)
        Tool.Set_Option('TARGET_USER_ROWS', TARGET_USER_ROWS)
        Tool.Set_Option('TARGET_USER_FLAT', TARGET_USER_FLAT)
        Tool.Set_Option('TARGET_USER_FITS', TARGET_USER_FITS)
        Tool.Set_Option('TARGET_SYSTEM', TARGET_SYSTEM)
        Tool.Set_Option('SEARCH_RANGE', SEARCH_RANGE)
        Tool.Set_Option('SEARCH_RADIUS', SEARCH_RADIUS)
        Tool.Set_Option('SEARCH_POINTS_ALL', SEARCH_POINTS_ALL)
        Tool.Set_Option('SEARCH_POINTS_MIN', SEARCH_POINTS_MIN)
        Tool.Set_Option('SEARCH_POINTS_MAX', SEARCH_POINTS_MAX)
        Tool.Set_Option('DW_WEIGHTING', DW_WEIGHTING)
        Tool.Set_Option('DW_IDW_POWER', DW_IDW_POWER)
        Tool.Set_Option('DW_BANDWIDTH', DW_BANDWIDTH)
        return Tool.Execute(Verbose)
    return False

def Nearest_Neighbour(POINTS=None, TARGET_TEMPLATE=None, CV_SUMMARY=None, CV_RESIDUALS=None, TARGET_OUT_GRID=None, FIELD=None, CV_METHOD=None, CV_SAMPLES=None, TARGET_DEFINITION=None, TARGET_USER_SIZE=None, TARGET_USER_XMIN=None, TARGET_USER_XMAX=None, TARGET_USER_YMIN=None, TARGET_USER_YMAX=None, TARGET_USER_COLS=None, TARGET_USER_ROWS=None, TARGET_USER_FLAT=None, TARGET_USER_FITS=None, TARGET_SYSTEM=None, Verbose=2):
    '''
    Nearest Neighbour
    ----------
    [grid_gridding.2]\n
    Nearest Neighbour method for grid interpolation from irregular distributed points.\n
    Arguments
    ----------
    - POINTS [`input shapes`] : Points
    - TARGET_TEMPLATE [`optional input grid`] : Target System. use this grid's system for output grids
    - CV_SUMMARY [`output table`] : Cross Validation Summary
    - CV_RESIDUALS [`output shapes`] : Cross Validation Residuals
    - TARGET_OUT_GRID [`output grid`] : Target Grid
    - FIELD [`table field`] : Attribute
    - CV_METHOD [`choice`] : Cross Validation. Available Choices: [0] none [1] leave one out [2] 2-fold [3] k-fold Default: 0
    - CV_SAMPLES [`integer number`] : Cross Validation Subsamples. Minimum: 2 Default: 10 number of subsamples for k-fold cross validation
    - TARGET_DEFINITION [`choice`] : Target Grid System. Available Choices: [0] user defined [1] grid or grid system Default: 0
    - TARGET_USER_SIZE [`floating point number`] : Cellsize. Minimum: 0.000000 Default: 1.000000
    - TARGET_USER_XMIN [`floating point number`] : West. Default: 0.000000
    - TARGET_USER_XMAX [`floating point number`] : East. Default: 100.000000
    - TARGET_USER_YMIN [`floating point number`] : South. Default: 0.000000
    - TARGET_USER_YMAX [`floating point number`] : North. Default: 100.000000
    - TARGET_USER_COLS [`integer number`] : Columns. Minimum: 1 Default: 101 Number of cells in East-West direction.
    - TARGET_USER_ROWS [`integer number`] : Rows. Minimum: 1 Default: 101 Number of cells in North-South direction.
    - TARGET_USER_FLAT [`boolean`] : Rounding. Default: 1 Round bounding coordinates to multiples of cell size. Ignored if cell size has decimal places.
    - TARGET_USER_FITS [`choice`] : Fit. Available Choices: [0] nodes [1] cells Default: 0
    - TARGET_SYSTEM [`grid system`] : Grid System

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_gridding', '2', 'Nearest Neighbour')
    if Tool.is_Okay():
        Tool.Set_Input ('POINTS', POINTS)
        Tool.Set_Input ('TARGET_TEMPLATE', TARGET_TEMPLATE)
        Tool.Set_Output('CV_SUMMARY', CV_SUMMARY)
        Tool.Set_Output('CV_RESIDUALS', CV_RESIDUALS)
        Tool.Set_Output('TARGET_OUT_GRID', TARGET_OUT_GRID)
        Tool.Set_Option('FIELD', FIELD)
        Tool.Set_Option('CV_METHOD', CV_METHOD)
        Tool.Set_Option('CV_SAMPLES', CV_SAMPLES)
        Tool.Set_Option('TARGET_DEFINITION', TARGET_DEFINITION)
        Tool.Set_Option('TARGET_USER_SIZE', TARGET_USER_SIZE)
        Tool.Set_Option('TARGET_USER_XMIN', TARGET_USER_XMIN)
        Tool.Set_Option('TARGET_USER_XMAX', TARGET_USER_XMAX)
        Tool.Set_Option('TARGET_USER_YMIN', TARGET_USER_YMIN)
        Tool.Set_Option('TARGET_USER_YMAX', TARGET_USER_YMAX)
        Tool.Set_Option('TARGET_USER_COLS', TARGET_USER_COLS)
        Tool.Set_Option('TARGET_USER_ROWS', TARGET_USER_ROWS)
        Tool.Set_Option('TARGET_USER_FLAT', TARGET_USER_FLAT)
        Tool.Set_Option('TARGET_USER_FITS', TARGET_USER_FITS)
        Tool.Set_Option('TARGET_SYSTEM', TARGET_SYSTEM)
        return Tool.Execute(Verbose)
    return False

def run_tool_grid_gridding_2(POINTS=None, TARGET_TEMPLATE=None, CV_SUMMARY=None, CV_RESIDUALS=None, TARGET_OUT_GRID=None, FIELD=None, CV_METHOD=None, CV_SAMPLES=None, TARGET_DEFINITION=None, TARGET_USER_SIZE=None, TARGET_USER_XMIN=None, TARGET_USER_XMAX=None, TARGET_USER_YMIN=None, TARGET_USER_YMAX=None, TARGET_USER_COLS=None, TARGET_USER_ROWS=None, TARGET_USER_FLAT=None, TARGET_USER_FITS=None, TARGET_SYSTEM=None, Verbose=2):
    '''
    Nearest Neighbour
    ----------
    [grid_gridding.2]\n
    Nearest Neighbour method for grid interpolation from irregular distributed points.\n
    Arguments
    ----------
    - POINTS [`input shapes`] : Points
    - TARGET_TEMPLATE [`optional input grid`] : Target System. use this grid's system for output grids
    - CV_SUMMARY [`output table`] : Cross Validation Summary
    - CV_RESIDUALS [`output shapes`] : Cross Validation Residuals
    - TARGET_OUT_GRID [`output grid`] : Target Grid
    - FIELD [`table field`] : Attribute
    - CV_METHOD [`choice`] : Cross Validation. Available Choices: [0] none [1] leave one out [2] 2-fold [3] k-fold Default: 0
    - CV_SAMPLES [`integer number`] : Cross Validation Subsamples. Minimum: 2 Default: 10 number of subsamples for k-fold cross validation
    - TARGET_DEFINITION [`choice`] : Target Grid System. Available Choices: [0] user defined [1] grid or grid system Default: 0
    - TARGET_USER_SIZE [`floating point number`] : Cellsize. Minimum: 0.000000 Default: 1.000000
    - TARGET_USER_XMIN [`floating point number`] : West. Default: 0.000000
    - TARGET_USER_XMAX [`floating point number`] : East. Default: 100.000000
    - TARGET_USER_YMIN [`floating point number`] : South. Default: 0.000000
    - TARGET_USER_YMAX [`floating point number`] : North. Default: 100.000000
    - TARGET_USER_COLS [`integer number`] : Columns. Minimum: 1 Default: 101 Number of cells in East-West direction.
    - TARGET_USER_ROWS [`integer number`] : Rows. Minimum: 1 Default: 101 Number of cells in North-South direction.
    - TARGET_USER_FLAT [`boolean`] : Rounding. Default: 1 Round bounding coordinates to multiples of cell size. Ignored if cell size has decimal places.
    - TARGET_USER_FITS [`choice`] : Fit. Available Choices: [0] nodes [1] cells Default: 0
    - TARGET_SYSTEM [`grid system`] : Grid System

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_gridding', '2', 'Nearest Neighbour')
    if Tool.is_Okay():
        Tool.Set_Input ('POINTS', POINTS)
        Tool.Set_Input ('TARGET_TEMPLATE', TARGET_TEMPLATE)
        Tool.Set_Output('CV_SUMMARY', CV_SUMMARY)
        Tool.Set_Output('CV_RESIDUALS', CV_RESIDUALS)
        Tool.Set_Output('TARGET_OUT_GRID', TARGET_OUT_GRID)
        Tool.Set_Option('FIELD', FIELD)
        Tool.Set_Option('CV_METHOD', CV_METHOD)
        Tool.Set_Option('CV_SAMPLES', CV_SAMPLES)
        Tool.Set_Option('TARGET_DEFINITION', TARGET_DEFINITION)
        Tool.Set_Option('TARGET_USER_SIZE', TARGET_USER_SIZE)
        Tool.Set_Option('TARGET_USER_XMIN', TARGET_USER_XMIN)
        Tool.Set_Option('TARGET_USER_XMAX', TARGET_USER_XMAX)
        Tool.Set_Option('TARGET_USER_YMIN', TARGET_USER_YMIN)
        Tool.Set_Option('TARGET_USER_YMAX', TARGET_USER_YMAX)
        Tool.Set_Option('TARGET_USER_COLS', TARGET_USER_COLS)
        Tool.Set_Option('TARGET_USER_ROWS', TARGET_USER_ROWS)
        Tool.Set_Option('TARGET_USER_FLAT', TARGET_USER_FLAT)
        Tool.Set_Option('TARGET_USER_FITS', TARGET_USER_FITS)
        Tool.Set_Option('TARGET_SYSTEM', TARGET_SYSTEM)
        return Tool.Execute(Verbose)
    return False

def Natural_Neighbour(POINTS=None, TARGET_TEMPLATE=None, TARGET_OUT_GRID=None, FIELD=None, TARGET_DEFINITION=None, TARGET_USER_SIZE=None, TARGET_USER_XMIN=None, TARGET_USER_XMAX=None, TARGET_USER_YMIN=None, TARGET_USER_YMAX=None, TARGET_USER_COLS=None, TARGET_USER_ROWS=None, TARGET_USER_FLAT=None, TARGET_USER_FITS=None, TARGET_SYSTEM=None, METHOD=None, WEIGHT=None, Verbose=2):
    '''
    Natural Neighbour
    ----------
    [grid_gridding.3]\n
    Natural Neighbour method for grid interpolation from irregular distributed points. This tool makes use of the 'nn - Natural Neighbours interpolation library' created and maintained by Pavel Sakov, CSIRO Marine Research.\n
    Arguments
    ----------
    - POINTS [`input shapes`] : Points
    - TARGET_TEMPLATE [`optional input grid`] : Target System. use this grid's system for output grids
    - TARGET_OUT_GRID [`output grid`] : Target Grid
    - FIELD [`table field`] : Attribute
    - TARGET_DEFINITION [`choice`] : Target Grid System. Available Choices: [0] user defined [1] grid or grid system Default: 0
    - TARGET_USER_SIZE [`floating point number`] : Cellsize. Minimum: 0.000000 Default: 1.000000
    - TARGET_USER_XMIN [`floating point number`] : West. Default: 0.000000
    - TARGET_USER_XMAX [`floating point number`] : East. Default: 100.000000
    - TARGET_USER_YMIN [`floating point number`] : South. Default: 0.000000
    - TARGET_USER_YMAX [`floating point number`] : North. Default: 100.000000
    - TARGET_USER_COLS [`integer number`] : Columns. Minimum: 1 Default: 101 Number of cells in East-West direction.
    - TARGET_USER_ROWS [`integer number`] : Rows. Minimum: 1 Default: 101 Number of cells in North-South direction.
    - TARGET_USER_FLAT [`boolean`] : Rounding. Default: 1 Round bounding coordinates to multiples of cell size. Ignored if cell size has decimal places.
    - TARGET_USER_FITS [`choice`] : Fit. Available Choices: [0] nodes [1] cells Default: 0
    - TARGET_SYSTEM [`grid system`] : Grid System
    - METHOD [`choice`] : Method. Available Choices: [0] Linear [1] Sibson [2] Non-Sibsonian Default: 1
    - WEIGHT [`floating point number`] : Minimum Weight. Maximum: 0.000000 Default: 0.000000 restricts extrapolation by assigning minimal allowed weight for a vertex (normally "-1" or so; lower values correspond to lower reliability; "0" means no extrapolation)

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_gridding', '3', 'Natural Neighbour')
    if Tool.is_Okay():
        Tool.Set_Input ('POINTS', POINTS)
        Tool.Set_Input ('TARGET_TEMPLATE', TARGET_TEMPLATE)
        Tool.Set_Output('TARGET_OUT_GRID', TARGET_OUT_GRID)
        Tool.Set_Option('FIELD', FIELD)
        Tool.Set_Option('TARGET_DEFINITION', TARGET_DEFINITION)
        Tool.Set_Option('TARGET_USER_SIZE', TARGET_USER_SIZE)
        Tool.Set_Option('TARGET_USER_XMIN', TARGET_USER_XMIN)
        Tool.Set_Option('TARGET_USER_XMAX', TARGET_USER_XMAX)
        Tool.Set_Option('TARGET_USER_YMIN', TARGET_USER_YMIN)
        Tool.Set_Option('TARGET_USER_YMAX', TARGET_USER_YMAX)
        Tool.Set_Option('TARGET_USER_COLS', TARGET_USER_COLS)
        Tool.Set_Option('TARGET_USER_ROWS', TARGET_USER_ROWS)
        Tool.Set_Option('TARGET_USER_FLAT', TARGET_USER_FLAT)
        Tool.Set_Option('TARGET_USER_FITS', TARGET_USER_FITS)
        Tool.Set_Option('TARGET_SYSTEM', TARGET_SYSTEM)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('WEIGHT', WEIGHT)
        return Tool.Execute(Verbose)
    return False

def run_tool_grid_gridding_3(POINTS=None, TARGET_TEMPLATE=None, TARGET_OUT_GRID=None, FIELD=None, TARGET_DEFINITION=None, TARGET_USER_SIZE=None, TARGET_USER_XMIN=None, TARGET_USER_XMAX=None, TARGET_USER_YMIN=None, TARGET_USER_YMAX=None, TARGET_USER_COLS=None, TARGET_USER_ROWS=None, TARGET_USER_FLAT=None, TARGET_USER_FITS=None, TARGET_SYSTEM=None, METHOD=None, WEIGHT=None, Verbose=2):
    '''
    Natural Neighbour
    ----------
    [grid_gridding.3]\n
    Natural Neighbour method for grid interpolation from irregular distributed points. This tool makes use of the 'nn - Natural Neighbours interpolation library' created and maintained by Pavel Sakov, CSIRO Marine Research.\n
    Arguments
    ----------
    - POINTS [`input shapes`] : Points
    - TARGET_TEMPLATE [`optional input grid`] : Target System. use this grid's system for output grids
    - TARGET_OUT_GRID [`output grid`] : Target Grid
    - FIELD [`table field`] : Attribute
    - TARGET_DEFINITION [`choice`] : Target Grid System. Available Choices: [0] user defined [1] grid or grid system Default: 0
    - TARGET_USER_SIZE [`floating point number`] : Cellsize. Minimum: 0.000000 Default: 1.000000
    - TARGET_USER_XMIN [`floating point number`] : West. Default: 0.000000
    - TARGET_USER_XMAX [`floating point number`] : East. Default: 100.000000
    - TARGET_USER_YMIN [`floating point number`] : South. Default: 0.000000
    - TARGET_USER_YMAX [`floating point number`] : North. Default: 100.000000
    - TARGET_USER_COLS [`integer number`] : Columns. Minimum: 1 Default: 101 Number of cells in East-West direction.
    - TARGET_USER_ROWS [`integer number`] : Rows. Minimum: 1 Default: 101 Number of cells in North-South direction.
    - TARGET_USER_FLAT [`boolean`] : Rounding. Default: 1 Round bounding coordinates to multiples of cell size. Ignored if cell size has decimal places.
    - TARGET_USER_FITS [`choice`] : Fit. Available Choices: [0] nodes [1] cells Default: 0
    - TARGET_SYSTEM [`grid system`] : Grid System
    - METHOD [`choice`] : Method. Available Choices: [0] Linear [1] Sibson [2] Non-Sibsonian Default: 1
    - WEIGHT [`floating point number`] : Minimum Weight. Maximum: 0.000000 Default: 0.000000 restricts extrapolation by assigning minimal allowed weight for a vertex (normally "-1" or so; lower values correspond to lower reliability; "0" means no extrapolation)

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_gridding', '3', 'Natural Neighbour')
    if Tool.is_Okay():
        Tool.Set_Input ('POINTS', POINTS)
        Tool.Set_Input ('TARGET_TEMPLATE', TARGET_TEMPLATE)
        Tool.Set_Output('TARGET_OUT_GRID', TARGET_OUT_GRID)
        Tool.Set_Option('FIELD', FIELD)
        Tool.Set_Option('TARGET_DEFINITION', TARGET_DEFINITION)
        Tool.Set_Option('TARGET_USER_SIZE', TARGET_USER_SIZE)
        Tool.Set_Option('TARGET_USER_XMIN', TARGET_USER_XMIN)
        Tool.Set_Option('TARGET_USER_XMAX', TARGET_USER_XMAX)
        Tool.Set_Option('TARGET_USER_YMIN', TARGET_USER_YMIN)
        Tool.Set_Option('TARGET_USER_YMAX', TARGET_USER_YMAX)
        Tool.Set_Option('TARGET_USER_COLS', TARGET_USER_COLS)
        Tool.Set_Option('TARGET_USER_ROWS', TARGET_USER_ROWS)
        Tool.Set_Option('TARGET_USER_FLAT', TARGET_USER_FLAT)
        Tool.Set_Option('TARGET_USER_FITS', TARGET_USER_FITS)
        Tool.Set_Option('TARGET_SYSTEM', TARGET_SYSTEM)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('WEIGHT', WEIGHT)
        return Tool.Execute(Verbose)
    return False

def Modified_Quadratic_Shepard(POINTS=None, TARGET_TEMPLATE=None, CV_SUMMARY=None, CV_RESIDUALS=None, TARGET_OUT_GRID=None, FIELD=None, CV_METHOD=None, CV_SAMPLES=None, TARGET_DEFINITION=None, TARGET_USER_SIZE=None, TARGET_USER_XMIN=None, TARGET_USER_XMAX=None, TARGET_USER_YMIN=None, TARGET_USER_YMAX=None, TARGET_USER_COLS=None, TARGET_USER_ROWS=None, TARGET_USER_FLAT=None, TARGET_USER_FITS=None, TARGET_SYSTEM=None, QUADRATIC_NEIGHBORS=None, WEIGHTING_NEIGHBORS=None, Verbose=2):
    '''
    Modified Quadratic Shepard
    ----------
    [grid_gridding.4]\n
    Modified  Quadratic Shepard method for grid interpolation from irregular distributed points. This tool is based on Tool 660 in TOMS.\n
    QSHEP2D: Fortran routines implementing the Quadratic Shepard method for bivariate interpolation of scattered data (see R. J. Renka, ACM TOMS 14 (1988) pp.149-150).\n
    Classes: E2b. Interpolation of scattered, non-gridded multivariate data.\n
    Arguments
    ----------
    - POINTS [`input shapes`] : Points
    - TARGET_TEMPLATE [`optional input grid`] : Target System. use this grid's system for output grids
    - CV_SUMMARY [`output table`] : Cross Validation Summary
    - CV_RESIDUALS [`output shapes`] : Cross Validation Residuals
    - TARGET_OUT_GRID [`output grid`] : Target Grid
    - FIELD [`table field`] : Attribute
    - CV_METHOD [`choice`] : Cross Validation. Available Choices: [0] none [1] leave one out [2] 2-fold [3] k-fold Default: 0
    - CV_SAMPLES [`integer number`] : Cross Validation Subsamples. Minimum: 2 Default: 10 number of subsamples for k-fold cross validation
    - TARGET_DEFINITION [`choice`] : Target Grid System. Available Choices: [0] user defined [1] grid or grid system Default: 0
    - TARGET_USER_SIZE [`floating point number`] : Cellsize. Minimum: 0.000000 Default: 1.000000
    - TARGET_USER_XMIN [`floating point number`] : West. Default: 0.000000
    - TARGET_USER_XMAX [`floating point number`] : East. Default: 100.000000
    - TARGET_USER_YMIN [`floating point number`] : South. Default: 0.000000
    - TARGET_USER_YMAX [`floating point number`] : North. Default: 100.000000
    - TARGET_USER_COLS [`integer number`] : Columns. Minimum: 1 Default: 101 Number of cells in East-West direction.
    - TARGET_USER_ROWS [`integer number`] : Rows. Minimum: 1 Default: 101 Number of cells in North-South direction.
    - TARGET_USER_FLAT [`boolean`] : Rounding. Default: 1 Round bounding coordinates to multiples of cell size. Ignored if cell size has decimal places.
    - TARGET_USER_FITS [`choice`] : Fit. Available Choices: [0] nodes [1] cells Default: 0
    - TARGET_SYSTEM [`grid system`] : Grid System
    - QUADRATIC_NEIGHBORS [`integer number`] : Quadratic Neighbors. Minimum: 5 Default: 13
    - WEIGHTING_NEIGHBORS [`integer number`] : Weighting Neighbors. Minimum: 3 Default: 19

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_gridding', '4', 'Modified Quadratic Shepard')
    if Tool.is_Okay():
        Tool.Set_Input ('POINTS', POINTS)
        Tool.Set_Input ('TARGET_TEMPLATE', TARGET_TEMPLATE)
        Tool.Set_Output('CV_SUMMARY', CV_SUMMARY)
        Tool.Set_Output('CV_RESIDUALS', CV_RESIDUALS)
        Tool.Set_Output('TARGET_OUT_GRID', TARGET_OUT_GRID)
        Tool.Set_Option('FIELD', FIELD)
        Tool.Set_Option('CV_METHOD', CV_METHOD)
        Tool.Set_Option('CV_SAMPLES', CV_SAMPLES)
        Tool.Set_Option('TARGET_DEFINITION', TARGET_DEFINITION)
        Tool.Set_Option('TARGET_USER_SIZE', TARGET_USER_SIZE)
        Tool.Set_Option('TARGET_USER_XMIN', TARGET_USER_XMIN)
        Tool.Set_Option('TARGET_USER_XMAX', TARGET_USER_XMAX)
        Tool.Set_Option('TARGET_USER_YMIN', TARGET_USER_YMIN)
        Tool.Set_Option('TARGET_USER_YMAX', TARGET_USER_YMAX)
        Tool.Set_Option('TARGET_USER_COLS', TARGET_USER_COLS)
        Tool.Set_Option('TARGET_USER_ROWS', TARGET_USER_ROWS)
        Tool.Set_Option('TARGET_USER_FLAT', TARGET_USER_FLAT)
        Tool.Set_Option('TARGET_USER_FITS', TARGET_USER_FITS)
        Tool.Set_Option('TARGET_SYSTEM', TARGET_SYSTEM)
        Tool.Set_Option('QUADRATIC_NEIGHBORS', QUADRATIC_NEIGHBORS)
        Tool.Set_Option('WEIGHTING_NEIGHBORS', WEIGHTING_NEIGHBORS)
        return Tool.Execute(Verbose)
    return False

def run_tool_grid_gridding_4(POINTS=None, TARGET_TEMPLATE=None, CV_SUMMARY=None, CV_RESIDUALS=None, TARGET_OUT_GRID=None, FIELD=None, CV_METHOD=None, CV_SAMPLES=None, TARGET_DEFINITION=None, TARGET_USER_SIZE=None, TARGET_USER_XMIN=None, TARGET_USER_XMAX=None, TARGET_USER_YMIN=None, TARGET_USER_YMAX=None, TARGET_USER_COLS=None, TARGET_USER_ROWS=None, TARGET_USER_FLAT=None, TARGET_USER_FITS=None, TARGET_SYSTEM=None, QUADRATIC_NEIGHBORS=None, WEIGHTING_NEIGHBORS=None, Verbose=2):
    '''
    Modified Quadratic Shepard
    ----------
    [grid_gridding.4]\n
    Modified  Quadratic Shepard method for grid interpolation from irregular distributed points. This tool is based on Tool 660 in TOMS.\n
    QSHEP2D: Fortran routines implementing the Quadratic Shepard method for bivariate interpolation of scattered data (see R. J. Renka, ACM TOMS 14 (1988) pp.149-150).\n
    Classes: E2b. Interpolation of scattered, non-gridded multivariate data.\n
    Arguments
    ----------
    - POINTS [`input shapes`] : Points
    - TARGET_TEMPLATE [`optional input grid`] : Target System. use this grid's system for output grids
    - CV_SUMMARY [`output table`] : Cross Validation Summary
    - CV_RESIDUALS [`output shapes`] : Cross Validation Residuals
    - TARGET_OUT_GRID [`output grid`] : Target Grid
    - FIELD [`table field`] : Attribute
    - CV_METHOD [`choice`] : Cross Validation. Available Choices: [0] none [1] leave one out [2] 2-fold [3] k-fold Default: 0
    - CV_SAMPLES [`integer number`] : Cross Validation Subsamples. Minimum: 2 Default: 10 number of subsamples for k-fold cross validation
    - TARGET_DEFINITION [`choice`] : Target Grid System. Available Choices: [0] user defined [1] grid or grid system Default: 0
    - TARGET_USER_SIZE [`floating point number`] : Cellsize. Minimum: 0.000000 Default: 1.000000
    - TARGET_USER_XMIN [`floating point number`] : West. Default: 0.000000
    - TARGET_USER_XMAX [`floating point number`] : East. Default: 100.000000
    - TARGET_USER_YMIN [`floating point number`] : South. Default: 0.000000
    - TARGET_USER_YMAX [`floating point number`] : North. Default: 100.000000
    - TARGET_USER_COLS [`integer number`] : Columns. Minimum: 1 Default: 101 Number of cells in East-West direction.
    - TARGET_USER_ROWS [`integer number`] : Rows. Minimum: 1 Default: 101 Number of cells in North-South direction.
    - TARGET_USER_FLAT [`boolean`] : Rounding. Default: 1 Round bounding coordinates to multiples of cell size. Ignored if cell size has decimal places.
    - TARGET_USER_FITS [`choice`] : Fit. Available Choices: [0] nodes [1] cells Default: 0
    - TARGET_SYSTEM [`grid system`] : Grid System
    - QUADRATIC_NEIGHBORS [`integer number`] : Quadratic Neighbors. Minimum: 5 Default: 13
    - WEIGHTING_NEIGHBORS [`integer number`] : Weighting Neighbors. Minimum: 3 Default: 19

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_gridding', '4', 'Modified Quadratic Shepard')
    if Tool.is_Okay():
        Tool.Set_Input ('POINTS', POINTS)
        Tool.Set_Input ('TARGET_TEMPLATE', TARGET_TEMPLATE)
        Tool.Set_Output('CV_SUMMARY', CV_SUMMARY)
        Tool.Set_Output('CV_RESIDUALS', CV_RESIDUALS)
        Tool.Set_Output('TARGET_OUT_GRID', TARGET_OUT_GRID)
        Tool.Set_Option('FIELD', FIELD)
        Tool.Set_Option('CV_METHOD', CV_METHOD)
        Tool.Set_Option('CV_SAMPLES', CV_SAMPLES)
        Tool.Set_Option('TARGET_DEFINITION', TARGET_DEFINITION)
        Tool.Set_Option('TARGET_USER_SIZE', TARGET_USER_SIZE)
        Tool.Set_Option('TARGET_USER_XMIN', TARGET_USER_XMIN)
        Tool.Set_Option('TARGET_USER_XMAX', TARGET_USER_XMAX)
        Tool.Set_Option('TARGET_USER_YMIN', TARGET_USER_YMIN)
        Tool.Set_Option('TARGET_USER_YMAX', TARGET_USER_YMAX)
        Tool.Set_Option('TARGET_USER_COLS', TARGET_USER_COLS)
        Tool.Set_Option('TARGET_USER_ROWS', TARGET_USER_ROWS)
        Tool.Set_Option('TARGET_USER_FLAT', TARGET_USER_FLAT)
        Tool.Set_Option('TARGET_USER_FITS', TARGET_USER_FITS)
        Tool.Set_Option('TARGET_SYSTEM', TARGET_SYSTEM)
        Tool.Set_Option('QUADRATIC_NEIGHBORS', QUADRATIC_NEIGHBORS)
        Tool.Set_Option('WEIGHTING_NEIGHBORS', WEIGHTING_NEIGHBORS)
        return Tool.Execute(Verbose)
    return False

def Triangulation(POINTS=None, TARGET_TEMPLATE=None, TARGET_OUT_GRID=None, FIELD=None, TARGET_DEFINITION=None, TARGET_USER_SIZE=None, TARGET_USER_XMIN=None, TARGET_USER_XMAX=None, TARGET_USER_YMIN=None, TARGET_USER_YMAX=None, TARGET_USER_COLS=None, TARGET_USER_ROWS=None, TARGET_USER_FLAT=None, TARGET_USER_FITS=None, TARGET_SYSTEM=None, FRAME=None, Verbose=2):
    '''
    Triangulation
    ----------
    [grid_gridding.5]\n
    Gridding of a shapes layer using Delaunay Triangulation.\n
    Arguments
    ----------
    - POINTS [`input shapes`] : Points
    - TARGET_TEMPLATE [`optional input grid`] : Target System. use this grid's system for output grids
    - TARGET_OUT_GRID [`output grid`] : Target Grid
    - FIELD [`table field`] : Attribute
    - TARGET_DEFINITION [`choice`] : Target Grid System. Available Choices: [0] user defined [1] grid or grid system Default: 0
    - TARGET_USER_SIZE [`floating point number`] : Cellsize. Minimum: 0.000000 Default: 1.000000
    - TARGET_USER_XMIN [`floating point number`] : West. Default: 0.000000
    - TARGET_USER_XMAX [`floating point number`] : East. Default: 100.000000
    - TARGET_USER_YMIN [`floating point number`] : South. Default: 0.000000
    - TARGET_USER_YMAX [`floating point number`] : North. Default: 100.000000
    - TARGET_USER_COLS [`integer number`] : Columns. Minimum: 1 Default: 101 Number of cells in East-West direction.
    - TARGET_USER_ROWS [`integer number`] : Rows. Minimum: 1 Default: 101 Number of cells in North-South direction.
    - TARGET_USER_FLAT [`boolean`] : Rounding. Default: 1 Round bounding coordinates to multiples of cell size. Ignored if cell size has decimal places.
    - TARGET_USER_FITS [`choice`] : Fit. Available Choices: [0] nodes [1] cells Default: 0
    - TARGET_SYSTEM [`grid system`] : Grid System
    - FRAME [`boolean`] : Add Frame. Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_gridding', '5', 'Triangulation')
    if Tool.is_Okay():
        Tool.Set_Input ('POINTS', POINTS)
        Tool.Set_Input ('TARGET_TEMPLATE', TARGET_TEMPLATE)
        Tool.Set_Output('TARGET_OUT_GRID', TARGET_OUT_GRID)
        Tool.Set_Option('FIELD', FIELD)
        Tool.Set_Option('TARGET_DEFINITION', TARGET_DEFINITION)
        Tool.Set_Option('TARGET_USER_SIZE', TARGET_USER_SIZE)
        Tool.Set_Option('TARGET_USER_XMIN', TARGET_USER_XMIN)
        Tool.Set_Option('TARGET_USER_XMAX', TARGET_USER_XMAX)
        Tool.Set_Option('TARGET_USER_YMIN', TARGET_USER_YMIN)
        Tool.Set_Option('TARGET_USER_YMAX', TARGET_USER_YMAX)
        Tool.Set_Option('TARGET_USER_COLS', TARGET_USER_COLS)
        Tool.Set_Option('TARGET_USER_ROWS', TARGET_USER_ROWS)
        Tool.Set_Option('TARGET_USER_FLAT', TARGET_USER_FLAT)
        Tool.Set_Option('TARGET_USER_FITS', TARGET_USER_FITS)
        Tool.Set_Option('TARGET_SYSTEM', TARGET_SYSTEM)
        Tool.Set_Option('FRAME', FRAME)
        return Tool.Execute(Verbose)
    return False

def run_tool_grid_gridding_5(POINTS=None, TARGET_TEMPLATE=None, TARGET_OUT_GRID=None, FIELD=None, TARGET_DEFINITION=None, TARGET_USER_SIZE=None, TARGET_USER_XMIN=None, TARGET_USER_XMAX=None, TARGET_USER_YMIN=None, TARGET_USER_YMAX=None, TARGET_USER_COLS=None, TARGET_USER_ROWS=None, TARGET_USER_FLAT=None, TARGET_USER_FITS=None, TARGET_SYSTEM=None, FRAME=None, Verbose=2):
    '''
    Triangulation
    ----------
    [grid_gridding.5]\n
    Gridding of a shapes layer using Delaunay Triangulation.\n
    Arguments
    ----------
    - POINTS [`input shapes`] : Points
    - TARGET_TEMPLATE [`optional input grid`] : Target System. use this grid's system for output grids
    - TARGET_OUT_GRID [`output grid`] : Target Grid
    - FIELD [`table field`] : Attribute
    - TARGET_DEFINITION [`choice`] : Target Grid System. Available Choices: [0] user defined [1] grid or grid system Default: 0
    - TARGET_USER_SIZE [`floating point number`] : Cellsize. Minimum: 0.000000 Default: 1.000000
    - TARGET_USER_XMIN [`floating point number`] : West. Default: 0.000000
    - TARGET_USER_XMAX [`floating point number`] : East. Default: 100.000000
    - TARGET_USER_YMIN [`floating point number`] : South. Default: 0.000000
    - TARGET_USER_YMAX [`floating point number`] : North. Default: 100.000000
    - TARGET_USER_COLS [`integer number`] : Columns. Minimum: 1 Default: 101 Number of cells in East-West direction.
    - TARGET_USER_ROWS [`integer number`] : Rows. Minimum: 1 Default: 101 Number of cells in North-South direction.
    - TARGET_USER_FLAT [`boolean`] : Rounding. Default: 1 Round bounding coordinates to multiples of cell size. Ignored if cell size has decimal places.
    - TARGET_USER_FITS [`choice`] : Fit. Available Choices: [0] nodes [1] cells Default: 0
    - TARGET_SYSTEM [`grid system`] : Grid System
    - FRAME [`boolean`] : Add Frame. Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_gridding', '5', 'Triangulation')
    if Tool.is_Okay():
        Tool.Set_Input ('POINTS', POINTS)
        Tool.Set_Input ('TARGET_TEMPLATE', TARGET_TEMPLATE)
        Tool.Set_Output('TARGET_OUT_GRID', TARGET_OUT_GRID)
        Tool.Set_Option('FIELD', FIELD)
        Tool.Set_Option('TARGET_DEFINITION', TARGET_DEFINITION)
        Tool.Set_Option('TARGET_USER_SIZE', TARGET_USER_SIZE)
        Tool.Set_Option('TARGET_USER_XMIN', TARGET_USER_XMIN)
        Tool.Set_Option('TARGET_USER_XMAX', TARGET_USER_XMAX)
        Tool.Set_Option('TARGET_USER_YMIN', TARGET_USER_YMIN)
        Tool.Set_Option('TARGET_USER_YMAX', TARGET_USER_YMAX)
        Tool.Set_Option('TARGET_USER_COLS', TARGET_USER_COLS)
        Tool.Set_Option('TARGET_USER_ROWS', TARGET_USER_ROWS)
        Tool.Set_Option('TARGET_USER_FLAT', TARGET_USER_FLAT)
        Tool.Set_Option('TARGET_USER_FITS', TARGET_USER_FITS)
        Tool.Set_Option('TARGET_SYSTEM', TARGET_SYSTEM)
        Tool.Set_Option('FRAME', FRAME)
        return Tool.Execute(Verbose)
    return False

def Kernel_Density_Estimation(POINTS=None, TARGET_TEMPLATE=None, TARGET_OUT_GRID=None, POPULATION=None, RADIUS=None, KERNEL=None, TARGET_DEFINITION=None, TARGET_USER_SIZE=None, TARGET_USER_XMIN=None, TARGET_USER_XMAX=None, TARGET_USER_YMIN=None, TARGET_USER_YMAX=None, TARGET_USER_COLS=None, TARGET_USER_ROWS=None, TARGET_USER_FLAT=None, TARGET_USER_FITS=None, TARGET_SYSTEM=None, Verbose=2):
    '''
    Kernel Density Estimation
    ----------
    [grid_gridding.6]\n
    Kernel density estimation. If any point is currently in selection only selected points are taken into account.\n
    Arguments
    ----------
    - POINTS [`input shapes`] : Points
    - TARGET_TEMPLATE [`optional input grid`] : Target System. use this grid's system for output grids
    - TARGET_OUT_GRID [`output grid`] : Target Grid
    - POPULATION [`table field`] : Population
    - RADIUS [`floating point number`] : Radius. Minimum: 0.000000 Default: 1.000000
    - KERNEL [`choice`] : Kernel. Available Choices: [0] quartic kernel [1] gaussian kernel Default: 0
    - TARGET_DEFINITION [`choice`] : Target Grid System. Available Choices: [0] user defined [1] grid or grid system Default: 0
    - TARGET_USER_SIZE [`floating point number`] : Cellsize. Minimum: 0.000000 Default: 1.000000
    - TARGET_USER_XMIN [`floating point number`] : West. Default: 0.000000
    - TARGET_USER_XMAX [`floating point number`] : East. Default: 100.000000
    - TARGET_USER_YMIN [`floating point number`] : South. Default: 0.000000
    - TARGET_USER_YMAX [`floating point number`] : North. Default: 100.000000
    - TARGET_USER_COLS [`integer number`] : Columns. Minimum: 1 Default: 101 Number of cells in East-West direction.
    - TARGET_USER_ROWS [`integer number`] : Rows. Minimum: 1 Default: 101 Number of cells in North-South direction.
    - TARGET_USER_FLAT [`boolean`] : Rounding. Default: 1 Round bounding coordinates to multiples of cell size. Ignored if cell size has decimal places.
    - TARGET_USER_FITS [`choice`] : Fit. Available Choices: [0] nodes [1] cells Default: 0
    - TARGET_SYSTEM [`grid system`] : Grid System

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_gridding', '6', 'Kernel Density Estimation')
    if Tool.is_Okay():
        Tool.Set_Input ('POINTS', POINTS)
        Tool.Set_Input ('TARGET_TEMPLATE', TARGET_TEMPLATE)
        Tool.Set_Output('TARGET_OUT_GRID', TARGET_OUT_GRID)
        Tool.Set_Option('POPULATION', POPULATION)
        Tool.Set_Option('RADIUS', RADIUS)
        Tool.Set_Option('KERNEL', KERNEL)
        Tool.Set_Option('TARGET_DEFINITION', TARGET_DEFINITION)
        Tool.Set_Option('TARGET_USER_SIZE', TARGET_USER_SIZE)
        Tool.Set_Option('TARGET_USER_XMIN', TARGET_USER_XMIN)
        Tool.Set_Option('TARGET_USER_XMAX', TARGET_USER_XMAX)
        Tool.Set_Option('TARGET_USER_YMIN', TARGET_USER_YMIN)
        Tool.Set_Option('TARGET_USER_YMAX', TARGET_USER_YMAX)
        Tool.Set_Option('TARGET_USER_COLS', TARGET_USER_COLS)
        Tool.Set_Option('TARGET_USER_ROWS', TARGET_USER_ROWS)
        Tool.Set_Option('TARGET_USER_FLAT', TARGET_USER_FLAT)
        Tool.Set_Option('TARGET_USER_FITS', TARGET_USER_FITS)
        Tool.Set_Option('TARGET_SYSTEM', TARGET_SYSTEM)
        return Tool.Execute(Verbose)
    return False

def run_tool_grid_gridding_6(POINTS=None, TARGET_TEMPLATE=None, TARGET_OUT_GRID=None, POPULATION=None, RADIUS=None, KERNEL=None, TARGET_DEFINITION=None, TARGET_USER_SIZE=None, TARGET_USER_XMIN=None, TARGET_USER_XMAX=None, TARGET_USER_YMIN=None, TARGET_USER_YMAX=None, TARGET_USER_COLS=None, TARGET_USER_ROWS=None, TARGET_USER_FLAT=None, TARGET_USER_FITS=None, TARGET_SYSTEM=None, Verbose=2):
    '''
    Kernel Density Estimation
    ----------
    [grid_gridding.6]\n
    Kernel density estimation. If any point is currently in selection only selected points are taken into account.\n
    Arguments
    ----------
    - POINTS [`input shapes`] : Points
    - TARGET_TEMPLATE [`optional input grid`] : Target System. use this grid's system for output grids
    - TARGET_OUT_GRID [`output grid`] : Target Grid
    - POPULATION [`table field`] : Population
    - RADIUS [`floating point number`] : Radius. Minimum: 0.000000 Default: 1.000000
    - KERNEL [`choice`] : Kernel. Available Choices: [0] quartic kernel [1] gaussian kernel Default: 0
    - TARGET_DEFINITION [`choice`] : Target Grid System. Available Choices: [0] user defined [1] grid or grid system Default: 0
    - TARGET_USER_SIZE [`floating point number`] : Cellsize. Minimum: 0.000000 Default: 1.000000
    - TARGET_USER_XMIN [`floating point number`] : West. Default: 0.000000
    - TARGET_USER_XMAX [`floating point number`] : East. Default: 100.000000
    - TARGET_USER_YMIN [`floating point number`] : South. Default: 0.000000
    - TARGET_USER_YMAX [`floating point number`] : North. Default: 100.000000
    - TARGET_USER_COLS [`integer number`] : Columns. Minimum: 1 Default: 101 Number of cells in East-West direction.
    - TARGET_USER_ROWS [`integer number`] : Rows. Minimum: 1 Default: 101 Number of cells in North-South direction.
    - TARGET_USER_FLAT [`boolean`] : Rounding. Default: 1 Round bounding coordinates to multiples of cell size. Ignored if cell size has decimal places.
    - TARGET_USER_FITS [`choice`] : Fit. Available Choices: [0] nodes [1] cells Default: 0
    - TARGET_SYSTEM [`grid system`] : Grid System

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_gridding', '6', 'Kernel Density Estimation')
    if Tool.is_Okay():
        Tool.Set_Input ('POINTS', POINTS)
        Tool.Set_Input ('TARGET_TEMPLATE', TARGET_TEMPLATE)
        Tool.Set_Output('TARGET_OUT_GRID', TARGET_OUT_GRID)
        Tool.Set_Option('POPULATION', POPULATION)
        Tool.Set_Option('RADIUS', RADIUS)
        Tool.Set_Option('KERNEL', KERNEL)
        Tool.Set_Option('TARGET_DEFINITION', TARGET_DEFINITION)
        Tool.Set_Option('TARGET_USER_SIZE', TARGET_USER_SIZE)
        Tool.Set_Option('TARGET_USER_XMIN', TARGET_USER_XMIN)
        Tool.Set_Option('TARGET_USER_XMAX', TARGET_USER_XMAX)
        Tool.Set_Option('TARGET_USER_YMIN', TARGET_USER_YMIN)
        Tool.Set_Option('TARGET_USER_YMAX', TARGET_USER_YMAX)
        Tool.Set_Option('TARGET_USER_COLS', TARGET_USER_COLS)
        Tool.Set_Option('TARGET_USER_ROWS', TARGET_USER_ROWS)
        Tool.Set_Option('TARGET_USER_FLAT', TARGET_USER_FLAT)
        Tool.Set_Option('TARGET_USER_FITS', TARGET_USER_FITS)
        Tool.Set_Option('TARGET_SYSTEM', TARGET_SYSTEM)
        return Tool.Execute(Verbose)
    return False

def Angular_Distance_Weighted(POINTS=None, TARGET_TEMPLATE=None, CV_SUMMARY=None, CV_RESIDUALS=None, TARGET_OUT_GRID=None, FIELD=None, CV_METHOD=None, CV_SAMPLES=None, TARGET_DEFINITION=None, TARGET_USER_SIZE=None, TARGET_USER_XMIN=None, TARGET_USER_XMAX=None, TARGET_USER_YMIN=None, TARGET_USER_YMAX=None, TARGET_USER_COLS=None, TARGET_USER_ROWS=None, TARGET_USER_FLAT=None, TARGET_USER_FITS=None, TARGET_SYSTEM=None, SEARCH_RANGE=None, SEARCH_RADIUS=None, SEARCH_POINTS_ALL=None, SEARCH_POINTS_MIN=None, SEARCH_POINTS_MAX=None, DW_WEIGHTING=None, DW_IDW_POWER=None, DW_BANDWIDTH=None, Verbose=2):
    '''
    Angular Distance Weighted
    ----------
    [grid_gridding.7]\n
    Angular Distance Weighted (ADW) grid interpolation from irregular distributed points.\n
    Arguments
    ----------
    - POINTS [`input shapes`] : Points
    - TARGET_TEMPLATE [`optional input grid`] : Target System. use this grid's system for output grids
    - CV_SUMMARY [`output table`] : Cross Validation Summary
    - CV_RESIDUALS [`output shapes`] : Cross Validation Residuals
    - TARGET_OUT_GRID [`output grid`] : Target Grid
    - FIELD [`table field`] : Attribute
    - CV_METHOD [`choice`] : Cross Validation. Available Choices: [0] none [1] leave one out [2] 2-fold [3] k-fold Default: 0
    - CV_SAMPLES [`integer number`] : Cross Validation Subsamples. Minimum: 2 Default: 10 number of subsamples for k-fold cross validation
    - TARGET_DEFINITION [`choice`] : Target Grid System. Available Choices: [0] user defined [1] grid or grid system Default: 0
    - TARGET_USER_SIZE [`floating point number`] : Cellsize. Minimum: 0.000000 Default: 1.000000
    - TARGET_USER_XMIN [`floating point number`] : West. Default: 0.000000
    - TARGET_USER_XMAX [`floating point number`] : East. Default: 100.000000
    - TARGET_USER_YMIN [`floating point number`] : South. Default: 0.000000
    - TARGET_USER_YMAX [`floating point number`] : North. Default: 100.000000
    - TARGET_USER_COLS [`integer number`] : Columns. Minimum: 1 Default: 101 Number of cells in East-West direction.
    - TARGET_USER_ROWS [`integer number`] : Rows. Minimum: 1 Default: 101 Number of cells in North-South direction.
    - TARGET_USER_FLAT [`boolean`] : Rounding. Default: 1 Round bounding coordinates to multiples of cell size. Ignored if cell size has decimal places.
    - TARGET_USER_FITS [`choice`] : Fit. Available Choices: [0] nodes [1] cells Default: 0
    - TARGET_SYSTEM [`grid system`] : Grid System
    - SEARCH_RANGE [`choice`] : Search Range. Available Choices: [0] local [1] global Default: 1
    - SEARCH_RADIUS [`floating point number`] : Maximum Search Distance. Minimum: 0.000000 Default: 1000.000000 local maximum search distance given in map units
    - SEARCH_POINTS_ALL [`choice`] : Number of Points. Available Choices: [0] maximum number of nearest points [1] all points within search distance Default: 1
    - SEARCH_POINTS_MIN [`integer number`] : Minimum. Minimum: 1 Default: 1 minimum number of points to use
    - SEARCH_POINTS_MAX [`integer number`] : Maximum. Minimum: 1 Default: 20 maximum number of nearest points
    - DW_WEIGHTING [`choice`] : Weighting Function. Available Choices: [0] no distance weighting [1] inverse distance to a power [2] exponential [3] gaussian Default: 1
    - DW_IDW_POWER [`floating point number`] : Power. Minimum: 0.000000 Default: 2.000000
    - DW_BANDWIDTH [`floating point number`] : Bandwidth. Minimum: 0.000000 Default: 1.000000 Bandwidth for exponential and Gaussian weighting

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_gridding', '7', 'Angular Distance Weighted')
    if Tool.is_Okay():
        Tool.Set_Input ('POINTS', POINTS)
        Tool.Set_Input ('TARGET_TEMPLATE', TARGET_TEMPLATE)
        Tool.Set_Output('CV_SUMMARY', CV_SUMMARY)
        Tool.Set_Output('CV_RESIDUALS', CV_RESIDUALS)
        Tool.Set_Output('TARGET_OUT_GRID', TARGET_OUT_GRID)
        Tool.Set_Option('FIELD', FIELD)
        Tool.Set_Option('CV_METHOD', CV_METHOD)
        Tool.Set_Option('CV_SAMPLES', CV_SAMPLES)
        Tool.Set_Option('TARGET_DEFINITION', TARGET_DEFINITION)
        Tool.Set_Option('TARGET_USER_SIZE', TARGET_USER_SIZE)
        Tool.Set_Option('TARGET_USER_XMIN', TARGET_USER_XMIN)
        Tool.Set_Option('TARGET_USER_XMAX', TARGET_USER_XMAX)
        Tool.Set_Option('TARGET_USER_YMIN', TARGET_USER_YMIN)
        Tool.Set_Option('TARGET_USER_YMAX', TARGET_USER_YMAX)
        Tool.Set_Option('TARGET_USER_COLS', TARGET_USER_COLS)
        Tool.Set_Option('TARGET_USER_ROWS', TARGET_USER_ROWS)
        Tool.Set_Option('TARGET_USER_FLAT', TARGET_USER_FLAT)
        Tool.Set_Option('TARGET_USER_FITS', TARGET_USER_FITS)
        Tool.Set_Option('TARGET_SYSTEM', TARGET_SYSTEM)
        Tool.Set_Option('SEARCH_RANGE', SEARCH_RANGE)
        Tool.Set_Option('SEARCH_RADIUS', SEARCH_RADIUS)
        Tool.Set_Option('SEARCH_POINTS_ALL', SEARCH_POINTS_ALL)
        Tool.Set_Option('SEARCH_POINTS_MIN', SEARCH_POINTS_MIN)
        Tool.Set_Option('SEARCH_POINTS_MAX', SEARCH_POINTS_MAX)
        Tool.Set_Option('DW_WEIGHTING', DW_WEIGHTING)
        Tool.Set_Option('DW_IDW_POWER', DW_IDW_POWER)
        Tool.Set_Option('DW_BANDWIDTH', DW_BANDWIDTH)
        return Tool.Execute(Verbose)
    return False

def run_tool_grid_gridding_7(POINTS=None, TARGET_TEMPLATE=None, CV_SUMMARY=None, CV_RESIDUALS=None, TARGET_OUT_GRID=None, FIELD=None, CV_METHOD=None, CV_SAMPLES=None, TARGET_DEFINITION=None, TARGET_USER_SIZE=None, TARGET_USER_XMIN=None, TARGET_USER_XMAX=None, TARGET_USER_YMIN=None, TARGET_USER_YMAX=None, TARGET_USER_COLS=None, TARGET_USER_ROWS=None, TARGET_USER_FLAT=None, TARGET_USER_FITS=None, TARGET_SYSTEM=None, SEARCH_RANGE=None, SEARCH_RADIUS=None, SEARCH_POINTS_ALL=None, SEARCH_POINTS_MIN=None, SEARCH_POINTS_MAX=None, DW_WEIGHTING=None, DW_IDW_POWER=None, DW_BANDWIDTH=None, Verbose=2):
    '''
    Angular Distance Weighted
    ----------
    [grid_gridding.7]\n
    Angular Distance Weighted (ADW) grid interpolation from irregular distributed points.\n
    Arguments
    ----------
    - POINTS [`input shapes`] : Points
    - TARGET_TEMPLATE [`optional input grid`] : Target System. use this grid's system for output grids
    - CV_SUMMARY [`output table`] : Cross Validation Summary
    - CV_RESIDUALS [`output shapes`] : Cross Validation Residuals
    - TARGET_OUT_GRID [`output grid`] : Target Grid
    - FIELD [`table field`] : Attribute
    - CV_METHOD [`choice`] : Cross Validation. Available Choices: [0] none [1] leave one out [2] 2-fold [3] k-fold Default: 0
    - CV_SAMPLES [`integer number`] : Cross Validation Subsamples. Minimum: 2 Default: 10 number of subsamples for k-fold cross validation
    - TARGET_DEFINITION [`choice`] : Target Grid System. Available Choices: [0] user defined [1] grid or grid system Default: 0
    - TARGET_USER_SIZE [`floating point number`] : Cellsize. Minimum: 0.000000 Default: 1.000000
    - TARGET_USER_XMIN [`floating point number`] : West. Default: 0.000000
    - TARGET_USER_XMAX [`floating point number`] : East. Default: 100.000000
    - TARGET_USER_YMIN [`floating point number`] : South. Default: 0.000000
    - TARGET_USER_YMAX [`floating point number`] : North. Default: 100.000000
    - TARGET_USER_COLS [`integer number`] : Columns. Minimum: 1 Default: 101 Number of cells in East-West direction.
    - TARGET_USER_ROWS [`integer number`] : Rows. Minimum: 1 Default: 101 Number of cells in North-South direction.
    - TARGET_USER_FLAT [`boolean`] : Rounding. Default: 1 Round bounding coordinates to multiples of cell size. Ignored if cell size has decimal places.
    - TARGET_USER_FITS [`choice`] : Fit. Available Choices: [0] nodes [1] cells Default: 0
    - TARGET_SYSTEM [`grid system`] : Grid System
    - SEARCH_RANGE [`choice`] : Search Range. Available Choices: [0] local [1] global Default: 1
    - SEARCH_RADIUS [`floating point number`] : Maximum Search Distance. Minimum: 0.000000 Default: 1000.000000 local maximum search distance given in map units
    - SEARCH_POINTS_ALL [`choice`] : Number of Points. Available Choices: [0] maximum number of nearest points [1] all points within search distance Default: 1
    - SEARCH_POINTS_MIN [`integer number`] : Minimum. Minimum: 1 Default: 1 minimum number of points to use
    - SEARCH_POINTS_MAX [`integer number`] : Maximum. Minimum: 1 Default: 20 maximum number of nearest points
    - DW_WEIGHTING [`choice`] : Weighting Function. Available Choices: [0] no distance weighting [1] inverse distance to a power [2] exponential [3] gaussian Default: 1
    - DW_IDW_POWER [`floating point number`] : Power. Minimum: 0.000000 Default: 2.000000
    - DW_BANDWIDTH [`floating point number`] : Bandwidth. Minimum: 0.000000 Default: 1.000000 Bandwidth for exponential and Gaussian weighting

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_gridding', '7', 'Angular Distance Weighted')
    if Tool.is_Okay():
        Tool.Set_Input ('POINTS', POINTS)
        Tool.Set_Input ('TARGET_TEMPLATE', TARGET_TEMPLATE)
        Tool.Set_Output('CV_SUMMARY', CV_SUMMARY)
        Tool.Set_Output('CV_RESIDUALS', CV_RESIDUALS)
        Tool.Set_Output('TARGET_OUT_GRID', TARGET_OUT_GRID)
        Tool.Set_Option('FIELD', FIELD)
        Tool.Set_Option('CV_METHOD', CV_METHOD)
        Tool.Set_Option('CV_SAMPLES', CV_SAMPLES)
        Tool.Set_Option('TARGET_DEFINITION', TARGET_DEFINITION)
        Tool.Set_Option('TARGET_USER_SIZE', TARGET_USER_SIZE)
        Tool.Set_Option('TARGET_USER_XMIN', TARGET_USER_XMIN)
        Tool.Set_Option('TARGET_USER_XMAX', TARGET_USER_XMAX)
        Tool.Set_Option('TARGET_USER_YMIN', TARGET_USER_YMIN)
        Tool.Set_Option('TARGET_USER_YMAX', TARGET_USER_YMAX)
        Tool.Set_Option('TARGET_USER_COLS', TARGET_USER_COLS)
        Tool.Set_Option('TARGET_USER_ROWS', TARGET_USER_ROWS)
        Tool.Set_Option('TARGET_USER_FLAT', TARGET_USER_FLAT)
        Tool.Set_Option('TARGET_USER_FITS', TARGET_USER_FITS)
        Tool.Set_Option('TARGET_SYSTEM', TARGET_SYSTEM)
        Tool.Set_Option('SEARCH_RANGE', SEARCH_RANGE)
        Tool.Set_Option('SEARCH_RADIUS', SEARCH_RADIUS)
        Tool.Set_Option('SEARCH_POINTS_ALL', SEARCH_POINTS_ALL)
        Tool.Set_Option('SEARCH_POINTS_MIN', SEARCH_POINTS_MIN)
        Tool.Set_Option('SEARCH_POINTS_MAX', SEARCH_POINTS_MAX)
        Tool.Set_Option('DW_WEIGHTING', DW_WEIGHTING)
        Tool.Set_Option('DW_IDW_POWER', DW_IDW_POWER)
        Tool.Set_Option('DW_BANDWIDTH', DW_BANDWIDTH)
        return Tool.Execute(Verbose)
    return False

def Grid_Cell_Area_Covered_by_Polygons(POLYGONS=None, TARGET_TEMPLATE=None, AREA=None, METHOD=None, OUTPUT=None, SELECTION=None, TARGET_DEFINITION=None, TARGET_USER_SIZE=None, TARGET_USER_XMIN=None, TARGET_USER_XMAX=None, TARGET_USER_YMIN=None, TARGET_USER_YMAX=None, TARGET_USER_COLS=None, TARGET_USER_ROWS=None, TARGET_USER_FLAT=None, TARGET_USER_FITS=None, TARGET_SYSTEM=None, Verbose=2):
    '''
    Grid Cell Area Covered by Polygons
    ----------
    [grid_gridding.8]\n
    This tool calculates for each grid cell of the selected grid system the area that is covered by the input polygons layer.\n
    Arguments
    ----------
    - POLYGONS [`input shapes`] : Polygons
    - TARGET_TEMPLATE [`optional input grid`] : Target System. use this grid's system for output grids
    - AREA [`output grid`] : Area of Coverage
    - METHOD [`choice`] : Method. Available Choices: [0] cell wise [1] polygon wise Default: 1 Choose cell wise, if you have not many polygons. Polygon wise will show much better performance, if you have many (small) polygons.
    - OUTPUT [`choice`] : Output. Available Choices: [0] area [1] percentage Default: 1
    - SELECTION [`boolean`] : Only Selected Polygons. Default: 1
    - TARGET_DEFINITION [`choice`] : Target Grid System. Available Choices: [0] user defined [1] grid or grid system Default: 0
    - TARGET_USER_SIZE [`floating point number`] : Cellsize. Minimum: 0.000000 Default: 1.000000
    - TARGET_USER_XMIN [`floating point number`] : West. Default: 0.000000
    - TARGET_USER_XMAX [`floating point number`] : East. Default: 100.000000
    - TARGET_USER_YMIN [`floating point number`] : South. Default: 0.000000
    - TARGET_USER_YMAX [`floating point number`] : North. Default: 100.000000
    - TARGET_USER_COLS [`integer number`] : Columns. Minimum: 1 Default: 101 Number of cells in East-West direction.
    - TARGET_USER_ROWS [`integer number`] : Rows. Minimum: 1 Default: 101 Number of cells in North-South direction.
    - TARGET_USER_FLAT [`boolean`] : Rounding. Default: 1 Round bounding coordinates to multiples of cell size. Ignored if cell size has decimal places.
    - TARGET_USER_FITS [`choice`] : Fit. Available Choices: [0] nodes [1] cells Default: 0
    - TARGET_SYSTEM [`grid system`] : Grid System

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_gridding', '8', 'Grid Cell Area Covered by Polygons')
    if Tool.is_Okay():
        Tool.Set_Input ('POLYGONS', POLYGONS)
        Tool.Set_Input ('TARGET_TEMPLATE', TARGET_TEMPLATE)
        Tool.Set_Output('AREA', AREA)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('OUTPUT', OUTPUT)
        Tool.Set_Option('SELECTION', SELECTION)
        Tool.Set_Option('TARGET_DEFINITION', TARGET_DEFINITION)
        Tool.Set_Option('TARGET_USER_SIZE', TARGET_USER_SIZE)
        Tool.Set_Option('TARGET_USER_XMIN', TARGET_USER_XMIN)
        Tool.Set_Option('TARGET_USER_XMAX', TARGET_USER_XMAX)
        Tool.Set_Option('TARGET_USER_YMIN', TARGET_USER_YMIN)
        Tool.Set_Option('TARGET_USER_YMAX', TARGET_USER_YMAX)
        Tool.Set_Option('TARGET_USER_COLS', TARGET_USER_COLS)
        Tool.Set_Option('TARGET_USER_ROWS', TARGET_USER_ROWS)
        Tool.Set_Option('TARGET_USER_FLAT', TARGET_USER_FLAT)
        Tool.Set_Option('TARGET_USER_FITS', TARGET_USER_FITS)
        Tool.Set_Option('TARGET_SYSTEM', TARGET_SYSTEM)
        return Tool.Execute(Verbose)
    return False

def run_tool_grid_gridding_8(POLYGONS=None, TARGET_TEMPLATE=None, AREA=None, METHOD=None, OUTPUT=None, SELECTION=None, TARGET_DEFINITION=None, TARGET_USER_SIZE=None, TARGET_USER_XMIN=None, TARGET_USER_XMAX=None, TARGET_USER_YMIN=None, TARGET_USER_YMAX=None, TARGET_USER_COLS=None, TARGET_USER_ROWS=None, TARGET_USER_FLAT=None, TARGET_USER_FITS=None, TARGET_SYSTEM=None, Verbose=2):
    '''
    Grid Cell Area Covered by Polygons
    ----------
    [grid_gridding.8]\n
    This tool calculates for each grid cell of the selected grid system the area that is covered by the input polygons layer.\n
    Arguments
    ----------
    - POLYGONS [`input shapes`] : Polygons
    - TARGET_TEMPLATE [`optional input grid`] : Target System. use this grid's system for output grids
    - AREA [`output grid`] : Area of Coverage
    - METHOD [`choice`] : Method. Available Choices: [0] cell wise [1] polygon wise Default: 1 Choose cell wise, if you have not many polygons. Polygon wise will show much better performance, if you have many (small) polygons.
    - OUTPUT [`choice`] : Output. Available Choices: [0] area [1] percentage Default: 1
    - SELECTION [`boolean`] : Only Selected Polygons. Default: 1
    - TARGET_DEFINITION [`choice`] : Target Grid System. Available Choices: [0] user defined [1] grid or grid system Default: 0
    - TARGET_USER_SIZE [`floating point number`] : Cellsize. Minimum: 0.000000 Default: 1.000000
    - TARGET_USER_XMIN [`floating point number`] : West. Default: 0.000000
    - TARGET_USER_XMAX [`floating point number`] : East. Default: 100.000000
    - TARGET_USER_YMIN [`floating point number`] : South. Default: 0.000000
    - TARGET_USER_YMAX [`floating point number`] : North. Default: 100.000000
    - TARGET_USER_COLS [`integer number`] : Columns. Minimum: 1 Default: 101 Number of cells in East-West direction.
    - TARGET_USER_ROWS [`integer number`] : Rows. Minimum: 1 Default: 101 Number of cells in North-South direction.
    - TARGET_USER_FLAT [`boolean`] : Rounding. Default: 1 Round bounding coordinates to multiples of cell size. Ignored if cell size has decimal places.
    - TARGET_USER_FITS [`choice`] : Fit. Available Choices: [0] nodes [1] cells Default: 0
    - TARGET_SYSTEM [`grid system`] : Grid System

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_gridding', '8', 'Grid Cell Area Covered by Polygons')
    if Tool.is_Okay():
        Tool.Set_Input ('POLYGONS', POLYGONS)
        Tool.Set_Input ('TARGET_TEMPLATE', TARGET_TEMPLATE)
        Tool.Set_Output('AREA', AREA)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('OUTPUT', OUTPUT)
        Tool.Set_Option('SELECTION', SELECTION)
        Tool.Set_Option('TARGET_DEFINITION', TARGET_DEFINITION)
        Tool.Set_Option('TARGET_USER_SIZE', TARGET_USER_SIZE)
        Tool.Set_Option('TARGET_USER_XMIN', TARGET_USER_XMIN)
        Tool.Set_Option('TARGET_USER_XMAX', TARGET_USER_XMAX)
        Tool.Set_Option('TARGET_USER_YMIN', TARGET_USER_YMIN)
        Tool.Set_Option('TARGET_USER_YMAX', TARGET_USER_YMAX)
        Tool.Set_Option('TARGET_USER_COLS', TARGET_USER_COLS)
        Tool.Set_Option('TARGET_USER_ROWS', TARGET_USER_ROWS)
        Tool.Set_Option('TARGET_USER_FLAT', TARGET_USER_FLAT)
        Tool.Set_Option('TARGET_USER_FITS', TARGET_USER_FITS)
        Tool.Set_Option('TARGET_SYSTEM', TARGET_SYSTEM)
        return Tool.Execute(Verbose)
    return False

def Polygons_to_Grid(POLYGONS=None, TARGET_TEMPLATE=None, GRID=None, COVERAGE=None, FIELD=None, OUTPUT=None, MULTIPLE=None, GRID_TYPE=None, TARGET_DEFINITION=None, TARGET_USER_SIZE=None, TARGET_USER_XMIN=None, TARGET_USER_XMAX=None, TARGET_USER_YMIN=None, TARGET_USER_YMAX=None, TARGET_USER_COLS=None, TARGET_USER_ROWS=None, TARGET_USER_FLAT=None, TARGET_USER_FITS=None, TARGET_SYSTEM=None, Verbose=2):
    '''
    Polygons to Grid
    ----------
    [grid_gridding.9]\n
    Gridding of polygons. If any polygons are selected, only these will be gridded.\n
    Arguments
    ----------
    - POLYGONS [`input shapes`] : Polygons
    - TARGET_TEMPLATE [`optional input grid`] : Target System. use this grid's system for output grids
    - GRID [`output grid`] : Grid
    - COVERAGE [`output grid`] : Coverage
    - FIELD [`table field`] : Attribute
    - OUTPUT [`choice`] : Output Values. Available Choices: [0] index number [1] attribute Default: 2
    - MULTIPLE [`choice`] : Multiple Polygons. Available Choices: [0] minimum coverage [1] maximum coverage [2] average proportional to area coverage Default: 1 Output value for cells that intersect with more than one polygon.
    - GRID_TYPE [`data type`] : Data Type. Available Choices: [0] bit [1] unsigned 1 byte integer [2] signed 1 byte integer [3] unsigned 2 byte integer [4] signed 2 byte integer [5] unsigned 4 byte integer [6] signed 4 byte integer [7] unsigned 8 byte integer [8] signed 8 byte integer [9] 4 byte floating point number [10] 8 byte floating point number [11] same as attribute Default: 11
    - TARGET_DEFINITION [`choice`] : Target Grid System. Available Choices: [0] user defined [1] grid or grid system Default: 0
    - TARGET_USER_SIZE [`floating point number`] : Cellsize. Minimum: 0.000000 Default: 1.000000
    - TARGET_USER_XMIN [`floating point number`] : West. Default: 0.000000
    - TARGET_USER_XMAX [`floating point number`] : East. Default: 100.000000
    - TARGET_USER_YMIN [`floating point number`] : South. Default: 0.000000
    - TARGET_USER_YMAX [`floating point number`] : North. Default: 100.000000
    - TARGET_USER_COLS [`integer number`] : Columns. Minimum: 1 Default: 101 Number of cells in East-West direction.
    - TARGET_USER_ROWS [`integer number`] : Rows. Minimum: 1 Default: 101 Number of cells in North-South direction.
    - TARGET_USER_FLAT [`boolean`] : Rounding. Default: 1 Round bounding coordinates to multiples of cell size. Ignored if cell size has decimal places.
    - TARGET_USER_FITS [`choice`] : Fit. Available Choices: [0] nodes [1] cells Default: 0
    - TARGET_SYSTEM [`grid system`] : Grid System

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_gridding', '9', 'Polygons to Grid')
    if Tool.is_Okay():
        Tool.Set_Input ('POLYGONS', POLYGONS)
        Tool.Set_Input ('TARGET_TEMPLATE', TARGET_TEMPLATE)
        Tool.Set_Output('GRID', GRID)
        Tool.Set_Output('COVERAGE', COVERAGE)
        Tool.Set_Option('FIELD', FIELD)
        Tool.Set_Option('OUTPUT', OUTPUT)
        Tool.Set_Option('MULTIPLE', MULTIPLE)
        Tool.Set_Option('GRID_TYPE', GRID_TYPE)
        Tool.Set_Option('TARGET_DEFINITION', TARGET_DEFINITION)
        Tool.Set_Option('TARGET_USER_SIZE', TARGET_USER_SIZE)
        Tool.Set_Option('TARGET_USER_XMIN', TARGET_USER_XMIN)
        Tool.Set_Option('TARGET_USER_XMAX', TARGET_USER_XMAX)
        Tool.Set_Option('TARGET_USER_YMIN', TARGET_USER_YMIN)
        Tool.Set_Option('TARGET_USER_YMAX', TARGET_USER_YMAX)
        Tool.Set_Option('TARGET_USER_COLS', TARGET_USER_COLS)
        Tool.Set_Option('TARGET_USER_ROWS', TARGET_USER_ROWS)
        Tool.Set_Option('TARGET_USER_FLAT', TARGET_USER_FLAT)
        Tool.Set_Option('TARGET_USER_FITS', TARGET_USER_FITS)
        Tool.Set_Option('TARGET_SYSTEM', TARGET_SYSTEM)
        return Tool.Execute(Verbose)
    return False

def run_tool_grid_gridding_9(POLYGONS=None, TARGET_TEMPLATE=None, GRID=None, COVERAGE=None, FIELD=None, OUTPUT=None, MULTIPLE=None, GRID_TYPE=None, TARGET_DEFINITION=None, TARGET_USER_SIZE=None, TARGET_USER_XMIN=None, TARGET_USER_XMAX=None, TARGET_USER_YMIN=None, TARGET_USER_YMAX=None, TARGET_USER_COLS=None, TARGET_USER_ROWS=None, TARGET_USER_FLAT=None, TARGET_USER_FITS=None, TARGET_SYSTEM=None, Verbose=2):
    '''
    Polygons to Grid
    ----------
    [grid_gridding.9]\n
    Gridding of polygons. If any polygons are selected, only these will be gridded.\n
    Arguments
    ----------
    - POLYGONS [`input shapes`] : Polygons
    - TARGET_TEMPLATE [`optional input grid`] : Target System. use this grid's system for output grids
    - GRID [`output grid`] : Grid
    - COVERAGE [`output grid`] : Coverage
    - FIELD [`table field`] : Attribute
    - OUTPUT [`choice`] : Output Values. Available Choices: [0] index number [1] attribute Default: 2
    - MULTIPLE [`choice`] : Multiple Polygons. Available Choices: [0] minimum coverage [1] maximum coverage [2] average proportional to area coverage Default: 1 Output value for cells that intersect with more than one polygon.
    - GRID_TYPE [`data type`] : Data Type. Available Choices: [0] bit [1] unsigned 1 byte integer [2] signed 1 byte integer [3] unsigned 2 byte integer [4] signed 2 byte integer [5] unsigned 4 byte integer [6] signed 4 byte integer [7] unsigned 8 byte integer [8] signed 8 byte integer [9] 4 byte floating point number [10] 8 byte floating point number [11] same as attribute Default: 11
    - TARGET_DEFINITION [`choice`] : Target Grid System. Available Choices: [0] user defined [1] grid or grid system Default: 0
    - TARGET_USER_SIZE [`floating point number`] : Cellsize. Minimum: 0.000000 Default: 1.000000
    - TARGET_USER_XMIN [`floating point number`] : West. Default: 0.000000
    - TARGET_USER_XMAX [`floating point number`] : East. Default: 100.000000
    - TARGET_USER_YMIN [`floating point number`] : South. Default: 0.000000
    - TARGET_USER_YMAX [`floating point number`] : North. Default: 100.000000
    - TARGET_USER_COLS [`integer number`] : Columns. Minimum: 1 Default: 101 Number of cells in East-West direction.
    - TARGET_USER_ROWS [`integer number`] : Rows. Minimum: 1 Default: 101 Number of cells in North-South direction.
    - TARGET_USER_FLAT [`boolean`] : Rounding. Default: 1 Round bounding coordinates to multiples of cell size. Ignored if cell size has decimal places.
    - TARGET_USER_FITS [`choice`] : Fit. Available Choices: [0] nodes [1] cells Default: 0
    - TARGET_SYSTEM [`grid system`] : Grid System

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_gridding', '9', 'Polygons to Grid')
    if Tool.is_Okay():
        Tool.Set_Input ('POLYGONS', POLYGONS)
        Tool.Set_Input ('TARGET_TEMPLATE', TARGET_TEMPLATE)
        Tool.Set_Output('GRID', GRID)
        Tool.Set_Output('COVERAGE', COVERAGE)
        Tool.Set_Option('FIELD', FIELD)
        Tool.Set_Option('OUTPUT', OUTPUT)
        Tool.Set_Option('MULTIPLE', MULTIPLE)
        Tool.Set_Option('GRID_TYPE', GRID_TYPE)
        Tool.Set_Option('TARGET_DEFINITION', TARGET_DEFINITION)
        Tool.Set_Option('TARGET_USER_SIZE', TARGET_USER_SIZE)
        Tool.Set_Option('TARGET_USER_XMIN', TARGET_USER_XMIN)
        Tool.Set_Option('TARGET_USER_XMAX', TARGET_USER_XMAX)
        Tool.Set_Option('TARGET_USER_YMIN', TARGET_USER_YMIN)
        Tool.Set_Option('TARGET_USER_YMAX', TARGET_USER_YMAX)
        Tool.Set_Option('TARGET_USER_COLS', TARGET_USER_COLS)
        Tool.Set_Option('TARGET_USER_ROWS', TARGET_USER_ROWS)
        Tool.Set_Option('TARGET_USER_FLAT', TARGET_USER_FLAT)
        Tool.Set_Option('TARGET_USER_FITS', TARGET_USER_FITS)
        Tool.Set_Option('TARGET_SYSTEM', TARGET_SYSTEM)
        return Tool.Execute(Verbose)
    return False

def Polygon_Categories_to_Grid(POLYGONS=None, TARGET_TEMPLATE=None, CLASSES=None, CATEGORY=None, COVERAGE=None, FIELD=None, METHOD=None, MULTIPLE=None, TARGET_DEFINITION=None, TARGET_USER_SIZE=None, TARGET_USER_XMIN=None, TARGET_USER_XMAX=None, TARGET_USER_YMIN=None, TARGET_USER_YMAX=None, TARGET_USER_COLS=None, TARGET_USER_ROWS=None, TARGET_USER_FLAT=None, TARGET_USER_FITS=None, TARGET_SYSTEM=None, Verbose=2):
    '''
    Polygon Categories to Grid
    ----------
    [grid_gridding.10]\n
    This tool has been designed to rasterize polygons representing categories and selects that category, which has maximum coverage of a cell. The advantage using this tool (instead the more simple 'Shapes to Grid' or 'Polygons to Grid' tools) is that it summarizes all polygon coverages belonging to the same category.\n
    Arguments
    ----------
    - POLYGONS [`input shapes`] : Polygons
    - TARGET_TEMPLATE [`optional input grid`] : Target System. use this grid's system for output grids
    - CLASSES [`output table`] : Classification. Classification look-up table for interpretation of resulting grid cell values.
    - CATEGORY [`output grid`] : Category
    - COVERAGE [`output grid`] : Coverage
    - FIELD [`table field`] : Category. The attribute field that specifies the category a polygon belongs to.
    - METHOD [`choice`] : Method. Available Choices: [0] cell wise [1] polygon wise Default: 1 Choose cell wise, if you have not many polygons. Polygon wise will show much better performance, if you have many (small) polygons.
    - MULTIPLE [`choice`] : Multiple Polygons. Available Choices: [0] minimum coverage [1] maximum coverage Default: 1 Output value for cells that intersect with more than one polygon.
    - TARGET_DEFINITION [`choice`] : Target Grid System. Available Choices: [0] user defined [1] grid or grid system Default: 0
    - TARGET_USER_SIZE [`floating point number`] : Cellsize. Minimum: 0.000000 Default: 1.000000
    - TARGET_USER_XMIN [`floating point number`] : West. Default: 0.000000
    - TARGET_USER_XMAX [`floating point number`] : East. Default: 100.000000
    - TARGET_USER_YMIN [`floating point number`] : South. Default: 0.000000
    - TARGET_USER_YMAX [`floating point number`] : North. Default: 100.000000
    - TARGET_USER_COLS [`integer number`] : Columns. Minimum: 1 Default: 101 Number of cells in East-West direction.
    - TARGET_USER_ROWS [`integer number`] : Rows. Minimum: 1 Default: 101 Number of cells in North-South direction.
    - TARGET_USER_FLAT [`boolean`] : Rounding. Default: 1 Round bounding coordinates to multiples of cell size. Ignored if cell size has decimal places.
    - TARGET_USER_FITS [`choice`] : Fit. Available Choices: [0] nodes [1] cells Default: 0
    - TARGET_SYSTEM [`grid system`] : Grid System

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_gridding', '10', 'Polygon Categories to Grid')
    if Tool.is_Okay():
        Tool.Set_Input ('POLYGONS', POLYGONS)
        Tool.Set_Input ('TARGET_TEMPLATE', TARGET_TEMPLATE)
        Tool.Set_Output('CLASSES', CLASSES)
        Tool.Set_Output('CATEGORY', CATEGORY)
        Tool.Set_Output('COVERAGE', COVERAGE)
        Tool.Set_Option('FIELD', FIELD)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('MULTIPLE', MULTIPLE)
        Tool.Set_Option('TARGET_DEFINITION', TARGET_DEFINITION)
        Tool.Set_Option('TARGET_USER_SIZE', TARGET_USER_SIZE)
        Tool.Set_Option('TARGET_USER_XMIN', TARGET_USER_XMIN)
        Tool.Set_Option('TARGET_USER_XMAX', TARGET_USER_XMAX)
        Tool.Set_Option('TARGET_USER_YMIN', TARGET_USER_YMIN)
        Tool.Set_Option('TARGET_USER_YMAX', TARGET_USER_YMAX)
        Tool.Set_Option('TARGET_USER_COLS', TARGET_USER_COLS)
        Tool.Set_Option('TARGET_USER_ROWS', TARGET_USER_ROWS)
        Tool.Set_Option('TARGET_USER_FLAT', TARGET_USER_FLAT)
        Tool.Set_Option('TARGET_USER_FITS', TARGET_USER_FITS)
        Tool.Set_Option('TARGET_SYSTEM', TARGET_SYSTEM)
        return Tool.Execute(Verbose)
    return False

def run_tool_grid_gridding_10(POLYGONS=None, TARGET_TEMPLATE=None, CLASSES=None, CATEGORY=None, COVERAGE=None, FIELD=None, METHOD=None, MULTIPLE=None, TARGET_DEFINITION=None, TARGET_USER_SIZE=None, TARGET_USER_XMIN=None, TARGET_USER_XMAX=None, TARGET_USER_YMIN=None, TARGET_USER_YMAX=None, TARGET_USER_COLS=None, TARGET_USER_ROWS=None, TARGET_USER_FLAT=None, TARGET_USER_FITS=None, TARGET_SYSTEM=None, Verbose=2):
    '''
    Polygon Categories to Grid
    ----------
    [grid_gridding.10]\n
    This tool has been designed to rasterize polygons representing categories and selects that category, which has maximum coverage of a cell. The advantage using this tool (instead the more simple 'Shapes to Grid' or 'Polygons to Grid' tools) is that it summarizes all polygon coverages belonging to the same category.\n
    Arguments
    ----------
    - POLYGONS [`input shapes`] : Polygons
    - TARGET_TEMPLATE [`optional input grid`] : Target System. use this grid's system for output grids
    - CLASSES [`output table`] : Classification. Classification look-up table for interpretation of resulting grid cell values.
    - CATEGORY [`output grid`] : Category
    - COVERAGE [`output grid`] : Coverage
    - FIELD [`table field`] : Category. The attribute field that specifies the category a polygon belongs to.
    - METHOD [`choice`] : Method. Available Choices: [0] cell wise [1] polygon wise Default: 1 Choose cell wise, if you have not many polygons. Polygon wise will show much better performance, if you have many (small) polygons.
    - MULTIPLE [`choice`] : Multiple Polygons. Available Choices: [0] minimum coverage [1] maximum coverage Default: 1 Output value for cells that intersect with more than one polygon.
    - TARGET_DEFINITION [`choice`] : Target Grid System. Available Choices: [0] user defined [1] grid or grid system Default: 0
    - TARGET_USER_SIZE [`floating point number`] : Cellsize. Minimum: 0.000000 Default: 1.000000
    - TARGET_USER_XMIN [`floating point number`] : West. Default: 0.000000
    - TARGET_USER_XMAX [`floating point number`] : East. Default: 100.000000
    - TARGET_USER_YMIN [`floating point number`] : South. Default: 0.000000
    - TARGET_USER_YMAX [`floating point number`] : North. Default: 100.000000
    - TARGET_USER_COLS [`integer number`] : Columns. Minimum: 1 Default: 101 Number of cells in East-West direction.
    - TARGET_USER_ROWS [`integer number`] : Rows. Minimum: 1 Default: 101 Number of cells in North-South direction.
    - TARGET_USER_FLAT [`boolean`] : Rounding. Default: 1 Round bounding coordinates to multiples of cell size. Ignored if cell size has decimal places.
    - TARGET_USER_FITS [`choice`] : Fit. Available Choices: [0] nodes [1] cells Default: 0
    - TARGET_SYSTEM [`grid system`] : Grid System

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_gridding', '10', 'Polygon Categories to Grid')
    if Tool.is_Okay():
        Tool.Set_Input ('POLYGONS', POLYGONS)
        Tool.Set_Input ('TARGET_TEMPLATE', TARGET_TEMPLATE)
        Tool.Set_Output('CLASSES', CLASSES)
        Tool.Set_Output('CATEGORY', CATEGORY)
        Tool.Set_Output('COVERAGE', COVERAGE)
        Tool.Set_Option('FIELD', FIELD)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('MULTIPLE', MULTIPLE)
        Tool.Set_Option('TARGET_DEFINITION', TARGET_DEFINITION)
        Tool.Set_Option('TARGET_USER_SIZE', TARGET_USER_SIZE)
        Tool.Set_Option('TARGET_USER_XMIN', TARGET_USER_XMIN)
        Tool.Set_Option('TARGET_USER_XMAX', TARGET_USER_XMAX)
        Tool.Set_Option('TARGET_USER_YMIN', TARGET_USER_YMIN)
        Tool.Set_Option('TARGET_USER_YMAX', TARGET_USER_YMAX)
        Tool.Set_Option('TARGET_USER_COLS', TARGET_USER_COLS)
        Tool.Set_Option('TARGET_USER_ROWS', TARGET_USER_ROWS)
        Tool.Set_Option('TARGET_USER_FLAT', TARGET_USER_FLAT)
        Tool.Set_Option('TARGET_USER_FITS', TARGET_USER_FITS)
        Tool.Set_Option('TARGET_SYSTEM', TARGET_SYSTEM)
        return Tool.Execute(Verbose)
    return False

