#! /usr/bin/env python

'''
Python Interface to SAGA Tools Library
----------
- Category : Shapes
- Name     : Tools
- ID       : shapes_tools

Description
----------
Tools for the manipulation of vector data.
'''

from PySAGA.helper import Tool_Wrapper

def Create_New_Shapes_Layer(SHAPES=None, NAME=None, TYPE=None, VERTEX=None, CRS_STRING=None, NFIELDS=None, FIELDS_NAME0=None, FIELDS_TYPE0=None, FIELDS_NAME1=None, FIELDS_TYPE1=None, Verbose=2):
    '''
    Create New Shapes Layer
    ----------
    [shapes_tools.0]\n
    Creates a new empty shapes layer of given type, which might be either point, multipoint, line or polygon.\n
    Possible field types for the attributes table are:\n
    (-) unsigned 1 byte integer\n
    (-) signed 1 byte integer\n
    (-) unsigned 2 byte integer\n
    (-) signed 2 byte integer\n
    (-) unsigned 4 byte integer\n
    (-) signed 4 byte integer\n
    (-) unsigned 8 byte integer\n
    (-) signed 8 byte integer\n
    (-) 4 byte floating point number\n
    (-) 8 byte floating point number\n
    (-) string\n
    (-) date\n
    (-) color\n
    (-) binary\n
    Arguments
    ----------
    - SHAPES [`output shapes`] : Shapes
    - NAME [`text`] : Name. Default: New Shapes Layer
    - TYPE [`choice`] : Geometry Type. Available Choices: [0] Point [1] Multipoint [2] Lines [3] Polygon Default: 0
    - VERTEX [`choice`] : Vertex Type. Available Choices: [0] x, y [1] x, y, z [2] x, y, z, m Default: 0
    - CRS_STRING [`text`] : Coordinate System Definition. Supported formats comprise PROJ and WKT strings, object codes (e.g. "EPSG:4326").
    - NFIELDS [`integer number`] : Number of Attributes. Minimum: 1 Default: 2
    - FIELDS_NAME0 [`text`] : Field 1. Default: Field 1 Name
    - FIELDS_TYPE0 [`data type`] : Type. Available Choices: [0] string [1] date [2] color [3] unsigned 1 byte integer [4] signed 1 byte integer [5] unsigned 2 byte integer [6] signed 2 byte integer [7] unsigned 4 byte integer [8] signed 4 byte integer [9] unsigned 8 byte integer [10] signed 8 byte integer [11] 4 byte floating point number [12] 8 byte floating point number [13] binary Default: 0 Type
    - FIELDS_NAME1 [`text`] : Field 2. Default: Field 2 Name
    - FIELDS_TYPE1 [`data type`] : Type. Available Choices: [0] string [1] date [2] color [3] unsigned 1 byte integer [4] signed 1 byte integer [5] unsigned 2 byte integer [6] signed 2 byte integer [7] unsigned 4 byte integer [8] signed 4 byte integer [9] unsigned 8 byte integer [10] signed 8 byte integer [11] 4 byte floating point number [12] 8 byte floating point number [13] binary Default: 0 Type

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_tools', '0', 'Create New Shapes Layer')
    if Tool.is_Okay():
        Tool.Set_Output('SHAPES', SHAPES)
        Tool.Set_Option('NAME', NAME)
        Tool.Set_Option('TYPE', TYPE)
        Tool.Set_Option('VERTEX', VERTEX)
        Tool.Set_Option('CRS_STRING', CRS_STRING)
        Tool.Set_Option('NFIELDS', NFIELDS)
        Tool.Set_Option('FIELDS.NAME0', FIELDS_NAME0)
        Tool.Set_Option('FIELDS.TYPE0', FIELDS_TYPE0)
        Tool.Set_Option('FIELDS.NAME1', FIELDS_NAME1)
        Tool.Set_Option('FIELDS.TYPE1', FIELDS_TYPE1)
        return Tool.Execute(Verbose)
    return False

def run_tool_shapes_tools_0(SHAPES=None, NAME=None, TYPE=None, VERTEX=None, CRS_STRING=None, NFIELDS=None, FIELDS_NAME0=None, FIELDS_TYPE0=None, FIELDS_NAME1=None, FIELDS_TYPE1=None, Verbose=2):
    '''
    Create New Shapes Layer
    ----------
    [shapes_tools.0]\n
    Creates a new empty shapes layer of given type, which might be either point, multipoint, line or polygon.\n
    Possible field types for the attributes table are:\n
    (-) unsigned 1 byte integer\n
    (-) signed 1 byte integer\n
    (-) unsigned 2 byte integer\n
    (-) signed 2 byte integer\n
    (-) unsigned 4 byte integer\n
    (-) signed 4 byte integer\n
    (-) unsigned 8 byte integer\n
    (-) signed 8 byte integer\n
    (-) 4 byte floating point number\n
    (-) 8 byte floating point number\n
    (-) string\n
    (-) date\n
    (-) color\n
    (-) binary\n
    Arguments
    ----------
    - SHAPES [`output shapes`] : Shapes
    - NAME [`text`] : Name. Default: New Shapes Layer
    - TYPE [`choice`] : Geometry Type. Available Choices: [0] Point [1] Multipoint [2] Lines [3] Polygon Default: 0
    - VERTEX [`choice`] : Vertex Type. Available Choices: [0] x, y [1] x, y, z [2] x, y, z, m Default: 0
    - CRS_STRING [`text`] : Coordinate System Definition. Supported formats comprise PROJ and WKT strings, object codes (e.g. "EPSG:4326").
    - NFIELDS [`integer number`] : Number of Attributes. Minimum: 1 Default: 2
    - FIELDS_NAME0 [`text`] : Field 1. Default: Field 1 Name
    - FIELDS_TYPE0 [`data type`] : Type. Available Choices: [0] string [1] date [2] color [3] unsigned 1 byte integer [4] signed 1 byte integer [5] unsigned 2 byte integer [6] signed 2 byte integer [7] unsigned 4 byte integer [8] signed 4 byte integer [9] unsigned 8 byte integer [10] signed 8 byte integer [11] 4 byte floating point number [12] 8 byte floating point number [13] binary Default: 0 Type
    - FIELDS_NAME1 [`text`] : Field 2. Default: Field 2 Name
    - FIELDS_TYPE1 [`data type`] : Type. Available Choices: [0] string [1] date [2] color [3] unsigned 1 byte integer [4] signed 1 byte integer [5] unsigned 2 byte integer [6] signed 2 byte integer [7] unsigned 4 byte integer [8] signed 4 byte integer [9] unsigned 8 byte integer [10] signed 8 byte integer [11] 4 byte floating point number [12] 8 byte floating point number [13] binary Default: 0 Type

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_tools', '0', 'Create New Shapes Layer')
    if Tool.is_Okay():
        Tool.Set_Output('SHAPES', SHAPES)
        Tool.Set_Option('NAME', NAME)
        Tool.Set_Option('TYPE', TYPE)
        Tool.Set_Option('VERTEX', VERTEX)
        Tool.Set_Option('CRS_STRING', CRS_STRING)
        Tool.Set_Option('NFIELDS', NFIELDS)
        Tool.Set_Option('FIELDS.NAME0', FIELDS_NAME0)
        Tool.Set_Option('FIELDS.TYPE0', FIELDS_TYPE0)
        Tool.Set_Option('FIELDS.NAME1', FIELDS_NAME1)
        Tool.Set_Option('FIELDS.TYPE1', FIELDS_TYPE1)
        return Tool.Execute(Verbose)
    return False

def Merge_Layers(INPUT=None, MERGED=None, SRCINFO=None, MATCH=None, DELETE=None, Verbose=2):
    '''
    Merge Layers
    ----------
    [shapes_tools.2]\n
    Merge vector layers.\n
    Arguments
    ----------
    - INPUT [`input shapes list`] : Layers. Output will inherit shape type and table structure from the first layer in this list.
    - MERGED [`output shapes`] : Merged Layer
    - SRCINFO [`boolean`] : Add Source Information. Default: 1 Adds a field with the name of the original input data set.
    - MATCH [`boolean`] : Match Fields by Name. Default: 1
    - DELETE [`boolean`] : Delete. Default: 0 Deletes each input data set immediately after it has been merged, thus saving memory resources.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_tools', '2', 'Merge Layers')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Output('MERGED', MERGED)
        Tool.Set_Option('SRCINFO', SRCINFO)
        Tool.Set_Option('MATCH', MATCH)
        Tool.Set_Option('DELETE', DELETE)
        return Tool.Execute(Verbose)
    return False

def run_tool_shapes_tools_2(INPUT=None, MERGED=None, SRCINFO=None, MATCH=None, DELETE=None, Verbose=2):
    '''
    Merge Layers
    ----------
    [shapes_tools.2]\n
    Merge vector layers.\n
    Arguments
    ----------
    - INPUT [`input shapes list`] : Layers. Output will inherit shape type and table structure from the first layer in this list.
    - MERGED [`output shapes`] : Merged Layer
    - SRCINFO [`boolean`] : Add Source Information. Default: 1 Adds a field with the name of the original input data set.
    - MATCH [`boolean`] : Match Fields by Name. Default: 1
    - DELETE [`boolean`] : Delete. Default: 0 Deletes each input data set immediately after it has been merged, thus saving memory resources.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_tools', '2', 'Merge Layers')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Output('MERGED', MERGED)
        Tool.Set_Option('SRCINFO', SRCINFO)
        Tool.Set_Option('MATCH', MATCH)
        Tool.Set_Option('DELETE', DELETE)
        return Tool.Execute(Verbose)
    return False

def Select_by_Attributes_Numerical_Expression(SHAPES=None, COPY=None, FIELD=None, EXPRESSION=None, INVERSE=None, USE_NODATA=None, METHOD=None, POSTJOB=None, Verbose=2):
    '''
    Select by Attributes... (Numerical Expression)
    ----------
    [shapes_tools.3]\n
    Selects records for which the expression evaluates to non-zero. The expression syntax is the same as the one for the table calculator. If an attribute field is selected, the expression evaluates only this attribute, which can be addressed with the letter 'x' in the expression formula. If no attribute is selected, attributes are addressed by the character 'f' (for 'field') followed by the field number (i.e.: f1, f2, ..., fn) or by the field in quota (e.g.: "Field Name").\n
    Examples:\n
    (-) f1 > f2\n
    (-) eq("Population" * 0.001, "Area")\n
    Arguments
    ----------
    - SHAPES [`input shapes`] : Shapes
    - COPY [`output shapes`] : Copy
    - FIELD [`table field`] : Attribute. attribute to be evaluated by expression ('x'); if not set all attributes can be addressed (f1, f2, ..., fn).
    - EXPRESSION [`text`] : Expression. Default: f1 > 0
    - INVERSE [`boolean`] : Inverse. Default: 0
    - USE_NODATA [`boolean`] : Use No-Data. Default: 0
    - METHOD [`choice`] : Method. Available Choices: [0] new selection [1] add to current selection [2] select from current selection [3] remove from current selection Default: 0
    - POSTJOB [`choice`] : Post Job. Available Choices: [0] none [1] copy [2] move [3] delete Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_tools', '3', 'Select by Attributes... (Numerical Expression)')
    if Tool.is_Okay():
        Tool.Set_Input ('SHAPES', SHAPES)
        Tool.Set_Output('COPY', COPY)
        Tool.Set_Option('FIELD', FIELD)
        Tool.Set_Option('EXPRESSION', EXPRESSION)
        Tool.Set_Option('INVERSE', INVERSE)
        Tool.Set_Option('USE_NODATA', USE_NODATA)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('POSTJOB', POSTJOB)
        return Tool.Execute(Verbose)
    return False

def run_tool_shapes_tools_3(SHAPES=None, COPY=None, FIELD=None, EXPRESSION=None, INVERSE=None, USE_NODATA=None, METHOD=None, POSTJOB=None, Verbose=2):
    '''
    Select by Attributes... (Numerical Expression)
    ----------
    [shapes_tools.3]\n
    Selects records for which the expression evaluates to non-zero. The expression syntax is the same as the one for the table calculator. If an attribute field is selected, the expression evaluates only this attribute, which can be addressed with the letter 'x' in the expression formula. If no attribute is selected, attributes are addressed by the character 'f' (for 'field') followed by the field number (i.e.: f1, f2, ..., fn) or by the field in quota (e.g.: "Field Name").\n
    Examples:\n
    (-) f1 > f2\n
    (-) eq("Population" * 0.001, "Area")\n
    Arguments
    ----------
    - SHAPES [`input shapes`] : Shapes
    - COPY [`output shapes`] : Copy
    - FIELD [`table field`] : Attribute. attribute to be evaluated by expression ('x'); if not set all attributes can be addressed (f1, f2, ..., fn).
    - EXPRESSION [`text`] : Expression. Default: f1 > 0
    - INVERSE [`boolean`] : Inverse. Default: 0
    - USE_NODATA [`boolean`] : Use No-Data. Default: 0
    - METHOD [`choice`] : Method. Available Choices: [0] new selection [1] add to current selection [2] select from current selection [3] remove from current selection Default: 0
    - POSTJOB [`choice`] : Post Job. Available Choices: [0] none [1] copy [2] move [3] delete Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_tools', '3', 'Select by Attributes... (Numerical Expression)')
    if Tool.is_Okay():
        Tool.Set_Input ('SHAPES', SHAPES)
        Tool.Set_Output('COPY', COPY)
        Tool.Set_Option('FIELD', FIELD)
        Tool.Set_Option('EXPRESSION', EXPRESSION)
        Tool.Set_Option('INVERSE', INVERSE)
        Tool.Set_Option('USE_NODATA', USE_NODATA)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('POSTJOB', POSTJOB)
        return Tool.Execute(Verbose)
    return False

def Select_by_Attributes_String_Expression(SHAPES=None, COPY=None, FIELD=None, EXPRESSION=None, CASE=None, COMPARE=None, INVERSE=None, METHOD=None, POSTJOB=None, Verbose=2):
    '''
    Select by Attributes... (String Expression)
    ----------
    [shapes_tools.4]\n
    Searches for an character string expression in the attributes table and selects records where the expression is found.\n
    Arguments
    ----------
    - SHAPES [`input shapes`] : Shapes
    - COPY [`output shapes`] : Copy
    - FIELD [`table field`] : Attribute. attribute to be searched; if not set all attributes will be searched
    - EXPRESSION [`text`] : Expression
    - CASE [`boolean`] : Case Sensitive. Default: 1
    - COMPARE [`choice`] : Select if.... Available Choices: [0] attribute is identical with search expression [1] attribute contains search expression [2] attribute is contained in search expression Default: 1
    - INVERSE [`boolean`] : Inverse. Default: 0
    - METHOD [`choice`] : Method. Available Choices: [0] new selection [1] add to current selection [2] select from current selection [3] remove from current selection Default: 0
    - POSTJOB [`choice`] : Post Job. Available Choices: [0] none [1] copy [2] move [3] delete Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_tools', '4', 'Select by Attributes... (String Expression)')
    if Tool.is_Okay():
        Tool.Set_Input ('SHAPES', SHAPES)
        Tool.Set_Output('COPY', COPY)
        Tool.Set_Option('FIELD', FIELD)
        Tool.Set_Option('EXPRESSION', EXPRESSION)
        Tool.Set_Option('CASE', CASE)
        Tool.Set_Option('COMPARE', COMPARE)
        Tool.Set_Option('INVERSE', INVERSE)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('POSTJOB', POSTJOB)
        return Tool.Execute(Verbose)
    return False

def run_tool_shapes_tools_4(SHAPES=None, COPY=None, FIELD=None, EXPRESSION=None, CASE=None, COMPARE=None, INVERSE=None, METHOD=None, POSTJOB=None, Verbose=2):
    '''
    Select by Attributes... (String Expression)
    ----------
    [shapes_tools.4]\n
    Searches for an character string expression in the attributes table and selects records where the expression is found.\n
    Arguments
    ----------
    - SHAPES [`input shapes`] : Shapes
    - COPY [`output shapes`] : Copy
    - FIELD [`table field`] : Attribute. attribute to be searched; if not set all attributes will be searched
    - EXPRESSION [`text`] : Expression
    - CASE [`boolean`] : Case Sensitive. Default: 1
    - COMPARE [`choice`] : Select if.... Available Choices: [0] attribute is identical with search expression [1] attribute contains search expression [2] attribute is contained in search expression Default: 1
    - INVERSE [`boolean`] : Inverse. Default: 0
    - METHOD [`choice`] : Method. Available Choices: [0] new selection [1] add to current selection [2] select from current selection [3] remove from current selection Default: 0
    - POSTJOB [`choice`] : Post Job. Available Choices: [0] none [1] copy [2] move [3] delete Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_tools', '4', 'Select by Attributes... (String Expression)')
    if Tool.is_Okay():
        Tool.Set_Input ('SHAPES', SHAPES)
        Tool.Set_Output('COPY', COPY)
        Tool.Set_Option('FIELD', FIELD)
        Tool.Set_Option('EXPRESSION', EXPRESSION)
        Tool.Set_Option('CASE', CASE)
        Tool.Set_Option('COMPARE', COMPARE)
        Tool.Set_Option('INVERSE', INVERSE)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('POSTJOB', POSTJOB)
        return Tool.Execute(Verbose)
    return False

def Select_by_Location(SHAPES=None, LOCATIONS=None, CONDITION=None, METHOD=None, Verbose=2):
    '''
    Select by Location...
    ----------
    [shapes_tools.5]\n
    Select by location.\n
    Arguments
    ----------
    - SHAPES [`input shapes`] : Shapes to Select From
    - LOCATIONS [`input shapes`] : Locations
    - CONDITION [`choice`] : Condition. Available Choices: [0] intersect [1] are completely within [2] completely contain [3] have their centroid in [4] contain the centeroid of Default: 0 Select shapes that fulfil this condition
    - METHOD [`choice`] : Method. Available Choices: [0] new selection [1] add to current selection [2] select from current selection [3] remove from current selection Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_tools', '5', 'Select by Location...')
    if Tool.is_Okay():
        Tool.Set_Input ('SHAPES', SHAPES)
        Tool.Set_Input ('LOCATIONS', LOCATIONS)
        Tool.Set_Option('CONDITION', CONDITION)
        Tool.Set_Option('METHOD', METHOD)
        return Tool.Execute(Verbose)
    return False

def run_tool_shapes_tools_5(SHAPES=None, LOCATIONS=None, CONDITION=None, METHOD=None, Verbose=2):
    '''
    Select by Location...
    ----------
    [shapes_tools.5]\n
    Select by location.\n
    Arguments
    ----------
    - SHAPES [`input shapes`] : Shapes to Select From
    - LOCATIONS [`input shapes`] : Locations
    - CONDITION [`choice`] : Condition. Available Choices: [0] intersect [1] are completely within [2] completely contain [3] have their centroid in [4] contain the centeroid of Default: 0 Select shapes that fulfil this condition
    - METHOD [`choice`] : Method. Available Choices: [0] new selection [1] add to current selection [2] select from current selection [3] remove from current selection Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_tools', '5', 'Select by Location...')
    if Tool.is_Okay():
        Tool.Set_Input ('SHAPES', SHAPES)
        Tool.Set_Input ('LOCATIONS', LOCATIONS)
        Tool.Set_Option('CONDITION', CONDITION)
        Tool.Set_Option('METHOD', METHOD)
        return Tool.Execute(Verbose)
    return False

def Copy_Selection_to_New_Shapes_Layer(INPUT=None, OUTPUT=None, Verbose=2):
    '''
    Copy Selection to New Shapes Layer
    ----------
    [shapes_tools.6]\n
    Copies selected shapes to a new shapes layer.\n
    Arguments
    ----------
    - INPUT [`input shapes`] : Input
    - OUTPUT [`output shapes`] : Output

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_tools', '6', 'Copy Selection to New Shapes Layer')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Output('OUTPUT', OUTPUT)
        return Tool.Execute(Verbose)
    return False

def run_tool_shapes_tools_6(INPUT=None, OUTPUT=None, Verbose=2):
    '''
    Copy Selection to New Shapes Layer
    ----------
    [shapes_tools.6]\n
    Copies selected shapes to a new shapes layer.\n
    Arguments
    ----------
    - INPUT [`input shapes`] : Input
    - OUTPUT [`output shapes`] : Output

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_tools', '6', 'Copy Selection to New Shapes Layer')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Output('OUTPUT', OUTPUT)
        return Tool.Execute(Verbose)
    return False

def Delete_Selection_from_Shapes_Layer(INPUT=None, Verbose=2):
    '''
    Delete Selection from Shapes Layer
    ----------
    [shapes_tools.7]\n
    Deletes selected shapes from shapes layer.\n
    Arguments
    ----------
    - INPUT [`input shapes`] : Input

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_tools', '7', 'Delete Selection from Shapes Layer')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        return Tool.Execute(Verbose)
    return False

def run_tool_shapes_tools_7(INPUT=None, Verbose=2):
    '''
    Delete Selection from Shapes Layer
    ----------
    [shapes_tools.7]\n
    Deletes selected shapes from shapes layer.\n
    Arguments
    ----------
    - INPUT [`input shapes`] : Input

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_tools', '7', 'Delete Selection from Shapes Layer')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        return Tool.Execute(Verbose)
    return False

def Invert_Selection_of_Shapes_Layer(INPUT=None, Verbose=2):
    '''
    Invert Selection of Shapes Layer
    ----------
    [shapes_tools.8]\n
    Deselects selected and selects unselected shapes of given shapes layer.\n
    Arguments
    ----------
    - INPUT [`input shapes`] : Input

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_tools', '8', 'Invert Selection of Shapes Layer')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        return Tool.Execute(Verbose)
    return False

def run_tool_shapes_tools_8(INPUT=None, Verbose=2):
    '''
    Invert Selection of Shapes Layer
    ----------
    [shapes_tools.8]\n
    Deselects selected and selects unselected shapes of given shapes layer.\n
    Arguments
    ----------
    - INPUT [`input shapes`] : Input

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_tools', '8', 'Invert Selection of Shapes Layer')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        return Tool.Execute(Verbose)
    return False

def Split_Shapes_Layer_Completely(SHAPES=None, LIST=None, FIELD=None, NAMING=None, Verbose=2):
    '''
    Split Shapes Layer Completely
    ----------
    [shapes_tools.9]\n
    Copies each shape of given layer to a separate target layer.\n
    Arguments
    ----------
    - SHAPES [`input shapes`] : Input
    - LIST [`output shapes list`] : Output
    - FIELD [`table field`] : Attribute
    - NAMING [`choice`] : Name by.... Available Choices: [0] number of order [1] attribute Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_tools', '9', 'Split Shapes Layer Completely')
    if Tool.is_Okay():
        Tool.Set_Input ('SHAPES', SHAPES)
        Tool.Set_Output('LIST', LIST)
        Tool.Set_Option('FIELD', FIELD)
        Tool.Set_Option('NAMING', NAMING)
        return Tool.Execute(Verbose)
    return False

def run_tool_shapes_tools_9(SHAPES=None, LIST=None, FIELD=None, NAMING=None, Verbose=2):
    '''
    Split Shapes Layer Completely
    ----------
    [shapes_tools.9]\n
    Copies each shape of given layer to a separate target layer.\n
    Arguments
    ----------
    - SHAPES [`input shapes`] : Input
    - LIST [`output shapes list`] : Output
    - FIELD [`table field`] : Attribute
    - NAMING [`choice`] : Name by.... Available Choices: [0] number of order [1] attribute Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_tools', '9', 'Split Shapes Layer Completely')
    if Tool.is_Okay():
        Tool.Set_Input ('SHAPES', SHAPES)
        Tool.Set_Output('LIST', LIST)
        Tool.Set_Option('FIELD', FIELD)
        Tool.Set_Option('NAMING', NAMING)
        return Tool.Execute(Verbose)
    return False

def Transform_Shapes(SHAPES=None, TRANSFORM=None, MOVEX=None, MOVEY=None, MOVEZ=None, ANCHORX=None, ANCHORY=None, ANCHORZ=None, ROTATEX=None, ROTATEY=None, ROTATEZ=None, SCALEX=None, SCALEY=None, SCALEZ=None, REFLECTION_TYPE=None, Verbose=2):
    '''
    Transform Shapes
    ----------
    [shapes_tools.10]\n
    The tool allows one to transform the input shapes by the following operations:\n
    - translation\n
    - rotation\n
    - scaling\n
    - reflection (mirroring)\n
    The operations are applied in the same order as listed above.\n
    Arguments
    ----------
    - SHAPES [`input shapes`] : Shapes. The input shapes to transform.
    - TRANSFORM [`output shapes`] : Transformed Shapes. The transformed output shapes.
    - MOVEX [`floating point number`] : dX. Default: 0.000000 The shift along the x-axis [map units].
    - MOVEY [`floating point number`] : dY. Default: 0.000000 The shift along the y-axis [map units].
    - MOVEZ [`floating point number`] : dZ. Default: 0.000000 The shift along the z-axis [map units].
    - ANCHORX [`floating point number`] : X. Default: 0.000000 The x-coordinate of the anchor point.
    - ANCHORY [`floating point number`] : Y. Default: 0.000000 The y-coordinate of the anchor point.
    - ANCHORZ [`floating point number`] : Z. Default: 0.000000 The z-coordinate of the anchor point.
    - ROTATEX [`floating point number`] : Angle X. Default: 0.000000 Angle in degrees, clockwise around x-axis.
    - ROTATEY [`floating point number`] : Angle Y. Default: 0.000000 Angle in degrees, clockwise around y-axis.
    - ROTATEZ [`floating point number`] : Angle Z. Default: 0.000000 Angle in degrees, clockwise around z-axis.
    - SCALEX [`floating point number`] : Scale Factor X. Default: 1.000000 The scale factor in x-direction.
    - SCALEY [`floating point number`] : Scale Factor Y. Default: 1.000000 The scale factor in y-direction.
    - SCALEZ [`floating point number`] : Scale Factor Z. Default: 1.000000 The scale factor in z-direction.
    - REFLECTION_TYPE [`choice`] : Type of Reflection. Available Choices: [0] no reflection [1] reflection relative to xz plane [2] reflection relative to yz plane [3] reflection relative to xy plane Default: 0 The type of reflection (mirroring) to apply.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_tools', '10', 'Transform Shapes')
    if Tool.is_Okay():
        Tool.Set_Input ('SHAPES', SHAPES)
        Tool.Set_Output('TRANSFORM', TRANSFORM)
        Tool.Set_Option('MOVEX', MOVEX)
        Tool.Set_Option('MOVEY', MOVEY)
        Tool.Set_Option('MOVEZ', MOVEZ)
        Tool.Set_Option('ANCHORX', ANCHORX)
        Tool.Set_Option('ANCHORY', ANCHORY)
        Tool.Set_Option('ANCHORZ', ANCHORZ)
        Tool.Set_Option('ROTATEX', ROTATEX)
        Tool.Set_Option('ROTATEY', ROTATEY)
        Tool.Set_Option('ROTATEZ', ROTATEZ)
        Tool.Set_Option('SCALEX', SCALEX)
        Tool.Set_Option('SCALEY', SCALEY)
        Tool.Set_Option('SCALEZ', SCALEZ)
        Tool.Set_Option('REFLECTION_TYPE', REFLECTION_TYPE)
        return Tool.Execute(Verbose)
    return False

def run_tool_shapes_tools_10(SHAPES=None, TRANSFORM=None, MOVEX=None, MOVEY=None, MOVEZ=None, ANCHORX=None, ANCHORY=None, ANCHORZ=None, ROTATEX=None, ROTATEY=None, ROTATEZ=None, SCALEX=None, SCALEY=None, SCALEZ=None, REFLECTION_TYPE=None, Verbose=2):
    '''
    Transform Shapes
    ----------
    [shapes_tools.10]\n
    The tool allows one to transform the input shapes by the following operations:\n
    - translation\n
    - rotation\n
    - scaling\n
    - reflection (mirroring)\n
    The operations are applied in the same order as listed above.\n
    Arguments
    ----------
    - SHAPES [`input shapes`] : Shapes. The input shapes to transform.
    - TRANSFORM [`output shapes`] : Transformed Shapes. The transformed output shapes.
    - MOVEX [`floating point number`] : dX. Default: 0.000000 The shift along the x-axis [map units].
    - MOVEY [`floating point number`] : dY. Default: 0.000000 The shift along the y-axis [map units].
    - MOVEZ [`floating point number`] : dZ. Default: 0.000000 The shift along the z-axis [map units].
    - ANCHORX [`floating point number`] : X. Default: 0.000000 The x-coordinate of the anchor point.
    - ANCHORY [`floating point number`] : Y. Default: 0.000000 The y-coordinate of the anchor point.
    - ANCHORZ [`floating point number`] : Z. Default: 0.000000 The z-coordinate of the anchor point.
    - ROTATEX [`floating point number`] : Angle X. Default: 0.000000 Angle in degrees, clockwise around x-axis.
    - ROTATEY [`floating point number`] : Angle Y. Default: 0.000000 Angle in degrees, clockwise around y-axis.
    - ROTATEZ [`floating point number`] : Angle Z. Default: 0.000000 Angle in degrees, clockwise around z-axis.
    - SCALEX [`floating point number`] : Scale Factor X. Default: 1.000000 The scale factor in x-direction.
    - SCALEY [`floating point number`] : Scale Factor Y. Default: 1.000000 The scale factor in y-direction.
    - SCALEZ [`floating point number`] : Scale Factor Z. Default: 1.000000 The scale factor in z-direction.
    - REFLECTION_TYPE [`choice`] : Type of Reflection. Available Choices: [0] no reflection [1] reflection relative to xz plane [2] reflection relative to yz plane [3] reflection relative to xy plane Default: 0 The type of reflection (mirroring) to apply.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_tools', '10', 'Transform Shapes')
    if Tool.is_Okay():
        Tool.Set_Input ('SHAPES', SHAPES)
        Tool.Set_Output('TRANSFORM', TRANSFORM)
        Tool.Set_Option('MOVEX', MOVEX)
        Tool.Set_Option('MOVEY', MOVEY)
        Tool.Set_Option('MOVEZ', MOVEZ)
        Tool.Set_Option('ANCHORX', ANCHORX)
        Tool.Set_Option('ANCHORY', ANCHORY)
        Tool.Set_Option('ANCHORZ', ANCHORZ)
        Tool.Set_Option('ROTATEX', ROTATEX)
        Tool.Set_Option('ROTATEY', ROTATEY)
        Tool.Set_Option('ROTATEZ', ROTATEZ)
        Tool.Set_Option('SCALEX', SCALEX)
        Tool.Set_Option('SCALEY', SCALEY)
        Tool.Set_Option('SCALEZ', SCALEZ)
        Tool.Set_Option('REFLECTION_TYPE', REFLECTION_TYPE)
        return Tool.Execute(Verbose)
    return False

def Create_Chart_Layer_BarsSectors(INPUT=None, OUTPUT=None, SIZE=None, FIELDS=None, MAXSIZE=None, MINSIZE=None, TYPE=None, Verbose=2):
    '''
    Create Chart Layer (Bars/Sectors)
    ----------
    [shapes_tools.11]\n
    (c) 2004 by Victor Olaya.\n
    Arguments
    ----------
    - INPUT [`input shapes`] : Shapes
    - OUTPUT [`output shapes`] : Chart
    - SIZE [`table field`] : Size
    - FIELDS [`table fields`] : Attributes
    - MAXSIZE [`floating point number`] : Maximum size. Minimum: 0.000000 Default: 100.000000
    - MINSIZE [`floating point number`] : Minimum size. Minimum: 0.000000 Default: 10.000000
    - TYPE [`choice`] : Type. Available Choices: [0] Sectors [1] Bars Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_tools', '11', 'Create Chart Layer (Bars/Sectors)')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Output('OUTPUT', OUTPUT)
        Tool.Set_Option('SIZE', SIZE)
        Tool.Set_Option('FIELDS', FIELDS)
        Tool.Set_Option('MAXSIZE', MAXSIZE)
        Tool.Set_Option('MINSIZE', MINSIZE)
        Tool.Set_Option('TYPE', TYPE)
        return Tool.Execute(Verbose)
    return False

def run_tool_shapes_tools_11(INPUT=None, OUTPUT=None, SIZE=None, FIELDS=None, MAXSIZE=None, MINSIZE=None, TYPE=None, Verbose=2):
    '''
    Create Chart Layer (Bars/Sectors)
    ----------
    [shapes_tools.11]\n
    (c) 2004 by Victor Olaya.\n
    Arguments
    ----------
    - INPUT [`input shapes`] : Shapes
    - OUTPUT [`output shapes`] : Chart
    - SIZE [`table field`] : Size
    - FIELDS [`table fields`] : Attributes
    - MAXSIZE [`floating point number`] : Maximum size. Minimum: 0.000000 Default: 100.000000
    - MINSIZE [`floating point number`] : Minimum size. Minimum: 0.000000 Default: 10.000000
    - TYPE [`choice`] : Type. Available Choices: [0] Sectors [1] Bars Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_tools', '11', 'Create Chart Layer (Bars/Sectors)')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Output('OUTPUT', OUTPUT)
        Tool.Set_Option('SIZE', SIZE)
        Tool.Set_Option('FIELDS', FIELDS)
        Tool.Set_Option('MAXSIZE', MAXSIZE)
        Tool.Set_Option('MINSIZE', MINSIZE)
        Tool.Set_Option('TYPE', TYPE)
        return Tool.Execute(Verbose)
    return False

def Create_Graticule(EXTENT=None, GRATICULE_LINE=None, GRATICULE_RECT=None, TYPE=None, EXTENT_X=None, EXTENT_Y=None, DIVISION_X=None, DIVISION_Y=None, ALIGNMENT=None, ROUND=None, Verbose=2):
    '''
    Create Graticule
    ----------
    [shapes_tools.12]\n
    The tool allows one to create a graticule with a user-specified width and height.\n
    Arguments
    ----------
    - EXTENT [`optional input shapes`] : Extent
    - GRATICULE_LINE [`output shapes`] : Graticule
    - GRATICULE_RECT [`output shapes`] : Graticule
    - TYPE [`choice`] : Type. Available Choices: [0] Lines [1] Rectangles Default: 0
    - EXTENT_X [`value range`] : Width
    - EXTENT_Y [`value range`] : Height
    - DIVISION_X [`floating point number`] : Division Width. Minimum: 0.000000 Default: 10.000000
    - DIVISION_Y [`floating point number`] : Division Height. Minimum: 0.000000 Default: 10.000000
    - ALIGNMENT [`choice`] : Alignment. Available Choices: [0] bottom-left [1] top-left [2] bottom-right [3] top-right [4] centered Default: 0 Determines how the graticule is aligned to the extent, if division sizes do not fit.
    - ROUND [`boolean`] : Round. Default: 0 Round bounding box coordinates to whole numbers; this blows up the bounding box.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_tools', '12', 'Create Graticule')
    if Tool.is_Okay():
        Tool.Set_Input ('EXTENT', EXTENT)
        Tool.Set_Output('GRATICULE_LINE', GRATICULE_LINE)
        Tool.Set_Output('GRATICULE_RECT', GRATICULE_RECT)
        Tool.Set_Option('TYPE', TYPE)
        Tool.Set_Option('EXTENT_X', EXTENT_X)
        Tool.Set_Option('EXTENT_Y', EXTENT_Y)
        Tool.Set_Option('DIVISION_X', DIVISION_X)
        Tool.Set_Option('DIVISION_Y', DIVISION_Y)
        Tool.Set_Option('ALIGNMENT', ALIGNMENT)
        Tool.Set_Option('ROUND', ROUND)
        return Tool.Execute(Verbose)
    return False

def run_tool_shapes_tools_12(EXTENT=None, GRATICULE_LINE=None, GRATICULE_RECT=None, TYPE=None, EXTENT_X=None, EXTENT_Y=None, DIVISION_X=None, DIVISION_Y=None, ALIGNMENT=None, ROUND=None, Verbose=2):
    '''
    Create Graticule
    ----------
    [shapes_tools.12]\n
    The tool allows one to create a graticule with a user-specified width and height.\n
    Arguments
    ----------
    - EXTENT [`optional input shapes`] : Extent
    - GRATICULE_LINE [`output shapes`] : Graticule
    - GRATICULE_RECT [`output shapes`] : Graticule
    - TYPE [`choice`] : Type. Available Choices: [0] Lines [1] Rectangles Default: 0
    - EXTENT_X [`value range`] : Width
    - EXTENT_Y [`value range`] : Height
    - DIVISION_X [`floating point number`] : Division Width. Minimum: 0.000000 Default: 10.000000
    - DIVISION_Y [`floating point number`] : Division Height. Minimum: 0.000000 Default: 10.000000
    - ALIGNMENT [`choice`] : Alignment. Available Choices: [0] bottom-left [1] top-left [2] bottom-right [3] top-right [4] centered Default: 0 Determines how the graticule is aligned to the extent, if division sizes do not fit.
    - ROUND [`boolean`] : Round. Default: 0 Round bounding box coordinates to whole numbers; this blows up the bounding box.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_tools', '12', 'Create Graticule')
    if Tool.is_Okay():
        Tool.Set_Input ('EXTENT', EXTENT)
        Tool.Set_Output('GRATICULE_LINE', GRATICULE_LINE)
        Tool.Set_Output('GRATICULE_RECT', GRATICULE_RECT)
        Tool.Set_Option('TYPE', TYPE)
        Tool.Set_Option('EXTENT_X', EXTENT_X)
        Tool.Set_Option('EXTENT_Y', EXTENT_Y)
        Tool.Set_Option('DIVISION_X', DIVISION_X)
        Tool.Set_Option('DIVISION_Y', DIVISION_Y)
        Tool.Set_Option('ALIGNMENT', ALIGNMENT)
        Tool.Set_Option('ROUND', ROUND)
        return Tool.Execute(Verbose)
    return False

def Copy_Shapes_from_Region(SHAPES=None, SHAPES_EXT=None, POLYGONS=None, CUT=None, METHOD=None, EXTENT=None, AX=None, BX=None, AY=None, BY=None, DX=None, DY=None, GRID_SYS=None, OVERLAP=None, Verbose=2):
    '''
    Copy Shapes from Region
    ----------
    [shapes_tools.13]\n
    Copies all shapes that belong to the specified region.\n
    Arguments
    ----------
    - SHAPES [`input shapes list`] : Shapes
    - SHAPES_EXT [`input shapes`] : Shapes
    - POLYGONS [`input shapes`] : Polygons
    - CUT [`output shapes list`] : Copy
    - METHOD [`choice`] : Method. Available Choices: [0] completely contained [1] intersects [2] center Default: 1
    - EXTENT [`choice`] : Region. Available Choices: [0] user defined [1] grid project [2] shapes layer extent [3] polygons Default: 0
    - AX [`floating point number`] : Left. Default: 0.000000
    - BX [`floating point number`] : Right. Default: 1.000000
    - AY [`floating point number`] : Bottom. Default: 0.000000
    - BY [`floating point number`] : Top. Default: 1.000000
    - DX [`floating point number`] : Horizontal Range. Minimum: 0.000000 Default: 1.000000
    - DY [`floating point number`] : Vertical Range. Minimum: 0.000000 Default: 1.000000
    - GRID_SYS [`grid system`] : Grid System
    - OVERLAP [`floating point number`] : Minimum Overlap. Minimum: 0.000000 Maximum: 100.000000 Default: 50.000000 minimum overlapping area as percentage of the total size of the input shape. applies to polygon layers only.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_tools', '13', 'Copy Shapes from Region')
    if Tool.is_Okay():
        Tool.Set_Input ('SHAPES', SHAPES)
        Tool.Set_Input ('SHAPES_EXT', SHAPES_EXT)
        Tool.Set_Input ('POLYGONS', POLYGONS)
        Tool.Set_Output('CUT', CUT)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('EXTENT', EXTENT)
        Tool.Set_Option('AX', AX)
        Tool.Set_Option('BX', BX)
        Tool.Set_Option('AY', AY)
        Tool.Set_Option('BY', BY)
        Tool.Set_Option('DX', DX)
        Tool.Set_Option('DY', DY)
        Tool.Set_Option('GRID_SYS', GRID_SYS)
        Tool.Set_Option('OVERLAP', OVERLAP)
        return Tool.Execute(Verbose)
    return False

def run_tool_shapes_tools_13(SHAPES=None, SHAPES_EXT=None, POLYGONS=None, CUT=None, METHOD=None, EXTENT=None, AX=None, BX=None, AY=None, BY=None, DX=None, DY=None, GRID_SYS=None, OVERLAP=None, Verbose=2):
    '''
    Copy Shapes from Region
    ----------
    [shapes_tools.13]\n
    Copies all shapes that belong to the specified region.\n
    Arguments
    ----------
    - SHAPES [`input shapes list`] : Shapes
    - SHAPES_EXT [`input shapes`] : Shapes
    - POLYGONS [`input shapes`] : Polygons
    - CUT [`output shapes list`] : Copy
    - METHOD [`choice`] : Method. Available Choices: [0] completely contained [1] intersects [2] center Default: 1
    - EXTENT [`choice`] : Region. Available Choices: [0] user defined [1] grid project [2] shapes layer extent [3] polygons Default: 0
    - AX [`floating point number`] : Left. Default: 0.000000
    - BX [`floating point number`] : Right. Default: 1.000000
    - AY [`floating point number`] : Bottom. Default: 0.000000
    - BY [`floating point number`] : Top. Default: 1.000000
    - DX [`floating point number`] : Horizontal Range. Minimum: 0.000000 Default: 1.000000
    - DY [`floating point number`] : Vertical Range. Minimum: 0.000000 Default: 1.000000
    - GRID_SYS [`grid system`] : Grid System
    - OVERLAP [`floating point number`] : Minimum Overlap. Minimum: 0.000000 Maximum: 100.000000 Default: 50.000000 minimum overlapping area as percentage of the total size of the input shape. applies to polygon layers only.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_tools', '13', 'Copy Shapes from Region')
    if Tool.is_Okay():
        Tool.Set_Input ('SHAPES', SHAPES)
        Tool.Set_Input ('SHAPES_EXT', SHAPES_EXT)
        Tool.Set_Input ('POLYGONS', POLYGONS)
        Tool.Set_Output('CUT', CUT)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('EXTENT', EXTENT)
        Tool.Set_Option('AX', AX)
        Tool.Set_Option('BX', BX)
        Tool.Set_Option('AY', AY)
        Tool.Set_Option('BY', BY)
        Tool.Set_Option('DX', DX)
        Tool.Set_Option('DY', DY)
        Tool.Set_Option('GRID_SYS', GRID_SYS)
        Tool.Set_Option('OVERLAP', OVERLAP)
        return Tool.Execute(Verbose)
    return False

def Split_Shapes_Layer(SHAPES=None, CUTS=None, EXTENT=None, NX=None, NY=None, METHOD=None, Verbose=2):
    '''
    Split Shapes Layer
    ----------
    [shapes_tools.15]\n
    Split Shapes Layer\n
    Arguments
    ----------
    - SHAPES [`input shapes`] : Shapes
    - CUTS [`output shapes list`] : Tiles
    - EXTENT [`output shapes`] : Extent
    - NX [`integer number`] : Number of horizontal tiles. Minimum: 1 Default: 2
    - NY [`integer number`] : Number of vertical tiles. Minimum: 1 Default: 2
    - METHOD [`choice`] : Method. Available Choices: [0] completely contained [1] intersects [2] center Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_tools', '15', 'Split Shapes Layer')
    if Tool.is_Okay():
        Tool.Set_Input ('SHAPES', SHAPES)
        Tool.Set_Output('CUTS', CUTS)
        Tool.Set_Output('EXTENT', EXTENT)
        Tool.Set_Option('NX', NX)
        Tool.Set_Option('NY', NY)
        Tool.Set_Option('METHOD', METHOD)
        return Tool.Execute(Verbose)
    return False

def run_tool_shapes_tools_15(SHAPES=None, CUTS=None, EXTENT=None, NX=None, NY=None, METHOD=None, Verbose=2):
    '''
    Split Shapes Layer
    ----------
    [shapes_tools.15]\n
    Split Shapes Layer\n
    Arguments
    ----------
    - SHAPES [`input shapes`] : Shapes
    - CUTS [`output shapes list`] : Tiles
    - EXTENT [`output shapes`] : Extent
    - NX [`integer number`] : Number of horizontal tiles. Minimum: 1 Default: 2
    - NY [`integer number`] : Number of vertical tiles. Minimum: 1 Default: 2
    - METHOD [`choice`] : Method. Available Choices: [0] completely contained [1] intersects [2] center Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_tools', '15', 'Split Shapes Layer')
    if Tool.is_Okay():
        Tool.Set_Input ('SHAPES', SHAPES)
        Tool.Set_Output('CUTS', CUTS)
        Tool.Set_Output('EXTENT', EXTENT)
        Tool.Set_Option('NX', NX)
        Tool.Set_Option('NY', NY)
        Tool.Set_Option('METHOD', METHOD)
        return Tool.Execute(Verbose)
    return False

def Split_Shapes_Layer_Randomly(SHAPES=None, A=None, B=None, FIELD=None, PERCENT=None, EXACT=None, Verbose=2):
    '''
    Split Shapes Layer Randomly
    ----------
    [shapes_tools.16]\n
    Randomly splits one layer into to two new layers. Useful to create a control group for model testing. Optionally this can be done category-wise if a category field is specified.\n
    Arguments
    ----------
    - SHAPES [`input shapes`] : Shapes
    - A [`output shapes`] : Group A
    - B [`output shapes`] : Group B
    - FIELD [`table field`] : Categories
    - PERCENT [`floating point number`] : Relation B / A. Minimum: 0.000000 Maximum: 100.000000 Default: 25.000000
    - EXACT [`boolean`] : Exact. Default: 1

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_tools', '16', 'Split Shapes Layer Randomly')
    if Tool.is_Okay():
        Tool.Set_Input ('SHAPES', SHAPES)
        Tool.Set_Output('A', A)
        Tool.Set_Output('B', B)
        Tool.Set_Option('FIELD', FIELD)
        Tool.Set_Option('PERCENT', PERCENT)
        Tool.Set_Option('EXACT', EXACT)
        return Tool.Execute(Verbose)
    return False

def run_tool_shapes_tools_16(SHAPES=None, A=None, B=None, FIELD=None, PERCENT=None, EXACT=None, Verbose=2):
    '''
    Split Shapes Layer Randomly
    ----------
    [shapes_tools.16]\n
    Randomly splits one layer into to two new layers. Useful to create a control group for model testing. Optionally this can be done category-wise if a category field is specified.\n
    Arguments
    ----------
    - SHAPES [`input shapes`] : Shapes
    - A [`output shapes`] : Group A
    - B [`output shapes`] : Group B
    - FIELD [`table field`] : Categories
    - PERCENT [`floating point number`] : Relation B / A. Minimum: 0.000000 Maximum: 100.000000 Default: 25.000000
    - EXACT [`boolean`] : Exact. Default: 1

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_tools', '16', 'Split Shapes Layer Randomly')
    if Tool.is_Okay():
        Tool.Set_Input ('SHAPES', SHAPES)
        Tool.Set_Output('A', A)
        Tool.Set_Output('B', B)
        Tool.Set_Option('FIELD', FIELD)
        Tool.Set_Option('PERCENT', PERCENT)
        Tool.Set_Option('EXACT', EXACT)
        return Tool.Execute(Verbose)
    return False

def Split_TableShapes_by_Attribute(TABLE=None, CUTS=None, FIELD=None, Verbose=2):
    '''
    Split Table/Shapes by Attribute
    ----------
    [shapes_tools.17]\n
    Split a table's records or a shapes layer's features attribute-wise.\n
    Arguments
    ----------
    - TABLE [`input table`] : Table / Shapes
    - CUTS [`output table list`] : Cuts
    - FIELD [`table field`] : Attribute

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_tools', '17', 'Split Table/Shapes by Attribute')
    if Tool.is_Okay():
        Tool.Set_Input ('TABLE', TABLE)
        Tool.Set_Output('CUTS', CUTS)
        Tool.Set_Option('FIELD', FIELD)
        return Tool.Execute(Verbose)
    return False

def run_tool_shapes_tools_17(TABLE=None, CUTS=None, FIELD=None, Verbose=2):
    '''
    Split Table/Shapes by Attribute
    ----------
    [shapes_tools.17]\n
    Split a table's records or a shapes layer's features attribute-wise.\n
    Arguments
    ----------
    - TABLE [`input table`] : Table / Shapes
    - CUTS [`output table list`] : Cuts
    - FIELD [`table field`] : Attribute

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_tools', '17', 'Split Table/Shapes by Attribute')
    if Tool.is_Okay():
        Tool.Set_Input ('TABLE', TABLE)
        Tool.Set_Output('CUTS', CUTS)
        Tool.Set_Option('FIELD', FIELD)
        return Tool.Execute(Verbose)
    return False

def Shapes_Buffer(SHAPES=None, BUFFER=None, DIST_FIELD=None, DIST_FIELD_DEFAULT=None, DIST_SCALE=None, DISSOLVE=None, NZONES=None, POLY_INNER=None, DARC=None, Verbose=2):
    '''
    Shapes Buffer
    ----------
    [shapes_tools.18]\n
    A vector based buffer construction partly based on the method supposed by Dong et al. (2003).\n
    Arguments
    ----------
    - SHAPES [`input shapes`] : Shapes
    - BUFFER [`output shapes`] : Buffer
    - DIST_FIELD [`table field`] : Buffer Distance
    - DIST_FIELD_DEFAULT [`floating point number`] : Default. Minimum: 0.000000 Default: 100.000000 default value if no attribute has been selected
    - DIST_SCALE [`floating point number`] : Scaling Factor for Attribute Value. Minimum: 0.000000 Default: 1.000000
    - DISSOLVE [`boolean`] : Dissolve Buffers. Default: 1
    - NZONES [`integer number`] : Number of Buffer Zones. Minimum: 1 Default: 1
    - POLY_INNER [`boolean`] : Inner Buffer. Default: 0
    - DARC [`floating point number`] : Arc Vertex Distance [Degree]. Minimum: 0.010000 Maximum: 45.000000 Default: 5.000000

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_tools', '18', 'Shapes Buffer')
    if Tool.is_Okay():
        Tool.Set_Input ('SHAPES', SHAPES)
        Tool.Set_Output('BUFFER', BUFFER)
        Tool.Set_Option('DIST_FIELD', DIST_FIELD)
        Tool.Set_Option('DIST_FIELD_DEFAULT', DIST_FIELD_DEFAULT)
        Tool.Set_Option('DIST_SCALE', DIST_SCALE)
        Tool.Set_Option('DISSOLVE', DISSOLVE)
        Tool.Set_Option('NZONES', NZONES)
        Tool.Set_Option('POLY_INNER', POLY_INNER)
        Tool.Set_Option('DARC', DARC)
        return Tool.Execute(Verbose)
    return False

def run_tool_shapes_tools_18(SHAPES=None, BUFFER=None, DIST_FIELD=None, DIST_FIELD_DEFAULT=None, DIST_SCALE=None, DISSOLVE=None, NZONES=None, POLY_INNER=None, DARC=None, Verbose=2):
    '''
    Shapes Buffer
    ----------
    [shapes_tools.18]\n
    A vector based buffer construction partly based on the method supposed by Dong et al. (2003).\n
    Arguments
    ----------
    - SHAPES [`input shapes`] : Shapes
    - BUFFER [`output shapes`] : Buffer
    - DIST_FIELD [`table field`] : Buffer Distance
    - DIST_FIELD_DEFAULT [`floating point number`] : Default. Minimum: 0.000000 Default: 100.000000 default value if no attribute has been selected
    - DIST_SCALE [`floating point number`] : Scaling Factor for Attribute Value. Minimum: 0.000000 Default: 1.000000
    - DISSOLVE [`boolean`] : Dissolve Buffers. Default: 1
    - NZONES [`integer number`] : Number of Buffer Zones. Minimum: 1 Default: 1
    - POLY_INNER [`boolean`] : Inner Buffer. Default: 0
    - DARC [`floating point number`] : Arc Vertex Distance [Degree]. Minimum: 0.010000 Maximum: 45.000000 Default: 5.000000

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_tools', '18', 'Shapes Buffer')
    if Tool.is_Okay():
        Tool.Set_Input ('SHAPES', SHAPES)
        Tool.Set_Output('BUFFER', BUFFER)
        Tool.Set_Option('DIST_FIELD', DIST_FIELD)
        Tool.Set_Option('DIST_FIELD_DEFAULT', DIST_FIELD_DEFAULT)
        Tool.Set_Option('DIST_SCALE', DIST_SCALE)
        Tool.Set_Option('DISSOLVE', DISSOLVE)
        Tool.Set_Option('NZONES', NZONES)
        Tool.Set_Option('POLY_INNER', POLY_INNER)
        Tool.Set_Option('DARC', DARC)
        return Tool.Execute(Verbose)
    return False

def Get_Shapes_Extents(SHAPES=None, EXTENTS=None, OUTPUT=None, Verbose=2):
    '''
    Get Shapes Extents
    ----------
    [shapes_tools.19]\n
    Get Shapes Extents\n
    Arguments
    ----------
    - SHAPES [`input shapes`] : Shapes
    - EXTENTS [`output shapes`] : Extents
    - OUTPUT [`choice`] : Get Extent for .... Available Choices: [0] all shapes [1] each shape [2] each shape's part Default: 1

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_tools', '19', 'Get Shapes Extents')
    if Tool.is_Okay():
        Tool.Set_Input ('SHAPES', SHAPES)
        Tool.Set_Output('EXTENTS', EXTENTS)
        Tool.Set_Option('OUTPUT', OUTPUT)
        return Tool.Execute(Verbose)
    return False

def run_tool_shapes_tools_19(SHAPES=None, EXTENTS=None, OUTPUT=None, Verbose=2):
    '''
    Get Shapes Extents
    ----------
    [shapes_tools.19]\n
    Get Shapes Extents\n
    Arguments
    ----------
    - SHAPES [`input shapes`] : Shapes
    - EXTENTS [`output shapes`] : Extents
    - OUTPUT [`choice`] : Get Extent for .... Available Choices: [0] all shapes [1] each shape [2] each shape's part Default: 1

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_tools', '19', 'Get Shapes Extents')
    if Tool.is_Okay():
        Tool.Set_Input ('SHAPES', SHAPES)
        Tool.Set_Output('EXTENTS', EXTENTS)
        Tool.Set_Option('OUTPUT', OUTPUT)
        return Tool.Execute(Verbose)
    return False

def QuadTree_Structure_to_Shapes(SHAPES=None, POLYGONS=None, LINES=None, POINTS=None, ATTRIBUTE=None, Verbose=2):
    '''
    QuadTree Structure to Shapes
    ----------
    [shapes_tools.20]\n
    QuadTree Structure to Shapes\n
    Arguments
    ----------
    - SHAPES [`input shapes`] : Shapes
    - POLYGONS [`output shapes`] : Polygons
    - LINES [`output shapes`] : Lines
    - POINTS [`output shapes`] : Duplicated Points
    - ATTRIBUTE [`table field`] : Attribute

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_tools', '20', 'QuadTree Structure to Shapes')
    if Tool.is_Okay():
        Tool.Set_Input ('SHAPES', SHAPES)
        Tool.Set_Output('POLYGONS', POLYGONS)
        Tool.Set_Output('LINES', LINES)
        Tool.Set_Output('POINTS', POINTS)
        Tool.Set_Option('ATTRIBUTE', ATTRIBUTE)
        return Tool.Execute(Verbose)
    return False

def run_tool_shapes_tools_20(SHAPES=None, POLYGONS=None, LINES=None, POINTS=None, ATTRIBUTE=None, Verbose=2):
    '''
    QuadTree Structure to Shapes
    ----------
    [shapes_tools.20]\n
    QuadTree Structure to Shapes\n
    Arguments
    ----------
    - SHAPES [`input shapes`] : Shapes
    - POLYGONS [`output shapes`] : Polygons
    - LINES [`output shapes`] : Lines
    - POINTS [`output shapes`] : Duplicated Points
    - ATTRIBUTE [`table field`] : Attribute

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_tools', '20', 'QuadTree Structure to Shapes')
    if Tool.is_Okay():
        Tool.Set_Input ('SHAPES', SHAPES)
        Tool.Set_Output('POLYGONS', POLYGONS)
        Tool.Set_Output('LINES', LINES)
        Tool.Set_Output('POINTS', POINTS)
        Tool.Set_Option('ATTRIBUTE', ATTRIBUTE)
        return Tool.Execute(Verbose)
    return False

def Polar_to_Cartesian_Coordinates(POLAR=None, CARTES=None, F_EXAGG=None, D_EXAGG=None, RADIUS=None, DEGREE=None, Verbose=2):
    '''
    Polar to Cartesian Coordinates
    ----------
    [shapes_tools.21]\n
    Polar to Cartesian Coordinates\n
    Arguments
    ----------
    - POLAR [`input shapes`] : Polar Coordinates
    - CARTES [`output shapes`] : Cartesian Coordinates
    - F_EXAGG [`table field`] : Exaggeration
    - D_EXAGG [`floating point number`] : Exaggeration Factor. Default: 1.000000
    - RADIUS [`floating point number`] : Radius. Minimum: 0.000000 Default: 6371000.000000
    - DEGREE [`boolean`] : Degree. Default: 1 polar coordinates given in degree

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_tools', '21', 'Polar to Cartesian Coordinates')
    if Tool.is_Okay():
        Tool.Set_Input ('POLAR', POLAR)
        Tool.Set_Output('CARTES', CARTES)
        Tool.Set_Option('F_EXAGG', F_EXAGG)
        Tool.Set_Option('D_EXAGG', D_EXAGG)
        Tool.Set_Option('RADIUS', RADIUS)
        Tool.Set_Option('DEGREE', DEGREE)
        return Tool.Execute(Verbose)
    return False

def run_tool_shapes_tools_21(POLAR=None, CARTES=None, F_EXAGG=None, D_EXAGG=None, RADIUS=None, DEGREE=None, Verbose=2):
    '''
    Polar to Cartesian Coordinates
    ----------
    [shapes_tools.21]\n
    Polar to Cartesian Coordinates\n
    Arguments
    ----------
    - POLAR [`input shapes`] : Polar Coordinates
    - CARTES [`output shapes`] : Cartesian Coordinates
    - F_EXAGG [`table field`] : Exaggeration
    - D_EXAGG [`floating point number`] : Exaggeration Factor. Default: 1.000000
    - RADIUS [`floating point number`] : Radius. Minimum: 0.000000 Default: 6371000.000000
    - DEGREE [`boolean`] : Degree. Default: 1 polar coordinates given in degree

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_tools', '21', 'Polar to Cartesian Coordinates')
    if Tool.is_Okay():
        Tool.Set_Input ('POLAR', POLAR)
        Tool.Set_Output('CARTES', CARTES)
        Tool.Set_Option('F_EXAGG', F_EXAGG)
        Tool.Set_Option('D_EXAGG', D_EXAGG)
        Tool.Set_Option('RADIUS', RADIUS)
        Tool.Set_Option('DEGREE', DEGREE)
        return Tool.Execute(Verbose)
    return False

def Generate_Shapes(INPUT=None, OUTPUT=None, FIELD_ID=None, FIELD_X=None, FIELD_Y=None, FIELD_Z=None, SHAPE_TYPE=None, Verbose=2):
    '''
    Generate Shapes
    ----------
    [shapes_tools.22]\n
    The tool allows one to generate point, line or polygon shapes from a table with the following attribute fields:\n
    - identifier\n
    - x coordinate\n
    - y coordinate\n
    - optional: z coordinate (this will create a 3D shapefile)\n
    The table must be sorted in vertex order.\n
    The identifier has different meanings:\n
    * Point Shapes: The identifier is arbitrary\n
    * Line Shapes: The identifier is unique for each line\n
    * Polygon Shapes: The identifier is unique for each polygon; the first polygon vertex may but must not be duplicated in order to close the polygon\n
    Arguments
    ----------
    - INPUT [`input table`] : Input. Table with coordinates.
    - OUTPUT [`output shapes`] : Output. Generated Shapefile.
    - FIELD_ID [`table field`] : ID. Field with the identifier.
    - FIELD_X [`table field`] : X. Field with the x coordinate.
    - FIELD_Y [`table field`] : Y. Field with the y coordinate.
    - FIELD_Z [`table field`] : Z. Field with the z coordinate.
    - SHAPE_TYPE [`choice`] : Shape Type. Available Choices: [0] Point(s) [1] Line(s) [2] Polygon(s) Default: 0 Shape type to generate.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_tools', '22', 'Generate Shapes')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Output('OUTPUT', OUTPUT)
        Tool.Set_Option('FIELD_ID', FIELD_ID)
        Tool.Set_Option('FIELD_X', FIELD_X)
        Tool.Set_Option('FIELD_Y', FIELD_Y)
        Tool.Set_Option('FIELD_Z', FIELD_Z)
        Tool.Set_Option('SHAPE_TYPE', SHAPE_TYPE)
        return Tool.Execute(Verbose)
    return False

def run_tool_shapes_tools_22(INPUT=None, OUTPUT=None, FIELD_ID=None, FIELD_X=None, FIELD_Y=None, FIELD_Z=None, SHAPE_TYPE=None, Verbose=2):
    '''
    Generate Shapes
    ----------
    [shapes_tools.22]\n
    The tool allows one to generate point, line or polygon shapes from a table with the following attribute fields:\n
    - identifier\n
    - x coordinate\n
    - y coordinate\n
    - optional: z coordinate (this will create a 3D shapefile)\n
    The table must be sorted in vertex order.\n
    The identifier has different meanings:\n
    * Point Shapes: The identifier is arbitrary\n
    * Line Shapes: The identifier is unique for each line\n
    * Polygon Shapes: The identifier is unique for each polygon; the first polygon vertex may but must not be duplicated in order to close the polygon\n
    Arguments
    ----------
    - INPUT [`input table`] : Input. Table with coordinates.
    - OUTPUT [`output shapes`] : Output. Generated Shapefile.
    - FIELD_ID [`table field`] : ID. Field with the identifier.
    - FIELD_X [`table field`] : X. Field with the x coordinate.
    - FIELD_Y [`table field`] : Y. Field with the y coordinate.
    - FIELD_Z [`table field`] : Z. Field with the z coordinate.
    - SHAPE_TYPE [`choice`] : Shape Type. Available Choices: [0] Point(s) [1] Line(s) [2] Polygon(s) Default: 0 Shape type to generate.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_tools', '22', 'Generate Shapes')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Output('OUTPUT', OUTPUT)
        Tool.Set_Option('FIELD_ID', FIELD_ID)
        Tool.Set_Option('FIELD_X', FIELD_X)
        Tool.Set_Option('FIELD_Y', FIELD_Y)
        Tool.Set_Option('FIELD_Z', FIELD_Z)
        Tool.Set_Option('SHAPE_TYPE', SHAPE_TYPE)
        return Tool.Execute(Verbose)
    return False

def Convert_Vertex_Type_2D3D(INPUT=None, OUTPUT=None, FIELD_Z=None, FIELD_M=None, Verbose=2):
    '''
    Convert Vertex Type (2D/3D)
    ----------
    [shapes_tools.23]\n
    The tool allows one to convert the vertex type of shapes from 'XY' (2D) to 'XYZ/M' (3D) and vice versa. The conversion from 3D to 2D is not lossless for lines and polygons, as only the Z/M value of one vertex can be retained (currently that of the last vertex).\n
    Arguments
    ----------
    - INPUT [`input shapes`] : Input. The shapefile to convert.
    - OUTPUT [`output shapes`] : Output. The converted shapefile.
    - FIELD_Z [`table field`] : Z. Field with z-coordinate information.
    - FIELD_M [`table field`] : M. Field with measure information.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_tools', '23', 'Convert Vertex Type (2D/3D)')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Output('OUTPUT', OUTPUT)
        Tool.Set_Option('FIELD_Z', FIELD_Z)
        Tool.Set_Option('FIELD_M', FIELD_M)
        return Tool.Execute(Verbose)
    return False

def run_tool_shapes_tools_23(INPUT=None, OUTPUT=None, FIELD_Z=None, FIELD_M=None, Verbose=2):
    '''
    Convert Vertex Type (2D/3D)
    ----------
    [shapes_tools.23]\n
    The tool allows one to convert the vertex type of shapes from 'XY' (2D) to 'XYZ/M' (3D) and vice versa. The conversion from 3D to 2D is not lossless for lines and polygons, as only the Z/M value of one vertex can be retained (currently that of the last vertex).\n
    Arguments
    ----------
    - INPUT [`input shapes`] : Input. The shapefile to convert.
    - OUTPUT [`output shapes`] : Output. The converted shapefile.
    - FIELD_Z [`table field`] : Z. Field with z-coordinate information.
    - FIELD_M [`table field`] : M. Field with measure information.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_tools', '23', 'Convert Vertex Type (2D/3D)')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Output('OUTPUT', OUTPUT)
        Tool.Set_Option('FIELD_Z', FIELD_Z)
        Tool.Set_Option('FIELD_M', FIELD_M)
        return Tool.Execute(Verbose)
    return False

def Merge_Tables(INPUT=None, MERGED=None, SRCINFO=None, MATCH=None, DELETE=None, Verbose=2):
    '''
    Merge Tables
    ----------
    [shapes_tools.24]\n
    Merge tables.\n
    Arguments
    ----------
    - INPUT [`input table list`] : Tables. The resulting table inherits its field structure from the first table in this list.
    - MERGED [`output table`] : Merged Table
    - SRCINFO [`boolean`] : Add Source Information. Default: 1 Adds a field with the name of the original input data set.
    - MATCH [`boolean`] : Match Fields by Name. Default: 1
    - DELETE [`boolean`] : Delete. Default: 0 Deletes each input data set immediately after it has been merged, thus saving memory resources.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_tools', '24', 'Merge Tables')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Output('MERGED', MERGED)
        Tool.Set_Option('SRCINFO', SRCINFO)
        Tool.Set_Option('MATCH', MATCH)
        Tool.Set_Option('DELETE', DELETE)
        return Tool.Execute(Verbose)
    return False

def run_tool_shapes_tools_24(INPUT=None, MERGED=None, SRCINFO=None, MATCH=None, DELETE=None, Verbose=2):
    '''
    Merge Tables
    ----------
    [shapes_tools.24]\n
    Merge tables.\n
    Arguments
    ----------
    - INPUT [`input table list`] : Tables. The resulting table inherits its field structure from the first table in this list.
    - MERGED [`output table`] : Merged Table
    - SRCINFO [`boolean`] : Add Source Information. Default: 1 Adds a field with the name of the original input data set.
    - MATCH [`boolean`] : Match Fields by Name. Default: 1
    - DELETE [`boolean`] : Delete. Default: 0 Deletes each input data set immediately after it has been merged, thus saving memory resources.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_tools', '24', 'Merge Tables')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Output('MERGED', MERGED)
        Tool.Set_Option('SRCINFO', SRCINFO)
        Tool.Set_Option('MATCH', MATCH)
        Tool.Set_Option('DELETE', DELETE)
        return Tool.Execute(Verbose)
    return False

def Land_Use_Scenario_Generator(FIELDS=None, STATISTICS=None, KNOWN_CROPS=None, SCENARIO=None, FIELD_ID=None, OUTPUT=None, Verbose=2):
    '''
    Land Use Scenario Generator
    ----------
    [shapes_tools.25]\n
    This tool generates land use scenarios for fields under agricultural use based on statistics about the amount of crop types grown in the investigated area of interest.\n
    Arguments
    ----------
    - FIELDS [`input shapes`] : Fields
    - STATISTICS [`input table`] : Crop Statistics. The first column specifies a crop type id. The second column provides a human readable name for the crop type (e.g. 'potatoes') .The third column must be an integer value, though this value is not yet used by this tool. The following columns provide the yearly amount of each crop type [%] for a sequence of years. 
    - KNOWN_CROPS [`optional input table`] : Known Crops. The first column specifies the field id as given by the 'Fields' layer. The following columns specify the crop type for each field and year and refer to the crop type identifiers used in the crop statistics table. The sequence of years must be identical with that of the crop statistics table.
    - SCENARIO [`output shapes`] : Land Use Scenario
    - FIELD_ID [`table field`] : Field Identifier
    - OUTPUT [`choice`] : Output of.... Available Choices: [0] Identifier [1] Name Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_tools', '25', 'Land Use Scenario Generator')
    if Tool.is_Okay():
        Tool.Set_Input ('FIELDS', FIELDS)
        Tool.Set_Input ('STATISTICS', STATISTICS)
        Tool.Set_Input ('KNOWN_CROPS', KNOWN_CROPS)
        Tool.Set_Output('SCENARIO', SCENARIO)
        Tool.Set_Option('FIELD_ID', FIELD_ID)
        Tool.Set_Option('OUTPUT', OUTPUT)
        return Tool.Execute(Verbose)
    return False

def run_tool_shapes_tools_25(FIELDS=None, STATISTICS=None, KNOWN_CROPS=None, SCENARIO=None, FIELD_ID=None, OUTPUT=None, Verbose=2):
    '''
    Land Use Scenario Generator
    ----------
    [shapes_tools.25]\n
    This tool generates land use scenarios for fields under agricultural use based on statistics about the amount of crop types grown in the investigated area of interest.\n
    Arguments
    ----------
    - FIELDS [`input shapes`] : Fields
    - STATISTICS [`input table`] : Crop Statistics. The first column specifies a crop type id. The second column provides a human readable name for the crop type (e.g. 'potatoes') .The third column must be an integer value, though this value is not yet used by this tool. The following columns provide the yearly amount of each crop type [%] for a sequence of years. 
    - KNOWN_CROPS [`optional input table`] : Known Crops. The first column specifies the field id as given by the 'Fields' layer. The following columns specify the crop type for each field and year and refer to the crop type identifiers used in the crop statistics table. The sequence of years must be identical with that of the crop statistics table.
    - SCENARIO [`output shapes`] : Land Use Scenario
    - FIELD_ID [`table field`] : Field Identifier
    - OUTPUT [`choice`] : Output of.... Available Choices: [0] Identifier [1] Name Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_tools', '25', 'Land Use Scenario Generator')
    if Tool.is_Okay():
        Tool.Set_Input ('FIELDS', FIELDS)
        Tool.Set_Input ('STATISTICS', STATISTICS)
        Tool.Set_Input ('KNOWN_CROPS', KNOWN_CROPS)
        Tool.Set_Output('SCENARIO', SCENARIO)
        Tool.Set_Option('FIELD_ID', FIELD_ID)
        Tool.Set_Option('OUTPUT', OUTPUT)
        return Tool.Execute(Verbose)
    return False

def Select_Shapes_from_List(SHAPESLIST=None, SHAPES=None, INDEX=None, Verbose=2):
    '''
    Select Shapes from List
    ----------
    [shapes_tools.26]\n
    Main use of this tool is to support tool chain development, allowing to pick a single shapefile from a shapes list.\n
    Arguments
    ----------
    - SHAPESLIST [`input shapes list`] : Shapes List. The input shapes list.
    - SHAPES [`output shapes`] : Shapes. The shapefile picked from the shapes list.
    - INDEX [`integer number`] : Index. Minimum: 0 Default: 0 The list index of the shapefile to pick. Indices start at zero.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_tools', '26', 'Select Shapes from List')
    if Tool.is_Okay():
        Tool.Set_Input ('SHAPESLIST', SHAPESLIST)
        Tool.Set_Output('SHAPES', SHAPES)
        Tool.Set_Option('INDEX', INDEX)
        return Tool.Execute(Verbose)
    return False

def run_tool_shapes_tools_26(SHAPESLIST=None, SHAPES=None, INDEX=None, Verbose=2):
    '''
    Select Shapes from List
    ----------
    [shapes_tools.26]\n
    Main use of this tool is to support tool chain development, allowing to pick a single shapefile from a shapes list.\n
    Arguments
    ----------
    - SHAPESLIST [`input shapes list`] : Shapes List. The input shapes list.
    - SHAPES [`output shapes`] : Shapes. The shapefile picked from the shapes list.
    - INDEX [`integer number`] : Index. Minimum: 0 Default: 0 The list index of the shapefile to pick. Indices start at zero.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_tools', '26', 'Select Shapes from List')
    if Tool.is_Okay():
        Tool.Set_Input ('SHAPESLIST', SHAPESLIST)
        Tool.Set_Output('SHAPES', SHAPES)
        Tool.Set_Option('INDEX', INDEX)
        return Tool.Execute(Verbose)
    return False

def Remove_Invalid_Shapes(SHAPES=None, Verbose=2):
    '''
    Remove Invalid Shapes
    ----------
    [shapes_tools.27]\n
    This tool deletes geometrically invalid elements from a shapes layer.\n
    Arguments
    ----------
    - SHAPES [`input shapes`] : Shapes

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_tools', '27', 'Remove Invalid Shapes')
    if Tool.is_Okay():
        Tool.Set_Input ('SHAPES', SHAPES)
        return Tool.Execute(Verbose)
    return False

def run_tool_shapes_tools_27(SHAPES=None, Verbose=2):
    '''
    Remove Invalid Shapes
    ----------
    [shapes_tools.27]\n
    This tool deletes geometrically invalid elements from a shapes layer.\n
    Arguments
    ----------
    - SHAPES [`input shapes`] : Shapes

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_tools', '27', 'Remove Invalid Shapes')
    if Tool.is_Okay():
        Tool.Set_Input ('SHAPES', SHAPES)
        return Tool.Execute(Verbose)
    return False

def Copy_Shapes(SHAPES=None, COPY=None, Verbose=2):
    '''
    Copy Shapes
    ----------
    [shapes_tools.28]\n
    Creates a copy of a shapes layer.\n
    Arguments
    ----------
    - SHAPES [`input shapes`] : Shapes
    - COPY [`output shapes`] : Copy

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_tools', '28', 'Copy Shapes')
    if Tool.is_Okay():
        Tool.Set_Input ('SHAPES', SHAPES)
        Tool.Set_Output('COPY', COPY)
        return Tool.Execute(Verbose)
    return False

def run_tool_shapes_tools_28(SHAPES=None, COPY=None, Verbose=2):
    '''
    Copy Shapes
    ----------
    [shapes_tools.28]\n
    Creates a copy of a shapes layer.\n
    Arguments
    ----------
    - SHAPES [`input shapes`] : Shapes
    - COPY [`output shapes`] : Copy

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_tools', '28', 'Copy Shapes')
    if Tool.is_Okay():
        Tool.Set_Input ('SHAPES', SHAPES)
        Tool.Set_Output('COPY', COPY)
        return Tool.Execute(Verbose)
    return False

def Focal_Mechanism_Beachball_Plots(POINTS=None, PLOTS=None, STRIKE=None, DIP=None, RAKE=None, SIZE=None, SIZE_DEF=None, SIZE_RANGE=None, DARC=None, STYLE=None, Verbose=2):
    '''
    Focal Mechanism (Beachball Plots)
    ----------
    [shapes_tools.29]\n
    This tool creates a new polygon layer with beachball plots as representation of focal mechanism solution data. Focal mechanism data of earthquakes have to be supplied as strike and dip angles of the fault plane and rake angle (slip vector) as deviation from the strike angle.\n
    Arguments
    ----------
    - POINTS [`input shapes`] : Points
    - PLOTS [`output shapes`] : Focal Mechanism Beachballs
    - STRIKE [`table field`] : Strike
    - DIP [`table field`] : Dip
    - RAKE [`table field`] : Rake
    - SIZE [`table field`] : Size
    - SIZE_DEF [`floating point number`] : Default. Minimum: 0.000000 Default: 1.000000
    - SIZE_RANGE [`value range`] : Scale to...
    - DARC [`floating point number`] : Arc Vertex Distance [Degree]. Minimum: 0.100000 Maximum: 10.000000 Default: 5.000000
    - STYLE [`choice`] : Style. Available Choices: [0] one [1] two Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_tools', '29', 'Focal Mechanism (Beachball Plots)')
    if Tool.is_Okay():
        Tool.Set_Input ('POINTS', POINTS)
        Tool.Set_Output('PLOTS', PLOTS)
        Tool.Set_Option('STRIKE', STRIKE)
        Tool.Set_Option('DIP', DIP)
        Tool.Set_Option('RAKE', RAKE)
        Tool.Set_Option('SIZE', SIZE)
        Tool.Set_Option('SIZE_DEF', SIZE_DEF)
        Tool.Set_Option('SIZE_RANGE', SIZE_RANGE)
        Tool.Set_Option('DARC', DARC)
        Tool.Set_Option('STYLE', STYLE)
        return Tool.Execute(Verbose)
    return False

def run_tool_shapes_tools_29(POINTS=None, PLOTS=None, STRIKE=None, DIP=None, RAKE=None, SIZE=None, SIZE_DEF=None, SIZE_RANGE=None, DARC=None, STYLE=None, Verbose=2):
    '''
    Focal Mechanism (Beachball Plots)
    ----------
    [shapes_tools.29]\n
    This tool creates a new polygon layer with beachball plots as representation of focal mechanism solution data. Focal mechanism data of earthquakes have to be supplied as strike and dip angles of the fault plane and rake angle (slip vector) as deviation from the strike angle.\n
    Arguments
    ----------
    - POINTS [`input shapes`] : Points
    - PLOTS [`output shapes`] : Focal Mechanism Beachballs
    - STRIKE [`table field`] : Strike
    - DIP [`table field`] : Dip
    - RAKE [`table field`] : Rake
    - SIZE [`table field`] : Size
    - SIZE_DEF [`floating point number`] : Default. Minimum: 0.000000 Default: 1.000000
    - SIZE_RANGE [`value range`] : Scale to...
    - DARC [`floating point number`] : Arc Vertex Distance [Degree]. Minimum: 0.100000 Maximum: 10.000000 Default: 5.000000
    - STYLE [`choice`] : Style. Available Choices: [0] one [1] two Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_tools', '29', 'Focal Mechanism (Beachball Plots)')
    if Tool.is_Okay():
        Tool.Set_Input ('POINTS', POINTS)
        Tool.Set_Output('PLOTS', PLOTS)
        Tool.Set_Option('STRIKE', STRIKE)
        Tool.Set_Option('DIP', DIP)
        Tool.Set_Option('RAKE', RAKE)
        Tool.Set_Option('SIZE', SIZE)
        Tool.Set_Option('SIZE_DEF', SIZE_DEF)
        Tool.Set_Option('SIZE_RANGE', SIZE_RANGE)
        Tool.Set_Option('DARC', DARC)
        Tool.Set_Option('STYLE', STYLE)
        return Tool.Execute(Verbose)
    return False

def Gradient_Lines_from_Points(POINTS=None, GRADIENTS=None, TARGETS=None, DIRECTION=None, LENGTH=None, X_COMP=None, Y_COMP=None, DEFINITION=None, SCALING=None, Verbose=2):
    '''
    Gradient Lines from Points
    ----------
    [shapes_tools.30]\n
    Create lines representing gradients from point data attributes.\n
    Arguments
    ----------
    - POINTS [`input shapes`] : Points
    - GRADIENTS [`output shapes`] : Gradient
    - TARGETS [`output shapes`] : Target Points
    - DIRECTION [`table field`] : Direction. The direction measured in degrees with the North direction as origin and increasing clockwise.
    - LENGTH [`table field`] : Length
    - X_COMP [`table field`] : X-Component
    - Y_COMP [`table field`] : Y-Component
    - DEFINITION [`choice`] : Gradient Definition. Available Choices: [0] direction and length [1] directional components Default: 0
    - SCALING [`floating point number`] : Length Scaling Factor. Minimum: 0.000000 Default: 1.000000

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_tools', '30', 'Gradient Lines from Points')
    if Tool.is_Okay():
        Tool.Set_Input ('POINTS', POINTS)
        Tool.Set_Output('GRADIENTS', GRADIENTS)
        Tool.Set_Output('TARGETS', TARGETS)
        Tool.Set_Option('DIRECTION', DIRECTION)
        Tool.Set_Option('LENGTH', LENGTH)
        Tool.Set_Option('X_COMP', X_COMP)
        Tool.Set_Option('Y_COMP', Y_COMP)
        Tool.Set_Option('DEFINITION', DEFINITION)
        Tool.Set_Option('SCALING', SCALING)
        return Tool.Execute(Verbose)
    return False

def run_tool_shapes_tools_30(POINTS=None, GRADIENTS=None, TARGETS=None, DIRECTION=None, LENGTH=None, X_COMP=None, Y_COMP=None, DEFINITION=None, SCALING=None, Verbose=2):
    '''
    Gradient Lines from Points
    ----------
    [shapes_tools.30]\n
    Create lines representing gradients from point data attributes.\n
    Arguments
    ----------
    - POINTS [`input shapes`] : Points
    - GRADIENTS [`output shapes`] : Gradient
    - TARGETS [`output shapes`] : Target Points
    - DIRECTION [`table field`] : Direction. The direction measured in degrees with the North direction as origin and increasing clockwise.
    - LENGTH [`table field`] : Length
    - X_COMP [`table field`] : X-Component
    - Y_COMP [`table field`] : Y-Component
    - DEFINITION [`choice`] : Gradient Definition. Available Choices: [0] direction and length [1] directional components Default: 0
    - SCALING [`floating point number`] : Length Scaling Factor. Minimum: 0.000000 Default: 1.000000

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_tools', '30', 'Gradient Lines from Points')
    if Tool.is_Okay():
        Tool.Set_Input ('POINTS', POINTS)
        Tool.Set_Output('GRADIENTS', GRADIENTS)
        Tool.Set_Output('TARGETS', TARGETS)
        Tool.Set_Option('DIRECTION', DIRECTION)
        Tool.Set_Option('LENGTH', LENGTH)
        Tool.Set_Option('X_COMP', X_COMP)
        Tool.Set_Option('Y_COMP', Y_COMP)
        Tool.Set_Option('DEFINITION', DEFINITION)
        Tool.Set_Option('SCALING', SCALING)
        return Tool.Execute(Verbose)
    return False

def Select_and_Delete(SHAPES=None, ATTRIBUTE=None, METHOD=None, EXPRESSION=None, COMPARE=None, CASE=None, Verbose=2):
    '''
    Select and Delete
    ----------
    [shapes_tools.select_and_delete]\n
    This tool selects and deletes all features from a shapes layer, which meet the select criterion defined by the expression.[\n
    If the 'Expression Type' is set to 'string expression', the selection uses the given character string expression and the chosen 'Select if...' option. If an 'Attribute' field is selected, only this attribute will be evaluated, or all attributes if not.[\n
    If the 'Expression Type' is set to 'numerical expression', those records will be selected for deletion, for which the expression evaluates to non-zero. The expression syntax is the same as the one for the table calculator. If an 'Attribute' is set, the expression evaluates only this attribute, which can be addressed with the letter 'a' in the expression formula. If no attribute is selected, attributes are addressed by the character 'f' (for 'field') followed by the field number (i.e.: f1, f2, ..., fn) or by the field name in square brackets (e.g.: [Field Name]).[\n
    Examples:\n
    (-) f1 > f2\n
    (-) eq([Population] * 2, [Area])\n
    \n
    Arguments
    ----------
    - SHAPES [`input shapes`] : Shapes
    - ATTRIBUTE [`table field`] : Attribute
    - METHOD [`choice`] : Expression Type. Available Choices: [0] string expression [1] numerical expression Default: 0
    - EXPRESSION [`text`] : Expression. Default: expression
    - COMPARE [`choice`] : Select if.... Available Choices: [0] attribute is identical with search expression [1] attribute contains search expression [2] attribute is contained in search expression Default: 0
    - CASE [`boolean`] : Case Sensitive. Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_tools', 'select_and_delete', 'Select and Delete')
    if Tool.is_Okay():
        Tool.Set_Input ('SHAPES', SHAPES)
        Tool.Set_Option('ATTRIBUTE', ATTRIBUTE)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('EXPRESSION', EXPRESSION)
        Tool.Set_Option('COMPARE', COMPARE)
        Tool.Set_Option('CASE', CASE)
        return Tool.Execute(Verbose)
    return False

def run_tool_shapes_tools_select_and_delete(SHAPES=None, ATTRIBUTE=None, METHOD=None, EXPRESSION=None, COMPARE=None, CASE=None, Verbose=2):
    '''
    Select and Delete
    ----------
    [shapes_tools.select_and_delete]\n
    This tool selects and deletes all features from a shapes layer, which meet the select criterion defined by the expression.[\n
    If the 'Expression Type' is set to 'string expression', the selection uses the given character string expression and the chosen 'Select if...' option. If an 'Attribute' field is selected, only this attribute will be evaluated, or all attributes if not.[\n
    If the 'Expression Type' is set to 'numerical expression', those records will be selected for deletion, for which the expression evaluates to non-zero. The expression syntax is the same as the one for the table calculator. If an 'Attribute' is set, the expression evaluates only this attribute, which can be addressed with the letter 'a' in the expression formula. If no attribute is selected, attributes are addressed by the character 'f' (for 'field') followed by the field number (i.e.: f1, f2, ..., fn) or by the field name in square brackets (e.g.: [Field Name]).[\n
    Examples:\n
    (-) f1 > f2\n
    (-) eq([Population] * 2, [Area])\n
    \n
    Arguments
    ----------
    - SHAPES [`input shapes`] : Shapes
    - ATTRIBUTE [`table field`] : Attribute
    - METHOD [`choice`] : Expression Type. Available Choices: [0] string expression [1] numerical expression Default: 0
    - EXPRESSION [`text`] : Expression. Default: expression
    - COMPARE [`choice`] : Select if.... Available Choices: [0] attribute is identical with search expression [1] attribute contains search expression [2] attribute is contained in search expression Default: 0
    - CASE [`boolean`] : Case Sensitive. Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_tools', 'select_and_delete', 'Select and Delete')
    if Tool.is_Okay():
        Tool.Set_Input ('SHAPES', SHAPES)
        Tool.Set_Option('ATTRIBUTE', ATTRIBUTE)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('EXPRESSION', EXPRESSION)
        Tool.Set_Option('COMPARE', COMPARE)
        Tool.Set_Option('CASE', CASE)
        return Tool.Execute(Verbose)
    return False

