#! /usr/bin/env python

'''
Python Interface to SAGA Tools Library
----------
- Category : Tool Chains
- Name     : Uncategorized Tool Chains
- ID       : _tool_chains_uncategorized

Description
----------
Uncategorized Tool Chains
'''

from PySAGA.helper import Tool_Wrapper

def Gridding_of_Points(POINTS=None, INTERPOLATION=None, ATTRIBUTE=None, CELL_SIZE=None, Verbose=2):
    '''
    Gridding of Points
    ----------
    [toolchains.gridding]\n
    This tool chain simply demonstrates how to create an output grid without having to choose a target grid system before execution by setting the output parameter's '"target"' attribute to '"none"'.\n
    \n
    Arguments
    ----------
    - POINTS [`input shapes`] : Points
    - INTERPOLATION [`output data object`] : Interpolation
    - ATTRIBUTE [`table field`] : Attribute. attribute to become interpolated
    - CELL_SIZE [`floating point number`] : Cell Size. Default: 10.000000 cell size of target grid

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('toolchains', 'gridding', 'Gridding of Points')
    if Tool.is_Okay():
        Tool.Set_Input ('POINTS', POINTS)
        Tool.Set_Output('INTERPOLATION', INTERPOLATION)
        Tool.Set_Option('ATTRIBUTE', ATTRIBUTE)
        Tool.Set_Option('CELL_SIZE', CELL_SIZE)
        return Tool.Execute(Verbose)
    return False

def run_tool_toolchains_gridding(POINTS=None, INTERPOLATION=None, ATTRIBUTE=None, CELL_SIZE=None, Verbose=2):
    '''
    Gridding of Points
    ----------
    [toolchains.gridding]\n
    This tool chain simply demonstrates how to create an output grid without having to choose a target grid system before execution by setting the output parameter's '"target"' attribute to '"none"'.\n
    \n
    Arguments
    ----------
    - POINTS [`input shapes`] : Points
    - INTERPOLATION [`output data object`] : Interpolation
    - ATTRIBUTE [`table field`] : Attribute. attribute to become interpolated
    - CELL_SIZE [`floating point number`] : Cell Size. Default: 10.000000 cell size of target grid

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('toolchains', 'gridding', 'Gridding of Points')
    if Tool.is_Okay():
        Tool.Set_Input ('POINTS', POINTS)
        Tool.Set_Output('INTERPOLATION', INTERPOLATION)
        Tool.Set_Option('ATTRIBUTE', ATTRIBUTE)
        Tool.Set_Option('CELL_SIZE', CELL_SIZE)
        return Tool.Execute(Verbose)
    return False

