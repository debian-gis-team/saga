#! /usr/bin/env python

'''
Python Interface to SAGA Tools Library
----------
- Category : Grid
- Name     : Tools
- ID       : grid_tools

Description
----------
Tools for the manipulation of gridded data.
'''

from PySAGA.helper import Tool_Wrapper

def Resampling(INPUT=None, TARGET_TEMPLATE=None, OUTPUT=None, KEEP_TYPE=None, SCALE_UP=None, SCALE_DOWN=None, TARGET_DEFINITION=None, TARGET_USER_SIZE=None, TARGET_USER_XMIN=None, TARGET_USER_XMAX=None, TARGET_USER_YMIN=None, TARGET_USER_YMAX=None, TARGET_USER_COLS=None, TARGET_USER_ROWS=None, TARGET_USER_FLAT=None, TARGET_USER_FITS=None, TARGET_SYSTEM=None, Verbose=2):
    '''
    Resampling
    ----------
    [grid_tools.0]\n
    Resampling of grids.\n
    Arguments
    ----------
    - INPUT [`input grid list`] : Grids
    - TARGET_TEMPLATE [`optional input grid`] : Target System. use this grid's system for output grids
    - OUTPUT [`output grid list`] : Resampled Grids
    - KEEP_TYPE [`boolean`] : Preserve Data Type. Default: 0
    - SCALE_UP [`choice`] : Upscaling Method. Available Choices: [0] Nearest Neighbour [1] Bilinear Interpolation [2] Bicubic Spline Interpolation [3] B-Spline Interpolation [4] Mean Value [5] Mean Value (cell area weighted) [6] Minimum Value [7] Maximum Value [8] Majority Default: 5
    - SCALE_DOWN [`choice`] : Downscaling Method. Available Choices: [0] Nearest Neighbour [1] Bilinear Interpolation [2] Bicubic Spline Interpolation [3] B-Spline Interpolation Default: 3
    - TARGET_DEFINITION [`choice`] : Target Grid System. Available Choices: [0] user defined [1] grid or grid system Default: 0
    - TARGET_USER_SIZE [`floating point number`] : Cellsize. Minimum: 0.000000 Default: 1.000000
    - TARGET_USER_XMIN [`floating point number`] : West. Default: 0.000000
    - TARGET_USER_XMAX [`floating point number`] : East. Default: 100.000000
    - TARGET_USER_YMIN [`floating point number`] : South. Default: 0.000000
    - TARGET_USER_YMAX [`floating point number`] : North. Default: 100.000000
    - TARGET_USER_COLS [`integer number`] : Columns. Minimum: 1 Default: 101 Number of cells in East-West direction.
    - TARGET_USER_ROWS [`integer number`] : Rows. Minimum: 1 Default: 101 Number of cells in North-South direction.
    - TARGET_USER_FLAT [`boolean`] : Rounding. Default: 1 Round bounding coordinates to multiples of cell size. Ignored if cell size has decimal places.
    - TARGET_USER_FITS [`choice`] : Fit. Available Choices: [0] nodes [1] cells Default: 0
    - TARGET_SYSTEM [`grid system`] : Grid System

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_tools', '0', 'Resampling')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Input ('TARGET_TEMPLATE', TARGET_TEMPLATE)
        Tool.Set_Output('OUTPUT', OUTPUT)
        Tool.Set_Option('KEEP_TYPE', KEEP_TYPE)
        Tool.Set_Option('SCALE_UP', SCALE_UP)
        Tool.Set_Option('SCALE_DOWN', SCALE_DOWN)
        Tool.Set_Option('TARGET_DEFINITION', TARGET_DEFINITION)
        Tool.Set_Option('TARGET_USER_SIZE', TARGET_USER_SIZE)
        Tool.Set_Option('TARGET_USER_XMIN', TARGET_USER_XMIN)
        Tool.Set_Option('TARGET_USER_XMAX', TARGET_USER_XMAX)
        Tool.Set_Option('TARGET_USER_YMIN', TARGET_USER_YMIN)
        Tool.Set_Option('TARGET_USER_YMAX', TARGET_USER_YMAX)
        Tool.Set_Option('TARGET_USER_COLS', TARGET_USER_COLS)
        Tool.Set_Option('TARGET_USER_ROWS', TARGET_USER_ROWS)
        Tool.Set_Option('TARGET_USER_FLAT', TARGET_USER_FLAT)
        Tool.Set_Option('TARGET_USER_FITS', TARGET_USER_FITS)
        Tool.Set_Option('TARGET_SYSTEM', TARGET_SYSTEM)
        return Tool.Execute(Verbose)
    return False

def run_tool_grid_tools_0(INPUT=None, TARGET_TEMPLATE=None, OUTPUT=None, KEEP_TYPE=None, SCALE_UP=None, SCALE_DOWN=None, TARGET_DEFINITION=None, TARGET_USER_SIZE=None, TARGET_USER_XMIN=None, TARGET_USER_XMAX=None, TARGET_USER_YMIN=None, TARGET_USER_YMAX=None, TARGET_USER_COLS=None, TARGET_USER_ROWS=None, TARGET_USER_FLAT=None, TARGET_USER_FITS=None, TARGET_SYSTEM=None, Verbose=2):
    '''
    Resampling
    ----------
    [grid_tools.0]\n
    Resampling of grids.\n
    Arguments
    ----------
    - INPUT [`input grid list`] : Grids
    - TARGET_TEMPLATE [`optional input grid`] : Target System. use this grid's system for output grids
    - OUTPUT [`output grid list`] : Resampled Grids
    - KEEP_TYPE [`boolean`] : Preserve Data Type. Default: 0
    - SCALE_UP [`choice`] : Upscaling Method. Available Choices: [0] Nearest Neighbour [1] Bilinear Interpolation [2] Bicubic Spline Interpolation [3] B-Spline Interpolation [4] Mean Value [5] Mean Value (cell area weighted) [6] Minimum Value [7] Maximum Value [8] Majority Default: 5
    - SCALE_DOWN [`choice`] : Downscaling Method. Available Choices: [0] Nearest Neighbour [1] Bilinear Interpolation [2] Bicubic Spline Interpolation [3] B-Spline Interpolation Default: 3
    - TARGET_DEFINITION [`choice`] : Target Grid System. Available Choices: [0] user defined [1] grid or grid system Default: 0
    - TARGET_USER_SIZE [`floating point number`] : Cellsize. Minimum: 0.000000 Default: 1.000000
    - TARGET_USER_XMIN [`floating point number`] : West. Default: 0.000000
    - TARGET_USER_XMAX [`floating point number`] : East. Default: 100.000000
    - TARGET_USER_YMIN [`floating point number`] : South. Default: 0.000000
    - TARGET_USER_YMAX [`floating point number`] : North. Default: 100.000000
    - TARGET_USER_COLS [`integer number`] : Columns. Minimum: 1 Default: 101 Number of cells in East-West direction.
    - TARGET_USER_ROWS [`integer number`] : Rows. Minimum: 1 Default: 101 Number of cells in North-South direction.
    - TARGET_USER_FLAT [`boolean`] : Rounding. Default: 1 Round bounding coordinates to multiples of cell size. Ignored if cell size has decimal places.
    - TARGET_USER_FITS [`choice`] : Fit. Available Choices: [0] nodes [1] cells Default: 0
    - TARGET_SYSTEM [`grid system`] : Grid System

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_tools', '0', 'Resampling')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Input ('TARGET_TEMPLATE', TARGET_TEMPLATE)
        Tool.Set_Output('OUTPUT', OUTPUT)
        Tool.Set_Option('KEEP_TYPE', KEEP_TYPE)
        Tool.Set_Option('SCALE_UP', SCALE_UP)
        Tool.Set_Option('SCALE_DOWN', SCALE_DOWN)
        Tool.Set_Option('TARGET_DEFINITION', TARGET_DEFINITION)
        Tool.Set_Option('TARGET_USER_SIZE', TARGET_USER_SIZE)
        Tool.Set_Option('TARGET_USER_XMIN', TARGET_USER_XMIN)
        Tool.Set_Option('TARGET_USER_XMAX', TARGET_USER_XMAX)
        Tool.Set_Option('TARGET_USER_YMIN', TARGET_USER_YMIN)
        Tool.Set_Option('TARGET_USER_YMAX', TARGET_USER_YMAX)
        Tool.Set_Option('TARGET_USER_COLS', TARGET_USER_COLS)
        Tool.Set_Option('TARGET_USER_ROWS', TARGET_USER_ROWS)
        Tool.Set_Option('TARGET_USER_FLAT', TARGET_USER_FLAT)
        Tool.Set_Option('TARGET_USER_FITS', TARGET_USER_FITS)
        Tool.Set_Option('TARGET_SYSTEM', TARGET_SYSTEM)
        return Tool.Execute(Verbose)
    return False

def Aggregate(INPUT=None, OUTPUT=None, SIZE=None, METHOD=None, Verbose=2):
    '''
    Aggregate
    ----------
    [grid_tools.1]\n
    Resamples a raster layer to a lower resolution, aggregatingthe values of a group of cells. This should be used in any case in which a normalresampling will result in wrong values in the resulting layer, such as, for instance,the number of elements of a given class in each cell.\n
    Arguments
    ----------
    - INPUT [`input grid`] : Grid
    - OUTPUT [`output data object`] : Aggregated Grid
    - SIZE [`integer number`] : Aggregation Size. Minimum: 2 Default: 2
    - METHOD [`choice`] : Method. Available Choices: [0] Sum [1] Minimum [2] Maximum [3] Median [4] Mean [5] Mode Default: 4

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_tools', '1', 'Aggregate')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Output('OUTPUT', OUTPUT)
        Tool.Set_Option('SIZE', SIZE)
        Tool.Set_Option('METHOD', METHOD)
        return Tool.Execute(Verbose)
    return False

def run_tool_grid_tools_1(INPUT=None, OUTPUT=None, SIZE=None, METHOD=None, Verbose=2):
    '''
    Aggregate
    ----------
    [grid_tools.1]\n
    Resamples a raster layer to a lower resolution, aggregatingthe values of a group of cells. This should be used in any case in which a normalresampling will result in wrong values in the resulting layer, such as, for instance,the number of elements of a given class in each cell.\n
    Arguments
    ----------
    - INPUT [`input grid`] : Grid
    - OUTPUT [`output data object`] : Aggregated Grid
    - SIZE [`integer number`] : Aggregation Size. Minimum: 2 Default: 2
    - METHOD [`choice`] : Method. Available Choices: [0] Sum [1] Minimum [2] Maximum [3] Median [4] Mean [5] Mode Default: 4

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_tools', '1', 'Aggregate')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Output('OUTPUT', OUTPUT)
        Tool.Set_Option('SIZE', SIZE)
        Tool.Set_Option('METHOD', METHOD)
        return Tool.Execute(Verbose)
    return False

def Mosaicking(GRIDS=None, TARGET_TEMPLATE=None, TARGET_OUT_GRID=None, FILE_LIST=None, TYPE=None, RESAMPLING=None, OVERLAP=None, BLEND_DIST=None, BLEND_BND=None, MATCH=None, TARGET_DEFINITION=None, TARGET_USER_SIZE=None, TARGET_USER_XMIN=None, TARGET_USER_XMAX=None, TARGET_USER_YMIN=None, TARGET_USER_YMAX=None, TARGET_USER_COLS=None, TARGET_USER_ROWS=None, TARGET_USER_FLAT=None, TARGET_USER_FITS=None, TARGET_SYSTEM=None, Verbose=2):
    '''
    Mosaicking
    ----------
    [grid_tools.3]\n
    The tool allows one to merge multiple grids into one single grid. This involves resampling if the input grids have different cell sizes or are not aligned to each other. Besides different resampling methods, the tool provides several options on how to handle overlapping areas. It is also possible to apply a histogram matching.\n
    In order to be able to also merge a large amount of grids, which, for example, would exceed the maximum command line length, the tools has the option to provide a file list as input (instead of using the input grid list). This is a text file with the full path to an input grid on each line. Please note the limitations: (i) the target grid system is set automatically in this case (the extent is calculated from all inputs and the cell size is set to the smallest one detected) and (ii) the input grids must still fit into memory, i.e. are all loaded at once.\n
    Arguments
    ----------
    - GRIDS [`optional input grid list`] : Grids
    - TARGET_TEMPLATE [`optional input grid`] : Target System. use this grid's system for output grids
    - TARGET_OUT_GRID [`output grid`] : Target Grid
    - FILE_LIST [`file path`] : Input File List. A text file with the full path to an input grid on each line
    - TYPE [`data type`] : Data Storage Type. Available Choices: [0] bit [1] unsigned 1 byte integer [2] signed 1 byte integer [3] unsigned 2 byte integer [4] signed 2 byte integer [5] unsigned 4 byte integer [6] signed 4 byte integer [7] unsigned 8 byte integer [8] signed 8 byte integer [9] 4 byte floating point number [10] 8 byte floating point number [11] same as first grid in list Default: 11
    - RESAMPLING [`choice`] : Resampling. Available Choices: [0] Nearest Neighbour [1] Bilinear Interpolation [2] Bicubic Spline Interpolation [3] B-Spline Interpolation Default: 3
    - OVERLAP [`choice`] : Overlapping Areas. Available Choices: [0] first [1] last [2] minimum [3] maximum [4] mean [5] blend boundary [6] feathering Default: 1
    - BLEND_DIST [`floating point number`] : Blending Distance. Minimum: 0.000000 Default: 10.000000 blending distance given in map units
    - BLEND_BND [`choice`] : Blending Boundary. Available Choices: [0] valid data cells [1] grid boundaries [2] vertical grid boundaries [3] horizontal grid boundaries Default: 0 blending boundary for distance calculation
    - MATCH [`choice`] : Match. Available Choices: [0] none [1] match histogram of first grid in list [2] match histogram of overlapping area [3] regression Default: 0
    - TARGET_DEFINITION [`choice`] : Target Grid System. Available Choices: [0] user defined [1] grid or grid system Default: 0
    - TARGET_USER_SIZE [`floating point number`] : Cellsize. Minimum: 0.000000 Default: 1.000000
    - TARGET_USER_XMIN [`floating point number`] : West. Default: 0.000000
    - TARGET_USER_XMAX [`floating point number`] : East. Default: 100.000000
    - TARGET_USER_YMIN [`floating point number`] : South. Default: 0.000000
    - TARGET_USER_YMAX [`floating point number`] : North. Default: 100.000000
    - TARGET_USER_COLS [`integer number`] : Columns. Minimum: 1 Default: 101 Number of cells in East-West direction.
    - TARGET_USER_ROWS [`integer number`] : Rows. Minimum: 1 Default: 101 Number of cells in North-South direction.
    - TARGET_USER_FLAT [`boolean`] : Rounding. Default: 1 Round bounding coordinates to multiples of cell size. Ignored if cell size has decimal places.
    - TARGET_USER_FITS [`choice`] : Fit. Available Choices: [0] nodes [1] cells Default: 0
    - TARGET_SYSTEM [`grid system`] : Grid System

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_tools', '3', 'Mosaicking')
    if Tool.is_Okay():
        Tool.Set_Input ('GRIDS', GRIDS)
        Tool.Set_Input ('TARGET_TEMPLATE', TARGET_TEMPLATE)
        Tool.Set_Output('TARGET_OUT_GRID', TARGET_OUT_GRID)
        Tool.Set_Option('FILE_LIST', FILE_LIST)
        Tool.Set_Option('TYPE', TYPE)
        Tool.Set_Option('RESAMPLING', RESAMPLING)
        Tool.Set_Option('OVERLAP', OVERLAP)
        Tool.Set_Option('BLEND_DIST', BLEND_DIST)
        Tool.Set_Option('BLEND_BND', BLEND_BND)
        Tool.Set_Option('MATCH', MATCH)
        Tool.Set_Option('TARGET_DEFINITION', TARGET_DEFINITION)
        Tool.Set_Option('TARGET_USER_SIZE', TARGET_USER_SIZE)
        Tool.Set_Option('TARGET_USER_XMIN', TARGET_USER_XMIN)
        Tool.Set_Option('TARGET_USER_XMAX', TARGET_USER_XMAX)
        Tool.Set_Option('TARGET_USER_YMIN', TARGET_USER_YMIN)
        Tool.Set_Option('TARGET_USER_YMAX', TARGET_USER_YMAX)
        Tool.Set_Option('TARGET_USER_COLS', TARGET_USER_COLS)
        Tool.Set_Option('TARGET_USER_ROWS', TARGET_USER_ROWS)
        Tool.Set_Option('TARGET_USER_FLAT', TARGET_USER_FLAT)
        Tool.Set_Option('TARGET_USER_FITS', TARGET_USER_FITS)
        Tool.Set_Option('TARGET_SYSTEM', TARGET_SYSTEM)
        return Tool.Execute(Verbose)
    return False

def run_tool_grid_tools_3(GRIDS=None, TARGET_TEMPLATE=None, TARGET_OUT_GRID=None, FILE_LIST=None, TYPE=None, RESAMPLING=None, OVERLAP=None, BLEND_DIST=None, BLEND_BND=None, MATCH=None, TARGET_DEFINITION=None, TARGET_USER_SIZE=None, TARGET_USER_XMIN=None, TARGET_USER_XMAX=None, TARGET_USER_YMIN=None, TARGET_USER_YMAX=None, TARGET_USER_COLS=None, TARGET_USER_ROWS=None, TARGET_USER_FLAT=None, TARGET_USER_FITS=None, TARGET_SYSTEM=None, Verbose=2):
    '''
    Mosaicking
    ----------
    [grid_tools.3]\n
    The tool allows one to merge multiple grids into one single grid. This involves resampling if the input grids have different cell sizes or are not aligned to each other. Besides different resampling methods, the tool provides several options on how to handle overlapping areas. It is also possible to apply a histogram matching.\n
    In order to be able to also merge a large amount of grids, which, for example, would exceed the maximum command line length, the tools has the option to provide a file list as input (instead of using the input grid list). This is a text file with the full path to an input grid on each line. Please note the limitations: (i) the target grid system is set automatically in this case (the extent is calculated from all inputs and the cell size is set to the smallest one detected) and (ii) the input grids must still fit into memory, i.e. are all loaded at once.\n
    Arguments
    ----------
    - GRIDS [`optional input grid list`] : Grids
    - TARGET_TEMPLATE [`optional input grid`] : Target System. use this grid's system for output grids
    - TARGET_OUT_GRID [`output grid`] : Target Grid
    - FILE_LIST [`file path`] : Input File List. A text file with the full path to an input grid on each line
    - TYPE [`data type`] : Data Storage Type. Available Choices: [0] bit [1] unsigned 1 byte integer [2] signed 1 byte integer [3] unsigned 2 byte integer [4] signed 2 byte integer [5] unsigned 4 byte integer [6] signed 4 byte integer [7] unsigned 8 byte integer [8] signed 8 byte integer [9] 4 byte floating point number [10] 8 byte floating point number [11] same as first grid in list Default: 11
    - RESAMPLING [`choice`] : Resampling. Available Choices: [0] Nearest Neighbour [1] Bilinear Interpolation [2] Bicubic Spline Interpolation [3] B-Spline Interpolation Default: 3
    - OVERLAP [`choice`] : Overlapping Areas. Available Choices: [0] first [1] last [2] minimum [3] maximum [4] mean [5] blend boundary [6] feathering Default: 1
    - BLEND_DIST [`floating point number`] : Blending Distance. Minimum: 0.000000 Default: 10.000000 blending distance given in map units
    - BLEND_BND [`choice`] : Blending Boundary. Available Choices: [0] valid data cells [1] grid boundaries [2] vertical grid boundaries [3] horizontal grid boundaries Default: 0 blending boundary for distance calculation
    - MATCH [`choice`] : Match. Available Choices: [0] none [1] match histogram of first grid in list [2] match histogram of overlapping area [3] regression Default: 0
    - TARGET_DEFINITION [`choice`] : Target Grid System. Available Choices: [0] user defined [1] grid or grid system Default: 0
    - TARGET_USER_SIZE [`floating point number`] : Cellsize. Minimum: 0.000000 Default: 1.000000
    - TARGET_USER_XMIN [`floating point number`] : West. Default: 0.000000
    - TARGET_USER_XMAX [`floating point number`] : East. Default: 100.000000
    - TARGET_USER_YMIN [`floating point number`] : South. Default: 0.000000
    - TARGET_USER_YMAX [`floating point number`] : North. Default: 100.000000
    - TARGET_USER_COLS [`integer number`] : Columns. Minimum: 1 Default: 101 Number of cells in East-West direction.
    - TARGET_USER_ROWS [`integer number`] : Rows. Minimum: 1 Default: 101 Number of cells in North-South direction.
    - TARGET_USER_FLAT [`boolean`] : Rounding. Default: 1 Round bounding coordinates to multiples of cell size. Ignored if cell size has decimal places.
    - TARGET_USER_FITS [`choice`] : Fit. Available Choices: [0] nodes [1] cells Default: 0
    - TARGET_SYSTEM [`grid system`] : Grid System

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_tools', '3', 'Mosaicking')
    if Tool.is_Okay():
        Tool.Set_Input ('GRIDS', GRIDS)
        Tool.Set_Input ('TARGET_TEMPLATE', TARGET_TEMPLATE)
        Tool.Set_Output('TARGET_OUT_GRID', TARGET_OUT_GRID)
        Tool.Set_Option('FILE_LIST', FILE_LIST)
        Tool.Set_Option('TYPE', TYPE)
        Tool.Set_Option('RESAMPLING', RESAMPLING)
        Tool.Set_Option('OVERLAP', OVERLAP)
        Tool.Set_Option('BLEND_DIST', BLEND_DIST)
        Tool.Set_Option('BLEND_BND', BLEND_BND)
        Tool.Set_Option('MATCH', MATCH)
        Tool.Set_Option('TARGET_DEFINITION', TARGET_DEFINITION)
        Tool.Set_Option('TARGET_USER_SIZE', TARGET_USER_SIZE)
        Tool.Set_Option('TARGET_USER_XMIN', TARGET_USER_XMIN)
        Tool.Set_Option('TARGET_USER_XMAX', TARGET_USER_XMAX)
        Tool.Set_Option('TARGET_USER_YMIN', TARGET_USER_YMIN)
        Tool.Set_Option('TARGET_USER_YMAX', TARGET_USER_YMAX)
        Tool.Set_Option('TARGET_USER_COLS', TARGET_USER_COLS)
        Tool.Set_Option('TARGET_USER_ROWS', TARGET_USER_ROWS)
        Tool.Set_Option('TARGET_USER_FLAT', TARGET_USER_FLAT)
        Tool.Set_Option('TARGET_USER_FITS', TARGET_USER_FITS)
        Tool.Set_Option('TARGET_SYSTEM', TARGET_SYSTEM)
        return Tool.Execute(Verbose)
    return False

def Constant_Grid(TEMPLATE=None, OUT_GRID=None, NAME=None, CONST=None, TYPE=None, DEFINITION=None, USER_SIZE=None, USER_XMIN=None, USER_XMAX=None, USER_YMIN=None, USER_YMAX=None, USER_COLS=None, USER_ROWS=None, USER_FLAT=None, USER_FITS=None, SYSTEM=None, Verbose=2):
    '''
    Constant Grid
    ----------
    [grid_tools.4]\n
    Constant grid creation.\n
    Arguments
    ----------
    - TEMPLATE [`optional input grid`] : Target System. use this grid's system for output grids
    - OUT_GRID [`output grid`] : Target Grid
    - NAME [`text`] : Name. Default: Constant Grid
    - CONST [`floating point number`] : Constant Value. Default: 1.000000
    - TYPE [`data type`] : Data Type. Available Choices: [0] bit [1] unsigned 1 byte integer [2] signed 1 byte integer [3] unsigned 2 byte integer [4] signed 2 byte integer [5] unsigned 4 byte integer [6] signed 4 byte integer [7] unsigned 8 byte integer [8] signed 8 byte integer [9] 4 byte floating point number [10] 8 byte floating point number Default: 9
    - DEFINITION [`choice`] : Target Grid System. Available Choices: [0] user defined [1] grid or grid system Default: 0
    - USER_SIZE [`floating point number`] : Cellsize. Minimum: 0.000000 Default: 1.000000
    - USER_XMIN [`floating point number`] : West. Default: 0.000000
    - USER_XMAX [`floating point number`] : East. Default: 100.000000
    - USER_YMIN [`floating point number`] : South. Default: 0.000000
    - USER_YMAX [`floating point number`] : North. Default: 100.000000
    - USER_COLS [`integer number`] : Columns. Minimum: 1 Default: 101 Number of cells in East-West direction.
    - USER_ROWS [`integer number`] : Rows. Minimum: 1 Default: 101 Number of cells in North-South direction.
    - USER_FLAT [`boolean`] : Rounding. Default: 1 Round bounding coordinates to multiples of cell size. Ignored if cell size has decimal places.
    - USER_FITS [`choice`] : Fit. Available Choices: [0] nodes [1] cells Default: 0
    - SYSTEM [`grid system`] : Grid System

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_tools', '4', 'Constant Grid')
    if Tool.is_Okay():
        Tool.Set_Input ('TEMPLATE', TEMPLATE)
        Tool.Set_Output('OUT_GRID', OUT_GRID)
        Tool.Set_Option('NAME', NAME)
        Tool.Set_Option('CONST', CONST)
        Tool.Set_Option('TYPE', TYPE)
        Tool.Set_Option('DEFINITION', DEFINITION)
        Tool.Set_Option('USER_SIZE', USER_SIZE)
        Tool.Set_Option('USER_XMIN', USER_XMIN)
        Tool.Set_Option('USER_XMAX', USER_XMAX)
        Tool.Set_Option('USER_YMIN', USER_YMIN)
        Tool.Set_Option('USER_YMAX', USER_YMAX)
        Tool.Set_Option('USER_COLS', USER_COLS)
        Tool.Set_Option('USER_ROWS', USER_ROWS)
        Tool.Set_Option('USER_FLAT', USER_FLAT)
        Tool.Set_Option('USER_FITS', USER_FITS)
        Tool.Set_Option('SYSTEM', SYSTEM)
        return Tool.Execute(Verbose)
    return False

def run_tool_grid_tools_4(TEMPLATE=None, OUT_GRID=None, NAME=None, CONST=None, TYPE=None, DEFINITION=None, USER_SIZE=None, USER_XMIN=None, USER_XMAX=None, USER_YMIN=None, USER_YMAX=None, USER_COLS=None, USER_ROWS=None, USER_FLAT=None, USER_FITS=None, SYSTEM=None, Verbose=2):
    '''
    Constant Grid
    ----------
    [grid_tools.4]\n
    Constant grid creation.\n
    Arguments
    ----------
    - TEMPLATE [`optional input grid`] : Target System. use this grid's system for output grids
    - OUT_GRID [`output grid`] : Target Grid
    - NAME [`text`] : Name. Default: Constant Grid
    - CONST [`floating point number`] : Constant Value. Default: 1.000000
    - TYPE [`data type`] : Data Type. Available Choices: [0] bit [1] unsigned 1 byte integer [2] signed 1 byte integer [3] unsigned 2 byte integer [4] signed 2 byte integer [5] unsigned 4 byte integer [6] signed 4 byte integer [7] unsigned 8 byte integer [8] signed 8 byte integer [9] 4 byte floating point number [10] 8 byte floating point number Default: 9
    - DEFINITION [`choice`] : Target Grid System. Available Choices: [0] user defined [1] grid or grid system Default: 0
    - USER_SIZE [`floating point number`] : Cellsize. Minimum: 0.000000 Default: 1.000000
    - USER_XMIN [`floating point number`] : West. Default: 0.000000
    - USER_XMAX [`floating point number`] : East. Default: 100.000000
    - USER_YMIN [`floating point number`] : South. Default: 0.000000
    - USER_YMAX [`floating point number`] : North. Default: 100.000000
    - USER_COLS [`integer number`] : Columns. Minimum: 1 Default: 101 Number of cells in East-West direction.
    - USER_ROWS [`integer number`] : Rows. Minimum: 1 Default: 101 Number of cells in North-South direction.
    - USER_FLAT [`boolean`] : Rounding. Default: 1 Round bounding coordinates to multiples of cell size. Ignored if cell size has decimal places.
    - USER_FITS [`choice`] : Fit. Available Choices: [0] nodes [1] cells Default: 0
    - SYSTEM [`grid system`] : Grid System

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_tools', '4', 'Constant Grid')
    if Tool.is_Okay():
        Tool.Set_Input ('TEMPLATE', TEMPLATE)
        Tool.Set_Output('OUT_GRID', OUT_GRID)
        Tool.Set_Option('NAME', NAME)
        Tool.Set_Option('CONST', CONST)
        Tool.Set_Option('TYPE', TYPE)
        Tool.Set_Option('DEFINITION', DEFINITION)
        Tool.Set_Option('USER_SIZE', USER_SIZE)
        Tool.Set_Option('USER_XMIN', USER_XMIN)
        Tool.Set_Option('USER_XMAX', USER_XMAX)
        Tool.Set_Option('USER_YMIN', USER_YMIN)
        Tool.Set_Option('USER_YMAX', USER_YMAX)
        Tool.Set_Option('USER_COLS', USER_COLS)
        Tool.Set_Option('USER_ROWS', USER_ROWS)
        Tool.Set_Option('USER_FLAT', USER_FLAT)
        Tool.Set_Option('USER_FITS', USER_FITS)
        Tool.Set_Option('SYSTEM', SYSTEM)
        return Tool.Execute(Verbose)
    return False

def Patching(ORIGINAL=None, ADDITIONAL=None, COMPLETED=None, ADDITIONAL_GRIDSYSTEM=None, RESAMPLING=None, Verbose=2):
    '''
    Patching
    ----------
    [grid_tools.5]\n
    Fill gaps of a grid with data from another grid. If the target is not set, the changes will be stored to the original grid.\n
    Arguments
    ----------
    - ORIGINAL [`input grid`] : Grid
    - ADDITIONAL [`input grid`] : Patch Grid
    - COMPLETED [`output grid`] : Patched Grid
    - ADDITIONAL_GRIDSYSTEM [`grid system`] : Grid system
    - RESAMPLING [`choice`] : Resampling. Available Choices: [0] Nearest Neighbour [1] Bilinear Interpolation [2] Bicubic Spline Interpolation [3] B-Spline Interpolation Default: 3

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_tools', '5', 'Patching')
    if Tool.is_Okay():
        Tool.Set_Input ('ORIGINAL', ORIGINAL)
        Tool.Set_Input ('ADDITIONAL', ADDITIONAL)
        Tool.Set_Output('COMPLETED', COMPLETED)
        Tool.Set_Option('ADDITIONAL_GRIDSYSTEM', ADDITIONAL_GRIDSYSTEM)
        Tool.Set_Option('RESAMPLING', RESAMPLING)
        return Tool.Execute(Verbose)
    return False

def run_tool_grid_tools_5(ORIGINAL=None, ADDITIONAL=None, COMPLETED=None, ADDITIONAL_GRIDSYSTEM=None, RESAMPLING=None, Verbose=2):
    '''
    Patching
    ----------
    [grid_tools.5]\n
    Fill gaps of a grid with data from another grid. If the target is not set, the changes will be stored to the original grid.\n
    Arguments
    ----------
    - ORIGINAL [`input grid`] : Grid
    - ADDITIONAL [`input grid`] : Patch Grid
    - COMPLETED [`output grid`] : Patched Grid
    - ADDITIONAL_GRIDSYSTEM [`grid system`] : Grid system
    - RESAMPLING [`choice`] : Resampling. Available Choices: [0] Nearest Neighbour [1] Bilinear Interpolation [2] Bicubic Spline Interpolation [3] B-Spline Interpolation Default: 3

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_tools', '5', 'Patching')
    if Tool.is_Okay():
        Tool.Set_Input ('ORIGINAL', ORIGINAL)
        Tool.Set_Input ('ADDITIONAL', ADDITIONAL)
        Tool.Set_Output('COMPLETED', COMPLETED)
        Tool.Set_Option('ADDITIONAL_GRIDSYSTEM', ADDITIONAL_GRIDSYSTEM)
        Tool.Set_Option('RESAMPLING', RESAMPLING)
        return Tool.Execute(Verbose)
    return False

def Close_One_Cell_Gaps(INPUT=None, RESULT=None, MODE=None, METHOD=None, Verbose=2):
    '''
    Close One Cell Gaps
    ----------
    [grid_tools.6]\n
    Closes one cell gaps using the arithmetic mean, median, majority or minority value of the surrounding cell values. If the target is not set, the changes will be stored to the original grid.\n
    Arguments
    ----------
    - INPUT [`input grid`] : Grid
    - RESULT [`output grid`] : Changed Grid
    - MODE [`choice`] : Neighbourhood. Available Choices: [0] Neumann [1] Moore Default: 1 Neumann: the four horizontally and vertically neighboured cells; Moore: all eight adjacent cells
    - METHOD [`choice`] : Value. Available Choices: [0] arithmetic mean [1] median [2] majority [3] minority Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_tools', '6', 'Close One Cell Gaps')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Output('RESULT', RESULT)
        Tool.Set_Option('MODE', MODE)
        Tool.Set_Option('METHOD', METHOD)
        return Tool.Execute(Verbose)
    return False

def run_tool_grid_tools_6(INPUT=None, RESULT=None, MODE=None, METHOD=None, Verbose=2):
    '''
    Close One Cell Gaps
    ----------
    [grid_tools.6]\n
    Closes one cell gaps using the arithmetic mean, median, majority or minority value of the surrounding cell values. If the target is not set, the changes will be stored to the original grid.\n
    Arguments
    ----------
    - INPUT [`input grid`] : Grid
    - RESULT [`output grid`] : Changed Grid
    - MODE [`choice`] : Neighbourhood. Available Choices: [0] Neumann [1] Moore Default: 1 Neumann: the four horizontally and vertically neighboured cells; Moore: all eight adjacent cells
    - METHOD [`choice`] : Value. Available Choices: [0] arithmetic mean [1] median [2] majority [3] minority Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_tools', '6', 'Close One Cell Gaps')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Output('RESULT', RESULT)
        Tool.Set_Option('MODE', MODE)
        Tool.Set_Option('METHOD', METHOD)
        return Tool.Execute(Verbose)
    return False

def Close_Gaps(INPUT=None, MASK=None, RESULT=None, THRESHOLD=None, Verbose=2):
    '''
    Close Gaps
    ----------
    [grid_tools.7]\n
    Close gaps of a grid data set (i.e. eliminate no data values). If the target is not set, the changes will be stored to the original grid.\n
    Arguments
    ----------
    - INPUT [`input grid`] : Grid
    - MASK [`optional input grid`] : Mask
    - RESULT [`output grid`] : Changed Grid
    - THRESHOLD [`floating point number`] : Tension Threshold. Default: 0.100000

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_tools', '7', 'Close Gaps')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Input ('MASK', MASK)
        Tool.Set_Output('RESULT', RESULT)
        Tool.Set_Option('THRESHOLD', THRESHOLD)
        return Tool.Execute(Verbose)
    return False

def run_tool_grid_tools_7(INPUT=None, MASK=None, RESULT=None, THRESHOLD=None, Verbose=2):
    '''
    Close Gaps
    ----------
    [grid_tools.7]\n
    Close gaps of a grid data set (i.e. eliminate no data values). If the target is not set, the changes will be stored to the original grid.\n
    Arguments
    ----------
    - INPUT [`input grid`] : Grid
    - MASK [`optional input grid`] : Mask
    - RESULT [`output grid`] : Changed Grid
    - THRESHOLD [`floating point number`] : Tension Threshold. Default: 0.100000

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_tools', '7', 'Close Gaps')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Input ('MASK', MASK)
        Tool.Set_Output('RESULT', RESULT)
        Tool.Set_Option('THRESHOLD', THRESHOLD)
        return Tool.Execute(Verbose)
    return False

def Grid_Buffer(FEATURES=None, BUFFER=None, TYPE=None, DISTANCE=None, Verbose=2):
    '''
    Grid Buffer
    ----------
    [grid_tools.8]\n
    This tool creates buffers around features in a grid. Features are defined by any value greater than zero. With the buffer distance method 'cell's value', the feature grid's cell values are used as buffer distance. In any case the buffer distance has to be specified using map units. The output buffer grid cell values refer to 1 := inside the buffer, 2 := feature location.\n
    Arguments
    ----------
    - FEATURES [`input grid`] : Features
    - BUFFER [`output grid`] : Buffer
    - TYPE [`choice`] : Type. Available Choices: [0] fixed [1] cell's value Default: 0
    - DISTANCE [`floating point number`] : Distance. Minimum: 0.000000 Default: 1000.000000 Fixed buffer distance given in map units.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_tools', '8', 'Grid Buffer')
    if Tool.is_Okay():
        Tool.Set_Input ('FEATURES', FEATURES)
        Tool.Set_Output('BUFFER', BUFFER)
        Tool.Set_Option('TYPE', TYPE)
        Tool.Set_Option('DISTANCE', DISTANCE)
        return Tool.Execute(Verbose)
    return False

def run_tool_grid_tools_8(FEATURES=None, BUFFER=None, TYPE=None, DISTANCE=None, Verbose=2):
    '''
    Grid Buffer
    ----------
    [grid_tools.8]\n
    This tool creates buffers around features in a grid. Features are defined by any value greater than zero. With the buffer distance method 'cell's value', the feature grid's cell values are used as buffer distance. In any case the buffer distance has to be specified using map units. The output buffer grid cell values refer to 1 := inside the buffer, 2 := feature location.\n
    Arguments
    ----------
    - FEATURES [`input grid`] : Features
    - BUFFER [`output grid`] : Buffer
    - TYPE [`choice`] : Type. Available Choices: [0] fixed [1] cell's value Default: 0
    - DISTANCE [`floating point number`] : Distance. Minimum: 0.000000 Default: 1000.000000 Fixed buffer distance given in map units.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_tools', '8', 'Grid Buffer')
    if Tool.is_Okay():
        Tool.Set_Input ('FEATURES', FEATURES)
        Tool.Set_Output('BUFFER', BUFFER)
        Tool.Set_Option('TYPE', TYPE)
        Tool.Set_Option('DISTANCE', DISTANCE)
        return Tool.Execute(Verbose)
    return False

def Threshold_Buffer(FEATURES=None, VALUE=None, THRESHOLDGRID=None, BUFFER=None, THRESHOLD=None, THRESHOLDTYPE=None, Verbose=2):
    '''
    Threshold Buffer
    ----------
    [grid_tools.9]\n
    Threshold Buffer Creation\n
    Arguments
    ----------
    - FEATURES [`input grid`] : Features
    - VALUE [`input grid`] : Value
    - THRESHOLDGRID [`optional input grid`] : Threshold
    - BUFFER [`output grid`] : Buffer
    - THRESHOLD [`floating point number`] : Threshold Value. Default: 0.000000
    - THRESHOLDTYPE [`choice`] : Threshold Type. Available Choices: [0] Absolute [1] Relative from cell value Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_tools', '9', 'Threshold Buffer')
    if Tool.is_Okay():
        Tool.Set_Input ('FEATURES', FEATURES)
        Tool.Set_Input ('VALUE', VALUE)
        Tool.Set_Input ('THRESHOLDGRID', THRESHOLDGRID)
        Tool.Set_Output('BUFFER', BUFFER)
        Tool.Set_Option('THRESHOLD', THRESHOLD)
        Tool.Set_Option('THRESHOLDTYPE', THRESHOLDTYPE)
        return Tool.Execute(Verbose)
    return False

def run_tool_grid_tools_9(FEATURES=None, VALUE=None, THRESHOLDGRID=None, BUFFER=None, THRESHOLD=None, THRESHOLDTYPE=None, Verbose=2):
    '''
    Threshold Buffer
    ----------
    [grid_tools.9]\n
    Threshold Buffer Creation\n
    Arguments
    ----------
    - FEATURES [`input grid`] : Features
    - VALUE [`input grid`] : Value
    - THRESHOLDGRID [`optional input grid`] : Threshold
    - BUFFER [`output grid`] : Buffer
    - THRESHOLD [`floating point number`] : Threshold Value. Default: 0.000000
    - THRESHOLDTYPE [`choice`] : Threshold Type. Available Choices: [0] Absolute [1] Relative from cell value Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_tools', '9', 'Threshold Buffer')
    if Tool.is_Okay():
        Tool.Set_Input ('FEATURES', FEATURES)
        Tool.Set_Input ('VALUE', VALUE)
        Tool.Set_Input ('THRESHOLDGRID', THRESHOLDGRID)
        Tool.Set_Output('BUFFER', BUFFER)
        Tool.Set_Option('THRESHOLD', THRESHOLD)
        Tool.Set_Option('THRESHOLDTYPE', THRESHOLDTYPE)
        return Tool.Execute(Verbose)
    return False

def Grid_Proximity_Buffer(SOURCE=None, DISTANCE=None, ALLOC=None, BUFFER=None, DIST=None, IVAL=None, Verbose=2):
    '''
    Grid Proximity Buffer
    ----------
    [grid_tools.10]\n
    This tool calculates the euclidean distance within a buffer distance from all NoData cells to the nearest valid neighbour in a source grid. Additionally, the source cells define the zones that will be used in the euclidean allocation calculations. Cell values in the source grid are treated as IDs (integer) and used in the allocation grid to identify the grid value of the closest source cell. If a cell is at an equal distance to two or more sources, the cell is assigned to the source that is first encountered in the tools scanning process. The buffer grid is a reclassification of the distance grid using a user specified equidistance to create a set of discrete distance buffers from source features. The buffer zones are coded with the maximum distance value of the corresponding buffer interval. The output value type for the distance grid is floating-point. The output values for the allocation and buffer grid are of type integer. The duration of tool execution is dependent on the number of source cells and the buffer distance.\n
    Arguments
    ----------
    - SOURCE [`input grid`] : Source Grid. Grid with features to be buffered [Category/NoData]
    - DISTANCE [`output grid`] : Distance Grid. Grid with euclidean distance to nearest source cell [grid units]
    - ALLOC [`output grid`] : Allocation Grid. Grid with category of nearest source cell [Category]
    - BUFFER [`output grid`] : Buffer Grid. Reclassification of distance grid to buffer zones with a width equal to the equidistance value.
    - DIST [`floating point number`] : Buffer distance. Default: 500.000000 Buffer distance (grid units).
    - IVAL [`integer number`] : Equidistance. Default: 100 Reclassify buffer distance to intervals of euqidistance ...

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_tools', '10', 'Grid Proximity Buffer')
    if Tool.is_Okay():
        Tool.Set_Input ('SOURCE', SOURCE)
        Tool.Set_Output('DISTANCE', DISTANCE)
        Tool.Set_Output('ALLOC', ALLOC)
        Tool.Set_Output('BUFFER', BUFFER)
        Tool.Set_Option('DIST', DIST)
        Tool.Set_Option('IVAL', IVAL)
        return Tool.Execute(Verbose)
    return False

def run_tool_grid_tools_10(SOURCE=None, DISTANCE=None, ALLOC=None, BUFFER=None, DIST=None, IVAL=None, Verbose=2):
    '''
    Grid Proximity Buffer
    ----------
    [grid_tools.10]\n
    This tool calculates the euclidean distance within a buffer distance from all NoData cells to the nearest valid neighbour in a source grid. Additionally, the source cells define the zones that will be used in the euclidean allocation calculations. Cell values in the source grid are treated as IDs (integer) and used in the allocation grid to identify the grid value of the closest source cell. If a cell is at an equal distance to two or more sources, the cell is assigned to the source that is first encountered in the tools scanning process. The buffer grid is a reclassification of the distance grid using a user specified equidistance to create a set of discrete distance buffers from source features. The buffer zones are coded with the maximum distance value of the corresponding buffer interval. The output value type for the distance grid is floating-point. The output values for the allocation and buffer grid are of type integer. The duration of tool execution is dependent on the number of source cells and the buffer distance.\n
    Arguments
    ----------
    - SOURCE [`input grid`] : Source Grid. Grid with features to be buffered [Category/NoData]
    - DISTANCE [`output grid`] : Distance Grid. Grid with euclidean distance to nearest source cell [grid units]
    - ALLOC [`output grid`] : Allocation Grid. Grid with category of nearest source cell [Category]
    - BUFFER [`output grid`] : Buffer Grid. Reclassification of distance grid to buffer zones with a width equal to the equidistance value.
    - DIST [`floating point number`] : Buffer distance. Default: 500.000000 Buffer distance (grid units).
    - IVAL [`integer number`] : Equidistance. Default: 100 Reclassify buffer distance to intervals of euqidistance ...

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_tools', '10', 'Grid Proximity Buffer')
    if Tool.is_Okay():
        Tool.Set_Input ('SOURCE', SOURCE)
        Tool.Set_Output('DISTANCE', DISTANCE)
        Tool.Set_Output('ALLOC', ALLOC)
        Tool.Set_Output('BUFFER', BUFFER)
        Tool.Set_Option('DIST', DIST)
        Tool.Set_Option('IVAL', IVAL)
        return Tool.Execute(Verbose)
    return False

def Change_Data_Storage(INPUT=None, OUTPUT=None, TYPE=None, OFFSET=None, SCALE=None, Verbose=2):
    '''
    Change Data Storage
    ----------
    [grid_tools.11]\n
    Changes a grid's data storage type, offset and scaling, e.g. from 4 byte floating point to 2 byte signed integer. This might be useful to increase precision or to save memory. If the target is not set, the original grid's storage type will be changed.\n
    Arguments
    ----------
    - INPUT [`input grid`] : Grid
    - OUTPUT [`output grid`] : Converted Grid
    - TYPE [`choice`] : Data storage type. Available Choices: [0] bit [1] unsigned 1 byte integer [2] signed 1 byte integer [3] unsigned 2 byte integer [4] signed 2 byte integer [5] unsigned 4 byte integer [6] signed 4 byte integer [7] 4 byte floating point number [8] 8 byte floating point number Default: 7
    - OFFSET [`floating point number`] : Offset. Default: 0.000000
    - SCALE [`floating point number`] : Scale. Default: 1.000000

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_tools', '11', 'Change Data Storage')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Output('OUTPUT', OUTPUT)
        Tool.Set_Option('TYPE', TYPE)
        Tool.Set_Option('OFFSET', OFFSET)
        Tool.Set_Option('SCALE', SCALE)
        return Tool.Execute(Verbose)
    return False

def run_tool_grid_tools_11(INPUT=None, OUTPUT=None, TYPE=None, OFFSET=None, SCALE=None, Verbose=2):
    '''
    Change Data Storage
    ----------
    [grid_tools.11]\n
    Changes a grid's data storage type, offset and scaling, e.g. from 4 byte floating point to 2 byte signed integer. This might be useful to increase precision or to save memory. If the target is not set, the original grid's storage type will be changed.\n
    Arguments
    ----------
    - INPUT [`input grid`] : Grid
    - OUTPUT [`output grid`] : Converted Grid
    - TYPE [`choice`] : Data storage type. Available Choices: [0] bit [1] unsigned 1 byte integer [2] signed 1 byte integer [3] unsigned 2 byte integer [4] signed 2 byte integer [5] unsigned 4 byte integer [6] signed 4 byte integer [7] 4 byte floating point number [8] 8 byte floating point number Default: 7
    - OFFSET [`floating point number`] : Offset. Default: 0.000000
    - SCALE [`floating point number`] : Scale. Default: 1.000000

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_tools', '11', 'Change Data Storage')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Output('OUTPUT', OUTPUT)
        Tool.Set_Option('TYPE', TYPE)
        Tool.Set_Option('OFFSET', OFFSET)
        Tool.Set_Option('SCALE', SCALE)
        return Tool.Execute(Verbose)
    return False

def Change_Grid_Values(INPUT=None, GRID=None, OUTPUT=None, METHOD=None, IDENTITY=None, RANGE=None, Verbose=2):
    '''
    Change Grid Values
    ----------
    [grid_tools.12]\n
    Changes values of a grid according to the rules of a user defined lookup table. Values or value ranges that are not listed in the lookup table remain unchanged. If the target is not set, the changes will be stored to the original grid.\n
    Arguments
    ----------
    - INPUT [`input grid`] : Grid
    - GRID [`input grid`] : Classified Grid. Synchronize with look-up table classification of another grid (gui only).
    - OUTPUT [`output grid`] : Changed Grid
    - METHOD [`choice`] : Replace Condition. Available Choices: [0] identity [1] range [2] synchronize look-up table classification Default: 0
    - IDENTITY [`static table`] : Lookup Table. 2 Fields: - 1. [8 byte floating point number] New Value - 2. [8 byte floating point number] Value 
    - RANGE [`static table`] : Lookup Table. 3 Fields: - 1. [8 byte floating point number] New Value - 2. [8 byte floating point number] Minimum - 3. [8 byte floating point number] Maximum 

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_tools', '12', 'Change Grid Values')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Input ('GRID', GRID)
        Tool.Set_Output('OUTPUT', OUTPUT)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('IDENTITY', IDENTITY)
        Tool.Set_Option('RANGE', RANGE)
        return Tool.Execute(Verbose)
    return False

def run_tool_grid_tools_12(INPUT=None, GRID=None, OUTPUT=None, METHOD=None, IDENTITY=None, RANGE=None, Verbose=2):
    '''
    Change Grid Values
    ----------
    [grid_tools.12]\n
    Changes values of a grid according to the rules of a user defined lookup table. Values or value ranges that are not listed in the lookup table remain unchanged. If the target is not set, the changes will be stored to the original grid.\n
    Arguments
    ----------
    - INPUT [`input grid`] : Grid
    - GRID [`input grid`] : Classified Grid. Synchronize with look-up table classification of another grid (gui only).
    - OUTPUT [`output grid`] : Changed Grid
    - METHOD [`choice`] : Replace Condition. Available Choices: [0] identity [1] range [2] synchronize look-up table classification Default: 0
    - IDENTITY [`static table`] : Lookup Table. 2 Fields: - 1. [8 byte floating point number] New Value - 2. [8 byte floating point number] Value 
    - RANGE [`static table`] : Lookup Table. 3 Fields: - 1. [8 byte floating point number] New Value - 2. [8 byte floating point number] Minimum - 3. [8 byte floating point number] Maximum 

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_tools', '12', 'Change Grid Values')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Input ('GRID', GRID)
        Tool.Set_Output('OUTPUT', OUTPUT)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('IDENTITY', IDENTITY)
        Tool.Set_Option('RANGE', RANGE)
        return Tool.Execute(Verbose)
    return False

def Reclassify_Grid_Values(INPUT=None, RETAB_2=None, RESULT=None, METHOD=None, OLD=None, NEW=None, SOPERATOR=None, MIN=None, MAX=None, RNEW=None, ROPERATOR=None, RETAB=None, TOPERATOR=None, F_MIN=None, F_MAX=None, F_CODE=None, NODATAOPT=None, NODATA=None, OTHEROPT=None, OTHERS=None, RESULT_TYPE=None, RESULT_NODATA_CHOICE=None, RESULT_NODATA_VALUE=None, Verbose=2):
    '''
    Reclassify Grid Values
    ----------
    [grid_tools.15]\n
    The tool can be used to reclassify the values of a grid. It provides three different options:\n
    (a) reclassification of single values\n
    (b) reclassification of a range of values\n
    (c) reclassification of value ranges specified in a lookup table (simple or user supplied table)\n
    In addition to these methods, two special cases (No Data values and values not included in the reclassification setup) are supported.\n
    With reclassification mode (a) and (b), the 'No Data' option is evaluated before the 'Method' settings. In reclassification mode (c) the option is evaluated only if the No Data value is not included in the lookup table.\n
    The 'Other Values' option is always evaluated after checking the 'Method' settings.\n
    The tool also provides options to control the data storage type and No Data value of the output grid.\n
    Arguments
    ----------
    - INPUT [`input grid`] : Grid. Grid to reclassify.
    - RETAB_2 [`optional input table`] : Lookup Table. The lookup table used with method 'user supplied table'.
    - RESULT [`output grid`] : Reclassified Grid. Reclassified grid.
    - METHOD [`choice`] : Method. Available Choices: [0] single [1] range [2] simple table [3] user supplied table Default: 0 Select the desired method: (1) a single value or a range defined by a single value is reclassified, (2) a range of values is reclassified, (3) and (4) a lookup table is used to reclassify the grid.
    - OLD [`floating point number`] : Old Value. Default: 0.000000 Value to reclassify.
    - NEW [`floating point number`] : New Value. Default: 1.000000 The value to assign (with method 'single value').
    - SOPERATOR [`choice`] : Operator. Available Choices: [0] = [1] < [2] <= [3] >= [4] > Default: 0 Select the desired operator; it is possible to define a range above or below the old value.
    - MIN [`floating point number`] : Minimum Value. Default: 0.000000 The minimum value of the range to be reclassified.
    - MAX [`floating point number`] : Maximum Value. Default: 10.000000 The maximum value of the range to be reclassified.
    - RNEW [`floating point number`] : New Value. Default: 5.000000 The value to assign (with method 'range').
    - ROPERATOR [`choice`] : Operator. Available Choices: [0] <= [1] < Default: 0 Select the desired operator (for method 'range'): eg. min < value < max.
    - RETAB [`static table`] : Lookup Table. 3 Fields: - 1. [8 byte floating point number] minimum - 2. [8 byte floating point number] maximum - 3. [8 byte floating point number] new  The lookup table used with method 'table'.
    - TOPERATOR [`choice`] : Operator. Available Choices: [0] min <= value < max [1] min <= value <= max [2] min < value <= max [3] min < value < max Default: 0 Select the desired operator (for method 'table').
    - F_MIN [`table field`] : Minimum Value. The table field with the minimum value.
    - F_MAX [`table field`] : Maximum Value. The table field with the maximum value.
    - F_CODE [`table field`] : New Value. The table field with the value to assign.
    - NODATAOPT [`boolean`] : No Data Values. Default: 0 Use this option to reclassify No Data values independently of the method settings.
    - NODATA [`floating point number`] : New Value. Default: 0.000000 The value to assign to No Data values.
    - OTHEROPT [`boolean`] : Other Values. Default: 0 Use this option to reclassify all values that are not included in the reclassification setup.
    - OTHERS [`floating point number`] : New Value. Default: 0.000000 The value to assign to all values not included in the reclassification setup.
    - RESULT_TYPE [`data type`] : Data Storage Type. Available Choices: [0] bit [1] unsigned 1 byte integer [2] signed 1 byte integer [3] unsigned 2 byte integer [4] signed 2 byte integer [5] unsigned 4 byte integer [6] signed 4 byte integer [7] unsigned 8 byte integer [8] signed 8 byte integer [9] 4 byte floating point number [10] 8 byte floating point number [11] same as first grid in list Default: 11 The data storage type of the output grid.
    - RESULT_NODATA_CHOICE [`choice`] : No Data Value. Available Choices: [0] No Data value of input grid [1] user defined No Data value [2] No Data value of data storage type Default: 0 Choose how to handle the No Data value of the output grid.
    - RESULT_NODATA_VALUE [`floating point number`] : No Data Value. Default: -99999.000000 User defined No Data value for output grid.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_tools', '15', 'Reclassify Grid Values')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Input ('RETAB_2', RETAB_2)
        Tool.Set_Output('RESULT', RESULT)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('OLD', OLD)
        Tool.Set_Option('NEW', NEW)
        Tool.Set_Option('SOPERATOR', SOPERATOR)
        Tool.Set_Option('MIN', MIN)
        Tool.Set_Option('MAX', MAX)
        Tool.Set_Option('RNEW', RNEW)
        Tool.Set_Option('ROPERATOR', ROPERATOR)
        Tool.Set_Option('RETAB', RETAB)
        Tool.Set_Option('TOPERATOR', TOPERATOR)
        Tool.Set_Option('F_MIN', F_MIN)
        Tool.Set_Option('F_MAX', F_MAX)
        Tool.Set_Option('F_CODE', F_CODE)
        Tool.Set_Option('NODATAOPT', NODATAOPT)
        Tool.Set_Option('NODATA', NODATA)
        Tool.Set_Option('OTHEROPT', OTHEROPT)
        Tool.Set_Option('OTHERS', OTHERS)
        Tool.Set_Option('RESULT_TYPE', RESULT_TYPE)
        Tool.Set_Option('RESULT_NODATA_CHOICE', RESULT_NODATA_CHOICE)
        Tool.Set_Option('RESULT_NODATA_VALUE', RESULT_NODATA_VALUE)
        return Tool.Execute(Verbose)
    return False

def run_tool_grid_tools_15(INPUT=None, RETAB_2=None, RESULT=None, METHOD=None, OLD=None, NEW=None, SOPERATOR=None, MIN=None, MAX=None, RNEW=None, ROPERATOR=None, RETAB=None, TOPERATOR=None, F_MIN=None, F_MAX=None, F_CODE=None, NODATAOPT=None, NODATA=None, OTHEROPT=None, OTHERS=None, RESULT_TYPE=None, RESULT_NODATA_CHOICE=None, RESULT_NODATA_VALUE=None, Verbose=2):
    '''
    Reclassify Grid Values
    ----------
    [grid_tools.15]\n
    The tool can be used to reclassify the values of a grid. It provides three different options:\n
    (a) reclassification of single values\n
    (b) reclassification of a range of values\n
    (c) reclassification of value ranges specified in a lookup table (simple or user supplied table)\n
    In addition to these methods, two special cases (No Data values and values not included in the reclassification setup) are supported.\n
    With reclassification mode (a) and (b), the 'No Data' option is evaluated before the 'Method' settings. In reclassification mode (c) the option is evaluated only if the No Data value is not included in the lookup table.\n
    The 'Other Values' option is always evaluated after checking the 'Method' settings.\n
    The tool also provides options to control the data storage type and No Data value of the output grid.\n
    Arguments
    ----------
    - INPUT [`input grid`] : Grid. Grid to reclassify.
    - RETAB_2 [`optional input table`] : Lookup Table. The lookup table used with method 'user supplied table'.
    - RESULT [`output grid`] : Reclassified Grid. Reclassified grid.
    - METHOD [`choice`] : Method. Available Choices: [0] single [1] range [2] simple table [3] user supplied table Default: 0 Select the desired method: (1) a single value or a range defined by a single value is reclassified, (2) a range of values is reclassified, (3) and (4) a lookup table is used to reclassify the grid.
    - OLD [`floating point number`] : Old Value. Default: 0.000000 Value to reclassify.
    - NEW [`floating point number`] : New Value. Default: 1.000000 The value to assign (with method 'single value').
    - SOPERATOR [`choice`] : Operator. Available Choices: [0] = [1] < [2] <= [3] >= [4] > Default: 0 Select the desired operator; it is possible to define a range above or below the old value.
    - MIN [`floating point number`] : Minimum Value. Default: 0.000000 The minimum value of the range to be reclassified.
    - MAX [`floating point number`] : Maximum Value. Default: 10.000000 The maximum value of the range to be reclassified.
    - RNEW [`floating point number`] : New Value. Default: 5.000000 The value to assign (with method 'range').
    - ROPERATOR [`choice`] : Operator. Available Choices: [0] <= [1] < Default: 0 Select the desired operator (for method 'range'): eg. min < value < max.
    - RETAB [`static table`] : Lookup Table. 3 Fields: - 1. [8 byte floating point number] minimum - 2. [8 byte floating point number] maximum - 3. [8 byte floating point number] new  The lookup table used with method 'table'.
    - TOPERATOR [`choice`] : Operator. Available Choices: [0] min <= value < max [1] min <= value <= max [2] min < value <= max [3] min < value < max Default: 0 Select the desired operator (for method 'table').
    - F_MIN [`table field`] : Minimum Value. The table field with the minimum value.
    - F_MAX [`table field`] : Maximum Value. The table field with the maximum value.
    - F_CODE [`table field`] : New Value. The table field with the value to assign.
    - NODATAOPT [`boolean`] : No Data Values. Default: 0 Use this option to reclassify No Data values independently of the method settings.
    - NODATA [`floating point number`] : New Value. Default: 0.000000 The value to assign to No Data values.
    - OTHEROPT [`boolean`] : Other Values. Default: 0 Use this option to reclassify all values that are not included in the reclassification setup.
    - OTHERS [`floating point number`] : New Value. Default: 0.000000 The value to assign to all values not included in the reclassification setup.
    - RESULT_TYPE [`data type`] : Data Storage Type. Available Choices: [0] bit [1] unsigned 1 byte integer [2] signed 1 byte integer [3] unsigned 2 byte integer [4] signed 2 byte integer [5] unsigned 4 byte integer [6] signed 4 byte integer [7] unsigned 8 byte integer [8] signed 8 byte integer [9] 4 byte floating point number [10] 8 byte floating point number [11] same as first grid in list Default: 11 The data storage type of the output grid.
    - RESULT_NODATA_CHOICE [`choice`] : No Data Value. Available Choices: [0] No Data value of input grid [1] user defined No Data value [2] No Data value of data storage type Default: 0 Choose how to handle the No Data value of the output grid.
    - RESULT_NODATA_VALUE [`floating point number`] : No Data Value. Default: -99999.000000 User defined No Data value for output grid.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_tools', '15', 'Reclassify Grid Values')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Input ('RETAB_2', RETAB_2)
        Tool.Set_Output('RESULT', RESULT)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('OLD', OLD)
        Tool.Set_Option('NEW', NEW)
        Tool.Set_Option('SOPERATOR', SOPERATOR)
        Tool.Set_Option('MIN', MIN)
        Tool.Set_Option('MAX', MAX)
        Tool.Set_Option('RNEW', RNEW)
        Tool.Set_Option('ROPERATOR', ROPERATOR)
        Tool.Set_Option('RETAB', RETAB)
        Tool.Set_Option('TOPERATOR', TOPERATOR)
        Tool.Set_Option('F_MIN', F_MIN)
        Tool.Set_Option('F_MAX', F_MAX)
        Tool.Set_Option('F_CODE', F_CODE)
        Tool.Set_Option('NODATAOPT', NODATAOPT)
        Tool.Set_Option('NODATA', NODATA)
        Tool.Set_Option('OTHEROPT', OTHEROPT)
        Tool.Set_Option('OTHERS', OTHERS)
        Tool.Set_Option('RESULT_TYPE', RESULT_TYPE)
        Tool.Set_Option('RESULT_NODATA_CHOICE', RESULT_NODATA_CHOICE)
        Tool.Set_Option('RESULT_NODATA_VALUE', RESULT_NODATA_VALUE)
        return Tool.Execute(Verbose)
    return False

def Crop_to_Data(INPUT=None, OUTPUT=None, Verbose=2):
    '''
    Crop to Data
    ----------
    [grid_tools.17]\n
    Crop grids to valid data cells.\n
    Arguments
    ----------
    - INPUT [`input grid list`] : Grids
    - OUTPUT [`output grid list`] : Cropped Grids

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_tools', '17', 'Crop to Data')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Output('OUTPUT', OUTPUT)
        return Tool.Execute(Verbose)
    return False

def run_tool_grid_tools_17(INPUT=None, OUTPUT=None, Verbose=2):
    '''
    Crop to Data
    ----------
    [grid_tools.17]\n
    Crop grids to valid data cells.\n
    Arguments
    ----------
    - INPUT [`input grid list`] : Grids
    - OUTPUT [`output grid list`] : Cropped Grids

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_tools', '17', 'Crop to Data')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Output('OUTPUT', OUTPUT)
        return Tool.Execute(Verbose)
    return False

def Invert_DataNoData(INPUT=None, OUTPUT=None, VALUE=None, Verbose=2):
    '''
    Invert Data/No-Data
    ----------
    [grid_tools.18]\n
    Converts valid data cells to no-data cells and no-data cells to the user specified value. Mostly suitable when dealing with masks.\n
    Arguments
    ----------
    - INPUT [`input grid`] : Grid
    - OUTPUT [`output grid`] : Result
    - VALUE [`floating point number`] : Data Value. Default: 1.000000

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_tools', '18', 'Invert Data/No-Data')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Output('OUTPUT', OUTPUT)
        Tool.Set_Option('VALUE', VALUE)
        return Tool.Execute(Verbose)
    return False

def run_tool_grid_tools_18(INPUT=None, OUTPUT=None, VALUE=None, Verbose=2):
    '''
    Invert Data/No-Data
    ----------
    [grid_tools.18]\n
    Converts valid data cells to no-data cells and no-data cells to the user specified value. Mostly suitable when dealing with masks.\n
    Arguments
    ----------
    - INPUT [`input grid`] : Grid
    - OUTPUT [`output grid`] : Result
    - VALUE [`floating point number`] : Data Value. Default: 1.000000

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_tools', '18', 'Invert Data/No-Data')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Output('OUTPUT', OUTPUT)
        Tool.Set_Option('VALUE', VALUE)
        return Tool.Execute(Verbose)
    return False

def Combine_Grids(GRID1=None, GRID2=None, RESULT=None, LOOKUP=None, Verbose=2):
    '''
    Combine Grids
    ----------
    [grid_tools.20]\n
    (c) 2005 by Victor Olaya.\n
    Arguments
    ----------
    - GRID1 [`input grid`] : Grid 1
    - GRID2 [`input grid`] : Grid 2
    - RESULT [`output grid`] : Result
    - LOOKUP [`static table`] : LookUp Table. 3 Fields: - 1. [8 byte floating point number] Value in Grid 1 - 2. [8 byte floating point number] Value in Grid 2 - 3. [8 byte floating point number] Resulting Value 

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_tools', '20', 'Combine Grids')
    if Tool.is_Okay():
        Tool.Set_Input ('GRID1', GRID1)
        Tool.Set_Input ('GRID2', GRID2)
        Tool.Set_Output('RESULT', RESULT)
        Tool.Set_Option('LOOKUP', LOOKUP)
        return Tool.Execute(Verbose)
    return False

def run_tool_grid_tools_20(GRID1=None, GRID2=None, RESULT=None, LOOKUP=None, Verbose=2):
    '''
    Combine Grids
    ----------
    [grid_tools.20]\n
    (c) 2005 by Victor Olaya.\n
    Arguments
    ----------
    - GRID1 [`input grid`] : Grid 1
    - GRID2 [`input grid`] : Grid 2
    - RESULT [`output grid`] : Result
    - LOOKUP [`static table`] : LookUp Table. 3 Fields: - 1. [8 byte floating point number] Value in Grid 1 - 2. [8 byte floating point number] Value in Grid 2 - 3. [8 byte floating point number] Resulting Value 

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_tools', '20', 'Combine Grids')
    if Tool.is_Okay():
        Tool.Set_Input ('GRID1', GRID1)
        Tool.Set_Input ('GRID2', GRID2)
        Tool.Set_Output('RESULT', RESULT)
        Tool.Set_Option('LOOKUP', LOOKUP)
        return Tool.Execute(Verbose)
    return False

def Grid_Cell_Index(GRID=None, INDEX=None, ORDER=None, Verbose=2):
    '''
    Grid Cell Index
    ----------
    [grid_tools.21]\n
    Creates an index grid according to the cell values either in ascending or descending order.\n
    Arguments
    ----------
    - GRID [`input grid`] : Grid
    - INDEX [`output grid`] : Index
    - ORDER [`choice`] : Sorting Order. Available Choices: [0] ascending [1] descending Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_tools', '21', 'Grid Cell Index')
    if Tool.is_Okay():
        Tool.Set_Input ('GRID', GRID)
        Tool.Set_Output('INDEX', INDEX)
        Tool.Set_Option('ORDER', ORDER)
        return Tool.Execute(Verbose)
    return False

def run_tool_grid_tools_21(GRID=None, INDEX=None, ORDER=None, Verbose=2):
    '''
    Grid Cell Index
    ----------
    [grid_tools.21]\n
    Creates an index grid according to the cell values either in ascending or descending order.\n
    Arguments
    ----------
    - GRID [`input grid`] : Grid
    - INDEX [`output grid`] : Index
    - ORDER [`choice`] : Sorting Order. Available Choices: [0] ascending [1] descending Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_tools', '21', 'Grid Cell Index')
    if Tool.is_Okay():
        Tool.Set_Input ('GRID', GRID)
        Tool.Set_Output('INDEX', INDEX)
        Tool.Set_Option('ORDER', ORDER)
        return Tool.Execute(Verbose)
    return False

def Grids_from_Classified_Grid_and_Table(TABLE=None, CLASSES=None, GRIDS=None, ID_FIELD=None, Verbose=2):
    '''
    Grids from Classified Grid and Table
    ----------
    [grid_tools.22]\n
    The tool allows one to create grids from a classified grid and a corresponding lookup table. The table must provide an attribute with the class identifiers used in the grid, which is used to link the table and the grid. A grid is created for each additional attribute field found in the table.\n
    Arguments
    ----------
    - TABLE [`input table`] : Table. The table with the (numeric) data values for each class. The tool creates a grid for each table column (besides the ID).
    - CLASSES [`input grid`] : Classes. The grid encoded with the class IDs.
    - GRIDS [`output grid list`] : Grids. The output grids, one grid for each table column.
    - ID_FIELD [`table field`] : Attribute. The attribute with the class IDs, used to link the table and the grid.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_tools', '22', 'Grids from Classified Grid and Table')
    if Tool.is_Okay():
        Tool.Set_Input ('TABLE', TABLE)
        Tool.Set_Input ('CLASSES', CLASSES)
        Tool.Set_Output('GRIDS', GRIDS)
        Tool.Set_Option('ID_FIELD', ID_FIELD)
        return Tool.Execute(Verbose)
    return False

def run_tool_grid_tools_22(TABLE=None, CLASSES=None, GRIDS=None, ID_FIELD=None, Verbose=2):
    '''
    Grids from Classified Grid and Table
    ----------
    [grid_tools.22]\n
    The tool allows one to create grids from a classified grid and a corresponding lookup table. The table must provide an attribute with the class identifiers used in the grid, which is used to link the table and the grid. A grid is created for each additional attribute field found in the table.\n
    Arguments
    ----------
    - TABLE [`input table`] : Table. The table with the (numeric) data values for each class. The tool creates a grid for each table column (besides the ID).
    - CLASSES [`input grid`] : Classes. The grid encoded with the class IDs.
    - GRIDS [`output grid list`] : Grids. The output grids, one grid for each table column.
    - ID_FIELD [`table field`] : Attribute. The attribute with the class IDs, used to link the table and the grid.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_tools', '22', 'Grids from Classified Grid and Table')
    if Tool.is_Okay():
        Tool.Set_Input ('TABLE', TABLE)
        Tool.Set_Input ('CLASSES', CLASSES)
        Tool.Set_Output('GRIDS', GRIDS)
        Tool.Set_Option('ID_FIELD', ID_FIELD)
        return Tool.Execute(Verbose)
    return False

def Create_Grid_System(SHAPESLIST=None, GRIDLIST=None, GRID=None, INIT=None, CELLSIZE=None, M_EXTENT=None, ADJUST=None, XMIN=None, XMAX=None, NX=None, YMIN=None, YMAX=None, NY=None, USEOFF=None, XOFFSET=None, YOFFSET=None, Verbose=2):
    '''
    Create Grid System
    ----------
    [grid_tools.23]\n
    This tool creates a new user specified Grid System for use with other tools.\n
    First of all, please consider the following issues before using the tool:\n
    (a) all calculations of the tool refer to the lower left corner of the grid system, i.e. the xMin and yMin values. This coordinate is fixed unless you specify an offset.\n
    (b) the tool follows the philosophy of SAGA in that the values describing the extent refer to the cell centers. If you like to match the extent with the border of a grid, use an offset.\n
    The tool provides four possibilities to set/determine the extent of the grid system:\n
    (1) by specifying the coordinate of the lower left cell (xMin, yMin) and the number of cells in W-E (NX) and S-N (NY) direction\n
    (2) by specifying the coordinates the of lower left (xMin, yMin) and the upper right (xMax, yMax) cell\n
    (3) by the extent of the shape(s) provided in the Data Objects section\n
    (4) by the extent of the grid(s) provided in the Data Objects section\n
    After selecting the appropriate method to determine the extent, the next step is to specify the Cellsize of the new grid system.\n
    For all methods supplied to determine the extent but number (1), three possibilities are provided to adjust Cellsize and grid system extent (please remember, the lower left corner is fixed!):\n
    (I) adjust the extent to match the Cellsize\n
    (II) adjust the Cellsize to match the extent in E-W direction\n
    (III) adjust the Cellsize to match the extent in S-N direction\n
    Finally it is possible to apply an offset to the lower left corner of the grid system. In this case check the Use Offset option and specify the offset in W-E and S-N direction. Positive values result in a shift in E/N, negative in W/S direction.\n
    In order to create the grid system the tool needs to create a dummy grid.\n
    Arguments
    ----------
    - SHAPESLIST [`input shapes list`] : Shapes Layers
    - GRIDLIST [`input grid list`] : Grids
    - GRID [`output data object`] : Dummy Grid
    - INIT [`floating point number`] : Initialization Value. Default: 0.000000 Value which is assigned to the dummy grid.
    - CELLSIZE [`floating point number`] : Cellsize. Minimum: 0.000000 Default: 10.000000
    - M_EXTENT [`choice`] : Extent Definition. Available Choices: [0] lower left coordinate and number of rows and columns [1] lower left and upper right coordinates [2] one or more shapes layers [3] one or more grids Default: 0
    - ADJUST [`choice`] : Adjust. Available Choices: [0] extent to cell size [1] cell size to left-right extent [2] cell size to bottom-top extent Default: 0
    - XMIN [`floating point number`] : Left. Default: 0.000000
    - XMAX [`floating point number`] : Right. Default: 100.000000
    - NX [`integer number`] : Columns. Minimum: 1 Default: 10
    - YMIN [`floating point number`] : Bottom. Default: 0.000000
    - YMAX [`floating point number`] : Top. Default: 100.000000
    - NY [`integer number`] : Rows. Minimum: 1 Default: 10
    - USEOFF [`boolean`] : Use Offset. Default: 0
    - XOFFSET [`floating point number`] : X Offset. Default: 0.000000 Positive values result in a shift in E direction.
    - YOFFSET [`floating point number`] : Y Offset. Default: 0.000000 Positive values result in a shift in N direction.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_tools', '23', 'Create Grid System')
    if Tool.is_Okay():
        Tool.Set_Input ('SHAPESLIST', SHAPESLIST)
        Tool.Set_Input ('GRIDLIST', GRIDLIST)
        Tool.Set_Output('GRID', GRID)
        Tool.Set_Option('INIT', INIT)
        Tool.Set_Option('CELLSIZE', CELLSIZE)
        Tool.Set_Option('M_EXTENT', M_EXTENT)
        Tool.Set_Option('ADJUST', ADJUST)
        Tool.Set_Option('XMIN', XMIN)
        Tool.Set_Option('XMAX', XMAX)
        Tool.Set_Option('NX', NX)
        Tool.Set_Option('YMIN', YMIN)
        Tool.Set_Option('YMAX', YMAX)
        Tool.Set_Option('NY', NY)
        Tool.Set_Option('USEOFF', USEOFF)
        Tool.Set_Option('XOFFSET', XOFFSET)
        Tool.Set_Option('YOFFSET', YOFFSET)
        return Tool.Execute(Verbose)
    return False

def run_tool_grid_tools_23(SHAPESLIST=None, GRIDLIST=None, GRID=None, INIT=None, CELLSIZE=None, M_EXTENT=None, ADJUST=None, XMIN=None, XMAX=None, NX=None, YMIN=None, YMAX=None, NY=None, USEOFF=None, XOFFSET=None, YOFFSET=None, Verbose=2):
    '''
    Create Grid System
    ----------
    [grid_tools.23]\n
    This tool creates a new user specified Grid System for use with other tools.\n
    First of all, please consider the following issues before using the tool:\n
    (a) all calculations of the tool refer to the lower left corner of the grid system, i.e. the xMin and yMin values. This coordinate is fixed unless you specify an offset.\n
    (b) the tool follows the philosophy of SAGA in that the values describing the extent refer to the cell centers. If you like to match the extent with the border of a grid, use an offset.\n
    The tool provides four possibilities to set/determine the extent of the grid system:\n
    (1) by specifying the coordinate of the lower left cell (xMin, yMin) and the number of cells in W-E (NX) and S-N (NY) direction\n
    (2) by specifying the coordinates the of lower left (xMin, yMin) and the upper right (xMax, yMax) cell\n
    (3) by the extent of the shape(s) provided in the Data Objects section\n
    (4) by the extent of the grid(s) provided in the Data Objects section\n
    After selecting the appropriate method to determine the extent, the next step is to specify the Cellsize of the new grid system.\n
    For all methods supplied to determine the extent but number (1), three possibilities are provided to adjust Cellsize and grid system extent (please remember, the lower left corner is fixed!):\n
    (I) adjust the extent to match the Cellsize\n
    (II) adjust the Cellsize to match the extent in E-W direction\n
    (III) adjust the Cellsize to match the extent in S-N direction\n
    Finally it is possible to apply an offset to the lower left corner of the grid system. In this case check the Use Offset option and specify the offset in W-E and S-N direction. Positive values result in a shift in E/N, negative in W/S direction.\n
    In order to create the grid system the tool needs to create a dummy grid.\n
    Arguments
    ----------
    - SHAPESLIST [`input shapes list`] : Shapes Layers
    - GRIDLIST [`input grid list`] : Grids
    - GRID [`output data object`] : Dummy Grid
    - INIT [`floating point number`] : Initialization Value. Default: 0.000000 Value which is assigned to the dummy grid.
    - CELLSIZE [`floating point number`] : Cellsize. Minimum: 0.000000 Default: 10.000000
    - M_EXTENT [`choice`] : Extent Definition. Available Choices: [0] lower left coordinate and number of rows and columns [1] lower left and upper right coordinates [2] one or more shapes layers [3] one or more grids Default: 0
    - ADJUST [`choice`] : Adjust. Available Choices: [0] extent to cell size [1] cell size to left-right extent [2] cell size to bottom-top extent Default: 0
    - XMIN [`floating point number`] : Left. Default: 0.000000
    - XMAX [`floating point number`] : Right. Default: 100.000000
    - NX [`integer number`] : Columns. Minimum: 1 Default: 10
    - YMIN [`floating point number`] : Bottom. Default: 0.000000
    - YMAX [`floating point number`] : Top. Default: 100.000000
    - NY [`integer number`] : Rows. Minimum: 1 Default: 10
    - USEOFF [`boolean`] : Use Offset. Default: 0
    - XOFFSET [`floating point number`] : X Offset. Default: 0.000000 Positive values result in a shift in E direction.
    - YOFFSET [`floating point number`] : Y Offset. Default: 0.000000 Positive values result in a shift in N direction.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_tools', '23', 'Create Grid System')
    if Tool.is_Okay():
        Tool.Set_Input ('SHAPESLIST', SHAPESLIST)
        Tool.Set_Input ('GRIDLIST', GRIDLIST)
        Tool.Set_Output('GRID', GRID)
        Tool.Set_Option('INIT', INIT)
        Tool.Set_Option('CELLSIZE', CELLSIZE)
        Tool.Set_Option('M_EXTENT', M_EXTENT)
        Tool.Set_Option('ADJUST', ADJUST)
        Tool.Set_Option('XMIN', XMIN)
        Tool.Set_Option('XMAX', XMAX)
        Tool.Set_Option('NX', NX)
        Tool.Set_Option('YMIN', YMIN)
        Tool.Set_Option('YMAX', YMAX)
        Tool.Set_Option('NY', NY)
        Tool.Set_Option('USEOFF', USEOFF)
        Tool.Set_Option('XOFFSET', XOFFSET)
        Tool.Set_Option('YOFFSET', YOFFSET)
        return Tool.Execute(Verbose)
    return False

def Grid_Masking(GRID=None, GRIDS=None, MASK=None, MASKED=None, GRIDS_MASKED=None, LIST=None, GRIDS_CREATE=None, MASK_GRIDSYSTEM=None, NODATA=None, Verbose=2):
    '''
    Grid Masking
    ----------
    [grid_tools.24]\n
    Cells of the input grid will be set to no-data, if their cell center lies outside or within a no-data cell of the mask grid.\n
    Arguments
    ----------
    - GRID [`input grid`] : Grid
    - GRIDS [`input grid list`] : Grids
    - MASK [`input grid`] : Mask
    - MASKED [`output grid`] : Masked Grid
    - GRIDS_MASKED [`output grid list`] : Masked Grids
    - LIST [`boolean`] : List Processing. Default: 0
    - GRIDS_CREATE [`boolean`] : Create Copies. Default: 0 Work on copies instead of overwriting the originals.
    - MASK_GRIDSYSTEM [`grid system`] : Grid system
    - NODATA [`choice`] : Mask Cells. Available Choices: [0] no-data cells [1] data cells Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_tools', '24', 'Grid Masking')
    if Tool.is_Okay():
        Tool.Set_Input ('GRID', GRID)
        Tool.Set_Input ('GRIDS', GRIDS)
        Tool.Set_Input ('MASK', MASK)
        Tool.Set_Output('MASKED', MASKED)
        Tool.Set_Output('GRIDS_MASKED', GRIDS_MASKED)
        Tool.Set_Option('LIST', LIST)
        Tool.Set_Option('GRIDS_CREATE', GRIDS_CREATE)
        Tool.Set_Option('MASK_GRIDSYSTEM', MASK_GRIDSYSTEM)
        Tool.Set_Option('NODATA', NODATA)
        return Tool.Execute(Verbose)
    return False

def run_tool_grid_tools_24(GRID=None, GRIDS=None, MASK=None, MASKED=None, GRIDS_MASKED=None, LIST=None, GRIDS_CREATE=None, MASK_GRIDSYSTEM=None, NODATA=None, Verbose=2):
    '''
    Grid Masking
    ----------
    [grid_tools.24]\n
    Cells of the input grid will be set to no-data, if their cell center lies outside or within a no-data cell of the mask grid.\n
    Arguments
    ----------
    - GRID [`input grid`] : Grid
    - GRIDS [`input grid list`] : Grids
    - MASK [`input grid`] : Mask
    - MASKED [`output grid`] : Masked Grid
    - GRIDS_MASKED [`output grid list`] : Masked Grids
    - LIST [`boolean`] : List Processing. Default: 0
    - GRIDS_CREATE [`boolean`] : Create Copies. Default: 0 Work on copies instead of overwriting the originals.
    - MASK_GRIDSYSTEM [`grid system`] : Grid system
    - NODATA [`choice`] : Mask Cells. Available Choices: [0] no-data cells [1] data cells Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_tools', '24', 'Grid Masking')
    if Tool.is_Okay():
        Tool.Set_Input ('GRID', GRID)
        Tool.Set_Input ('GRIDS', GRIDS)
        Tool.Set_Input ('MASK', MASK)
        Tool.Set_Output('MASKED', MASKED)
        Tool.Set_Output('GRIDS_MASKED', GRIDS_MASKED)
        Tool.Set_Option('LIST', LIST)
        Tool.Set_Option('GRIDS_CREATE', GRIDS_CREATE)
        Tool.Set_Option('MASK_GRIDSYSTEM', MASK_GRIDSYSTEM)
        Tool.Set_Option('NODATA', NODATA)
        return Tool.Execute(Verbose)
    return False

def Close_Gaps_with_Spline(GRID=None, MASK=None, CLOSED=None, MAXGAPCELLS=None, MAXPOINTS=None, LOCALPOINTS=None, EXTENDED=None, NEIGHBOURS=None, RADIUS=None, RELAXATION=None, Verbose=2):
    '''
    Close Gaps with Spline
    ----------
    [grid_tools.25]\n
    Close Gaps with Spline\n
    Arguments
    ----------
    - GRID [`input grid`] : Grid
    - MASK [`optional input grid`] : Mask
    - CLOSED [`output grid`] : Closed Gaps Grid
    - MAXGAPCELLS [`integer number`] : Only Process Gaps with Less Cells. Minimum: 0 Default: 0 ignored if set to zero
    - MAXPOINTS [`integer number`] : Maximum Points. Minimum: 2 Default: 1000
    - LOCALPOINTS [`integer number`] : Number of Points for Local Interpolation. Minimum: 2 Default: 20
    - EXTENDED [`boolean`] : Extended Neighbourhood. Default: 0
    - NEIGHBOURS [`choice`] : Neighbourhood. Available Choices: [0] Neumann [1] Moore Default: 0
    - RADIUS [`integer number`] : Radius (Cells). Minimum: 0 Default: 0
    - RELAXATION [`floating point number`] : Relaxation. Minimum: 0.000000 Default: 0.000000

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_tools', '25', 'Close Gaps with Spline')
    if Tool.is_Okay():
        Tool.Set_Input ('GRID', GRID)
        Tool.Set_Input ('MASK', MASK)
        Tool.Set_Output('CLOSED', CLOSED)
        Tool.Set_Option('MAXGAPCELLS', MAXGAPCELLS)
        Tool.Set_Option('MAXPOINTS', MAXPOINTS)
        Tool.Set_Option('LOCALPOINTS', LOCALPOINTS)
        Tool.Set_Option('EXTENDED', EXTENDED)
        Tool.Set_Option('NEIGHBOURS', NEIGHBOURS)
        Tool.Set_Option('RADIUS', RADIUS)
        Tool.Set_Option('RELAXATION', RELAXATION)
        return Tool.Execute(Verbose)
    return False

def run_tool_grid_tools_25(GRID=None, MASK=None, CLOSED=None, MAXGAPCELLS=None, MAXPOINTS=None, LOCALPOINTS=None, EXTENDED=None, NEIGHBOURS=None, RADIUS=None, RELAXATION=None, Verbose=2):
    '''
    Close Gaps with Spline
    ----------
    [grid_tools.25]\n
    Close Gaps with Spline\n
    Arguments
    ----------
    - GRID [`input grid`] : Grid
    - MASK [`optional input grid`] : Mask
    - CLOSED [`output grid`] : Closed Gaps Grid
    - MAXGAPCELLS [`integer number`] : Only Process Gaps with Less Cells. Minimum: 0 Default: 0 ignored if set to zero
    - MAXPOINTS [`integer number`] : Maximum Points. Minimum: 2 Default: 1000
    - LOCALPOINTS [`integer number`] : Number of Points for Local Interpolation. Minimum: 2 Default: 20
    - EXTENDED [`boolean`] : Extended Neighbourhood. Default: 0
    - NEIGHBOURS [`choice`] : Neighbourhood. Available Choices: [0] Neumann [1] Moore Default: 0
    - RADIUS [`integer number`] : Radius (Cells). Minimum: 0 Default: 0
    - RELAXATION [`floating point number`] : Relaxation. Minimum: 0.000000 Default: 0.000000

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_tools', '25', 'Close Gaps with Spline')
    if Tool.is_Okay():
        Tool.Set_Input ('GRID', GRID)
        Tool.Set_Input ('MASK', MASK)
        Tool.Set_Output('CLOSED', CLOSED)
        Tool.Set_Option('MAXGAPCELLS', MAXGAPCELLS)
        Tool.Set_Option('MAXPOINTS', MAXPOINTS)
        Tool.Set_Option('LOCALPOINTS', LOCALPOINTS)
        Tool.Set_Option('EXTENDED', EXTENDED)
        Tool.Set_Option('NEIGHBOURS', NEIGHBOURS)
        Tool.Set_Option('RADIUS', RADIUS)
        Tool.Set_Option('RELAXATION', RELAXATION)
        return Tool.Execute(Verbose)
    return False

def Proximity_Grid(FEATURES=None, DISTANCE=None, DIRECTION=None, ALLOCATION=None, Verbose=2):
    '''
    Proximity Grid
    ----------
    [grid_tools.26]\n
    Calculates a grid with euclidean distance to feature cells (not no-data cells).\n
    Arguments
    ----------
    - FEATURES [`input grid`] : Features
    - DISTANCE [`output grid`] : Distance
    - DIRECTION [`output grid`] : Direction
    - ALLOCATION [`output grid`] : Allocation

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_tools', '26', 'Proximity Grid')
    if Tool.is_Okay():
        Tool.Set_Input ('FEATURES', FEATURES)
        Tool.Set_Output('DISTANCE', DISTANCE)
        Tool.Set_Output('DIRECTION', DIRECTION)
        Tool.Set_Output('ALLOCATION', ALLOCATION)
        return Tool.Execute(Verbose)
    return False

def run_tool_grid_tools_26(FEATURES=None, DISTANCE=None, DIRECTION=None, ALLOCATION=None, Verbose=2):
    '''
    Proximity Grid
    ----------
    [grid_tools.26]\n
    Calculates a grid with euclidean distance to feature cells (not no-data cells).\n
    Arguments
    ----------
    - FEATURES [`input grid`] : Features
    - DISTANCE [`output grid`] : Distance
    - DIRECTION [`output grid`] : Direction
    - ALLOCATION [`output grid`] : Allocation

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_tools', '26', 'Proximity Grid')
    if Tool.is_Okay():
        Tool.Set_Input ('FEATURES', FEATURES)
        Tool.Set_Output('DISTANCE', DISTANCE)
        Tool.Set_Output('DIRECTION', DIRECTION)
        Tool.Set_Output('ALLOCATION', ALLOCATION)
        return Tool.Execute(Verbose)
    return False

def Tiling(GRID=None, TILES=None, TILES_SAVE=None, TILES_PATH=None, TILES_NAME=None, OVERLAP=None, OVERLAP_SYM=None, METHOD=None, NX=None, NY=None, XRANGE=None, YRANGE=None, DCELL=None, DX=None, DY=None, Verbose=2):
    '''
    Tiling
    ----------
    [grid_tools.27]\n
    Tiling\n
    Arguments
    ----------
    - GRID [`input grid`] : Grid
    - TILES [`output grid list`] : Tiles
    - TILES_SAVE [`boolean`] : Save Tiles to Disk. Default: 0 Save tiles to disk individually
    - TILES_PATH [`file path`] : Output Directory
    - TILES_NAME [`text`] : Base Name. Default: tile The base name of the tiles
    - OVERLAP [`integer number`] : Overlapping Cells. Minimum: 0 Default: 0
    - OVERLAP_SYM [`choice`] : Add Cells. Available Choices: [0] symmetric [1] bottom / left [2] top / right Default: 0
    - METHOD [`choice`] : Tile Size Definition. Available Choices: [0] number of grid cells per tile [1] coordinates (offset, range, cell size, tile size) Default: 0
    - NX [`integer number`] : Number of Column Cells. Minimum: 1 Default: 100
    - NY [`integer number`] : Number of Row Cells. Minimum: 1 Default: 100
    - XRANGE [`value range`] : Offset and Range (X)
    - YRANGE [`value range`] : Offset and Range (Y)
    - DCELL [`floating point number`] : Cell Size. Minimum: 0.000000 Default: 1.000000
    - DX [`floating point number`] : Tile Size (X). Minimum: 0.000000 Default: 100.000000
    - DY [`floating point number`] : Tile Size (Y). Minimum: 0.000000 Default: 100.000000

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_tools', '27', 'Tiling')
    if Tool.is_Okay():
        Tool.Set_Input ('GRID', GRID)
        Tool.Set_Output('TILES', TILES)
        Tool.Set_Option('TILES_SAVE', TILES_SAVE)
        Tool.Set_Option('TILES_PATH', TILES_PATH)
        Tool.Set_Option('TILES_NAME', TILES_NAME)
        Tool.Set_Option('OVERLAP', OVERLAP)
        Tool.Set_Option('OVERLAP_SYM', OVERLAP_SYM)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('NX', NX)
        Tool.Set_Option('NY', NY)
        Tool.Set_Option('XRANGE', XRANGE)
        Tool.Set_Option('YRANGE', YRANGE)
        Tool.Set_Option('DCELL', DCELL)
        Tool.Set_Option('DX', DX)
        Tool.Set_Option('DY', DY)
        return Tool.Execute(Verbose)
    return False

def run_tool_grid_tools_27(GRID=None, TILES=None, TILES_SAVE=None, TILES_PATH=None, TILES_NAME=None, OVERLAP=None, OVERLAP_SYM=None, METHOD=None, NX=None, NY=None, XRANGE=None, YRANGE=None, DCELL=None, DX=None, DY=None, Verbose=2):
    '''
    Tiling
    ----------
    [grid_tools.27]\n
    Tiling\n
    Arguments
    ----------
    - GRID [`input grid`] : Grid
    - TILES [`output grid list`] : Tiles
    - TILES_SAVE [`boolean`] : Save Tiles to Disk. Default: 0 Save tiles to disk individually
    - TILES_PATH [`file path`] : Output Directory
    - TILES_NAME [`text`] : Base Name. Default: tile The base name of the tiles
    - OVERLAP [`integer number`] : Overlapping Cells. Minimum: 0 Default: 0
    - OVERLAP_SYM [`choice`] : Add Cells. Available Choices: [0] symmetric [1] bottom / left [2] top / right Default: 0
    - METHOD [`choice`] : Tile Size Definition. Available Choices: [0] number of grid cells per tile [1] coordinates (offset, range, cell size, tile size) Default: 0
    - NX [`integer number`] : Number of Column Cells. Minimum: 1 Default: 100
    - NY [`integer number`] : Number of Row Cells. Minimum: 1 Default: 100
    - XRANGE [`value range`] : Offset and Range (X)
    - YRANGE [`value range`] : Offset and Range (Y)
    - DCELL [`floating point number`] : Cell Size. Minimum: 0.000000 Default: 1.000000
    - DX [`floating point number`] : Tile Size (X). Minimum: 0.000000 Default: 100.000000
    - DY [`floating point number`] : Tile Size (Y). Minimum: 0.000000 Default: 100.000000

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_tools', '27', 'Tiling')
    if Tool.is_Okay():
        Tool.Set_Input ('GRID', GRID)
        Tool.Set_Output('TILES', TILES)
        Tool.Set_Option('TILES_SAVE', TILES_SAVE)
        Tool.Set_Option('TILES_PATH', TILES_PATH)
        Tool.Set_Option('TILES_NAME', TILES_NAME)
        Tool.Set_Option('OVERLAP', OVERLAP)
        Tool.Set_Option('OVERLAP_SYM', OVERLAP_SYM)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('NX', NX)
        Tool.Set_Option('NY', NY)
        Tool.Set_Option('XRANGE', XRANGE)
        Tool.Set_Option('YRANGE', YRANGE)
        Tool.Set_Option('DCELL', DCELL)
        Tool.Set_Option('DX', DX)
        Tool.Set_Option('DY', DY)
        return Tool.Execute(Verbose)
    return False

def Shrink_and_Expand(INPUT=None, RESULT=None, OPERATION=None, CIRCLE=None, RADIUS=None, EXPAND=None, KEEP_TYPE=None, ITERATIVE=None, Verbose=2):
    '''
    Shrink and Expand
    ----------
    [grid_tools.28]\n
    With this tool you can shrink and/or expand regions with valid data by a certain distance defined by the (kernel) radius. Shrinking just invalidates all (valid) data cells found within the given distance to no-data cells, while expansion replaces no-data cells with new values based on the evaluation of all (valid) data cells found within the neighbourhood as defined by the kernel. Both operations can be combined.\n
    The method for the value expansion can be chosen as minimum, maximum, mean or majority value. The neighbourhood can be evaluated either at once or in a stepwise iterative way.\n
    Arguments
    ----------
    - INPUT [`input grid`] : Grid
    - RESULT [`output grid`] : Result
    - OPERATION [`choice`] : Operation. Available Choices: [0] shrink [1] expand [2] shrink and expand [3] expand and shrink Default: 3
    - CIRCLE [`choice`] : Search Mode. Available Choices: [0] Square [1] Circle Default: 1 Choose the shape of the kernel.
    - RADIUS [`integer number`] : Radius. Minimum: 1 Default: 1 The kernel radius [cells].
    - EXPAND [`choice`] : Method. Available Choices: [0] minimum [1] maximum [2] mean [3] majority Default: 3 Choose how to fill no-data cells.
    - KEEP_TYPE [`boolean`] : Preserve Data Type. Default: 1 If false, mean value expansion results will be stored with floating point precision.
    - ITERATIVE [`boolean`] : Iterative Expansion. Default: 0 If false, the neighbourhood for expansion is evaluated in one step, else expansion is done stepwise with a one cell radius for each iteration until desired kernel radius is reached.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_tools', '28', 'Shrink and Expand')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Output('RESULT', RESULT)
        Tool.Set_Option('OPERATION', OPERATION)
        Tool.Set_Option('CIRCLE', CIRCLE)
        Tool.Set_Option('RADIUS', RADIUS)
        Tool.Set_Option('EXPAND', EXPAND)
        Tool.Set_Option('KEEP_TYPE', KEEP_TYPE)
        Tool.Set_Option('ITERATIVE', ITERATIVE)
        return Tool.Execute(Verbose)
    return False

def run_tool_grid_tools_28(INPUT=None, RESULT=None, OPERATION=None, CIRCLE=None, RADIUS=None, EXPAND=None, KEEP_TYPE=None, ITERATIVE=None, Verbose=2):
    '''
    Shrink and Expand
    ----------
    [grid_tools.28]\n
    With this tool you can shrink and/or expand regions with valid data by a certain distance defined by the (kernel) radius. Shrinking just invalidates all (valid) data cells found within the given distance to no-data cells, while expansion replaces no-data cells with new values based on the evaluation of all (valid) data cells found within the neighbourhood as defined by the kernel. Both operations can be combined.\n
    The method for the value expansion can be chosen as minimum, maximum, mean or majority value. The neighbourhood can be evaluated either at once or in a stepwise iterative way.\n
    Arguments
    ----------
    - INPUT [`input grid`] : Grid
    - RESULT [`output grid`] : Result
    - OPERATION [`choice`] : Operation. Available Choices: [0] shrink [1] expand [2] shrink and expand [3] expand and shrink Default: 3
    - CIRCLE [`choice`] : Search Mode. Available Choices: [0] Square [1] Circle Default: 1 Choose the shape of the kernel.
    - RADIUS [`integer number`] : Radius. Minimum: 1 Default: 1 The kernel radius [cells].
    - EXPAND [`choice`] : Method. Available Choices: [0] minimum [1] maximum [2] mean [3] majority Default: 3 Choose how to fill no-data cells.
    - KEEP_TYPE [`boolean`] : Preserve Data Type. Default: 1 If false, mean value expansion results will be stored with floating point precision.
    - ITERATIVE [`boolean`] : Iterative Expansion. Default: 0 If false, the neighbourhood for expansion is evaluated in one step, else expansion is done stepwise with a one cell radius for each iteration until desired kernel radius is reached.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_tools', '28', 'Shrink and Expand')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Output('RESULT', RESULT)
        Tool.Set_Option('OPERATION', OPERATION)
        Tool.Set_Option('CIRCLE', CIRCLE)
        Tool.Set_Option('RADIUS', RADIUS)
        Tool.Set_Option('EXPAND', EXPAND)
        Tool.Set_Option('KEEP_TYPE', KEEP_TYPE)
        Tool.Set_Option('ITERATIVE', ITERATIVE)
        return Tool.Execute(Verbose)
    return False

def Close_Gaps_with_Stepwise_Resampling(INPUT=None, MASK=None, RESULT=None, RESAMPLING=None, GROW=None, Verbose=2):
    '''
    Close Gaps with Stepwise Resampling
    ----------
    [grid_tools.29]\n
    Close gaps of a grid data set (i.e. eliminate no data values). If the target is not set, the changes will be stored to the original grid.\n
    Arguments
    ----------
    - INPUT [`input grid`] : Grid
    - MASK [`optional input grid`] : Mask
    - RESULT [`output grid`] : Result
    - RESAMPLING [`choice`] : Resampling. Available Choices: [0] Nearest Neighbour [1] Bilinear Interpolation [2] Bicubic Spline Interpolation [3] B-Spline Interpolation Default: 3
    - GROW [`floating point number`] : Grow Factor. Minimum: 1.000000 Default: 2.000000

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_tools', '29', 'Close Gaps with Stepwise Resampling')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Input ('MASK', MASK)
        Tool.Set_Output('RESULT', RESULT)
        Tool.Set_Option('RESAMPLING', RESAMPLING)
        Tool.Set_Option('GROW', GROW)
        return Tool.Execute(Verbose)
    return False

def run_tool_grid_tools_29(INPUT=None, MASK=None, RESULT=None, RESAMPLING=None, GROW=None, Verbose=2):
    '''
    Close Gaps with Stepwise Resampling
    ----------
    [grid_tools.29]\n
    Close gaps of a grid data set (i.e. eliminate no data values). If the target is not set, the changes will be stored to the original grid.\n
    Arguments
    ----------
    - INPUT [`input grid`] : Grid
    - MASK [`optional input grid`] : Mask
    - RESULT [`output grid`] : Result
    - RESAMPLING [`choice`] : Resampling. Available Choices: [0] Nearest Neighbour [1] Bilinear Interpolation [2] Bicubic Spline Interpolation [3] B-Spline Interpolation Default: 3
    - GROW [`floating point number`] : Grow Factor. Minimum: 1.000000 Default: 2.000000

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_tools', '29', 'Close Gaps with Stepwise Resampling')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Input ('MASK', MASK)
        Tool.Set_Output('RESULT', RESULT)
        Tool.Set_Option('RESAMPLING', RESAMPLING)
        Tool.Set_Option('GROW', GROW)
        return Tool.Execute(Verbose)
    return False

def Transpose_Grids(GRIDS=None, TRANSPOSED=None, MIRROR_X=None, MIRROR_Y=None, Verbose=2):
    '''
    Transpose Grids
    ----------
    [grid_tools.30]\n
    Transpose Grids\n
    Arguments
    ----------
    - GRIDS [`input grid list`] : Grids
    - TRANSPOSED [`output grid list`] : Transposed Grids
    - MIRROR_X [`boolean`] : Mirror Horizontally. Default: 0
    - MIRROR_Y [`boolean`] : Mirror Vertically. Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_tools', '30', 'Transpose Grids')
    if Tool.is_Okay():
        Tool.Set_Input ('GRIDS', GRIDS)
        Tool.Set_Output('TRANSPOSED', TRANSPOSED)
        Tool.Set_Option('MIRROR_X', MIRROR_X)
        Tool.Set_Option('MIRROR_Y', MIRROR_Y)
        return Tool.Execute(Verbose)
    return False

def run_tool_grid_tools_30(GRIDS=None, TRANSPOSED=None, MIRROR_X=None, MIRROR_Y=None, Verbose=2):
    '''
    Transpose Grids
    ----------
    [grid_tools.30]\n
    Transpose Grids\n
    Arguments
    ----------
    - GRIDS [`input grid list`] : Grids
    - TRANSPOSED [`output grid list`] : Transposed Grids
    - MIRROR_X [`boolean`] : Mirror Horizontally. Default: 0
    - MIRROR_Y [`boolean`] : Mirror Vertically. Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_tools', '30', 'Transpose Grids')
    if Tool.is_Okay():
        Tool.Set_Input ('GRIDS', GRIDS)
        Tool.Set_Output('TRANSPOSED', TRANSPOSED)
        Tool.Set_Option('MIRROR_X', MIRROR_X)
        Tool.Set_Option('MIRROR_Y', MIRROR_Y)
        return Tool.Execute(Verbose)
    return False

def Clip_Grids(GRIDS=None, SHAPES=None, POLYGONS=None, CLIPPED=None, EXTENT=None, GRIDSYSTEM=None, INTERIOR=None, CROP=None, XMIN=None, XMAX=None, YMIN=None, YMAX=None, NX=None, NY=None, BUFFER=None, Verbose=2):
    '''
    Clip Grids
    ----------
    [grid_tools.31]\n
    Clip selected grids to specified extent.\n
    Arguments
    ----------
    - GRIDS [`input grid list`] : Grids
    - SHAPES [`input shapes`] : Shapes Extent
    - POLYGONS [`input shapes`] : Polygons
    - CLIPPED [`output grid list`] : Clipped Grids
    - EXTENT [`choice`] : Extent. Available Choices: [0] user defined [1] grid system [2] shapes extent [3] polygon Default: 0
    - GRIDSYSTEM [`grid system`] : Grid System
    - INTERIOR [`boolean`] : Interior. Default: 0 Clip those cells that are covered by the polygons instead of those that are not.
    - CROP [`boolean`] : Crop. Default: 1
    - XMIN [`floating point number`] : Left. Default: 0.000000
    - XMAX [`floating point number`] : Right. Default: 0.000000
    - YMIN [`floating point number`] : Bottom. Default: 0.000000
    - YMAX [`floating point number`] : Top. Default: 0.000000
    - NX [`integer number`] : Columns. Minimum: 1 Default: 1
    - NY [`integer number`] : Rows. Minimum: 1 Default: 1
    - BUFFER [`floating point number`] : Buffer. Minimum: 0.000000 Default: 0.000000 add buffer (map units) to extent

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_tools', '31', 'Clip Grids')
    if Tool.is_Okay():
        Tool.Set_Input ('GRIDS', GRIDS)
        Tool.Set_Input ('SHAPES', SHAPES)
        Tool.Set_Input ('POLYGONS', POLYGONS)
        Tool.Set_Output('CLIPPED', CLIPPED)
        Tool.Set_Option('EXTENT', EXTENT)
        Tool.Set_Option('GRIDSYSTEM', GRIDSYSTEM)
        Tool.Set_Option('INTERIOR', INTERIOR)
        Tool.Set_Option('CROP', CROP)
        Tool.Set_Option('XMIN', XMIN)
        Tool.Set_Option('XMAX', XMAX)
        Tool.Set_Option('YMIN', YMIN)
        Tool.Set_Option('YMAX', YMAX)
        Tool.Set_Option('NX', NX)
        Tool.Set_Option('NY', NY)
        Tool.Set_Option('BUFFER', BUFFER)
        return Tool.Execute(Verbose)
    return False

def run_tool_grid_tools_31(GRIDS=None, SHAPES=None, POLYGONS=None, CLIPPED=None, EXTENT=None, GRIDSYSTEM=None, INTERIOR=None, CROP=None, XMIN=None, XMAX=None, YMIN=None, YMAX=None, NX=None, NY=None, BUFFER=None, Verbose=2):
    '''
    Clip Grids
    ----------
    [grid_tools.31]\n
    Clip selected grids to specified extent.\n
    Arguments
    ----------
    - GRIDS [`input grid list`] : Grids
    - SHAPES [`input shapes`] : Shapes Extent
    - POLYGONS [`input shapes`] : Polygons
    - CLIPPED [`output grid list`] : Clipped Grids
    - EXTENT [`choice`] : Extent. Available Choices: [0] user defined [1] grid system [2] shapes extent [3] polygon Default: 0
    - GRIDSYSTEM [`grid system`] : Grid System
    - INTERIOR [`boolean`] : Interior. Default: 0 Clip those cells that are covered by the polygons instead of those that are not.
    - CROP [`boolean`] : Crop. Default: 1
    - XMIN [`floating point number`] : Left. Default: 0.000000
    - XMAX [`floating point number`] : Right. Default: 0.000000
    - YMIN [`floating point number`] : Bottom. Default: 0.000000
    - YMAX [`floating point number`] : Top. Default: 0.000000
    - NX [`integer number`] : Columns. Minimum: 1 Default: 1
    - NY [`integer number`] : Rows. Minimum: 1 Default: 1
    - BUFFER [`floating point number`] : Buffer. Minimum: 0.000000 Default: 0.000000 add buffer (map units) to extent

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_tools', '31', 'Clip Grids')
    if Tool.is_Okay():
        Tool.Set_Input ('GRIDS', GRIDS)
        Tool.Set_Input ('SHAPES', SHAPES)
        Tool.Set_Input ('POLYGONS', POLYGONS)
        Tool.Set_Output('CLIPPED', CLIPPED)
        Tool.Set_Option('EXTENT', EXTENT)
        Tool.Set_Option('GRIDSYSTEM', GRIDSYSTEM)
        Tool.Set_Option('INTERIOR', INTERIOR)
        Tool.Set_Option('CROP', CROP)
        Tool.Set_Option('XMIN', XMIN)
        Tool.Set_Option('XMAX', XMAX)
        Tool.Set_Option('YMIN', YMIN)
        Tool.Set_Option('YMAX', YMAX)
        Tool.Set_Option('NX', NX)
        Tool.Set_Option('NY', NY)
        Tool.Set_Option('BUFFER', BUFFER)
        return Tool.Execute(Verbose)
    return False

def Select_Grid_from_List(GRIDS=None, GRID=None, INDEX=None, Verbose=2):
    '''
    Select Grid from List
    ----------
    [grid_tools.32]\n
    Main use of this tool is to support tool chain development, allowing to pick a single grid from a grid list.\n
    Arguments
    ----------
    - GRIDS [`input grid list`] : Grid List
    - GRID [`output grid`] : Grid
    - INDEX [`integer number`] : Index. Minimum: 0 Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_tools', '32', 'Select Grid from List')
    if Tool.is_Okay():
        Tool.Set_Input ('GRIDS', GRIDS)
        Tool.Set_Output('GRID', GRID)
        Tool.Set_Option('INDEX', INDEX)
        return Tool.Execute(Verbose)
    return False

def run_tool_grid_tools_32(GRIDS=None, GRID=None, INDEX=None, Verbose=2):
    '''
    Select Grid from List
    ----------
    [grid_tools.32]\n
    Main use of this tool is to support tool chain development, allowing to pick a single grid from a grid list.\n
    Arguments
    ----------
    - GRIDS [`input grid list`] : Grid List
    - GRID [`output grid`] : Grid
    - INDEX [`integer number`] : Index. Minimum: 0 Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_tools', '32', 'Select Grid from List')
    if Tool.is_Okay():
        Tool.Set_Input ('GRIDS', GRIDS)
        Tool.Set_Output('GRID', GRID)
        Tool.Set_Option('INDEX', INDEX)
        return Tool.Execute(Verbose)
    return False

def Copy_Grid(GRID=None, COPY=None, Verbose=2):
    '''
    Copy Grid
    ----------
    [grid_tools.33]\n
    Copy a grid.\n
    Arguments
    ----------
    - GRID [`input grid`] : Grid
    - COPY [`output grid`] : Copy

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_tools', '33', 'Copy Grid')
    if Tool.is_Okay():
        Tool.Set_Input ('GRID', GRID)
        Tool.Set_Output('COPY', COPY)
        return Tool.Execute(Verbose)
    return False

def run_tool_grid_tools_33(GRID=None, COPY=None, Verbose=2):
    '''
    Copy Grid
    ----------
    [grid_tools.33]\n
    Copy a grid.\n
    Arguments
    ----------
    - GRID [`input grid`] : Grid
    - COPY [`output grid`] : Copy

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_tools', '33', 'Copy Grid')
    if Tool.is_Okay():
        Tool.Set_Input ('GRID', GRID)
        Tool.Set_Output('COPY', COPY)
        return Tool.Execute(Verbose)
    return False

def Invert_Grid(GRID=None, INVERSE=None, Verbose=2):
    '''
    Invert Grid
    ----------
    [grid_tools.34]\n
    Invert a grid, i.e. the highest value becomes the lowest and vice versa. If the target is not set, the changes will be stored to the original grid.\n
    Arguments
    ----------
    - GRID [`input grid`] : Grid
    - INVERSE [`output grid`] : Inverse Grid

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_tools', '34', 'Invert Grid')
    if Tool.is_Okay():
        Tool.Set_Input ('GRID', GRID)
        Tool.Set_Output('INVERSE', INVERSE)
        return Tool.Execute(Verbose)
    return False

def run_tool_grid_tools_34(GRID=None, INVERSE=None, Verbose=2):
    '''
    Invert Grid
    ----------
    [grid_tools.34]\n
    Invert a grid, i.e. the highest value becomes the lowest and vice versa. If the target is not set, the changes will be stored to the original grid.\n
    Arguments
    ----------
    - GRID [`input grid`] : Grid
    - INVERSE [`output grid`] : Inverse Grid

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_tools', '34', 'Invert Grid')
    if Tool.is_Okay():
        Tool.Set_Input ('GRID', GRID)
        Tool.Set_Output('INVERSE', INVERSE)
        return Tool.Execute(Verbose)
    return False

def Mirror_Grid(GRID=None, MIRROR=None, METHOD=None, Verbose=2):
    '''
    Mirror Grid
    ----------
    [grid_tools.35]\n
    Mirror a grid at its center axes', either vertically, horizontally or both. If the target is not set, the changes will be stored to the original grid.\n
    Arguments
    ----------
    - GRID [`input grid`] : Grid
    - MIRROR [`output grid`] : Mirror Grid
    - METHOD [`choice`] : Method. Available Choices: [0] horizontally [1] vertically [2] both Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_tools', '35', 'Mirror Grid')
    if Tool.is_Okay():
        Tool.Set_Input ('GRID', GRID)
        Tool.Set_Output('MIRROR', MIRROR)
        Tool.Set_Option('METHOD', METHOD)
        return Tool.Execute(Verbose)
    return False

def run_tool_grid_tools_35(GRID=None, MIRROR=None, METHOD=None, Verbose=2):
    '''
    Mirror Grid
    ----------
    [grid_tools.35]\n
    Mirror a grid at its center axes', either vertically, horizontally or both. If the target is not set, the changes will be stored to the original grid.\n
    Arguments
    ----------
    - GRID [`input grid`] : Grid
    - MIRROR [`output grid`] : Mirror Grid
    - METHOD [`choice`] : Method. Available Choices: [0] horizontally [1] vertically [2] both Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_tools', '35', 'Mirror Grid')
    if Tool.is_Okay():
        Tool.Set_Input ('GRID', GRID)
        Tool.Set_Output('MIRROR', MIRROR)
        Tool.Set_Option('METHOD', METHOD)
        return Tool.Execute(Verbose)
    return False

def Change_a_Grids_NoData_Value(GRID=None, OUTPUT=None, TYPE=None, VALUE=None, RANGE=None, CHANGE=None, Verbose=2):
    '''
    Change a Grid's No-Data Value
    ----------
    [grid_tools.36]\n
    This tool allows changing a grid's no-data value or value range definition. It does not change the cell values of the grid, unless you check the 'Change Values' option. Its main purpose is to support this type of operation for tool chains and scripting environments. If the 'Change Values' option is checked all no-data cells will be changed to the new no-data value.\n
    Arguments
    ----------
    - GRID [`input grid`] : Grid
    - OUTPUT [`output grid`] : Changed Grid
    - TYPE [`choice`] : Type. Available Choices: [0] single value [1] value range Default: 0
    - VALUE [`floating point number`] : No-Data Value. Default: -99999.000000
    - RANGE [`value range`] : No-Data Value Range
    - CHANGE [`boolean`] : Change Values. Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_tools', '36', 'Change a Grid\'s No-Data Value')
    if Tool.is_Okay():
        Tool.Set_Input ('GRID', GRID)
        Tool.Set_Output('OUTPUT', OUTPUT)
        Tool.Set_Option('TYPE', TYPE)
        Tool.Set_Option('VALUE', VALUE)
        Tool.Set_Option('RANGE', RANGE)
        Tool.Set_Option('CHANGE', CHANGE)
        return Tool.Execute(Verbose)
    return False

def run_tool_grid_tools_36(GRID=None, OUTPUT=None, TYPE=None, VALUE=None, RANGE=None, CHANGE=None, Verbose=2):
    '''
    Change a Grid's No-Data Value
    ----------
    [grid_tools.36]\n
    This tool allows changing a grid's no-data value or value range definition. It does not change the cell values of the grid, unless you check the 'Change Values' option. Its main purpose is to support this type of operation for tool chains and scripting environments. If the 'Change Values' option is checked all no-data cells will be changed to the new no-data value.\n
    Arguments
    ----------
    - GRID [`input grid`] : Grid
    - OUTPUT [`output grid`] : Changed Grid
    - TYPE [`choice`] : Type. Available Choices: [0] single value [1] value range Default: 0
    - VALUE [`floating point number`] : No-Data Value. Default: -99999.000000
    - RANGE [`value range`] : No-Data Value Range
    - CHANGE [`boolean`] : Change Values. Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_tools', '36', 'Change a Grid\'s No-Data Value')
    if Tool.is_Okay():
        Tool.Set_Input ('GRID', GRID)
        Tool.Set_Output('OUTPUT', OUTPUT)
        Tool.Set_Option('TYPE', TYPE)
        Tool.Set_Option('VALUE', VALUE)
        Tool.Set_Option('RANGE', RANGE)
        Tool.Set_Option('CHANGE', CHANGE)
        return Tool.Execute(Verbose)
    return False

def Combine_Classes(GRID=None, OUTPUT=None, Verbose=2):
    '''
    Combine Classes
    ----------
    [grid_tools.37]\n
    Based on the look-up table classification of a grid, this tool allows changing and combining class belongings of the cells.\n
    Arguments
    ----------
    - GRID [`input grid`] : Grid
    - OUTPUT [`output grid`] : Output

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_tools', '37', 'Combine Classes')
    if Tool.is_Okay():
        Tool.Set_Input ('GRID', GRID)
        Tool.Set_Output('OUTPUT', OUTPUT)
        return Tool.Execute(Verbose)
    return False

def run_tool_grid_tools_37(GRID=None, OUTPUT=None, Verbose=2):
    '''
    Combine Classes
    ----------
    [grid_tools.37]\n
    Based on the look-up table classification of a grid, this tool allows changing and combining class belongings of the cells.\n
    Arguments
    ----------
    - GRID [`input grid`] : Grid
    - OUTPUT [`output grid`] : Output

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_tools', '37', 'Combine Classes')
    if Tool.is_Okay():
        Tool.Set_Input ('GRID', GRID)
        Tool.Set_Output('OUTPUT', OUTPUT)
        return Tool.Execute(Verbose)
    return False

def Mosaicking_Grid_Collections(GRIDS=None, TARGET_TEMPLATE=None, MOSAIC=None, TYPE=None, RESAMPLING=None, OVERLAP=None, BLEND_DIST=None, BLEND_BND=None, MATCH=None, TARGET_DEFINITION=None, TARGET_USER_SIZE=None, TARGET_USER_XMIN=None, TARGET_USER_XMAX=None, TARGET_USER_YMIN=None, TARGET_USER_YMAX=None, TARGET_USER_COLS=None, TARGET_USER_ROWS=None, TARGET_USER_FLAT=None, TARGET_USER_FITS=None, TARGET_SYSTEM=None, Verbose=2):
    '''
    Mosaicking (Grid Collections)
    ----------
    [grid_tools.38]\n
    Merges multiple grid collections into one single grid collection. Input grid collections have to share the same number of grid levels. Attributes and other general properties will be inherited from the first grid collection in input list.\n
    Arguments
    ----------
    - GRIDS [`input grid collection list`] : Grid Collections
    - TARGET_TEMPLATE [`optional input grid`] : Target System. use this grid's system for output grids
    - MOSAIC [`output grid collection`] : Mosaic
    - TYPE [`data type`] : Data Storage Type. Available Choices: [0] bit [1] unsigned 1 byte integer [2] signed 1 byte integer [3] unsigned 2 byte integer [4] signed 2 byte integer [5] unsigned 4 byte integer [6] signed 4 byte integer [7] unsigned 8 byte integer [8] signed 8 byte integer [9] 4 byte floating point number [10] 8 byte floating point number [11] same as first grid in list Default: 11
    - RESAMPLING [`choice`] : Resampling. Available Choices: [0] Nearest Neighbour [1] Bilinear Interpolation [2] Bicubic Spline Interpolation [3] B-Spline Interpolation Default: 3
    - OVERLAP [`choice`] : Overlapping Areas. Available Choices: [0] first [1] last [2] minimum [3] maximum [4] mean [5] blend boundary [6] feathering Default: 1
    - BLEND_DIST [`floating point number`] : Blending Distance. Minimum: 0.000000 Default: 10.000000 blending distance given in map units
    - BLEND_BND [`choice`] : Blending Boundary. Available Choices: [0] valid data cells [1] grid boundaries [2] vertical grid boundaries [3] horizontal grid boundaries Default: 0 blending boundary for distance calculation
    - MATCH [`choice`] : Match. Available Choices: [0] none [1] match histogram of first grid in list [2] match histogram of overlapping area [3] regression Default: 0
    - TARGET_DEFINITION [`choice`] : Target Grid System. Available Choices: [0] user defined [1] grid or grid system Default: 0
    - TARGET_USER_SIZE [`floating point number`] : Cellsize. Minimum: 0.000000 Default: 1.000000
    - TARGET_USER_XMIN [`floating point number`] : West. Default: 0.000000
    - TARGET_USER_XMAX [`floating point number`] : East. Default: 100.000000
    - TARGET_USER_YMIN [`floating point number`] : South. Default: 0.000000
    - TARGET_USER_YMAX [`floating point number`] : North. Default: 100.000000
    - TARGET_USER_COLS [`integer number`] : Columns. Minimum: 1 Default: 101 Number of cells in East-West direction.
    - TARGET_USER_ROWS [`integer number`] : Rows. Minimum: 1 Default: 101 Number of cells in North-South direction.
    - TARGET_USER_FLAT [`boolean`] : Rounding. Default: 1 Round bounding coordinates to multiples of cell size. Ignored if cell size has decimal places.
    - TARGET_USER_FITS [`choice`] : Fit. Available Choices: [0] nodes [1] cells Default: 0
    - TARGET_SYSTEM [`grid system`] : Grid System

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_tools', '38', 'Mosaicking (Grid Collections)')
    if Tool.is_Okay():
        Tool.Set_Input ('GRIDS', GRIDS)
        Tool.Set_Input ('TARGET_TEMPLATE', TARGET_TEMPLATE)
        Tool.Set_Output('MOSAIC', MOSAIC)
        Tool.Set_Option('TYPE', TYPE)
        Tool.Set_Option('RESAMPLING', RESAMPLING)
        Tool.Set_Option('OVERLAP', OVERLAP)
        Tool.Set_Option('BLEND_DIST', BLEND_DIST)
        Tool.Set_Option('BLEND_BND', BLEND_BND)
        Tool.Set_Option('MATCH', MATCH)
        Tool.Set_Option('TARGET_DEFINITION', TARGET_DEFINITION)
        Tool.Set_Option('TARGET_USER_SIZE', TARGET_USER_SIZE)
        Tool.Set_Option('TARGET_USER_XMIN', TARGET_USER_XMIN)
        Tool.Set_Option('TARGET_USER_XMAX', TARGET_USER_XMAX)
        Tool.Set_Option('TARGET_USER_YMIN', TARGET_USER_YMIN)
        Tool.Set_Option('TARGET_USER_YMAX', TARGET_USER_YMAX)
        Tool.Set_Option('TARGET_USER_COLS', TARGET_USER_COLS)
        Tool.Set_Option('TARGET_USER_ROWS', TARGET_USER_ROWS)
        Tool.Set_Option('TARGET_USER_FLAT', TARGET_USER_FLAT)
        Tool.Set_Option('TARGET_USER_FITS', TARGET_USER_FITS)
        Tool.Set_Option('TARGET_SYSTEM', TARGET_SYSTEM)
        return Tool.Execute(Verbose)
    return False

def run_tool_grid_tools_38(GRIDS=None, TARGET_TEMPLATE=None, MOSAIC=None, TYPE=None, RESAMPLING=None, OVERLAP=None, BLEND_DIST=None, BLEND_BND=None, MATCH=None, TARGET_DEFINITION=None, TARGET_USER_SIZE=None, TARGET_USER_XMIN=None, TARGET_USER_XMAX=None, TARGET_USER_YMIN=None, TARGET_USER_YMAX=None, TARGET_USER_COLS=None, TARGET_USER_ROWS=None, TARGET_USER_FLAT=None, TARGET_USER_FITS=None, TARGET_SYSTEM=None, Verbose=2):
    '''
    Mosaicking (Grid Collections)
    ----------
    [grid_tools.38]\n
    Merges multiple grid collections into one single grid collection. Input grid collections have to share the same number of grid levels. Attributes and other general properties will be inherited from the first grid collection in input list.\n
    Arguments
    ----------
    - GRIDS [`input grid collection list`] : Grid Collections
    - TARGET_TEMPLATE [`optional input grid`] : Target System. use this grid's system for output grids
    - MOSAIC [`output grid collection`] : Mosaic
    - TYPE [`data type`] : Data Storage Type. Available Choices: [0] bit [1] unsigned 1 byte integer [2] signed 1 byte integer [3] unsigned 2 byte integer [4] signed 2 byte integer [5] unsigned 4 byte integer [6] signed 4 byte integer [7] unsigned 8 byte integer [8] signed 8 byte integer [9] 4 byte floating point number [10] 8 byte floating point number [11] same as first grid in list Default: 11
    - RESAMPLING [`choice`] : Resampling. Available Choices: [0] Nearest Neighbour [1] Bilinear Interpolation [2] Bicubic Spline Interpolation [3] B-Spline Interpolation Default: 3
    - OVERLAP [`choice`] : Overlapping Areas. Available Choices: [0] first [1] last [2] minimum [3] maximum [4] mean [5] blend boundary [6] feathering Default: 1
    - BLEND_DIST [`floating point number`] : Blending Distance. Minimum: 0.000000 Default: 10.000000 blending distance given in map units
    - BLEND_BND [`choice`] : Blending Boundary. Available Choices: [0] valid data cells [1] grid boundaries [2] vertical grid boundaries [3] horizontal grid boundaries Default: 0 blending boundary for distance calculation
    - MATCH [`choice`] : Match. Available Choices: [0] none [1] match histogram of first grid in list [2] match histogram of overlapping area [3] regression Default: 0
    - TARGET_DEFINITION [`choice`] : Target Grid System. Available Choices: [0] user defined [1] grid or grid system Default: 0
    - TARGET_USER_SIZE [`floating point number`] : Cellsize. Minimum: 0.000000 Default: 1.000000
    - TARGET_USER_XMIN [`floating point number`] : West. Default: 0.000000
    - TARGET_USER_XMAX [`floating point number`] : East. Default: 100.000000
    - TARGET_USER_YMIN [`floating point number`] : South. Default: 0.000000
    - TARGET_USER_YMAX [`floating point number`] : North. Default: 100.000000
    - TARGET_USER_COLS [`integer number`] : Columns. Minimum: 1 Default: 101 Number of cells in East-West direction.
    - TARGET_USER_ROWS [`integer number`] : Rows. Minimum: 1 Default: 101 Number of cells in North-South direction.
    - TARGET_USER_FLAT [`boolean`] : Rounding. Default: 1 Round bounding coordinates to multiples of cell size. Ignored if cell size has decimal places.
    - TARGET_USER_FITS [`choice`] : Fit. Available Choices: [0] nodes [1] cells Default: 0
    - TARGET_SYSTEM [`grid system`] : Grid System

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_tools', '38', 'Mosaicking (Grid Collections)')
    if Tool.is_Okay():
        Tool.Set_Input ('GRIDS', GRIDS)
        Tool.Set_Input ('TARGET_TEMPLATE', TARGET_TEMPLATE)
        Tool.Set_Output('MOSAIC', MOSAIC)
        Tool.Set_Option('TYPE', TYPE)
        Tool.Set_Option('RESAMPLING', RESAMPLING)
        Tool.Set_Option('OVERLAP', OVERLAP)
        Tool.Set_Option('BLEND_DIST', BLEND_DIST)
        Tool.Set_Option('BLEND_BND', BLEND_BND)
        Tool.Set_Option('MATCH', MATCH)
        Tool.Set_Option('TARGET_DEFINITION', TARGET_DEFINITION)
        Tool.Set_Option('TARGET_USER_SIZE', TARGET_USER_SIZE)
        Tool.Set_Option('TARGET_USER_XMIN', TARGET_USER_XMIN)
        Tool.Set_Option('TARGET_USER_XMAX', TARGET_USER_XMAX)
        Tool.Set_Option('TARGET_USER_YMIN', TARGET_USER_YMIN)
        Tool.Set_Option('TARGET_USER_YMAX', TARGET_USER_YMAX)
        Tool.Set_Option('TARGET_USER_COLS', TARGET_USER_COLS)
        Tool.Set_Option('TARGET_USER_ROWS', TARGET_USER_ROWS)
        Tool.Set_Option('TARGET_USER_FLAT', TARGET_USER_FLAT)
        Tool.Set_Option('TARGET_USER_FITS', TARGET_USER_FITS)
        Tool.Set_Option('TARGET_SYSTEM', TARGET_SYSTEM)
        return Tool.Execute(Verbose)
    return False

def Change_Grid_Values__Flood_Fill(POINTS=None, GRID=None, GRID_OUT=None, REPLACE=None, REPLACE_VALUE=None, TOLERANCE=None, IGNORE_NODATA=None, FILL_NODATA=None, FILL_VALUE=None, Verbose=2):
    '''
    Change Grid Values - Flood Fill
    ----------
    [grid_tools.39]\n
    A flood fill algorithm will be used for replacement of grid cell values starting at the positions of the input points. If one or more points are selected, only these will be processed, otherwise all. If the target grid is not set, the changes will be applied to the original grid.\n
    Arguments
    ----------
    - POINTS [`input shapes`] : Points
    - GRID [`input grid`] : Grid
    - GRID_OUT [`output grid`] : Changed Grid
    - REPLACE [`choice`] : Value to be replaced. Available Choices: [0] value at mouse position [1] fixed value Default: 0
    - REPLACE_VALUE [`floating point number`] : Fixed value to be replaced. Default: 0.000000 Replace only this value with respect to the specified tolerance.
    - TOLERANCE [`floating point number`] : Tolerance. Minimum: 0.000000 Default: 1.000000
    - IGNORE_NODATA [`boolean`] : Ignore No-Data. Default: 1 Do not fill areas representing no-data.
    - FILL_NODATA [`boolean`] : Fill with No-Data. Default: 0
    - FILL_VALUE [`floating point number`] : Fill Value. Default: 0.000000

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_tools', '39', 'Change Grid Values - Flood Fill')
    if Tool.is_Okay():
        Tool.Set_Input ('POINTS', POINTS)
        Tool.Set_Input ('GRID', GRID)
        Tool.Set_Output('GRID_OUT', GRID_OUT)
        Tool.Set_Option('REPLACE', REPLACE)
        Tool.Set_Option('REPLACE_VALUE', REPLACE_VALUE)
        Tool.Set_Option('TOLERANCE', TOLERANCE)
        Tool.Set_Option('IGNORE_NODATA', IGNORE_NODATA)
        Tool.Set_Option('FILL_NODATA', FILL_NODATA)
        Tool.Set_Option('FILL_VALUE', FILL_VALUE)
        return Tool.Execute(Verbose)
    return False

def run_tool_grid_tools_39(POINTS=None, GRID=None, GRID_OUT=None, REPLACE=None, REPLACE_VALUE=None, TOLERANCE=None, IGNORE_NODATA=None, FILL_NODATA=None, FILL_VALUE=None, Verbose=2):
    '''
    Change Grid Values - Flood Fill
    ----------
    [grid_tools.39]\n
    A flood fill algorithm will be used for replacement of grid cell values starting at the positions of the input points. If one or more points are selected, only these will be processed, otherwise all. If the target grid is not set, the changes will be applied to the original grid.\n
    Arguments
    ----------
    - POINTS [`input shapes`] : Points
    - GRID [`input grid`] : Grid
    - GRID_OUT [`output grid`] : Changed Grid
    - REPLACE [`choice`] : Value to be replaced. Available Choices: [0] value at mouse position [1] fixed value Default: 0
    - REPLACE_VALUE [`floating point number`] : Fixed value to be replaced. Default: 0.000000 Replace only this value with respect to the specified tolerance.
    - TOLERANCE [`floating point number`] : Tolerance. Minimum: 0.000000 Default: 1.000000
    - IGNORE_NODATA [`boolean`] : Ignore No-Data. Default: 1 Do not fill areas representing no-data.
    - FILL_NODATA [`boolean`] : Fill with No-Data. Default: 0
    - FILL_VALUE [`floating point number`] : Fill Value. Default: 0.000000

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_tools', '39', 'Change Grid Values - Flood Fill')
    if Tool.is_Okay():
        Tool.Set_Input ('POINTS', POINTS)
        Tool.Set_Input ('GRID', GRID)
        Tool.Set_Output('GRID_OUT', GRID_OUT)
        Tool.Set_Option('REPLACE', REPLACE)
        Tool.Set_Option('REPLACE_VALUE', REPLACE_VALUE)
        Tool.Set_Option('TOLERANCE', TOLERANCE)
        Tool.Set_Option('IGNORE_NODATA', IGNORE_NODATA)
        Tool.Set_Option('FILL_NODATA', FILL_NODATA)
        Tool.Set_Option('FILL_VALUE', FILL_VALUE)
        return Tool.Execute(Verbose)
    return False

def Shrink_and_Expand_Grid_Collection(INPUT=None, RESULT=None, OPERATION=None, CIRCLE=None, RADIUS=None, EXPAND=None, KEEP_TYPE=None, ITERATIVE=None, Verbose=2):
    '''
    Shrink and Expand (Grid Collection)
    ----------
    [grid_tools.41]\n
    This is the multi-raster version of the 'Shrink and Expand' tool for single grids and applies the tool operation to each grid provided by the input grid collection.\n
    With this tool you can shrink and/or expand regions with valid data by a certain distance defined by the (kernel) radius. Shrinking just invalidates all (valid) data cells found within the given distance to no-data cells, while expansion replaces no-data cells with new values based on the evaluation of all (valid) data cells found within the neighbourhood as defined by the kernel. Both operations can be combined.\n
    The method for the value expansion can be chosen as minimum, maximum, mean or majority value. The neighbourhood can be evaluated either at once or in a stepwise iterative way.\n
    Arguments
    ----------
    - INPUT [`input grid collection`] : Input
    - RESULT [`output grid collection`] : Result
    - OPERATION [`choice`] : Operation. Available Choices: [0] shrink [1] expand [2] shrink and expand [3] expand and shrink Default: 3
    - CIRCLE [`choice`] : Search Mode. Available Choices: [0] Square [1] Circle Default: 1 Choose the shape of the kernel.
    - RADIUS [`integer number`] : Radius. Minimum: 1 Default: 1 The kernel radius [cells].
    - EXPAND [`choice`] : Method. Available Choices: [0] minimum [1] maximum [2] mean [3] majority Default: 3 Choose how to fill no-data cells.
    - KEEP_TYPE [`boolean`] : Preserve Data Type. Default: 1 If false, mean value expansion results will be stored with floating point precision.
    - ITERATIVE [`boolean`] : Iterative Expansion. Default: 0 If false, the neighbourhood for expansion is evaluated in one step, else expansion is done stepwise with a one cell radius for each iteration until desired kernel radius is reached.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_tools', '41', 'Shrink and Expand (Grid Collection)')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Output('RESULT', RESULT)
        Tool.Set_Option('OPERATION', OPERATION)
        Tool.Set_Option('CIRCLE', CIRCLE)
        Tool.Set_Option('RADIUS', RADIUS)
        Tool.Set_Option('EXPAND', EXPAND)
        Tool.Set_Option('KEEP_TYPE', KEEP_TYPE)
        Tool.Set_Option('ITERATIVE', ITERATIVE)
        return Tool.Execute(Verbose)
    return False

def run_tool_grid_tools_41(INPUT=None, RESULT=None, OPERATION=None, CIRCLE=None, RADIUS=None, EXPAND=None, KEEP_TYPE=None, ITERATIVE=None, Verbose=2):
    '''
    Shrink and Expand (Grid Collection)
    ----------
    [grid_tools.41]\n
    This is the multi-raster version of the 'Shrink and Expand' tool for single grids and applies the tool operation to each grid provided by the input grid collection.\n
    With this tool you can shrink and/or expand regions with valid data by a certain distance defined by the (kernel) radius. Shrinking just invalidates all (valid) data cells found within the given distance to no-data cells, while expansion replaces no-data cells with new values based on the evaluation of all (valid) data cells found within the neighbourhood as defined by the kernel. Both operations can be combined.\n
    The method for the value expansion can be chosen as minimum, maximum, mean or majority value. The neighbourhood can be evaluated either at once or in a stepwise iterative way.\n
    Arguments
    ----------
    - INPUT [`input grid collection`] : Input
    - RESULT [`output grid collection`] : Result
    - OPERATION [`choice`] : Operation. Available Choices: [0] shrink [1] expand [2] shrink and expand [3] expand and shrink Default: 3
    - CIRCLE [`choice`] : Search Mode. Available Choices: [0] Square [1] Circle Default: 1 Choose the shape of the kernel.
    - RADIUS [`integer number`] : Radius. Minimum: 1 Default: 1 The kernel radius [cells].
    - EXPAND [`choice`] : Method. Available Choices: [0] minimum [1] maximum [2] mean [3] majority Default: 3 Choose how to fill no-data cells.
    - KEEP_TYPE [`boolean`] : Preserve Data Type. Default: 1 If false, mean value expansion results will be stored with floating point precision.
    - ITERATIVE [`boolean`] : Iterative Expansion. Default: 0 If false, the neighbourhood for expansion is evaluated in one step, else expansion is done stepwise with a one cell radius for each iteration until desired kernel radius is reached.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_tools', '41', 'Shrink and Expand (Grid Collection)')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Output('RESULT', RESULT)
        Tool.Set_Option('OPERATION', OPERATION)
        Tool.Set_Option('CIRCLE', CIRCLE)
        Tool.Set_Option('RADIUS', RADIUS)
        Tool.Set_Option('EXPAND', EXPAND)
        Tool.Set_Option('KEEP_TYPE', KEEP_TYPE)
        Tool.Set_Option('ITERATIVE', ITERATIVE)
        return Tool.Execute(Verbose)
    return False

def Change_Grid_System(GRID=None, OUT=None, METHOD=None, X=None, Y=None, Verbose=2):
    '''
    Change Grid System
    ----------
    [grid_tools.42]\n
    This tool changes the grid system by assigning new origin coordinates (lower left corner).\n
    (-) Set Origin: Defines the new lower left corner.\n
    (-) Shift Origin: Moves the origin in the given direction.\n
    Arguments
    ----------
    - GRID [`input grid`] : Grid
    - OUT [`output data object`] : Grid
    - METHOD [`choice`] : Method. Available Choices: [0] Set Origin [1] Shift Origin Default: 0
    - X [`floating point number`] : X. Default: 0.000000
    - Y [`floating point number`] : Y. Default: 0.000000

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_tools', '42', 'Change Grid System')
    if Tool.is_Okay():
        Tool.Set_Input ('GRID', GRID)
        Tool.Set_Output('OUT', OUT)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('X', X)
        Tool.Set_Option('Y', Y)
        return Tool.Execute(Verbose)
    return False

def run_tool_grid_tools_42(GRID=None, OUT=None, METHOD=None, X=None, Y=None, Verbose=2):
    '''
    Change Grid System
    ----------
    [grid_tools.42]\n
    This tool changes the grid system by assigning new origin coordinates (lower left corner).\n
    (-) Set Origin: Defines the new lower left corner.\n
    (-) Shift Origin: Moves the origin in the given direction.\n
    Arguments
    ----------
    - GRID [`input grid`] : Grid
    - OUT [`output data object`] : Grid
    - METHOD [`choice`] : Method. Available Choices: [0] Set Origin [1] Shift Origin Default: 0
    - X [`floating point number`] : X. Default: 0.000000
    - Y [`floating point number`] : Y. Default: 0.000000

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_tools', '42', 'Change Grid System')
    if Tool.is_Okay():
        Tool.Set_Input ('GRID', GRID)
        Tool.Set_Output('OUT', OUT)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('X', X)
        Tool.Set_Option('Y', Y)
        return Tool.Execute(Verbose)
    return False

def Change_a_Grids_NoData_Value_Bulk_Processing(GRIDS=None, OUTPUT=None, COPY=None, TYPE=None, VALUE=None, RANGE=None, CHANGE=None, Verbose=2):
    '''
    Change a Grid's No-Data Value [Bulk Processing]
    ----------
    [grid_tools.bulk_nodata_change]\n
    This is the bulk processing version of the likewise named tool for single grid processing.\n
    Arguments
    ----------
    - GRIDS [`input grid list`] : Grids
    - OUTPUT [`output grid list`] : Changed Grids
    - COPY [`boolean`] : Create Copies. Default: 1
    - TYPE [`choice`] : Type. Available Choices: [0] single value [1] value range Default: 0
    - VALUE [`floating point number`] : No-Data Value. Default: -99999.000000
    - RANGE [`value range`] : No-Data Value Range
    - CHANGE [`boolean`] : Change Values. Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_tools', 'bulk_nodata_change', 'Change a Grid\'s No-Data Value [Bulk Processing]')
    if Tool.is_Okay():
        Tool.Set_Input ('GRIDS', GRIDS)
        Tool.Set_Output('OUTPUT', OUTPUT)
        Tool.Set_Option('COPY', COPY)
        Tool.Set_Option('TYPE', TYPE)
        Tool.Set_Option('VALUE', VALUE)
        Tool.Set_Option('RANGE', RANGE)
        Tool.Set_Option('CHANGE', CHANGE)
        return Tool.Execute(Verbose)
    return False

def run_tool_grid_tools_bulk_nodata_change(GRIDS=None, OUTPUT=None, COPY=None, TYPE=None, VALUE=None, RANGE=None, CHANGE=None, Verbose=2):
    '''
    Change a Grid's No-Data Value [Bulk Processing]
    ----------
    [grid_tools.bulk_nodata_change]\n
    This is the bulk processing version of the likewise named tool for single grid processing.\n
    Arguments
    ----------
    - GRIDS [`input grid list`] : Grids
    - OUTPUT [`output grid list`] : Changed Grids
    - COPY [`boolean`] : Create Copies. Default: 1
    - TYPE [`choice`] : Type. Available Choices: [0] single value [1] value range Default: 0
    - VALUE [`floating point number`] : No-Data Value. Default: -99999.000000
    - RANGE [`value range`] : No-Data Value Range
    - CHANGE [`boolean`] : Change Values. Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_tools', 'bulk_nodata_change', 'Change a Grid\'s No-Data Value [Bulk Processing]')
    if Tool.is_Okay():
        Tool.Set_Input ('GRIDS', GRIDS)
        Tool.Set_Output('OUTPUT', OUTPUT)
        Tool.Set_Option('COPY', COPY)
        Tool.Set_Option('TYPE', TYPE)
        Tool.Set_Option('VALUE', VALUE)
        Tool.Set_Option('RANGE', RANGE)
        Tool.Set_Option('CHANGE', CHANGE)
        return Tool.Execute(Verbose)
    return False

def Two_Grids_Weighted_Mean_Bulk_Processing(GRIDS_A=None, GRIDS_B=None, MEAN=None, GRID_SYSTEM=None, RATIO=None, Verbose=2):
    '''
    Two Grids Weighted Mean [Bulk Processing]
    ----------
    [grid_tools.grid_tools_bulk_weighted_mean]\n
    This is a bulk processing for the calculation of two grids weighted mean.\n
    Arguments
    ----------
    - GRIDS_A [`input grid list`] : List of 1st Grids
    - GRIDS_B [`input grid list`] : List of 2nd Grids
    - MEAN [`output grid list`] : Weighted Mean
    - GRID_SYSTEM [`grid system`] : Grid System
    - RATIO [`floating point number`] : Weighting Ratio. Default: 0.500000 Weighting ratio comparing 1st and 2nd grid.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_tools', 'grid_tools_bulk_weighted_mean', 'Two Grids Weighted Mean [Bulk Processing]')
    if Tool.is_Okay():
        Tool.Set_Input ('GRIDS_A', GRIDS_A)
        Tool.Set_Input ('GRIDS_B', GRIDS_B)
        Tool.Set_Output('MEAN', MEAN)
        Tool.Set_Option('GRID_SYSTEM', GRID_SYSTEM)
        Tool.Set_Option('RATIO', RATIO)
        return Tool.Execute(Verbose)
    return False

def run_tool_grid_tools_grid_tools_bulk_weighted_mean(GRIDS_A=None, GRIDS_B=None, MEAN=None, GRID_SYSTEM=None, RATIO=None, Verbose=2):
    '''
    Two Grids Weighted Mean [Bulk Processing]
    ----------
    [grid_tools.grid_tools_bulk_weighted_mean]\n
    This is a bulk processing for the calculation of two grids weighted mean.\n
    Arguments
    ----------
    - GRIDS_A [`input grid list`] : List of 1st Grids
    - GRIDS_B [`input grid list`] : List of 2nd Grids
    - MEAN [`output grid list`] : Weighted Mean
    - GRID_SYSTEM [`grid system`] : Grid System
    - RATIO [`floating point number`] : Weighting Ratio. Default: 0.500000 Weighting ratio comparing 1st and 2nd grid.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_tools', 'grid_tools_bulk_weighted_mean', 'Two Grids Weighted Mean [Bulk Processing]')
    if Tool.is_Okay():
        Tool.Set_Input ('GRIDS_A', GRIDS_A)
        Tool.Set_Input ('GRIDS_B', GRIDS_B)
        Tool.Set_Output('MEAN', MEAN)
        Tool.Set_Option('GRID_SYSTEM', GRID_SYSTEM)
        Tool.Set_Option('RATIO', RATIO)
        return Tool.Execute(Verbose)
    return False

