#! /usr/bin/env python

'''
Python Interface to SAGA Tools Library
----------
- Category : Tool Chains
- Name     : Travel Time Analysis
- ID       : tta_tools

Description
----------
		The travel time tools were created to join together and automate a number of processing steps in order to simplify service access modelling. The purpose of these tools is to create a simple open source way of calculating travel time to services to facilitate rapid testing of multiple travel scenarios.

		Two tools have been created using  SAGA GIS Tool Chains. The SAGA Tool Chains capability uses a simple xml scripting code that allows the joining of multiple SAGA processes. The two tools produced are (1) for land cover grid creation and (2) travel time calculation, the latter requiring the output of the first. Separating the two tools allows for simpler and faster execution to test multiple scenarios  with the Travel Time Grid creation tool without having to recalculate the base land cover grid each analysis iteration.

		Find further information at Rohan Fisher's web page on [Travel Time Modelling with SAGA](http://rohanfisher.wordpress.com/travel-time-modelling-saga/).
	
'''

from PySAGA.helper import Tool_Wrapper

def Land_Cover_Scenario_Offset(ROAD_LINES=None, DEM=None, VEGETATION=None, CHANNEL_LINES=None, LANDCOVER=None, ROAD_CLASS=None, CHANNEL_ORDER=None, DEM_SYSTEM=None, LANDCOVER_SYSTEM=None, Verbose=2):
    '''
    Land Cover Scenario Offset
    ----------
    [tta_tools.tta_LandCover]\n
    Prepare your scenario for subsequent travel time analysis with information on topography, road network, and vegetation cover.\n
    Topography is used for automated channel network delineation. Resulting channels are categorized by their Strahler order. In the final land cover map channels use numbers above 100 (i.e. '101, 102, 103, ...' representing increasing Strahler orders). Likewise roads use numbers above 200 with the exact number based on the attribute chosen to specify the road type (i.e. 201, 202, 203, ...). Vegetation cover takes the original numbers for the final map, so these should not intermingle with numbers used for channel and road representation.\n
    Find further information at Rohan Fisher's web page on [Travel Time Modelling with SAGA](http://rohanfisher.wordpress.com/travel-time-modelling-saga/).\n
    \n
    Arguments
    ----------
    - ROAD_LINES [`input shapes`] : Roads
    - DEM [`input grid`] : Elevation
    - VEGETATION [`input grid`] : Vegetation
    - CHANNEL_LINES [`optional input shapes`] : Channel Network
    - LANDCOVER [`output grid`] : Land Cover
    - ROAD_CLASS [`table field`] : Attribute. attribute specifying road's type with regard to travel time
    - CHANNEL_ORDER [`table field`] : Stream Order
    - DEM_SYSTEM [`grid system`] : Grid System
    - LANDCOVER_SYSTEM [`grid system`] : Grid System

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('tta_tools', 'tta_LandCover', 'Land Cover Scenario Offset')
    if Tool.is_Okay():
        Tool.Set_Input ('ROAD_LINES', ROAD_LINES)
        Tool.Set_Input ('DEM', DEM)
        Tool.Set_Input ('VEGETATION', VEGETATION)
        Tool.Set_Input ('CHANNEL_LINES', CHANNEL_LINES)
        Tool.Set_Output('LANDCOVER', LANDCOVER)
        Tool.Set_Option('ROAD_CLASS', ROAD_CLASS)
        Tool.Set_Option('CHANNEL_ORDER', CHANNEL_ORDER)
        Tool.Set_Option('DEM_SYSTEM', DEM_SYSTEM)
        Tool.Set_Option('LANDCOVER_SYSTEM', LANDCOVER_SYSTEM)
        return Tool.Execute(Verbose)
    return False

def run_tool_tta_tools_tta_LandCover(ROAD_LINES=None, DEM=None, VEGETATION=None, CHANNEL_LINES=None, LANDCOVER=None, ROAD_CLASS=None, CHANNEL_ORDER=None, DEM_SYSTEM=None, LANDCOVER_SYSTEM=None, Verbose=2):
    '''
    Land Cover Scenario Offset
    ----------
    [tta_tools.tta_LandCover]\n
    Prepare your scenario for subsequent travel time analysis with information on topography, road network, and vegetation cover.\n
    Topography is used for automated channel network delineation. Resulting channels are categorized by their Strahler order. In the final land cover map channels use numbers above 100 (i.e. '101, 102, 103, ...' representing increasing Strahler orders). Likewise roads use numbers above 200 with the exact number based on the attribute chosen to specify the road type (i.e. 201, 202, 203, ...). Vegetation cover takes the original numbers for the final map, so these should not intermingle with numbers used for channel and road representation.\n
    Find further information at Rohan Fisher's web page on [Travel Time Modelling with SAGA](http://rohanfisher.wordpress.com/travel-time-modelling-saga/).\n
    \n
    Arguments
    ----------
    - ROAD_LINES [`input shapes`] : Roads
    - DEM [`input grid`] : Elevation
    - VEGETATION [`input grid`] : Vegetation
    - CHANNEL_LINES [`optional input shapes`] : Channel Network
    - LANDCOVER [`output grid`] : Land Cover
    - ROAD_CLASS [`table field`] : Attribute. attribute specifying road's type with regard to travel time
    - CHANNEL_ORDER [`table field`] : Stream Order
    - DEM_SYSTEM [`grid system`] : Grid System
    - LANDCOVER_SYSTEM [`grid system`] : Grid System

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('tta_tools', 'tta_LandCover', 'Land Cover Scenario Offset')
    if Tool.is_Okay():
        Tool.Set_Input ('ROAD_LINES', ROAD_LINES)
        Tool.Set_Input ('DEM', DEM)
        Tool.Set_Input ('VEGETATION', VEGETATION)
        Tool.Set_Input ('CHANNEL_LINES', CHANNEL_LINES)
        Tool.Set_Output('LANDCOVER', LANDCOVER)
        Tool.Set_Option('ROAD_CLASS', ROAD_CLASS)
        Tool.Set_Option('CHANNEL_ORDER', CHANNEL_ORDER)
        Tool.Set_Option('DEM_SYSTEM', DEM_SYSTEM)
        Tool.Set_Option('LANDCOVER_SYSTEM', LANDCOVER_SYSTEM)
        return Tool.Execute(Verbose)
    return False

def Travel_Time_Calculation(DESTINATIONS=None, LANDCOVER=None, LANDCOVER_TO_TRAVELTIME=None, TRAVELTIME_LUT=None, TRAVELTIME_MINUTES=None, GRID_SYSTEM=None, FIELD_LANDCOVER=None, FIELD_TRAVELTIME=None, Verbose=2):
    '''
    Travel Time Calculation
    ----------
    [tta_tools.tta_TravelTime]\n
    Perform travel time calculation.\n
    Find further information at Rohan Fisher's web page on [Travel Time Modelling with SAGA](http://rohanfisher.wordpress.com/travel-time-modelling-saga/).\n
    \n
    Arguments
    ----------
    - DESTINATIONS [`input shapes`] : Destination Points
    - LANDCOVER [`input grid`] : Land Cover
    - LANDCOVER_TO_TRAVELTIME [`input table`] : Travel Times. This table supplies average travel times through a cell (in seconds) for each land cover type
    - TRAVELTIME_LUT [`optional input table`] : Travel Time Zones Classification. Look-up table for classification of travel time zones.
    - TRAVELTIME_MINUTES [`output grid`] : Travel Time
    - GRID_SYSTEM [`grid system`] : Grid System
    - FIELD_LANDCOVER [`table field`] : Land Cover ID
    - FIELD_TRAVELTIME [`table field`] : Travel Time

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('tta_tools', 'tta_TravelTime', 'Travel Time Calculation')
    if Tool.is_Okay():
        Tool.Set_Input ('DESTINATIONS', DESTINATIONS)
        Tool.Set_Input ('LANDCOVER', LANDCOVER)
        Tool.Set_Input ('LANDCOVER_TO_TRAVELTIME', LANDCOVER_TO_TRAVELTIME)
        Tool.Set_Input ('TRAVELTIME_LUT', TRAVELTIME_LUT)
        Tool.Set_Output('TRAVELTIME_MINUTES', TRAVELTIME_MINUTES)
        Tool.Set_Option('GRID_SYSTEM', GRID_SYSTEM)
        Tool.Set_Option('FIELD_LANDCOVER', FIELD_LANDCOVER)
        Tool.Set_Option('FIELD_TRAVELTIME', FIELD_TRAVELTIME)
        return Tool.Execute(Verbose)
    return False

def run_tool_tta_tools_tta_TravelTime(DESTINATIONS=None, LANDCOVER=None, LANDCOVER_TO_TRAVELTIME=None, TRAVELTIME_LUT=None, TRAVELTIME_MINUTES=None, GRID_SYSTEM=None, FIELD_LANDCOVER=None, FIELD_TRAVELTIME=None, Verbose=2):
    '''
    Travel Time Calculation
    ----------
    [tta_tools.tta_TravelTime]\n
    Perform travel time calculation.\n
    Find further information at Rohan Fisher's web page on [Travel Time Modelling with SAGA](http://rohanfisher.wordpress.com/travel-time-modelling-saga/).\n
    \n
    Arguments
    ----------
    - DESTINATIONS [`input shapes`] : Destination Points
    - LANDCOVER [`input grid`] : Land Cover
    - LANDCOVER_TO_TRAVELTIME [`input table`] : Travel Times. This table supplies average travel times through a cell (in seconds) for each land cover type
    - TRAVELTIME_LUT [`optional input table`] : Travel Time Zones Classification. Look-up table for classification of travel time zones.
    - TRAVELTIME_MINUTES [`output grid`] : Travel Time
    - GRID_SYSTEM [`grid system`] : Grid System
    - FIELD_LANDCOVER [`table field`] : Land Cover ID
    - FIELD_TRAVELTIME [`table field`] : Travel Time

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('tta_tools', 'tta_TravelTime', 'Travel Time Calculation')
    if Tool.is_Okay():
        Tool.Set_Input ('DESTINATIONS', DESTINATIONS)
        Tool.Set_Input ('LANDCOVER', LANDCOVER)
        Tool.Set_Input ('LANDCOVER_TO_TRAVELTIME', LANDCOVER_TO_TRAVELTIME)
        Tool.Set_Input ('TRAVELTIME_LUT', TRAVELTIME_LUT)
        Tool.Set_Output('TRAVELTIME_MINUTES', TRAVELTIME_MINUTES)
        Tool.Set_Option('GRID_SYSTEM', GRID_SYSTEM)
        Tool.Set_Option('FIELD_LANDCOVER', FIELD_LANDCOVER)
        Tool.Set_Option('FIELD_TRAVELTIME', FIELD_TRAVELTIME)
        return Tool.Execute(Verbose)
    return False

