#! /usr/bin/env python

'''
Python Interface to SAGA Tools Library
----------
- Category : Import/Export
- Name     : GDAL/OGR
- ID       : io_gdal

Description
----------
Interface to Frank Warmerdam's Geospatial Data Abstraction Library (GDAL).
Version 3.9.2
Homepage: [www.gdal.org](http://www.gdal.org/)

'''

from PySAGA.helper import Tool_Wrapper

def Import_Raster(EXTENT_SHAPES=None, GRIDS=None, FILES=None, MULTIPLE=None, SUBSETS=None, SELECTION=None, TRANSFORM=None, RESAMPLING=None, EXTENT=None, EXTENT_XMIN=None, EXTENT_XMAX=None, EXTENT_YMIN=None, EXTENT_YMAX=None, EXTENT_GRID=None, EXTENT_BUFFER=None, Verbose=2):
    '''
    Import Raster
    ----------
    [io_gdal.0]\n
    The "GDAL Raster Import" tool imports grid data from various file formats using the "Geospatial Data Abstraction Library" (GDAL) by Frank Warmerdam.\n
    GDAL Version:3.9.2\n
    Following raster formats are currently supported:\n
    ============\n
    Name	ID	Extension\n
    ACE2	ACE2	ACE2\n
    ARC Digitized Raster Graphics	ADRG	gen\n
    ASCII Gridded XYZ	XYZ	xyz\n
    AirSAR Polarimetric Image	AirSAR\n
    Airbus DS Intelligence Data As A Service driver	DAAS\n
    Arc/Info ASCII Grid	AAIGrid	asc\n
    Arc/Info Binary Grid	AIG\n
    AutoCAD Driver	CAD	dwg\n
    Bathymetry Attributed Grid	BAG	bag\n
    CALS (Type 1)	CALS\n
    CEOS Image	CEOS\n
    CEOS SAR Image	SAR_CEOS\n
    COSAR Annotated Binary Matrix (TerraSAR-X)	COSAR\n
    CTable2 Datum Grid Shift	CTable2\n
    Convair PolGASP	CPG\n
    DIPEx	DIPEx\n
    DRDC COASP SAR Processor Raster	COASP	hdr\n
    DTED Elevation Raster	DTED\n
    Derived datasets using VRT pixel functions	DERIVED\n
    ECRG TOC format	ECRGTOC	xml\n
    ELAS	ELAS\n
    ENVI .hdr Labelled	ENVI\n
    EOSAT FAST Format	FAST\n
    ERMapper .ers Labelled	ERS	ers\n
    ESRI .hdr Labelled	EHdr	bil\n
    ESRI FileGDB	OpenFileGDB	gdb\n
    EUMETSAT Archive native (.nat)	MSGN	nat\n
    Earth Engine Data API Image	EEDAI\n
    EarthWatch .TIL	TIL\n
    Envisat Image Format	ESAT	n1\n
    Erdas .LAN/.GIS	LAN\n
    Erdas Imagine Images (.img)	HFA	img\n
    Erdas Imagine Raw	EIR\n
    Esri Compact Cache	ESRIC\n
    FARSITE v.4 Landscape File (.lcp)	LCP	lcp\n
    FIT Image	FIT\n
    Flexible Image Transport System	FITS	fits\n
    GDAL Raster Tile Index	GTI\n
    GRASS ASCII Grid	GRASSASCIIGrid\n
    GRIdded Binary (.grb, .grb2)	GRIB	grb grb2 grib2\n
    GSC Geogrid	GSC\n
    Generic Binary (.hdr Labelled)	GenBin\n
    GeoPackage	GPKG\n
    GeoSoft Grid Exchange Format	GXF	gxf\n
    GeoTIFF	GTiff	tif\n
    Geospatial PDF	PDF	pdf\n
    Golden Software 7 Binary Grid (.grd)	GS7BG	grd\n
    Golden Software ASCII Grid (.grd)	GSAG	grd\n
    Golden Software Binary Grid (.grd)	GSBG	grd\n
    Graphics Interchange Format (.gif)	GIF	gif\n
    Ground-based SAR Applications Testbed File Format (.gff)	GFF	gff\n
    HDF4 Dataset	HDF4Image\n
    HDF5 Dataset	HDF5Image\n
    HF2/HFZ heightfield raster	HF2	hf2\n
    HTTP Fetching Wrapper	HTTP\n
    Hierarchical Data Format Release 4	HDF4	hdf\n
    Hierarchical Data Format Release 5	HDF5	h5 hdf5\n
    ILWIS Raster Map	ILWIS\n
    IRIS data (.PPI, .CAPPi etc)	IRIS	ppi\n
    ISCE raster	ISCE\n
    Idrisi Raster A.1	RST	rst\n
    In Memory Raster	MEM\n
    International Service for the Geoid	ISG	isg\n
    JAXA PALSAR Product Reader (Level 1.1/1.5)	JAXAPALSAR\n
    JPEG JFIF	JPEG	jpg\n
    JPEG-2000 driver based on JP2OpenJPEG library	JP2OpenJPEG	jp2\n
    Japanese DEM (.mem)	JDEM	mem\n
    KEA Image Format (.kea)	KEA	kea\n
    KOLOR Raw	KRO	kro\n
    Kml Super Overlay	KMLSUPEROVERLAY\n
    Leveller heightfield	Leveller	ter\n
    MBTiles	MBTiles	mbtiles\n
    MIPL VICAR file	VICAR\n
    MS Windows Device Independent Bitmap	BMP	bmp\n
    Magellan topo (.blx)	BLX	blx\n
    Maptech BSB Nautical Charts	BSB	kap\n
    Meta Raster Format	MRF	mrf\n
    NADCON .los/.las Datum Grid Shift	LOSLAS\n
    NASA Planetary Data System	PDS\n
    NASA Planetary Data System 4	PDS4	xml\n
    NLAPS Data Format	NDF\n
    NOAA GEOCON/NADCON5 .b format	NOAA_B	b\n
    NOAA NGS Geoid Height Grids	NGSGEOID	bin\n
    NOAA Polar Orbiter Level 1b Data Set	L1B\n
    NOAA Vertical Datum .GTX	GTX	gtx\n
    NSIDC Sea Ice Concentrations binary (.bin)	NSIDCbin	bin\n
    NTv2 Datum Grid Shift	NTv2\n
    National Imagery Transmission Format	NITF	ntf\n
    Natural Resources Canada's Geoid	BYN\n
    Network Common Data Format	netCDF	nc\n
    NextGIS Web	NGW\n
    Northwood Classified Grid Format .grc/.tab	NWT_GRC	grc\n
    Northwood Numeric Grid Format .grd/.tab	NWT_GRD	grd\n
    OGC Web Coverage Service	WCS\n
    OGC Web Map Service	WMS\n
    OGC Web Map Tile Service	WMTS\n
    OGCAPI	OGCAPI\n
    OziExplorer .MAP	MAP\n
    OziExplorer Image File	OZI\n
    PCI .aux Labelled	PAux\n
    PCIDSK Database File	PCIDSK	pix\n
    PCRaster Raster File	PCRaster	map\n
    Planet Labs Mosaics API	PLMOSAIC\n
    Planet Labs Scenes API	PLSCENES\n
    Portable Network Graphics	PNG	png\n
    Portable Pixmap Format (netpbm)	PNM\n
    PostGIS Raster driver	PostGISRaster\n
    R Object Data Store	R	rda\n
    R Raster	RRASTER	grd\n
    ROI_PAC raster	ROI_PAC\n
    Racurs PHOTOMOD PRF	PRF	prf\n
    RadarSat 2 XML Product	RS2\n
    Raster Matrix Format	RMF	rsw\n
    Raster Product Format TOC format	RPFTOC	toc\n
    Rasterlite	Rasterlite	sqlite\n
    S-102 Bathymetric Surface Product	S102	h5\n
    S-104 Water Level Information for Surface Navigation Product	S104	h5\n
    SAGA GIS Binary Grid (.sdat, .sg-grd-z)	SAGA\n
    SDTS Raster	SDTS	ddf\n
    SGI Image File Format 1.0	SGI	rgb\n
    SPOT DIMAP	DIMAP\n
    SRTMHGT File Format	SRTMHGT	hgt\n
    Scaled Integer Gridded DEM .sigdem	SIGDEM	sigdem\n
    Sentinel 2	SENTINEL2\n
    Sentinel-1 SAR SAFE Product	SAFE\n
    Snow Data Assimilation System	SNODAS	hdr\n
    Spatio-Temporal Asset Catalog Items	STACIT\n
    Spatio-Temporal Asset Catalog Tiled Assets	STACTA	json\n
    Standard Raster Product (ASRP/USRP)	SRP	img\n
    Surface Currents Product	S111	h5\n
    Swedish Grid RIK (.rik)	RIK	rik\n
    TGA/TARGA Image File Format	TGA	tga\n
    TerraSAR-X Product	TSX\n
    Terragen heightfield	Terragen	ter\n
    TileDB	TileDB\n
    USGS Astrogeology ISIS cube (Version 2)	ISIS2\n
    USGS Astrogeology ISIS cube (Version 3)	ISIS3\n
    USGS DOQ (New Style)	DOQ2\n
    USGS DOQ (Old Style)	DOQ1\n
    USGS LULC Composite Theme Grid	CTG\n
    USGS Optional ASCII DEM (and CDED)	USGSDEM	dem\n
    VTP .bt (Binary Terrain) 1.3 Format	BT	bt\n
    Vexcel MFF Raster	MFF	hdr\n
    Vexcel MFF2 (HKV) Raster	MFF2\n
    Virtual Raster	VRT	vrt\n
    WEBP	WEBP	webp\n
    X11 PixMap Format	XPM	xpm\n
    ZMap Plus Grid	ZMap	dat\n
    Zarr	Zarr\n
    ============\n
    Arguments
    ----------
    - EXTENT_SHAPES [`input shapes`] : Shapes Extent
    - GRIDS [`output grid list`] : Grids
    - FILES [`file path`] : Files
    - MULTIPLE [`choice`] : Multiple Bands Output. Available Choices: [0] single grids [1] grid collection [2] automatic Default: 2
    - SUBSETS [`text`] : Subsets. Semicolon separated list of subset names or indexes (zero-based). If empty (default) all subsets will be imported (if there are any).
    - SELECTION [`text`] : Select from Multiple Bands. Semicolon separated list of band indexes (zero-based). If empty (default) all bands will be imported.
    - TRANSFORM [`boolean`] : Transformation. Default: 1 align grid to coordinate system
    - RESAMPLING [`choice`] : Resampling. Available Choices: [0] Nearest Neighbour [1] Bilinear Interpolation [2] Bicubic Spline Interpolation [3] B-Spline Interpolation Default: 0 Resampling type to be used, if grid needs to be aligned to coordinate system.
    - EXTENT [`choice`] : Extent. Available Choices: [0] original [1] user defined [2] grid system [3] shapes extent Default: 0
    - EXTENT_XMIN [`floating point number`] : Left. Default: 0.000000
    - EXTENT_XMAX [`floating point number`] : Right. Default: 0.000000
    - EXTENT_YMIN [`floating point number`] : Bottom. Default: 0.000000
    - EXTENT_YMAX [`floating point number`] : Top. Default: 0.000000
    - EXTENT_GRID [`grid system`] : Grid System
    - EXTENT_BUFFER [`floating point number`] : Buffer. Minimum: 0.000000 Default: 0.000000

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('io_gdal', '0', 'Import Raster')
    if Tool.is_Okay():
        Tool.Set_Input ('EXTENT_SHAPES', EXTENT_SHAPES)
        Tool.Set_Output('GRIDS', GRIDS)
        Tool.Set_Option('FILES', FILES)
        Tool.Set_Option('MULTIPLE', MULTIPLE)
        Tool.Set_Option('SUBSETS', SUBSETS)
        Tool.Set_Option('SELECTION', SELECTION)
        Tool.Set_Option('TRANSFORM', TRANSFORM)
        Tool.Set_Option('RESAMPLING', RESAMPLING)
        Tool.Set_Option('EXTENT', EXTENT)
        Tool.Set_Option('EXTENT_XMIN', EXTENT_XMIN)
        Tool.Set_Option('EXTENT_XMAX', EXTENT_XMAX)
        Tool.Set_Option('EXTENT_YMIN', EXTENT_YMIN)
        Tool.Set_Option('EXTENT_YMAX', EXTENT_YMAX)
        Tool.Set_Option('EXTENT_GRID', EXTENT_GRID)
        Tool.Set_Option('EXTENT_BUFFER', EXTENT_BUFFER)
        return Tool.Execute(Verbose)
    return False

def run_tool_io_gdal_0(EXTENT_SHAPES=None, GRIDS=None, FILES=None, MULTIPLE=None, SUBSETS=None, SELECTION=None, TRANSFORM=None, RESAMPLING=None, EXTENT=None, EXTENT_XMIN=None, EXTENT_XMAX=None, EXTENT_YMIN=None, EXTENT_YMAX=None, EXTENT_GRID=None, EXTENT_BUFFER=None, Verbose=2):
    '''
    Import Raster
    ----------
    [io_gdal.0]\n
    The "GDAL Raster Import" tool imports grid data from various file formats using the "Geospatial Data Abstraction Library" (GDAL) by Frank Warmerdam.\n
    GDAL Version:3.9.2\n
    Following raster formats are currently supported:\n
    ============\n
    Name	ID	Extension\n
    ACE2	ACE2	ACE2\n
    ARC Digitized Raster Graphics	ADRG	gen\n
    ASCII Gridded XYZ	XYZ	xyz\n
    AirSAR Polarimetric Image	AirSAR\n
    Airbus DS Intelligence Data As A Service driver	DAAS\n
    Arc/Info ASCII Grid	AAIGrid	asc\n
    Arc/Info Binary Grid	AIG\n
    AutoCAD Driver	CAD	dwg\n
    Bathymetry Attributed Grid	BAG	bag\n
    CALS (Type 1)	CALS\n
    CEOS Image	CEOS\n
    CEOS SAR Image	SAR_CEOS\n
    COSAR Annotated Binary Matrix (TerraSAR-X)	COSAR\n
    CTable2 Datum Grid Shift	CTable2\n
    Convair PolGASP	CPG\n
    DIPEx	DIPEx\n
    DRDC COASP SAR Processor Raster	COASP	hdr\n
    DTED Elevation Raster	DTED\n
    Derived datasets using VRT pixel functions	DERIVED\n
    ECRG TOC format	ECRGTOC	xml\n
    ELAS	ELAS\n
    ENVI .hdr Labelled	ENVI\n
    EOSAT FAST Format	FAST\n
    ERMapper .ers Labelled	ERS	ers\n
    ESRI .hdr Labelled	EHdr	bil\n
    ESRI FileGDB	OpenFileGDB	gdb\n
    EUMETSAT Archive native (.nat)	MSGN	nat\n
    Earth Engine Data API Image	EEDAI\n
    EarthWatch .TIL	TIL\n
    Envisat Image Format	ESAT	n1\n
    Erdas .LAN/.GIS	LAN\n
    Erdas Imagine Images (.img)	HFA	img\n
    Erdas Imagine Raw	EIR\n
    Esri Compact Cache	ESRIC\n
    FARSITE v.4 Landscape File (.lcp)	LCP	lcp\n
    FIT Image	FIT\n
    Flexible Image Transport System	FITS	fits\n
    GDAL Raster Tile Index	GTI\n
    GRASS ASCII Grid	GRASSASCIIGrid\n
    GRIdded Binary (.grb, .grb2)	GRIB	grb grb2 grib2\n
    GSC Geogrid	GSC\n
    Generic Binary (.hdr Labelled)	GenBin\n
    GeoPackage	GPKG\n
    GeoSoft Grid Exchange Format	GXF	gxf\n
    GeoTIFF	GTiff	tif\n
    Geospatial PDF	PDF	pdf\n
    Golden Software 7 Binary Grid (.grd)	GS7BG	grd\n
    Golden Software ASCII Grid (.grd)	GSAG	grd\n
    Golden Software Binary Grid (.grd)	GSBG	grd\n
    Graphics Interchange Format (.gif)	GIF	gif\n
    Ground-based SAR Applications Testbed File Format (.gff)	GFF	gff\n
    HDF4 Dataset	HDF4Image\n
    HDF5 Dataset	HDF5Image\n
    HF2/HFZ heightfield raster	HF2	hf2\n
    HTTP Fetching Wrapper	HTTP\n
    Hierarchical Data Format Release 4	HDF4	hdf\n
    Hierarchical Data Format Release 5	HDF5	h5 hdf5\n
    ILWIS Raster Map	ILWIS\n
    IRIS data (.PPI, .CAPPi etc)	IRIS	ppi\n
    ISCE raster	ISCE\n
    Idrisi Raster A.1	RST	rst\n
    In Memory Raster	MEM\n
    International Service for the Geoid	ISG	isg\n
    JAXA PALSAR Product Reader (Level 1.1/1.5)	JAXAPALSAR\n
    JPEG JFIF	JPEG	jpg\n
    JPEG-2000 driver based on JP2OpenJPEG library	JP2OpenJPEG	jp2\n
    Japanese DEM (.mem)	JDEM	mem\n
    KEA Image Format (.kea)	KEA	kea\n
    KOLOR Raw	KRO	kro\n
    Kml Super Overlay	KMLSUPEROVERLAY\n
    Leveller heightfield	Leveller	ter\n
    MBTiles	MBTiles	mbtiles\n
    MIPL VICAR file	VICAR\n
    MS Windows Device Independent Bitmap	BMP	bmp\n
    Magellan topo (.blx)	BLX	blx\n
    Maptech BSB Nautical Charts	BSB	kap\n
    Meta Raster Format	MRF	mrf\n
    NADCON .los/.las Datum Grid Shift	LOSLAS\n
    NASA Planetary Data System	PDS\n
    NASA Planetary Data System 4	PDS4	xml\n
    NLAPS Data Format	NDF\n
    NOAA GEOCON/NADCON5 .b format	NOAA_B	b\n
    NOAA NGS Geoid Height Grids	NGSGEOID	bin\n
    NOAA Polar Orbiter Level 1b Data Set	L1B\n
    NOAA Vertical Datum .GTX	GTX	gtx\n
    NSIDC Sea Ice Concentrations binary (.bin)	NSIDCbin	bin\n
    NTv2 Datum Grid Shift	NTv2\n
    National Imagery Transmission Format	NITF	ntf\n
    Natural Resources Canada's Geoid	BYN\n
    Network Common Data Format	netCDF	nc\n
    NextGIS Web	NGW\n
    Northwood Classified Grid Format .grc/.tab	NWT_GRC	grc\n
    Northwood Numeric Grid Format .grd/.tab	NWT_GRD	grd\n
    OGC Web Coverage Service	WCS\n
    OGC Web Map Service	WMS\n
    OGC Web Map Tile Service	WMTS\n
    OGCAPI	OGCAPI\n
    OziExplorer .MAP	MAP\n
    OziExplorer Image File	OZI\n
    PCI .aux Labelled	PAux\n
    PCIDSK Database File	PCIDSK	pix\n
    PCRaster Raster File	PCRaster	map\n
    Planet Labs Mosaics API	PLMOSAIC\n
    Planet Labs Scenes API	PLSCENES\n
    Portable Network Graphics	PNG	png\n
    Portable Pixmap Format (netpbm)	PNM\n
    PostGIS Raster driver	PostGISRaster\n
    R Object Data Store	R	rda\n
    R Raster	RRASTER	grd\n
    ROI_PAC raster	ROI_PAC\n
    Racurs PHOTOMOD PRF	PRF	prf\n
    RadarSat 2 XML Product	RS2\n
    Raster Matrix Format	RMF	rsw\n
    Raster Product Format TOC format	RPFTOC	toc\n
    Rasterlite	Rasterlite	sqlite\n
    S-102 Bathymetric Surface Product	S102	h5\n
    S-104 Water Level Information for Surface Navigation Product	S104	h5\n
    SAGA GIS Binary Grid (.sdat, .sg-grd-z)	SAGA\n
    SDTS Raster	SDTS	ddf\n
    SGI Image File Format 1.0	SGI	rgb\n
    SPOT DIMAP	DIMAP\n
    SRTMHGT File Format	SRTMHGT	hgt\n
    Scaled Integer Gridded DEM .sigdem	SIGDEM	sigdem\n
    Sentinel 2	SENTINEL2\n
    Sentinel-1 SAR SAFE Product	SAFE\n
    Snow Data Assimilation System	SNODAS	hdr\n
    Spatio-Temporal Asset Catalog Items	STACIT\n
    Spatio-Temporal Asset Catalog Tiled Assets	STACTA	json\n
    Standard Raster Product (ASRP/USRP)	SRP	img\n
    Surface Currents Product	S111	h5\n
    Swedish Grid RIK (.rik)	RIK	rik\n
    TGA/TARGA Image File Format	TGA	tga\n
    TerraSAR-X Product	TSX\n
    Terragen heightfield	Terragen	ter\n
    TileDB	TileDB\n
    USGS Astrogeology ISIS cube (Version 2)	ISIS2\n
    USGS Astrogeology ISIS cube (Version 3)	ISIS3\n
    USGS DOQ (New Style)	DOQ2\n
    USGS DOQ (Old Style)	DOQ1\n
    USGS LULC Composite Theme Grid	CTG\n
    USGS Optional ASCII DEM (and CDED)	USGSDEM	dem\n
    VTP .bt (Binary Terrain) 1.3 Format	BT	bt\n
    Vexcel MFF Raster	MFF	hdr\n
    Vexcel MFF2 (HKV) Raster	MFF2\n
    Virtual Raster	VRT	vrt\n
    WEBP	WEBP	webp\n
    X11 PixMap Format	XPM	xpm\n
    ZMap Plus Grid	ZMap	dat\n
    Zarr	Zarr\n
    ============\n
    Arguments
    ----------
    - EXTENT_SHAPES [`input shapes`] : Shapes Extent
    - GRIDS [`output grid list`] : Grids
    - FILES [`file path`] : Files
    - MULTIPLE [`choice`] : Multiple Bands Output. Available Choices: [0] single grids [1] grid collection [2] automatic Default: 2
    - SUBSETS [`text`] : Subsets. Semicolon separated list of subset names or indexes (zero-based). If empty (default) all subsets will be imported (if there are any).
    - SELECTION [`text`] : Select from Multiple Bands. Semicolon separated list of band indexes (zero-based). If empty (default) all bands will be imported.
    - TRANSFORM [`boolean`] : Transformation. Default: 1 align grid to coordinate system
    - RESAMPLING [`choice`] : Resampling. Available Choices: [0] Nearest Neighbour [1] Bilinear Interpolation [2] Bicubic Spline Interpolation [3] B-Spline Interpolation Default: 0 Resampling type to be used, if grid needs to be aligned to coordinate system.
    - EXTENT [`choice`] : Extent. Available Choices: [0] original [1] user defined [2] grid system [3] shapes extent Default: 0
    - EXTENT_XMIN [`floating point number`] : Left. Default: 0.000000
    - EXTENT_XMAX [`floating point number`] : Right. Default: 0.000000
    - EXTENT_YMIN [`floating point number`] : Bottom. Default: 0.000000
    - EXTENT_YMAX [`floating point number`] : Top. Default: 0.000000
    - EXTENT_GRID [`grid system`] : Grid System
    - EXTENT_BUFFER [`floating point number`] : Buffer. Minimum: 0.000000 Default: 0.000000

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('io_gdal', '0', 'Import Raster')
    if Tool.is_Okay():
        Tool.Set_Input ('EXTENT_SHAPES', EXTENT_SHAPES)
        Tool.Set_Output('GRIDS', GRIDS)
        Tool.Set_Option('FILES', FILES)
        Tool.Set_Option('MULTIPLE', MULTIPLE)
        Tool.Set_Option('SUBSETS', SUBSETS)
        Tool.Set_Option('SELECTION', SELECTION)
        Tool.Set_Option('TRANSFORM', TRANSFORM)
        Tool.Set_Option('RESAMPLING', RESAMPLING)
        Tool.Set_Option('EXTENT', EXTENT)
        Tool.Set_Option('EXTENT_XMIN', EXTENT_XMIN)
        Tool.Set_Option('EXTENT_XMAX', EXTENT_XMAX)
        Tool.Set_Option('EXTENT_YMIN', EXTENT_YMIN)
        Tool.Set_Option('EXTENT_YMAX', EXTENT_YMAX)
        Tool.Set_Option('EXTENT_GRID', EXTENT_GRID)
        Tool.Set_Option('EXTENT_BUFFER', EXTENT_BUFFER)
        return Tool.Execute(Verbose)
    return False

def Export_Raster(GRIDS=None, MULTIPLE=None, FOLDER=None, EXTENSION=None, FILE=None, FORMAT=None, TYPE=None, SET_NODATA=None, NODATA=None, OPTIONS=None, Verbose=2):
    '''
    Export Raster
    ----------
    [io_gdal.1]\n
    The "GDAL Raster Export" tool exports one or more grids to various file formats using the "Geospatial Data Abstraction Library" (GDAL) by Frank Warmerdam.\n
    GDAL Version:3.9.2\n
    Following raster formats are currently supported:\n
    ============\n
    Name	ID	Extension\n
    ARC Digitized Raster Graphics	ADRG	gen\n
    Bathymetry Attributed Grid	BAG	bag\n
    CTable2 Datum Grid Shift	CTable2\n
    ELAS	ELAS\n
    ENVI .hdr Labelled	ENVI\n
    ERMapper .ers Labelled	ERS	ers\n
    ESRI .hdr Labelled	EHdr	bil\n
    ESRI FileGDB	OpenFileGDB	gdb\n
    Erdas .LAN/.GIS	LAN\n
    Erdas Imagine Images (.img)	HFA	img\n
    Flexible Image Transport System	FITS	fits\n
    GeoPackage	GPKG\n
    GeoTIFF	GTiff	tif\n
    Geospatial PDF	PDF	pdf\n
    Golden Software 7 Binary Grid (.grd)	GS7BG	grd\n
    Golden Software Binary Grid (.grd)	GSBG	grd\n
    HDF4 Dataset	HDF4Image\n
    ILWIS Raster Map	ILWIS\n
    ISCE raster	ISCE\n
    Idrisi Raster A.1	RST	rst\n
    In Memory Raster	MEM\n
    KEA Image Format (.kea)	KEA	kea\n
    KOLOR Raw	KRO	kro\n
    Leveller heightfield	Leveller	ter\n
    MBTiles	MBTiles	mbtiles\n
    MIPL VICAR file	VICAR\n
    MS Windows Device Independent Bitmap	BMP	bmp\n
    Meta Raster Format	MRF	mrf\n
    NASA Planetary Data System 4	PDS4	xml\n
    NOAA Vertical Datum .GTX	GTX	gtx\n
    NTv2 Datum Grid Shift	NTv2\n
    National Imagery Transmission Format	NITF	ntf\n
    Natural Resources Canada's Geoid	BYN\n
    Network Common Data Format	netCDF	nc\n
    NextGIS Web	NGW\n
    Northwood Numeric Grid Format .grd/.tab	NWT_GRD	grd\n
    PCI .aux Labelled	PAux\n
    PCIDSK Database File	PCIDSK	pix\n
    PCRaster Raster File	PCRaster	map\n
    Portable Pixmap Format (netpbm)	PNM\n
    R Raster	RRASTER	grd\n
    ROI_PAC raster	ROI_PAC\n
    Raster Matrix Format	RMF	rsw\n
    SAGA GIS Binary Grid (.sdat, .sg-grd-z)	SAGA\n
    SGI Image File Format 1.0	SGI	rgb\n
    Terragen heightfield	Terragen	ter\n
    TileDB	TileDB\n
    USGS Astrogeology ISIS cube (Version 2)	ISIS2\n
    USGS Astrogeology ISIS cube (Version 3)	ISIS3\n
    VTP .bt (Binary Terrain) 1.3 Format	BT	bt\n
    Vexcel MFF Raster	MFF	hdr\n
    Vexcel MFF2 (HKV) Raster	MFF2\n
    Virtual Raster	VRT	vrt\n
    Zarr	Zarr\n
    ============\n
    Arguments
    ----------
    - GRIDS [`input grid list`] : Grid(s)
    - MULTIPLE [`choice`] : Multiple. Available Choices: [0] single files [1] one file Default: 1 If multiple grids are supplied: export each grid to a separate file or all grids to one multi-band file. Notice: Storing multiple bands in one file might not be supported by the selected format. For single files the grid name is used as file name.
    - FOLDER [`file path`] : Folder. The folder location to which single files will be stored.
    - EXTENSION [`text`] : Extension
    - FILE [`file path`] : File. The GDAL dataset to be created.
    - FORMAT [`choice`] : Format. Available Choices: [0] ARC Digitized Raster Graphics [1] Bathymetry Attributed Grid [2] CTable2 Datum Grid Shift [3] ELAS [4] ENVI .hdr Labelled [5] ERMapper .ers Labelled [6] ESRI .hdr Labelled [7] ESRI FileGDB [8] Erdas .LAN/.GIS [9] Erdas Imagine Images (.img) [10] Flexible Image Transport System [11] GeoPackage [12] GeoTIFF [13] Geospatial PDF [14] Golden Software 7 Binary Grid (.grd) [15] Golden Software Binary Grid (.grd) [16] HDF4 Dataset [17] ILWIS Raster Map [18] ISCE raster [19] Idrisi Raster A.1 [20] In Memory Raster [21] KEA Image Format (.kea) [22] KOLOR Raw [23] Leveller heightfield [24] MBTiles [25] MIPL VICAR file [26] MS Windows Device Independent Bitmap [27] Meta Raster Format [28] NASA Planetary Data System 4 [29] NOAA Vertical Datum .GTX [30] NTv2 Datum Grid Shift [31] National Imagery Transmission Format [32] Natural Resources Canada's Geoid [33] Network Common Data Format [34] NextGIS Web [35] Northwood Numeric Grid Format .grd/.tab [36] PCI .aux Labelled [37] PCIDSK Database File [38] PCRaster Raster File [39] Portable Pixmap Format (netpbm) [40] R Raster [41] ROI_PAC raster [42] Raster Matrix Format [43] SAGA GIS Binary Grid (.sdat, .sg-grd-z) [44] SGI Image File Format 1.0 [45] Terragen heightfield [46] TileDB [47] USGS Astrogeology ISIS cube (Version 2) [48] USGS Astrogeology ISIS cube (Version 3) [49] VTP .bt (Binary Terrain) 1.3 Format [50] Vexcel MFF Raster [51] Vexcel MFF2 (HKV) Raster [52] Virtual Raster [53] Zarr Default: 0 The GDAL raster format (driver) to be used.
    - TYPE [`choice`] : Data Type. Available Choices: [0] match input data [1] 8 bit unsigned integer [2] 16 bit unsigned integer [3] 16 bit signed integer [4] 32 bit unsigned integer [5] 32 bit signed integer [6] 32 bit floating point [7] 64 bit floating point Default: 0 The GDAL datatype of the created dataset.
    - SET_NODATA [`boolean`] : Set Custom NoData. Default: 0
    - NODATA [`floating point number`] : NoData Value. Default: 0.000000
    - OPTIONS [`text`] : Creation Options. A space separated list of key-value pairs (K=V).

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('io_gdal', '1', 'Export Raster')
    if Tool.is_Okay():
        Tool.Set_Input ('GRIDS', GRIDS)
        Tool.Set_Option('MULTIPLE', MULTIPLE)
        Tool.Set_Option('FOLDER', FOLDER)
        Tool.Set_Option('EXTENSION', EXTENSION)
        Tool.Set_Option('FILE', FILE)
        Tool.Set_Option('FORMAT', FORMAT)
        Tool.Set_Option('TYPE', TYPE)
        Tool.Set_Option('SET_NODATA', SET_NODATA)
        Tool.Set_Option('NODATA', NODATA)
        Tool.Set_Option('OPTIONS', OPTIONS)
        return Tool.Execute(Verbose)
    return False

def run_tool_io_gdal_1(GRIDS=None, MULTIPLE=None, FOLDER=None, EXTENSION=None, FILE=None, FORMAT=None, TYPE=None, SET_NODATA=None, NODATA=None, OPTIONS=None, Verbose=2):
    '''
    Export Raster
    ----------
    [io_gdal.1]\n
    The "GDAL Raster Export" tool exports one or more grids to various file formats using the "Geospatial Data Abstraction Library" (GDAL) by Frank Warmerdam.\n
    GDAL Version:3.9.2\n
    Following raster formats are currently supported:\n
    ============\n
    Name	ID	Extension\n
    ARC Digitized Raster Graphics	ADRG	gen\n
    Bathymetry Attributed Grid	BAG	bag\n
    CTable2 Datum Grid Shift	CTable2\n
    ELAS	ELAS\n
    ENVI .hdr Labelled	ENVI\n
    ERMapper .ers Labelled	ERS	ers\n
    ESRI .hdr Labelled	EHdr	bil\n
    ESRI FileGDB	OpenFileGDB	gdb\n
    Erdas .LAN/.GIS	LAN\n
    Erdas Imagine Images (.img)	HFA	img\n
    Flexible Image Transport System	FITS	fits\n
    GeoPackage	GPKG\n
    GeoTIFF	GTiff	tif\n
    Geospatial PDF	PDF	pdf\n
    Golden Software 7 Binary Grid (.grd)	GS7BG	grd\n
    Golden Software Binary Grid (.grd)	GSBG	grd\n
    HDF4 Dataset	HDF4Image\n
    ILWIS Raster Map	ILWIS\n
    ISCE raster	ISCE\n
    Idrisi Raster A.1	RST	rst\n
    In Memory Raster	MEM\n
    KEA Image Format (.kea)	KEA	kea\n
    KOLOR Raw	KRO	kro\n
    Leveller heightfield	Leveller	ter\n
    MBTiles	MBTiles	mbtiles\n
    MIPL VICAR file	VICAR\n
    MS Windows Device Independent Bitmap	BMP	bmp\n
    Meta Raster Format	MRF	mrf\n
    NASA Planetary Data System 4	PDS4	xml\n
    NOAA Vertical Datum .GTX	GTX	gtx\n
    NTv2 Datum Grid Shift	NTv2\n
    National Imagery Transmission Format	NITF	ntf\n
    Natural Resources Canada's Geoid	BYN\n
    Network Common Data Format	netCDF	nc\n
    NextGIS Web	NGW\n
    Northwood Numeric Grid Format .grd/.tab	NWT_GRD	grd\n
    PCI .aux Labelled	PAux\n
    PCIDSK Database File	PCIDSK	pix\n
    PCRaster Raster File	PCRaster	map\n
    Portable Pixmap Format (netpbm)	PNM\n
    R Raster	RRASTER	grd\n
    ROI_PAC raster	ROI_PAC\n
    Raster Matrix Format	RMF	rsw\n
    SAGA GIS Binary Grid (.sdat, .sg-grd-z)	SAGA\n
    SGI Image File Format 1.0	SGI	rgb\n
    Terragen heightfield	Terragen	ter\n
    TileDB	TileDB\n
    USGS Astrogeology ISIS cube (Version 2)	ISIS2\n
    USGS Astrogeology ISIS cube (Version 3)	ISIS3\n
    VTP .bt (Binary Terrain) 1.3 Format	BT	bt\n
    Vexcel MFF Raster	MFF	hdr\n
    Vexcel MFF2 (HKV) Raster	MFF2\n
    Virtual Raster	VRT	vrt\n
    Zarr	Zarr\n
    ============\n
    Arguments
    ----------
    - GRIDS [`input grid list`] : Grid(s)
    - MULTIPLE [`choice`] : Multiple. Available Choices: [0] single files [1] one file Default: 1 If multiple grids are supplied: export each grid to a separate file or all grids to one multi-band file. Notice: Storing multiple bands in one file might not be supported by the selected format. For single files the grid name is used as file name.
    - FOLDER [`file path`] : Folder. The folder location to which single files will be stored.
    - EXTENSION [`text`] : Extension
    - FILE [`file path`] : File. The GDAL dataset to be created.
    - FORMAT [`choice`] : Format. Available Choices: [0] ARC Digitized Raster Graphics [1] Bathymetry Attributed Grid [2] CTable2 Datum Grid Shift [3] ELAS [4] ENVI .hdr Labelled [5] ERMapper .ers Labelled [6] ESRI .hdr Labelled [7] ESRI FileGDB [8] Erdas .LAN/.GIS [9] Erdas Imagine Images (.img) [10] Flexible Image Transport System [11] GeoPackage [12] GeoTIFF [13] Geospatial PDF [14] Golden Software 7 Binary Grid (.grd) [15] Golden Software Binary Grid (.grd) [16] HDF4 Dataset [17] ILWIS Raster Map [18] ISCE raster [19] Idrisi Raster A.1 [20] In Memory Raster [21] KEA Image Format (.kea) [22] KOLOR Raw [23] Leveller heightfield [24] MBTiles [25] MIPL VICAR file [26] MS Windows Device Independent Bitmap [27] Meta Raster Format [28] NASA Planetary Data System 4 [29] NOAA Vertical Datum .GTX [30] NTv2 Datum Grid Shift [31] National Imagery Transmission Format [32] Natural Resources Canada's Geoid [33] Network Common Data Format [34] NextGIS Web [35] Northwood Numeric Grid Format .grd/.tab [36] PCI .aux Labelled [37] PCIDSK Database File [38] PCRaster Raster File [39] Portable Pixmap Format (netpbm) [40] R Raster [41] ROI_PAC raster [42] Raster Matrix Format [43] SAGA GIS Binary Grid (.sdat, .sg-grd-z) [44] SGI Image File Format 1.0 [45] Terragen heightfield [46] TileDB [47] USGS Astrogeology ISIS cube (Version 2) [48] USGS Astrogeology ISIS cube (Version 3) [49] VTP .bt (Binary Terrain) 1.3 Format [50] Vexcel MFF Raster [51] Vexcel MFF2 (HKV) Raster [52] Virtual Raster [53] Zarr Default: 0 The GDAL raster format (driver) to be used.
    - TYPE [`choice`] : Data Type. Available Choices: [0] match input data [1] 8 bit unsigned integer [2] 16 bit unsigned integer [3] 16 bit signed integer [4] 32 bit unsigned integer [5] 32 bit signed integer [6] 32 bit floating point [7] 64 bit floating point Default: 0 The GDAL datatype of the created dataset.
    - SET_NODATA [`boolean`] : Set Custom NoData. Default: 0
    - NODATA [`floating point number`] : NoData Value. Default: 0.000000
    - OPTIONS [`text`] : Creation Options. A space separated list of key-value pairs (K=V).

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('io_gdal', '1', 'Export Raster')
    if Tool.is_Okay():
        Tool.Set_Input ('GRIDS', GRIDS)
        Tool.Set_Option('MULTIPLE', MULTIPLE)
        Tool.Set_Option('FOLDER', FOLDER)
        Tool.Set_Option('EXTENSION', EXTENSION)
        Tool.Set_Option('FILE', FILE)
        Tool.Set_Option('FORMAT', FORMAT)
        Tool.Set_Option('TYPE', TYPE)
        Tool.Set_Option('SET_NODATA', SET_NODATA)
        Tool.Set_Option('NODATA', NODATA)
        Tool.Set_Option('OPTIONS', OPTIONS)
        return Tool.Execute(Verbose)
    return False

def Export_GeoTIFF(GRIDS=None, FILE=None, OPTIONS=None, Verbose=2):
    '''
    Export GeoTIFF
    ----------
    [io_gdal.2]\n
    The "GDAL GeoTIFF Export" tool exports one or more grids to a Geocoded Tagged Image File Format using the "Geospatial Data Abstraction Library" (GDAL) by Frank Warmerdam.\n
    Arguments
    ----------
    - GRIDS [`input grid list`] : Grid(s). The SAGA grids to be exported.
    - FILE [`file path`] : File. The GeoTIFF File to be created.
    - OPTIONS [`text`] : Creation Options. A space separated list of key-value pairs (K=V).

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('io_gdal', '2', 'Export GeoTIFF')
    if Tool.is_Okay():
        Tool.Set_Input ('GRIDS', GRIDS)
        Tool.Set_Option('FILE', FILE)
        Tool.Set_Option('OPTIONS', OPTIONS)
        return Tool.Execute(Verbose)
    return False

def run_tool_io_gdal_2(GRIDS=None, FILE=None, OPTIONS=None, Verbose=2):
    '''
    Export GeoTIFF
    ----------
    [io_gdal.2]\n
    The "GDAL GeoTIFF Export" tool exports one or more grids to a Geocoded Tagged Image File Format using the "Geospatial Data Abstraction Library" (GDAL) by Frank Warmerdam.\n
    Arguments
    ----------
    - GRIDS [`input grid list`] : Grid(s). The SAGA grids to be exported.
    - FILE [`file path`] : File. The GeoTIFF File to be created.
    - OPTIONS [`text`] : Creation Options. A space separated list of key-value pairs (K=V).

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('io_gdal', '2', 'Export GeoTIFF')
    if Tool.is_Okay():
        Tool.Set_Input ('GRIDS', GRIDS)
        Tool.Set_Option('FILE', FILE)
        Tool.Set_Option('OPTIONS', OPTIONS)
        return Tool.Execute(Verbose)
    return False

def Import_Shapes(SHAPES=None, FILES=None, GEOM_TYPE=None, Verbose=2):
    '''
    Import Shapes
    ----------
    [io_gdal.3]\n
    The "OGR Vector Data Import" tool imports vector data from various file/database formats using the "Geospatial Data Abstraction Library" (GDAL) by Frank Warmerdam.\n
    GDAL Version:3.9.2\n
    Following vector formats are currently supported:\n
    ============\n
    Name	ID	Extension\n
    ODBC\n
    AmigoCloud	AmigoCloud\n
    Arc/Info Binary Coverage	AVCBin\n
    Arc/Info E00 (ASCII) Coverage	AVCE00	e00\n
    AutoCAD DXF	DXF	dxf\n
    AutoCAD Driver	CAD	dwg\n
    Bathymetry Attributed Grid	BAG	bag\n
    Carto	Carto\n
    Comma Separated Value (.csv)	CSV\n
    Czech Cadastral Exchange Data Format	VFK	vfk\n
    ESRI FileGDB	OpenFileGDB	gdb\n
    ESRI Personal GeoDatabase	PGeo	mdb\n
    ESRI Shapefile	ESRI Shapefile	shp\n
    ESRIJSON	ESRIJSON	json\n
    Earth Engine Data API	EEDA\n
    Elastic Search	Elasticsearch\n
    FlatGeobuf	FlatGeobuf	fgb\n
    Flexible Image Transport System	FITS	fits\n
    French EDIGEO exchange format	EDIGEO	thf\n
    GMT ASCII Vectors (.gmt)	OGR_GMT	gmt\n
    GPSBabel	GPSBabel\n
    GPX	GPX	gpx\n
    General Transit Feed Specification	GTFS	zip\n
    GeoJSON	GeoJSON\n
    GeoJSON Sequence	GeoJSONSeq\n
    GeoPackage	GPKG\n
    GeoRSS	GeoRSS\n
    Geoconcept	Geoconcept\n
    Geography Markup Language (GML)	GML	gml\n
    Geography Markup Language (GML) driven by application schemas	GMLAS\n
    Geospatial PDF	PDF	pdf\n
    HTTP Fetching Wrapper	HTTP\n
    IHO S-57 (ENC)	S57	000\n
    Idrisi Vector (.vct)	Idrisi	vct\n
    Interlis 1	Interlis 1\n
    Interlis 2	Interlis 2\n
    JPEG-2000 driver based on JP2OpenJPEG library	JP2OpenJPEG	jp2\n
    Kadaster LV BAG Extract 2.0	LVBAG	xml\n
    Keyhole Markup Language (KML)	KML	kml\n
    Keyhole Markup Language (LIBKML)	LIBKML\n
    MBTiles	MBTiles	mbtiles\n
    MIPL VICAR file	VICAR\n
    MS Excel format	XLS	xls\n
    MS Office Open XML spreadsheet	XLSX\n
    MapInfo File	MapInfo File\n
    MapML	MapML\n
    Mapbox Vector Tiles	MVT\n
    Memory	Memory\n
    Microsoft SQL Server Spatial Database	MSSQLSpatial\n
    Microstation DGN	DGN	dgn\n
    MiraMon Vectors (.pol, .arc, .pnt)	MiraMonVector\n
    NAS - ALKIS	NAS	xml\n
    NASA Planetary Data System 4	PDS4	xml\n
    Network Common Data Format	netCDF	nc\n
    NextGIS Web	NGW\n
    OGC API - Features	OAPIF\n
    OGC CSW (Catalog  Service for the Web)	CSW\n
    OGC Features and Geometries JSON	JSONFG	json\n
    OGC WFS (Web Feature Service)	WFS\n
    OGCAPI	OGCAPI\n
    Open Document/ LibreOffice / OpenOffice Spreadsheet	ODS	ods\n
    OpenJUMP JML	JML	jml\n
    OpenStreetMap XML and PBF	OSM\n
    PCIDSK Database File	PCIDSK	pix\n
    Planet Labs Scenes API	PLSCENES\n
    Planetary Data Systems TABLE	OGR_PDS\n
    PostgreSQL SQL dump	PGDUMP	sql\n
    PostgreSQL/PostGIS	PostgreSQL\n
    ProtoMap Tiles	PMTiles	pmtiles\n
    SDTS	OGR_SDTS\n
    SQLite / Spatialite	SQLite\n
    Scalable Vector Graphics	SVG	svg\n
    Selafin	Selafin\n
    Storage and eXchange Format	SXF	sxf\n
    TileDB	TileDB\n
    TopoJSON	TopoJSON\n
    U.S. Census TIGER/Line	TIGER\n
    UK .NTF	UK .NTF\n
    VDV-451/VDV-452/INTREST Data Format	VDV\n
    VRT - Virtual Datasource	OGR_VRT	vrt\n
    WAsP .map format	WAsP	map\n
    ============\n
    Arguments
    ----------
    - SHAPES [`output shapes list`] : Shapes
    - FILES [`file path`] : Files
    - GEOM_TYPE [`choice`] : Geometry Type. Available Choices: [0] Automatic [1] Point [2] Point (2.5D) [3] Multi-Point [4] Multi-Point (2.5D) [5] Line [6] Line (2.5D) [7] Polyline [8] Polyline (2.5D) [9] Polygon [10] Polygon (2.5D) [11] Multi-Polygon [12] Multi-Polygon (2.5D) Default: 0 Some OGR drivers are unable to determine the geometry type automatically, please choose the appropriate one in this case

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('io_gdal', '3', 'Import Shapes')
    if Tool.is_Okay():
        Tool.Set_Output('SHAPES', SHAPES)
        Tool.Set_Option('FILES', FILES)
        Tool.Set_Option('GEOM_TYPE', GEOM_TYPE)
        return Tool.Execute(Verbose)
    return False

def run_tool_io_gdal_3(SHAPES=None, FILES=None, GEOM_TYPE=None, Verbose=2):
    '''
    Import Shapes
    ----------
    [io_gdal.3]\n
    The "OGR Vector Data Import" tool imports vector data from various file/database formats using the "Geospatial Data Abstraction Library" (GDAL) by Frank Warmerdam.\n
    GDAL Version:3.9.2\n
    Following vector formats are currently supported:\n
    ============\n
    Name	ID	Extension\n
    ODBC\n
    AmigoCloud	AmigoCloud\n
    Arc/Info Binary Coverage	AVCBin\n
    Arc/Info E00 (ASCII) Coverage	AVCE00	e00\n
    AutoCAD DXF	DXF	dxf\n
    AutoCAD Driver	CAD	dwg\n
    Bathymetry Attributed Grid	BAG	bag\n
    Carto	Carto\n
    Comma Separated Value (.csv)	CSV\n
    Czech Cadastral Exchange Data Format	VFK	vfk\n
    ESRI FileGDB	OpenFileGDB	gdb\n
    ESRI Personal GeoDatabase	PGeo	mdb\n
    ESRI Shapefile	ESRI Shapefile	shp\n
    ESRIJSON	ESRIJSON	json\n
    Earth Engine Data API	EEDA\n
    Elastic Search	Elasticsearch\n
    FlatGeobuf	FlatGeobuf	fgb\n
    Flexible Image Transport System	FITS	fits\n
    French EDIGEO exchange format	EDIGEO	thf\n
    GMT ASCII Vectors (.gmt)	OGR_GMT	gmt\n
    GPSBabel	GPSBabel\n
    GPX	GPX	gpx\n
    General Transit Feed Specification	GTFS	zip\n
    GeoJSON	GeoJSON\n
    GeoJSON Sequence	GeoJSONSeq\n
    GeoPackage	GPKG\n
    GeoRSS	GeoRSS\n
    Geoconcept	Geoconcept\n
    Geography Markup Language (GML)	GML	gml\n
    Geography Markup Language (GML) driven by application schemas	GMLAS\n
    Geospatial PDF	PDF	pdf\n
    HTTP Fetching Wrapper	HTTP\n
    IHO S-57 (ENC)	S57	000\n
    Idrisi Vector (.vct)	Idrisi	vct\n
    Interlis 1	Interlis 1\n
    Interlis 2	Interlis 2\n
    JPEG-2000 driver based on JP2OpenJPEG library	JP2OpenJPEG	jp2\n
    Kadaster LV BAG Extract 2.0	LVBAG	xml\n
    Keyhole Markup Language (KML)	KML	kml\n
    Keyhole Markup Language (LIBKML)	LIBKML\n
    MBTiles	MBTiles	mbtiles\n
    MIPL VICAR file	VICAR\n
    MS Excel format	XLS	xls\n
    MS Office Open XML spreadsheet	XLSX\n
    MapInfo File	MapInfo File\n
    MapML	MapML\n
    Mapbox Vector Tiles	MVT\n
    Memory	Memory\n
    Microsoft SQL Server Spatial Database	MSSQLSpatial\n
    Microstation DGN	DGN	dgn\n
    MiraMon Vectors (.pol, .arc, .pnt)	MiraMonVector\n
    NAS - ALKIS	NAS	xml\n
    NASA Planetary Data System 4	PDS4	xml\n
    Network Common Data Format	netCDF	nc\n
    NextGIS Web	NGW\n
    OGC API - Features	OAPIF\n
    OGC CSW (Catalog  Service for the Web)	CSW\n
    OGC Features and Geometries JSON	JSONFG	json\n
    OGC WFS (Web Feature Service)	WFS\n
    OGCAPI	OGCAPI\n
    Open Document/ LibreOffice / OpenOffice Spreadsheet	ODS	ods\n
    OpenJUMP JML	JML	jml\n
    OpenStreetMap XML and PBF	OSM\n
    PCIDSK Database File	PCIDSK	pix\n
    Planet Labs Scenes API	PLSCENES\n
    Planetary Data Systems TABLE	OGR_PDS\n
    PostgreSQL SQL dump	PGDUMP	sql\n
    PostgreSQL/PostGIS	PostgreSQL\n
    ProtoMap Tiles	PMTiles	pmtiles\n
    SDTS	OGR_SDTS\n
    SQLite / Spatialite	SQLite\n
    Scalable Vector Graphics	SVG	svg\n
    Selafin	Selafin\n
    Storage and eXchange Format	SXF	sxf\n
    TileDB	TileDB\n
    TopoJSON	TopoJSON\n
    U.S. Census TIGER/Line	TIGER\n
    UK .NTF	UK .NTF\n
    VDV-451/VDV-452/INTREST Data Format	VDV\n
    VRT - Virtual Datasource	OGR_VRT	vrt\n
    WAsP .map format	WAsP	map\n
    ============\n
    Arguments
    ----------
    - SHAPES [`output shapes list`] : Shapes
    - FILES [`file path`] : Files
    - GEOM_TYPE [`choice`] : Geometry Type. Available Choices: [0] Automatic [1] Point [2] Point (2.5D) [3] Multi-Point [4] Multi-Point (2.5D) [5] Line [6] Line (2.5D) [7] Polyline [8] Polyline (2.5D) [9] Polygon [10] Polygon (2.5D) [11] Multi-Polygon [12] Multi-Polygon (2.5D) Default: 0 Some OGR drivers are unable to determine the geometry type automatically, please choose the appropriate one in this case

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('io_gdal', '3', 'Import Shapes')
    if Tool.is_Okay():
        Tool.Set_Output('SHAPES', SHAPES)
        Tool.Set_Option('FILES', FILES)
        Tool.Set_Option('GEOM_TYPE', GEOM_TYPE)
        return Tool.Execute(Verbose)
    return False

def Export_Shapes(SHAPES=None, FILE=None, FORMAT=None, OPTIONS=None, LAYER_OPTIONS=None, Verbose=2):
    '''
    Export Shapes
    ----------
    [io_gdal.4]\n
    The "OGR Vector Data Export" tool exports vector data to various file formats using the "Geospatial Data Abstraction Library" (GDAL) by Frank Warmerdam.\n
    GDAL Version:3.9.2\n
    Following vector formats are currently supported:\n
    ============\n
    Name	ID	Extension\n
    AmigoCloud	AmigoCloud\n
    AutoCAD DXF	DXF	dxf\n
    Bathymetry Attributed Grid	BAG	bag\n
    Carto	Carto\n
    Comma Separated Value (.csv)	CSV\n
    ESRI FileGDB	OpenFileGDB	gdb\n
    ESRI Shapefile	ESRI Shapefile	shp\n
    Elastic Search	Elasticsearch\n
    FlatGeobuf	FlatGeobuf	fgb\n
    Flexible Image Transport System	FITS	fits\n
    GMT ASCII Vectors (.gmt)	OGR_GMT	gmt\n
    GPSBabel	GPSBabel\n
    GPX	GPX	gpx\n
    GeoJSON	GeoJSON\n
    GeoJSON Sequence	GeoJSONSeq\n
    GeoPackage	GPKG\n
    GeoRSS	GeoRSS\n
    Geoconcept	Geoconcept\n
    Geography Markup Language (GML)	GML	gml\n
    Geospatial PDF	PDF	pdf\n
    IHO S-57 (ENC)	S57	000\n
    Interlis 1	Interlis 1\n
    Interlis 2	Interlis 2\n
    Keyhole Markup Language (KML)	KML	kml\n
    Keyhole Markup Language (LIBKML)	LIBKML\n
    MBTiles	MBTiles	mbtiles\n
    MIPL VICAR file	VICAR\n
    MS Office Open XML spreadsheet	XLSX\n
    MapInfo File	MapInfo File\n
    MapML	MapML\n
    Mapbox Vector Tiles	MVT\n
    Memory	Memory\n
    Microsoft SQL Server Spatial Database	MSSQLSpatial\n
    Microstation DGN	DGN	dgn\n
    MiraMon Vectors (.pol, .arc, .pnt)	MiraMonVector\n
    NASA Planetary Data System 4	PDS4	xml\n
    Network Common Data Format	netCDF	nc\n
    NextGIS Web	NGW\n
    OGC Features and Geometries JSON	JSONFG	json\n
    Open Document/ LibreOffice / OpenOffice Spreadsheet	ODS	ods\n
    OpenJUMP JML	JML	jml\n
    PCIDSK Database File	PCIDSK	pix\n
    PostgreSQL SQL dump	PGDUMP	sql\n
    PostgreSQL/PostGIS	PostgreSQL\n
    ProtoMap Tiles	PMTiles	pmtiles\n
    SQLite / Spatialite	SQLite\n
    Selafin	Selafin\n
    TileDB	TileDB\n
    VDV-451/VDV-452/INTREST Data Format	VDV\n
    WAsP .map format	WAsP	map\n
    ============\n
    Arguments
    ----------
    - SHAPES [`input shapes`] : Shapes
    - FILE [`file path`] : File
    - FORMAT [`choice`] : Format. Available Choices: [0] AmigoCloud [1] AutoCAD DXF [2] Bathymetry Attributed Grid [3] Carto [4] Comma Separated Value (.csv) [5] ESRI FileGDB [6] ESRI Shapefile [7] Elastic Search [8] FlatGeobuf [9] Flexible Image Transport System [10] GMT ASCII Vectors (.gmt) [11] GPSBabel [12] GPX [13] GeoJSON [14] GeoJSON Sequence [15] GeoPackage [16] GeoRSS [17] Geoconcept [18] Geography Markup Language (GML) [19] Geospatial PDF [20] IHO S-57 (ENC) [21] Interlis 1 [22] Interlis 2 [23] Keyhole Markup Language (KML) [24] Keyhole Markup Language (LIBKML) [25] MBTiles [26] MIPL VICAR file [27] MS Office Open XML spreadsheet [28] MapInfo File [29] MapML [30] Mapbox Vector Tiles [31] Memory [32] Microsoft SQL Server Spatial Database [33] Microstation DGN [34] MiraMon Vectors (.pol, .arc, .pnt) [35] NASA Planetary Data System 4 [36] Network Common Data Format [37] NextGIS Web [38] OGC Features and Geometries JSON [39] Open Document/ LibreOffice / OpenOffice Spreadsheet [40] OpenJUMP JML [41] PCIDSK Database File [42] PostgreSQL SQL dump [43] PostgreSQL/PostGIS [44] ProtoMap Tiles [45] SQLite / Spatialite [46] Selafin [47] TileDB [48] VDV-451/VDV-452/INTREST Data Format [49] WAsP .map format Default: 0
    - OPTIONS [`text`] : Creation Options. The dataset creation options. A space separated list of key-value pairs (K=V).
    - LAYER_OPTIONS [`text`] : Layer Creation Options. The layer creation options. A space separated list of key-value pairs (K=V).

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('io_gdal', '4', 'Export Shapes')
    if Tool.is_Okay():
        Tool.Set_Input ('SHAPES', SHAPES)
        Tool.Set_Option('FILE', FILE)
        Tool.Set_Option('FORMAT', FORMAT)
        Tool.Set_Option('OPTIONS', OPTIONS)
        Tool.Set_Option('LAYER_OPTIONS', LAYER_OPTIONS)
        return Tool.Execute(Verbose)
    return False

def run_tool_io_gdal_4(SHAPES=None, FILE=None, FORMAT=None, OPTIONS=None, LAYER_OPTIONS=None, Verbose=2):
    '''
    Export Shapes
    ----------
    [io_gdal.4]\n
    The "OGR Vector Data Export" tool exports vector data to various file formats using the "Geospatial Data Abstraction Library" (GDAL) by Frank Warmerdam.\n
    GDAL Version:3.9.2\n
    Following vector formats are currently supported:\n
    ============\n
    Name	ID	Extension\n
    AmigoCloud	AmigoCloud\n
    AutoCAD DXF	DXF	dxf\n
    Bathymetry Attributed Grid	BAG	bag\n
    Carto	Carto\n
    Comma Separated Value (.csv)	CSV\n
    ESRI FileGDB	OpenFileGDB	gdb\n
    ESRI Shapefile	ESRI Shapefile	shp\n
    Elastic Search	Elasticsearch\n
    FlatGeobuf	FlatGeobuf	fgb\n
    Flexible Image Transport System	FITS	fits\n
    GMT ASCII Vectors (.gmt)	OGR_GMT	gmt\n
    GPSBabel	GPSBabel\n
    GPX	GPX	gpx\n
    GeoJSON	GeoJSON\n
    GeoJSON Sequence	GeoJSONSeq\n
    GeoPackage	GPKG\n
    GeoRSS	GeoRSS\n
    Geoconcept	Geoconcept\n
    Geography Markup Language (GML)	GML	gml\n
    Geospatial PDF	PDF	pdf\n
    IHO S-57 (ENC)	S57	000\n
    Interlis 1	Interlis 1\n
    Interlis 2	Interlis 2\n
    Keyhole Markup Language (KML)	KML	kml\n
    Keyhole Markup Language (LIBKML)	LIBKML\n
    MBTiles	MBTiles	mbtiles\n
    MIPL VICAR file	VICAR\n
    MS Office Open XML spreadsheet	XLSX\n
    MapInfo File	MapInfo File\n
    MapML	MapML\n
    Mapbox Vector Tiles	MVT\n
    Memory	Memory\n
    Microsoft SQL Server Spatial Database	MSSQLSpatial\n
    Microstation DGN	DGN	dgn\n
    MiraMon Vectors (.pol, .arc, .pnt)	MiraMonVector\n
    NASA Planetary Data System 4	PDS4	xml\n
    Network Common Data Format	netCDF	nc\n
    NextGIS Web	NGW\n
    OGC Features and Geometries JSON	JSONFG	json\n
    Open Document/ LibreOffice / OpenOffice Spreadsheet	ODS	ods\n
    OpenJUMP JML	JML	jml\n
    PCIDSK Database File	PCIDSK	pix\n
    PostgreSQL SQL dump	PGDUMP	sql\n
    PostgreSQL/PostGIS	PostgreSQL\n
    ProtoMap Tiles	PMTiles	pmtiles\n
    SQLite / Spatialite	SQLite\n
    Selafin	Selafin\n
    TileDB	TileDB\n
    VDV-451/VDV-452/INTREST Data Format	VDV\n
    WAsP .map format	WAsP	map\n
    ============\n
    Arguments
    ----------
    - SHAPES [`input shapes`] : Shapes
    - FILE [`file path`] : File
    - FORMAT [`choice`] : Format. Available Choices: [0] AmigoCloud [1] AutoCAD DXF [2] Bathymetry Attributed Grid [3] Carto [4] Comma Separated Value (.csv) [5] ESRI FileGDB [6] ESRI Shapefile [7] Elastic Search [8] FlatGeobuf [9] Flexible Image Transport System [10] GMT ASCII Vectors (.gmt) [11] GPSBabel [12] GPX [13] GeoJSON [14] GeoJSON Sequence [15] GeoPackage [16] GeoRSS [17] Geoconcept [18] Geography Markup Language (GML) [19] Geospatial PDF [20] IHO S-57 (ENC) [21] Interlis 1 [22] Interlis 2 [23] Keyhole Markup Language (KML) [24] Keyhole Markup Language (LIBKML) [25] MBTiles [26] MIPL VICAR file [27] MS Office Open XML spreadsheet [28] MapInfo File [29] MapML [30] Mapbox Vector Tiles [31] Memory [32] Microsoft SQL Server Spatial Database [33] Microstation DGN [34] MiraMon Vectors (.pol, .arc, .pnt) [35] NASA Planetary Data System 4 [36] Network Common Data Format [37] NextGIS Web [38] OGC Features and Geometries JSON [39] Open Document/ LibreOffice / OpenOffice Spreadsheet [40] OpenJUMP JML [41] PCIDSK Database File [42] PostgreSQL SQL dump [43] PostgreSQL/PostGIS [44] ProtoMap Tiles [45] SQLite / Spatialite [46] Selafin [47] TileDB [48] VDV-451/VDV-452/INTREST Data Format [49] WAsP .map format Default: 0
    - OPTIONS [`text`] : Creation Options. The dataset creation options. A space separated list of key-value pairs (K=V).
    - LAYER_OPTIONS [`text`] : Layer Creation Options. The layer creation options. A space separated list of key-value pairs (K=V).

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('io_gdal', '4', 'Export Shapes')
    if Tool.is_Okay():
        Tool.Set_Input ('SHAPES', SHAPES)
        Tool.Set_Option('FILE', FILE)
        Tool.Set_Option('FORMAT', FORMAT)
        Tool.Set_Option('OPTIONS', OPTIONS)
        Tool.Set_Option('LAYER_OPTIONS', LAYER_OPTIONS)
        return Tool.Execute(Verbose)
    return False

def Export_Shapes_to_KML(SHAPES=None, FILE=None, Verbose=2):
    '''
    Export Shapes to KML
    ----------
    [io_gdal.5]\n
    This tool exports a vector layer to the Google Earth KML format using Frank Warmerdam's "Geospatial Data Abstraction Library" (GDAL/OGR). The output file is projected to geographic coordinates if necessary and possible.\n
    Arguments
    ----------
    - SHAPES [`input shapes`] : Shapes
    - FILE [`file path`] : File

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('io_gdal', '5', 'Export Shapes to KML')
    if Tool.is_Okay():
        Tool.Set_Input ('SHAPES', SHAPES)
        Tool.Set_Option('FILE', FILE)
        return Tool.Execute(Verbose)
    return False

def run_tool_io_gdal_5(SHAPES=None, FILE=None, Verbose=2):
    '''
    Export Shapes to KML
    ----------
    [io_gdal.5]\n
    This tool exports a vector layer to the Google Earth KML format using Frank Warmerdam's "Geospatial Data Abstraction Library" (GDAL/OGR). The output file is projected to geographic coordinates if necessary and possible.\n
    Arguments
    ----------
    - SHAPES [`input shapes`] : Shapes
    - FILE [`file path`] : File

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('io_gdal', '5', 'Export Shapes to KML')
    if Tool.is_Okay():
        Tool.Set_Input ('SHAPES', SHAPES)
        Tool.Set_Option('FILE', FILE)
        return Tool.Execute(Verbose)
    return False

def Import_NetCDF(GRIDS=None, FILE=None, SAVE_FILE=None, SAVE_PATH=None, TRANSFORM=None, RESAMPLING=None, Verbose=2):
    '''
    Import NetCDF
    ----------
    [io_gdal.6]\n
    This tool imports grids NetCDF Format using the "Geospatial Data Abstraction Library" (GDAL) by Frank Warmerdam.\n
    Arguments
    ----------
    - GRIDS [`output grid list`] : Grids
    - FILE [`file path`] : File
    - SAVE_FILE [`boolean`] : Save to File. Default: 0 save output to file instead of memory
    - SAVE_PATH [`file path`] : Save to Path
    - TRANSFORM [`boolean`] : Transformation. Default: 1 apply coordinate transformation if appropriate
    - RESAMPLING [`choice`] : Resampling. Available Choices: [0] Nearest Neighbour [1] Bilinear Interpolation [2] Bicubic Spline Interpolation [3] B-Spline Interpolation Default: 3 interpolation method to use if grid needs to be aligned to coordinate system

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('io_gdal', '6', 'Import NetCDF')
    if Tool.is_Okay():
        Tool.Set_Output('GRIDS', GRIDS)
        Tool.Set_Option('FILE', FILE)
        Tool.Set_Option('SAVE_FILE', SAVE_FILE)
        Tool.Set_Option('SAVE_PATH', SAVE_PATH)
        Tool.Set_Option('TRANSFORM', TRANSFORM)
        Tool.Set_Option('RESAMPLING', RESAMPLING)
        return Tool.Execute(Verbose)
    return False

def run_tool_io_gdal_6(GRIDS=None, FILE=None, SAVE_FILE=None, SAVE_PATH=None, TRANSFORM=None, RESAMPLING=None, Verbose=2):
    '''
    Import NetCDF
    ----------
    [io_gdal.6]\n
    This tool imports grids NetCDF Format using the "Geospatial Data Abstraction Library" (GDAL) by Frank Warmerdam.\n
    Arguments
    ----------
    - GRIDS [`output grid list`] : Grids
    - FILE [`file path`] : File
    - SAVE_FILE [`boolean`] : Save to File. Default: 0 save output to file instead of memory
    - SAVE_PATH [`file path`] : Save to Path
    - TRANSFORM [`boolean`] : Transformation. Default: 1 apply coordinate transformation if appropriate
    - RESAMPLING [`choice`] : Resampling. Available Choices: [0] Nearest Neighbour [1] Bilinear Interpolation [2] Bicubic Spline Interpolation [3] B-Spline Interpolation Default: 3 interpolation method to use if grid needs to be aligned to coordinate system

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('io_gdal', '6', 'Import NetCDF')
    if Tool.is_Okay():
        Tool.Set_Output('GRIDS', GRIDS)
        Tool.Set_Option('FILE', FILE)
        Tool.Set_Option('SAVE_FILE', SAVE_FILE)
        Tool.Set_Option('SAVE_PATH', SAVE_PATH)
        Tool.Set_Option('TRANSFORM', TRANSFORM)
        Tool.Set_Option('RESAMPLING', RESAMPLING)
        return Tool.Execute(Verbose)
    return False

def Create_Raster_Catalogue_from_Files(CATALOGUE=None, FILES=None, Verbose=2):
    '''
    Create Raster Catalogue from Files
    ----------
    [io_gdal.7]\n
    Create a raster catalogue from selected files. Output is a polygon layer that shows the extent for each valid raster file. Each extent is attributed with the original file path and raster system properties.\n
    The "GDAL Raster Import" tool imports grid data from various file formats using the "Geospatial Data Abstraction Library" (GDAL) by Frank Warmerdam.\n
    GDAL Version:3.9.2\n
    Following raster formats are currently supported:\n
    ============\n
    ID	Name\n
    VRT	Virtual Raster\n
    GTI	GDAL Raster Tile Index\n
    DERIVED	Derived datasets using VRT pixel functions\n
    GTiff	GeoTIFF\n
    COG	Cloud optimized GeoTIFF generator\n
    NITF	National Imagery Transmission Format\n
    RPFTOC	Raster Product Format TOC format\n
    ECRGTOC	ECRG TOC format\n
    HFA	Erdas Imagine Images (.img)\n
    SAR_CEOS	CEOS SAR Image\n
    CEOS	CEOS Image\n
    JAXAPALSAR	JAXA PALSAR Product Reader (Level 1.1/1.5)\n
    GFF	Ground-based SAR Applications Testbed File Format (.gff)\n
    ELAS	ELAS\n
    ESRIC	Esri Compact Cache\n
    AIG	Arc/Info Binary Grid\n
    AAIGrid	Arc/Info ASCII Grid\n
    GRASSASCIIGrid	GRASS ASCII Grid\n
    ISG	International Service for the Geoid\n
    SDTS	SDTS Raster\n
    DTED	DTED Elevation Raster\n
    PNG	Portable Network Graphics\n
    JPEG	JPEG JFIF\n
    MEM	In Memory Raster\n
    JDEM	Japanese DEM (.mem)\n
    GIF	Graphics Interchange Format (.gif)\n
    BIGGIF	Graphics Interchange Format (.gif)\n
    ESAT	Envisat Image Format\n
    FITS	Flexible Image Transport System\n
    BSB	Maptech BSB Nautical Charts\n
    XPM	X11 PixMap Format\n
    BMP	MS Windows Device Independent Bitmap\n
    DIMAP	SPOT DIMAP\n
    AirSAR	AirSAR Polarimetric Image\n
    RS2	RadarSat 2 XML Product\n
    SAFE	Sentinel-1 SAR SAFE Product\n
    PCIDSK	PCIDSK Database File\n
    PCRaster	PCRaster Raster File\n
    ILWIS	ILWIS Raster Map\n
    SGI	SGI Image File Format 1.0\n
    SRTMHGT	SRTMHGT File Format\n
    Leveller	Leveller heightfield\n
    Terragen	Terragen heightfield\n
    netCDF	Network Common Data Format\n
    HDF4	Hierarchical Data Format Release 4\n
    HDF4Image	HDF4 Dataset\n
    ISIS3	USGS Astrogeology ISIS cube (Version 3)\n
    ISIS2	USGS Astrogeology ISIS cube (Version 2)\n
    PDS	NASA Planetary Data System\n
    PDS4	NASA Planetary Data System 4\n
    VICAR	MIPL VICAR file\n
    TIL	EarthWatch .TIL\n
    ERS	ERMapper .ers Labelled\n
    JP2OpenJPEG	JPEG-2000 driver based on JP2OpenJPEG library\n
    L1B	NOAA Polar Orbiter Level 1b Data Set\n
    FIT	FIT Image\n
    GRIB	GRIdded Binary (.grb, .grb2)\n
    RMF	Raster Matrix Format\n
    WCS	OGC Web Coverage Service\n
    WMS	OGC Web Map Service\n
    MSGN	EUMETSAT Archive native (.nat)\n
    RST	Idrisi Raster A.1\n
    GSAG	Golden Software ASCII Grid (.grd)\n
    GSBG	Golden Software Binary Grid (.grd)\n
    GS7BG	Golden Software 7 Binary Grid (.grd)\n
    COSAR	COSAR Annotated Binary Matrix (TerraSAR-X)\n
    TSX	TerraSAR-X Product\n
    COASP	DRDC COASP SAR Processor Raster\n
    R	R Object Data Store\n
    MAP	OziExplorer .MAP\n
    KMLSUPEROVERLAY	Kml Super Overlay\n
    WEBP	WEBP\n
    PDF	Geospatial PDF\n
    Rasterlite	Rasterlite\n
    MBTiles	MBTiles\n
    PLMOSAIC	Planet Labs Mosaics API\n
    CALS	CALS (Type 1)\n
    WMTS	OGC Web Map Tile Service\n
    SENTINEL2	Sentinel 2\n
    MRF	Meta Raster Format\n
    TileDB	TileDB\n
    PNM	Portable Pixmap Format (netpbm)\n
    DOQ1	USGS DOQ (Old Style)\n
    DOQ2	USGS DOQ (New Style)\n
    PAux	PCI .aux Labelled\n
    MFF	Vexcel MFF Raster\n
    MFF2	Vexcel MFF2 (HKV) Raster\n
    GSC	GSC Geogrid\n
    FAST	EOSAT FAST Format\n
    BT	VTP .bt (Binary Terrain) 1.3 Format\n
    LAN	Erdas .LAN/.GIS\n
    CPG	Convair PolGASP\n
    NDF	NLAPS Data Format\n
    EIR	Erdas Imagine Raw\n
    DIPEx	DIPEx\n
    LCP	FARSITE v.4 Landscape File (.lcp)\n
    GTX	NOAA Vertical Datum .GTX\n
    LOSLAS	NADCON .los/.las Datum Grid Shift\n
    NTv2	NTv2 Datum Grid Shift\n
    CTable2	CTable2 Datum Grid Shift\n
    ACE2	ACE2\n
    SNODAS	Snow Data Assimilation System\n
    KRO	KOLOR Raw\n
    ROI_PAC	ROI_PAC raster\n
    RRASTER	R Raster\n
    BYN	Natural Resources Canada's Geoid\n
    NOAA_B	NOAA GEOCON/NADCON5 .b format\n
    RIK	Swedish Grid RIK (.rik)\n
    USGSDEM	USGS Optional ASCII DEM (and CDED)\n
    GXF	GeoSoft Grid Exchange Format\n
    KEA	KEA Image Format (.kea)\n
    BAG	Bathymetry Attributed Grid\n
    S102	S-102 Bathymetric Surface Product\n
    S104	S-104 Water Level Information for Surface Navigation Product\n
    S111	Surface Currents Product\n
    HDF5	Hierarchical Data Format Release 5\n
    HDF5Image	HDF5 Dataset\n
    NWT_GRD	Northwood Numeric Grid Format .grd/.tab\n
    NWT_GRC	Northwood Classified Grid Format .grc/.tab\n
    ADRG	ARC Digitized Raster Graphics\n
    SRP	Standard Raster Product (ASRP/USRP)\n
    BLX	Magellan topo (.blx)\n
    PostGISRaster	PostGIS Raster driver\n
    SAGA	SAGA GIS Binary Grid (.sdat, .sg-grd-z)\n
    XYZ	ASCII Gridded XYZ\n
    HF2	HF2/HFZ heightfield raster\n
    OZI	OziExplorer Image File\n
    CTG	USGS LULC Composite Theme Grid\n
    ZMap	ZMap Plus Grid\n
    NGSGEOID	NOAA NGS Geoid Height Grids\n
    IRIS	IRIS data (.PPI, .CAPPi etc)\n
    PRF	Racurs PHOTOMOD PRF\n
    EEDAI	Earth Engine Data API Image\n
    EEDA	Earth Engine Data API\n
    DAAS	Airbus DS Intelligence Data As A Service driver\n
    SIGDEM	Scaled Integer Gridded DEM .sigdem\n
    TGA	TGA/TARGA Image File Format\n
    OGCAPI	OGCAPI\n
    STACTA	Spatio-Temporal Asset Catalog Tiled Assets\n
    STACIT	Spatio-Temporal Asset Catalog Items\n
    NSIDCbin	NSIDC Sea Ice Concentrations binary (.bin)\n
    GNMFile	Geographic Network generic file based model\n
    GNMDatabase	Geographic Network generic DB based model\n
    ESRI Shapefile	ESRI Shapefile\n
    MapInfo File	MapInfo File\n
    UK .NTF	UK .NTF\n
    LVBAG	Kadaster LV BAG Extract 2.0\n
    OGR_SDTS	SDTS\n
    S57	IHO S-57 (ENC)\n
    DGN	Microstation DGN\n
    OGR_VRT	VRT - Virtual Datasource\n
    Memory	Memory\n
    CSV	Comma Separated Value (.csv)\n
    NAS	NAS - ALKIS\n
    GML	Geography Markup Language (GML)\n
    GPX	GPX\n
    LIBKML	Keyhole Markup Language (LIBKML)\n
    KML	Keyhole Markup Language (KML)\n
    GeoJSON	GeoJSON\n
    GeoJSONSeq	GeoJSON Sequence\n
    ESRIJSON	ESRIJSON\n
    TopoJSON	TopoJSON\n
    Interlis 1	Interlis 1\n
    Interlis 2	Interlis 2\n
    OGR_GMT	GMT ASCII Vectors (.gmt)\n
    GPKG	GeoPackage\n
    SQLite	SQLite / Spatialite\n
    ODBC\n
    WAsP	WAsP .map format\n
    PGeo	ESRI Personal GeoDatabase\n
    MSSQLSpatial	Microsoft SQL Server Spatial Database\n
    PostgreSQL	PostgreSQL/PostGIS\n
    OpenFileGDB	ESRI FileGDB\n
    DXF	AutoCAD DXF\n
    CAD	AutoCAD Driver\n
    FlatGeobuf	FlatGeobuf\n
    Geoconcept	Geoconcept\n
    GeoRSS	GeoRSS\n
    VFK	Czech Cadastral Exchange Data Format\n
    PGDUMP	PostgreSQL SQL dump\n
    OSM	OpenStreetMap XML and PBF\n
    GPSBabel	GPSBabel\n
    OGR_PDS	Planetary Data Systems TABLE\n
    WFS	OGC WFS (Web Feature Service)\n
    OAPIF	OGC API - Features\n
    EDIGEO	French EDIGEO exchange format\n
    SVG	Scalable Vector Graphics\n
    Idrisi	Idrisi Vector (.vct)\n
    XLS	MS Excel format\n
    ODS	Open Document/ LibreOffice / OpenOffice Spreadsheet\n
    XLSX	MS Office Open XML spreadsheet\n
    Elasticsearch	Elastic Search\n
    Carto	Carto\n
    AmigoCloud	AmigoCloud\n
    SXF	Storage and eXchange Format\n
    Selafin	Selafin\n
    JML	OpenJUMP JML\n
    PLSCENES	Planet Labs Scenes API\n
    CSW	OGC CSW (Catalog  Service for the Web)\n
    VDV	VDV-451/VDV-452/INTREST Data Format\n
    GMLAS	Geography Markup Language (GML) driven by application schemas\n
    MVT	Mapbox Vector Tiles\n
    NGW	NextGIS Web\n
    MapML	MapML\n
    GTFS	General Transit Feed Specification\n
    PMTiles	ProtoMap Tiles\n
    JSONFG	OGC Features and Geometries JSON\n
    MiraMonVector	MiraMon Vectors (.pol, .arc, .pnt)\n
    TIGER	U.S. Census TIGER/Line\n
    AVCBin	Arc/Info Binary Coverage\n
    AVCE00	Arc/Info E00 (ASCII) Coverage\n
    GenBin	Generic Binary (.hdr Labelled)\n
    ENVI	ENVI .hdr Labelled\n
    EHdr	ESRI .hdr Labelled\n
    ISCE	ISCE raster\n
    Zarr	Zarr\n
    HTTP	HTTP Fetching Wrapper\n
    ============\n
    Arguments
    ----------
    - CATALOGUE [`output shapes`] : Raster Catalogue
    - FILES [`file path`] : Files

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('io_gdal', '7', 'Create Raster Catalogue from Files')
    if Tool.is_Okay():
        Tool.Set_Output('CATALOGUE', CATALOGUE)
        Tool.Set_Option('FILES', FILES)
        return Tool.Execute(Verbose)
    return False

def run_tool_io_gdal_7(CATALOGUE=None, FILES=None, Verbose=2):
    '''
    Create Raster Catalogue from Files
    ----------
    [io_gdal.7]\n
    Create a raster catalogue from selected files. Output is a polygon layer that shows the extent for each valid raster file. Each extent is attributed with the original file path and raster system properties.\n
    The "GDAL Raster Import" tool imports grid data from various file formats using the "Geospatial Data Abstraction Library" (GDAL) by Frank Warmerdam.\n
    GDAL Version:3.9.2\n
    Following raster formats are currently supported:\n
    ============\n
    ID	Name\n
    VRT	Virtual Raster\n
    GTI	GDAL Raster Tile Index\n
    DERIVED	Derived datasets using VRT pixel functions\n
    GTiff	GeoTIFF\n
    COG	Cloud optimized GeoTIFF generator\n
    NITF	National Imagery Transmission Format\n
    RPFTOC	Raster Product Format TOC format\n
    ECRGTOC	ECRG TOC format\n
    HFA	Erdas Imagine Images (.img)\n
    SAR_CEOS	CEOS SAR Image\n
    CEOS	CEOS Image\n
    JAXAPALSAR	JAXA PALSAR Product Reader (Level 1.1/1.5)\n
    GFF	Ground-based SAR Applications Testbed File Format (.gff)\n
    ELAS	ELAS\n
    ESRIC	Esri Compact Cache\n
    AIG	Arc/Info Binary Grid\n
    AAIGrid	Arc/Info ASCII Grid\n
    GRASSASCIIGrid	GRASS ASCII Grid\n
    ISG	International Service for the Geoid\n
    SDTS	SDTS Raster\n
    DTED	DTED Elevation Raster\n
    PNG	Portable Network Graphics\n
    JPEG	JPEG JFIF\n
    MEM	In Memory Raster\n
    JDEM	Japanese DEM (.mem)\n
    GIF	Graphics Interchange Format (.gif)\n
    BIGGIF	Graphics Interchange Format (.gif)\n
    ESAT	Envisat Image Format\n
    FITS	Flexible Image Transport System\n
    BSB	Maptech BSB Nautical Charts\n
    XPM	X11 PixMap Format\n
    BMP	MS Windows Device Independent Bitmap\n
    DIMAP	SPOT DIMAP\n
    AirSAR	AirSAR Polarimetric Image\n
    RS2	RadarSat 2 XML Product\n
    SAFE	Sentinel-1 SAR SAFE Product\n
    PCIDSK	PCIDSK Database File\n
    PCRaster	PCRaster Raster File\n
    ILWIS	ILWIS Raster Map\n
    SGI	SGI Image File Format 1.0\n
    SRTMHGT	SRTMHGT File Format\n
    Leveller	Leveller heightfield\n
    Terragen	Terragen heightfield\n
    netCDF	Network Common Data Format\n
    HDF4	Hierarchical Data Format Release 4\n
    HDF4Image	HDF4 Dataset\n
    ISIS3	USGS Astrogeology ISIS cube (Version 3)\n
    ISIS2	USGS Astrogeology ISIS cube (Version 2)\n
    PDS	NASA Planetary Data System\n
    PDS4	NASA Planetary Data System 4\n
    VICAR	MIPL VICAR file\n
    TIL	EarthWatch .TIL\n
    ERS	ERMapper .ers Labelled\n
    JP2OpenJPEG	JPEG-2000 driver based on JP2OpenJPEG library\n
    L1B	NOAA Polar Orbiter Level 1b Data Set\n
    FIT	FIT Image\n
    GRIB	GRIdded Binary (.grb, .grb2)\n
    RMF	Raster Matrix Format\n
    WCS	OGC Web Coverage Service\n
    WMS	OGC Web Map Service\n
    MSGN	EUMETSAT Archive native (.nat)\n
    RST	Idrisi Raster A.1\n
    GSAG	Golden Software ASCII Grid (.grd)\n
    GSBG	Golden Software Binary Grid (.grd)\n
    GS7BG	Golden Software 7 Binary Grid (.grd)\n
    COSAR	COSAR Annotated Binary Matrix (TerraSAR-X)\n
    TSX	TerraSAR-X Product\n
    COASP	DRDC COASP SAR Processor Raster\n
    R	R Object Data Store\n
    MAP	OziExplorer .MAP\n
    KMLSUPEROVERLAY	Kml Super Overlay\n
    WEBP	WEBP\n
    PDF	Geospatial PDF\n
    Rasterlite	Rasterlite\n
    MBTiles	MBTiles\n
    PLMOSAIC	Planet Labs Mosaics API\n
    CALS	CALS (Type 1)\n
    WMTS	OGC Web Map Tile Service\n
    SENTINEL2	Sentinel 2\n
    MRF	Meta Raster Format\n
    TileDB	TileDB\n
    PNM	Portable Pixmap Format (netpbm)\n
    DOQ1	USGS DOQ (Old Style)\n
    DOQ2	USGS DOQ (New Style)\n
    PAux	PCI .aux Labelled\n
    MFF	Vexcel MFF Raster\n
    MFF2	Vexcel MFF2 (HKV) Raster\n
    GSC	GSC Geogrid\n
    FAST	EOSAT FAST Format\n
    BT	VTP .bt (Binary Terrain) 1.3 Format\n
    LAN	Erdas .LAN/.GIS\n
    CPG	Convair PolGASP\n
    NDF	NLAPS Data Format\n
    EIR	Erdas Imagine Raw\n
    DIPEx	DIPEx\n
    LCP	FARSITE v.4 Landscape File (.lcp)\n
    GTX	NOAA Vertical Datum .GTX\n
    LOSLAS	NADCON .los/.las Datum Grid Shift\n
    NTv2	NTv2 Datum Grid Shift\n
    CTable2	CTable2 Datum Grid Shift\n
    ACE2	ACE2\n
    SNODAS	Snow Data Assimilation System\n
    KRO	KOLOR Raw\n
    ROI_PAC	ROI_PAC raster\n
    RRASTER	R Raster\n
    BYN	Natural Resources Canada's Geoid\n
    NOAA_B	NOAA GEOCON/NADCON5 .b format\n
    RIK	Swedish Grid RIK (.rik)\n
    USGSDEM	USGS Optional ASCII DEM (and CDED)\n
    GXF	GeoSoft Grid Exchange Format\n
    KEA	KEA Image Format (.kea)\n
    BAG	Bathymetry Attributed Grid\n
    S102	S-102 Bathymetric Surface Product\n
    S104	S-104 Water Level Information for Surface Navigation Product\n
    S111	Surface Currents Product\n
    HDF5	Hierarchical Data Format Release 5\n
    HDF5Image	HDF5 Dataset\n
    NWT_GRD	Northwood Numeric Grid Format .grd/.tab\n
    NWT_GRC	Northwood Classified Grid Format .grc/.tab\n
    ADRG	ARC Digitized Raster Graphics\n
    SRP	Standard Raster Product (ASRP/USRP)\n
    BLX	Magellan topo (.blx)\n
    PostGISRaster	PostGIS Raster driver\n
    SAGA	SAGA GIS Binary Grid (.sdat, .sg-grd-z)\n
    XYZ	ASCII Gridded XYZ\n
    HF2	HF2/HFZ heightfield raster\n
    OZI	OziExplorer Image File\n
    CTG	USGS LULC Composite Theme Grid\n
    ZMap	ZMap Plus Grid\n
    NGSGEOID	NOAA NGS Geoid Height Grids\n
    IRIS	IRIS data (.PPI, .CAPPi etc)\n
    PRF	Racurs PHOTOMOD PRF\n
    EEDAI	Earth Engine Data API Image\n
    EEDA	Earth Engine Data API\n
    DAAS	Airbus DS Intelligence Data As A Service driver\n
    SIGDEM	Scaled Integer Gridded DEM .sigdem\n
    TGA	TGA/TARGA Image File Format\n
    OGCAPI	OGCAPI\n
    STACTA	Spatio-Temporal Asset Catalog Tiled Assets\n
    STACIT	Spatio-Temporal Asset Catalog Items\n
    NSIDCbin	NSIDC Sea Ice Concentrations binary (.bin)\n
    GNMFile	Geographic Network generic file based model\n
    GNMDatabase	Geographic Network generic DB based model\n
    ESRI Shapefile	ESRI Shapefile\n
    MapInfo File	MapInfo File\n
    UK .NTF	UK .NTF\n
    LVBAG	Kadaster LV BAG Extract 2.0\n
    OGR_SDTS	SDTS\n
    S57	IHO S-57 (ENC)\n
    DGN	Microstation DGN\n
    OGR_VRT	VRT - Virtual Datasource\n
    Memory	Memory\n
    CSV	Comma Separated Value (.csv)\n
    NAS	NAS - ALKIS\n
    GML	Geography Markup Language (GML)\n
    GPX	GPX\n
    LIBKML	Keyhole Markup Language (LIBKML)\n
    KML	Keyhole Markup Language (KML)\n
    GeoJSON	GeoJSON\n
    GeoJSONSeq	GeoJSON Sequence\n
    ESRIJSON	ESRIJSON\n
    TopoJSON	TopoJSON\n
    Interlis 1	Interlis 1\n
    Interlis 2	Interlis 2\n
    OGR_GMT	GMT ASCII Vectors (.gmt)\n
    GPKG	GeoPackage\n
    SQLite	SQLite / Spatialite\n
    ODBC\n
    WAsP	WAsP .map format\n
    PGeo	ESRI Personal GeoDatabase\n
    MSSQLSpatial	Microsoft SQL Server Spatial Database\n
    PostgreSQL	PostgreSQL/PostGIS\n
    OpenFileGDB	ESRI FileGDB\n
    DXF	AutoCAD DXF\n
    CAD	AutoCAD Driver\n
    FlatGeobuf	FlatGeobuf\n
    Geoconcept	Geoconcept\n
    GeoRSS	GeoRSS\n
    VFK	Czech Cadastral Exchange Data Format\n
    PGDUMP	PostgreSQL SQL dump\n
    OSM	OpenStreetMap XML and PBF\n
    GPSBabel	GPSBabel\n
    OGR_PDS	Planetary Data Systems TABLE\n
    WFS	OGC WFS (Web Feature Service)\n
    OAPIF	OGC API - Features\n
    EDIGEO	French EDIGEO exchange format\n
    SVG	Scalable Vector Graphics\n
    Idrisi	Idrisi Vector (.vct)\n
    XLS	MS Excel format\n
    ODS	Open Document/ LibreOffice / OpenOffice Spreadsheet\n
    XLSX	MS Office Open XML spreadsheet\n
    Elasticsearch	Elastic Search\n
    Carto	Carto\n
    AmigoCloud	AmigoCloud\n
    SXF	Storage and eXchange Format\n
    Selafin	Selafin\n
    JML	OpenJUMP JML\n
    PLSCENES	Planet Labs Scenes API\n
    CSW	OGC CSW (Catalog  Service for the Web)\n
    VDV	VDV-451/VDV-452/INTREST Data Format\n
    GMLAS	Geography Markup Language (GML) driven by application schemas\n
    MVT	Mapbox Vector Tiles\n
    NGW	NextGIS Web\n
    MapML	MapML\n
    GTFS	General Transit Feed Specification\n
    PMTiles	ProtoMap Tiles\n
    JSONFG	OGC Features and Geometries JSON\n
    MiraMonVector	MiraMon Vectors (.pol, .arc, .pnt)\n
    TIGER	U.S. Census TIGER/Line\n
    AVCBin	Arc/Info Binary Coverage\n
    AVCE00	Arc/Info E00 (ASCII) Coverage\n
    GenBin	Generic Binary (.hdr Labelled)\n
    ENVI	ENVI .hdr Labelled\n
    EHdr	ESRI .hdr Labelled\n
    ISCE	ISCE raster\n
    Zarr	Zarr\n
    HTTP	HTTP Fetching Wrapper\n
    ============\n
    Arguments
    ----------
    - CATALOGUE [`output shapes`] : Raster Catalogue
    - FILES [`file path`] : Files

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('io_gdal', '7', 'Create Raster Catalogue from Files')
    if Tool.is_Okay():
        Tool.Set_Output('CATALOGUE', CATALOGUE)
        Tool.Set_Option('FILES', FILES)
        return Tool.Execute(Verbose)
    return False

def Create_Raster_Catalogues_from_Directory(CATALOGUES=None, CATALOGUE_GCS=None, CATALOGUE_UKN=None, DIRECTORY=None, EXTENSIONS=None, OUTPUT=None, Verbose=2):
    '''
    Create Raster Catalogues from Directory
    ----------
    [io_gdal.8]\n
    Creates raster catalogues from all raster files found in the selected directory. Catalogues are split according to the coordinate reference system used by the different raster files. Output are polygon layers that show the extent for each raster file in it. Each extent is attributed with the original file path and raster system properties.\n
    The "GDAL Raster Import" tool imports grid data from various file formats using the "Geospatial Data Abstraction Library" (GDAL) by Frank Warmerdam. For more information have a look at the GDAL homepage:\n
    [  http://www.gdal.org](http://www.gdal.org/)\n
    GDAL Version:3.9.2\n
    Following raster formats are currently supported:\n
    ============\n
    ID	Name\n
    VRT	Virtual Raster\n
    GTI	GDAL Raster Tile Index\n
    DERIVED	Derived datasets using VRT pixel functions\n
    GTiff	GeoTIFF\n
    COG	Cloud optimized GeoTIFF generator\n
    NITF	National Imagery Transmission Format\n
    RPFTOC	Raster Product Format TOC format\n
    ECRGTOC	ECRG TOC format\n
    HFA	Erdas Imagine Images (.img)\n
    SAR_CEOS	CEOS SAR Image\n
    CEOS	CEOS Image\n
    JAXAPALSAR	JAXA PALSAR Product Reader (Level 1.1/1.5)\n
    GFF	Ground-based SAR Applications Testbed File Format (.gff)\n
    ELAS	ELAS\n
    ESRIC	Esri Compact Cache\n
    AIG	Arc/Info Binary Grid\n
    AAIGrid	Arc/Info ASCII Grid\n
    GRASSASCIIGrid	GRASS ASCII Grid\n
    ISG	International Service for the Geoid\n
    SDTS	SDTS Raster\n
    DTED	DTED Elevation Raster\n
    PNG	Portable Network Graphics\n
    JPEG	JPEG JFIF\n
    MEM	In Memory Raster\n
    JDEM	Japanese DEM (.mem)\n
    GIF	Graphics Interchange Format (.gif)\n
    BIGGIF	Graphics Interchange Format (.gif)\n
    ESAT	Envisat Image Format\n
    FITS	Flexible Image Transport System\n
    BSB	Maptech BSB Nautical Charts\n
    XPM	X11 PixMap Format\n
    BMP	MS Windows Device Independent Bitmap\n
    DIMAP	SPOT DIMAP\n
    AirSAR	AirSAR Polarimetric Image\n
    RS2	RadarSat 2 XML Product\n
    SAFE	Sentinel-1 SAR SAFE Product\n
    PCIDSK	PCIDSK Database File\n
    PCRaster	PCRaster Raster File\n
    ILWIS	ILWIS Raster Map\n
    SGI	SGI Image File Format 1.0\n
    SRTMHGT	SRTMHGT File Format\n
    Leveller	Leveller heightfield\n
    Terragen	Terragen heightfield\n
    netCDF	Network Common Data Format\n
    HDF4	Hierarchical Data Format Release 4\n
    HDF4Image	HDF4 Dataset\n
    ISIS3	USGS Astrogeology ISIS cube (Version 3)\n
    ISIS2	USGS Astrogeology ISIS cube (Version 2)\n
    PDS	NASA Planetary Data System\n
    PDS4	NASA Planetary Data System 4\n
    VICAR	MIPL VICAR file\n
    TIL	EarthWatch .TIL\n
    ERS	ERMapper .ers Labelled\n
    JP2OpenJPEG	JPEG-2000 driver based on JP2OpenJPEG library\n
    L1B	NOAA Polar Orbiter Level 1b Data Set\n
    FIT	FIT Image\n
    GRIB	GRIdded Binary (.grb, .grb2)\n
    RMF	Raster Matrix Format\n
    WCS	OGC Web Coverage Service\n
    WMS	OGC Web Map Service\n
    MSGN	EUMETSAT Archive native (.nat)\n
    RST	Idrisi Raster A.1\n
    GSAG	Golden Software ASCII Grid (.grd)\n
    GSBG	Golden Software Binary Grid (.grd)\n
    GS7BG	Golden Software 7 Binary Grid (.grd)\n
    COSAR	COSAR Annotated Binary Matrix (TerraSAR-X)\n
    TSX	TerraSAR-X Product\n
    COASP	DRDC COASP SAR Processor Raster\n
    R	R Object Data Store\n
    MAP	OziExplorer .MAP\n
    KMLSUPEROVERLAY	Kml Super Overlay\n
    WEBP	WEBP\n
    PDF	Geospatial PDF\n
    Rasterlite	Rasterlite\n
    MBTiles	MBTiles\n
    PLMOSAIC	Planet Labs Mosaics API\n
    CALS	CALS (Type 1)\n
    WMTS	OGC Web Map Tile Service\n
    SENTINEL2	Sentinel 2\n
    MRF	Meta Raster Format\n
    TileDB	TileDB\n
    PNM	Portable Pixmap Format (netpbm)\n
    DOQ1	USGS DOQ (Old Style)\n
    DOQ2	USGS DOQ (New Style)\n
    PAux	PCI .aux Labelled\n
    MFF	Vexcel MFF Raster\n
    MFF2	Vexcel MFF2 (HKV) Raster\n
    GSC	GSC Geogrid\n
    FAST	EOSAT FAST Format\n
    BT	VTP .bt (Binary Terrain) 1.3 Format\n
    LAN	Erdas .LAN/.GIS\n
    CPG	Convair PolGASP\n
    NDF	NLAPS Data Format\n
    EIR	Erdas Imagine Raw\n
    DIPEx	DIPEx\n
    LCP	FARSITE v.4 Landscape File (.lcp)\n
    GTX	NOAA Vertical Datum .GTX\n
    LOSLAS	NADCON .los/.las Datum Grid Shift\n
    NTv2	NTv2 Datum Grid Shift\n
    CTable2	CTable2 Datum Grid Shift\n
    ACE2	ACE2\n
    SNODAS	Snow Data Assimilation System\n
    KRO	KOLOR Raw\n
    ROI_PAC	ROI_PAC raster\n
    RRASTER	R Raster\n
    BYN	Natural Resources Canada's Geoid\n
    NOAA_B	NOAA GEOCON/NADCON5 .b format\n
    RIK	Swedish Grid RIK (.rik)\n
    USGSDEM	USGS Optional ASCII DEM (and CDED)\n
    GXF	GeoSoft Grid Exchange Format\n
    KEA	KEA Image Format (.kea)\n
    BAG	Bathymetry Attributed Grid\n
    S102	S-102 Bathymetric Surface Product\n
    S104	S-104 Water Level Information for Surface Navigation Product\n
    S111	Surface Currents Product\n
    HDF5	Hierarchical Data Format Release 5\n
    HDF5Image	HDF5 Dataset\n
    NWT_GRD	Northwood Numeric Grid Format .grd/.tab\n
    NWT_GRC	Northwood Classified Grid Format .grc/.tab\n
    ADRG	ARC Digitized Raster Graphics\n
    SRP	Standard Raster Product (ASRP/USRP)\n
    BLX	Magellan topo (.blx)\n
    PostGISRaster	PostGIS Raster driver\n
    SAGA	SAGA GIS Binary Grid (.sdat, .sg-grd-z)\n
    XYZ	ASCII Gridded XYZ\n
    HF2	HF2/HFZ heightfield raster\n
    OZI	OziExplorer Image File\n
    CTG	USGS LULC Composite Theme Grid\n
    ZMap	ZMap Plus Grid\n
    NGSGEOID	NOAA NGS Geoid Height Grids\n
    IRIS	IRIS data (.PPI, .CAPPi etc)\n
    PRF	Racurs PHOTOMOD PRF\n
    EEDAI	Earth Engine Data API Image\n
    EEDA	Earth Engine Data API\n
    DAAS	Airbus DS Intelligence Data As A Service driver\n
    SIGDEM	Scaled Integer Gridded DEM .sigdem\n
    TGA	TGA/TARGA Image File Format\n
    OGCAPI	OGCAPI\n
    STACTA	Spatio-Temporal Asset Catalog Tiled Assets\n
    STACIT	Spatio-Temporal Asset Catalog Items\n
    NSIDCbin	NSIDC Sea Ice Concentrations binary (.bin)\n
    GNMFile	Geographic Network generic file based model\n
    GNMDatabase	Geographic Network generic DB based model\n
    ESRI Shapefile	ESRI Shapefile\n
    MapInfo File	MapInfo File\n
    UK .NTF	UK .NTF\n
    LVBAG	Kadaster LV BAG Extract 2.0\n
    OGR_SDTS	SDTS\n
    S57	IHO S-57 (ENC)\n
    DGN	Microstation DGN\n
    OGR_VRT	VRT - Virtual Datasource\n
    Memory	Memory\n
    CSV	Comma Separated Value (.csv)\n
    NAS	NAS - ALKIS\n
    GML	Geography Markup Language (GML)\n
    GPX	GPX\n
    LIBKML	Keyhole Markup Language (LIBKML)\n
    KML	Keyhole Markup Language (KML)\n
    GeoJSON	GeoJSON\n
    GeoJSONSeq	GeoJSON Sequence\n
    ESRIJSON	ESRIJSON\n
    TopoJSON	TopoJSON\n
    Interlis 1	Interlis 1\n
    Interlis 2	Interlis 2\n
    OGR_GMT	GMT ASCII Vectors (.gmt)\n
    GPKG	GeoPackage\n
    SQLite	SQLite / Spatialite\n
    ODBC\n
    WAsP	WAsP .map format\n
    PGeo	ESRI Personal GeoDatabase\n
    MSSQLSpatial	Microsoft SQL Server Spatial Database\n
    PostgreSQL	PostgreSQL/PostGIS\n
    OpenFileGDB	ESRI FileGDB\n
    DXF	AutoCAD DXF\n
    CAD	AutoCAD Driver\n
    FlatGeobuf	FlatGeobuf\n
    Geoconcept	Geoconcept\n
    GeoRSS	GeoRSS\n
    VFK	Czech Cadastral Exchange Data Format\n
    PGDUMP	PostgreSQL SQL dump\n
    OSM	OpenStreetMap XML and PBF\n
    GPSBabel	GPSBabel\n
    OGR_PDS	Planetary Data Systems TABLE\n
    WFS	OGC WFS (Web Feature Service)\n
    OAPIF	OGC API - Features\n
    EDIGEO	French EDIGEO exchange format\n
    SVG	Scalable Vector Graphics\n
    Idrisi	Idrisi Vector (.vct)\n
    XLS	MS Excel format\n
    ODS	Open Document/ LibreOffice / OpenOffice Spreadsheet\n
    XLSX	MS Office Open XML spreadsheet\n
    Elasticsearch	Elastic Search\n
    Carto	Carto\n
    AmigoCloud	AmigoCloud\n
    SXF	Storage and eXchange Format\n
    Selafin	Selafin\n
    JML	OpenJUMP JML\n
    PLSCENES	Planet Labs Scenes API\n
    CSW	OGC CSW (Catalog  Service for the Web)\n
    VDV	VDV-451/VDV-452/INTREST Data Format\n
    GMLAS	Geography Markup Language (GML) driven by application schemas\n
    MVT	Mapbox Vector Tiles\n
    NGW	NextGIS Web\n
    MapML	MapML\n
    GTFS	General Transit Feed Specification\n
    PMTiles	ProtoMap Tiles\n
    JSONFG	OGC Features and Geometries JSON\n
    MiraMonVector	MiraMon Vectors (.pol, .arc, .pnt)\n
    TIGER	U.S. Census TIGER/Line\n
    AVCBin	Arc/Info Binary Coverage\n
    AVCE00	Arc/Info E00 (ASCII) Coverage\n
    GenBin	Generic Binary (.hdr Labelled)\n
    ENVI	ENVI .hdr Labelled\n
    EHdr	ESRI .hdr Labelled\n
    ISCE	ISCE raster\n
    Zarr	Zarr\n
    HTTP	HTTP Fetching Wrapper\n
    ============\n
    Arguments
    ----------
    - CATALOGUES [`output shapes list`] : Raster Catalogues
    - CATALOGUE_GCS [`output shapes`] : Raster Catalogue
    - CATALOGUE_UKN [`output shapes`] : Raster Catalogue (unknown CRS)
    - DIRECTORY [`file path`] : Directory
    - EXTENSIONS [`text`] : Extensions. Default: sgrd; tif
    - OUTPUT [`choice`] : Output. Available Choices: [0] one catalogue for each coordinate system [1] one catalogue using geographic coordinates Default: 1

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('io_gdal', '8', 'Create Raster Catalogues from Directory')
    if Tool.is_Okay():
        Tool.Set_Output('CATALOGUES', CATALOGUES)
        Tool.Set_Output('CATALOGUE_GCS', CATALOGUE_GCS)
        Tool.Set_Output('CATALOGUE_UKN', CATALOGUE_UKN)
        Tool.Set_Option('DIRECTORY', DIRECTORY)
        Tool.Set_Option('EXTENSIONS', EXTENSIONS)
        Tool.Set_Option('OUTPUT', OUTPUT)
        return Tool.Execute(Verbose)
    return False

def run_tool_io_gdal_8(CATALOGUES=None, CATALOGUE_GCS=None, CATALOGUE_UKN=None, DIRECTORY=None, EXTENSIONS=None, OUTPUT=None, Verbose=2):
    '''
    Create Raster Catalogues from Directory
    ----------
    [io_gdal.8]\n
    Creates raster catalogues from all raster files found in the selected directory. Catalogues are split according to the coordinate reference system used by the different raster files. Output are polygon layers that show the extent for each raster file in it. Each extent is attributed with the original file path and raster system properties.\n
    The "GDAL Raster Import" tool imports grid data from various file formats using the "Geospatial Data Abstraction Library" (GDAL) by Frank Warmerdam. For more information have a look at the GDAL homepage:\n
    [  http://www.gdal.org](http://www.gdal.org/)\n
    GDAL Version:3.9.2\n
    Following raster formats are currently supported:\n
    ============\n
    ID	Name\n
    VRT	Virtual Raster\n
    GTI	GDAL Raster Tile Index\n
    DERIVED	Derived datasets using VRT pixel functions\n
    GTiff	GeoTIFF\n
    COG	Cloud optimized GeoTIFF generator\n
    NITF	National Imagery Transmission Format\n
    RPFTOC	Raster Product Format TOC format\n
    ECRGTOC	ECRG TOC format\n
    HFA	Erdas Imagine Images (.img)\n
    SAR_CEOS	CEOS SAR Image\n
    CEOS	CEOS Image\n
    JAXAPALSAR	JAXA PALSAR Product Reader (Level 1.1/1.5)\n
    GFF	Ground-based SAR Applications Testbed File Format (.gff)\n
    ELAS	ELAS\n
    ESRIC	Esri Compact Cache\n
    AIG	Arc/Info Binary Grid\n
    AAIGrid	Arc/Info ASCII Grid\n
    GRASSASCIIGrid	GRASS ASCII Grid\n
    ISG	International Service for the Geoid\n
    SDTS	SDTS Raster\n
    DTED	DTED Elevation Raster\n
    PNG	Portable Network Graphics\n
    JPEG	JPEG JFIF\n
    MEM	In Memory Raster\n
    JDEM	Japanese DEM (.mem)\n
    GIF	Graphics Interchange Format (.gif)\n
    BIGGIF	Graphics Interchange Format (.gif)\n
    ESAT	Envisat Image Format\n
    FITS	Flexible Image Transport System\n
    BSB	Maptech BSB Nautical Charts\n
    XPM	X11 PixMap Format\n
    BMP	MS Windows Device Independent Bitmap\n
    DIMAP	SPOT DIMAP\n
    AirSAR	AirSAR Polarimetric Image\n
    RS2	RadarSat 2 XML Product\n
    SAFE	Sentinel-1 SAR SAFE Product\n
    PCIDSK	PCIDSK Database File\n
    PCRaster	PCRaster Raster File\n
    ILWIS	ILWIS Raster Map\n
    SGI	SGI Image File Format 1.0\n
    SRTMHGT	SRTMHGT File Format\n
    Leveller	Leveller heightfield\n
    Terragen	Terragen heightfield\n
    netCDF	Network Common Data Format\n
    HDF4	Hierarchical Data Format Release 4\n
    HDF4Image	HDF4 Dataset\n
    ISIS3	USGS Astrogeology ISIS cube (Version 3)\n
    ISIS2	USGS Astrogeology ISIS cube (Version 2)\n
    PDS	NASA Planetary Data System\n
    PDS4	NASA Planetary Data System 4\n
    VICAR	MIPL VICAR file\n
    TIL	EarthWatch .TIL\n
    ERS	ERMapper .ers Labelled\n
    JP2OpenJPEG	JPEG-2000 driver based on JP2OpenJPEG library\n
    L1B	NOAA Polar Orbiter Level 1b Data Set\n
    FIT	FIT Image\n
    GRIB	GRIdded Binary (.grb, .grb2)\n
    RMF	Raster Matrix Format\n
    WCS	OGC Web Coverage Service\n
    WMS	OGC Web Map Service\n
    MSGN	EUMETSAT Archive native (.nat)\n
    RST	Idrisi Raster A.1\n
    GSAG	Golden Software ASCII Grid (.grd)\n
    GSBG	Golden Software Binary Grid (.grd)\n
    GS7BG	Golden Software 7 Binary Grid (.grd)\n
    COSAR	COSAR Annotated Binary Matrix (TerraSAR-X)\n
    TSX	TerraSAR-X Product\n
    COASP	DRDC COASP SAR Processor Raster\n
    R	R Object Data Store\n
    MAP	OziExplorer .MAP\n
    KMLSUPEROVERLAY	Kml Super Overlay\n
    WEBP	WEBP\n
    PDF	Geospatial PDF\n
    Rasterlite	Rasterlite\n
    MBTiles	MBTiles\n
    PLMOSAIC	Planet Labs Mosaics API\n
    CALS	CALS (Type 1)\n
    WMTS	OGC Web Map Tile Service\n
    SENTINEL2	Sentinel 2\n
    MRF	Meta Raster Format\n
    TileDB	TileDB\n
    PNM	Portable Pixmap Format (netpbm)\n
    DOQ1	USGS DOQ (Old Style)\n
    DOQ2	USGS DOQ (New Style)\n
    PAux	PCI .aux Labelled\n
    MFF	Vexcel MFF Raster\n
    MFF2	Vexcel MFF2 (HKV) Raster\n
    GSC	GSC Geogrid\n
    FAST	EOSAT FAST Format\n
    BT	VTP .bt (Binary Terrain) 1.3 Format\n
    LAN	Erdas .LAN/.GIS\n
    CPG	Convair PolGASP\n
    NDF	NLAPS Data Format\n
    EIR	Erdas Imagine Raw\n
    DIPEx	DIPEx\n
    LCP	FARSITE v.4 Landscape File (.lcp)\n
    GTX	NOAA Vertical Datum .GTX\n
    LOSLAS	NADCON .los/.las Datum Grid Shift\n
    NTv2	NTv2 Datum Grid Shift\n
    CTable2	CTable2 Datum Grid Shift\n
    ACE2	ACE2\n
    SNODAS	Snow Data Assimilation System\n
    KRO	KOLOR Raw\n
    ROI_PAC	ROI_PAC raster\n
    RRASTER	R Raster\n
    BYN	Natural Resources Canada's Geoid\n
    NOAA_B	NOAA GEOCON/NADCON5 .b format\n
    RIK	Swedish Grid RIK (.rik)\n
    USGSDEM	USGS Optional ASCII DEM (and CDED)\n
    GXF	GeoSoft Grid Exchange Format\n
    KEA	KEA Image Format (.kea)\n
    BAG	Bathymetry Attributed Grid\n
    S102	S-102 Bathymetric Surface Product\n
    S104	S-104 Water Level Information for Surface Navigation Product\n
    S111	Surface Currents Product\n
    HDF5	Hierarchical Data Format Release 5\n
    HDF5Image	HDF5 Dataset\n
    NWT_GRD	Northwood Numeric Grid Format .grd/.tab\n
    NWT_GRC	Northwood Classified Grid Format .grc/.tab\n
    ADRG	ARC Digitized Raster Graphics\n
    SRP	Standard Raster Product (ASRP/USRP)\n
    BLX	Magellan topo (.blx)\n
    PostGISRaster	PostGIS Raster driver\n
    SAGA	SAGA GIS Binary Grid (.sdat, .sg-grd-z)\n
    XYZ	ASCII Gridded XYZ\n
    HF2	HF2/HFZ heightfield raster\n
    OZI	OziExplorer Image File\n
    CTG	USGS LULC Composite Theme Grid\n
    ZMap	ZMap Plus Grid\n
    NGSGEOID	NOAA NGS Geoid Height Grids\n
    IRIS	IRIS data (.PPI, .CAPPi etc)\n
    PRF	Racurs PHOTOMOD PRF\n
    EEDAI	Earth Engine Data API Image\n
    EEDA	Earth Engine Data API\n
    DAAS	Airbus DS Intelligence Data As A Service driver\n
    SIGDEM	Scaled Integer Gridded DEM .sigdem\n
    TGA	TGA/TARGA Image File Format\n
    OGCAPI	OGCAPI\n
    STACTA	Spatio-Temporal Asset Catalog Tiled Assets\n
    STACIT	Spatio-Temporal Asset Catalog Items\n
    NSIDCbin	NSIDC Sea Ice Concentrations binary (.bin)\n
    GNMFile	Geographic Network generic file based model\n
    GNMDatabase	Geographic Network generic DB based model\n
    ESRI Shapefile	ESRI Shapefile\n
    MapInfo File	MapInfo File\n
    UK .NTF	UK .NTF\n
    LVBAG	Kadaster LV BAG Extract 2.0\n
    OGR_SDTS	SDTS\n
    S57	IHO S-57 (ENC)\n
    DGN	Microstation DGN\n
    OGR_VRT	VRT - Virtual Datasource\n
    Memory	Memory\n
    CSV	Comma Separated Value (.csv)\n
    NAS	NAS - ALKIS\n
    GML	Geography Markup Language (GML)\n
    GPX	GPX\n
    LIBKML	Keyhole Markup Language (LIBKML)\n
    KML	Keyhole Markup Language (KML)\n
    GeoJSON	GeoJSON\n
    GeoJSONSeq	GeoJSON Sequence\n
    ESRIJSON	ESRIJSON\n
    TopoJSON	TopoJSON\n
    Interlis 1	Interlis 1\n
    Interlis 2	Interlis 2\n
    OGR_GMT	GMT ASCII Vectors (.gmt)\n
    GPKG	GeoPackage\n
    SQLite	SQLite / Spatialite\n
    ODBC\n
    WAsP	WAsP .map format\n
    PGeo	ESRI Personal GeoDatabase\n
    MSSQLSpatial	Microsoft SQL Server Spatial Database\n
    PostgreSQL	PostgreSQL/PostGIS\n
    OpenFileGDB	ESRI FileGDB\n
    DXF	AutoCAD DXF\n
    CAD	AutoCAD Driver\n
    FlatGeobuf	FlatGeobuf\n
    Geoconcept	Geoconcept\n
    GeoRSS	GeoRSS\n
    VFK	Czech Cadastral Exchange Data Format\n
    PGDUMP	PostgreSQL SQL dump\n
    OSM	OpenStreetMap XML and PBF\n
    GPSBabel	GPSBabel\n
    OGR_PDS	Planetary Data Systems TABLE\n
    WFS	OGC WFS (Web Feature Service)\n
    OAPIF	OGC API - Features\n
    EDIGEO	French EDIGEO exchange format\n
    SVG	Scalable Vector Graphics\n
    Idrisi	Idrisi Vector (.vct)\n
    XLS	MS Excel format\n
    ODS	Open Document/ LibreOffice / OpenOffice Spreadsheet\n
    XLSX	MS Office Open XML spreadsheet\n
    Elasticsearch	Elastic Search\n
    Carto	Carto\n
    AmigoCloud	AmigoCloud\n
    SXF	Storage and eXchange Format\n
    Selafin	Selafin\n
    JML	OpenJUMP JML\n
    PLSCENES	Planet Labs Scenes API\n
    CSW	OGC CSW (Catalog  Service for the Web)\n
    VDV	VDV-451/VDV-452/INTREST Data Format\n
    GMLAS	Geography Markup Language (GML) driven by application schemas\n
    MVT	Mapbox Vector Tiles\n
    NGW	NextGIS Web\n
    MapML	MapML\n
    GTFS	General Transit Feed Specification\n
    PMTiles	ProtoMap Tiles\n
    JSONFG	OGC Features and Geometries JSON\n
    MiraMonVector	MiraMon Vectors (.pol, .arc, .pnt)\n
    TIGER	U.S. Census TIGER/Line\n
    AVCBin	Arc/Info Binary Coverage\n
    AVCE00	Arc/Info E00 (ASCII) Coverage\n
    GenBin	Generic Binary (.hdr Labelled)\n
    ENVI	ENVI .hdr Labelled\n
    EHdr	ESRI .hdr Labelled\n
    ISCE	ISCE raster\n
    Zarr	Zarr\n
    HTTP	HTTP Fetching Wrapper\n
    ============\n
    Arguments
    ----------
    - CATALOGUES [`output shapes list`] : Raster Catalogues
    - CATALOGUE_GCS [`output shapes`] : Raster Catalogue
    - CATALOGUE_UKN [`output shapes`] : Raster Catalogue (unknown CRS)
    - DIRECTORY [`file path`] : Directory
    - EXTENSIONS [`text`] : Extensions. Default: sgrd; tif
    - OUTPUT [`choice`] : Output. Available Choices: [0] one catalogue for each coordinate system [1] one catalogue using geographic coordinates Default: 1

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('io_gdal', '8', 'Create Raster Catalogues from Directory')
    if Tool.is_Okay():
        Tool.Set_Output('CATALOGUES', CATALOGUES)
        Tool.Set_Output('CATALOGUE_GCS', CATALOGUE_GCS)
        Tool.Set_Output('CATALOGUE_UKN', CATALOGUE_UKN)
        Tool.Set_Option('DIRECTORY', DIRECTORY)
        Tool.Set_Option('EXTENSIONS', EXTENSIONS)
        Tool.Set_Option('OUTPUT', OUTPUT)
        return Tool.Execute(Verbose)
    return False

def Import_TMS_Image(TARGET=None, TARGET_MAP=None, TARGET_GRIDSYSTEM=None, TARGET_MAP_GRIDSYSTEM=None, SERVER=None, SERVER_USER=None, SERVER_EPSG=None, BLOCKSIZE=None, CACHE=None, CACHE_DIR=None, GRAYSCALE=None, XMIN=None, YMIN=None, XMAX=None, YMAX=None, NX=None, NY=None, Verbose=2):
    '''
    Import TMS Image
    ----------
    [io_gdal.9]\n
    The "Import TMS Image" tool imports a map image from a Tile Mapping Service (TMS) using the "Geospatial Data Abstraction Library" (GDAL) by Frank Warmerdam.\n
    GDAL Version:3.9.2\n
    Arguments
    ----------
    - TARGET [`optional input grid`] : Target System
    - TARGET_MAP [`output grid`] : Target Map
    - TARGET_GRIDSYSTEM [`grid system`] : Grid system
    - TARGET_MAP_GRIDSYSTEM [`grid system`] : Grid system
    - SERVER [`choice`] : Server. Available Choices: [0] Open Street Map [1] Google Map [2] Google Satellite [3] Google Hybrid [4] Google Terrain [5] Google Terrain, Streets and Water [6] ArcGIS MapServer Tiles [7] TopPlusOpen [8] EMODnet Bathymetry WMTS service [9] user defined Default: 0
    - SERVER_USER [`text`] : Server. Default: tile.openstreetmap.org/${z}/${x}/${y}.png
    - SERVER_EPSG [`integer number`] : EPSG. Default: 3857
    - BLOCKSIZE [`integer number`] : Block Size. Minimum: 32 Default: 256
    - CACHE [`boolean`] : Cache. Default: 0 Enable local disk cache. Allows for offline operation.
    - CACHE_DIR [`file path`] : Cache Directory. If not specified the cache will be created in the current user's temporary directory.
    - GRAYSCALE [`boolean`] : Gray Scale Image. Default: 0
    - XMIN [`floating point number`] : West. Minimum: -20037508.340000 Maximum: 20037508.340000 Default: -20037508.340000
    - YMIN [`floating point number`] : South. Minimum: -20037508.340000 Maximum: 20037508.340000 Default: -20037508.340000
    - XMAX [`floating point number`] : East. Minimum: -20037508.340000 Maximum: 20037508.340000 Default: 20037508.340000
    - YMAX [`floating point number`] : North. Minimum: -20037508.340000 Maximum: 20037508.340000 Default: 20037508.340000
    - NX [`integer number`] : Columns. Minimum: 1 Default: 600
    - NY [`integer number`] : Rows. Minimum: 1 Default: 600

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('io_gdal', '9', 'Import TMS Image')
    if Tool.is_Okay():
        Tool.Set_Input ('TARGET', TARGET)
        Tool.Set_Output('TARGET_MAP', TARGET_MAP)
        Tool.Set_Option('TARGET_GRIDSYSTEM', TARGET_GRIDSYSTEM)
        Tool.Set_Option('TARGET_MAP_GRIDSYSTEM', TARGET_MAP_GRIDSYSTEM)
        Tool.Set_Option('SERVER', SERVER)
        Tool.Set_Option('SERVER_USER', SERVER_USER)
        Tool.Set_Option('SERVER_EPSG', SERVER_EPSG)
        Tool.Set_Option('BLOCKSIZE', BLOCKSIZE)
        Tool.Set_Option('CACHE', CACHE)
        Tool.Set_Option('CACHE_DIR', CACHE_DIR)
        Tool.Set_Option('GRAYSCALE', GRAYSCALE)
        Tool.Set_Option('XMIN', XMIN)
        Tool.Set_Option('YMIN', YMIN)
        Tool.Set_Option('XMAX', XMAX)
        Tool.Set_Option('YMAX', YMAX)
        Tool.Set_Option('NX', NX)
        Tool.Set_Option('NY', NY)
        return Tool.Execute(Verbose)
    return False

def run_tool_io_gdal_9(TARGET=None, TARGET_MAP=None, TARGET_GRIDSYSTEM=None, TARGET_MAP_GRIDSYSTEM=None, SERVER=None, SERVER_USER=None, SERVER_EPSG=None, BLOCKSIZE=None, CACHE=None, CACHE_DIR=None, GRAYSCALE=None, XMIN=None, YMIN=None, XMAX=None, YMAX=None, NX=None, NY=None, Verbose=2):
    '''
    Import TMS Image
    ----------
    [io_gdal.9]\n
    The "Import TMS Image" tool imports a map image from a Tile Mapping Service (TMS) using the "Geospatial Data Abstraction Library" (GDAL) by Frank Warmerdam.\n
    GDAL Version:3.9.2\n
    Arguments
    ----------
    - TARGET [`optional input grid`] : Target System
    - TARGET_MAP [`output grid`] : Target Map
    - TARGET_GRIDSYSTEM [`grid system`] : Grid system
    - TARGET_MAP_GRIDSYSTEM [`grid system`] : Grid system
    - SERVER [`choice`] : Server. Available Choices: [0] Open Street Map [1] Google Map [2] Google Satellite [3] Google Hybrid [4] Google Terrain [5] Google Terrain, Streets and Water [6] ArcGIS MapServer Tiles [7] TopPlusOpen [8] EMODnet Bathymetry WMTS service [9] user defined Default: 0
    - SERVER_USER [`text`] : Server. Default: tile.openstreetmap.org/${z}/${x}/${y}.png
    - SERVER_EPSG [`integer number`] : EPSG. Default: 3857
    - BLOCKSIZE [`integer number`] : Block Size. Minimum: 32 Default: 256
    - CACHE [`boolean`] : Cache. Default: 0 Enable local disk cache. Allows for offline operation.
    - CACHE_DIR [`file path`] : Cache Directory. If not specified the cache will be created in the current user's temporary directory.
    - GRAYSCALE [`boolean`] : Gray Scale Image. Default: 0
    - XMIN [`floating point number`] : West. Minimum: -20037508.340000 Maximum: 20037508.340000 Default: -20037508.340000
    - YMIN [`floating point number`] : South. Minimum: -20037508.340000 Maximum: 20037508.340000 Default: -20037508.340000
    - XMAX [`floating point number`] : East. Minimum: -20037508.340000 Maximum: 20037508.340000 Default: 20037508.340000
    - YMAX [`floating point number`] : North. Minimum: -20037508.340000 Maximum: 20037508.340000 Default: 20037508.340000
    - NX [`integer number`] : Columns. Minimum: 1 Default: 600
    - NY [`integer number`] : Rows. Minimum: 1 Default: 600

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('io_gdal', '9', 'Import TMS Image')
    if Tool.is_Okay():
        Tool.Set_Input ('TARGET', TARGET)
        Tool.Set_Output('TARGET_MAP', TARGET_MAP)
        Tool.Set_Option('TARGET_GRIDSYSTEM', TARGET_GRIDSYSTEM)
        Tool.Set_Option('TARGET_MAP_GRIDSYSTEM', TARGET_MAP_GRIDSYSTEM)
        Tool.Set_Option('SERVER', SERVER)
        Tool.Set_Option('SERVER_USER', SERVER_USER)
        Tool.Set_Option('SERVER_EPSG', SERVER_EPSG)
        Tool.Set_Option('BLOCKSIZE', BLOCKSIZE)
        Tool.Set_Option('CACHE', CACHE)
        Tool.Set_Option('CACHE_DIR', CACHE_DIR)
        Tool.Set_Option('GRAYSCALE', GRAYSCALE)
        Tool.Set_Option('XMIN', XMIN)
        Tool.Set_Option('YMIN', YMIN)
        Tool.Set_Option('XMAX', XMAX)
        Tool.Set_Option('YMAX', YMAX)
        Tool.Set_Option('NX', NX)
        Tool.Set_Option('NY', NY)
        return Tool.Execute(Verbose)
    return False

def GDAL_Formats(FORMATS=None, TYPE=None, ACCESS=None, RECOGNIZED=None, Verbose=2):
    '''
    GDAL Formats
    ----------
    [io_gdal.10]\n
    This tool lists all (file) formats supported by the currently loaded GDAL library.\n
    GDAL Version:3.9.2\n
    Arguments
    ----------
    - FORMATS [`output table`] : GDAL Formats
    - TYPE [`choice`] : Type. Available Choices: [0] raster [1] vector [2] all Default: 2
    - ACCESS [`choice`] : Access. Available Choices: [0] read [1] write [2] read or write Default: 2
    - RECOGNIZED [`boolean`] : All Recognized Files. Default: 1 Add an entry for all recognized files.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('io_gdal', '10', 'GDAL Formats')
    if Tool.is_Okay():
        Tool.Set_Output('FORMATS', FORMATS)
        Tool.Set_Option('TYPE', TYPE)
        Tool.Set_Option('ACCESS', ACCESS)
        Tool.Set_Option('RECOGNIZED', RECOGNIZED)
        return Tool.Execute(Verbose)
    return False

def run_tool_io_gdal_10(FORMATS=None, TYPE=None, ACCESS=None, RECOGNIZED=None, Verbose=2):
    '''
    GDAL Formats
    ----------
    [io_gdal.10]\n
    This tool lists all (file) formats supported by the currently loaded GDAL library.\n
    GDAL Version:3.9.2\n
    Arguments
    ----------
    - FORMATS [`output table`] : GDAL Formats
    - TYPE [`choice`] : Type. Available Choices: [0] raster [1] vector [2] all Default: 2
    - ACCESS [`choice`] : Access. Available Choices: [0] read [1] write [2] read or write Default: 2
    - RECOGNIZED [`boolean`] : All Recognized Files. Default: 1 Add an entry for all recognized files.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('io_gdal', '10', 'GDAL Formats')
    if Tool.is_Okay():
        Tool.Set_Output('FORMATS', FORMATS)
        Tool.Set_Option('TYPE', TYPE)
        Tool.Set_Option('ACCESS', ACCESS)
        Tool.Set_Option('RECOGNIZED', RECOGNIZED)
        return Tool.Execute(Verbose)
    return False

def Import_ASTER_Scene(VNIR=None, SWIR=None, TIR=None, BANDS=None, METADATA=None, FILE=None, FORMAT=None, Verbose=2):
    '''
    Import ASTER Scene
    ----------
    [io_gdal.11]\n
    Import ASTER scene from Hierarchical Data Format version 4 (HDF4).\n
    Arguments
    ----------
    - VNIR [`output data object`] : Visible and Near Infrared
    - SWIR [`output data object`] : Short Wave Infrared
    - TIR [`output data object`] : Thermal Infrared
    - BANDS [`output grid list`] : Bands
    - METADATA [`output table`] : Metadata
    - FILE [`file path`] : File
    - FORMAT [`choice`] : Format. Available Choices: [0] single grids [1] grid collections Default: 1

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('io_gdal', '11', 'Import ASTER Scene')
    if Tool.is_Okay():
        Tool.Set_Output('VNIR', VNIR)
        Tool.Set_Output('SWIR', SWIR)
        Tool.Set_Output('TIR', TIR)
        Tool.Set_Output('BANDS', BANDS)
        Tool.Set_Output('METADATA', METADATA)
        Tool.Set_Option('FILE', FILE)
        Tool.Set_Option('FORMAT', FORMAT)
        return Tool.Execute(Verbose)
    return False

def run_tool_io_gdal_11(VNIR=None, SWIR=None, TIR=None, BANDS=None, METADATA=None, FILE=None, FORMAT=None, Verbose=2):
    '''
    Import ASTER Scene
    ----------
    [io_gdal.11]\n
    Import ASTER scene from Hierarchical Data Format version 4 (HDF4).\n
    Arguments
    ----------
    - VNIR [`output data object`] : Visible and Near Infrared
    - SWIR [`output data object`] : Short Wave Infrared
    - TIR [`output data object`] : Thermal Infrared
    - BANDS [`output grid list`] : Bands
    - METADATA [`output table`] : Metadata
    - FILE [`file path`] : File
    - FORMAT [`choice`] : Format. Available Choices: [0] single grids [1] grid collections Default: 1

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('io_gdal', '11', 'Import ASTER Scene')
    if Tool.is_Okay():
        Tool.Set_Output('VNIR', VNIR)
        Tool.Set_Output('SWIR', SWIR)
        Tool.Set_Output('TIR', TIR)
        Tool.Set_Output('BANDS', BANDS)
        Tool.Set_Output('METADATA', METADATA)
        Tool.Set_Option('FILE', FILE)
        Tool.Set_Option('FORMAT', FORMAT)
        return Tool.Execute(Verbose)
    return False

def Create_Virtual_Raster_VRT(FILES=None, FILE_LIST=None, VRT_NAME=None, RESAMPLING=None, RESOLUTION=None, CELLSIZE=None, ALIGN=None, Verbose=2):
    '''
    Create Virtual Raster (VRT)
    ----------
    [io_gdal.12]\n
    The tool allows one to create a virtual dataset (VRT) which is a mosaic of the input raster datasets. Such a VRT can be used for seamless data access to a large number of raster tiles, a typical application is the clipping of raster tiles from such a VRT.\n
    GDAL Version:3.9.2\n
    Following raster formats are currently supported:\n
    ============\n
    ID	Name	Extension\n
    VRT	Virtual Raster	vrt\n
    GTI	GDAL Raster Tile Index\n
    DERIVED	Derived datasets using VRT pixel functions\n
    GTiff	GeoTIFF	tif\n
    NITF	National Imagery Transmission Format	ntf\n
    RPFTOC	Raster Product Format TOC format	toc\n
    ECRGTOC	ECRG TOC format	xml\n
    HFA	Erdas Imagine Images (.img)	img\n
    SAR_CEOS	CEOS SAR Image\n
    CEOS	CEOS Image\n
    JAXAPALSAR	JAXA PALSAR Product Reader (Level 1.1/1.5)\n
    GFF	Ground-based SAR Applications Testbed File Format (.gff)	gff\n
    ELAS	ELAS\n
    ESRIC	Esri Compact Cache\n
    AIG	Arc/Info Binary Grid\n
    AAIGrid	Arc/Info ASCII Grid	asc\n
    GRASSASCIIGrid	GRASS ASCII Grid\n
    ISG	International Service for the Geoid	isg\n
    SDTS	SDTS Raster	ddf\n
    DTED	DTED Elevation Raster\n
    PNG	Portable Network Graphics	png\n
    JPEG	JPEG JFIF	jpg\n
    MEM	In Memory Raster\n
    JDEM	Japanese DEM (.mem)	mem\n
    GIF	Graphics Interchange Format (.gif)	gif\n
    BIGGIF	Graphics Interchange Format (.gif)	gif\n
    ESAT	Envisat Image Format	n1\n
    FITS	Flexible Image Transport System	fits\n
    BSB	Maptech BSB Nautical Charts	kap\n
    XPM	X11 PixMap Format	xpm\n
    BMP	MS Windows Device Independent Bitmap	bmp\n
    DIMAP	SPOT DIMAP\n
    AirSAR	AirSAR Polarimetric Image\n
    RS2	RadarSat 2 XML Product\n
    SAFE	Sentinel-1 SAR SAFE Product\n
    PCIDSK	PCIDSK Database File	pix\n
    PCRaster	PCRaster Raster File	map\n
    ILWIS	ILWIS Raster Map\n
    SGI	SGI Image File Format 1.0	rgb\n
    SRTMHGT	SRTMHGT File Format	hgt\n
    Leveller	Leveller heightfield	ter\n
    Terragen	Terragen heightfield	ter\n
    netCDF	Network Common Data Format	nc\n
    HDF4	Hierarchical Data Format Release 4	hdf\n
    HDF4Image	HDF4 Dataset\n
    ISIS3	USGS Astrogeology ISIS cube (Version 3)\n
    ISIS2	USGS Astrogeology ISIS cube (Version 2)\n
    PDS	NASA Planetary Data System\n
    PDS4	NASA Planetary Data System 4	xml\n
    VICAR	MIPL VICAR file\n
    TIL	EarthWatch .TIL\n
    ERS	ERMapper .ers Labelled	ers\n
    JP2OpenJPEG	JPEG-2000 driver based on JP2OpenJPEG library	jp2\n
    L1B	NOAA Polar Orbiter Level 1b Data Set\n
    FIT	FIT Image\n
    GRIB	GRIdded Binary (.grb, .grb2)	grb grb2 grib2\n
    RMF	Raster Matrix Format	rsw\n
    WCS	OGC Web Coverage Service\n
    WMS	OGC Web Map Service\n
    MSGN	EUMETSAT Archive native (.nat)	nat\n
    RST	Idrisi Raster A.1	rst\n
    GSAG	Golden Software ASCII Grid (.grd)	grd\n
    GSBG	Golden Software Binary Grid (.grd)	grd\n
    GS7BG	Golden Software 7 Binary Grid (.grd)	grd\n
    COSAR	COSAR Annotated Binary Matrix (TerraSAR-X)\n
    TSX	TerraSAR-X Product\n
    COASP	DRDC COASP SAR Processor Raster	hdr\n
    R	R Object Data Store	rda\n
    MAP	OziExplorer .MAP\n
    KMLSUPEROVERLAY	Kml Super Overlay\n
    WEBP	WEBP	webp\n
    PDF	Geospatial PDF	pdf\n
    Rasterlite	Rasterlite	sqlite\n
    MBTiles	MBTiles	mbtiles\n
    PLMOSAIC	Planet Labs Mosaics API\n
    CALS	CALS (Type 1)\n
    WMTS	OGC Web Map Tile Service\n
    SENTINEL2	Sentinel 2\n
    MRF	Meta Raster Format	mrf\n
    TileDB	TileDB\n
    PNM	Portable Pixmap Format (netpbm)\n
    DOQ1	USGS DOQ (Old Style)\n
    DOQ2	USGS DOQ (New Style)\n
    PAux	PCI .aux Labelled\n
    MFF	Vexcel MFF Raster	hdr\n
    MFF2	Vexcel MFF2 (HKV) Raster\n
    GSC	GSC Geogrid\n
    FAST	EOSAT FAST Format\n
    BT	VTP .bt (Binary Terrain) 1.3 Format	bt\n
    LAN	Erdas .LAN/.GIS\n
    CPG	Convair PolGASP\n
    NDF	NLAPS Data Format\n
    EIR	Erdas Imagine Raw\n
    DIPEx	DIPEx\n
    LCP	FARSITE v.4 Landscape File (.lcp)	lcp\n
    GTX	NOAA Vertical Datum .GTX	gtx\n
    LOSLAS	NADCON .los/.las Datum Grid Shift\n
    NTv2	NTv2 Datum Grid Shift\n
    CTable2	CTable2 Datum Grid Shift\n
    ACE2	ACE2	ACE2\n
    SNODAS	Snow Data Assimilation System	hdr\n
    KRO	KOLOR Raw	kro\n
    ROI_PAC	ROI_PAC raster\n
    RRASTER	R Raster	grd\n
    BYN	Natural Resources Canada's Geoid\n
    NOAA_B	NOAA GEOCON/NADCON5 .b format	b\n
    RIK	Swedish Grid RIK (.rik)	rik\n
    USGSDEM	USGS Optional ASCII DEM (and CDED)	dem\n
    GXF	GeoSoft Grid Exchange Format	gxf\n
    KEA	KEA Image Format (.kea)	kea\n
    BAG	Bathymetry Attributed Grid	bag\n
    S102	S-102 Bathymetric Surface Product	h5\n
    S104	S-104 Water Level Information for Surface Navigation Product	h5\n
    S111	Surface Currents Product	h5\n
    HDF5	Hierarchical Data Format Release 5	h5 hdf5\n
    HDF5Image	HDF5 Dataset\n
    NWT_GRD	Northwood Numeric Grid Format .grd/.tab	grd\n
    NWT_GRC	Northwood Classified Grid Format .grc/.tab	grc\n
    ADRG	ARC Digitized Raster Graphics	gen\n
    SRP	Standard Raster Product (ASRP/USRP)	img\n
    BLX	Magellan topo (.blx)	blx\n
    PostGISRaster	PostGIS Raster driver\n
    SAGA	SAGA GIS Binary Grid (.sdat, .sg-grd-z)\n
    XYZ	ASCII Gridded XYZ	xyz\n
    HF2	HF2/HFZ heightfield raster	hf2\n
    OZI	OziExplorer Image File\n
    CTG	USGS LULC Composite Theme Grid\n
    ZMap	ZMap Plus Grid	dat\n
    NGSGEOID	NOAA NGS Geoid Height Grids	bin\n
    IRIS	IRIS data (.PPI, .CAPPi etc)	ppi\n
    PRF	Racurs PHOTOMOD PRF	prf\n
    EEDAI	Earth Engine Data API Image\n
    DAAS	Airbus DS Intelligence Data As A Service driver\n
    SIGDEM	Scaled Integer Gridded DEM .sigdem	sigdem\n
    TGA	TGA/TARGA Image File Format	tga\n
    OGCAPI	OGCAPI\n
    STACTA	Spatio-Temporal Asset Catalog Tiled Assets	json\n
    STACIT	Spatio-Temporal Asset Catalog Items\n
    NSIDCbin	NSIDC Sea Ice Concentrations binary (.bin)	bin\n
    GPKG	GeoPackage\n
    OpenFileGDB	ESRI FileGDB	gdb\n
    CAD	AutoCAD Driver	dwg\n
    PLSCENES	Planet Labs Scenes API\n
    NGW	NextGIS Web\n
    GenBin	Generic Binary (.hdr Labelled)\n
    ENVI	ENVI .hdr Labelled\n
    EHdr	ESRI .hdr Labelled	bil\n
    ISCE	ISCE raster\n
    Zarr	Zarr\n
    HTTP	HTTP Fetching Wrapper\n
    ============\n
    Arguments
    ----------
    - FILES [`file path`] : Files. The input files.
    - FILE_LIST [`file path`] : Input File List. A text file with the full path to an input grid on each line.
    - VRT_NAME [`file path`] : VRT Filename. The full path and name of the .vrt output file.
    - RESAMPLING [`choice`] : Resampling. Available Choices: [0] nearest [1] bilinear [2] cubic [3] cubic spline [4] lanczos [5] average [6] mode Default: 0 The resampling algorithm used when datasets are queried from the VRT.
    - RESOLUTION [`choice`] : Resolution. Available Choices: [0] highest [1] lowest [2] average [3] user Default: 0 The method how to compute the output resolution if the resolution of all input files is not the same.
    - CELLSIZE [`floating point number`] : Cellsize. Minimum: 0.000000 Default: 1.000000
    - ALIGN [`boolean`] : Align. Default: 1 Align the coordinates of the extent to the cellsize.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('io_gdal', '12', 'Create Virtual Raster (VRT)')
    if Tool.is_Okay():
        Tool.Set_Option('FILES', FILES)
        Tool.Set_Option('FILE_LIST', FILE_LIST)
        Tool.Set_Option('VRT_NAME', VRT_NAME)
        Tool.Set_Option('RESAMPLING', RESAMPLING)
        Tool.Set_Option('RESOLUTION', RESOLUTION)
        Tool.Set_Option('CELLSIZE', CELLSIZE)
        Tool.Set_Option('ALIGN', ALIGN)
        return Tool.Execute(Verbose)
    return False

def run_tool_io_gdal_12(FILES=None, FILE_LIST=None, VRT_NAME=None, RESAMPLING=None, RESOLUTION=None, CELLSIZE=None, ALIGN=None, Verbose=2):
    '''
    Create Virtual Raster (VRT)
    ----------
    [io_gdal.12]\n
    The tool allows one to create a virtual dataset (VRT) which is a mosaic of the input raster datasets. Such a VRT can be used for seamless data access to a large number of raster tiles, a typical application is the clipping of raster tiles from such a VRT.\n
    GDAL Version:3.9.2\n
    Following raster formats are currently supported:\n
    ============\n
    ID	Name	Extension\n
    VRT	Virtual Raster	vrt\n
    GTI	GDAL Raster Tile Index\n
    DERIVED	Derived datasets using VRT pixel functions\n
    GTiff	GeoTIFF	tif\n
    NITF	National Imagery Transmission Format	ntf\n
    RPFTOC	Raster Product Format TOC format	toc\n
    ECRGTOC	ECRG TOC format	xml\n
    HFA	Erdas Imagine Images (.img)	img\n
    SAR_CEOS	CEOS SAR Image\n
    CEOS	CEOS Image\n
    JAXAPALSAR	JAXA PALSAR Product Reader (Level 1.1/1.5)\n
    GFF	Ground-based SAR Applications Testbed File Format (.gff)	gff\n
    ELAS	ELAS\n
    ESRIC	Esri Compact Cache\n
    AIG	Arc/Info Binary Grid\n
    AAIGrid	Arc/Info ASCII Grid	asc\n
    GRASSASCIIGrid	GRASS ASCII Grid\n
    ISG	International Service for the Geoid	isg\n
    SDTS	SDTS Raster	ddf\n
    DTED	DTED Elevation Raster\n
    PNG	Portable Network Graphics	png\n
    JPEG	JPEG JFIF	jpg\n
    MEM	In Memory Raster\n
    JDEM	Japanese DEM (.mem)	mem\n
    GIF	Graphics Interchange Format (.gif)	gif\n
    BIGGIF	Graphics Interchange Format (.gif)	gif\n
    ESAT	Envisat Image Format	n1\n
    FITS	Flexible Image Transport System	fits\n
    BSB	Maptech BSB Nautical Charts	kap\n
    XPM	X11 PixMap Format	xpm\n
    BMP	MS Windows Device Independent Bitmap	bmp\n
    DIMAP	SPOT DIMAP\n
    AirSAR	AirSAR Polarimetric Image\n
    RS2	RadarSat 2 XML Product\n
    SAFE	Sentinel-1 SAR SAFE Product\n
    PCIDSK	PCIDSK Database File	pix\n
    PCRaster	PCRaster Raster File	map\n
    ILWIS	ILWIS Raster Map\n
    SGI	SGI Image File Format 1.0	rgb\n
    SRTMHGT	SRTMHGT File Format	hgt\n
    Leveller	Leveller heightfield	ter\n
    Terragen	Terragen heightfield	ter\n
    netCDF	Network Common Data Format	nc\n
    HDF4	Hierarchical Data Format Release 4	hdf\n
    HDF4Image	HDF4 Dataset\n
    ISIS3	USGS Astrogeology ISIS cube (Version 3)\n
    ISIS2	USGS Astrogeology ISIS cube (Version 2)\n
    PDS	NASA Planetary Data System\n
    PDS4	NASA Planetary Data System 4	xml\n
    VICAR	MIPL VICAR file\n
    TIL	EarthWatch .TIL\n
    ERS	ERMapper .ers Labelled	ers\n
    JP2OpenJPEG	JPEG-2000 driver based on JP2OpenJPEG library	jp2\n
    L1B	NOAA Polar Orbiter Level 1b Data Set\n
    FIT	FIT Image\n
    GRIB	GRIdded Binary (.grb, .grb2)	grb grb2 grib2\n
    RMF	Raster Matrix Format	rsw\n
    WCS	OGC Web Coverage Service\n
    WMS	OGC Web Map Service\n
    MSGN	EUMETSAT Archive native (.nat)	nat\n
    RST	Idrisi Raster A.1	rst\n
    GSAG	Golden Software ASCII Grid (.grd)	grd\n
    GSBG	Golden Software Binary Grid (.grd)	grd\n
    GS7BG	Golden Software 7 Binary Grid (.grd)	grd\n
    COSAR	COSAR Annotated Binary Matrix (TerraSAR-X)\n
    TSX	TerraSAR-X Product\n
    COASP	DRDC COASP SAR Processor Raster	hdr\n
    R	R Object Data Store	rda\n
    MAP	OziExplorer .MAP\n
    KMLSUPEROVERLAY	Kml Super Overlay\n
    WEBP	WEBP	webp\n
    PDF	Geospatial PDF	pdf\n
    Rasterlite	Rasterlite	sqlite\n
    MBTiles	MBTiles	mbtiles\n
    PLMOSAIC	Planet Labs Mosaics API\n
    CALS	CALS (Type 1)\n
    WMTS	OGC Web Map Tile Service\n
    SENTINEL2	Sentinel 2\n
    MRF	Meta Raster Format	mrf\n
    TileDB	TileDB\n
    PNM	Portable Pixmap Format (netpbm)\n
    DOQ1	USGS DOQ (Old Style)\n
    DOQ2	USGS DOQ (New Style)\n
    PAux	PCI .aux Labelled\n
    MFF	Vexcel MFF Raster	hdr\n
    MFF2	Vexcel MFF2 (HKV) Raster\n
    GSC	GSC Geogrid\n
    FAST	EOSAT FAST Format\n
    BT	VTP .bt (Binary Terrain) 1.3 Format	bt\n
    LAN	Erdas .LAN/.GIS\n
    CPG	Convair PolGASP\n
    NDF	NLAPS Data Format\n
    EIR	Erdas Imagine Raw\n
    DIPEx	DIPEx\n
    LCP	FARSITE v.4 Landscape File (.lcp)	lcp\n
    GTX	NOAA Vertical Datum .GTX	gtx\n
    LOSLAS	NADCON .los/.las Datum Grid Shift\n
    NTv2	NTv2 Datum Grid Shift\n
    CTable2	CTable2 Datum Grid Shift\n
    ACE2	ACE2	ACE2\n
    SNODAS	Snow Data Assimilation System	hdr\n
    KRO	KOLOR Raw	kro\n
    ROI_PAC	ROI_PAC raster\n
    RRASTER	R Raster	grd\n
    BYN	Natural Resources Canada's Geoid\n
    NOAA_B	NOAA GEOCON/NADCON5 .b format	b\n
    RIK	Swedish Grid RIK (.rik)	rik\n
    USGSDEM	USGS Optional ASCII DEM (and CDED)	dem\n
    GXF	GeoSoft Grid Exchange Format	gxf\n
    KEA	KEA Image Format (.kea)	kea\n
    BAG	Bathymetry Attributed Grid	bag\n
    S102	S-102 Bathymetric Surface Product	h5\n
    S104	S-104 Water Level Information for Surface Navigation Product	h5\n
    S111	Surface Currents Product	h5\n
    HDF5	Hierarchical Data Format Release 5	h5 hdf5\n
    HDF5Image	HDF5 Dataset\n
    NWT_GRD	Northwood Numeric Grid Format .grd/.tab	grd\n
    NWT_GRC	Northwood Classified Grid Format .grc/.tab	grc\n
    ADRG	ARC Digitized Raster Graphics	gen\n
    SRP	Standard Raster Product (ASRP/USRP)	img\n
    BLX	Magellan topo (.blx)	blx\n
    PostGISRaster	PostGIS Raster driver\n
    SAGA	SAGA GIS Binary Grid (.sdat, .sg-grd-z)\n
    XYZ	ASCII Gridded XYZ	xyz\n
    HF2	HF2/HFZ heightfield raster	hf2\n
    OZI	OziExplorer Image File\n
    CTG	USGS LULC Composite Theme Grid\n
    ZMap	ZMap Plus Grid	dat\n
    NGSGEOID	NOAA NGS Geoid Height Grids	bin\n
    IRIS	IRIS data (.PPI, .CAPPi etc)	ppi\n
    PRF	Racurs PHOTOMOD PRF	prf\n
    EEDAI	Earth Engine Data API Image\n
    DAAS	Airbus DS Intelligence Data As A Service driver\n
    SIGDEM	Scaled Integer Gridded DEM .sigdem	sigdem\n
    TGA	TGA/TARGA Image File Format	tga\n
    OGCAPI	OGCAPI\n
    STACTA	Spatio-Temporal Asset Catalog Tiled Assets	json\n
    STACIT	Spatio-Temporal Asset Catalog Items\n
    NSIDCbin	NSIDC Sea Ice Concentrations binary (.bin)	bin\n
    GPKG	GeoPackage\n
    OpenFileGDB	ESRI FileGDB	gdb\n
    CAD	AutoCAD Driver	dwg\n
    PLSCENES	Planet Labs Scenes API\n
    NGW	NextGIS Web\n
    GenBin	Generic Binary (.hdr Labelled)\n
    ENVI	ENVI .hdr Labelled\n
    EHdr	ESRI .hdr Labelled	bil\n
    ISCE	ISCE raster\n
    Zarr	Zarr\n
    HTTP	HTTP Fetching Wrapper\n
    ============\n
    Arguments
    ----------
    - FILES [`file path`] : Files. The input files.
    - FILE_LIST [`file path`] : Input File List. A text file with the full path to an input grid on each line.
    - VRT_NAME [`file path`] : VRT Filename. The full path and name of the .vrt output file.
    - RESAMPLING [`choice`] : Resampling. Available Choices: [0] nearest [1] bilinear [2] cubic [3] cubic spline [4] lanczos [5] average [6] mode Default: 0 The resampling algorithm used when datasets are queried from the VRT.
    - RESOLUTION [`choice`] : Resolution. Available Choices: [0] highest [1] lowest [2] average [3] user Default: 0 The method how to compute the output resolution if the resolution of all input files is not the same.
    - CELLSIZE [`floating point number`] : Cellsize. Minimum: 0.000000 Default: 1.000000
    - ALIGN [`boolean`] : Align. Default: 1 Align the coordinates of the extent to the cellsize.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('io_gdal', '12', 'Create Virtual Raster (VRT)')
    if Tool.is_Okay():
        Tool.Set_Option('FILES', FILES)
        Tool.Set_Option('FILE_LIST', FILE_LIST)
        Tool.Set_Option('VRT_NAME', VRT_NAME)
        Tool.Set_Option('RESAMPLING', RESAMPLING)
        Tool.Set_Option('RESOLUTION', RESOLUTION)
        Tool.Set_Option('CELLSIZE', CELLSIZE)
        Tool.Set_Option('ALIGN', ALIGN)
        return Tool.Execute(Verbose)
    return False

def Import_from_Virtual_Raster_VRT(SHAPES=None, GRIDS=None, VRT_NAME=None, EXTENT=None, GRIDSYSTEM=None, XMIN=None, XMAX=None, YMIN=None, YMAX=None, BUFFER=None, MULTIPLE=None, TRANSFORM=None, RESAMPLING=None, Verbose=2):
    '''
    Import from Virtual Raster (VRT)
    ----------
    [io_gdal.13]\n
    The tool allows one to clip / extract a raster subset from a virtual raster dataset (VRT). Such a VRT is actually an XML based description of a mosaic of raster datasets and can be created with the "Create Virtual Raster (VRT)" tool.\n
    Arguments
    ----------
    - SHAPES [`input shapes`] : Shapes Extent. The shapefile defining the clipping extent.
    - GRIDS [`output grid list`] : Grids
    - VRT_NAME [`file path`] : VRT Filename. The full path and name of the .vrt file.
    - EXTENT [`choice`] : Extent. Available Choices: [0] user defined [1] grid system [2] shapes extent Default: 0 Choose how to define the clipping extent.
    - GRIDSYSTEM [`grid system`] : Grid System. The target grid system.
    - XMIN [`floating point number`] : Left. Default: 0.000000 The minimum x-coordinate of the clipping extent.
    - XMAX [`floating point number`] : Right. Default: 0.000000 The maximum x-coordinate of the clipping extent.
    - YMIN [`floating point number`] : Bottom. Default: 0.000000 The minimum y-coordinate of the clipping extent.
    - YMAX [`floating point number`] : Top. Default: 0.000000 The maximum y-coordinate of the clipping extent.
    - BUFFER [`floating point number`] : Buffer. Minimum: 0.000000 Default: 0.000000 The optional buffer added to the clipping extent [map units].
    - MULTIPLE [`choice`] : Multiple Bands Output. Available Choices: [0] single grids [1] grid collection [2] automatic Default: 2 Choose how to handle output datasets with multiple bands.
    - TRANSFORM [`boolean`] : Transformation. Default: 1 Align grid to coordinate system.
    - RESAMPLING [`choice`] : Resampling. Available Choices: [0] Nearest Neighbour [1] Bilinear Interpolation [2] Bicubic Spline Interpolation [3] B-Spline Interpolation Default: 0 Resampling type to be used, if grid needs to be aligned to coordinate system.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('io_gdal', '13', 'Import from Virtual Raster (VRT)')
    if Tool.is_Okay():
        Tool.Set_Input ('SHAPES', SHAPES)
        Tool.Set_Output('GRIDS', GRIDS)
        Tool.Set_Option('VRT_NAME', VRT_NAME)
        Tool.Set_Option('EXTENT', EXTENT)
        Tool.Set_Option('GRIDSYSTEM', GRIDSYSTEM)
        Tool.Set_Option('XMIN', XMIN)
        Tool.Set_Option('XMAX', XMAX)
        Tool.Set_Option('YMIN', YMIN)
        Tool.Set_Option('YMAX', YMAX)
        Tool.Set_Option('BUFFER', BUFFER)
        Tool.Set_Option('MULTIPLE', MULTIPLE)
        Tool.Set_Option('TRANSFORM', TRANSFORM)
        Tool.Set_Option('RESAMPLING', RESAMPLING)
        return Tool.Execute(Verbose)
    return False

def run_tool_io_gdal_13(SHAPES=None, GRIDS=None, VRT_NAME=None, EXTENT=None, GRIDSYSTEM=None, XMIN=None, XMAX=None, YMIN=None, YMAX=None, BUFFER=None, MULTIPLE=None, TRANSFORM=None, RESAMPLING=None, Verbose=2):
    '''
    Import from Virtual Raster (VRT)
    ----------
    [io_gdal.13]\n
    The tool allows one to clip / extract a raster subset from a virtual raster dataset (VRT). Such a VRT is actually an XML based description of a mosaic of raster datasets and can be created with the "Create Virtual Raster (VRT)" tool.\n
    Arguments
    ----------
    - SHAPES [`input shapes`] : Shapes Extent. The shapefile defining the clipping extent.
    - GRIDS [`output grid list`] : Grids
    - VRT_NAME [`file path`] : VRT Filename. The full path and name of the .vrt file.
    - EXTENT [`choice`] : Extent. Available Choices: [0] user defined [1] grid system [2] shapes extent Default: 0 Choose how to define the clipping extent.
    - GRIDSYSTEM [`grid system`] : Grid System. The target grid system.
    - XMIN [`floating point number`] : Left. Default: 0.000000 The minimum x-coordinate of the clipping extent.
    - XMAX [`floating point number`] : Right. Default: 0.000000 The maximum x-coordinate of the clipping extent.
    - YMIN [`floating point number`] : Bottom. Default: 0.000000 The minimum y-coordinate of the clipping extent.
    - YMAX [`floating point number`] : Top. Default: 0.000000 The maximum y-coordinate of the clipping extent.
    - BUFFER [`floating point number`] : Buffer. Minimum: 0.000000 Default: 0.000000 The optional buffer added to the clipping extent [map units].
    - MULTIPLE [`choice`] : Multiple Bands Output. Available Choices: [0] single grids [1] grid collection [2] automatic Default: 2 Choose how to handle output datasets with multiple bands.
    - TRANSFORM [`boolean`] : Transformation. Default: 1 Align grid to coordinate system.
    - RESAMPLING [`choice`] : Resampling. Available Choices: [0] Nearest Neighbour [1] Bilinear Interpolation [2] Bicubic Spline Interpolation [3] B-Spline Interpolation Default: 0 Resampling type to be used, if grid needs to be aligned to coordinate system.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('io_gdal', '13', 'Import from Virtual Raster (VRT)')
    if Tool.is_Okay():
        Tool.Set_Input ('SHAPES', SHAPES)
        Tool.Set_Output('GRIDS', GRIDS)
        Tool.Set_Option('VRT_NAME', VRT_NAME)
        Tool.Set_Option('EXTENT', EXTENT)
        Tool.Set_Option('GRIDSYSTEM', GRIDSYSTEM)
        Tool.Set_Option('XMIN', XMIN)
        Tool.Set_Option('XMAX', XMAX)
        Tool.Set_Option('YMIN', YMIN)
        Tool.Set_Option('YMAX', YMAX)
        Tool.Set_Option('BUFFER', BUFFER)
        Tool.Set_Option('MULTIPLE', MULTIPLE)
        Tool.Set_Option('TRANSFORM', TRANSFORM)
        Tool.Set_Option('RESAMPLING', RESAMPLING)
        return Tool.Execute(Verbose)
    return False

def Create_Raster_Catalogue_from_Virtual_Raster_VRT(CATALOGUE=None, VRT_FILE=None, Verbose=2):
    '''
    Create Raster Catalogue from Virtual Raster (VRT)
    ----------
    [io_gdal.14]\n
    The tool allows one to create a polygon layer that shows the extent of each raster file referenced in the virtual raster. Each extent is attributed with the original file path, which can be used to load the dataset by 'CTRL + left-click' in the table field.\n
    Note: the tool only supports basic variants of the VRT format.\n
    Arguments
    ----------
    - CATALOGUE [`output shapes`] : Raster Catalogue. The polygon layer with dataset boundaries.
    - VRT_FILE [`file path`] : VRT File. The full path and name of the .vrt input file.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('io_gdal', '14', 'Create Raster Catalogue from Virtual Raster (VRT)')
    if Tool.is_Okay():
        Tool.Set_Output('CATALOGUE', CATALOGUE)
        Tool.Set_Option('VRT_FILE', VRT_FILE)
        return Tool.Execute(Verbose)
    return False

def run_tool_io_gdal_14(CATALOGUE=None, VRT_FILE=None, Verbose=2):
    '''
    Create Raster Catalogue from Virtual Raster (VRT)
    ----------
    [io_gdal.14]\n
    The tool allows one to create a polygon layer that shows the extent of each raster file referenced in the virtual raster. Each extent is attributed with the original file path, which can be used to load the dataset by 'CTRL + left-click' in the table field.\n
    Note: the tool only supports basic variants of the VRT format.\n
    Arguments
    ----------
    - CATALOGUE [`output shapes`] : Raster Catalogue. The polygon layer with dataset boundaries.
    - VRT_FILE [`file path`] : VRT File. The full path and name of the .vrt input file.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('io_gdal', '14', 'Create Raster Catalogue from Virtual Raster (VRT)')
    if Tool.is_Okay():
        Tool.Set_Output('CATALOGUE', CATALOGUE)
        Tool.Set_Option('VRT_FILE', VRT_FILE)
        return Tool.Execute(Verbose)
    return False

