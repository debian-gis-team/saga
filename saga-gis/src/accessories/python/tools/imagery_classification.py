#! /usr/bin/env python

'''
Python Interface to SAGA Tools Library
----------
- Category : Imagery
- Name     : Classification
- ID       : imagery_classification

Description
----------
Classification tools for grids.
'''

from PySAGA.helper import Tool_Wrapper

def Supervised_Image_Classification(GRIDS=None, TRAINING=None, TRAIN_SAMPLES=None, CLASSES=None, QUALITY=None, CLASSES_LUT=None, NORMALISE=None, GRID_SYSTEM=None, TRAIN_WITH=None, TRAINING_CLASS=None, TRAIN_BUFFER=None, FILE_LOAD=None, FILE_SAVE=None, METHOD=None, THRESHOLD_DIST=None, THRESHOLD_ANGLE=None, THRESHOLD_PROB=None, RELATIVE_PROB=None, Verbose=2):
    '''
    Supervised Image Classification
    ----------
    [imagery_classification.0]\n
    Standard methods for supervised image classification, including minimum distance, maximum likelihood, spectral angle mapping. Classifiers can be trained by areas defined through shapes, samples supplied as table records, or statistics previously stored to file.\n
    Arguments
    ----------
    - GRIDS [`input grid list`] : Features
    - TRAINING [`input shapes`] : Training Areas
    - TRAIN_SAMPLES [`input table`] : Training Samples. Provide a class identifier in the first field followed by sample data corresponding to the input feature grids.
    - CLASSES [`output grid`] : Classification
    - QUALITY [`output grid`] : Quality. Dependent on chosen method, these are distances or probabilities.
    - CLASSES_LUT [`output table`] : Look-up Table. A reference list of the grid values that have been assigned to the training classes.
    - NORMALISE [`boolean`] : Normalize. Default: 0
    - GRID_SYSTEM [`grid system`] : Grid System
    - TRAIN_WITH [`choice`] : Training. Available Choices: [0] training areas [1] training samples [2] load from file Default: 0
    - TRAINING_CLASS [`table field`] : Class Identifier
    - TRAIN_BUFFER [`floating point number`] : Buffer Size. Minimum: 0.000000 Default: 1.000000 For non-polygon type training areas, creates a buffer with a diameter of specified size.
    - FILE_LOAD [`file path`] : Load Statistics from File...
    - FILE_SAVE [`file path`] : Save Statistics to File...
    - METHOD [`choice`] : Method. Available Choices: [0] Binary Encoding [1] Parallelepiped [2] Minimum Distance [3] Mahalanobis Distance [4] Maximum Likelihood [5] Spectral Angle Mapping Default: 2
    - THRESHOLD_DIST [`floating point number`] : Distance Threshold. Minimum: 0.000000 Default: 0.000000 Let pixel stay unclassified, if minimum euclidean or mahalanobis distance is greater than threshold.
    - THRESHOLD_ANGLE [`floating point number`] : Spectral Angle Threshold (Degree). Minimum: 0.000000 Maximum: 90.000000 Default: 0.000000 Let pixel stay unclassified, if spectral angle distance is greater than threshold.
    - THRESHOLD_PROB [`floating point number`] : Probability Threshold. Minimum: 0.000000 Maximum: 100.000000 Default: 0.000000 Let pixel stay unclassified, if maximum likelihood probability value is less than threshold.
    - RELATIVE_PROB [`choice`] : Probability Reference. Available Choices: [0] absolute [1] relative Default: 1

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('imagery_classification', '0', 'Supervised Image Classification')
    if Tool.is_Okay():
        Tool.Set_Input ('GRIDS', GRIDS)
        Tool.Set_Input ('TRAINING', TRAINING)
        Tool.Set_Input ('TRAIN_SAMPLES', TRAIN_SAMPLES)
        Tool.Set_Output('CLASSES', CLASSES)
        Tool.Set_Output('QUALITY', QUALITY)
        Tool.Set_Output('CLASSES_LUT', CLASSES_LUT)
        Tool.Set_Option('NORMALISE', NORMALISE)
        Tool.Set_Option('GRID_SYSTEM', GRID_SYSTEM)
        Tool.Set_Option('TRAIN_WITH', TRAIN_WITH)
        Tool.Set_Option('TRAINING_CLASS', TRAINING_CLASS)
        Tool.Set_Option('TRAIN_BUFFER', TRAIN_BUFFER)
        Tool.Set_Option('FILE_LOAD', FILE_LOAD)
        Tool.Set_Option('FILE_SAVE', FILE_SAVE)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('THRESHOLD_DIST', THRESHOLD_DIST)
        Tool.Set_Option('THRESHOLD_ANGLE', THRESHOLD_ANGLE)
        Tool.Set_Option('THRESHOLD_PROB', THRESHOLD_PROB)
        Tool.Set_Option('RELATIVE_PROB', RELATIVE_PROB)
        return Tool.Execute(Verbose)
    return False

def run_tool_imagery_classification_0(GRIDS=None, TRAINING=None, TRAIN_SAMPLES=None, CLASSES=None, QUALITY=None, CLASSES_LUT=None, NORMALISE=None, GRID_SYSTEM=None, TRAIN_WITH=None, TRAINING_CLASS=None, TRAIN_BUFFER=None, FILE_LOAD=None, FILE_SAVE=None, METHOD=None, THRESHOLD_DIST=None, THRESHOLD_ANGLE=None, THRESHOLD_PROB=None, RELATIVE_PROB=None, Verbose=2):
    '''
    Supervised Image Classification
    ----------
    [imagery_classification.0]\n
    Standard methods for supervised image classification, including minimum distance, maximum likelihood, spectral angle mapping. Classifiers can be trained by areas defined through shapes, samples supplied as table records, or statistics previously stored to file.\n
    Arguments
    ----------
    - GRIDS [`input grid list`] : Features
    - TRAINING [`input shapes`] : Training Areas
    - TRAIN_SAMPLES [`input table`] : Training Samples. Provide a class identifier in the first field followed by sample data corresponding to the input feature grids.
    - CLASSES [`output grid`] : Classification
    - QUALITY [`output grid`] : Quality. Dependent on chosen method, these are distances or probabilities.
    - CLASSES_LUT [`output table`] : Look-up Table. A reference list of the grid values that have been assigned to the training classes.
    - NORMALISE [`boolean`] : Normalize. Default: 0
    - GRID_SYSTEM [`grid system`] : Grid System
    - TRAIN_WITH [`choice`] : Training. Available Choices: [0] training areas [1] training samples [2] load from file Default: 0
    - TRAINING_CLASS [`table field`] : Class Identifier
    - TRAIN_BUFFER [`floating point number`] : Buffer Size. Minimum: 0.000000 Default: 1.000000 For non-polygon type training areas, creates a buffer with a diameter of specified size.
    - FILE_LOAD [`file path`] : Load Statistics from File...
    - FILE_SAVE [`file path`] : Save Statistics to File...
    - METHOD [`choice`] : Method. Available Choices: [0] Binary Encoding [1] Parallelepiped [2] Minimum Distance [3] Mahalanobis Distance [4] Maximum Likelihood [5] Spectral Angle Mapping Default: 2
    - THRESHOLD_DIST [`floating point number`] : Distance Threshold. Minimum: 0.000000 Default: 0.000000 Let pixel stay unclassified, if minimum euclidean or mahalanobis distance is greater than threshold.
    - THRESHOLD_ANGLE [`floating point number`] : Spectral Angle Threshold (Degree). Minimum: 0.000000 Maximum: 90.000000 Default: 0.000000 Let pixel stay unclassified, if spectral angle distance is greater than threshold.
    - THRESHOLD_PROB [`floating point number`] : Probability Threshold. Minimum: 0.000000 Maximum: 100.000000 Default: 0.000000 Let pixel stay unclassified, if maximum likelihood probability value is less than threshold.
    - RELATIVE_PROB [`choice`] : Probability Reference. Available Choices: [0] absolute [1] relative Default: 1

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('imagery_classification', '0', 'Supervised Image Classification')
    if Tool.is_Okay():
        Tool.Set_Input ('GRIDS', GRIDS)
        Tool.Set_Input ('TRAINING', TRAINING)
        Tool.Set_Input ('TRAIN_SAMPLES', TRAIN_SAMPLES)
        Tool.Set_Output('CLASSES', CLASSES)
        Tool.Set_Output('QUALITY', QUALITY)
        Tool.Set_Output('CLASSES_LUT', CLASSES_LUT)
        Tool.Set_Option('NORMALISE', NORMALISE)
        Tool.Set_Option('GRID_SYSTEM', GRID_SYSTEM)
        Tool.Set_Option('TRAIN_WITH', TRAIN_WITH)
        Tool.Set_Option('TRAINING_CLASS', TRAINING_CLASS)
        Tool.Set_Option('TRAIN_BUFFER', TRAIN_BUFFER)
        Tool.Set_Option('FILE_LOAD', FILE_LOAD)
        Tool.Set_Option('FILE_SAVE', FILE_SAVE)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('THRESHOLD_DIST', THRESHOLD_DIST)
        Tool.Set_Option('THRESHOLD_ANGLE', THRESHOLD_ANGLE)
        Tool.Set_Option('THRESHOLD_PROB', THRESHOLD_PROB)
        Tool.Set_Option('RELATIVE_PROB', RELATIVE_PROB)
        return Tool.Execute(Verbose)
    return False

def KMeans_Clustering_for_Grids(GRIDS=None, CLUSTER=None, STATISTICS=None, METHOD=None, NCLUSTER=None, MAXITER=None, NORMALISE=None, INITIALIZE=None, OLDVERSION=None, Verbose=2):
    '''
    K-Means Clustering for Grids
    ----------
    [imagery_classification.1]\n
    This tool implements the K-Means cluster analysis for grids in two variants, iterative minimum distance (Forgy 1965) and hill climbing (Rubin 1967).\n
    Arguments
    ----------
    - GRIDS [`input grid list`] : Grids
    - CLUSTER [`output grid`] : Clusters
    - STATISTICS [`output table`] : Statistics
    - METHOD [`choice`] : Method. Available Choices: [0] Iterative Minimum Distance (Forgy 1965) [1] Hill-Climbing (Rubin 1967) [2] Combined Minimum Distance / Hillclimbing Default: 1
    - NCLUSTER [`integer number`] : Clusters. Minimum: 2 Default: 10 Number of clusters
    - MAXITER [`integer number`] : Maximum Iterations. Minimum: 0 Default: 10 Maximum number of iterations, ignored if set to zero.
    - NORMALISE [`boolean`] : Normalise. Default: 0 Automatically normalise grids by standard deviation before clustering.
    - INITIALIZE [`choice`] : Start Partition. Available Choices: [0] random [1] periodical [2] keep values Default: 0
    - OLDVERSION [`boolean`] : Old Version. Default: 0 slower but memory saving

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('imagery_classification', '1', 'K-Means Clustering for Grids')
    if Tool.is_Okay():
        Tool.Set_Input ('GRIDS', GRIDS)
        Tool.Set_Output('CLUSTER', CLUSTER)
        Tool.Set_Output('STATISTICS', STATISTICS)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('NCLUSTER', NCLUSTER)
        Tool.Set_Option('MAXITER', MAXITER)
        Tool.Set_Option('NORMALISE', NORMALISE)
        Tool.Set_Option('INITIALIZE', INITIALIZE)
        Tool.Set_Option('OLDVERSION', OLDVERSION)
        return Tool.Execute(Verbose)
    return False

def run_tool_imagery_classification_1(GRIDS=None, CLUSTER=None, STATISTICS=None, METHOD=None, NCLUSTER=None, MAXITER=None, NORMALISE=None, INITIALIZE=None, OLDVERSION=None, Verbose=2):
    '''
    K-Means Clustering for Grids
    ----------
    [imagery_classification.1]\n
    This tool implements the K-Means cluster analysis for grids in two variants, iterative minimum distance (Forgy 1965) and hill climbing (Rubin 1967).\n
    Arguments
    ----------
    - GRIDS [`input grid list`] : Grids
    - CLUSTER [`output grid`] : Clusters
    - STATISTICS [`output table`] : Statistics
    - METHOD [`choice`] : Method. Available Choices: [0] Iterative Minimum Distance (Forgy 1965) [1] Hill-Climbing (Rubin 1967) [2] Combined Minimum Distance / Hillclimbing Default: 1
    - NCLUSTER [`integer number`] : Clusters. Minimum: 2 Default: 10 Number of clusters
    - MAXITER [`integer number`] : Maximum Iterations. Minimum: 0 Default: 10 Maximum number of iterations, ignored if set to zero.
    - NORMALISE [`boolean`] : Normalise. Default: 0 Automatically normalise grids by standard deviation before clustering.
    - INITIALIZE [`choice`] : Start Partition. Available Choices: [0] random [1] periodical [2] keep values Default: 0
    - OLDVERSION [`boolean`] : Old Version. Default: 0 slower but memory saving

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('imagery_classification', '1', 'K-Means Clustering for Grids')
    if Tool.is_Okay():
        Tool.Set_Input ('GRIDS', GRIDS)
        Tool.Set_Output('CLUSTER', CLUSTER)
        Tool.Set_Output('STATISTICS', STATISTICS)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('NCLUSTER', NCLUSTER)
        Tool.Set_Option('MAXITER', MAXITER)
        Tool.Set_Option('NORMALISE', NORMALISE)
        Tool.Set_Option('INITIALIZE', INITIALIZE)
        Tool.Set_Option('OLDVERSION', OLDVERSION)
        return Tool.Execute(Verbose)
    return False

def Confusion_Matrix_Two_Grids(ONE=None, TWO=None, ONE_LUT=None, TWO_LUT=None, COMBINED=None, CONFUSION=None, CLASSES=None, SUMMARY=None, ONE_LUT_MIN=None, ONE_LUT_MAX=None, ONE_LUT_NAM=None, TWO_LUT_MIN=None, TWO_LUT_MAX=None, TWO_LUT_NAM=None, NOCHANGE=None, NODATA=None, OUTPUT=None, Verbose=2):
    '''
    Confusion Matrix (Two Grids)
    ----------
    [imagery_classification.2]\n
    The tool allows one to compare two classified grids. It creates a confusion matrix and derived coefficients as well as the combinations of both classifications as new grid. The values of both grids must match each other in order to do the comparison.\n
    The tool provides three options to define the grid classes:\n
    - by providing a look-up table for each grid\n
    - by coloring each grid with a look-up table (colors type = classified)  beforehand (only available in the GUI)\n
    - by preparing the grid values appropriately; i.e., if no look-up table is provided, the tool simply extracts the classes from the grid values found in each grid\n
    A typical application is a change detection analysis based on land cover classification of satellite imagery.\n
    Arguments
    ----------
    - ONE [`input grid`] : Classification 1
    - TWO [`input grid`] : Classification 2
    - ONE_LUT [`optional input table`] : Look-up Table
    - TWO_LUT [`optional input table`] : Look-up Table
    - COMBINED [`output grid`] : Combined Classes
    - CONFUSION [`output table`] : Confusion Matrix
    - CLASSES [`output table`] : Class Values
    - SUMMARY [`output table`] : Summary
    - ONE_LUT_MIN [`table field`] : Value
    - ONE_LUT_MAX [`table field`] : Value (Maximum)
    - ONE_LUT_NAM [`table field`] : Name
    - TWO_LUT_MIN [`table field`] : Value
    - TWO_LUT_MAX [`table field`] : Value (Maximum)
    - TWO_LUT_NAM [`table field`] : Name
    - NOCHANGE [`boolean`] : Report Unchanged Classes. Default: 1
    - NODATA [`boolean`] : Include Unclassified Cells. Default: 1
    - OUTPUT [`choice`] : Output as.... Available Choices: [0] cells [1] percent [2] area Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('imagery_classification', '2', 'Confusion Matrix (Two Grids)')
    if Tool.is_Okay():
        Tool.Set_Input ('ONE', ONE)
        Tool.Set_Input ('TWO', TWO)
        Tool.Set_Input ('ONE_LUT', ONE_LUT)
        Tool.Set_Input ('TWO_LUT', TWO_LUT)
        Tool.Set_Output('COMBINED', COMBINED)
        Tool.Set_Output('CONFUSION', CONFUSION)
        Tool.Set_Output('CLASSES', CLASSES)
        Tool.Set_Output('SUMMARY', SUMMARY)
        Tool.Set_Option('ONE_LUT_MIN', ONE_LUT_MIN)
        Tool.Set_Option('ONE_LUT_MAX', ONE_LUT_MAX)
        Tool.Set_Option('ONE_LUT_NAM', ONE_LUT_NAM)
        Tool.Set_Option('TWO_LUT_MIN', TWO_LUT_MIN)
        Tool.Set_Option('TWO_LUT_MAX', TWO_LUT_MAX)
        Tool.Set_Option('TWO_LUT_NAM', TWO_LUT_NAM)
        Tool.Set_Option('NOCHANGE', NOCHANGE)
        Tool.Set_Option('NODATA', NODATA)
        Tool.Set_Option('OUTPUT', OUTPUT)
        return Tool.Execute(Verbose)
    return False

def run_tool_imagery_classification_2(ONE=None, TWO=None, ONE_LUT=None, TWO_LUT=None, COMBINED=None, CONFUSION=None, CLASSES=None, SUMMARY=None, ONE_LUT_MIN=None, ONE_LUT_MAX=None, ONE_LUT_NAM=None, TWO_LUT_MIN=None, TWO_LUT_MAX=None, TWO_LUT_NAM=None, NOCHANGE=None, NODATA=None, OUTPUT=None, Verbose=2):
    '''
    Confusion Matrix (Two Grids)
    ----------
    [imagery_classification.2]\n
    The tool allows one to compare two classified grids. It creates a confusion matrix and derived coefficients as well as the combinations of both classifications as new grid. The values of both grids must match each other in order to do the comparison.\n
    The tool provides three options to define the grid classes:\n
    - by providing a look-up table for each grid\n
    - by coloring each grid with a look-up table (colors type = classified)  beforehand (only available in the GUI)\n
    - by preparing the grid values appropriately; i.e., if no look-up table is provided, the tool simply extracts the classes from the grid values found in each grid\n
    A typical application is a change detection analysis based on land cover classification of satellite imagery.\n
    Arguments
    ----------
    - ONE [`input grid`] : Classification 1
    - TWO [`input grid`] : Classification 2
    - ONE_LUT [`optional input table`] : Look-up Table
    - TWO_LUT [`optional input table`] : Look-up Table
    - COMBINED [`output grid`] : Combined Classes
    - CONFUSION [`output table`] : Confusion Matrix
    - CLASSES [`output table`] : Class Values
    - SUMMARY [`output table`] : Summary
    - ONE_LUT_MIN [`table field`] : Value
    - ONE_LUT_MAX [`table field`] : Value (Maximum)
    - ONE_LUT_NAM [`table field`] : Name
    - TWO_LUT_MIN [`table field`] : Value
    - TWO_LUT_MAX [`table field`] : Value (Maximum)
    - TWO_LUT_NAM [`table field`] : Name
    - NOCHANGE [`boolean`] : Report Unchanged Classes. Default: 1
    - NODATA [`boolean`] : Include Unclassified Cells. Default: 1
    - OUTPUT [`choice`] : Output as.... Available Choices: [0] cells [1] percent [2] area Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('imagery_classification', '2', 'Confusion Matrix (Two Grids)')
    if Tool.is_Okay():
        Tool.Set_Input ('ONE', ONE)
        Tool.Set_Input ('TWO', TWO)
        Tool.Set_Input ('ONE_LUT', ONE_LUT)
        Tool.Set_Input ('TWO_LUT', TWO_LUT)
        Tool.Set_Output('COMBINED', COMBINED)
        Tool.Set_Output('CONFUSION', CONFUSION)
        Tool.Set_Output('CLASSES', CLASSES)
        Tool.Set_Output('SUMMARY', SUMMARY)
        Tool.Set_Option('ONE_LUT_MIN', ONE_LUT_MIN)
        Tool.Set_Option('ONE_LUT_MAX', ONE_LUT_MAX)
        Tool.Set_Option('ONE_LUT_NAM', ONE_LUT_NAM)
        Tool.Set_Option('TWO_LUT_MIN', TWO_LUT_MIN)
        Tool.Set_Option('TWO_LUT_MAX', TWO_LUT_MAX)
        Tool.Set_Option('TWO_LUT_NAM', TWO_LUT_NAM)
        Tool.Set_Option('NOCHANGE', NOCHANGE)
        Tool.Set_Option('NODATA', NODATA)
        Tool.Set_Option('OUTPUT', OUTPUT)
        return Tool.Execute(Verbose)
    return False

def Decision_Tree(ROOT_GRID=None, CLASSES=None, ROOT_GRID_GRIDSYSTEM=None, ROOT_THRESHOLD=None, ROOT_A_NAME=None, ROOT_A_NODE=None, ROOT_B_NAME=None, ROOT_B_NODE=None, Verbose=2):
    '''
    Decision Tree
    ----------
    [imagery_classification.3]\n
    Decision Tree\n
    Arguments
    ----------
    - ROOT_GRID [`input grid`] : Grid
    - CLASSES [`output grid`] : Decision Tree
    - ROOT_GRID_GRIDSYSTEM [`grid system`] : Grid system
    - ROOT_THRESHOLD [`floating point number`] : Threshold. Default: 0.000000
    - ROOT_A_NAME [`text`] : Name. Default: A
    - ROOT_A_NODE [`boolean`] : Children. Default: 0
    - ROOT_B_NAME [`text`] : Name. Default: B
    - ROOT_B_NODE [`boolean`] : Children. Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('imagery_classification', '3', 'Decision Tree')
    if Tool.is_Okay():
        Tool.Set_Input ('ROOT.GRID', ROOT_GRID)
        Tool.Set_Output('CLASSES', CLASSES)
        Tool.Set_Option('ROOT.GRID_GRIDSYSTEM', ROOT_GRID_GRIDSYSTEM)
        Tool.Set_Option('ROOT.THRESHOLD', ROOT_THRESHOLD)
        Tool.Set_Option('ROOT.A|NAME', ROOT_A_NAME)
        Tool.Set_Option('ROOT.A|NODE', ROOT_A_NODE)
        Tool.Set_Option('ROOT.B|NAME', ROOT_B_NAME)
        Tool.Set_Option('ROOT.B|NODE', ROOT_B_NODE)
        return Tool.Execute(Verbose)
    return False

def run_tool_imagery_classification_3(ROOT_GRID=None, CLASSES=None, ROOT_GRID_GRIDSYSTEM=None, ROOT_THRESHOLD=None, ROOT_A_NAME=None, ROOT_A_NODE=None, ROOT_B_NAME=None, ROOT_B_NODE=None, Verbose=2):
    '''
    Decision Tree
    ----------
    [imagery_classification.3]\n
    Decision Tree\n
    Arguments
    ----------
    - ROOT_GRID [`input grid`] : Grid
    - CLASSES [`output grid`] : Decision Tree
    - ROOT_GRID_GRIDSYSTEM [`grid system`] : Grid system
    - ROOT_THRESHOLD [`floating point number`] : Threshold. Default: 0.000000
    - ROOT_A_NAME [`text`] : Name. Default: A
    - ROOT_A_NODE [`boolean`] : Children. Default: 0
    - ROOT_B_NAME [`text`] : Name. Default: B
    - ROOT_B_NODE [`boolean`] : Children. Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('imagery_classification', '3', 'Decision Tree')
    if Tool.is_Okay():
        Tool.Set_Input ('ROOT.GRID', ROOT_GRID)
        Tool.Set_Output('CLASSES', CLASSES)
        Tool.Set_Option('ROOT.GRID_GRIDSYSTEM', ROOT_GRID_GRIDSYSTEM)
        Tool.Set_Option('ROOT.THRESHOLD', ROOT_THRESHOLD)
        Tool.Set_Option('ROOT.A|NAME', ROOT_A_NAME)
        Tool.Set_Option('ROOT.A|NODE', ROOT_A_NODE)
        Tool.Set_Option('ROOT.B|NAME', ROOT_B_NAME)
        Tool.Set_Option('ROOT.B|NODE', ROOT_B_NODE)
        return Tool.Execute(Verbose)
    return False

def Confusion_Matrix_Polygons__Grid(GRID=None, POLYGONS=None, GRID_LUT=None, CONFUSION=None, CLASSES=None, SUMMARY=None, GRID_VALUES=None, GRID_LUT_MIN=None, GRID_LUT_MAX=None, GRID_LUT_NAM=None, FIELD=None, NO_CLASS=None, Verbose=2):
    '''
    Confusion Matrix (Polygons / Grid)
    ----------
    [imagery_classification.6]\n
    Compares a classified polygons layer with grid classes and creates a confusion matrix and derived coefficients. Grid classes have to be defined with a look-up table and values must match those of the polygon classes for the subsequent comparison. This tool is typically used for a quality assessment of a supervised classification.\n
    Arguments
    ----------
    - GRID [`input grid`] : Classification
    - POLYGONS [`input shapes`] : Polygons
    - GRID_LUT [`optional input table`] : Look-up Table
    - CONFUSION [`output table`] : Confusion Matrix
    - CLASSES [`output table`] : Class Values
    - SUMMARY [`output table`] : Summary
    - GRID_VALUES [`choice`] : Value Interpretation. Available Choices: [0] values are class identifiers [1] use look-up table Default: 1
    - GRID_LUT_MIN [`table field`] : Value
    - GRID_LUT_MAX [`table field`] : Value (Maximum)
    - GRID_LUT_NAM [`table field`] : Name
    - FIELD [`table field`] : Classes
    - NO_CLASS [`boolean`] : Unclassified. Default: 1

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('imagery_classification', '6', 'Confusion Matrix (Polygons / Grid)')
    if Tool.is_Okay():
        Tool.Set_Input ('GRID', GRID)
        Tool.Set_Input ('POLYGONS', POLYGONS)
        Tool.Set_Input ('GRID_LUT', GRID_LUT)
        Tool.Set_Output('CONFUSION', CONFUSION)
        Tool.Set_Output('CLASSES', CLASSES)
        Tool.Set_Output('SUMMARY', SUMMARY)
        Tool.Set_Option('GRID_VALUES', GRID_VALUES)
        Tool.Set_Option('GRID_LUT_MIN', GRID_LUT_MIN)
        Tool.Set_Option('GRID_LUT_MAX', GRID_LUT_MAX)
        Tool.Set_Option('GRID_LUT_NAM', GRID_LUT_NAM)
        Tool.Set_Option('FIELD', FIELD)
        Tool.Set_Option('NO_CLASS', NO_CLASS)
        return Tool.Execute(Verbose)
    return False

def run_tool_imagery_classification_6(GRID=None, POLYGONS=None, GRID_LUT=None, CONFUSION=None, CLASSES=None, SUMMARY=None, GRID_VALUES=None, GRID_LUT_MIN=None, GRID_LUT_MAX=None, GRID_LUT_NAM=None, FIELD=None, NO_CLASS=None, Verbose=2):
    '''
    Confusion Matrix (Polygons / Grid)
    ----------
    [imagery_classification.6]\n
    Compares a classified polygons layer with grid classes and creates a confusion matrix and derived coefficients. Grid classes have to be defined with a look-up table and values must match those of the polygon classes for the subsequent comparison. This tool is typically used for a quality assessment of a supervised classification.\n
    Arguments
    ----------
    - GRID [`input grid`] : Classification
    - POLYGONS [`input shapes`] : Polygons
    - GRID_LUT [`optional input table`] : Look-up Table
    - CONFUSION [`output table`] : Confusion Matrix
    - CLASSES [`output table`] : Class Values
    - SUMMARY [`output table`] : Summary
    - GRID_VALUES [`choice`] : Value Interpretation. Available Choices: [0] values are class identifiers [1] use look-up table Default: 1
    - GRID_LUT_MIN [`table field`] : Value
    - GRID_LUT_MAX [`table field`] : Value (Maximum)
    - GRID_LUT_NAM [`table field`] : Name
    - FIELD [`table field`] : Classes
    - NO_CLASS [`boolean`] : Unclassified. Default: 1

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('imagery_classification', '6', 'Confusion Matrix (Polygons / Grid)')
    if Tool.is_Okay():
        Tool.Set_Input ('GRID', GRID)
        Tool.Set_Input ('POLYGONS', POLYGONS)
        Tool.Set_Input ('GRID_LUT', GRID_LUT)
        Tool.Set_Output('CONFUSION', CONFUSION)
        Tool.Set_Output('CLASSES', CLASSES)
        Tool.Set_Output('SUMMARY', SUMMARY)
        Tool.Set_Option('GRID_VALUES', GRID_VALUES)
        Tool.Set_Option('GRID_LUT_MIN', GRID_LUT_MIN)
        Tool.Set_Option('GRID_LUT_MAX', GRID_LUT_MAX)
        Tool.Set_Option('GRID_LUT_NAM', GRID_LUT_NAM)
        Tool.Set_Option('FIELD', FIELD)
        Tool.Set_Option('NO_CLASS', NO_CLASS)
        return Tool.Execute(Verbose)
    return False

def Local_Climate_Zone_Classification(FEATURES=None, LCZC=None, LCZC_FILTERED=None, GRID_SYSTEM=None, GRIDDEFILE=None, FILE_TRAINING=None, CLASSIFIER=None, CLASS_DEF_SRC=None, CLASS_DEF_FILE=None, LCZC_FILE=None, FILE_FILTERED_LCZC=None, FILTER_RADIUS=None, Verbose=2):
    '''
    Local Climate Zone Classification
    ----------
    [imagery_classification.lczc]\n
    Updates:\n
    \n
    (-) [2016/08/15] Automated filtering of Style Place Holders\n
    (-) [2016/09/07] added cmd line support for kml import\n
    \n
    \n
    \n
    Arguments
    ----------
    - FEATURES [`input grid list`] : Features
    - LCZC [`output grid`] : LCZC
    - LCZC_FILTERED [`output grid`] : LCZC (Filtered)
    - GRID_SYSTEM [`grid system`] : Grid System
    - GRIDDEFILE [`file path`] : grid definition for KML2shp conversion. well known text, prj file - workaround for command line
    - FILE_TRAINING [`file path`] : Training Areas
    - CLASSIFIER [`choice`] : Classifier. Available Choices: [0] Random Forest (ViGrA) [1] Random Forest (OpenCV) [2] Normal Bayes [3] Support Vector Machine [4] Artificial Neural Network Default: 0
    - CLASS_DEF_SRC [`choice`] : Class Definition. Available Choices: [0] built-in [1] from file Default: 0
    - CLASS_DEF_FILE [`file path`] : Class Definition File
    - LCZC_FILE [`file path`] : KMZ File. Export to KMZ file (Google Earth). Ignored if empty.
    - FILE_FILTERED_LCZC [`file path`] : KMZ File. Export to KMZ file (Google Earth). Ignored if empty.
    - FILTER_RADIUS [`integer number`] : Majority Filter Radius. Minimum: 1 Default: 3

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('imagery_classification', 'lczc', 'Local Climate Zone Classification')
    if Tool.is_Okay():
        Tool.Set_Input ('FEATURES', FEATURES)
        Tool.Set_Output('LCZC', LCZC)
        Tool.Set_Output('LCZC_FILTERED', LCZC_FILTERED)
        Tool.Set_Option('GRID_SYSTEM', GRID_SYSTEM)
        Tool.Set_Option('GRIDDEFILE', GRIDDEFILE)
        Tool.Set_Option('FILE_TRAINING', FILE_TRAINING)
        Tool.Set_Option('CLASSIFIER', CLASSIFIER)
        Tool.Set_Option('CLASS_DEF_SRC', CLASS_DEF_SRC)
        Tool.Set_Option('CLASS_DEF_FILE', CLASS_DEF_FILE)
        Tool.Set_Option('LCZC_FILE', LCZC_FILE)
        Tool.Set_Option('FILE_FILTERED_LCZC', FILE_FILTERED_LCZC)
        Tool.Set_Option('FILTER_RADIUS', FILTER_RADIUS)
        return Tool.Execute(Verbose)
    return False

def run_tool_imagery_classification_lczc(FEATURES=None, LCZC=None, LCZC_FILTERED=None, GRID_SYSTEM=None, GRIDDEFILE=None, FILE_TRAINING=None, CLASSIFIER=None, CLASS_DEF_SRC=None, CLASS_DEF_FILE=None, LCZC_FILE=None, FILE_FILTERED_LCZC=None, FILTER_RADIUS=None, Verbose=2):
    '''
    Local Climate Zone Classification
    ----------
    [imagery_classification.lczc]\n
    Updates:\n
    \n
    (-) [2016/08/15] Automated filtering of Style Place Holders\n
    (-) [2016/09/07] added cmd line support for kml import\n
    \n
    \n
    \n
    Arguments
    ----------
    - FEATURES [`input grid list`] : Features
    - LCZC [`output grid`] : LCZC
    - LCZC_FILTERED [`output grid`] : LCZC (Filtered)
    - GRID_SYSTEM [`grid system`] : Grid System
    - GRIDDEFILE [`file path`] : grid definition for KML2shp conversion. well known text, prj file - workaround for command line
    - FILE_TRAINING [`file path`] : Training Areas
    - CLASSIFIER [`choice`] : Classifier. Available Choices: [0] Random Forest (ViGrA) [1] Random Forest (OpenCV) [2] Normal Bayes [3] Support Vector Machine [4] Artificial Neural Network Default: 0
    - CLASS_DEF_SRC [`choice`] : Class Definition. Available Choices: [0] built-in [1] from file Default: 0
    - CLASS_DEF_FILE [`file path`] : Class Definition File
    - LCZC_FILE [`file path`] : KMZ File. Export to KMZ file (Google Earth). Ignored if empty.
    - FILE_FILTERED_LCZC [`file path`] : KMZ File. Export to KMZ file (Google Earth). Ignored if empty.
    - FILTER_RADIUS [`integer number`] : Majority Filter Radius. Minimum: 1 Default: 3

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('imagery_classification', 'lczc', 'Local Climate Zone Classification')
    if Tool.is_Okay():
        Tool.Set_Input ('FEATURES', FEATURES)
        Tool.Set_Output('LCZC', LCZC)
        Tool.Set_Output('LCZC_FILTERED', LCZC_FILTERED)
        Tool.Set_Option('GRID_SYSTEM', GRID_SYSTEM)
        Tool.Set_Option('GRIDDEFILE', GRIDDEFILE)
        Tool.Set_Option('FILE_TRAINING', FILE_TRAINING)
        Tool.Set_Option('CLASSIFIER', CLASSIFIER)
        Tool.Set_Option('CLASS_DEF_SRC', CLASS_DEF_SRC)
        Tool.Set_Option('CLASS_DEF_FILE', CLASS_DEF_FILE)
        Tool.Set_Option('LCZC_FILE', LCZC_FILE)
        Tool.Set_Option('FILE_FILTERED_LCZC', FILE_FILTERED_LCZC)
        Tool.Set_Option('FILTER_RADIUS', FILTER_RADIUS)
        return Tool.Execute(Verbose)
    return False

def Supervised_Majority_Choice_Image_Classification(FEATURES=None, TRAIN_AREAS=None, TRAIN_SAMPLES=None, CLASSES=None, MAJORITY_COUNT=None, NUNIQUES=None, NORMALIZE=None, MODEL_TRAIN=None, TRAIN_CLASS=None, TRAIN_BUFFER=None, GRID_SYSTEM=None, UNAMBIGUOUS=None, CLASSIFY_BOX=None, CLASSIFY_MINDIST=None, CLASSIFY_MAHALONOBIS=None, CLASSIFY_MAXLIKE=None, CLASSIFY_SAM=None, CLASSIFY_BAYES=None, CLASSIFY_DT=None, CLASSIFY_RF=None, CLASSIFY_SVM=None, CLASSIFY_KNN=None, CLASSIFY_ANN=None, Verbose=2):
    '''
    Supervised Majority Choice Image Classification
    ----------
    [imagery_classification.classify_majority]\n
    The majority choice tool for supervised image classification runs the selected classification tools using standard settings and takes for each pixel of the resulting classification the class that has been identified most often by the individual classifiers.\n
    'Random Forest' is not selected by default because it generates randomized results with more or less strong differences each time it is run.\n
    'K-Nearest Neighbours' and 'Artificial Neural Network' have been excluded from default, because using them might be a bit more time consuming.\n
    \n
    Arguments
    ----------
    - FEATURES [`input grid list`] : Features
    - TRAIN_AREAS [`input shapes`] : Training Areas
    - TRAIN_SAMPLES [`input table`] : Training Samples
    - CLASSES [`output grid`] : Majority Choice
    - MAJORITY_COUNT [`output grid`] : Majority Count
    - NUNIQUES [`output grid`] : Classes Count
    - NORMALIZE [`boolean`] : Normalize. Default: 0
    - MODEL_TRAIN [`choice`] : Training. Available Choices: [0] training areas [1] training samples Default: 0
    - TRAIN_CLASS [`table field`] : Class Identifier
    - TRAIN_BUFFER [`floating point number`] : Buffer Size. Minimum: 0.000000 Default: 30.000000 For non-polygon type training areas, creates a buffer with a diameter of specified size.
    - GRID_SYSTEM [`grid system`] : Grid System
    - UNAMBIGUOUS [`boolean`] : Unambiguous. Default: 0 Do not classify a pixel if more than one class reaches the same majority count for it.
    - CLASSIFY_BOX [`boolean`] : Parallel Epiped. Default: 1
    - CLASSIFY_MINDIST [`boolean`] : Minimum Distance. Default: 1
    - CLASSIFY_MAHALONOBIS [`boolean`] : Mahalonobis Distance. Default: 1
    - CLASSIFY_MAXLIKE [`boolean`] : Maximum Likelihood. Default: 1
    - CLASSIFY_SAM [`boolean`] : Spectral Angle Mapping. Default: 1
    - CLASSIFY_BAYES [`boolean`] : Normal Bayes. Default: 1
    - CLASSIFY_DT [`boolean`] : Decision Tree. Default: 1
    - CLASSIFY_RF [`boolean`] : Random Forest. Default: 0
    - CLASSIFY_SVM [`boolean`] : Support Vector Machine. Default: 1
    - CLASSIFY_KNN [`boolean`] : K-Nearest Neighbours. Default: 0
    - CLASSIFY_ANN [`boolean`] : Artificial Neural Network. Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('imagery_classification', 'classify_majority', 'Supervised Majority Choice Image Classification')
    if Tool.is_Okay():
        Tool.Set_Input ('FEATURES', FEATURES)
        Tool.Set_Input ('TRAIN_AREAS', TRAIN_AREAS)
        Tool.Set_Input ('TRAIN_SAMPLES', TRAIN_SAMPLES)
        Tool.Set_Output('CLASSES', CLASSES)
        Tool.Set_Output('MAJORITY_COUNT', MAJORITY_COUNT)
        Tool.Set_Output('NUNIQUES', NUNIQUES)
        Tool.Set_Option('NORMALIZE', NORMALIZE)
        Tool.Set_Option('MODEL_TRAIN', MODEL_TRAIN)
        Tool.Set_Option('TRAIN_CLASS', TRAIN_CLASS)
        Tool.Set_Option('TRAIN_BUFFER', TRAIN_BUFFER)
        Tool.Set_Option('GRID_SYSTEM', GRID_SYSTEM)
        Tool.Set_Option('UNAMBIGUOUS', UNAMBIGUOUS)
        Tool.Set_Option('CLASSIFY_BOX', CLASSIFY_BOX)
        Tool.Set_Option('CLASSIFY_MINDIST', CLASSIFY_MINDIST)
        Tool.Set_Option('CLASSIFY_MAHALONOBIS', CLASSIFY_MAHALONOBIS)
        Tool.Set_Option('CLASSIFY_MAXLIKE', CLASSIFY_MAXLIKE)
        Tool.Set_Option('CLASSIFY_SAM', CLASSIFY_SAM)
        Tool.Set_Option('CLASSIFY_BAYES', CLASSIFY_BAYES)
        Tool.Set_Option('CLASSIFY_DT', CLASSIFY_DT)
        Tool.Set_Option('CLASSIFY_RF', CLASSIFY_RF)
        Tool.Set_Option('CLASSIFY_SVM', CLASSIFY_SVM)
        Tool.Set_Option('CLASSIFY_KNN', CLASSIFY_KNN)
        Tool.Set_Option('CLASSIFY_ANN', CLASSIFY_ANN)
        return Tool.Execute(Verbose)
    return False

def run_tool_imagery_classification_classify_majority(FEATURES=None, TRAIN_AREAS=None, TRAIN_SAMPLES=None, CLASSES=None, MAJORITY_COUNT=None, NUNIQUES=None, NORMALIZE=None, MODEL_TRAIN=None, TRAIN_CLASS=None, TRAIN_BUFFER=None, GRID_SYSTEM=None, UNAMBIGUOUS=None, CLASSIFY_BOX=None, CLASSIFY_MINDIST=None, CLASSIFY_MAHALONOBIS=None, CLASSIFY_MAXLIKE=None, CLASSIFY_SAM=None, CLASSIFY_BAYES=None, CLASSIFY_DT=None, CLASSIFY_RF=None, CLASSIFY_SVM=None, CLASSIFY_KNN=None, CLASSIFY_ANN=None, Verbose=2):
    '''
    Supervised Majority Choice Image Classification
    ----------
    [imagery_classification.classify_majority]\n
    The majority choice tool for supervised image classification runs the selected classification tools using standard settings and takes for each pixel of the resulting classification the class that has been identified most often by the individual classifiers.\n
    'Random Forest' is not selected by default because it generates randomized results with more or less strong differences each time it is run.\n
    'K-Nearest Neighbours' and 'Artificial Neural Network' have been excluded from default, because using them might be a bit more time consuming.\n
    \n
    Arguments
    ----------
    - FEATURES [`input grid list`] : Features
    - TRAIN_AREAS [`input shapes`] : Training Areas
    - TRAIN_SAMPLES [`input table`] : Training Samples
    - CLASSES [`output grid`] : Majority Choice
    - MAJORITY_COUNT [`output grid`] : Majority Count
    - NUNIQUES [`output grid`] : Classes Count
    - NORMALIZE [`boolean`] : Normalize. Default: 0
    - MODEL_TRAIN [`choice`] : Training. Available Choices: [0] training areas [1] training samples Default: 0
    - TRAIN_CLASS [`table field`] : Class Identifier
    - TRAIN_BUFFER [`floating point number`] : Buffer Size. Minimum: 0.000000 Default: 30.000000 For non-polygon type training areas, creates a buffer with a diameter of specified size.
    - GRID_SYSTEM [`grid system`] : Grid System
    - UNAMBIGUOUS [`boolean`] : Unambiguous. Default: 0 Do not classify a pixel if more than one class reaches the same majority count for it.
    - CLASSIFY_BOX [`boolean`] : Parallel Epiped. Default: 1
    - CLASSIFY_MINDIST [`boolean`] : Minimum Distance. Default: 1
    - CLASSIFY_MAHALONOBIS [`boolean`] : Mahalonobis Distance. Default: 1
    - CLASSIFY_MAXLIKE [`boolean`] : Maximum Likelihood. Default: 1
    - CLASSIFY_SAM [`boolean`] : Spectral Angle Mapping. Default: 1
    - CLASSIFY_BAYES [`boolean`] : Normal Bayes. Default: 1
    - CLASSIFY_DT [`boolean`] : Decision Tree. Default: 1
    - CLASSIFY_RF [`boolean`] : Random Forest. Default: 0
    - CLASSIFY_SVM [`boolean`] : Support Vector Machine. Default: 1
    - CLASSIFY_KNN [`boolean`] : K-Nearest Neighbours. Default: 0
    - CLASSIFY_ANN [`boolean`] : Artificial Neural Network. Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('imagery_classification', 'classify_majority', 'Supervised Majority Choice Image Classification')
    if Tool.is_Okay():
        Tool.Set_Input ('FEATURES', FEATURES)
        Tool.Set_Input ('TRAIN_AREAS', TRAIN_AREAS)
        Tool.Set_Input ('TRAIN_SAMPLES', TRAIN_SAMPLES)
        Tool.Set_Output('CLASSES', CLASSES)
        Tool.Set_Output('MAJORITY_COUNT', MAJORITY_COUNT)
        Tool.Set_Output('NUNIQUES', NUNIQUES)
        Tool.Set_Option('NORMALIZE', NORMALIZE)
        Tool.Set_Option('MODEL_TRAIN', MODEL_TRAIN)
        Tool.Set_Option('TRAIN_CLASS', TRAIN_CLASS)
        Tool.Set_Option('TRAIN_BUFFER', TRAIN_BUFFER)
        Tool.Set_Option('GRID_SYSTEM', GRID_SYSTEM)
        Tool.Set_Option('UNAMBIGUOUS', UNAMBIGUOUS)
        Tool.Set_Option('CLASSIFY_BOX', CLASSIFY_BOX)
        Tool.Set_Option('CLASSIFY_MINDIST', CLASSIFY_MINDIST)
        Tool.Set_Option('CLASSIFY_MAHALONOBIS', CLASSIFY_MAHALONOBIS)
        Tool.Set_Option('CLASSIFY_MAXLIKE', CLASSIFY_MAXLIKE)
        Tool.Set_Option('CLASSIFY_SAM', CLASSIFY_SAM)
        Tool.Set_Option('CLASSIFY_BAYES', CLASSIFY_BAYES)
        Tool.Set_Option('CLASSIFY_DT', CLASSIFY_DT)
        Tool.Set_Option('CLASSIFY_RF', CLASSIFY_RF)
        Tool.Set_Option('CLASSIFY_SVM', CLASSIFY_SVM)
        Tool.Set_Option('CLASSIFY_KNN', CLASSIFY_KNN)
        Tool.Set_Option('CLASSIFY_ANN', CLASSIFY_ANN)
        return Tool.Execute(Verbose)
    return False

