#! /usr/bin/env python

'''
Python Interface to SAGA Tools Library
----------
- Category : Terrain Analysis
- Name     : Hydrology
- ID       : ta_hydrology

Description
----------
Tools for digital terrain analysis.
'''

from PySAGA.helper import Tool_Wrapper

def Flow_Accumulation_TopDown(ELEVATION=None, ACCU_TARGET=None, SINKROUTE=None, WEIGHTS=None, VAL_INPUT=None, ACCU_MATERIAL=None, LINEAR_VAL=None, LINEAR_DIR=None, FLOW=None, VAL_MEAN=None, ACCU_TOTAL=None, ACCU_LEFT=None, ACCU_RIGHT=None, FLOW_LENGTH=None, WEIGHT_LOSS=None, STEP=None, FLOW_UNIT=None, METHOD=None, LINEAR_DO=None, LINEAR_MIN=None, CONVERGENCE=None, MFD_CONTOUR=None, NO_NEGATIVES=None, Verbose=2):
    '''
    Flow Accumulation (Top-Down)
    ----------
    [ta_hydrology.0]\n
    Top-down processing of cells for calculation of flow accumulation and related parameters. This set of algorithms processes a DEM downwards from the highest to the lowest cell.\n
    Flow routing methods provided by this tool:\n
    (-) Deterministic 8 (aka D8, O'Callaghan & Mark 1984)\n
    (-) Braunschweiger Reliefmodell (Bauer et al. 1985)\n
    (-) Rho 8 (Fairfield & Leymarie 1991)\n
    (-) Multiple Flow Direction (Freeman 1991, Quinn et al. 1991)\n
    (-) Deterministic Infinity (Tarboton 1997)\n
    (-) Triangular Multiple Flow Direction (Seibert & McGlynn 2007\n
    (-) Multiple Flow Direction based on Maximum Downslope Gradient (Qin et al. 2011)\n
    Arguments
    ----------
    - ELEVATION [`input grid`] : Elevation
    - ACCU_TARGET [`input grid`] : Accumulation Target
    - SINKROUTE [`optional input grid`] : Sink Routes
    - WEIGHTS [`optional input grid`] : Weights
    - VAL_INPUT [`optional input grid`] : Input for Mean over Catchment
    - ACCU_MATERIAL [`optional input grid`] : Material for Accumulation
    - LINEAR_VAL [`optional input grid`] : Linear Flow Threshold Grid. optional grid providing values to be compared with linear flow threshold instead of flow accumulation
    - LINEAR_DIR [`optional input grid`] : Channel Direction. use this for (linear) flow routing, if the value is a valid direction (0-7 = N, NE, E, SE, S, SW, W, NW)
    - FLOW [`output grid`] : Flow Accumulation
    - VAL_MEAN [`output grid`] : Mean over Catchment
    - ACCU_TOTAL [`output grid`] : Accumulated Material
    - ACCU_LEFT [`output grid`] : Accumulated Material (Left Side)
    - ACCU_RIGHT [`output grid`] : Accumulated Material (Right Side)
    - FLOW_LENGTH [`output grid`] : Flow Path Length. average distance that a cell's accumulated flow travelled
    - WEIGHT_LOSS [`output grid`] : Loss through Negative Weights. when using weights without support for negative flow: output of the absolute amount of negative flow that occurred
    - STEP [`integer number`] : Step. Minimum: 1 Default: 1 For testing purposes. Only generate flow at cells with step distance (each step row/column).
    - FLOW_UNIT [`choice`] : Flow Accumulation Unit. Available Choices: [0] number of cells [1] cell area Default: 1
    - METHOD [`choice`] : Method. Available Choices: [0] Deterministic 8 [1] Rho 8 [2] Braunschweiger Reliefmodell [3] Deterministic Infinity [4] Multiple Flow Direction [5] Multiple Triangular Flow Direction [6] Multiple Maximum Downslope Gradient Based Flow Direction Default: 4
    - LINEAR_DO [`boolean`] : Thresholded Linear Flow. Default: 0 apply linear flow routing (D8) to all cells, having a flow accumulation greater than the specified threshold
    - LINEAR_MIN [`integer number`] : Linear Flow Threshold. Minimum: 0 Default: 500 flow accumulation threshold (cells) for linear flow routing
    - CONVERGENCE [`floating point number`] : Convergence. Minimum: 0.001000 Default: 1.100000 Convergence factor for Multiple Flow Direction Algorithm (Freeman 1991).
Applies also to the Multiple Triangular Flow Direction Algorithm.
    - MFD_CONTOUR [`boolean`] : Contour Length. Default: 0 Include (pseudo) contour length as additional weighting factor in multiple flow direction routing, reduces flow to diagonal neighbour cells by a factor of 0.71 (s. Quinn et al. 1991 for details).
    - NO_NEGATIVES [`boolean`] : Prevent Negative Flow Accumulation. Default: 1 when using weights: do not transport negative flow, set it to zero instead; useful e.g. when accumulating measures of water balance.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_hydrology', '0', 'Flow Accumulation (Top-Down)')
    if Tool.is_Okay():
        Tool.Set_Input ('ELEVATION', ELEVATION)
        Tool.Set_Input ('ACCU_TARGET', ACCU_TARGET)
        Tool.Set_Input ('SINKROUTE', SINKROUTE)
        Tool.Set_Input ('WEIGHTS', WEIGHTS)
        Tool.Set_Input ('VAL_INPUT', VAL_INPUT)
        Tool.Set_Input ('ACCU_MATERIAL', ACCU_MATERIAL)
        Tool.Set_Input ('LINEAR_VAL', LINEAR_VAL)
        Tool.Set_Input ('LINEAR_DIR', LINEAR_DIR)
        Tool.Set_Output('FLOW', FLOW)
        Tool.Set_Output('VAL_MEAN', VAL_MEAN)
        Tool.Set_Output('ACCU_TOTAL', ACCU_TOTAL)
        Tool.Set_Output('ACCU_LEFT', ACCU_LEFT)
        Tool.Set_Output('ACCU_RIGHT', ACCU_RIGHT)
        Tool.Set_Output('FLOW_LENGTH', FLOW_LENGTH)
        Tool.Set_Output('WEIGHT_LOSS', WEIGHT_LOSS)
        Tool.Set_Option('STEP', STEP)
        Tool.Set_Option('FLOW_UNIT', FLOW_UNIT)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('LINEAR_DO', LINEAR_DO)
        Tool.Set_Option('LINEAR_MIN', LINEAR_MIN)
        Tool.Set_Option('CONVERGENCE', CONVERGENCE)
        Tool.Set_Option('MFD_CONTOUR', MFD_CONTOUR)
        Tool.Set_Option('NO_NEGATIVES', NO_NEGATIVES)
        return Tool.Execute(Verbose)
    return False

def run_tool_ta_hydrology_0(ELEVATION=None, ACCU_TARGET=None, SINKROUTE=None, WEIGHTS=None, VAL_INPUT=None, ACCU_MATERIAL=None, LINEAR_VAL=None, LINEAR_DIR=None, FLOW=None, VAL_MEAN=None, ACCU_TOTAL=None, ACCU_LEFT=None, ACCU_RIGHT=None, FLOW_LENGTH=None, WEIGHT_LOSS=None, STEP=None, FLOW_UNIT=None, METHOD=None, LINEAR_DO=None, LINEAR_MIN=None, CONVERGENCE=None, MFD_CONTOUR=None, NO_NEGATIVES=None, Verbose=2):
    '''
    Flow Accumulation (Top-Down)
    ----------
    [ta_hydrology.0]\n
    Top-down processing of cells for calculation of flow accumulation and related parameters. This set of algorithms processes a DEM downwards from the highest to the lowest cell.\n
    Flow routing methods provided by this tool:\n
    (-) Deterministic 8 (aka D8, O'Callaghan & Mark 1984)\n
    (-) Braunschweiger Reliefmodell (Bauer et al. 1985)\n
    (-) Rho 8 (Fairfield & Leymarie 1991)\n
    (-) Multiple Flow Direction (Freeman 1991, Quinn et al. 1991)\n
    (-) Deterministic Infinity (Tarboton 1997)\n
    (-) Triangular Multiple Flow Direction (Seibert & McGlynn 2007\n
    (-) Multiple Flow Direction based on Maximum Downslope Gradient (Qin et al. 2011)\n
    Arguments
    ----------
    - ELEVATION [`input grid`] : Elevation
    - ACCU_TARGET [`input grid`] : Accumulation Target
    - SINKROUTE [`optional input grid`] : Sink Routes
    - WEIGHTS [`optional input grid`] : Weights
    - VAL_INPUT [`optional input grid`] : Input for Mean over Catchment
    - ACCU_MATERIAL [`optional input grid`] : Material for Accumulation
    - LINEAR_VAL [`optional input grid`] : Linear Flow Threshold Grid. optional grid providing values to be compared with linear flow threshold instead of flow accumulation
    - LINEAR_DIR [`optional input grid`] : Channel Direction. use this for (linear) flow routing, if the value is a valid direction (0-7 = N, NE, E, SE, S, SW, W, NW)
    - FLOW [`output grid`] : Flow Accumulation
    - VAL_MEAN [`output grid`] : Mean over Catchment
    - ACCU_TOTAL [`output grid`] : Accumulated Material
    - ACCU_LEFT [`output grid`] : Accumulated Material (Left Side)
    - ACCU_RIGHT [`output grid`] : Accumulated Material (Right Side)
    - FLOW_LENGTH [`output grid`] : Flow Path Length. average distance that a cell's accumulated flow travelled
    - WEIGHT_LOSS [`output grid`] : Loss through Negative Weights. when using weights without support for negative flow: output of the absolute amount of negative flow that occurred
    - STEP [`integer number`] : Step. Minimum: 1 Default: 1 For testing purposes. Only generate flow at cells with step distance (each step row/column).
    - FLOW_UNIT [`choice`] : Flow Accumulation Unit. Available Choices: [0] number of cells [1] cell area Default: 1
    - METHOD [`choice`] : Method. Available Choices: [0] Deterministic 8 [1] Rho 8 [2] Braunschweiger Reliefmodell [3] Deterministic Infinity [4] Multiple Flow Direction [5] Multiple Triangular Flow Direction [6] Multiple Maximum Downslope Gradient Based Flow Direction Default: 4
    - LINEAR_DO [`boolean`] : Thresholded Linear Flow. Default: 0 apply linear flow routing (D8) to all cells, having a flow accumulation greater than the specified threshold
    - LINEAR_MIN [`integer number`] : Linear Flow Threshold. Minimum: 0 Default: 500 flow accumulation threshold (cells) for linear flow routing
    - CONVERGENCE [`floating point number`] : Convergence. Minimum: 0.001000 Default: 1.100000 Convergence factor for Multiple Flow Direction Algorithm (Freeman 1991).
Applies also to the Multiple Triangular Flow Direction Algorithm.
    - MFD_CONTOUR [`boolean`] : Contour Length. Default: 0 Include (pseudo) contour length as additional weighting factor in multiple flow direction routing, reduces flow to diagonal neighbour cells by a factor of 0.71 (s. Quinn et al. 1991 for details).
    - NO_NEGATIVES [`boolean`] : Prevent Negative Flow Accumulation. Default: 1 when using weights: do not transport negative flow, set it to zero instead; useful e.g. when accumulating measures of water balance.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_hydrology', '0', 'Flow Accumulation (Top-Down)')
    if Tool.is_Okay():
        Tool.Set_Input ('ELEVATION', ELEVATION)
        Tool.Set_Input ('ACCU_TARGET', ACCU_TARGET)
        Tool.Set_Input ('SINKROUTE', SINKROUTE)
        Tool.Set_Input ('WEIGHTS', WEIGHTS)
        Tool.Set_Input ('VAL_INPUT', VAL_INPUT)
        Tool.Set_Input ('ACCU_MATERIAL', ACCU_MATERIAL)
        Tool.Set_Input ('LINEAR_VAL', LINEAR_VAL)
        Tool.Set_Input ('LINEAR_DIR', LINEAR_DIR)
        Tool.Set_Output('FLOW', FLOW)
        Tool.Set_Output('VAL_MEAN', VAL_MEAN)
        Tool.Set_Output('ACCU_TOTAL', ACCU_TOTAL)
        Tool.Set_Output('ACCU_LEFT', ACCU_LEFT)
        Tool.Set_Output('ACCU_RIGHT', ACCU_RIGHT)
        Tool.Set_Output('FLOW_LENGTH', FLOW_LENGTH)
        Tool.Set_Output('WEIGHT_LOSS', WEIGHT_LOSS)
        Tool.Set_Option('STEP', STEP)
        Tool.Set_Option('FLOW_UNIT', FLOW_UNIT)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('LINEAR_DO', LINEAR_DO)
        Tool.Set_Option('LINEAR_MIN', LINEAR_MIN)
        Tool.Set_Option('CONVERGENCE', CONVERGENCE)
        Tool.Set_Option('MFD_CONTOUR', MFD_CONTOUR)
        Tool.Set_Option('NO_NEGATIVES', NO_NEGATIVES)
        return Tool.Execute(Verbose)
    return False

def Flow_Accumulation_Recursive(ELEVATION=None, ACCU_TARGET=None, SINKROUTE=None, WEIGHTS=None, VAL_INPUT=None, ACCU_MATERIAL=None, TARGETS=None, FLOW=None, VAL_MEAN=None, ACCU_TOTAL=None, ACCU_LEFT=None, ACCU_RIGHT=None, FLOW_LENGTH=None, WEIGHT_LOSS=None, FLOW_UNIT=None, METHOD=None, CONVERGENCE=None, MFD_CONTOUR=None, NO_NEGATIVES=None, Verbose=2):
    '''
    Flow Accumulation (Recursive)
    ----------
    [ta_hydrology.1]\n
    Recursive upward processing of cells for calculation of flow accumulation and related parameters. This set of algorithms processes recursively all upwards connected cells until each cell of the DEM has been processed.\n
    Flow routing methods provided by this tool:\n
    (-) Deterministic 8 (aka D8, O'Callaghan & Mark 1984)\n
    (-) Rho 8 (Fairfield & Leymarie 1991)\n
    (-) Multiple Flow Direction (Freeman 1991, Quinn et al. 1991)\n
    (-) Deterministic Infinity (Tarboton 1997)\n
    Arguments
    ----------
    - ELEVATION [`input grid`] : Elevation
    - ACCU_TARGET [`input grid`] : Accumulation Target
    - SINKROUTE [`optional input grid`] : Sink Routes
    - WEIGHTS [`optional input grid`] : Weights
    - VAL_INPUT [`optional input grid`] : Input for Mean over Catchment
    - ACCU_MATERIAL [`optional input grid`] : Material for Accumulation
    - TARGETS [`optional input grid`] : Target Areas
    - FLOW [`output grid`] : Flow Accumulation
    - VAL_MEAN [`output grid`] : Mean over Catchment
    - ACCU_TOTAL [`output grid`] : Accumulated Material
    - ACCU_LEFT [`output grid`] : Accumulated Material (Left Side)
    - ACCU_RIGHT [`output grid`] : Accumulated Material (Right Side)
    - FLOW_LENGTH [`output grid`] : Flow Path Length. average distance that a cell's accumulated flow travelled
    - WEIGHT_LOSS [`output grid`] : Loss through Negative Weights. when using weights without support for negative flow: output of the absolute amount of negative flow that occurred
    - FLOW_UNIT [`choice`] : Flow Accumulation Unit. Available Choices: [0] number of cells [1] cell area Default: 1
    - METHOD [`choice`] : Method. Available Choices: [0] Deterministic 8 [1] Rho 8 [2] Deterministic Infinity [3] Multiple Flow Direction Default: 3
    - CONVERGENCE [`floating point number`] : Convergence. Minimum: 0.000000 Default: 1.100000 Convergence factor for Multiple Flow Direction Algorithm (Freeman 1991)
    - MFD_CONTOUR [`boolean`] : Contour Length. Default: 0 Include (pseudo) contour length as additional weighting factor in multiple flow direction routing, reduces flow to diagonal neighbour cells by a factor of 0.71 (s. Quinn et al. 1991 for details).
    - NO_NEGATIVES [`boolean`] : Prevent Negative Flow Accumulation. Default: 1 when using weights: do not transport negative flow, set it to zero instead; useful e.g. when accumulating measures of water balance.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_hydrology', '1', 'Flow Accumulation (Recursive)')
    if Tool.is_Okay():
        Tool.Set_Input ('ELEVATION', ELEVATION)
        Tool.Set_Input ('ACCU_TARGET', ACCU_TARGET)
        Tool.Set_Input ('SINKROUTE', SINKROUTE)
        Tool.Set_Input ('WEIGHTS', WEIGHTS)
        Tool.Set_Input ('VAL_INPUT', VAL_INPUT)
        Tool.Set_Input ('ACCU_MATERIAL', ACCU_MATERIAL)
        Tool.Set_Input ('TARGETS', TARGETS)
        Tool.Set_Output('FLOW', FLOW)
        Tool.Set_Output('VAL_MEAN', VAL_MEAN)
        Tool.Set_Output('ACCU_TOTAL', ACCU_TOTAL)
        Tool.Set_Output('ACCU_LEFT', ACCU_LEFT)
        Tool.Set_Output('ACCU_RIGHT', ACCU_RIGHT)
        Tool.Set_Output('FLOW_LENGTH', FLOW_LENGTH)
        Tool.Set_Output('WEIGHT_LOSS', WEIGHT_LOSS)
        Tool.Set_Option('FLOW_UNIT', FLOW_UNIT)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('CONVERGENCE', CONVERGENCE)
        Tool.Set_Option('MFD_CONTOUR', MFD_CONTOUR)
        Tool.Set_Option('NO_NEGATIVES', NO_NEGATIVES)
        return Tool.Execute(Verbose)
    return False

def run_tool_ta_hydrology_1(ELEVATION=None, ACCU_TARGET=None, SINKROUTE=None, WEIGHTS=None, VAL_INPUT=None, ACCU_MATERIAL=None, TARGETS=None, FLOW=None, VAL_MEAN=None, ACCU_TOTAL=None, ACCU_LEFT=None, ACCU_RIGHT=None, FLOW_LENGTH=None, WEIGHT_LOSS=None, FLOW_UNIT=None, METHOD=None, CONVERGENCE=None, MFD_CONTOUR=None, NO_NEGATIVES=None, Verbose=2):
    '''
    Flow Accumulation (Recursive)
    ----------
    [ta_hydrology.1]\n
    Recursive upward processing of cells for calculation of flow accumulation and related parameters. This set of algorithms processes recursively all upwards connected cells until each cell of the DEM has been processed.\n
    Flow routing methods provided by this tool:\n
    (-) Deterministic 8 (aka D8, O'Callaghan & Mark 1984)\n
    (-) Rho 8 (Fairfield & Leymarie 1991)\n
    (-) Multiple Flow Direction (Freeman 1991, Quinn et al. 1991)\n
    (-) Deterministic Infinity (Tarboton 1997)\n
    Arguments
    ----------
    - ELEVATION [`input grid`] : Elevation
    - ACCU_TARGET [`input grid`] : Accumulation Target
    - SINKROUTE [`optional input grid`] : Sink Routes
    - WEIGHTS [`optional input grid`] : Weights
    - VAL_INPUT [`optional input grid`] : Input for Mean over Catchment
    - ACCU_MATERIAL [`optional input grid`] : Material for Accumulation
    - TARGETS [`optional input grid`] : Target Areas
    - FLOW [`output grid`] : Flow Accumulation
    - VAL_MEAN [`output grid`] : Mean over Catchment
    - ACCU_TOTAL [`output grid`] : Accumulated Material
    - ACCU_LEFT [`output grid`] : Accumulated Material (Left Side)
    - ACCU_RIGHT [`output grid`] : Accumulated Material (Right Side)
    - FLOW_LENGTH [`output grid`] : Flow Path Length. average distance that a cell's accumulated flow travelled
    - WEIGHT_LOSS [`output grid`] : Loss through Negative Weights. when using weights without support for negative flow: output of the absolute amount of negative flow that occurred
    - FLOW_UNIT [`choice`] : Flow Accumulation Unit. Available Choices: [0] number of cells [1] cell area Default: 1
    - METHOD [`choice`] : Method. Available Choices: [0] Deterministic 8 [1] Rho 8 [2] Deterministic Infinity [3] Multiple Flow Direction Default: 3
    - CONVERGENCE [`floating point number`] : Convergence. Minimum: 0.000000 Default: 1.100000 Convergence factor for Multiple Flow Direction Algorithm (Freeman 1991)
    - MFD_CONTOUR [`boolean`] : Contour Length. Default: 0 Include (pseudo) contour length as additional weighting factor in multiple flow direction routing, reduces flow to diagonal neighbour cells by a factor of 0.71 (s. Quinn et al. 1991 for details).
    - NO_NEGATIVES [`boolean`] : Prevent Negative Flow Accumulation. Default: 1 when using weights: do not transport negative flow, set it to zero instead; useful e.g. when accumulating measures of water balance.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_hydrology', '1', 'Flow Accumulation (Recursive)')
    if Tool.is_Okay():
        Tool.Set_Input ('ELEVATION', ELEVATION)
        Tool.Set_Input ('ACCU_TARGET', ACCU_TARGET)
        Tool.Set_Input ('SINKROUTE', SINKROUTE)
        Tool.Set_Input ('WEIGHTS', WEIGHTS)
        Tool.Set_Input ('VAL_INPUT', VAL_INPUT)
        Tool.Set_Input ('ACCU_MATERIAL', ACCU_MATERIAL)
        Tool.Set_Input ('TARGETS', TARGETS)
        Tool.Set_Output('FLOW', FLOW)
        Tool.Set_Output('VAL_MEAN', VAL_MEAN)
        Tool.Set_Output('ACCU_TOTAL', ACCU_TOTAL)
        Tool.Set_Output('ACCU_LEFT', ACCU_LEFT)
        Tool.Set_Output('ACCU_RIGHT', ACCU_RIGHT)
        Tool.Set_Output('FLOW_LENGTH', FLOW_LENGTH)
        Tool.Set_Output('WEIGHT_LOSS', WEIGHT_LOSS)
        Tool.Set_Option('FLOW_UNIT', FLOW_UNIT)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('CONVERGENCE', CONVERGENCE)
        Tool.Set_Option('MFD_CONTOUR', MFD_CONTOUR)
        Tool.Set_Option('NO_NEGATIVES', NO_NEGATIVES)
        return Tool.Execute(Verbose)
    return False

def Flow_Accumulation_Flow_Tracing(ELEVATION=None, ACCU_TARGET=None, SINKROUTE=None, WEIGHTS=None, VAL_INPUT=None, ACCU_MATERIAL=None, FLOW=None, VAL_MEAN=None, ACCU_TOTAL=None, ACCU_LEFT=None, ACCU_RIGHT=None, STEP=None, FLOW_UNIT=None, METHOD=None, CORRECT=None, MINDQV=None, Verbose=2):
    '''
    Flow Accumulation (Flow Tracing)
    ----------
    [ta_hydrology.2]\n
    Flow tracing algorithms for calculations of flow accumulation and related parameters. These algorithms trace the flow of each cell in a DEM separately until it finally leaves the DEM or ends in a sink.\n
    The Rho 8 implementation (Fairfield & Leymarie 1991) adopts the original algorithm only for the flow routing and will give quite different results.\n
    Arguments
    ----------
    - ELEVATION [`input grid`] : Elevation
    - ACCU_TARGET [`input grid`] : Accumulation Target
    - SINKROUTE [`optional input grid`] : Sink Routes
    - WEIGHTS [`optional input grid`] : Weights
    - VAL_INPUT [`optional input grid`] : Input for Mean over Catchment
    - ACCU_MATERIAL [`optional input grid`] : Material for Accumulation
    - FLOW [`output grid`] : Flow Accumulation
    - VAL_MEAN [`output grid`] : Mean over Catchment
    - ACCU_TOTAL [`output grid`] : Accumulated Material
    - ACCU_LEFT [`output grid`] : Accumulated Material (Left Side)
    - ACCU_RIGHT [`output grid`] : Accumulated Material (Right Side)
    - STEP [`integer number`] : Step. Minimum: 1 Default: 1 For testing purposes. Only generate flow at cells with step distance (each step row/column).
    - FLOW_UNIT [`choice`] : Flow Accumulation Unit. Available Choices: [0] number of cells [1] cell area Default: 1
    - METHOD [`choice`] : Method. Available Choices: [0] Rho 8 [1] Kinematic Routing Algorithm [2] DEMON Default: 1
    - CORRECT [`boolean`] : Flow Correction. Default: 1
    - MINDQV [`floating point number`] : Minimum DQV. Minimum: 0.000000 Maximum: 1.000000 Default: 0.000000 DEMON - Minimum Drainage Quota Volume (DQV) for traced flow tubes

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_hydrology', '2', 'Flow Accumulation (Flow Tracing)')
    if Tool.is_Okay():
        Tool.Set_Input ('ELEVATION', ELEVATION)
        Tool.Set_Input ('ACCU_TARGET', ACCU_TARGET)
        Tool.Set_Input ('SINKROUTE', SINKROUTE)
        Tool.Set_Input ('WEIGHTS', WEIGHTS)
        Tool.Set_Input ('VAL_INPUT', VAL_INPUT)
        Tool.Set_Input ('ACCU_MATERIAL', ACCU_MATERIAL)
        Tool.Set_Output('FLOW', FLOW)
        Tool.Set_Output('VAL_MEAN', VAL_MEAN)
        Tool.Set_Output('ACCU_TOTAL', ACCU_TOTAL)
        Tool.Set_Output('ACCU_LEFT', ACCU_LEFT)
        Tool.Set_Output('ACCU_RIGHT', ACCU_RIGHT)
        Tool.Set_Option('STEP', STEP)
        Tool.Set_Option('FLOW_UNIT', FLOW_UNIT)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('CORRECT', CORRECT)
        Tool.Set_Option('MINDQV', MINDQV)
        return Tool.Execute(Verbose)
    return False

def run_tool_ta_hydrology_2(ELEVATION=None, ACCU_TARGET=None, SINKROUTE=None, WEIGHTS=None, VAL_INPUT=None, ACCU_MATERIAL=None, FLOW=None, VAL_MEAN=None, ACCU_TOTAL=None, ACCU_LEFT=None, ACCU_RIGHT=None, STEP=None, FLOW_UNIT=None, METHOD=None, CORRECT=None, MINDQV=None, Verbose=2):
    '''
    Flow Accumulation (Flow Tracing)
    ----------
    [ta_hydrology.2]\n
    Flow tracing algorithms for calculations of flow accumulation and related parameters. These algorithms trace the flow of each cell in a DEM separately until it finally leaves the DEM or ends in a sink.\n
    The Rho 8 implementation (Fairfield & Leymarie 1991) adopts the original algorithm only for the flow routing and will give quite different results.\n
    Arguments
    ----------
    - ELEVATION [`input grid`] : Elevation
    - ACCU_TARGET [`input grid`] : Accumulation Target
    - SINKROUTE [`optional input grid`] : Sink Routes
    - WEIGHTS [`optional input grid`] : Weights
    - VAL_INPUT [`optional input grid`] : Input for Mean over Catchment
    - ACCU_MATERIAL [`optional input grid`] : Material for Accumulation
    - FLOW [`output grid`] : Flow Accumulation
    - VAL_MEAN [`output grid`] : Mean over Catchment
    - ACCU_TOTAL [`output grid`] : Accumulated Material
    - ACCU_LEFT [`output grid`] : Accumulated Material (Left Side)
    - ACCU_RIGHT [`output grid`] : Accumulated Material (Right Side)
    - STEP [`integer number`] : Step. Minimum: 1 Default: 1 For testing purposes. Only generate flow at cells with step distance (each step row/column).
    - FLOW_UNIT [`choice`] : Flow Accumulation Unit. Available Choices: [0] number of cells [1] cell area Default: 1
    - METHOD [`choice`] : Method. Available Choices: [0] Rho 8 [1] Kinematic Routing Algorithm [2] DEMON Default: 1
    - CORRECT [`boolean`] : Flow Correction. Default: 1
    - MINDQV [`floating point number`] : Minimum DQV. Minimum: 0.000000 Maximum: 1.000000 Default: 0.000000 DEMON - Minimum Drainage Quota Volume (DQV) for traced flow tubes

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_hydrology', '2', 'Flow Accumulation (Flow Tracing)')
    if Tool.is_Okay():
        Tool.Set_Input ('ELEVATION', ELEVATION)
        Tool.Set_Input ('ACCU_TARGET', ACCU_TARGET)
        Tool.Set_Input ('SINKROUTE', SINKROUTE)
        Tool.Set_Input ('WEIGHTS', WEIGHTS)
        Tool.Set_Input ('VAL_INPUT', VAL_INPUT)
        Tool.Set_Input ('ACCU_MATERIAL', ACCU_MATERIAL)
        Tool.Set_Output('FLOW', FLOW)
        Tool.Set_Output('VAL_MEAN', VAL_MEAN)
        Tool.Set_Output('ACCU_TOTAL', ACCU_TOTAL)
        Tool.Set_Output('ACCU_LEFT', ACCU_LEFT)
        Tool.Set_Output('ACCU_RIGHT', ACCU_RIGHT)
        Tool.Set_Option('STEP', STEP)
        Tool.Set_Option('FLOW_UNIT', FLOW_UNIT)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('CORRECT', CORRECT)
        Tool.Set_Option('MINDQV', MINDQV)
        return Tool.Execute(Verbose)
    return False

def Upslope_Area(ELEVATION=None, TARGET=None, SINKROUTE=None, AREA=None, TARGET_PT_X=None, TARGET_PT_Y=None, METHOD=None, CONVERGE=None, MFD_CONTOUR=None, Verbose=2):
    '''
    Upslope Area
    ----------
    [ta_hydrology.4]\n
    This tool allows you to specify target cells, for which the upslope contributing area shall be identified. The result will give for each cell the percentage of its flow that reaches the target cell(s).\n
    _______\n
    This version uses all valid cells (not 'no data' values) of a given target grid to determine the contributing area. In case no target grid is provided as input, the specified x/y coordinates are used as target point.\n
    Arguments
    ----------
    - ELEVATION [`input grid`] : Elevation
    - TARGET [`optional input grid`] : Target Area
    - SINKROUTE [`optional input grid`] : Sink Routes
    - AREA [`output grid`] : Upslope Area
    - TARGET_PT_X [`floating point number`] : Target X coordinate. Default: 0.000000 The x-coordinate of the target point in world coordinates [map units]
    - TARGET_PT_Y [`floating point number`] : Target Y coordinate. Default: 0.000000 The y-coordinate of the target point in world coordinates [map units]
    - METHOD [`choice`] : Method. Available Choices: [0] Deterministic 8 [1] Deterministic Infinity [2] Multiple Flow Direction [3] Multiple Triangular Flow Direction [4] Multiple Maximum Downslope Gradient Based Flow Direction Default: 2
    - CONVERGE [`floating point number`] : Convergence. Minimum: 0.001000 Default: 1.100000 Convergence factor for Multiple Flow Direction algorithm
    - MFD_CONTOUR [`boolean`] : Contour Length. Default: 0 Include (pseudo) contour length as additional weighting factor in multiple flow direction routing, reduces flow to diagonal neighbour cells by a factor of 0.71 (s. Quinn et al. 1991 for details).

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_hydrology', '4', 'Upslope Area')
    if Tool.is_Okay():
        Tool.Set_Input ('ELEVATION', ELEVATION)
        Tool.Set_Input ('TARGET', TARGET)
        Tool.Set_Input ('SINKROUTE', SINKROUTE)
        Tool.Set_Output('AREA', AREA)
        Tool.Set_Option('TARGET_PT_X', TARGET_PT_X)
        Tool.Set_Option('TARGET_PT_Y', TARGET_PT_Y)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('CONVERGE', CONVERGE)
        Tool.Set_Option('MFD_CONTOUR', MFD_CONTOUR)
        return Tool.Execute(Verbose)
    return False

def run_tool_ta_hydrology_4(ELEVATION=None, TARGET=None, SINKROUTE=None, AREA=None, TARGET_PT_X=None, TARGET_PT_Y=None, METHOD=None, CONVERGE=None, MFD_CONTOUR=None, Verbose=2):
    '''
    Upslope Area
    ----------
    [ta_hydrology.4]\n
    This tool allows you to specify target cells, for which the upslope contributing area shall be identified. The result will give for each cell the percentage of its flow that reaches the target cell(s).\n
    _______\n
    This version uses all valid cells (not 'no data' values) of a given target grid to determine the contributing area. In case no target grid is provided as input, the specified x/y coordinates are used as target point.\n
    Arguments
    ----------
    - ELEVATION [`input grid`] : Elevation
    - TARGET [`optional input grid`] : Target Area
    - SINKROUTE [`optional input grid`] : Sink Routes
    - AREA [`output grid`] : Upslope Area
    - TARGET_PT_X [`floating point number`] : Target X coordinate. Default: 0.000000 The x-coordinate of the target point in world coordinates [map units]
    - TARGET_PT_Y [`floating point number`] : Target Y coordinate. Default: 0.000000 The y-coordinate of the target point in world coordinates [map units]
    - METHOD [`choice`] : Method. Available Choices: [0] Deterministic 8 [1] Deterministic Infinity [2] Multiple Flow Direction [3] Multiple Triangular Flow Direction [4] Multiple Maximum Downslope Gradient Based Flow Direction Default: 2
    - CONVERGE [`floating point number`] : Convergence. Minimum: 0.001000 Default: 1.100000 Convergence factor for Multiple Flow Direction algorithm
    - MFD_CONTOUR [`boolean`] : Contour Length. Default: 0 Include (pseudo) contour length as additional weighting factor in multiple flow direction routing, reduces flow to diagonal neighbour cells by a factor of 0.71 (s. Quinn et al. 1991 for details).

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_hydrology', '4', 'Upslope Area')
    if Tool.is_Okay():
        Tool.Set_Input ('ELEVATION', ELEVATION)
        Tool.Set_Input ('TARGET', TARGET)
        Tool.Set_Input ('SINKROUTE', SINKROUTE)
        Tool.Set_Output('AREA', AREA)
        Tool.Set_Option('TARGET_PT_X', TARGET_PT_X)
        Tool.Set_Option('TARGET_PT_Y', TARGET_PT_Y)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('CONVERGE', CONVERGE)
        Tool.Set_Option('MFD_CONTOUR', MFD_CONTOUR)
        return Tool.Execute(Verbose)
    return False

def Flow_Path_Length(ELEVATION=None, SEED=None, LENGTH=None, SEEDS_ONLY=None, METHOD=None, CONVERGENCE=None, Verbose=2):
    '''
    Flow Path Length
    ----------
    [ta_hydrology.6]\n
    This tool calculates the average flow path length starting from the seeds, that are given by the optional 'Seeds' grid and optionally from cells without upslope contributing areas (i.e. summits, ridges). Seeds will be all grid cells, that are not 'no data' values. If seeds are not given, only summits and ridges as given by the flow routing will be taken into account. Available flow routing methods are based on the 'Deterministic 8 (D8)' (Callaghan and Mark 1984) and the 'Multiple Flow Direction (FD8)' (Freeman 1991, Quinn et al. 1991) algorithms.\n
    Arguments
    ----------
    - ELEVATION [`input grid`] : Elevation
    - SEED [`optional input grid`] : Seeds
    - LENGTH [`output grid`] : Flow Path Length
    - SEEDS_ONLY [`boolean`] : Seeds Only. Default: 0 Use only seed cells as starting points.
    - METHOD [`choice`] : Flow Routing Algorithm. Available Choices: [0] Deterministic 8 (D8) [1] Multiple Flow Direction (FD8) Default: 1
    - CONVERGENCE [`floating point number`] : Convergence (FD8). Minimum: 0.001000 Default: 1.100000 Convergence factor for the 'Multiple Flow Direction' algorithm (after Freeman 1991)

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_hydrology', '6', 'Flow Path Length')
    if Tool.is_Okay():
        Tool.Set_Input ('ELEVATION', ELEVATION)
        Tool.Set_Input ('SEED', SEED)
        Tool.Set_Output('LENGTH', LENGTH)
        Tool.Set_Option('SEEDS_ONLY', SEEDS_ONLY)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('CONVERGENCE', CONVERGENCE)
        return Tool.Execute(Verbose)
    return False

def run_tool_ta_hydrology_6(ELEVATION=None, SEED=None, LENGTH=None, SEEDS_ONLY=None, METHOD=None, CONVERGENCE=None, Verbose=2):
    '''
    Flow Path Length
    ----------
    [ta_hydrology.6]\n
    This tool calculates the average flow path length starting from the seeds, that are given by the optional 'Seeds' grid and optionally from cells without upslope contributing areas (i.e. summits, ridges). Seeds will be all grid cells, that are not 'no data' values. If seeds are not given, only summits and ridges as given by the flow routing will be taken into account. Available flow routing methods are based on the 'Deterministic 8 (D8)' (Callaghan and Mark 1984) and the 'Multiple Flow Direction (FD8)' (Freeman 1991, Quinn et al. 1991) algorithms.\n
    Arguments
    ----------
    - ELEVATION [`input grid`] : Elevation
    - SEED [`optional input grid`] : Seeds
    - LENGTH [`output grid`] : Flow Path Length
    - SEEDS_ONLY [`boolean`] : Seeds Only. Default: 0 Use only seed cells as starting points.
    - METHOD [`choice`] : Flow Routing Algorithm. Available Choices: [0] Deterministic 8 (D8) [1] Multiple Flow Direction (FD8) Default: 1
    - CONVERGENCE [`floating point number`] : Convergence (FD8). Minimum: 0.001000 Default: 1.100000 Convergence factor for the 'Multiple Flow Direction' algorithm (after Freeman 1991)

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_hydrology', '6', 'Flow Path Length')
    if Tool.is_Okay():
        Tool.Set_Input ('ELEVATION', ELEVATION)
        Tool.Set_Input ('SEED', SEED)
        Tool.Set_Output('LENGTH', LENGTH)
        Tool.Set_Option('SEEDS_ONLY', SEEDS_ONLY)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('CONVERGENCE', CONVERGENCE)
        return Tool.Execute(Verbose)
    return False

def Slope_Length(DEM=None, LENGTH=None, Verbose=2):
    '''
    Slope Length
    ----------
    [ta_hydrology.7]\n
    The tool allows one to calculate a special version of slope (or flowpath) length. In a first processing step, the slope of each cell is calculated with the method of Zevenbergen & Thorne (1986). The slope length values are then accumulated downslope using the Deterministic 8 (D8) single flow direction algorithm and a threshold criterion: the slope length is only accumulated downslope if the slope of the receiving cell is steeper than half of the slope of the contributing cell (i.e. the accumulation stops if the slope profile gets abruptly flatter).\n
    If several cells are contributing to a cell, the maximum of the accumulated slope length values found in these cells is passed to the receiving cell and then routed further downslope.\n
    Arguments
    ----------
    - DEM [`input grid`] : Elevation. The digital elevation model.
    - LENGTH [`output grid`] : Slope Length. The maximum slope (flowpath) length.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_hydrology', '7', 'Slope Length')
    if Tool.is_Okay():
        Tool.Set_Input ('DEM', DEM)
        Tool.Set_Output('LENGTH', LENGTH)
        return Tool.Execute(Verbose)
    return False

def run_tool_ta_hydrology_7(DEM=None, LENGTH=None, Verbose=2):
    '''
    Slope Length
    ----------
    [ta_hydrology.7]\n
    The tool allows one to calculate a special version of slope (or flowpath) length. In a first processing step, the slope of each cell is calculated with the method of Zevenbergen & Thorne (1986). The slope length values are then accumulated downslope using the Deterministic 8 (D8) single flow direction algorithm and a threshold criterion: the slope length is only accumulated downslope if the slope of the receiving cell is steeper than half of the slope of the contributing cell (i.e. the accumulation stops if the slope profile gets abruptly flatter).\n
    If several cells are contributing to a cell, the maximum of the accumulated slope length values found in these cells is passed to the receiving cell and then routed further downslope.\n
    Arguments
    ----------
    - DEM [`input grid`] : Elevation. The digital elevation model.
    - LENGTH [`output grid`] : Slope Length. The maximum slope (flowpath) length.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_hydrology', '7', 'Slope Length')
    if Tool.is_Okay():
        Tool.Set_Input ('DEM', DEM)
        Tool.Set_Output('LENGTH', LENGTH)
        return Tool.Execute(Verbose)
    return False

def Cell_Balance(DEM=None, WEIGHTS=None, BALANCE=None, WEIGHTS_DEFAULT=None, METHOD=None, Verbose=2):
    '''
    Cell Balance
    ----------
    [ta_hydrology.10]\n
    Cell Balance\n
    Arguments
    ----------
    - DEM [`input grid`] : Elevation
    - WEIGHTS [`optional input grid`] : Weights
    - BALANCE [`output grid`] : Cell Balance
    - WEIGHTS_DEFAULT [`floating point number`] : Default. Minimum: 0.000000 Default: 1.000000 default value if no grid has been selected
    - METHOD [`choice`] : Method. Available Choices: [0] Deterministic 8 [1] Multiple Flow Direction Default: 1

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_hydrology', '10', 'Cell Balance')
    if Tool.is_Okay():
        Tool.Set_Input ('DEM', DEM)
        Tool.Set_Input ('WEIGHTS', WEIGHTS)
        Tool.Set_Output('BALANCE', BALANCE)
        Tool.Set_Option('WEIGHTS_DEFAULT', WEIGHTS_DEFAULT)
        Tool.Set_Option('METHOD', METHOD)
        return Tool.Execute(Verbose)
    return False

def run_tool_ta_hydrology_10(DEM=None, WEIGHTS=None, BALANCE=None, WEIGHTS_DEFAULT=None, METHOD=None, Verbose=2):
    '''
    Cell Balance
    ----------
    [ta_hydrology.10]\n
    Cell Balance\n
    Arguments
    ----------
    - DEM [`input grid`] : Elevation
    - WEIGHTS [`optional input grid`] : Weights
    - BALANCE [`output grid`] : Cell Balance
    - WEIGHTS_DEFAULT [`floating point number`] : Default. Minimum: 0.000000 Default: 1.000000 default value if no grid has been selected
    - METHOD [`choice`] : Method. Available Choices: [0] Deterministic 8 [1] Multiple Flow Direction Default: 1

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_hydrology', '10', 'Cell Balance')
    if Tool.is_Okay():
        Tool.Set_Input ('DEM', DEM)
        Tool.Set_Input ('WEIGHTS', WEIGHTS)
        Tool.Set_Output('BALANCE', BALANCE)
        Tool.Set_Option('WEIGHTS_DEFAULT', WEIGHTS_DEFAULT)
        Tool.Set_Option('METHOD', METHOD)
        return Tool.Execute(Verbose)
    return False

def Edge_Contamination(ELEVATION=None, CONTAMINATION=None, METHOD=None, Verbose=2):
    '''
    Edge Contamination
    ----------
    [ta_hydrology.13]\n
    This tool uses flow directions to estimate possible contamination effects moving from outside of the grid passing the edge into its interior. This means that derived contributing area values might be underestimated for the marked cells. Cells not contamined will be marked as no data.\n
    Arguments
    ----------
    - ELEVATION [`input grid`] : Elevation
    - CONTAMINATION [`output grid`] : Edge Contamination
    - METHOD [`choice`] : Method. Available Choices: [0] single flow direction [1] multiple flow direction Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_hydrology', '13', 'Edge Contamination')
    if Tool.is_Okay():
        Tool.Set_Input ('ELEVATION', ELEVATION)
        Tool.Set_Output('CONTAMINATION', CONTAMINATION)
        Tool.Set_Option('METHOD', METHOD)
        return Tool.Execute(Verbose)
    return False

def run_tool_ta_hydrology_13(ELEVATION=None, CONTAMINATION=None, METHOD=None, Verbose=2):
    '''
    Edge Contamination
    ----------
    [ta_hydrology.13]\n
    This tool uses flow directions to estimate possible contamination effects moving from outside of the grid passing the edge into its interior. This means that derived contributing area values might be underestimated for the marked cells. Cells not contamined will be marked as no data.\n
    Arguments
    ----------
    - ELEVATION [`input grid`] : Elevation
    - CONTAMINATION [`output grid`] : Edge Contamination
    - METHOD [`choice`] : Method. Available Choices: [0] single flow direction [1] multiple flow direction Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_hydrology', '13', 'Edge Contamination')
    if Tool.is_Okay():
        Tool.Set_Input ('ELEVATION', ELEVATION)
        Tool.Set_Output('CONTAMINATION', CONTAMINATION)
        Tool.Set_Option('METHOD', METHOD)
        return Tool.Execute(Verbose)
    return False

def SAGA_Wetness_Index(DEM=None, WEIGHT=None, AREA=None, SLOPE=None, AREA_MOD=None, TWI=None, SUCTION=None, AREA_TYPE=None, SLOPE_TYPE=None, SLOPE_MIN=None, SLOPE_OFF=None, SLOPE_WEIGHT=None, Verbose=2):
    '''
    SAGA Wetness Index
    ----------
    [ta_hydrology.15]\n
    The 'SAGA Wetness Index' is, as the name says, similar to the 'Topographic Wetness Index' (TWI), but it is based on a modified catchment area calculation ('Modified Catchment Area'), which does not think of the flow as very thin film. As result it predicts for cells situated in valley floors with a small vertical distance to a channel a more realistic, higher potential soil moisture compared to the standard TWI calculation.\n
    Arguments
    ----------
    - DEM [`input grid`] : Elevation
    - WEIGHT [`optional input grid`] : Weights
    - AREA [`output grid`] : Catchment Area
    - SLOPE [`output grid`] : Catchment Slope
    - AREA_MOD [`output grid`] : Modified Catchment Area
    - TWI [`output grid`] : Topographic Wetness Index
    - SUCTION [`floating point number`] : Suction. Minimum: 0.000000 Default: 10.000000 the lower this value is the stronger is the suction effect
    - AREA_TYPE [`choice`] : Type of Area. Available Choices: [0] total catchment area [1] square root of catchment area [2] specific catchment area Default: 2
    - SLOPE_TYPE [`choice`] : Type of Slope. Available Choices: [0] local slope [1] catchment slope Default: 1
    - SLOPE_MIN [`floating point number`] : Minimum Slope. Minimum: 0.000000 Default: 0.000000
    - SLOPE_OFF [`floating point number`] : Offset Slope. Minimum: 0.000000 Default: 0.100000
    - SLOPE_WEIGHT [`floating point number`] : Slope Weighting. Minimum: 0.000000 Default: 1.000000 weighting factor for slope in index calculation

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_hydrology', '15', 'SAGA Wetness Index')
    if Tool.is_Okay():
        Tool.Set_Input ('DEM', DEM)
        Tool.Set_Input ('WEIGHT', WEIGHT)
        Tool.Set_Output('AREA', AREA)
        Tool.Set_Output('SLOPE', SLOPE)
        Tool.Set_Output('AREA_MOD', AREA_MOD)
        Tool.Set_Output('TWI', TWI)
        Tool.Set_Option('SUCTION', SUCTION)
        Tool.Set_Option('AREA_TYPE', AREA_TYPE)
        Tool.Set_Option('SLOPE_TYPE', SLOPE_TYPE)
        Tool.Set_Option('SLOPE_MIN', SLOPE_MIN)
        Tool.Set_Option('SLOPE_OFF', SLOPE_OFF)
        Tool.Set_Option('SLOPE_WEIGHT', SLOPE_WEIGHT)
        return Tool.Execute(Verbose)
    return False

def run_tool_ta_hydrology_15(DEM=None, WEIGHT=None, AREA=None, SLOPE=None, AREA_MOD=None, TWI=None, SUCTION=None, AREA_TYPE=None, SLOPE_TYPE=None, SLOPE_MIN=None, SLOPE_OFF=None, SLOPE_WEIGHT=None, Verbose=2):
    '''
    SAGA Wetness Index
    ----------
    [ta_hydrology.15]\n
    The 'SAGA Wetness Index' is, as the name says, similar to the 'Topographic Wetness Index' (TWI), but it is based on a modified catchment area calculation ('Modified Catchment Area'), which does not think of the flow as very thin film. As result it predicts for cells situated in valley floors with a small vertical distance to a channel a more realistic, higher potential soil moisture compared to the standard TWI calculation.\n
    Arguments
    ----------
    - DEM [`input grid`] : Elevation
    - WEIGHT [`optional input grid`] : Weights
    - AREA [`output grid`] : Catchment Area
    - SLOPE [`output grid`] : Catchment Slope
    - AREA_MOD [`output grid`] : Modified Catchment Area
    - TWI [`output grid`] : Topographic Wetness Index
    - SUCTION [`floating point number`] : Suction. Minimum: 0.000000 Default: 10.000000 the lower this value is the stronger is the suction effect
    - AREA_TYPE [`choice`] : Type of Area. Available Choices: [0] total catchment area [1] square root of catchment area [2] specific catchment area Default: 2
    - SLOPE_TYPE [`choice`] : Type of Slope. Available Choices: [0] local slope [1] catchment slope Default: 1
    - SLOPE_MIN [`floating point number`] : Minimum Slope. Minimum: 0.000000 Default: 0.000000
    - SLOPE_OFF [`floating point number`] : Offset Slope. Minimum: 0.000000 Default: 0.100000
    - SLOPE_WEIGHT [`floating point number`] : Slope Weighting. Minimum: 0.000000 Default: 1.000000 weighting factor for slope in index calculation

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_hydrology', '15', 'SAGA Wetness Index')
    if Tool.is_Okay():
        Tool.Set_Input ('DEM', DEM)
        Tool.Set_Input ('WEIGHT', WEIGHT)
        Tool.Set_Output('AREA', AREA)
        Tool.Set_Output('SLOPE', SLOPE)
        Tool.Set_Output('AREA_MOD', AREA_MOD)
        Tool.Set_Output('TWI', TWI)
        Tool.Set_Option('SUCTION', SUCTION)
        Tool.Set_Option('AREA_TYPE', AREA_TYPE)
        Tool.Set_Option('SLOPE_TYPE', SLOPE_TYPE)
        Tool.Set_Option('SLOPE_MIN', SLOPE_MIN)
        Tool.Set_Option('SLOPE_OFF', SLOPE_OFF)
        Tool.Set_Option('SLOPE_WEIGHT', SLOPE_WEIGHT)
        return Tool.Execute(Verbose)
    return False

def Flow_Accumulation_MassFlux_Method(DEM=None, AREA=None, QUARTERS_GRIDS=None, FLOW_LINES=None, METHOD=None, QUARTERS=None, Verbose=2):
    '''
    Flow Accumulation (Mass-Flux Method)
    ----------
    [ta_hydrology.18]\n
    The Mass-Flux Method (MFM) for the DEM based calculation of flow accumulation as proposed by Gruber and Peckham (2008).\n
    !!!UNDER DEVELOPMENT!!! To be done: solving the streamline resolution problem\n
    Arguments
    ----------
    - DEM [`input grid`] : Elevation
    - AREA [`output grid`] : Flow Accumulation
    - QUARTERS_GRIDS [`output grid list`] : Grids
    - FLOW_LINES [`output shapes`] : Flow Lines
    - METHOD [`choice`] : Flow Split Method. Available Choices: [0] flow width (original) [1] cell area Default: 0
    - QUARTERS [`boolean`] : Create Quarter Cell Information. Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_hydrology', '18', 'Flow Accumulation (Mass-Flux Method)')
    if Tool.is_Okay():
        Tool.Set_Input ('DEM', DEM)
        Tool.Set_Output('AREA', AREA)
        Tool.Set_Output('QUARTERS_GRIDS', QUARTERS_GRIDS)
        Tool.Set_Output('FLOW_LINES', FLOW_LINES)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('QUARTERS', QUARTERS)
        return Tool.Execute(Verbose)
    return False

def run_tool_ta_hydrology_18(DEM=None, AREA=None, QUARTERS_GRIDS=None, FLOW_LINES=None, METHOD=None, QUARTERS=None, Verbose=2):
    '''
    Flow Accumulation (Mass-Flux Method)
    ----------
    [ta_hydrology.18]\n
    The Mass-Flux Method (MFM) for the DEM based calculation of flow accumulation as proposed by Gruber and Peckham (2008).\n
    !!!UNDER DEVELOPMENT!!! To be done: solving the streamline resolution problem\n
    Arguments
    ----------
    - DEM [`input grid`] : Elevation
    - AREA [`output grid`] : Flow Accumulation
    - QUARTERS_GRIDS [`output grid list`] : Grids
    - FLOW_LINES [`output shapes`] : Flow Lines
    - METHOD [`choice`] : Flow Split Method. Available Choices: [0] flow width (original) [1] cell area Default: 0
    - QUARTERS [`boolean`] : Create Quarter Cell Information. Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_hydrology', '18', 'Flow Accumulation (Mass-Flux Method)')
    if Tool.is_Okay():
        Tool.Set_Input ('DEM', DEM)
        Tool.Set_Output('AREA', AREA)
        Tool.Set_Output('QUARTERS_GRIDS', QUARTERS_GRIDS)
        Tool.Set_Output('FLOW_LINES', FLOW_LINES)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('QUARTERS', QUARTERS)
        return Tool.Execute(Verbose)
    return False

def Flow_Width_and_Specific_Catchment_Area(DEM=None, TCA=None, WIDTH=None, SCA=None, COORD_UNIT=None, METHOD=None, Verbose=2):
    '''
    Flow Width and Specific Catchment Area
    ----------
    [ta_hydrology.19]\n
    Flow width and specific catchment area (SCA) calculation. SCA calculation needs total catchment area (TCA) as input, which can be calculated with one of the flow accumulation tools.\n
    Arguments
    ----------
    - DEM [`input grid`] : Elevation
    - TCA [`optional input grid`] : Total Catchment Area (TCA)
    - WIDTH [`output grid`] : Flow Width
    - SCA [`output grid`] : Specific Catchment Area (SCA)
    - COORD_UNIT [`choice`] : Coordinate Unit. Available Choices: [0] meter [1] feet Default: 0
    - METHOD [`choice`] : Method. Available Choices: [0] Deterministic 8 [1] Multiple Flow Direction (Quinn et al. 1991) [2] Aspect Default: 2

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_hydrology', '19', 'Flow Width and Specific Catchment Area')
    if Tool.is_Okay():
        Tool.Set_Input ('DEM', DEM)
        Tool.Set_Input ('TCA', TCA)
        Tool.Set_Output('WIDTH', WIDTH)
        Tool.Set_Output('SCA', SCA)
        Tool.Set_Option('COORD_UNIT', COORD_UNIT)
        Tool.Set_Option('METHOD', METHOD)
        return Tool.Execute(Verbose)
    return False

def run_tool_ta_hydrology_19(DEM=None, TCA=None, WIDTH=None, SCA=None, COORD_UNIT=None, METHOD=None, Verbose=2):
    '''
    Flow Width and Specific Catchment Area
    ----------
    [ta_hydrology.19]\n
    Flow width and specific catchment area (SCA) calculation. SCA calculation needs total catchment area (TCA) as input, which can be calculated with one of the flow accumulation tools.\n
    Arguments
    ----------
    - DEM [`input grid`] : Elevation
    - TCA [`optional input grid`] : Total Catchment Area (TCA)
    - WIDTH [`output grid`] : Flow Width
    - SCA [`output grid`] : Specific Catchment Area (SCA)
    - COORD_UNIT [`choice`] : Coordinate Unit. Available Choices: [0] meter [1] feet Default: 0
    - METHOD [`choice`] : Method. Available Choices: [0] Deterministic 8 [1] Multiple Flow Direction (Quinn et al. 1991) [2] Aspect Default: 2

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_hydrology', '19', 'Flow Width and Specific Catchment Area')
    if Tool.is_Okay():
        Tool.Set_Input ('DEM', DEM)
        Tool.Set_Input ('TCA', TCA)
        Tool.Set_Output('WIDTH', WIDTH)
        Tool.Set_Output('SCA', SCA)
        Tool.Set_Option('COORD_UNIT', COORD_UNIT)
        Tool.Set_Option('METHOD', METHOD)
        return Tool.Execute(Verbose)
    return False

def Topographic_Wetness_Index(SLOPE=None, AREA=None, TRANS=None, TWI=None, CONV=None, METHOD=None, Verbose=2):
    '''
    Topographic Wetness Index
    ----------
    [ta_hydrology.20]\n
    Calculates the Topographic Wetness Index (TWI) using the supplied rasters for slope (Beta), specific catchment area (SCA), and optional transmissivity (T) when the soil profile is saturated:\n
    TWI = ln(SCA / tan(Beta))\n
    or with transmissivity\n
    TWI = ln(SCA / (T * tan(Beta)))\n
    Arguments
    ----------
    - SLOPE [`input grid`] : Slope
    - AREA [`input grid`] : Catchment Area
    - TRANS [`optional input grid`] : Transmissivity
    - TWI [`output grid`] : Topographic Wetness Index
    - CONV [`choice`] : Area Conversion. Available Choices: [0] no conversion (areas already given as specific catchment area) [1] 1 / cell size (pseudo specific catchment area) Default: 0
    - METHOD [`choice`] : Method (TWI). Available Choices: [0] Standard [1] TOPMODEL Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_hydrology', '20', 'Topographic Wetness Index')
    if Tool.is_Okay():
        Tool.Set_Input ('SLOPE', SLOPE)
        Tool.Set_Input ('AREA', AREA)
        Tool.Set_Input ('TRANS', TRANS)
        Tool.Set_Output('TWI', TWI)
        Tool.Set_Option('CONV', CONV)
        Tool.Set_Option('METHOD', METHOD)
        return Tool.Execute(Verbose)
    return False

def run_tool_ta_hydrology_20(SLOPE=None, AREA=None, TRANS=None, TWI=None, CONV=None, METHOD=None, Verbose=2):
    '''
    Topographic Wetness Index
    ----------
    [ta_hydrology.20]\n
    Calculates the Topographic Wetness Index (TWI) using the supplied rasters for slope (Beta), specific catchment area (SCA), and optional transmissivity (T) when the soil profile is saturated:\n
    TWI = ln(SCA / tan(Beta))\n
    or with transmissivity\n
    TWI = ln(SCA / (T * tan(Beta)))\n
    Arguments
    ----------
    - SLOPE [`input grid`] : Slope
    - AREA [`input grid`] : Catchment Area
    - TRANS [`optional input grid`] : Transmissivity
    - TWI [`output grid`] : Topographic Wetness Index
    - CONV [`choice`] : Area Conversion. Available Choices: [0] no conversion (areas already given as specific catchment area) [1] 1 / cell size (pseudo specific catchment area) Default: 0
    - METHOD [`choice`] : Method (TWI). Available Choices: [0] Standard [1] TOPMODEL Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_hydrology', '20', 'Topographic Wetness Index')
    if Tool.is_Okay():
        Tool.Set_Input ('SLOPE', SLOPE)
        Tool.Set_Input ('AREA', AREA)
        Tool.Set_Input ('TRANS', TRANS)
        Tool.Set_Output('TWI', TWI)
        Tool.Set_Option('CONV', CONV)
        Tool.Set_Option('METHOD', METHOD)
        return Tool.Execute(Verbose)
    return False

def Stream_Power_Index(SLOPE=None, AREA=None, SPI=None, CONV=None, Verbose=2):
    '''
    Stream Power Index
    ----------
    [ta_hydrology.21]\n
    Calculation of stream power index based on slope and specific catchment area (SCA).\n
    SPI = SCA * tan(Slope)\n
    Arguments
    ----------
    - SLOPE [`input grid`] : Slope
    - AREA [`input grid`] : Catchment Area
    - SPI [`output grid`] : Stream Power Index
    - CONV [`choice`] : Area Conversion. Available Choices: [0] no conversion (areas already given as specific catchment area) [1] 1 / cell size (pseudo specific catchment area) Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_hydrology', '21', 'Stream Power Index')
    if Tool.is_Okay():
        Tool.Set_Input ('SLOPE', SLOPE)
        Tool.Set_Input ('AREA', AREA)
        Tool.Set_Output('SPI', SPI)
        Tool.Set_Option('CONV', CONV)
        return Tool.Execute(Verbose)
    return False

def run_tool_ta_hydrology_21(SLOPE=None, AREA=None, SPI=None, CONV=None, Verbose=2):
    '''
    Stream Power Index
    ----------
    [ta_hydrology.21]\n
    Calculation of stream power index based on slope and specific catchment area (SCA).\n
    SPI = SCA * tan(Slope)\n
    Arguments
    ----------
    - SLOPE [`input grid`] : Slope
    - AREA [`input grid`] : Catchment Area
    - SPI [`output grid`] : Stream Power Index
    - CONV [`choice`] : Area Conversion. Available Choices: [0] no conversion (areas already given as specific catchment area) [1] 1 / cell size (pseudo specific catchment area) Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_hydrology', '21', 'Stream Power Index')
    if Tool.is_Okay():
        Tool.Set_Input ('SLOPE', SLOPE)
        Tool.Set_Input ('AREA', AREA)
        Tool.Set_Output('SPI', SPI)
        Tool.Set_Option('CONV', CONV)
        return Tool.Execute(Verbose)
    return False

def LS_Factor(SLOPE=None, AREA=None, LS=None, CONV=None, FEET=None, METHOD=None, EROSIVITY=None, STABILITY=None, Verbose=2):
    '''
    LS Factor
    ----------
    [ta_hydrology.22]\n
    Calculation of slope length (LS) factor as used by the Universal Soil Loss Equation (USLE), based on slope and specific catchment area (SCA, as substitute for slope length).\n
    Arguments
    ----------
    - SLOPE [`input grid`] : Slope
    - AREA [`input grid`] : Catchment Area
    - LS [`output grid`] : LS Factor
    - CONV [`choice`] : Area to Length Conversion. Available Choices: [0] no conversion (areas already given as specific catchment area) [1] 1 / cell size (specific catchment area) [2] square root (catchment length) Default: 0 Derivation of slope lengths from catchment areas. These are rough approximations! Applies not to Desmet & Govers' method.
    - FEET [`boolean`] : Feet Adjustment. Default: 0 Needed if area and lengths come from coordinates measured in feet.
    - METHOD [`choice`] : Method (LS). Available Choices: [0] Moore et al. 1991 [1] Desmet & Govers 1996 [2] Boehner & Selige 2006 Default: 0
    - EROSIVITY [`floating point number`] : Rill/Interrill Erosivity. Minimum: 0.000000 Default: 1.000000
    - STABILITY [`choice`] : Stability. Available Choices: [0] stable [1] instable (thawing) Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_hydrology', '22', 'LS Factor')
    if Tool.is_Okay():
        Tool.Set_Input ('SLOPE', SLOPE)
        Tool.Set_Input ('AREA', AREA)
        Tool.Set_Output('LS', LS)
        Tool.Set_Option('CONV', CONV)
        Tool.Set_Option('FEET', FEET)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('EROSIVITY', EROSIVITY)
        Tool.Set_Option('STABILITY', STABILITY)
        return Tool.Execute(Verbose)
    return False

def run_tool_ta_hydrology_22(SLOPE=None, AREA=None, LS=None, CONV=None, FEET=None, METHOD=None, EROSIVITY=None, STABILITY=None, Verbose=2):
    '''
    LS Factor
    ----------
    [ta_hydrology.22]\n
    Calculation of slope length (LS) factor as used by the Universal Soil Loss Equation (USLE), based on slope and specific catchment area (SCA, as substitute for slope length).\n
    Arguments
    ----------
    - SLOPE [`input grid`] : Slope
    - AREA [`input grid`] : Catchment Area
    - LS [`output grid`] : LS Factor
    - CONV [`choice`] : Area to Length Conversion. Available Choices: [0] no conversion (areas already given as specific catchment area) [1] 1 / cell size (specific catchment area) [2] square root (catchment length) Default: 0 Derivation of slope lengths from catchment areas. These are rough approximations! Applies not to Desmet & Govers' method.
    - FEET [`boolean`] : Feet Adjustment. Default: 0 Needed if area and lengths come from coordinates measured in feet.
    - METHOD [`choice`] : Method (LS). Available Choices: [0] Moore et al. 1991 [1] Desmet & Govers 1996 [2] Boehner & Selige 2006 Default: 0
    - EROSIVITY [`floating point number`] : Rill/Interrill Erosivity. Minimum: 0.000000 Default: 1.000000
    - STABILITY [`choice`] : Stability. Available Choices: [0] stable [1] instable (thawing) Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_hydrology', '22', 'LS Factor')
    if Tool.is_Okay():
        Tool.Set_Input ('SLOPE', SLOPE)
        Tool.Set_Input ('AREA', AREA)
        Tool.Set_Output('LS', LS)
        Tool.Set_Option('CONV', CONV)
        Tool.Set_Option('FEET', FEET)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('EROSIVITY', EROSIVITY)
        Tool.Set_Option('STABILITY', STABILITY)
        return Tool.Execute(Verbose)
    return False

def Melton_Ruggedness_Number(DEM=None, AREA=None, ZMAX=None, MRN=None, Verbose=2):
    '''
    Melton Ruggedness Number
    ----------
    [ta_hydrology.23]\n
    Melton ruggedness number (MNR) is a simple flow accumulation related index, calculated as difference between maximum and minimum elevation in catchment area divided by square root of catchment area size. The calculation is performed for each grid cell, therefore minimum elevation is same as elevation at cell's position. Due to the discrete character of a single maximum elevation, flow calculation is simply done with Deterministic 8.\n
    Arguments
    ----------
    - DEM [`input grid`] : Elevation
    - AREA [`output grid`] : Catchment Area
    - ZMAX [`output grid`] : Maximum Height
    - MRN [`output grid`] : Melton Ruggedness Number

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_hydrology', '23', 'Melton Ruggedness Number')
    if Tool.is_Okay():
        Tool.Set_Input ('DEM', DEM)
        Tool.Set_Output('AREA', AREA)
        Tool.Set_Output('ZMAX', ZMAX)
        Tool.Set_Output('MRN', MRN)
        return Tool.Execute(Verbose)
    return False

def run_tool_ta_hydrology_23(DEM=None, AREA=None, ZMAX=None, MRN=None, Verbose=2):
    '''
    Melton Ruggedness Number
    ----------
    [ta_hydrology.23]\n
    Melton ruggedness number (MNR) is a simple flow accumulation related index, calculated as difference between maximum and minimum elevation in catchment area divided by square root of catchment area size. The calculation is performed for each grid cell, therefore minimum elevation is same as elevation at cell's position. Due to the discrete character of a single maximum elevation, flow calculation is simply done with Deterministic 8.\n
    Arguments
    ----------
    - DEM [`input grid`] : Elevation
    - AREA [`output grid`] : Catchment Area
    - ZMAX [`output grid`] : Maximum Height
    - MRN [`output grid`] : Melton Ruggedness Number

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_hydrology', '23', 'Melton Ruggedness Number')
    if Tool.is_Okay():
        Tool.Set_Input ('DEM', DEM)
        Tool.Set_Output('AREA', AREA)
        Tool.Set_Output('ZMAX', ZMAX)
        Tool.Set_Output('MRN', MRN)
        return Tool.Execute(Verbose)
    return False

def TCI_Low(DISTANCE=None, TWI=None, TCILOW=None, Verbose=2):
    '''
    TCI Low
    ----------
    [ta_hydrology.24]\n
    Terrain Classification Index for Lowlands (TCI Low).\n
    Arguments
    ----------
    - DISTANCE [`input grid`] : Vertical Distance to Channel Network
    - TWI [`input grid`] : Topographic Wetness Index
    - TCILOW [`output grid`] : TCI Low

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_hydrology', '24', 'TCI Low')
    if Tool.is_Okay():
        Tool.Set_Input ('DISTANCE', DISTANCE)
        Tool.Set_Input ('TWI', TWI)
        Tool.Set_Output('TCILOW', TCILOW)
        return Tool.Execute(Verbose)
    return False

def run_tool_ta_hydrology_24(DISTANCE=None, TWI=None, TCILOW=None, Verbose=2):
    '''
    TCI Low
    ----------
    [ta_hydrology.24]\n
    Terrain Classification Index for Lowlands (TCI Low).\n
    Arguments
    ----------
    - DISTANCE [`input grid`] : Vertical Distance to Channel Network
    - TWI [`input grid`] : Topographic Wetness Index
    - TCILOW [`output grid`] : TCI Low

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_hydrology', '24', 'TCI Low')
    if Tool.is_Okay():
        Tool.Set_Input ('DISTANCE', DISTANCE)
        Tool.Set_Input ('TWI', TWI)
        Tool.Set_Output('TCILOW', TCILOW)
        return Tool.Execute(Verbose)
    return False

def LSFactor_Field_Based(DEM=None, FIELDS=None, STATISTICS=None, UPSLOPE_AREA=None, UPSLOPE_LENGTH=None, UPSLOPE_SLOPE=None, LS_FACTOR=None, BALANCE=None, METHOD=None, METHOD_SLOPE=None, METHOD_AREA=None, FEET=None, STOP_AT_EDGE=None, EROSIVITY=None, STABILITY=None, Verbose=2):
    '''
    LS-Factor, Field Based
    ----------
    [ta_hydrology.25]\n
    Calculation of slope length (LS) factor as used for the Universal Soil Loss Equation (USLE), based on slope and (specific) catchment area, latter as substitute for slope length. This tool takes only a Digital Elevation Model (DEM) as input and derives catchment areas according to Freeman (1991). Optionally field polygons can be supplied. Is this the case, calculations will be performed field by field, i.e. catchment area calculation is restricted to each field's area.\n
    Arguments
    ----------
    - DEM [`input grid`] : Elevation
    - FIELDS [`optional input shapes`] : Fields
    - STATISTICS [`output shapes`] : Field Statistics
    - UPSLOPE_AREA [`output grid`] : Upslope Length Factor
    - UPSLOPE_LENGTH [`output grid`] : Effective Flow Length
    - UPSLOPE_SLOPE [`output grid`] : Upslope Slope
    - LS_FACTOR [`output grid`] : LS Factor
    - BALANCE [`output grid`] : Sediment Balance
    - METHOD [`choice`] : LS Calculation. Available Choices: [0] Moore & Nieber 1989 [1] Desmet & Govers 1996 [2] Wischmeier & Smith 1978 Default: 0
    - METHOD_SLOPE [`choice`] : Type of Slope. Available Choices: [0] local slope [1] distance weighted average catchment slope Default: 0
    - METHOD_AREA [`choice`] : Specific Catchment Area. Available Choices: [0] specific catchment area (contour length simply as cell size) [1] specific catchment area (contour length dependent on aspect) [2] catchment length (square root of catchment area) [3] effective flow length Default: 1
    - FEET [`boolean`] : Feet Adjustment. Default: 0 Needed if area and lengths come from coordinates measured in feet.
    - STOP_AT_EDGE [`boolean`] : Stop at Edge. Default: 1
    - EROSIVITY [`floating point number`] : Rill/Interrill Erosivity. Minimum: 0.000000 Default: 1.000000
    - STABILITY [`choice`] : Stability. Available Choices: [0] stable [1] instable (thawing) Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_hydrology', '25', 'LS-Factor, Field Based')
    if Tool.is_Okay():
        Tool.Set_Input ('DEM', DEM)
        Tool.Set_Input ('FIELDS', FIELDS)
        Tool.Set_Output('STATISTICS', STATISTICS)
        Tool.Set_Output('UPSLOPE_AREA', UPSLOPE_AREA)
        Tool.Set_Output('UPSLOPE_LENGTH', UPSLOPE_LENGTH)
        Tool.Set_Output('UPSLOPE_SLOPE', UPSLOPE_SLOPE)
        Tool.Set_Output('LS_FACTOR', LS_FACTOR)
        Tool.Set_Output('BALANCE', BALANCE)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('METHOD_SLOPE', METHOD_SLOPE)
        Tool.Set_Option('METHOD_AREA', METHOD_AREA)
        Tool.Set_Option('FEET', FEET)
        Tool.Set_Option('STOP_AT_EDGE', STOP_AT_EDGE)
        Tool.Set_Option('EROSIVITY', EROSIVITY)
        Tool.Set_Option('STABILITY', STABILITY)
        return Tool.Execute(Verbose)
    return False

def run_tool_ta_hydrology_25(DEM=None, FIELDS=None, STATISTICS=None, UPSLOPE_AREA=None, UPSLOPE_LENGTH=None, UPSLOPE_SLOPE=None, LS_FACTOR=None, BALANCE=None, METHOD=None, METHOD_SLOPE=None, METHOD_AREA=None, FEET=None, STOP_AT_EDGE=None, EROSIVITY=None, STABILITY=None, Verbose=2):
    '''
    LS-Factor, Field Based
    ----------
    [ta_hydrology.25]\n
    Calculation of slope length (LS) factor as used for the Universal Soil Loss Equation (USLE), based on slope and (specific) catchment area, latter as substitute for slope length. This tool takes only a Digital Elevation Model (DEM) as input and derives catchment areas according to Freeman (1991). Optionally field polygons can be supplied. Is this the case, calculations will be performed field by field, i.e. catchment area calculation is restricted to each field's area.\n
    Arguments
    ----------
    - DEM [`input grid`] : Elevation
    - FIELDS [`optional input shapes`] : Fields
    - STATISTICS [`output shapes`] : Field Statistics
    - UPSLOPE_AREA [`output grid`] : Upslope Length Factor
    - UPSLOPE_LENGTH [`output grid`] : Effective Flow Length
    - UPSLOPE_SLOPE [`output grid`] : Upslope Slope
    - LS_FACTOR [`output grid`] : LS Factor
    - BALANCE [`output grid`] : Sediment Balance
    - METHOD [`choice`] : LS Calculation. Available Choices: [0] Moore & Nieber 1989 [1] Desmet & Govers 1996 [2] Wischmeier & Smith 1978 Default: 0
    - METHOD_SLOPE [`choice`] : Type of Slope. Available Choices: [0] local slope [1] distance weighted average catchment slope Default: 0
    - METHOD_AREA [`choice`] : Specific Catchment Area. Available Choices: [0] specific catchment area (contour length simply as cell size) [1] specific catchment area (contour length dependent on aspect) [2] catchment length (square root of catchment area) [3] effective flow length Default: 1
    - FEET [`boolean`] : Feet Adjustment. Default: 0 Needed if area and lengths come from coordinates measured in feet.
    - STOP_AT_EDGE [`boolean`] : Stop at Edge. Default: 1
    - EROSIVITY [`floating point number`] : Rill/Interrill Erosivity. Minimum: 0.000000 Default: 1.000000
    - STABILITY [`choice`] : Stability. Available Choices: [0] stable [1] instable (thawing) Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_hydrology', '25', 'LS-Factor, Field Based')
    if Tool.is_Okay():
        Tool.Set_Input ('DEM', DEM)
        Tool.Set_Input ('FIELDS', FIELDS)
        Tool.Set_Output('STATISTICS', STATISTICS)
        Tool.Set_Output('UPSLOPE_AREA', UPSLOPE_AREA)
        Tool.Set_Output('UPSLOPE_LENGTH', UPSLOPE_LENGTH)
        Tool.Set_Output('UPSLOPE_SLOPE', UPSLOPE_SLOPE)
        Tool.Set_Output('LS_FACTOR', LS_FACTOR)
        Tool.Set_Output('BALANCE', BALANCE)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('METHOD_SLOPE', METHOD_SLOPE)
        Tool.Set_Option('METHOD_AREA', METHOD_AREA)
        Tool.Set_Option('FEET', FEET)
        Tool.Set_Option('STOP_AT_EDGE', STOP_AT_EDGE)
        Tool.Set_Option('EROSIVITY', EROSIVITY)
        Tool.Set_Option('STABILITY', STABILITY)
        return Tool.Execute(Verbose)
    return False

def Slope_Limited_Flow_Accumulation(DEM=None, WEIGHT=None, FLOW=None, SLOPE_MIN=None, SLOPE_MAX=None, B_FLOW=None, T_FLOW=None, Verbose=2):
    '''
    Slope Limited Flow Accumulation
    ----------
    [ta_hydrology.26]\n
    Flow accumulation is calculated as upslope contributing (catchment) area using the multiple flow direction approach of Freeman (1991). For this tool the approach has been modified to limit the flow portion routed through a cell depending on the local slope. If a cell is not inclined, no flow is routed through it at all. With increasing slopes the portion of flow routed through a cell becomes higher. Cells with slopes greater than a specified slope threshold route their entire accumulated flow downhill.\n
    Arguments
    ----------
    - DEM [`input grid`] : Elevation
    - WEIGHT [`optional input grid`] : Weight
    - FLOW [`output grid`] : Flow Accumulation
    - SLOPE_MIN [`floating point number`] : Slope Minimum. Minimum: 0.000000 Default: 0.000000 Assume a given minimum slope for each cell.
    - SLOPE_MAX [`floating point number`] : Slope Threshold. Minimum: 0.000000 Maximum: 90.000000 Default: 5.000000 Slope threshold, given as degree, above which flow transport is unlimited.
    - B_FLOW [`boolean`] : Use Flow Threshold. Default: 0
    - T_FLOW [`value range`] : Flow Threshold. Flow threshold, given as amount of cells, above which flow transport is unlimited. Ignored if range equals zero.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_hydrology', '26', 'Slope Limited Flow Accumulation')
    if Tool.is_Okay():
        Tool.Set_Input ('DEM', DEM)
        Tool.Set_Input ('WEIGHT', WEIGHT)
        Tool.Set_Output('FLOW', FLOW)
        Tool.Set_Option('SLOPE_MIN', SLOPE_MIN)
        Tool.Set_Option('SLOPE_MAX', SLOPE_MAX)
        Tool.Set_Option('B_FLOW', B_FLOW)
        Tool.Set_Option('T_FLOW', T_FLOW)
        return Tool.Execute(Verbose)
    return False

def run_tool_ta_hydrology_26(DEM=None, WEIGHT=None, FLOW=None, SLOPE_MIN=None, SLOPE_MAX=None, B_FLOW=None, T_FLOW=None, Verbose=2):
    '''
    Slope Limited Flow Accumulation
    ----------
    [ta_hydrology.26]\n
    Flow accumulation is calculated as upslope contributing (catchment) area using the multiple flow direction approach of Freeman (1991). For this tool the approach has been modified to limit the flow portion routed through a cell depending on the local slope. If a cell is not inclined, no flow is routed through it at all. With increasing slopes the portion of flow routed through a cell becomes higher. Cells with slopes greater than a specified slope threshold route their entire accumulated flow downhill.\n
    Arguments
    ----------
    - DEM [`input grid`] : Elevation
    - WEIGHT [`optional input grid`] : Weight
    - FLOW [`output grid`] : Flow Accumulation
    - SLOPE_MIN [`floating point number`] : Slope Minimum. Minimum: 0.000000 Default: 0.000000 Assume a given minimum slope for each cell.
    - SLOPE_MAX [`floating point number`] : Slope Threshold. Minimum: 0.000000 Maximum: 90.000000 Default: 5.000000 Slope threshold, given as degree, above which flow transport is unlimited.
    - B_FLOW [`boolean`] : Use Flow Threshold. Default: 0
    - T_FLOW [`value range`] : Flow Threshold. Flow threshold, given as amount of cells, above which flow transport is unlimited. Ignored if range equals zero.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_hydrology', '26', 'Slope Limited Flow Accumulation')
    if Tool.is_Okay():
        Tool.Set_Input ('DEM', DEM)
        Tool.Set_Input ('WEIGHT', WEIGHT)
        Tool.Set_Output('FLOW', FLOW)
        Tool.Set_Option('SLOPE_MIN', SLOPE_MIN)
        Tool.Set_Option('SLOPE_MAX', SLOPE_MAX)
        Tool.Set_Option('B_FLOW', B_FLOW)
        Tool.Set_Option('T_FLOW', T_FLOW)
        return Tool.Execute(Verbose)
    return False

def Maximum_Flow_Path_Length(ELEVATION=None, WEIGHTS=None, DISTANCE=None, DIRECTION=None, Verbose=2):
    '''
    Maximum Flow Path Length
    ----------
    [ta_hydrology.27]\n
    This tool calculates the maximum upstream or downstream distance or weighted distance along the flow path for each cell based on 'Deterministic 8 (D8)' (O'Callaghan and Mark 1984) flow directions.\n
    Arguments
    ----------
    - ELEVATION [`input grid`] : Elevation
    - WEIGHTS [`optional input grid`] : Weights
    - DISTANCE [`output grid`] : Flow Path Length
    - DIRECTION [`choice`] : Direction of Measurement. Available Choices: [0] downstream [1] upstream Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_hydrology', '27', 'Maximum Flow Path Length')
    if Tool.is_Okay():
        Tool.Set_Input ('ELEVATION', ELEVATION)
        Tool.Set_Input ('WEIGHTS', WEIGHTS)
        Tool.Set_Output('DISTANCE', DISTANCE)
        Tool.Set_Option('DIRECTION', DIRECTION)
        return Tool.Execute(Verbose)
    return False

def run_tool_ta_hydrology_27(ELEVATION=None, WEIGHTS=None, DISTANCE=None, DIRECTION=None, Verbose=2):
    '''
    Maximum Flow Path Length
    ----------
    [ta_hydrology.27]\n
    This tool calculates the maximum upstream or downstream distance or weighted distance along the flow path for each cell based on 'Deterministic 8 (D8)' (O'Callaghan and Mark 1984) flow directions.\n
    Arguments
    ----------
    - ELEVATION [`input grid`] : Elevation
    - WEIGHTS [`optional input grid`] : Weights
    - DISTANCE [`output grid`] : Flow Path Length
    - DIRECTION [`choice`] : Direction of Measurement. Available Choices: [0] downstream [1] upstream Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_hydrology', '27', 'Maximum Flow Path Length')
    if Tool.is_Okay():
        Tool.Set_Input ('ELEVATION', ELEVATION)
        Tool.Set_Input ('WEIGHTS', WEIGHTS)
        Tool.Set_Output('DISTANCE', DISTANCE)
        Tool.Set_Option('DIRECTION', DIRECTION)
        return Tool.Execute(Verbose)
    return False

def Flow_between_fields(ELEVATION=None, FIELDS=None, FLOW=None, UPAREA=None, STOP=None, Verbose=2):
    '''
    Flow between fields
    ----------
    [ta_hydrology.28]\n
    Flow between fields (identified by ID)\n
    Arguments
    ----------
    - ELEVATION [`input grid`] : Elevation
    - FIELDS [`input grid`] : FIELDS
    - FLOW [`output table`] : Flow table. Table containing the flow from and to each field
    - UPAREA [`output grid`] : Uparea. uparea
    - STOP [`boolean`] : Stop at edge. Default: 1 Stop flow at the edge of a field

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_hydrology', '28', 'Flow between fields')
    if Tool.is_Okay():
        Tool.Set_Input ('ELEVATION', ELEVATION)
        Tool.Set_Input ('FIELDS', FIELDS)
        Tool.Set_Output('FLOW', FLOW)
        Tool.Set_Output('UPAREA', UPAREA)
        Tool.Set_Option('STOP', STOP)
        return Tool.Execute(Verbose)
    return False

def run_tool_ta_hydrology_28(ELEVATION=None, FIELDS=None, FLOW=None, UPAREA=None, STOP=None, Verbose=2):
    '''
    Flow between fields
    ----------
    [ta_hydrology.28]\n
    Flow between fields (identified by ID)\n
    Arguments
    ----------
    - ELEVATION [`input grid`] : Elevation
    - FIELDS [`input grid`] : FIELDS
    - FLOW [`output table`] : Flow table. Table containing the flow from and to each field
    - UPAREA [`output grid`] : Uparea. uparea
    - STOP [`boolean`] : Stop at edge. Default: 1 Stop flow at the edge of a field

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_hydrology', '28', 'Flow between fields')
    if Tool.is_Okay():
        Tool.Set_Input ('ELEVATION', ELEVATION)
        Tool.Set_Input ('FIELDS', FIELDS)
        Tool.Set_Output('FLOW', FLOW)
        Tool.Set_Output('UPAREA', UPAREA)
        Tool.Set_Option('STOP', STOP)
        return Tool.Execute(Verbose)
    return False

def Flow_Accumulation_Parallelizable(DEM=None, FLOW=None, UPDATE=None, METHOD=None, CONVERGENCE=None, Verbose=2):
    '''
    Flow Accumulation (Parallelizable)
    ----------
    [ta_hydrology.29]\n
    A simple implementation of a parallelizable flow accumulation algorithn.\n
    Arguments
    ----------
    - DEM [`input grid`] : Elevation
    - FLOW [`output grid`] : Flow Accumulation
    - UPDATE [`integer number`] : Update Frequency. Minimum: 0 Default: 0 if zero no updates will be done
    - METHOD [`choice`] : Method. Available Choices: [0] D8 [1] Dinfinity [2] MFD Default: 2
    - CONVERGENCE [`floating point number`] : Convergence. Minimum: 0.000000 Default: 1.100000

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_hydrology', '29', 'Flow Accumulation (Parallelizable)')
    if Tool.is_Okay():
        Tool.Set_Input ('DEM', DEM)
        Tool.Set_Output('FLOW', FLOW)
        Tool.Set_Option('UPDATE', UPDATE)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('CONVERGENCE', CONVERGENCE)
        return Tool.Execute(Verbose)
    return False

def run_tool_ta_hydrology_29(DEM=None, FLOW=None, UPDATE=None, METHOD=None, CONVERGENCE=None, Verbose=2):
    '''
    Flow Accumulation (Parallelizable)
    ----------
    [ta_hydrology.29]\n
    A simple implementation of a parallelizable flow accumulation algorithn.\n
    Arguments
    ----------
    - DEM [`input grid`] : Elevation
    - FLOW [`output grid`] : Flow Accumulation
    - UPDATE [`integer number`] : Update Frequency. Minimum: 0 Default: 0 if zero no updates will be done
    - METHOD [`choice`] : Method. Available Choices: [0] D8 [1] Dinfinity [2] MFD Default: 2
    - CONVERGENCE [`floating point number`] : Convergence. Minimum: 0.000000 Default: 1.100000

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_hydrology', '29', 'Flow Accumulation (Parallelizable)')
    if Tool.is_Okay():
        Tool.Set_Input ('DEM', DEM)
        Tool.Set_Output('FLOW', FLOW)
        Tool.Set_Option('UPDATE', UPDATE)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('CONVERGENCE', CONVERGENCE)
        return Tool.Execute(Verbose)
    return False

def Isochrones_Variable_Speed(DEM=None, SLOPE=None, FLOWACC=None, CN=None, MANNING=None, TARGET_PT=None, TIME=None, SPEED=None, AVGMANNING=None, AVGCN=None, THRSMIXED=None, THRSCHANNEL=None, AVGRAINFALL=None, CHANSLOPE=None, MINSPEED=None, TARGET_PT_X=None, TARGET_PT_Y=None, Verbose=2):
    '''
    Isochrones Variable Speed
    ----------
    [ta_hydrology.30]\n
    Calculation of isochrones with variable speed.\n
    In case a cell in an optional input grid is NoData, the corresponding parameter value will be used instead of skipping this cell.\n
    This is the non-interactive tool version, where the target point can be specified either as point shapefile (the first point in the file will be used) or by target coordinates.\n
    Arguments
    ----------
    - DEM [`input grid`] : Elevation
    - SLOPE [`input grid`] : Slope
    - FLOWACC [`input grid`] : Catchment Area
    - CN [`optional input grid`] : Curve Number
    - MANNING [`optional input grid`] : Manning's N
    - TARGET_PT [`optional input shapes`] : Target Point. A point shapefile with the target point.
    - TIME [`output grid`] : Time Out(h)
    - SPEED [`output grid`] : Speed (m/s)
    - AVGMANNING [`floating point number`] : Avg. Manning's N. Default: 0.150000
    - AVGCN [`floating point number`] : Avg. Curve Number. Default: 75.000000
    - THRSMIXED [`floating point number`] : Mixed Flow Threshold (ha). Default: 18.000000
    - THRSCHANNEL [`floating point number`] : Channel Definition Threshold (ha). Default: 360.000000
    - AVGRAINFALL [`floating point number`] : Avg. Rainfall Intensity (mm/h). Default: 1.000000
    - CHANSLOPE [`floating point number`] : Channel side slope(m/m). Default: 0.500000
    - MINSPEED [`floating point number`] : Min. Flow Speed (m/s). Default: 0.050000
    - TARGET_PT_X [`floating point number`] : Target X Coordinate. Default: 0.000000 The x-coordinate of the target point in world coordinates [map units]
    - TARGET_PT_Y [`floating point number`] : Target Y Coordinate. Default: 0.000000 The y-coordinate of the target point in world coordinates [map units]

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_hydrology', '30', 'Isochrones Variable Speed')
    if Tool.is_Okay():
        Tool.Set_Input ('DEM', DEM)
        Tool.Set_Input ('SLOPE', SLOPE)
        Tool.Set_Input ('FLOWACC', FLOWACC)
        Tool.Set_Input ('CN', CN)
        Tool.Set_Input ('MANNING', MANNING)
        Tool.Set_Input ('TARGET_PT', TARGET_PT)
        Tool.Set_Output('TIME', TIME)
        Tool.Set_Output('SPEED', SPEED)
        Tool.Set_Option('AVGMANNING', AVGMANNING)
        Tool.Set_Option('AVGCN', AVGCN)
        Tool.Set_Option('THRSMIXED', THRSMIXED)
        Tool.Set_Option('THRSCHANNEL', THRSCHANNEL)
        Tool.Set_Option('AVGRAINFALL', AVGRAINFALL)
        Tool.Set_Option('CHANSLOPE', CHANSLOPE)
        Tool.Set_Option('MINSPEED', MINSPEED)
        Tool.Set_Option('TARGET_PT_X', TARGET_PT_X)
        Tool.Set_Option('TARGET_PT_Y', TARGET_PT_Y)
        return Tool.Execute(Verbose)
    return False

def run_tool_ta_hydrology_30(DEM=None, SLOPE=None, FLOWACC=None, CN=None, MANNING=None, TARGET_PT=None, TIME=None, SPEED=None, AVGMANNING=None, AVGCN=None, THRSMIXED=None, THRSCHANNEL=None, AVGRAINFALL=None, CHANSLOPE=None, MINSPEED=None, TARGET_PT_X=None, TARGET_PT_Y=None, Verbose=2):
    '''
    Isochrones Variable Speed
    ----------
    [ta_hydrology.30]\n
    Calculation of isochrones with variable speed.\n
    In case a cell in an optional input grid is NoData, the corresponding parameter value will be used instead of skipping this cell.\n
    This is the non-interactive tool version, where the target point can be specified either as point shapefile (the first point in the file will be used) or by target coordinates.\n
    Arguments
    ----------
    - DEM [`input grid`] : Elevation
    - SLOPE [`input grid`] : Slope
    - FLOWACC [`input grid`] : Catchment Area
    - CN [`optional input grid`] : Curve Number
    - MANNING [`optional input grid`] : Manning's N
    - TARGET_PT [`optional input shapes`] : Target Point. A point shapefile with the target point.
    - TIME [`output grid`] : Time Out(h)
    - SPEED [`output grid`] : Speed (m/s)
    - AVGMANNING [`floating point number`] : Avg. Manning's N. Default: 0.150000
    - AVGCN [`floating point number`] : Avg. Curve Number. Default: 75.000000
    - THRSMIXED [`floating point number`] : Mixed Flow Threshold (ha). Default: 18.000000
    - THRSCHANNEL [`floating point number`] : Channel Definition Threshold (ha). Default: 360.000000
    - AVGRAINFALL [`floating point number`] : Avg. Rainfall Intensity (mm/h). Default: 1.000000
    - CHANSLOPE [`floating point number`] : Channel side slope(m/m). Default: 0.500000
    - MINSPEED [`floating point number`] : Min. Flow Speed (m/s). Default: 0.050000
    - TARGET_PT_X [`floating point number`] : Target X Coordinate. Default: 0.000000 The x-coordinate of the target point in world coordinates [map units]
    - TARGET_PT_Y [`floating point number`] : Target Y Coordinate. Default: 0.000000 The y-coordinate of the target point in world coordinates [map units]

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_hydrology', '30', 'Isochrones Variable Speed')
    if Tool.is_Okay():
        Tool.Set_Input ('DEM', DEM)
        Tool.Set_Input ('SLOPE', SLOPE)
        Tool.Set_Input ('FLOWACC', FLOWACC)
        Tool.Set_Input ('CN', CN)
        Tool.Set_Input ('MANNING', MANNING)
        Tool.Set_Input ('TARGET_PT', TARGET_PT)
        Tool.Set_Output('TIME', TIME)
        Tool.Set_Output('SPEED', SPEED)
        Tool.Set_Option('AVGMANNING', AVGMANNING)
        Tool.Set_Option('AVGCN', AVGCN)
        Tool.Set_Option('THRSMIXED', THRSMIXED)
        Tool.Set_Option('THRSCHANNEL', THRSCHANNEL)
        Tool.Set_Option('AVGRAINFALL', AVGRAINFALL)
        Tool.Set_Option('CHANSLOPE', CHANSLOPE)
        Tool.Set_Option('MINSPEED', MINSPEED)
        Tool.Set_Option('TARGET_PT_X', TARGET_PT_X)
        Tool.Set_Option('TARGET_PT_Y', TARGET_PT_Y)
        return Tool.Execute(Verbose)
    return False

def CIT_Index(SLOPE=None, AREA=None, CIT=None, CONV=None, Verbose=2):
    '''
    CIT Index
    ----------
    [ta_hydrology.31]\n
    The tool allows one to calculate a variant of the Stream Power Index, which was introduced to detect channel heads (channel initiation) based on a drainage area-slope threshold. The Channel Initiation Threshold (CIT) index is calculated as: CIT = SCA * tan(Slope)^2\n
    Arguments
    ----------
    - SLOPE [`input grid`] : Slope. The slope grid [radians].
    - AREA [`input grid`] : Catchment Area. The catchment area grid [map unitsz^2].
    - CIT [`output grid`] : CIT Index. The Channel Initiation Threshold index.
    - CONV [`choice`] : Area Conversion. Available Choices: [0] no conversion (areas already given as specific catchment area) [1] 1 / cell size (pseudo specific catchment area) Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_hydrology', '31', 'CIT Index')
    if Tool.is_Okay():
        Tool.Set_Input ('SLOPE', SLOPE)
        Tool.Set_Input ('AREA', AREA)
        Tool.Set_Output('CIT', CIT)
        Tool.Set_Option('CONV', CONV)
        return Tool.Execute(Verbose)
    return False

def run_tool_ta_hydrology_31(SLOPE=None, AREA=None, CIT=None, CONV=None, Verbose=2):
    '''
    CIT Index
    ----------
    [ta_hydrology.31]\n
    The tool allows one to calculate a variant of the Stream Power Index, which was introduced to detect channel heads (channel initiation) based on a drainage area-slope threshold. The Channel Initiation Threshold (CIT) index is calculated as: CIT = SCA * tan(Slope)^2\n
    Arguments
    ----------
    - SLOPE [`input grid`] : Slope. The slope grid [radians].
    - AREA [`input grid`] : Catchment Area. The catchment area grid [map unitsz^2].
    - CIT [`output grid`] : CIT Index. The Channel Initiation Threshold index.
    - CONV [`choice`] : Area Conversion. Available Choices: [0] no conversion (areas already given as specific catchment area) [1] 1 / cell size (pseudo specific catchment area) Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_hydrology', '31', 'CIT Index')
    if Tool.is_Okay():
        Tool.Set_Input ('SLOPE', SLOPE)
        Tool.Set_Input ('AREA', AREA)
        Tool.Set_Output('CIT', CIT)
        Tool.Set_Option('CONV', CONV)
        return Tool.Execute(Verbose)
    return False

def Terrain_Flooding(DEM=None, SEEDS=None, WATER_BODY=None, DEM_FLOODED=None, WATER_LEVEL=None, WATER_LEVEL_DEFAULT=None, LEVEL_REFERENCE=None, CONSTANT_LEVEL=None, Verbose=2):
    '''
    Terrain Flooding
    ----------
    [ta_hydrology.32]\n
    The tool allows one to flood a digital elevation model for a given water level. The water level can be provided either as absolute height or relative to the DEM.\n
    If the water level is given relative to the DEM, the tool can model a constant water level starting from the seed cell, or a water level that is always relative to the currently processed cell. This way also inclined water surfaces, e.g. along a river, can be modelled. Note that this usually requires rather small relative water levels in order to prevent the flooding of the complete DEM; the functionality is most suited to generate a segment (connected component) of a river bed.\n
    Arguments
    ----------
    - DEM [`input grid`] : DEM. The digital elevation model to flood.
    - SEEDS [`input shapes`] : Seed Points. The point(s) from where to start the flooding.
    - WATER_BODY [`output grid`] : Water Body. The extent of the water body, labeled with local water depth [map units].
    - DEM_FLOODED [`output grid`] : Flooded DEM. The flooded digital elevation model.
    - WATER_LEVEL [`table field`] : Water Level. The attribute field with the local water level, given either relative to the DEM or as absolute height [map units].
    - WATER_LEVEL_DEFAULT [`floating point number`] : Default. Default: 0.500000 default value if no attribute has been selected
    - LEVEL_REFERENCE [`choice`] : Water Level Reference. Available Choices: [0] level is given relative to DEM [1] level is given as absolute water height Default: 0
    - CONSTANT_LEVEL [`boolean`] : Constant Water Level. Default: 1 Model the water level as constant. Otherwise the specified level is always taken as relative to the currently processed cell. This option is only available if the water level reference is relative to the DEM.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_hydrology', '32', 'Terrain Flooding')
    if Tool.is_Okay():
        Tool.Set_Input ('DEM', DEM)
        Tool.Set_Input ('SEEDS', SEEDS)
        Tool.Set_Output('WATER_BODY', WATER_BODY)
        Tool.Set_Output('DEM_FLOODED', DEM_FLOODED)
        Tool.Set_Option('WATER_LEVEL', WATER_LEVEL)
        Tool.Set_Option('WATER_LEVEL_DEFAULT', WATER_LEVEL_DEFAULT)
        Tool.Set_Option('LEVEL_REFERENCE', LEVEL_REFERENCE)
        Tool.Set_Option('CONSTANT_LEVEL', CONSTANT_LEVEL)
        return Tool.Execute(Verbose)
    return False

def run_tool_ta_hydrology_32(DEM=None, SEEDS=None, WATER_BODY=None, DEM_FLOODED=None, WATER_LEVEL=None, WATER_LEVEL_DEFAULT=None, LEVEL_REFERENCE=None, CONSTANT_LEVEL=None, Verbose=2):
    '''
    Terrain Flooding
    ----------
    [ta_hydrology.32]\n
    The tool allows one to flood a digital elevation model for a given water level. The water level can be provided either as absolute height or relative to the DEM.\n
    If the water level is given relative to the DEM, the tool can model a constant water level starting from the seed cell, or a water level that is always relative to the currently processed cell. This way also inclined water surfaces, e.g. along a river, can be modelled. Note that this usually requires rather small relative water levels in order to prevent the flooding of the complete DEM; the functionality is most suited to generate a segment (connected component) of a river bed.\n
    Arguments
    ----------
    - DEM [`input grid`] : DEM. The digital elevation model to flood.
    - SEEDS [`input shapes`] : Seed Points. The point(s) from where to start the flooding.
    - WATER_BODY [`output grid`] : Water Body. The extent of the water body, labeled with local water depth [map units].
    - DEM_FLOODED [`output grid`] : Flooded DEM. The flooded digital elevation model.
    - WATER_LEVEL [`table field`] : Water Level. The attribute field with the local water level, given either relative to the DEM or as absolute height [map units].
    - WATER_LEVEL_DEFAULT [`floating point number`] : Default. Default: 0.500000 default value if no attribute has been selected
    - LEVEL_REFERENCE [`choice`] : Water Level Reference. Available Choices: [0] level is given relative to DEM [1] level is given as absolute water height Default: 0
    - CONSTANT_LEVEL [`boolean`] : Constant Water Level. Default: 1 Model the water level as constant. Otherwise the specified level is always taken as relative to the currently processed cell. This option is only available if the water level reference is relative to the DEM.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_hydrology', '32', 'Terrain Flooding')
    if Tool.is_Okay():
        Tool.Set_Input ('DEM', DEM)
        Tool.Set_Input ('SEEDS', SEEDS)
        Tool.Set_Output('WATER_BODY', WATER_BODY)
        Tool.Set_Output('DEM_FLOODED', DEM_FLOODED)
        Tool.Set_Option('WATER_LEVEL', WATER_LEVEL)
        Tool.Set_Option('WATER_LEVEL_DEFAULT', WATER_LEVEL_DEFAULT)
        Tool.Set_Option('LEVEL_REFERENCE', LEVEL_REFERENCE)
        Tool.Set_Option('CONSTANT_LEVEL', CONSTANT_LEVEL)
        return Tool.Execute(Verbose)
    return False

def Flow_Accumulation_One_Step(DEM=None, TCA=None, SCA=None, GRID_SYSTEM=None, PREPROCESSING=None, FLOW_ROUTING=None, Verbose=2):
    '''
    Flow Accumulation (One Step)
    ----------
    [ta_hydrology.flow_accumulation]\n
    This is a simple to use one step tool to derive flow accumulation from a Digital Terrain Model. It includes a preprocessing followed by a call to 'Flow Accumulation (Top-Down) exposing only the most important options.\n
    \n
    Arguments
    ----------
    - DEM [`input grid`] : Elevation
    - TCA [`output grid`] : Flow Accumulation. The flow accumulation, a.k.a. Total Catchment Area (TCA)
    - SCA [`output grid`] : Specific Catchment Area
    - GRID_SYSTEM [`grid system`] : Grid System
    - PREPROCESSING [`choice`] : Preprocessing. Available Choices: [0] Fill Sinks (Wang & Liu) [1] Sink Removal Default: 0
    - FLOW_ROUTING [`choice`] : Flow Routing. Available Choices: [0] Deterministic 8 [1] Rho 8 [2] Braunschweiger Reliefmodell [3] Deterministic Infinity [4] Multiple Flow Direction [5] Multiple Triangular Flow Direction [6] Multiple Maximum Downslope Gradient Based Flow Direction Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_hydrology', 'flow_accumulation', 'Flow Accumulation (One Step)')
    if Tool.is_Okay():
        Tool.Set_Input ('DEM', DEM)
        Tool.Set_Output('TCA', TCA)
        Tool.Set_Output('SCA', SCA)
        Tool.Set_Option('GRID_SYSTEM', GRID_SYSTEM)
        Tool.Set_Option('PREPROCESSING', PREPROCESSING)
        Tool.Set_Option('FLOW_ROUTING', FLOW_ROUTING)
        return Tool.Execute(Verbose)
    return False

def run_tool_ta_hydrology_flow_accumulation(DEM=None, TCA=None, SCA=None, GRID_SYSTEM=None, PREPROCESSING=None, FLOW_ROUTING=None, Verbose=2):
    '''
    Flow Accumulation (One Step)
    ----------
    [ta_hydrology.flow_accumulation]\n
    This is a simple to use one step tool to derive flow accumulation from a Digital Terrain Model. It includes a preprocessing followed by a call to 'Flow Accumulation (Top-Down) exposing only the most important options.\n
    \n
    Arguments
    ----------
    - DEM [`input grid`] : Elevation
    - TCA [`output grid`] : Flow Accumulation. The flow accumulation, a.k.a. Total Catchment Area (TCA)
    - SCA [`output grid`] : Specific Catchment Area
    - GRID_SYSTEM [`grid system`] : Grid System
    - PREPROCESSING [`choice`] : Preprocessing. Available Choices: [0] Fill Sinks (Wang & Liu) [1] Sink Removal Default: 0
    - FLOW_ROUTING [`choice`] : Flow Routing. Available Choices: [0] Deterministic 8 [1] Rho 8 [2] Braunschweiger Reliefmodell [3] Deterministic Infinity [4] Multiple Flow Direction [5] Multiple Triangular Flow Direction [6] Multiple Maximum Downslope Gradient Based Flow Direction Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_hydrology', 'flow_accumulation', 'Flow Accumulation (One Step)')
    if Tool.is_Okay():
        Tool.Set_Input ('DEM', DEM)
        Tool.Set_Output('TCA', TCA)
        Tool.Set_Output('SCA', SCA)
        Tool.Set_Option('GRID_SYSTEM', GRID_SYSTEM)
        Tool.Set_Option('PREPROCESSING', PREPROCESSING)
        Tool.Set_Option('FLOW_ROUTING', FLOW_ROUTING)
        return Tool.Execute(Verbose)
    return False

def LS_Factor_One_Step(DEM=None, LS_FACTOR=None, GRID_SYSTEM=None, FEET=None, LS_METHOD=None, PREPROCESSING=None, MINSLOPE=None, Verbose=2):
    '''
    LS Factor (One Step)
    ----------
    [ta_hydrology.ls_factor]\n
    This is a simple to use one step tool to derive the LS Factor of the Universal Soil Loss Equation (USLE) from a Digital Elevatin Model (DEM). It combines DEM preprocessing (sink removal), flow accumulation and specific catchment area (SCA) derivation, slope and final LS factor calculation exposing only the most important options.\n
    \n
    Arguments
    ----------
    - DEM [`input grid`] : DEM
    - LS_FACTOR [`output grid`] : LS Factor
    - GRID_SYSTEM [`grid system`] : Grid System
    - FEET [`boolean`] : Feet Conversion. Default: 0 Needed if area and lengths come from coordinates measured in feet.
    - LS_METHOD [`choice`] : Method. Available Choices: [0] Moore et al. 1991 [1] Desmond & Govers 1996 Default: 0
    - PREPROCESSING [`choice`] : Preprocessing. Available Choices: [0] none [1] Fill Sinks (Wang & Liu) [2] Sink Removal Default: 0
    - MINSLOPE [`floating point number`] : Minimum Slope. Minimum: 0.000000 Default: 0.000100 Minimum slope (degree) for filled areas.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_hydrology', 'ls_factor', 'LS Factor (One Step)')
    if Tool.is_Okay():
        Tool.Set_Input ('DEM', DEM)
        Tool.Set_Output('LS_FACTOR', LS_FACTOR)
        Tool.Set_Option('GRID_SYSTEM', GRID_SYSTEM)
        Tool.Set_Option('FEET', FEET)
        Tool.Set_Option('LS_METHOD', LS_METHOD)
        Tool.Set_Option('PREPROCESSING', PREPROCESSING)
        Tool.Set_Option('MINSLOPE', MINSLOPE)
        return Tool.Execute(Verbose)
    return False

def run_tool_ta_hydrology_ls_factor(DEM=None, LS_FACTOR=None, GRID_SYSTEM=None, FEET=None, LS_METHOD=None, PREPROCESSING=None, MINSLOPE=None, Verbose=2):
    '''
    LS Factor (One Step)
    ----------
    [ta_hydrology.ls_factor]\n
    This is a simple to use one step tool to derive the LS Factor of the Universal Soil Loss Equation (USLE) from a Digital Elevatin Model (DEM). It combines DEM preprocessing (sink removal), flow accumulation and specific catchment area (SCA) derivation, slope and final LS factor calculation exposing only the most important options.\n
    \n
    Arguments
    ----------
    - DEM [`input grid`] : DEM
    - LS_FACTOR [`output grid`] : LS Factor
    - GRID_SYSTEM [`grid system`] : Grid System
    - FEET [`boolean`] : Feet Conversion. Default: 0 Needed if area and lengths come from coordinates measured in feet.
    - LS_METHOD [`choice`] : Method. Available Choices: [0] Moore et al. 1991 [1] Desmond & Govers 1996 Default: 0
    - PREPROCESSING [`choice`] : Preprocessing. Available Choices: [0] none [1] Fill Sinks (Wang & Liu) [2] Sink Removal Default: 0
    - MINSLOPE [`floating point number`] : Minimum Slope. Minimum: 0.000000 Default: 0.000100 Minimum slope (degree) for filled areas.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_hydrology', 'ls_factor', 'LS Factor (One Step)')
    if Tool.is_Okay():
        Tool.Set_Input ('DEM', DEM)
        Tool.Set_Output('LS_FACTOR', LS_FACTOR)
        Tool.Set_Option('GRID_SYSTEM', GRID_SYSTEM)
        Tool.Set_Option('FEET', FEET)
        Tool.Set_Option('LS_METHOD', LS_METHOD)
        Tool.Set_Option('PREPROCESSING', PREPROCESSING)
        Tool.Set_Option('MINSLOPE', MINSLOPE)
        return Tool.Execute(Verbose)
    return False

def Topographic_Wetness_Index_One_Step(DEM=None, TWI=None, GRID_SYSTEM=None, FLOW_METHOD=None, Verbose=2):
    '''
    Topographic Wetness Index (One Step)
    ----------
    [ta_hydrology.twi]\n
    Calculates the Topographic Wetness Index (TWI) based on the supplied Digital Elevation Model (DEM). Slopes (Beta), Specific Catchment Areas (SCA), and a depressionless DEM for SCA calculation are derived internally.\n
    TWI = ln(SCA / tan(Beta))\n
    \n
    Arguments
    ----------
    - DEM [`input grid`] : Elevation
    - TWI [`output grid`] : Topographic Wetness Index
    - GRID_SYSTEM [`grid system`] : Grid System
    - FLOW_METHOD [`choice`] : Flow Distribution. Available Choices: [0] Deterministic 8 [1] Rho 8 [2] Braunschweiger Reliefmodell [3] Deterministic Infinity [4] Multiple Flow Direction [5] Multiple Triangular Flow Direction [6] Multiple Maximum Downslope Gradient Based Flow Direction Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_hydrology', 'twi', 'Topographic Wetness Index (One Step)')
    if Tool.is_Okay():
        Tool.Set_Input ('DEM', DEM)
        Tool.Set_Output('TWI', TWI)
        Tool.Set_Option('GRID_SYSTEM', GRID_SYSTEM)
        Tool.Set_Option('FLOW_METHOD', FLOW_METHOD)
        return Tool.Execute(Verbose)
    return False

def run_tool_ta_hydrology_twi(DEM=None, TWI=None, GRID_SYSTEM=None, FLOW_METHOD=None, Verbose=2):
    '''
    Topographic Wetness Index (One Step)
    ----------
    [ta_hydrology.twi]\n
    Calculates the Topographic Wetness Index (TWI) based on the supplied Digital Elevation Model (DEM). Slopes (Beta), Specific Catchment Areas (SCA), and a depressionless DEM for SCA calculation are derived internally.\n
    TWI = ln(SCA / tan(Beta))\n
    \n
    Arguments
    ----------
    - DEM [`input grid`] : Elevation
    - TWI [`output grid`] : Topographic Wetness Index
    - GRID_SYSTEM [`grid system`] : Grid System
    - FLOW_METHOD [`choice`] : Flow Distribution. Available Choices: [0] Deterministic 8 [1] Rho 8 [2] Braunschweiger Reliefmodell [3] Deterministic Infinity [4] Multiple Flow Direction [5] Multiple Triangular Flow Direction [6] Multiple Maximum Downslope Gradient Based Flow Direction Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_hydrology', 'twi', 'Topographic Wetness Index (One Step)')
    if Tool.is_Okay():
        Tool.Set_Input ('DEM', DEM)
        Tool.Set_Output('TWI', TWI)
        Tool.Set_Option('GRID_SYSTEM', GRID_SYSTEM)
        Tool.Set_Option('FLOW_METHOD', FLOW_METHOD)
        return Tool.Execute(Verbose)
    return False

def Upslope_Height_Slope_Aspect(DEM=None, HEIGHT=None, SLOPE=None, ASPECT=None, GRID_SYSTEM=None, FLOW_METHOD=None, Verbose=2):
    '''
    Upslope Height, Slope, Aspect
    ----------
    [ta_hydrology.upslope_height]\n
    This tool calculates the mean height, mean slope and mean aspect of the upslope contributing area.\n
    \n
    Arguments
    ----------
    - DEM [`input grid`] : Elevation
    - HEIGHT [`output grid`] : Upslope Height
    - SLOPE [`output grid`] : Slope
    - ASPECT [`output grid`] : Aspect
    - GRID_SYSTEM [`grid system`] : Grid System
    - FLOW_METHOD [`choice`] : Flow Distribution. Available Choices: [0] Deterministic 8 [1] Rho 8 [2] Braunschweiger Reliefmodell [3] Deterministic Infinity [4] Multiple Flow Direction [5] Multiple Triangular Flow Direction [6] Multiple Maximum Downslope Gradient Based Flow Direction Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_hydrology', 'upslope_height', 'Upslope Height, Slope, Aspect')
    if Tool.is_Okay():
        Tool.Set_Input ('DEM', DEM)
        Tool.Set_Output('HEIGHT', HEIGHT)
        Tool.Set_Output('SLOPE', SLOPE)
        Tool.Set_Output('ASPECT', ASPECT)
        Tool.Set_Option('GRID_SYSTEM', GRID_SYSTEM)
        Tool.Set_Option('FLOW_METHOD', FLOW_METHOD)
        return Tool.Execute(Verbose)
    return False

def run_tool_ta_hydrology_upslope_height(DEM=None, HEIGHT=None, SLOPE=None, ASPECT=None, GRID_SYSTEM=None, FLOW_METHOD=None, Verbose=2):
    '''
    Upslope Height, Slope, Aspect
    ----------
    [ta_hydrology.upslope_height]\n
    This tool calculates the mean height, mean slope and mean aspect of the upslope contributing area.\n
    \n
    Arguments
    ----------
    - DEM [`input grid`] : Elevation
    - HEIGHT [`output grid`] : Upslope Height
    - SLOPE [`output grid`] : Slope
    - ASPECT [`output grid`] : Aspect
    - GRID_SYSTEM [`grid system`] : Grid System
    - FLOW_METHOD [`choice`] : Flow Distribution. Available Choices: [0] Deterministic 8 [1] Rho 8 [2] Braunschweiger Reliefmodell [3] Deterministic Infinity [4] Multiple Flow Direction [5] Multiple Triangular Flow Direction [6] Multiple Maximum Downslope Gradient Based Flow Direction Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_hydrology', 'upslope_height', 'Upslope Height, Slope, Aspect')
    if Tool.is_Okay():
        Tool.Set_Input ('DEM', DEM)
        Tool.Set_Output('HEIGHT', HEIGHT)
        Tool.Set_Output('SLOPE', SLOPE)
        Tool.Set_Output('ASPECT', ASPECT)
        Tool.Set_Option('GRID_SYSTEM', GRID_SYSTEM)
        Tool.Set_Option('FLOW_METHOD', FLOW_METHOD)
        return Tool.Execute(Verbose)
    return False

