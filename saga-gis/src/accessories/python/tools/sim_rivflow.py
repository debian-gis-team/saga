#! /usr/bin/env python

'''
Python Interface to SAGA Tools Library
----------
- Category : Simulation
- Name     : RivFlow
- ID       : sim_rivflow

Description
----------
RivFlow.
'''

from PySAGA.helper import Tool_Wrapper

def RiverBasin(INPUT=None, INPUT2=None, INPUT3=None, OUTPUT2=None, OUTPUT3=None, OUTPUT4=None, OUTPUT5=None, OUTPUT6=None, OUTPUT7=None, OUTPUT8=None, OUTPUT9=None, WCONS=None, WCONS2=None, PCR=None, NCR=None, ENFVMAX=None, VTRESH=None, Verbose=2):
    '''
    RiverBasin
    ----------
    [sim_rivflow.0]\n
    Parameters of RiverBasin\n
    Arguments
    ----------
    - INPUT [`input grid`] : DTM. Digitales Gelaendemodell des Flusseinzugsgebietes
    - INPUT2 [`input grid`] : HGGrid. Eingaberaster der Hauptgerinnerasterzellen des Flussnetzwerkes
    - INPUT3 [`optional input grid`] : statisches Entnahmeraster. Eingaberaster mit Angaben zur statischen Flaechenwasserentnahme.
    - OUTPUT2 [`output grid`] : Grad. Ausgabe der Abflussgradienten jeder Rasterzelle
    - OUTPUT3 [`output grid`] : Direc. Ausgabe der Abflussrichtung fuer jede Rasterzelle
    - OUTPUT4 [`output grid`] : HGGrad. Ausgabe der Abflussgradienten jeder Hauptgerinnerasterzelle
    - OUTPUT5 [`output grid`] : RivSpeed. Ausgabe Flussgeschwindigkeiten der Hauptgerinnerasterzellen (siehe Parametereinstellungen)
    - OUTPUT6 [`output grid`] : Coordinates. Ausgabe der Koordinatenwerte der Rasterzellen im Format xxxx.yyyy
    - OUTPUT7 [`output grid`] : BasinShare. Ausagbe der Rasterzellen des Flusseinzugsgebiets
    - OUTPUT8 [`output grid`] : statWUse. Ausagbe der anteiligen Flaechenwasserentnahme je Rasterzelle
    - OUTPUT9 [`output grid`] : NumInFlowCells. Ausgaberaster mit Angabe ueber die Anzahl der Rasterzellen, die in eine spezifische Rasterzelle (x,y) abflieszen
    - WCONS [`boolean`] : Anteilige Flaechenwasserentnahme. Default: 0 Wenn gesetzt, werden die Werte des statischen Entahmerasters anteilig entnommen und als Raster statWUse ausgegeben
    - WCONS2 [`choice`] : Dynamische Flaechenwassernutzung.... Available Choices: [0] ...anteilig aus den Flussrasterzellen [1] ...anteilig aus Rasterzellen der Teileinzugegebiete Default: 0 Auswahl der Art der anteiligen Flaechenwasserwasserentnahme.
    - PCR [`floating point number`] : Hauptgerinne-Parameter pHG. Minimum: 0.000000 Default: 0.003500 Parameter pHG zur Berechnung der Lagtime kHG des Hauptgerinneabflusses
    - NCR [`integer number`] : Hauptgerinne-Speicherkaskade nHG. Minimum: 1 Default: 1 Festlegen, wieviele Speicher die ChannelFlow River-Speicherkaskade enthaelt
    - ENFVMAX [`boolean`] : Maximal Geschwindigkeit des Hauptgerinnes beruecksichtigen. Default: 1 Angegebene Maximalgeschwindigkeit im Hauptgerinne bei der Berechnung der durchschnittlichen Flieszgeschwindigkeit des Hauptgerinnes beruecksichtigen.
    - VTRESH [`floating point number`] : Maximalgeschwindigkeit im Hauptgerinne in km/h. Minimum: 0.000000 Maximum: 10.000000 Default: 4.000000 Festlegung der maximalen Wasserflussgeschwindigkeit im Hauptgerinne in km/h (oberer Grenzwert)

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('sim_rivflow', '0', 'RiverBasin')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Input ('INPUT2', INPUT2)
        Tool.Set_Input ('INPUT3', INPUT3)
        Tool.Set_Output('OUTPUT2', OUTPUT2)
        Tool.Set_Output('OUTPUT3', OUTPUT3)
        Tool.Set_Output('OUTPUT4', OUTPUT4)
        Tool.Set_Output('OUTPUT5', OUTPUT5)
        Tool.Set_Output('OUTPUT6', OUTPUT6)
        Tool.Set_Output('OUTPUT7', OUTPUT7)
        Tool.Set_Output('OUTPUT8', OUTPUT8)
        Tool.Set_Output('OUTPUT9', OUTPUT9)
        Tool.Set_Option('WCons', WCONS)
        Tool.Set_Option('WCons2', WCONS2)
        Tool.Set_Option('pCr', PCR)
        Tool.Set_Option('nCr', NCR)
        Tool.Set_Option('EnfVmax', ENFVMAX)
        Tool.Set_Option('VTresh', VTRESH)
        return Tool.Execute(Verbose)
    return False

def run_tool_sim_rivflow_0(INPUT=None, INPUT2=None, INPUT3=None, OUTPUT2=None, OUTPUT3=None, OUTPUT4=None, OUTPUT5=None, OUTPUT6=None, OUTPUT7=None, OUTPUT8=None, OUTPUT9=None, WCONS=None, WCONS2=None, PCR=None, NCR=None, ENFVMAX=None, VTRESH=None, Verbose=2):
    '''
    RiverBasin
    ----------
    [sim_rivflow.0]\n
    Parameters of RiverBasin\n
    Arguments
    ----------
    - INPUT [`input grid`] : DTM. Digitales Gelaendemodell des Flusseinzugsgebietes
    - INPUT2 [`input grid`] : HGGrid. Eingaberaster der Hauptgerinnerasterzellen des Flussnetzwerkes
    - INPUT3 [`optional input grid`] : statisches Entnahmeraster. Eingaberaster mit Angaben zur statischen Flaechenwasserentnahme.
    - OUTPUT2 [`output grid`] : Grad. Ausgabe der Abflussgradienten jeder Rasterzelle
    - OUTPUT3 [`output grid`] : Direc. Ausgabe der Abflussrichtung fuer jede Rasterzelle
    - OUTPUT4 [`output grid`] : HGGrad. Ausgabe der Abflussgradienten jeder Hauptgerinnerasterzelle
    - OUTPUT5 [`output grid`] : RivSpeed. Ausgabe Flussgeschwindigkeiten der Hauptgerinnerasterzellen (siehe Parametereinstellungen)
    - OUTPUT6 [`output grid`] : Coordinates. Ausgabe der Koordinatenwerte der Rasterzellen im Format xxxx.yyyy
    - OUTPUT7 [`output grid`] : BasinShare. Ausagbe der Rasterzellen des Flusseinzugsgebiets
    - OUTPUT8 [`output grid`] : statWUse. Ausagbe der anteiligen Flaechenwasserentnahme je Rasterzelle
    - OUTPUT9 [`output grid`] : NumInFlowCells. Ausgaberaster mit Angabe ueber die Anzahl der Rasterzellen, die in eine spezifische Rasterzelle (x,y) abflieszen
    - WCONS [`boolean`] : Anteilige Flaechenwasserentnahme. Default: 0 Wenn gesetzt, werden die Werte des statischen Entahmerasters anteilig entnommen und als Raster statWUse ausgegeben
    - WCONS2 [`choice`] : Dynamische Flaechenwassernutzung.... Available Choices: [0] ...anteilig aus den Flussrasterzellen [1] ...anteilig aus Rasterzellen der Teileinzugegebiete Default: 0 Auswahl der Art der anteiligen Flaechenwasserwasserentnahme.
    - PCR [`floating point number`] : Hauptgerinne-Parameter pHG. Minimum: 0.000000 Default: 0.003500 Parameter pHG zur Berechnung der Lagtime kHG des Hauptgerinneabflusses
    - NCR [`integer number`] : Hauptgerinne-Speicherkaskade nHG. Minimum: 1 Default: 1 Festlegen, wieviele Speicher die ChannelFlow River-Speicherkaskade enthaelt
    - ENFVMAX [`boolean`] : Maximal Geschwindigkeit des Hauptgerinnes beruecksichtigen. Default: 1 Angegebene Maximalgeschwindigkeit im Hauptgerinne bei der Berechnung der durchschnittlichen Flieszgeschwindigkeit des Hauptgerinnes beruecksichtigen.
    - VTRESH [`floating point number`] : Maximalgeschwindigkeit im Hauptgerinne in km/h. Minimum: 0.000000 Maximum: 10.000000 Default: 4.000000 Festlegung der maximalen Wasserflussgeschwindigkeit im Hauptgerinne in km/h (oberer Grenzwert)

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('sim_rivflow', '0', 'RiverBasin')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Input ('INPUT2', INPUT2)
        Tool.Set_Input ('INPUT3', INPUT3)
        Tool.Set_Output('OUTPUT2', OUTPUT2)
        Tool.Set_Output('OUTPUT3', OUTPUT3)
        Tool.Set_Output('OUTPUT4', OUTPUT4)
        Tool.Set_Output('OUTPUT5', OUTPUT5)
        Tool.Set_Output('OUTPUT6', OUTPUT6)
        Tool.Set_Output('OUTPUT7', OUTPUT7)
        Tool.Set_Output('OUTPUT8', OUTPUT8)
        Tool.Set_Output('OUTPUT9', OUTPUT9)
        Tool.Set_Option('WCons', WCONS)
        Tool.Set_Option('WCons2', WCONS2)
        Tool.Set_Option('pCr', PCR)
        Tool.Set_Option('nCr', NCR)
        Tool.Set_Option('EnfVmax', ENFVMAX)
        Tool.Set_Option('VTresh', VTRESH)
        return Tool.Execute(Verbose)
    return False

def LandFlow_Version_10_build_351b(INPUT=None, INPUT2=None, INPUT10=None, INPUT8=None, INPUT9=None, INPUT11=None, INPUT12=None, INPUT3=None, INPUT5=None, INPUT6=None, INPUT7=None, OUTPUT=None, OUTPUT2=None, OUTPUT3=None, OUTPUT4=None, OUTPUT5=None, OUTPUT6=None, ONLYRB=None, TIMESTEP=None, CALCT=None, SYEAR=None, DAYNUM=None, FOLDER2=None, FOLDER1=None, STREDFACR=None, AUTOFACD=None, STREDFACD=None, OFFSETR=None, OFFSETD=None, CACHEUSE=None, FOLDER4=None, WNC=None, PARAMC=None, PARAMG=None, PARAMB=None, nG=None, RIVG=None, PARAMCR=None, NHG=None, ENFVMAX=None, VTRESH=None, WCONS=None, FOLDER3=None, WCONUNIT=None, WCONSD=None, WCONTHRES=None, STCONSALL=None, STCONSRIV=None, VRM1=None, RM1X=None, RM1Y=None, RM1Q=None, RM1A=None, VRM2=None, RM2X=None, RM2Y=None, RM2Q=None, RM2A=None, EVP1S=None, EVP1X=None, EVP1Y=None, EVP2S=None, EVP2X=None, EVP2Y=None, EVP3S=None, EVP3X=None, EVP3Y=None, RBMX=None, RBMY=None, wP=None, eP=None, MONILOG1=None, MONILOG3=None, MONILOG2=None, TEST1=None, XT1=None, YT1=None, Verbose=2):
    '''
    LandFlow Version 1.0 (build 3.5.1b)
    ----------
    [sim_rivflow.1]\n
    Landflow of RiverBasine\n
    Arguments
    ----------
    - INPUT [`input grid`] : Gelaendemodell (DTM). Digitales Gelaendemodell des Flusseinzugsgebietes
    - INPUT2 [`input grid`] : Hoehengradienten (Grad). Abflussgradienten jeder Rasterzelle des Flusseinzugsgebietes
    - INPUT10 [`input grid`] : Flusseinzugsgebiet (BasinShare). Abgrenzung des Flusseinzugsgebiets mit den Teileinzugsgebieten des Flussnetzwerkes
    - INPUT8 [`input grid`] : HG-Raster (HGGrid). Hauptgerinnerasterzellen
    - INPUT9 [`input grid`] : HG-Hoehengradienten (HGGrad). Abflussgradienten der Hauptgerinnerasterzellen
    - INPUT11 [`optional input grid`] : Referenzverlauf Flussbilanz. Raster des Referenzverlauf des (linearen) Hauptflussarmes, der zur Erstellung der Flusswasserbilanz herangezogen werden soll
    - INPUT12 [`optional input grid`] : Zuflussrasterzellen (NumInFlowCells). Raster mit Angabe ueber die Anzahl der Rasterzellen, die in eine spezifische Rasterzelle (x,y) abflieszen
    - INPUT3 [`optional input grid`] : statische Wassserentnahme (statWUse). Flaechenbasierte statische Wassserentnahme pro RiverGridbox in. ACHTUNG: Funktioniert ggf. nur bei bereits initiierten Systemen bzw. Vorlauf
    - INPUT5 [`optional input grid`] : Fluss-Speicher einlesen. Simulation mit spezifischen Gerinne- bzw. Hauptgerinne-Speicherwerten (ChannelFlow) initiieren
    - INPUT6 [`optional input grid`] : Oberflaechenabfluss-Speicher einlesen. Simulation mit spezifischen Oberflaechenabfluss-Speicherwerten (OverlandFlow) initiieren
    - INPUT7 [`optional input grid`] : Grundwasserabfluss-Speicher einlesen. Simulation mit spezifischen Grundwasserabfluss-Speicherwerten (BaseFlow) initiieren
    - OUTPUT [`output grid`] : Fluss-Speicher ausgeben. Fluss-Speicher 'CFCache' (ChannelFlowCache) am Ende der Simulation ausgeben - Initialisierungsdaten)
    - OUTPUT2 [`output grid`] : Oberflaechenabfluss-Speicher ausgeben. Oberflaechenabfluss-Speicher 'OFCache' (OverlandFlowCache) am Ende der Simulation ausgeben - Initialisierungsdaten
    - OUTPUT3 [`output grid`] : Grundwasserabfluss-Speicher ausgeben. Grundwasserabfluss-Speicher 'BFCache' (BaseFlowCache) am Ende der Simulation ausgeben - Initialisierungsdaten
    - OUTPUT4 [`output grid`] : Wasserflussvolumen. Wasserflussvolumen in m3/s
    - OUTPUT5 [`output grid`] : SumRunoffDrainage. Aufsummieren der vertikalen Runoff und Drainage Zufluesse fuer jede Rasterzelle ueber den gesamten Simulationsverlauf
    - OUTPUT6 [`output grid`] : DynWUse. Dynamisch eingelesene Flaechenwasssernutzung pro Rasterzelle. ACHTUNG: Funktioniert nur bei bereits initiierten Systemen bzw. Vorlauf
    - ONLYRB [`boolean`] : Berechnung nur im Flusseinzugsgebiet. Default: 1 Der Abfluss wird nur ueber die Rasterzellen des Flusseinzugsgebietes berechnet
    - TIMESTEP [`integer number`] : Zeitschrittdauer [s]. Minimum: 1 Default: 1 Dauer eines Simulations-Zeitschritts in Sekunden
    - CALCT [`boolean`] : automatisierter max. Zeitschritt. Default: 1 Automatisierte Berechnung der maximal moeglichen Dauer eines Zeitschrittes in [s] - Zeitschrittdauern wird ignoriert.
    - SYEAR [`integer number`] : Startjahr. Minimum: 1979 Maximum: 2009 Default: 1999 Jahr in dem die Simulation zum 1. Januar gestartet wird
    - DAYNUM [`integer number`] : Anzahl der Simulationstage. Minimum: 1 Maximum: 7670 Default: 365 Anzahl der gesamten Simulationtage ueber die berechnete wird
    - FOLDER2 [`file path`] : Speicherordner. Ordern in den alle Ausgabedaten gespeichert werden sollen
    - FOLDER1 [`file path`] : Pfad LS-Daten. Ordnerpfad der Surface Runoff- und Drainagerasterdaten des Landoberflaechenschemas
    - STREDFACR [`floating point number`] : Reduzierungsfaktor Surface Runoff [% / 100]. Minimum: 0.000000 Maximum: 1.000000 Default: 0.000000 Statische Reduzierung des vertikalen Zuflusses 'Surface Runoff' in [% / 100] pro Zeitschritt.
    - AUTOFACD [`boolean`] : Ausgleich der Surface Runoff Reduktion. Default: 0 Erhoeht automatisch die Drainage um denjenigen Volumenbetrag, um den der Surface Runoff reduziert wurde. Wenn gesetzt, wird etwaige Reduzierung der Drainage NICHT beruecksichtigt!
    - STREDFACD [`floating point number`] : Reduzierungsfaktor Drainage [% / 100]. Minimum: 0.000000 Maximum: 1.000000 Default: 0.000000 Statische Reduzierung des vertikalen Zuflusses der 'Drainage' in [% / 100] pro Zeitschritt.
    - OFFSETR [`floating point number`] : Offsetwert Surface Runoff [m3/s]. Minimum: -1.000000 Maximum: 1.000000 Default: 0.000000 Statischer Offsetwert auf den vertikalen Zufluss 'Surface Runoff' in [m3/s] je Rasterzelle. ACHTUNG wird auf jede Rasterzelle angewendet, daher limitiert auf +/-1 m3/s. Bei Wahl des Wertes Aufloesung beachten! 
    - OFFSETD [`floating point number`] : Offsetwert Drainage [m3/s]. Minimum: -1.000000 Maximum: 1.000000 Default: 0.000000 Statischer Offsetwert auf den vertikalen Zufluss 'Drainage' in [m3/s] je Rasterzelle. ACHTUNG wird auf jede Rasterzelle angewendet, daher limitiert auf +/-1 m3/s. Bei Wahl des Wertes Aufloesung beachten!
    - CACHEUSE [`boolean`] : Initiierung mit vorhandenen Speichern. Default: 0 Initialisierung der Simulation mit spezifischen Speicherwerten (Raster und NCache)
    - FOLDER4 [`file path`] : NCache einlesen. Einlesen der NCache Textdatei
    - WNC [`boolean`] : Ausgabe NCache. Default: 0 Schreibt die Werte der Speicherkaskaden zum Ende Simulation in eine Textdatei (Initialisierungsdaten)
    - PARAMC [`floating point number`] : Gerinne-Parameter cG. Minimum: 0.000000 Default: 0.060000 Parameter cG zur Berechnung der Lagtime kG des Gerinneabflusses
    - PARAMG [`floating point number`] : Oberflaechen-Parameter cO. Minimum: 0.000000 Default: 0.357000 Parameter cO zur Berechnung der Lagtime kO des Oberflaechenabflusses
    - PARAMB [`floating point number`] : Grundwasser-Parameter pB. Minimum: 0.000000 Default: 300.000000 Parameter pB zur Berechnung der Lagtime kB des Grundwasserabflusses
    - nG [`integer number`] : Gerinne-Speicherkaskade nG. Minimum: 1 Default: 3 Festlegen, wieviele Speicher die Gerinne-Speicherkaskade nG enthaelt
    - RIVG [`choice`] : Beruecksichtigung der Hauptgerinnerasterzellen?. Available Choices: [0] nein [1] ja; bestimmen anhand eines Hauptgerinnerasters (HG Raster) Default: 1 Seperate Beruecksichtigung und Berechnung ueber Hauptgerinnerasterzellen - neben den normalen Gerinnerasterzellen.
    - PARAMCR [`floating point number`] : Hauptgerinne-Parameter cHG. Minimum: 0.000000 Maximum: 1.000000 Default: 0.007000 Parameter cHG zur Berechnung der Lagtime kHG des ChannelFlow-River [optional siehe oben]
    - NHG [`integer number`] : Hauptgerinne-Speicherkaskade nHG. Minimum: 1 Default: 1 Festlegen, wieviele Speicher die Hauptgerinne-Speicherkaskade enthaelt  [optional siehe oben]
    - ENFVMAX [`boolean`] : Abflussgeschwindigkeit begrenzen. Default: 1 Die mittlere Wasserabflussgeschwindigkeit wird auf einen Hoechstwert begrenzt - Zeitschrittvorgaben und automatisierter Zeitschritt wird ueberschrieben.
    - VTRESH [`floating point number`] : Oberer Abflussgeschwindigkeitsgrenzwert [km/h]. Minimum: 0.000000 Maximum: 10.000000 Default: 4.000000 Maximale mittlere Wasserabflussgeschwindigkeit in km/h
    - WCONS [`choice`] : Dynamische Flaechenwassernutzung.... Available Choices: [0] keine [1] ...anteilig aus den Hauptgerinnerasterzellen [2] ...anteilig aus Rasterzellen der Teileinzugegebiete [3] ...genau den entsprechenden Wert aus der jeweiligen Rasterzelle entnehmen Default: 0 Auswahl der Art der dynamischen Flaechenwasserwassernutzung (WUse). ACHTUNG: Funktioniert ggf. nur bei bereits initiierten Systemen bzw. nach Vorlauf
    - FOLDER3 [`file path`] : Ordnerpfad der WUse Daten. Speicherpfad der dynamischen Wassernutzungsdaten
    - WCONUNIT [`choice`] : WUse Einheit. Available Choices: [0] m3/s [1] m3/Monat Default: 0 Einheit in der die WUse Daten vorliegen
    - WCONSD [`integer number`] : Vorlauftage ohne Entnahmen. Minimum: 0 Default: 0 Anzahl der Simulationestage bevor eine Wasserentnahme beruecksichtigt wird
    - WCONTHRES [`floating point number`] : Abflussschwellenwert [m3/s]. Minimum: 0.000000 Default: 0.000000 Fester Abflussschwellenwert in m3/s fuer das Hauptgerinne, der durch Entnahmen nicht unterschritten werden soll.
    - STCONSALL [`floating point number`] : Abflussreduzierungsfaktor Gerinnerasterzellen [% / 100]. Minimum: 0.000000 Maximum: 1.000000 Default: 0.000000 Statische Reduzierung des Gesamtabflusses in den Gerinnerasterzellen um [% / 100] pro Zeitschritt.
    - STCONSRIV [`floating point number`] : Abflussreduzierungsfaktor HG-Rasterzellen [% / 100]. Minimum: 0.000000 Maximum: 1.000000 Default: 0.000000 Statische Reduzierung des Gesamtabflusses  in den Hauptgerinnegitterboxen um [% / 100] pro Zeitschritt.
    - VRM1 [`choice`] : Abflussmanipulation Rasterzelle 1... . Available Choices: [0] nein [1] Ja, berechneten Abfluss veraendern: res. Abfluss = berechn. Abfluss * q + a [2] Ja, Abfluss manuell vorgeben: res. Abfluss = Speicherinhalt * q + a Default: 0 Der Gerinne- bzw. Hauptgerinneabfluss kann mit dieser Methode fuer Rasterzelle 1 manipuliert werden. 
    - RM1X [`integer number`] : Rasterzelle 1 [x-Koord.]. Minimum: -1 Default: -1 x-Koordinate der Rasterzelle 1, fuer die der Abfluss manipulatiert werden soll
    - RM1Y [`integer number`] : Rasterzelle 1 [y-Koord.]. Minimum: -1 Default: -1 y-Koordinate der Rasterzelle 1, fuer die der Abfluss manipulatiert werden soll
    - RM1Q [`floating point number`] : q [%/100]. Minimum: 0.000000 Default: 1.000000 Prozentualer Faktor q [%/100] fuer Rasterzelle 1
    - RM1A [`floating point number`] : a [+-m3/s]. Default: 0.000000 Addidativer Offsetwert a [+-m3/s] fuer Rasterzelle 1
    - VRM2 [`choice`] : Abflussmanipulation Rasterzelle 2... . Available Choices: [0] nein [1] Ja, berechneten Abfluss veraendern: res. Abfluss = berechn. Abfluss * q + a [2] Ja, Abfluss manuell vorgeben: res. Abfluss = Speicherinhalt * q + a Default: 0 Der Gerinne- bzw. Hauptgerinneabfluss kann mit dieser Methode fuer Rasterzelle 2 manipuliert werden. 
    - RM2X [`integer number`] : Rasterzelle 2 [x-Koord.]. Minimum: -1 Default: -1 x-Koordinate der Rasterzelle 2, fuer die der Abfluss manipulatiert werden soll
    - RM2Y [`integer number`] : Rasterzelle 2 [y-Koord.]. Minimum: -1 Default: -1 y-Koordinate der Rasterzelle 2, fuer die der Abfluss manipulatiert werden soll
    - RM2Q [`floating point number`] : q [%/100]. Minimum: 0.000000 Default: 1.000000 Prozentualer Faktor q [%/100] fuer Rasterzelle 2
    - RM2A [`floating point number`] : a [+-m3/s]. Default: 0.000000 Addidativer Offsetwert a [+-m3/s] fuer Rasterzelle 2
    - EVP1S [`text`] : EvP1 Name. Default: NeuDarchau.txt Name des Evaluierungspunktes 1
    - EVP1X [`integer number`] : EvP1 Rasterzelle [x-Koord.]. Minimum: 0 Default: 30 x-Koordinate der spezifischen Hauptgerinnerasterzelle des Evaluerungspunktes 1
    - EVP1Y [`integer number`] : EvP1 Rasterzelle [y-Koord.]. Minimum: 0 Default: 115 y-Koordinate der spezifischen Hauptgerinnerasterzelle des Evaluerungspunktes 1
    - EVP2S [`text`] : EvP2 Name. Default: Lutherstadt-Wittenberg.txt Name des Evaluierungspunktes 2
    - EVP2X [`integer number`] : EvP2 Rasterzelle [x-Koord.]. Minimum: 0 Default: 54 x-Koordinate der spezifischen Hauptgerinnerasterzelle des Evaluerungspunktes 2
    - EVP2Y [`integer number`] : EvP2 Rasterzelle [y-Koord.]. Minimum: 0 Default: 85 y-Koordinate der spezifischen Hauptgerinnerasterzelle des Evaluerungspunktes 2
    - EVP3S [`text`] : EvP3 Name. Default: Schoena.txt Name des Evaluierungspunktes 3
    - EVP3X [`integer number`] : EvP3 Rasterzelle [x-Koord.]. Minimum: 0 Default: 78 x-Koordinate der spezifischen Hauptgerinnerasterzelle des Evaluerungspunktes 3
    - EVP3Y [`integer number`] : EvP3 Rasterzelle [y-Koord.]. Minimum: 0 Default: 65 y-Koordinate der spezifischen Hauptgerinnerasterzelle des Evaluerungspunktes 3
    - RBMX [`integer number`] : Ausgangsrasterzelle [x-Koord.]. Minimum: 0 Default: 16 x-Koordinate der Ausgangsrasterzelle des Flusseinzugsgebietes
    - RBMY [`integer number`] : Ausgangsrasterzelle [y-Koord.]. Minimum: 0 Default: 121 y-Koordinate der Ausgangsrasterzelle des Flusseinzugsgebietes
    - wP [`boolean`] : SimParameters. Default: 1 Schreibt wichtige, der Simulation zugrundeliegenden Parameter in eine Textdatei
    - eP [`boolean`] : Fehlerprotokoll. Default: 1 Schreibt Fehler in Textfile
    - MONILOG1 [`boolean`] : RiverBasinDay-Monitoring. Default: 1 Monitoring taegicher Werte des Flusseinzugsgebiets
    - MONILOG3 [`boolean`] : RiverBasinMonth-Monitoring. Default: 1 Monitoring monatlicher Werte des Flusseinzugsgebiets
    - MONILOG2 [`boolean`] : WSystem-Monitoring. Default: 1 Monitoring von Zu- und Abflusswerten des WasserGesamtsystems
    - TEST1 [`choice`] : Testroutine1 durchfuehren... . Available Choices: [0] nein [1] Ja, TestRoutine1 nur fuer Teileinzugsgbiet der HG-Rasterzelle [2] Ja, TestRoutine1 fuer Flusseinzugsgebiet bis zu der HG-Rasterzelle Default: 0 Waehlen ob TestRoutine 1 durchgefuehrt werden soll... 1) nur fuer Teileinzugsgebiet der HG-Rasterzelle oder 2) fuer das Flusseinzugsgebiet bis zum Erreichen der HG-Rasterzelle.
    - XT1 [`integer number`] : Hauptgerinnerasterzelle [x-Koord.]. Minimum: 0 Default: 0 x-Koordinate der spezifischen Hauptgerinnerasterzelle fuer TestRoutine 1
    - YT1 [`integer number`] : Hauptgerinnerasterzelle [y-Koord.]. Minimum: 0 Default: 0 y-Koordinate der spezifischen Hauptgerinnerasterzelle fuer TestRoutine 1

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('sim_rivflow', '1', 'LandFlow Version 1.0 (build 3.5.1b)')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Input ('INPUT2', INPUT2)
        Tool.Set_Input ('INPUT10', INPUT10)
        Tool.Set_Input ('INPUT8', INPUT8)
        Tool.Set_Input ('INPUT9', INPUT9)
        Tool.Set_Input ('INPUT11', INPUT11)
        Tool.Set_Input ('INPUT12', INPUT12)
        Tool.Set_Input ('INPUT3', INPUT3)
        Tool.Set_Input ('INPUT5', INPUT5)
        Tool.Set_Input ('INPUT6', INPUT6)
        Tool.Set_Input ('INPUT7', INPUT7)
        Tool.Set_Output('OUTPUT', OUTPUT)
        Tool.Set_Output('OUTPUT2', OUTPUT2)
        Tool.Set_Output('OUTPUT3', OUTPUT3)
        Tool.Set_Output('OUTPUT4', OUTPUT4)
        Tool.Set_Output('OUTPUT5', OUTPUT5)
        Tool.Set_Output('OUTPUT6', OUTPUT6)
        Tool.Set_Option('onlyRB', ONLYRB)
        Tool.Set_Option('TimeStep', TIMESTEP)
        Tool.Set_Option('CalcT', CALCT)
        Tool.Set_Option('sYear', SYEAR)
        Tool.Set_Option('DayNum', DAYNUM)
        Tool.Set_Option('Folder2', FOLDER2)
        Tool.Set_Option('Folder1', FOLDER1)
        Tool.Set_Option('stRedFacR', STREDFACR)
        Tool.Set_Option('autoFacD', AUTOFACD)
        Tool.Set_Option('stRedFacD', STREDFACD)
        Tool.Set_Option('OffsetR', OFFSETR)
        Tool.Set_Option('OffsetD', OFFSETD)
        Tool.Set_Option('CacheUse', CACHEUSE)
        Tool.Set_Option('Folder4', FOLDER4)
        Tool.Set_Option('wNC', WNC)
        Tool.Set_Option('ParamC', PARAMC)
        Tool.Set_Option('ParamG', PARAMG)
        Tool.Set_Option('ParamB', PARAMB)
        Tool.Set_Option('nG', nG)
        Tool.Set_Option('RivG', RIVG)
        Tool.Set_Option('ParamCr', PARAMCR)
        Tool.Set_Option('nHG', NHG)
        Tool.Set_Option('EnfVmax', ENFVMAX)
        Tool.Set_Option('VTresh', VTRESH)
        Tool.Set_Option('WCons', WCONS)
        Tool.Set_Option('Folder3', FOLDER3)
        Tool.Set_Option('WConUnit', WCONUNIT)
        Tool.Set_Option('WConsD', WCONSD)
        Tool.Set_Option('WConThres', WCONTHRES)
        Tool.Set_Option('stConsAll', STCONSALL)
        Tool.Set_Option('stConsRiv', STCONSRIV)
        Tool.Set_Option('vRM1', VRM1)
        Tool.Set_Option('RM1x', RM1X)
        Tool.Set_Option('RM1y', RM1Y)
        Tool.Set_Option('RM1q', RM1Q)
        Tool.Set_Option('RM1a', RM1A)
        Tool.Set_Option('vRM2', VRM2)
        Tool.Set_Option('RM2x', RM2X)
        Tool.Set_Option('RM2y', RM2Y)
        Tool.Set_Option('RM2q', RM2Q)
        Tool.Set_Option('RM2a', RM2A)
        Tool.Set_Option('EvP1s', EVP1S)
        Tool.Set_Option('EvP1x', EVP1X)
        Tool.Set_Option('EvP1y', EVP1Y)
        Tool.Set_Option('EvP2s', EVP2S)
        Tool.Set_Option('EvP2x', EVP2X)
        Tool.Set_Option('EvP2y', EVP2Y)
        Tool.Set_Option('EvP3s', EVP3S)
        Tool.Set_Option('EvP3x', EVP3X)
        Tool.Set_Option('EvP3y', EVP3Y)
        Tool.Set_Option('RBMx', RBMX)
        Tool.Set_Option('RBMy', RBMY)
        Tool.Set_Option('wP', wP)
        Tool.Set_Option('eP', eP)
        Tool.Set_Option('MoniLog1', MONILOG1)
        Tool.Set_Option('MoniLog3', MONILOG3)
        Tool.Set_Option('MoniLog2', MONILOG2)
        Tool.Set_Option('Test1', TEST1)
        Tool.Set_Option('xt1', XT1)
        Tool.Set_Option('yt1', YT1)
        return Tool.Execute(Verbose)
    return False

def run_tool_sim_rivflow_1(INPUT=None, INPUT2=None, INPUT10=None, INPUT8=None, INPUT9=None, INPUT11=None, INPUT12=None, INPUT3=None, INPUT5=None, INPUT6=None, INPUT7=None, OUTPUT=None, OUTPUT2=None, OUTPUT3=None, OUTPUT4=None, OUTPUT5=None, OUTPUT6=None, ONLYRB=None, TIMESTEP=None, CALCT=None, SYEAR=None, DAYNUM=None, FOLDER2=None, FOLDER1=None, STREDFACR=None, AUTOFACD=None, STREDFACD=None, OFFSETR=None, OFFSETD=None, CACHEUSE=None, FOLDER4=None, WNC=None, PARAMC=None, PARAMG=None, PARAMB=None, nG=None, RIVG=None, PARAMCR=None, NHG=None, ENFVMAX=None, VTRESH=None, WCONS=None, FOLDER3=None, WCONUNIT=None, WCONSD=None, WCONTHRES=None, STCONSALL=None, STCONSRIV=None, VRM1=None, RM1X=None, RM1Y=None, RM1Q=None, RM1A=None, VRM2=None, RM2X=None, RM2Y=None, RM2Q=None, RM2A=None, EVP1S=None, EVP1X=None, EVP1Y=None, EVP2S=None, EVP2X=None, EVP2Y=None, EVP3S=None, EVP3X=None, EVP3Y=None, RBMX=None, RBMY=None, wP=None, eP=None, MONILOG1=None, MONILOG3=None, MONILOG2=None, TEST1=None, XT1=None, YT1=None, Verbose=2):
    '''
    LandFlow Version 1.0 (build 3.5.1b)
    ----------
    [sim_rivflow.1]\n
    Landflow of RiverBasine\n
    Arguments
    ----------
    - INPUT [`input grid`] : Gelaendemodell (DTM). Digitales Gelaendemodell des Flusseinzugsgebietes
    - INPUT2 [`input grid`] : Hoehengradienten (Grad). Abflussgradienten jeder Rasterzelle des Flusseinzugsgebietes
    - INPUT10 [`input grid`] : Flusseinzugsgebiet (BasinShare). Abgrenzung des Flusseinzugsgebiets mit den Teileinzugsgebieten des Flussnetzwerkes
    - INPUT8 [`input grid`] : HG-Raster (HGGrid). Hauptgerinnerasterzellen
    - INPUT9 [`input grid`] : HG-Hoehengradienten (HGGrad). Abflussgradienten der Hauptgerinnerasterzellen
    - INPUT11 [`optional input grid`] : Referenzverlauf Flussbilanz. Raster des Referenzverlauf des (linearen) Hauptflussarmes, der zur Erstellung der Flusswasserbilanz herangezogen werden soll
    - INPUT12 [`optional input grid`] : Zuflussrasterzellen (NumInFlowCells). Raster mit Angabe ueber die Anzahl der Rasterzellen, die in eine spezifische Rasterzelle (x,y) abflieszen
    - INPUT3 [`optional input grid`] : statische Wassserentnahme (statWUse). Flaechenbasierte statische Wassserentnahme pro RiverGridbox in. ACHTUNG: Funktioniert ggf. nur bei bereits initiierten Systemen bzw. Vorlauf
    - INPUT5 [`optional input grid`] : Fluss-Speicher einlesen. Simulation mit spezifischen Gerinne- bzw. Hauptgerinne-Speicherwerten (ChannelFlow) initiieren
    - INPUT6 [`optional input grid`] : Oberflaechenabfluss-Speicher einlesen. Simulation mit spezifischen Oberflaechenabfluss-Speicherwerten (OverlandFlow) initiieren
    - INPUT7 [`optional input grid`] : Grundwasserabfluss-Speicher einlesen. Simulation mit spezifischen Grundwasserabfluss-Speicherwerten (BaseFlow) initiieren
    - OUTPUT [`output grid`] : Fluss-Speicher ausgeben. Fluss-Speicher 'CFCache' (ChannelFlowCache) am Ende der Simulation ausgeben - Initialisierungsdaten)
    - OUTPUT2 [`output grid`] : Oberflaechenabfluss-Speicher ausgeben. Oberflaechenabfluss-Speicher 'OFCache' (OverlandFlowCache) am Ende der Simulation ausgeben - Initialisierungsdaten
    - OUTPUT3 [`output grid`] : Grundwasserabfluss-Speicher ausgeben. Grundwasserabfluss-Speicher 'BFCache' (BaseFlowCache) am Ende der Simulation ausgeben - Initialisierungsdaten
    - OUTPUT4 [`output grid`] : Wasserflussvolumen. Wasserflussvolumen in m3/s
    - OUTPUT5 [`output grid`] : SumRunoffDrainage. Aufsummieren der vertikalen Runoff und Drainage Zufluesse fuer jede Rasterzelle ueber den gesamten Simulationsverlauf
    - OUTPUT6 [`output grid`] : DynWUse. Dynamisch eingelesene Flaechenwasssernutzung pro Rasterzelle. ACHTUNG: Funktioniert nur bei bereits initiierten Systemen bzw. Vorlauf
    - ONLYRB [`boolean`] : Berechnung nur im Flusseinzugsgebiet. Default: 1 Der Abfluss wird nur ueber die Rasterzellen des Flusseinzugsgebietes berechnet
    - TIMESTEP [`integer number`] : Zeitschrittdauer [s]. Minimum: 1 Default: 1 Dauer eines Simulations-Zeitschritts in Sekunden
    - CALCT [`boolean`] : automatisierter max. Zeitschritt. Default: 1 Automatisierte Berechnung der maximal moeglichen Dauer eines Zeitschrittes in [s] - Zeitschrittdauern wird ignoriert.
    - SYEAR [`integer number`] : Startjahr. Minimum: 1979 Maximum: 2009 Default: 1999 Jahr in dem die Simulation zum 1. Januar gestartet wird
    - DAYNUM [`integer number`] : Anzahl der Simulationstage. Minimum: 1 Maximum: 7670 Default: 365 Anzahl der gesamten Simulationtage ueber die berechnete wird
    - FOLDER2 [`file path`] : Speicherordner. Ordern in den alle Ausgabedaten gespeichert werden sollen
    - FOLDER1 [`file path`] : Pfad LS-Daten. Ordnerpfad der Surface Runoff- und Drainagerasterdaten des Landoberflaechenschemas
    - STREDFACR [`floating point number`] : Reduzierungsfaktor Surface Runoff [% / 100]. Minimum: 0.000000 Maximum: 1.000000 Default: 0.000000 Statische Reduzierung des vertikalen Zuflusses 'Surface Runoff' in [% / 100] pro Zeitschritt.
    - AUTOFACD [`boolean`] : Ausgleich der Surface Runoff Reduktion. Default: 0 Erhoeht automatisch die Drainage um denjenigen Volumenbetrag, um den der Surface Runoff reduziert wurde. Wenn gesetzt, wird etwaige Reduzierung der Drainage NICHT beruecksichtigt!
    - STREDFACD [`floating point number`] : Reduzierungsfaktor Drainage [% / 100]. Minimum: 0.000000 Maximum: 1.000000 Default: 0.000000 Statische Reduzierung des vertikalen Zuflusses der 'Drainage' in [% / 100] pro Zeitschritt.
    - OFFSETR [`floating point number`] : Offsetwert Surface Runoff [m3/s]. Minimum: -1.000000 Maximum: 1.000000 Default: 0.000000 Statischer Offsetwert auf den vertikalen Zufluss 'Surface Runoff' in [m3/s] je Rasterzelle. ACHTUNG wird auf jede Rasterzelle angewendet, daher limitiert auf +/-1 m3/s. Bei Wahl des Wertes Aufloesung beachten! 
    - OFFSETD [`floating point number`] : Offsetwert Drainage [m3/s]. Minimum: -1.000000 Maximum: 1.000000 Default: 0.000000 Statischer Offsetwert auf den vertikalen Zufluss 'Drainage' in [m3/s] je Rasterzelle. ACHTUNG wird auf jede Rasterzelle angewendet, daher limitiert auf +/-1 m3/s. Bei Wahl des Wertes Aufloesung beachten!
    - CACHEUSE [`boolean`] : Initiierung mit vorhandenen Speichern. Default: 0 Initialisierung der Simulation mit spezifischen Speicherwerten (Raster und NCache)
    - FOLDER4 [`file path`] : NCache einlesen. Einlesen der NCache Textdatei
    - WNC [`boolean`] : Ausgabe NCache. Default: 0 Schreibt die Werte der Speicherkaskaden zum Ende Simulation in eine Textdatei (Initialisierungsdaten)
    - PARAMC [`floating point number`] : Gerinne-Parameter cG. Minimum: 0.000000 Default: 0.060000 Parameter cG zur Berechnung der Lagtime kG des Gerinneabflusses
    - PARAMG [`floating point number`] : Oberflaechen-Parameter cO. Minimum: 0.000000 Default: 0.357000 Parameter cO zur Berechnung der Lagtime kO des Oberflaechenabflusses
    - PARAMB [`floating point number`] : Grundwasser-Parameter pB. Minimum: 0.000000 Default: 300.000000 Parameter pB zur Berechnung der Lagtime kB des Grundwasserabflusses
    - nG [`integer number`] : Gerinne-Speicherkaskade nG. Minimum: 1 Default: 3 Festlegen, wieviele Speicher die Gerinne-Speicherkaskade nG enthaelt
    - RIVG [`choice`] : Beruecksichtigung der Hauptgerinnerasterzellen?. Available Choices: [0] nein [1] ja; bestimmen anhand eines Hauptgerinnerasters (HG Raster) Default: 1 Seperate Beruecksichtigung und Berechnung ueber Hauptgerinnerasterzellen - neben den normalen Gerinnerasterzellen.
    - PARAMCR [`floating point number`] : Hauptgerinne-Parameter cHG. Minimum: 0.000000 Maximum: 1.000000 Default: 0.007000 Parameter cHG zur Berechnung der Lagtime kHG des ChannelFlow-River [optional siehe oben]
    - NHG [`integer number`] : Hauptgerinne-Speicherkaskade nHG. Minimum: 1 Default: 1 Festlegen, wieviele Speicher die Hauptgerinne-Speicherkaskade enthaelt  [optional siehe oben]
    - ENFVMAX [`boolean`] : Abflussgeschwindigkeit begrenzen. Default: 1 Die mittlere Wasserabflussgeschwindigkeit wird auf einen Hoechstwert begrenzt - Zeitschrittvorgaben und automatisierter Zeitschritt wird ueberschrieben.
    - VTRESH [`floating point number`] : Oberer Abflussgeschwindigkeitsgrenzwert [km/h]. Minimum: 0.000000 Maximum: 10.000000 Default: 4.000000 Maximale mittlere Wasserabflussgeschwindigkeit in km/h
    - WCONS [`choice`] : Dynamische Flaechenwassernutzung.... Available Choices: [0] keine [1] ...anteilig aus den Hauptgerinnerasterzellen [2] ...anteilig aus Rasterzellen der Teileinzugegebiete [3] ...genau den entsprechenden Wert aus der jeweiligen Rasterzelle entnehmen Default: 0 Auswahl der Art der dynamischen Flaechenwasserwassernutzung (WUse). ACHTUNG: Funktioniert ggf. nur bei bereits initiierten Systemen bzw. nach Vorlauf
    - FOLDER3 [`file path`] : Ordnerpfad der WUse Daten. Speicherpfad der dynamischen Wassernutzungsdaten
    - WCONUNIT [`choice`] : WUse Einheit. Available Choices: [0] m3/s [1] m3/Monat Default: 0 Einheit in der die WUse Daten vorliegen
    - WCONSD [`integer number`] : Vorlauftage ohne Entnahmen. Minimum: 0 Default: 0 Anzahl der Simulationestage bevor eine Wasserentnahme beruecksichtigt wird
    - WCONTHRES [`floating point number`] : Abflussschwellenwert [m3/s]. Minimum: 0.000000 Default: 0.000000 Fester Abflussschwellenwert in m3/s fuer das Hauptgerinne, der durch Entnahmen nicht unterschritten werden soll.
    - STCONSALL [`floating point number`] : Abflussreduzierungsfaktor Gerinnerasterzellen [% / 100]. Minimum: 0.000000 Maximum: 1.000000 Default: 0.000000 Statische Reduzierung des Gesamtabflusses in den Gerinnerasterzellen um [% / 100] pro Zeitschritt.
    - STCONSRIV [`floating point number`] : Abflussreduzierungsfaktor HG-Rasterzellen [% / 100]. Minimum: 0.000000 Maximum: 1.000000 Default: 0.000000 Statische Reduzierung des Gesamtabflusses  in den Hauptgerinnegitterboxen um [% / 100] pro Zeitschritt.
    - VRM1 [`choice`] : Abflussmanipulation Rasterzelle 1... . Available Choices: [0] nein [1] Ja, berechneten Abfluss veraendern: res. Abfluss = berechn. Abfluss * q + a [2] Ja, Abfluss manuell vorgeben: res. Abfluss = Speicherinhalt * q + a Default: 0 Der Gerinne- bzw. Hauptgerinneabfluss kann mit dieser Methode fuer Rasterzelle 1 manipuliert werden. 
    - RM1X [`integer number`] : Rasterzelle 1 [x-Koord.]. Minimum: -1 Default: -1 x-Koordinate der Rasterzelle 1, fuer die der Abfluss manipulatiert werden soll
    - RM1Y [`integer number`] : Rasterzelle 1 [y-Koord.]. Minimum: -1 Default: -1 y-Koordinate der Rasterzelle 1, fuer die der Abfluss manipulatiert werden soll
    - RM1Q [`floating point number`] : q [%/100]. Minimum: 0.000000 Default: 1.000000 Prozentualer Faktor q [%/100] fuer Rasterzelle 1
    - RM1A [`floating point number`] : a [+-m3/s]. Default: 0.000000 Addidativer Offsetwert a [+-m3/s] fuer Rasterzelle 1
    - VRM2 [`choice`] : Abflussmanipulation Rasterzelle 2... . Available Choices: [0] nein [1] Ja, berechneten Abfluss veraendern: res. Abfluss = berechn. Abfluss * q + a [2] Ja, Abfluss manuell vorgeben: res. Abfluss = Speicherinhalt * q + a Default: 0 Der Gerinne- bzw. Hauptgerinneabfluss kann mit dieser Methode fuer Rasterzelle 2 manipuliert werden. 
    - RM2X [`integer number`] : Rasterzelle 2 [x-Koord.]. Minimum: -1 Default: -1 x-Koordinate der Rasterzelle 2, fuer die der Abfluss manipulatiert werden soll
    - RM2Y [`integer number`] : Rasterzelle 2 [y-Koord.]. Minimum: -1 Default: -1 y-Koordinate der Rasterzelle 2, fuer die der Abfluss manipulatiert werden soll
    - RM2Q [`floating point number`] : q [%/100]. Minimum: 0.000000 Default: 1.000000 Prozentualer Faktor q [%/100] fuer Rasterzelle 2
    - RM2A [`floating point number`] : a [+-m3/s]. Default: 0.000000 Addidativer Offsetwert a [+-m3/s] fuer Rasterzelle 2
    - EVP1S [`text`] : EvP1 Name. Default: NeuDarchau.txt Name des Evaluierungspunktes 1
    - EVP1X [`integer number`] : EvP1 Rasterzelle [x-Koord.]. Minimum: 0 Default: 30 x-Koordinate der spezifischen Hauptgerinnerasterzelle des Evaluerungspunktes 1
    - EVP1Y [`integer number`] : EvP1 Rasterzelle [y-Koord.]. Minimum: 0 Default: 115 y-Koordinate der spezifischen Hauptgerinnerasterzelle des Evaluerungspunktes 1
    - EVP2S [`text`] : EvP2 Name. Default: Lutherstadt-Wittenberg.txt Name des Evaluierungspunktes 2
    - EVP2X [`integer number`] : EvP2 Rasterzelle [x-Koord.]. Minimum: 0 Default: 54 x-Koordinate der spezifischen Hauptgerinnerasterzelle des Evaluerungspunktes 2
    - EVP2Y [`integer number`] : EvP2 Rasterzelle [y-Koord.]. Minimum: 0 Default: 85 y-Koordinate der spezifischen Hauptgerinnerasterzelle des Evaluerungspunktes 2
    - EVP3S [`text`] : EvP3 Name. Default: Schoena.txt Name des Evaluierungspunktes 3
    - EVP3X [`integer number`] : EvP3 Rasterzelle [x-Koord.]. Minimum: 0 Default: 78 x-Koordinate der spezifischen Hauptgerinnerasterzelle des Evaluerungspunktes 3
    - EVP3Y [`integer number`] : EvP3 Rasterzelle [y-Koord.]. Minimum: 0 Default: 65 y-Koordinate der spezifischen Hauptgerinnerasterzelle des Evaluerungspunktes 3
    - RBMX [`integer number`] : Ausgangsrasterzelle [x-Koord.]. Minimum: 0 Default: 16 x-Koordinate der Ausgangsrasterzelle des Flusseinzugsgebietes
    - RBMY [`integer number`] : Ausgangsrasterzelle [y-Koord.]. Minimum: 0 Default: 121 y-Koordinate der Ausgangsrasterzelle des Flusseinzugsgebietes
    - wP [`boolean`] : SimParameters. Default: 1 Schreibt wichtige, der Simulation zugrundeliegenden Parameter in eine Textdatei
    - eP [`boolean`] : Fehlerprotokoll. Default: 1 Schreibt Fehler in Textfile
    - MONILOG1 [`boolean`] : RiverBasinDay-Monitoring. Default: 1 Monitoring taegicher Werte des Flusseinzugsgebiets
    - MONILOG3 [`boolean`] : RiverBasinMonth-Monitoring. Default: 1 Monitoring monatlicher Werte des Flusseinzugsgebiets
    - MONILOG2 [`boolean`] : WSystem-Monitoring. Default: 1 Monitoring von Zu- und Abflusswerten des WasserGesamtsystems
    - TEST1 [`choice`] : Testroutine1 durchfuehren... . Available Choices: [0] nein [1] Ja, TestRoutine1 nur fuer Teileinzugsgbiet der HG-Rasterzelle [2] Ja, TestRoutine1 fuer Flusseinzugsgebiet bis zu der HG-Rasterzelle Default: 0 Waehlen ob TestRoutine 1 durchgefuehrt werden soll... 1) nur fuer Teileinzugsgebiet der HG-Rasterzelle oder 2) fuer das Flusseinzugsgebiet bis zum Erreichen der HG-Rasterzelle.
    - XT1 [`integer number`] : Hauptgerinnerasterzelle [x-Koord.]. Minimum: 0 Default: 0 x-Koordinate der spezifischen Hauptgerinnerasterzelle fuer TestRoutine 1
    - YT1 [`integer number`] : Hauptgerinnerasterzelle [y-Koord.]. Minimum: 0 Default: 0 y-Koordinate der spezifischen Hauptgerinnerasterzelle fuer TestRoutine 1

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('sim_rivflow', '1', 'LandFlow Version 1.0 (build 3.5.1b)')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Input ('INPUT2', INPUT2)
        Tool.Set_Input ('INPUT10', INPUT10)
        Tool.Set_Input ('INPUT8', INPUT8)
        Tool.Set_Input ('INPUT9', INPUT9)
        Tool.Set_Input ('INPUT11', INPUT11)
        Tool.Set_Input ('INPUT12', INPUT12)
        Tool.Set_Input ('INPUT3', INPUT3)
        Tool.Set_Input ('INPUT5', INPUT5)
        Tool.Set_Input ('INPUT6', INPUT6)
        Tool.Set_Input ('INPUT7', INPUT7)
        Tool.Set_Output('OUTPUT', OUTPUT)
        Tool.Set_Output('OUTPUT2', OUTPUT2)
        Tool.Set_Output('OUTPUT3', OUTPUT3)
        Tool.Set_Output('OUTPUT4', OUTPUT4)
        Tool.Set_Output('OUTPUT5', OUTPUT5)
        Tool.Set_Output('OUTPUT6', OUTPUT6)
        Tool.Set_Option('onlyRB', ONLYRB)
        Tool.Set_Option('TimeStep', TIMESTEP)
        Tool.Set_Option('CalcT', CALCT)
        Tool.Set_Option('sYear', SYEAR)
        Tool.Set_Option('DayNum', DAYNUM)
        Tool.Set_Option('Folder2', FOLDER2)
        Tool.Set_Option('Folder1', FOLDER1)
        Tool.Set_Option('stRedFacR', STREDFACR)
        Tool.Set_Option('autoFacD', AUTOFACD)
        Tool.Set_Option('stRedFacD', STREDFACD)
        Tool.Set_Option('OffsetR', OFFSETR)
        Tool.Set_Option('OffsetD', OFFSETD)
        Tool.Set_Option('CacheUse', CACHEUSE)
        Tool.Set_Option('Folder4', FOLDER4)
        Tool.Set_Option('wNC', WNC)
        Tool.Set_Option('ParamC', PARAMC)
        Tool.Set_Option('ParamG', PARAMG)
        Tool.Set_Option('ParamB', PARAMB)
        Tool.Set_Option('nG', nG)
        Tool.Set_Option('RivG', RIVG)
        Tool.Set_Option('ParamCr', PARAMCR)
        Tool.Set_Option('nHG', NHG)
        Tool.Set_Option('EnfVmax', ENFVMAX)
        Tool.Set_Option('VTresh', VTRESH)
        Tool.Set_Option('WCons', WCONS)
        Tool.Set_Option('Folder3', FOLDER3)
        Tool.Set_Option('WConUnit', WCONUNIT)
        Tool.Set_Option('WConsD', WCONSD)
        Tool.Set_Option('WConThres', WCONTHRES)
        Tool.Set_Option('stConsAll', STCONSALL)
        Tool.Set_Option('stConsRiv', STCONSRIV)
        Tool.Set_Option('vRM1', VRM1)
        Tool.Set_Option('RM1x', RM1X)
        Tool.Set_Option('RM1y', RM1Y)
        Tool.Set_Option('RM1q', RM1Q)
        Tool.Set_Option('RM1a', RM1A)
        Tool.Set_Option('vRM2', VRM2)
        Tool.Set_Option('RM2x', RM2X)
        Tool.Set_Option('RM2y', RM2Y)
        Tool.Set_Option('RM2q', RM2Q)
        Tool.Set_Option('RM2a', RM2A)
        Tool.Set_Option('EvP1s', EVP1S)
        Tool.Set_Option('EvP1x', EVP1X)
        Tool.Set_Option('EvP1y', EVP1Y)
        Tool.Set_Option('EvP2s', EVP2S)
        Tool.Set_Option('EvP2x', EVP2X)
        Tool.Set_Option('EvP2y', EVP2Y)
        Tool.Set_Option('EvP3s', EVP3S)
        Tool.Set_Option('EvP3x', EVP3X)
        Tool.Set_Option('EvP3y', EVP3Y)
        Tool.Set_Option('RBMx', RBMX)
        Tool.Set_Option('RBMy', RBMY)
        Tool.Set_Option('wP', wP)
        Tool.Set_Option('eP', eP)
        Tool.Set_Option('MoniLog1', MONILOG1)
        Tool.Set_Option('MoniLog3', MONILOG3)
        Tool.Set_Option('MoniLog2', MONILOG2)
        Tool.Set_Option('Test1', TEST1)
        Tool.Set_Option('xt1', XT1)
        Tool.Set_Option('yt1', YT1)
        return Tool.Execute(Verbose)
    return False

def RiverGridGeneration(INPUT=None, OUTPUT=None, SX=None, SY=None, MX=None, MY=None, OWRITE=None, Verbose=2):
    '''
    RiverGridGeneration
    ----------
    [sim_rivflow.3]\n
    Generation of RiverCourse-GridCells\n
    Arguments
    ----------
    - INPUT [`input grid`] : GelÃ¤ndemodell (DTM). Digitales GelÃ¤ndemodell des Flusseinzugsgebietes
    - OUTPUT [`output grid`] : HG Raster. Schrittweise Ausgabe der gewÃ¼nschten Abflusspfade des Hauprgerinnerasters
    - SX [`integer number`] : Abflusspfad-Quelle, x-Wert. Minimum: 0 Default: 0 X-Wert der Quellen-Rasterzelle
    - SY [`integer number`] : Abflusspfad-Quelle, y-Wert. Minimum: 0 Default: 0 Y-Wert der Quellen-Rastertzelle
    - MX [`integer number`] : Abflusspfad-MÃ¼ndung, x-Wert. Minimum: 0 Default: 0 X-Wert des MÃ¼ndungs-Rasterzelle
    - MY [`integer number`] : Abflusspfad-MÃ¼ndung, y-Wert. Minimum: 0 Default: 0 Y-Wert des MÃ¼ndungsRasterzelle
    - OWRITE [`boolean`] : Overwrite RiverGridCells. Default: 0 Bereits existierende RiverGridCells werden Ã¼berschrieben (Wenn nicht gesetzt: Abflusspfadende, wenn eine existierende RiverGridCell erreicht wird -> zB bei Fluss mit zwei Quellen sinnvoll).

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('sim_rivflow', '3', 'RiverGridGeneration')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Output('OUTPUT', OUTPUT)
        Tool.Set_Option('SX', SX)
        Tool.Set_Option('SY', SY)
        Tool.Set_Option('MX', MX)
        Tool.Set_Option('MY', MY)
        Tool.Set_Option('Owrite', OWRITE)
        return Tool.Execute(Verbose)
    return False

def run_tool_sim_rivflow_3(INPUT=None, OUTPUT=None, SX=None, SY=None, MX=None, MY=None, OWRITE=None, Verbose=2):
    '''
    RiverGridGeneration
    ----------
    [sim_rivflow.3]\n
    Generation of RiverCourse-GridCells\n
    Arguments
    ----------
    - INPUT [`input grid`] : GelÃ¤ndemodell (DTM). Digitales GelÃ¤ndemodell des Flusseinzugsgebietes
    - OUTPUT [`output grid`] : HG Raster. Schrittweise Ausgabe der gewÃ¼nschten Abflusspfade des Hauprgerinnerasters
    - SX [`integer number`] : Abflusspfad-Quelle, x-Wert. Minimum: 0 Default: 0 X-Wert der Quellen-Rasterzelle
    - SY [`integer number`] : Abflusspfad-Quelle, y-Wert. Minimum: 0 Default: 0 Y-Wert der Quellen-Rastertzelle
    - MX [`integer number`] : Abflusspfad-MÃ¼ndung, x-Wert. Minimum: 0 Default: 0 X-Wert des MÃ¼ndungs-Rasterzelle
    - MY [`integer number`] : Abflusspfad-MÃ¼ndung, y-Wert. Minimum: 0 Default: 0 Y-Wert des MÃ¼ndungsRasterzelle
    - OWRITE [`boolean`] : Overwrite RiverGridCells. Default: 0 Bereits existierende RiverGridCells werden Ã¼berschrieben (Wenn nicht gesetzt: Abflusspfadende, wenn eine existierende RiverGridCell erreicht wird -> zB bei Fluss mit zwei Quellen sinnvoll).

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('sim_rivflow', '3', 'RiverGridGeneration')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Output('OUTPUT', OUTPUT)
        Tool.Set_Option('SX', SX)
        Tool.Set_Option('SY', SY)
        Tool.Set_Option('MX', MX)
        Tool.Set_Option('MY', MY)
        Tool.Set_Option('Owrite', OWRITE)
        return Tool.Execute(Verbose)
    return False

def GridCombination(INPUT=None, FOLDER1=None, sY=None, eY=None, DOMW=None, ELECW=None, LIVEW=None, MANW=None, IRRW=None, FVA=None, Verbose=2):
    '''
    GridCombination
    ----------
    [sim_rivflow.4]\n
    Grid Combination\n
    Arguments
    ----------
    - INPUT [`input grid`] : Gelaendemodell (DTM). Digitales Gelaendemodell des Flusseinzugsgebietes
    - FOLDER1 [`file path`] : Pfad WaterGap Raster. Ordnerpfad in dem alle zu bearbeitenden WaterGap Raster abgelegt sind
    - sY [`integer number`] : Start-Jahr. Minimum: 1906 Maximum: 2000 Default: 1990 Jahr in dem die Gridoperation startet
    - eY [`integer number`] : End-Jahr. Minimum: 1906 Maximum: 2000 Default: 1990 Jahr in dem der Verarbeitungsprozess enden soll
    - DOMW [`boolean`] : Domestic Water. Default: 1 Beruecksichtigung der Domestic Water im resultieren Raster.
    - ELECW [`boolean`] : Electricity Water. Default: 1 Beruecksichtigung der Electricity Water im resultieren Raster.
    - LIVEW [`boolean`] : Livestock Water. Default: 1 Beruecksichtigung der Livestock Water im resultieren Raster.
    - MANW [`boolean`] : Manufacturing Water. Default: 1 Beruecksichtigung des Manufacturing Water im resultieren Raster.
    - IRRW [`boolean`] : Irrigation Water. Default: 1 Beruecksichtigung des Irrigation Water im resultieren Raster (moeglicher Einheitenfehler im WaterGapDatensatz!!).
    - FVA [`choice`] : Flaechenverbrauch-Auswahl (FvA). Available Choices: [0] Resultierendes Raster ueber WasserENTNAHME erstellen [1] Resultierendes Raster ueber WasserNUTZUNG erstellen Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('sim_rivflow', '4', 'GridCombination')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Option('Folder1', FOLDER1)
        Tool.Set_Option('sY', sY)
        Tool.Set_Option('eY', eY)
        Tool.Set_Option('DomW', DOMW)
        Tool.Set_Option('ElecW', ELECW)
        Tool.Set_Option('LiveW', LIVEW)
        Tool.Set_Option('ManW', MANW)
        Tool.Set_Option('IrrW', IRRW)
        Tool.Set_Option('FvA', FVA)
        return Tool.Execute(Verbose)
    return False

def run_tool_sim_rivflow_4(INPUT=None, FOLDER1=None, sY=None, eY=None, DOMW=None, ELECW=None, LIVEW=None, MANW=None, IRRW=None, FVA=None, Verbose=2):
    '''
    GridCombination
    ----------
    [sim_rivflow.4]\n
    Grid Combination\n
    Arguments
    ----------
    - INPUT [`input grid`] : Gelaendemodell (DTM). Digitales Gelaendemodell des Flusseinzugsgebietes
    - FOLDER1 [`file path`] : Pfad WaterGap Raster. Ordnerpfad in dem alle zu bearbeitenden WaterGap Raster abgelegt sind
    - sY [`integer number`] : Start-Jahr. Minimum: 1906 Maximum: 2000 Default: 1990 Jahr in dem die Gridoperation startet
    - eY [`integer number`] : End-Jahr. Minimum: 1906 Maximum: 2000 Default: 1990 Jahr in dem der Verarbeitungsprozess enden soll
    - DOMW [`boolean`] : Domestic Water. Default: 1 Beruecksichtigung der Domestic Water im resultieren Raster.
    - ELECW [`boolean`] : Electricity Water. Default: 1 Beruecksichtigung der Electricity Water im resultieren Raster.
    - LIVEW [`boolean`] : Livestock Water. Default: 1 Beruecksichtigung der Livestock Water im resultieren Raster.
    - MANW [`boolean`] : Manufacturing Water. Default: 1 Beruecksichtigung des Manufacturing Water im resultieren Raster.
    - IRRW [`boolean`] : Irrigation Water. Default: 1 Beruecksichtigung des Irrigation Water im resultieren Raster (moeglicher Einheitenfehler im WaterGapDatensatz!!).
    - FVA [`choice`] : Flaechenverbrauch-Auswahl (FvA). Available Choices: [0] Resultierendes Raster ueber WasserENTNAHME erstellen [1] Resultierendes Raster ueber WasserNUTZUNG erstellen Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('sim_rivflow', '4', 'GridCombination')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Option('Folder1', FOLDER1)
        Tool.Set_Option('sY', sY)
        Tool.Set_Option('eY', eY)
        Tool.Set_Option('DomW', DOMW)
        Tool.Set_Option('ElecW', ELECW)
        Tool.Set_Option('LiveW', LIVEW)
        Tool.Set_Option('ManW', MANW)
        Tool.Set_Option('IrrW', IRRW)
        Tool.Set_Option('FvA', FVA)
        return Tool.Execute(Verbose)
    return False

