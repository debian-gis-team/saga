#! /usr/bin/env python

'''
Python Interface to SAGA Tools Library
----------
- Category : Simulation
- Name     : Landscape Evolution
- ID       : sim_landscape_evolution

Description
----------
Tools for landscape evolution modelling.
'''

from PySAGA.helper import Tool_Wrapper

def SaLEM(SURFACE_T0=None, CLIMATE_TREND=None, CLIMATE_ANNUAL=None, REGOLITH_T0=None, ALLOCHTHONE=None, BEDROCK_ROCK_LAYERS=None, SURFACE=None, REGOLITH=None, DIFFERENCE=None, TRACERS_POINTS=None, TRACERS_LINES=None, REGOLITH_T0_DEFAULT=None, ALLOCHTHONE_DEFAULT=None, CLIMATE_TREND_YEAR=None, CLIMATE_TREND_T=None, CLIMATE_TREND_T_OFFSET=None, CLIMATE_ANNUAL_T=None, CLIMATE_ANNUAL_TMIN=None, CLIMATE_ANNUAL_TMAX=None, CLIMATE_ANNUAL_P=None, CLIMATE_ANNUAL_T_UNIT=None, CLIMATE_T_LAPSE=None, CLIMATE_T_LAPSE_CELL=None, BEDROCK_WEATHERING=None, BEDROCK_FROST=None, BEDROCK_CHEMICAL=None, TRACERS_TRIM=None, TRACERS_DIR_RAND=None, TRACERS_H_DENSITY=None, TRACERS_H_RANDOM=None, TRACERS_V_DENSITY=None, TRACERS_V_RANDOM=None, TIME_START=None, TIME_STOP=None, TIME_STEP=None, DIFFUSIVE_KD=None, DIFFUSIVE_NEIGHBOURS=None, Verbose=2):
    '''
    SaLEM
    ----------
    [sim_landscape_evolution.0]\n
    This is the implementation of a Soil and Landscape Evolution Model (SaLEM) for the spatiotemporal investigation of soil parent material evolution following a lithologically differentiated approach. The model needs a digital elevation model and (paleo-)climatic data for the simulation of weathering, erosion and transport processes. Weathering is controlled by user defined functions in dependence of climate conditions, local slope, regolith cover and outcropping bedrock lithology. Lithology can be supplied as a set of grids, of which each grid represents the top elevation of the underlying bedrock type.\n
    Arguments
    ----------
    - SURFACE_T0 [`input grid`] : Initial Surface Elevation
    - CLIMATE_TREND [`input table`] : Long-term Temperature Signal. Long-term temperature signal, used as adjustment for annual scenarios, i.e. to let their original values appear cooler or warmer.
    - CLIMATE_ANNUAL [`input table`] : Annual Climate
    - REGOLITH_T0 [`optional input grid`] : Initial Regolith Thickness. Initial sediment cover (m)
    - ALLOCHTHONE [`optional input grid`] : Allochthone Input. Allochthone input in meter per year (e.g. of aeolian origin, 'Loess').
    - BEDROCK_ROCK_LAYERS [`optional input grid list`] : Lithology
    - SURFACE [`output grid`] : Surface Elevation
    - REGOLITH [`output grid`] : Regolith Thickness
    - DIFFERENCE [`output grid`] : Difference
    - TRACERS_POINTS [`output shapes`] : Tracer
    - TRACERS_LINES [`output shapes`] : Tracer Paths
    - REGOLITH_T0_DEFAULT [`floating point number`] : Default. Minimum: 0.000000 Default: 0.000000 default value if no grid has been selected
    - ALLOCHTHONE_DEFAULT [`floating point number`] : Default. Minimum: 0.000000 Default: 0.000000 default value if no grid has been selected
    - CLIMATE_TREND_YEAR [`table field`] : Year. Time expected as 1000 years before present (ka BP).
    - CLIMATE_TREND_T [`table field`] : Temperature. Temperature expected as degree Celsius.
    - CLIMATE_TREND_T_OFFSET [`floating point number`] : Temperature Offset. Default: 31.000000 Temperature offset (degree Celsius).
    - CLIMATE_ANNUAL_T [`table field`] : Mean Temperature
    - CLIMATE_ANNUAL_TMIN [`table field`] : Minimum Temperature
    - CLIMATE_ANNUAL_TMAX [`table field`] : Maximum Temperature
    - CLIMATE_ANNUAL_P [`table field`] : Precipitation
    - CLIMATE_ANNUAL_T_UNIT [`choice`] : Temperature Unit. Available Choices: [0] Celsius [1] Kelvin Default: 0
    - CLIMATE_T_LAPSE [`floating point number`] : Temperature Lapse Rate. Minimum: 0.000000 Default: 0.600000 Temperature lapse rate as degree Celsius per 100 meter.
    - CLIMATE_T_LAPSE_CELL [`boolean`] : Temperature Height Correction. Default: 1 Cellwise temperature correction applying specified temperature lapse rate to surface elevation.
    - BEDROCK_WEATHERING [`static table`] : Weathering Formulas. 2 Fields: - 1. [string] Frost - 2. [string] Chemical  Variables that can be used in formulas are 'T', 'Tmin', 'Tmax', 'Tamp' (mean, minimum, maximum, range of temperature), 'P' (precipitation), 'R' (regolith thickness), 'S' (slope gradient).
    - BEDROCK_FROST [`text`] : Frost Weathering. Default: 0.0250 * (0.00175 * R + T - Tmax) / (-Tamp * cos(S))
    - BEDROCK_CHEMICAL [`text`] : Chemical Weathering. Default: 0.0002 * exp(-5.0 * R)
    - TRACERS_TRIM [`choice`] : Trim. Available Choices: [0] no [1] temporarily [2] permanently Default: 1 Remove tracers and associated paths that moved out of the investigated area (performance gain).
    - TRACERS_DIR_RAND [`floating point number`] : Randomize Direction. Minimum: 0.000000 Default: 0.000000 Uncertainty in routing direction expressed as standard deviation (degree).
    - TRACERS_H_DENSITY [`integer number`] : Horizontal Density. Minimum: 1 Default: 1 Horizontal tracer density in cells.
    - TRACERS_H_RANDOM [`boolean`] : Randomize. Default: 1
    - TRACERS_V_DENSITY [`floating point number`] : Vertical Density. Minimum: 0.010000 Default: 0.500000 Vertical tracer density in meter.
    - TRACERS_V_RANDOM [`boolean`] : Randomize. Default: 1
    - TIME_START [`integer number`] : Start [Years BP]. Default: 50000
    - TIME_STOP [`integer number`] : Stop [Years BP]. Default: 10000
    - TIME_STEP [`integer number`] : Step [Years]. Minimum: 1 Default: 100
    - DIFFUSIVE_KD [`floating point number`] : Diffusivity Coefficient Kd [m²/a]. Minimum: 0.000000 Default: 0.010000
    - DIFFUSIVE_NEIGHBOURS [`choice`] : Neighbourhood. Available Choices: [0] Neumann [1] Moore Default: 1

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('sim_landscape_evolution', '0', 'SaLEM')
    if Tool.is_Okay():
        Tool.Set_Input ('SURFACE_T0', SURFACE_T0)
        Tool.Set_Input ('CLIMATE.TREND', CLIMATE_TREND)
        Tool.Set_Input ('CLIMATE.ANNUAL', CLIMATE_ANNUAL)
        Tool.Set_Input ('REGOLITH_T0', REGOLITH_T0)
        Tool.Set_Input ('ALLOCHTHONE', ALLOCHTHONE)
        Tool.Set_Input ('BEDROCK.ROCK_LAYERS', BEDROCK_ROCK_LAYERS)
        Tool.Set_Output('SURFACE', SURFACE)
        Tool.Set_Output('REGOLITH', REGOLITH)
        Tool.Set_Output('DIFFERENCE', DIFFERENCE)
        Tool.Set_Output('TRACERS.POINTS', TRACERS_POINTS)
        Tool.Set_Output('TRACERS.LINES', TRACERS_LINES)
        Tool.Set_Option('REGOLITH_T0_DEFAULT', REGOLITH_T0_DEFAULT)
        Tool.Set_Option('ALLOCHTHONE_DEFAULT', ALLOCHTHONE_DEFAULT)
        Tool.Set_Option('CLIMATE.TREND_YEAR', CLIMATE_TREND_YEAR)
        Tool.Set_Option('CLIMATE.TREND_T', CLIMATE_TREND_T)
        Tool.Set_Option('CLIMATE.TREND_T_OFFSET', CLIMATE_TREND_T_OFFSET)
        Tool.Set_Option('CLIMATE.ANNUAL_T', CLIMATE_ANNUAL_T)
        Tool.Set_Option('CLIMATE.ANNUAL_TMIN', CLIMATE_ANNUAL_TMIN)
        Tool.Set_Option('CLIMATE.ANNUAL_TMAX', CLIMATE_ANNUAL_TMAX)
        Tool.Set_Option('CLIMATE.ANNUAL_P', CLIMATE_ANNUAL_P)
        Tool.Set_Option('CLIMATE.ANNUAL_T_UNIT', CLIMATE_ANNUAL_T_UNIT)
        Tool.Set_Option('CLIMATE.T_LAPSE', CLIMATE_T_LAPSE)
        Tool.Set_Option('CLIMATE.T_LAPSE_CELL', CLIMATE_T_LAPSE_CELL)
        Tool.Set_Option('BEDROCK.WEATHERING', BEDROCK_WEATHERING)
        Tool.Set_Option('BEDROCK.FROST', BEDROCK_FROST)
        Tool.Set_Option('BEDROCK.CHEMICAL', BEDROCK_CHEMICAL)
        Tool.Set_Option('TRACERS.TRIM', TRACERS_TRIM)
        Tool.Set_Option('TRACERS.DIR_RAND', TRACERS_DIR_RAND)
        Tool.Set_Option('TRACERS.H_DENSITY', TRACERS_H_DENSITY)
        Tool.Set_Option('TRACERS.H_RANDOM', TRACERS_H_RANDOM)
        Tool.Set_Option('TRACERS.V_DENSITY', TRACERS_V_DENSITY)
        Tool.Set_Option('TRACERS.V_RANDOM', TRACERS_V_RANDOM)
        Tool.Set_Option('TIME_START', TIME_START)
        Tool.Set_Option('TIME_STOP', TIME_STOP)
        Tool.Set_Option('TIME_STEP', TIME_STEP)
        Tool.Set_Option('DIFFUSIVE_KD', DIFFUSIVE_KD)
        Tool.Set_Option('DIFFUSIVE_NEIGHBOURS', DIFFUSIVE_NEIGHBOURS)
        return Tool.Execute(Verbose)
    return False

def run_tool_sim_landscape_evolution_0(SURFACE_T0=None, CLIMATE_TREND=None, CLIMATE_ANNUAL=None, REGOLITH_T0=None, ALLOCHTHONE=None, BEDROCK_ROCK_LAYERS=None, SURFACE=None, REGOLITH=None, DIFFERENCE=None, TRACERS_POINTS=None, TRACERS_LINES=None, REGOLITH_T0_DEFAULT=None, ALLOCHTHONE_DEFAULT=None, CLIMATE_TREND_YEAR=None, CLIMATE_TREND_T=None, CLIMATE_TREND_T_OFFSET=None, CLIMATE_ANNUAL_T=None, CLIMATE_ANNUAL_TMIN=None, CLIMATE_ANNUAL_TMAX=None, CLIMATE_ANNUAL_P=None, CLIMATE_ANNUAL_T_UNIT=None, CLIMATE_T_LAPSE=None, CLIMATE_T_LAPSE_CELL=None, BEDROCK_WEATHERING=None, BEDROCK_FROST=None, BEDROCK_CHEMICAL=None, TRACERS_TRIM=None, TRACERS_DIR_RAND=None, TRACERS_H_DENSITY=None, TRACERS_H_RANDOM=None, TRACERS_V_DENSITY=None, TRACERS_V_RANDOM=None, TIME_START=None, TIME_STOP=None, TIME_STEP=None, DIFFUSIVE_KD=None, DIFFUSIVE_NEIGHBOURS=None, Verbose=2):
    '''
    SaLEM
    ----------
    [sim_landscape_evolution.0]\n
    This is the implementation of a Soil and Landscape Evolution Model (SaLEM) for the spatiotemporal investigation of soil parent material evolution following a lithologically differentiated approach. The model needs a digital elevation model and (paleo-)climatic data for the simulation of weathering, erosion and transport processes. Weathering is controlled by user defined functions in dependence of climate conditions, local slope, regolith cover and outcropping bedrock lithology. Lithology can be supplied as a set of grids, of which each grid represents the top elevation of the underlying bedrock type.\n
    Arguments
    ----------
    - SURFACE_T0 [`input grid`] : Initial Surface Elevation
    - CLIMATE_TREND [`input table`] : Long-term Temperature Signal. Long-term temperature signal, used as adjustment for annual scenarios, i.e. to let their original values appear cooler or warmer.
    - CLIMATE_ANNUAL [`input table`] : Annual Climate
    - REGOLITH_T0 [`optional input grid`] : Initial Regolith Thickness. Initial sediment cover (m)
    - ALLOCHTHONE [`optional input grid`] : Allochthone Input. Allochthone input in meter per year (e.g. of aeolian origin, 'Loess').
    - BEDROCK_ROCK_LAYERS [`optional input grid list`] : Lithology
    - SURFACE [`output grid`] : Surface Elevation
    - REGOLITH [`output grid`] : Regolith Thickness
    - DIFFERENCE [`output grid`] : Difference
    - TRACERS_POINTS [`output shapes`] : Tracer
    - TRACERS_LINES [`output shapes`] : Tracer Paths
    - REGOLITH_T0_DEFAULT [`floating point number`] : Default. Minimum: 0.000000 Default: 0.000000 default value if no grid has been selected
    - ALLOCHTHONE_DEFAULT [`floating point number`] : Default. Minimum: 0.000000 Default: 0.000000 default value if no grid has been selected
    - CLIMATE_TREND_YEAR [`table field`] : Year. Time expected as 1000 years before present (ka BP).
    - CLIMATE_TREND_T [`table field`] : Temperature. Temperature expected as degree Celsius.
    - CLIMATE_TREND_T_OFFSET [`floating point number`] : Temperature Offset. Default: 31.000000 Temperature offset (degree Celsius).
    - CLIMATE_ANNUAL_T [`table field`] : Mean Temperature
    - CLIMATE_ANNUAL_TMIN [`table field`] : Minimum Temperature
    - CLIMATE_ANNUAL_TMAX [`table field`] : Maximum Temperature
    - CLIMATE_ANNUAL_P [`table field`] : Precipitation
    - CLIMATE_ANNUAL_T_UNIT [`choice`] : Temperature Unit. Available Choices: [0] Celsius [1] Kelvin Default: 0
    - CLIMATE_T_LAPSE [`floating point number`] : Temperature Lapse Rate. Minimum: 0.000000 Default: 0.600000 Temperature lapse rate as degree Celsius per 100 meter.
    - CLIMATE_T_LAPSE_CELL [`boolean`] : Temperature Height Correction. Default: 1 Cellwise temperature correction applying specified temperature lapse rate to surface elevation.
    - BEDROCK_WEATHERING [`static table`] : Weathering Formulas. 2 Fields: - 1. [string] Frost - 2. [string] Chemical  Variables that can be used in formulas are 'T', 'Tmin', 'Tmax', 'Tamp' (mean, minimum, maximum, range of temperature), 'P' (precipitation), 'R' (regolith thickness), 'S' (slope gradient).
    - BEDROCK_FROST [`text`] : Frost Weathering. Default: 0.0250 * (0.00175 * R + T - Tmax) / (-Tamp * cos(S))
    - BEDROCK_CHEMICAL [`text`] : Chemical Weathering. Default: 0.0002 * exp(-5.0 * R)
    - TRACERS_TRIM [`choice`] : Trim. Available Choices: [0] no [1] temporarily [2] permanently Default: 1 Remove tracers and associated paths that moved out of the investigated area (performance gain).
    - TRACERS_DIR_RAND [`floating point number`] : Randomize Direction. Minimum: 0.000000 Default: 0.000000 Uncertainty in routing direction expressed as standard deviation (degree).
    - TRACERS_H_DENSITY [`integer number`] : Horizontal Density. Minimum: 1 Default: 1 Horizontal tracer density in cells.
    - TRACERS_H_RANDOM [`boolean`] : Randomize. Default: 1
    - TRACERS_V_DENSITY [`floating point number`] : Vertical Density. Minimum: 0.010000 Default: 0.500000 Vertical tracer density in meter.
    - TRACERS_V_RANDOM [`boolean`] : Randomize. Default: 1
    - TIME_START [`integer number`] : Start [Years BP]. Default: 50000
    - TIME_STOP [`integer number`] : Stop [Years BP]. Default: 10000
    - TIME_STEP [`integer number`] : Step [Years]. Minimum: 1 Default: 100
    - DIFFUSIVE_KD [`floating point number`] : Diffusivity Coefficient Kd [m²/a]. Minimum: 0.000000 Default: 0.010000
    - DIFFUSIVE_NEIGHBOURS [`choice`] : Neighbourhood. Available Choices: [0] Neumann [1] Moore Default: 1

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('sim_landscape_evolution', '0', 'SaLEM')
    if Tool.is_Okay():
        Tool.Set_Input ('SURFACE_T0', SURFACE_T0)
        Tool.Set_Input ('CLIMATE.TREND', CLIMATE_TREND)
        Tool.Set_Input ('CLIMATE.ANNUAL', CLIMATE_ANNUAL)
        Tool.Set_Input ('REGOLITH_T0', REGOLITH_T0)
        Tool.Set_Input ('ALLOCHTHONE', ALLOCHTHONE)
        Tool.Set_Input ('BEDROCK.ROCK_LAYERS', BEDROCK_ROCK_LAYERS)
        Tool.Set_Output('SURFACE', SURFACE)
        Tool.Set_Output('REGOLITH', REGOLITH)
        Tool.Set_Output('DIFFERENCE', DIFFERENCE)
        Tool.Set_Output('TRACERS.POINTS', TRACERS_POINTS)
        Tool.Set_Output('TRACERS.LINES', TRACERS_LINES)
        Tool.Set_Option('REGOLITH_T0_DEFAULT', REGOLITH_T0_DEFAULT)
        Tool.Set_Option('ALLOCHTHONE_DEFAULT', ALLOCHTHONE_DEFAULT)
        Tool.Set_Option('CLIMATE.TREND_YEAR', CLIMATE_TREND_YEAR)
        Tool.Set_Option('CLIMATE.TREND_T', CLIMATE_TREND_T)
        Tool.Set_Option('CLIMATE.TREND_T_OFFSET', CLIMATE_TREND_T_OFFSET)
        Tool.Set_Option('CLIMATE.ANNUAL_T', CLIMATE_ANNUAL_T)
        Tool.Set_Option('CLIMATE.ANNUAL_TMIN', CLIMATE_ANNUAL_TMIN)
        Tool.Set_Option('CLIMATE.ANNUAL_TMAX', CLIMATE_ANNUAL_TMAX)
        Tool.Set_Option('CLIMATE.ANNUAL_P', CLIMATE_ANNUAL_P)
        Tool.Set_Option('CLIMATE.ANNUAL_T_UNIT', CLIMATE_ANNUAL_T_UNIT)
        Tool.Set_Option('CLIMATE.T_LAPSE', CLIMATE_T_LAPSE)
        Tool.Set_Option('CLIMATE.T_LAPSE_CELL', CLIMATE_T_LAPSE_CELL)
        Tool.Set_Option('BEDROCK.WEATHERING', BEDROCK_WEATHERING)
        Tool.Set_Option('BEDROCK.FROST', BEDROCK_FROST)
        Tool.Set_Option('BEDROCK.CHEMICAL', BEDROCK_CHEMICAL)
        Tool.Set_Option('TRACERS.TRIM', TRACERS_TRIM)
        Tool.Set_Option('TRACERS.DIR_RAND', TRACERS_DIR_RAND)
        Tool.Set_Option('TRACERS.H_DENSITY', TRACERS_H_DENSITY)
        Tool.Set_Option('TRACERS.H_RANDOM', TRACERS_H_RANDOM)
        Tool.Set_Option('TRACERS.V_DENSITY', TRACERS_V_DENSITY)
        Tool.Set_Option('TRACERS.V_RANDOM', TRACERS_V_RANDOM)
        Tool.Set_Option('TIME_START', TIME_START)
        Tool.Set_Option('TIME_STOP', TIME_STOP)
        Tool.Set_Option('TIME_STEP', TIME_STEP)
        Tool.Set_Option('DIFFUSIVE_KD', DIFFUSIVE_KD)
        Tool.Set_Option('DIFFUSIVE_NEIGHBOURS', DIFFUSIVE_NEIGHBOURS)
        return Tool.Execute(Verbose)
    return False

