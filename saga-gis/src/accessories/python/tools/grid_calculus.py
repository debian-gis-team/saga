#! /usr/bin/env python

'''
Python Interface to SAGA Tools Library
----------
- Category : Grid
- Name     : Calculus
- ID       : grid_calculus

Description
----------
Grid based or related calculations.
'''

from PySAGA.helper import Tool_Wrapper

def Grid_Normalization(INPUT=None, OUTPUT=None, RANGE=None, Verbose=2):
    '''
    Grid Normalization
    ----------
    [grid_calculus.0]\n
    Normalise the values of a grid. Rescales all grid values to fall in the range 'Minimum' to 'Maximum', usually 0 to 1.\n
    Arguments
    ----------
    - INPUT [`input grid`] : Grid
    - OUTPUT [`output grid`] : Normalized Grid
    - RANGE [`value range`] : Target Range

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_calculus', '0', 'Grid Normalization')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Output('OUTPUT', OUTPUT)
        Tool.Set_Option('RANGE', RANGE)
        return Tool.Execute(Verbose)
    return False

def run_tool_grid_calculus_0(INPUT=None, OUTPUT=None, RANGE=None, Verbose=2):
    '''
    Grid Normalization
    ----------
    [grid_calculus.0]\n
    Normalise the values of a grid. Rescales all grid values to fall in the range 'Minimum' to 'Maximum', usually 0 to 1.\n
    Arguments
    ----------
    - INPUT [`input grid`] : Grid
    - OUTPUT [`output grid`] : Normalized Grid
    - RANGE [`value range`] : Target Range

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_calculus', '0', 'Grid Normalization')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Output('OUTPUT', OUTPUT)
        Tool.Set_Option('RANGE', RANGE)
        return Tool.Execute(Verbose)
    return False

def Grid_Calculator(GRIDS=None, XGRIDS=None, RESULT=None, RESAMPLING=None, FORMULA=None, USE_NODATA=None, TYPE=None, Verbose=2):
    '''
    Grid Calculator
    ----------
    [grid_calculus.1]\n
    The Grid Calculator calculates a new grid based on existing grids and a mathematical formula. The grid variables in the formula begin with the letter 'g' followed by a position index, which corresponds to the order of the grids in the input grid list (i.e.: g1, g2, g3, ... correspond to the first, second, third, ... grid in list). Grids from other systems than the default one can be addressed likewise using the letter 'h' (h1, h2, h3, ...), which correspond to the 'Grids from different Systems' list.\n
    Example:	 sin(g1) * g2 + 2 * h1\n
    To make complex formulas look more intuitive you have the option to use shortcuts. Shortcuts are defined following the formula separated by semicolons as 'shortcut = expression'.\n
    Example:	 ifelse(lt(NDVI, 0.4), nodata(), NDVI); NDVI = (g1 - g2) / (g1 + g2)\n
    The following operators are available for the formula definition:\n
    ============\n
    [+]	Addition\n
    [-]	Subtraction\n
    [*]	Multiplication\n
    [/]	Division\n
    [abs(x)]	Absolute Value\n
    [mod(x, y)]	Returns the floating point remainder of x/y\n
    [int(x)]	Returns the integer part of floating point value x\n
    [sqr(x)]	Square\n
    [sqrt(x)]	Square Root\n
    [exp(x)]	Exponential\n
    [pow(x, y)]	Returns x raised to the power of y\n
    [x ^ y]	Returns x raised to the power of y\n
    [ln(x)]	Natural Logarithm\n
    [log(x)]	Base 10 Logarithm\n
    [pi()]	Returns the value of Pi\n
    [sin(x)]	Sine, expects radians\n
    [cos(x)]	Cosine, expects radians\n
    [tan(x)]	Tangent, expects radians\n
    [asin(x)]	Arcsine, returns radians\n
    [acos(x)]	Arccosine, returns radians\n
    [atan(x)]	Arctangent, returns radians\n
    [atan2(x, y)]	Arctangent of x/y, returns radians\n
    [min(x, y)]	Returns the minimum of values x and y\n
    [max(x, y)]	Returns the maximum of values x and y\n
    [gt(x, y)]	Returns true (1), if x is greater than y, else false (0)\n
    [x > y]	Returns true (1), if x is greater than y, else false (0)\n
    [lt(x, y)]	Returns true (1), if x is less than y, else false (0)\n
    [x &lt; y]	Returns true (1), if x is less than y, else false (0)\n
    [eq(x, y)]	Returns true (1), if x equals y, else false (0)\n
    [x = y]	Returns true (1), if x equals y, else false (0)\n
    [and(x, y)]	Returns true (1), if both x and y are true (i.e. not 0)\n
    [or(x, y)]	Returns true (1), if at least one of both x and y is true (i.e. not 0)\n
    [ifelse(c, x, y)]	Returns x, if condition c is true (i.e. not 0), else y\n
    [rand_u(x, y)]	Random number, uniform distribution with minimum x and maximum y\n
    [rand_g(x, y)]	Random number, Gaussian distribution with mean x and standard deviation y\n
    [xpos(), ypos()]	The coordinate (x/y) for the center of the currently processed cell\n
    [col(), row()]	The currently processed cell's column/row index\n
    [ncols(), nrows()]	Number of the grid system's columns/rows\n
    [nodata(), nodata(g)]	No-data value of the resulting (empty) or requested grid (g = g1...gn, h1...hn)\n
    [cellsize(), cellsize(g)]	Cell size of the resulting (empty) or requested grid (g = h1...hn)\n
    [cellarea(), cellarea(g)]	Cell area of the resulting (empty) or requested grid (g = h1...hn)\n
    [xmin(), xmin(g)]	Left bound of the resulting (empty) or requested grid (g = h1...hn)\n
    [xmax(), xmax(g)]	Right bound of the resulting (empty) or requested grid (g = h1...hn)\n
    [xrange(), xrange(g)]	Left to right range of the resulting (empty) or requested grid (g = h1...hn)\n
    [ymin(), ymin(g)]	Lower bound of the resulting (empty) or requested grid (g = h1...hn)\n
    [ymax(), ymax(g)]	Upper bound of the resulting (empty) or requested grid (g = h1...hn)\n
    [yrange(), yrange(g)]	Lower to upper range of the resulting (empty) or requested grid (g = h1...hn)\n
    [zmin(g)]	Minimum value of the requested grid (g = g1...gn, h1...hn)\n
    [zmax(g)]	Maximum value of the requested grid (g = g1...gn, h1...hn)\n
    [zrange(g)]	Value range of the requested grid (g = g1...gn, h1...hn)\n
    [zmean(g)]	Mean value of the requested grid (g = g1...gn, h1...hn)\n
    [zstddev(g)]	Standard deviation of the requested grid (g = g1...gn, h1...hn)\n
    ============\n
    Arguments
    ----------
    - GRIDS [`optional input grid list`] : Grids. in the formula these grids are addressed in order of the list as 'g1, g2, g3, ...'
    - XGRIDS [`optional input grid list`] : Grids from different Systems. in the formula these grids are addressed in order of the list as 'h1, h2, h3, ...'
    - RESULT [`output grid`] : Result
    - RESAMPLING [`choice`] : Resampling. Available Choices: [0] Nearest Neighbour [1] Bilinear Interpolation [2] Bicubic Spline Interpolation [3] B-Spline Interpolation Default: 3
    - FORMULA [`text`] : Formula. Default: (g1 - g2) / (g1 + g2)
    - USE_NODATA [`boolean`] : Use No-Data. Default: 0 Check this in order to include no-data cells in the calculation.
    - TYPE [`data type`] : Data Type. Available Choices: [0] bit [1] unsigned 1 byte integer [2] signed 1 byte integer [3] unsigned 2 byte integer [4] signed 2 byte integer [5] unsigned 4 byte integer [6] signed 4 byte integer [7] unsigned 8 byte integer [8] signed 8 byte integer [9] 4 byte floating point number [10] 8 byte floating point number Default: 9

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_calculus', '1', 'Grid Calculator')
    if Tool.is_Okay():
        Tool.Set_Input ('GRIDS', GRIDS)
        Tool.Set_Input ('XGRIDS', XGRIDS)
        Tool.Set_Output('RESULT', RESULT)
        Tool.Set_Option('RESAMPLING', RESAMPLING)
        Tool.Set_Option('FORMULA', FORMULA)
        Tool.Set_Option('USE_NODATA', USE_NODATA)
        Tool.Set_Option('TYPE', TYPE)
        return Tool.Execute(Verbose)
    return False

def run_tool_grid_calculus_1(GRIDS=None, XGRIDS=None, RESULT=None, RESAMPLING=None, FORMULA=None, USE_NODATA=None, TYPE=None, Verbose=2):
    '''
    Grid Calculator
    ----------
    [grid_calculus.1]\n
    The Grid Calculator calculates a new grid based on existing grids and a mathematical formula. The grid variables in the formula begin with the letter 'g' followed by a position index, which corresponds to the order of the grids in the input grid list (i.e.: g1, g2, g3, ... correspond to the first, second, third, ... grid in list). Grids from other systems than the default one can be addressed likewise using the letter 'h' (h1, h2, h3, ...), which correspond to the 'Grids from different Systems' list.\n
    Example:	 sin(g1) * g2 + 2 * h1\n
    To make complex formulas look more intuitive you have the option to use shortcuts. Shortcuts are defined following the formula separated by semicolons as 'shortcut = expression'.\n
    Example:	 ifelse(lt(NDVI, 0.4), nodata(), NDVI); NDVI = (g1 - g2) / (g1 + g2)\n
    The following operators are available for the formula definition:\n
    ============\n
    [+]	Addition\n
    [-]	Subtraction\n
    [*]	Multiplication\n
    [/]	Division\n
    [abs(x)]	Absolute Value\n
    [mod(x, y)]	Returns the floating point remainder of x/y\n
    [int(x)]	Returns the integer part of floating point value x\n
    [sqr(x)]	Square\n
    [sqrt(x)]	Square Root\n
    [exp(x)]	Exponential\n
    [pow(x, y)]	Returns x raised to the power of y\n
    [x ^ y]	Returns x raised to the power of y\n
    [ln(x)]	Natural Logarithm\n
    [log(x)]	Base 10 Logarithm\n
    [pi()]	Returns the value of Pi\n
    [sin(x)]	Sine, expects radians\n
    [cos(x)]	Cosine, expects radians\n
    [tan(x)]	Tangent, expects radians\n
    [asin(x)]	Arcsine, returns radians\n
    [acos(x)]	Arccosine, returns radians\n
    [atan(x)]	Arctangent, returns radians\n
    [atan2(x, y)]	Arctangent of x/y, returns radians\n
    [min(x, y)]	Returns the minimum of values x and y\n
    [max(x, y)]	Returns the maximum of values x and y\n
    [gt(x, y)]	Returns true (1), if x is greater than y, else false (0)\n
    [x > y]	Returns true (1), if x is greater than y, else false (0)\n
    [lt(x, y)]	Returns true (1), if x is less than y, else false (0)\n
    [x &lt; y]	Returns true (1), if x is less than y, else false (0)\n
    [eq(x, y)]	Returns true (1), if x equals y, else false (0)\n
    [x = y]	Returns true (1), if x equals y, else false (0)\n
    [and(x, y)]	Returns true (1), if both x and y are true (i.e. not 0)\n
    [or(x, y)]	Returns true (1), if at least one of both x and y is true (i.e. not 0)\n
    [ifelse(c, x, y)]	Returns x, if condition c is true (i.e. not 0), else y\n
    [rand_u(x, y)]	Random number, uniform distribution with minimum x and maximum y\n
    [rand_g(x, y)]	Random number, Gaussian distribution with mean x and standard deviation y\n
    [xpos(), ypos()]	The coordinate (x/y) for the center of the currently processed cell\n
    [col(), row()]	The currently processed cell's column/row index\n
    [ncols(), nrows()]	Number of the grid system's columns/rows\n
    [nodata(), nodata(g)]	No-data value of the resulting (empty) or requested grid (g = g1...gn, h1...hn)\n
    [cellsize(), cellsize(g)]	Cell size of the resulting (empty) or requested grid (g = h1...hn)\n
    [cellarea(), cellarea(g)]	Cell area of the resulting (empty) or requested grid (g = h1...hn)\n
    [xmin(), xmin(g)]	Left bound of the resulting (empty) or requested grid (g = h1...hn)\n
    [xmax(), xmax(g)]	Right bound of the resulting (empty) or requested grid (g = h1...hn)\n
    [xrange(), xrange(g)]	Left to right range of the resulting (empty) or requested grid (g = h1...hn)\n
    [ymin(), ymin(g)]	Lower bound of the resulting (empty) or requested grid (g = h1...hn)\n
    [ymax(), ymax(g)]	Upper bound of the resulting (empty) or requested grid (g = h1...hn)\n
    [yrange(), yrange(g)]	Lower to upper range of the resulting (empty) or requested grid (g = h1...hn)\n
    [zmin(g)]	Minimum value of the requested grid (g = g1...gn, h1...hn)\n
    [zmax(g)]	Maximum value of the requested grid (g = g1...gn, h1...hn)\n
    [zrange(g)]	Value range of the requested grid (g = g1...gn, h1...hn)\n
    [zmean(g)]	Mean value of the requested grid (g = g1...gn, h1...hn)\n
    [zstddev(g)]	Standard deviation of the requested grid (g = g1...gn, h1...hn)\n
    ============\n
    Arguments
    ----------
    - GRIDS [`optional input grid list`] : Grids. in the formula these grids are addressed in order of the list as 'g1, g2, g3, ...'
    - XGRIDS [`optional input grid list`] : Grids from different Systems. in the formula these grids are addressed in order of the list as 'h1, h2, h3, ...'
    - RESULT [`output grid`] : Result
    - RESAMPLING [`choice`] : Resampling. Available Choices: [0] Nearest Neighbour [1] Bilinear Interpolation [2] Bicubic Spline Interpolation [3] B-Spline Interpolation Default: 3
    - FORMULA [`text`] : Formula. Default: (g1 - g2) / (g1 + g2)
    - USE_NODATA [`boolean`] : Use No-Data. Default: 0 Check this in order to include no-data cells in the calculation.
    - TYPE [`data type`] : Data Type. Available Choices: [0] bit [1] unsigned 1 byte integer [2] signed 1 byte integer [3] unsigned 2 byte integer [4] signed 2 byte integer [5] unsigned 4 byte integer [6] signed 4 byte integer [7] unsigned 8 byte integer [8] signed 8 byte integer [9] 4 byte floating point number [10] 8 byte floating point number Default: 9

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_calculus', '1', 'Grid Calculator')
    if Tool.is_Okay():
        Tool.Set_Input ('GRIDS', GRIDS)
        Tool.Set_Input ('XGRIDS', XGRIDS)
        Tool.Set_Output('RESULT', RESULT)
        Tool.Set_Option('RESAMPLING', RESAMPLING)
        Tool.Set_Option('FORMULA', FORMULA)
        Tool.Set_Option('USE_NODATA', USE_NODATA)
        Tool.Set_Option('TYPE', TYPE)
        return Tool.Execute(Verbose)
    return False

def Grid_Volume(GRID=None, METHOD=None, LEVEL=None, Verbose=2):
    '''
    Grid Volume
    ----------
    [grid_calculus.2]\n
    Calculate the volume under the grid's surface. This is mainly useful for Digital Elevation Models (DEM).\n
    Arguments
    ----------
    - GRID [`input grid`] : Grid
    - METHOD [`choice`] : Method. Available Choices: [0] Count Only Above Base Level [1] Count Only Below Base Level [2] Subtract Volumes Below Base Level [3] Add Volumes Below Base Level Default: 0
    - LEVEL [`floating point number`] : Base Level. Default: 0.000000

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_calculus', '2', 'Grid Volume')
    if Tool.is_Okay():
        Tool.Set_Input ('GRID', GRID)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('LEVEL', LEVEL)
        return Tool.Execute(Verbose)
    return False

def run_tool_grid_calculus_2(GRID=None, METHOD=None, LEVEL=None, Verbose=2):
    '''
    Grid Volume
    ----------
    [grid_calculus.2]\n
    Calculate the volume under the grid's surface. This is mainly useful for Digital Elevation Models (DEM).\n
    Arguments
    ----------
    - GRID [`input grid`] : Grid
    - METHOD [`choice`] : Method. Available Choices: [0] Count Only Above Base Level [1] Count Only Below Base Level [2] Subtract Volumes Below Base Level [3] Add Volumes Below Base Level Default: 0
    - LEVEL [`floating point number`] : Base Level. Default: 0.000000

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_calculus', '2', 'Grid Volume')
    if Tool.is_Okay():
        Tool.Set_Input ('GRID', GRID)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('LEVEL', LEVEL)
        return Tool.Execute(Verbose)
    return False

def Grid_Difference(A=None, B=None, C=None, B_DEFAULT=None, Verbose=2):
    '''
    Grid Difference
    ----------
    [grid_calculus.3]\n
    Grid Difference\n
    Arguments
    ----------
    - A [`input grid`] : Minuend. The grid being subtracted from.
    - B [`optional input grid`] : Subtrahend. The grid or values being subtracted.
    - C [`output grid`] : Difference. The minuend less the subtrahend.
    - B_DEFAULT [`floating point number`] : Default. Default: 0.000000 default value if no grid has been selected

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_calculus', '3', 'Grid Difference')
    if Tool.is_Okay():
        Tool.Set_Input ('A', A)
        Tool.Set_Input ('B', B)
        Tool.Set_Output('C', C)
        Tool.Set_Option('B_DEFAULT', B_DEFAULT)
        return Tool.Execute(Verbose)
    return False

def run_tool_grid_calculus_3(A=None, B=None, C=None, B_DEFAULT=None, Verbose=2):
    '''
    Grid Difference
    ----------
    [grid_calculus.3]\n
    Grid Difference\n
    Arguments
    ----------
    - A [`input grid`] : Minuend. The grid being subtracted from.
    - B [`optional input grid`] : Subtrahend. The grid or values being subtracted.
    - C [`output grid`] : Difference. The minuend less the subtrahend.
    - B_DEFAULT [`floating point number`] : Default. Default: 0.000000 default value if no grid has been selected

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_calculus', '3', 'Grid Difference')
    if Tool.is_Okay():
        Tool.Set_Input ('A', A)
        Tool.Set_Input ('B', B)
        Tool.Set_Output('C', C)
        Tool.Set_Option('B_DEFAULT', B_DEFAULT)
        return Tool.Execute(Verbose)
    return False

def Function_Plotter(TARGET_TEMPLATE=None, FUNCTION=None, FORMULA=None, X_RANGE=None, Y_RANGE=None, TARGET_DEFINITION=None, TARGET_USER_SIZE=None, TARGET_USER_XMIN=None, TARGET_USER_XMAX=None, TARGET_USER_YMIN=None, TARGET_USER_YMAX=None, TARGET_USER_COLS=None, TARGET_USER_ROWS=None, TARGET_USER_FLAT=None, TARGET_USER_FITS=None, TARGET_SYSTEM=None, Verbose=2):
    '''
    Function Plotter
    ----------
    [grid_calculus.4]\n
    Generate a grid based on a functional expression. The function interpreter uses an formula expression parser that offers the following operators:\n
    ============\n
    [+]	Addition\n
    [-]	Subtraction\n
    [*]	Multiplication\n
    [/]	Division\n
    [abs(x)]	Absolute Value\n
    [mod(x, y)]	Returns the floating point remainder of x/y\n
    [int(x)]	Returns the integer part of floating point value x\n
    [sqr(x)]	Square\n
    [sqrt(x)]	Square Root\n
    [exp(x)]	Exponential\n
    [pow(x, y)]	Returns x raised to the power of y\n
    [x ^ y]	Returns x raised to the power of y\n
    [ln(x)]	Natural Logarithm\n
    [log(x)]	Base 10 Logarithm\n
    [pi()]	Returns the value of Pi\n
    [sin(x)]	Sine, expects radians\n
    [cos(x)]	Cosine, expects radians\n
    [tan(x)]	Tangent, expects radians\n
    [asin(x)]	Arcsine, returns radians\n
    [acos(x)]	Arccosine, returns radians\n
    [atan(x)]	Arctangent, returns radians\n
    [atan2(x, y)]	Arctangent of x/y, returns radians\n
    [min(x, y)]	Returns the minimum of values x and y\n
    [max(x, y)]	Returns the maximum of values x and y\n
    [gt(x, y)]	Returns true (1), if x is greater than y, else false (0)\n
    [x > y]	Returns true (1), if x is greater than y, else false (0)\n
    [lt(x, y)]	Returns true (1), if x is less than y, else false (0)\n
    [x &lt; y]	Returns true (1), if x is less than y, else false (0)\n
    [eq(x, y)]	Returns true (1), if x equals y, else false (0)\n
    [x = y]	Returns true (1), if x equals y, else false (0)\n
    [and(x, y)]	Returns true (1), if both x and y are true (i.e. not 0)\n
    [or(x, y)]	Returns true (1), if at least one of both x and y is true (i.e. not 0)\n
    [ifelse(c, x, y)]	Returns x, if condition c is true (i.e. not 0), else y\n
    [rand_u(x, y)]	Random number, uniform distribution with minimum x and maximum y\n
    [rand_g(x, y)]	Random number, Gaussian distribution with mean x and standard deviation y\n
    ============\n
    Arguments
    ----------
    - TARGET_TEMPLATE [`optional input grid`] : Target System. use this grid's system for output grids
    - FUNCTION [`output grid`] : Function
    - FORMULA [`text`] : Formula. Default: sin(x*x + y*y)
    - X_RANGE [`value range`] : X Range
    - Y_RANGE [`value range`] : Y Range
    - TARGET_DEFINITION [`choice`] : Target Grid System. Available Choices: [0] user defined [1] grid or grid system Default: 0
    - TARGET_USER_SIZE [`floating point number`] : Cellsize. Minimum: 0.000000 Default: 1.000000
    - TARGET_USER_XMIN [`floating point number`] : West. Default: 0.000000
    - TARGET_USER_XMAX [`floating point number`] : East. Default: 100.000000
    - TARGET_USER_YMIN [`floating point number`] : South. Default: 0.000000
    - TARGET_USER_YMAX [`floating point number`] : North. Default: 100.000000
    - TARGET_USER_COLS [`integer number`] : Columns. Minimum: 1 Default: 101 Number of cells in East-West direction.
    - TARGET_USER_ROWS [`integer number`] : Rows. Minimum: 1 Default: 101 Number of cells in North-South direction.
    - TARGET_USER_FLAT [`boolean`] : Rounding. Default: 1 Round bounding coordinates to multiples of cell size. Ignored if cell size has decimal places.
    - TARGET_USER_FITS [`choice`] : Fit. Available Choices: [0] nodes [1] cells Default: 0
    - TARGET_SYSTEM [`grid system`] : Grid System

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_calculus', '4', 'Function Plotter')
    if Tool.is_Okay():
        Tool.Set_Input ('TARGET_TEMPLATE', TARGET_TEMPLATE)
        Tool.Set_Output('FUNCTION', FUNCTION)
        Tool.Set_Option('FORMULA', FORMULA)
        Tool.Set_Option('X_RANGE', X_RANGE)
        Tool.Set_Option('Y_RANGE', Y_RANGE)
        Tool.Set_Option('TARGET_DEFINITION', TARGET_DEFINITION)
        Tool.Set_Option('TARGET_USER_SIZE', TARGET_USER_SIZE)
        Tool.Set_Option('TARGET_USER_XMIN', TARGET_USER_XMIN)
        Tool.Set_Option('TARGET_USER_XMAX', TARGET_USER_XMAX)
        Tool.Set_Option('TARGET_USER_YMIN', TARGET_USER_YMIN)
        Tool.Set_Option('TARGET_USER_YMAX', TARGET_USER_YMAX)
        Tool.Set_Option('TARGET_USER_COLS', TARGET_USER_COLS)
        Tool.Set_Option('TARGET_USER_ROWS', TARGET_USER_ROWS)
        Tool.Set_Option('TARGET_USER_FLAT', TARGET_USER_FLAT)
        Tool.Set_Option('TARGET_USER_FITS', TARGET_USER_FITS)
        Tool.Set_Option('TARGET_SYSTEM', TARGET_SYSTEM)
        return Tool.Execute(Verbose)
    return False

def run_tool_grid_calculus_4(TARGET_TEMPLATE=None, FUNCTION=None, FORMULA=None, X_RANGE=None, Y_RANGE=None, TARGET_DEFINITION=None, TARGET_USER_SIZE=None, TARGET_USER_XMIN=None, TARGET_USER_XMAX=None, TARGET_USER_YMIN=None, TARGET_USER_YMAX=None, TARGET_USER_COLS=None, TARGET_USER_ROWS=None, TARGET_USER_FLAT=None, TARGET_USER_FITS=None, TARGET_SYSTEM=None, Verbose=2):
    '''
    Function Plotter
    ----------
    [grid_calculus.4]\n
    Generate a grid based on a functional expression. The function interpreter uses an formula expression parser that offers the following operators:\n
    ============\n
    [+]	Addition\n
    [-]	Subtraction\n
    [*]	Multiplication\n
    [/]	Division\n
    [abs(x)]	Absolute Value\n
    [mod(x, y)]	Returns the floating point remainder of x/y\n
    [int(x)]	Returns the integer part of floating point value x\n
    [sqr(x)]	Square\n
    [sqrt(x)]	Square Root\n
    [exp(x)]	Exponential\n
    [pow(x, y)]	Returns x raised to the power of y\n
    [x ^ y]	Returns x raised to the power of y\n
    [ln(x)]	Natural Logarithm\n
    [log(x)]	Base 10 Logarithm\n
    [pi()]	Returns the value of Pi\n
    [sin(x)]	Sine, expects radians\n
    [cos(x)]	Cosine, expects radians\n
    [tan(x)]	Tangent, expects radians\n
    [asin(x)]	Arcsine, returns radians\n
    [acos(x)]	Arccosine, returns radians\n
    [atan(x)]	Arctangent, returns radians\n
    [atan2(x, y)]	Arctangent of x/y, returns radians\n
    [min(x, y)]	Returns the minimum of values x and y\n
    [max(x, y)]	Returns the maximum of values x and y\n
    [gt(x, y)]	Returns true (1), if x is greater than y, else false (0)\n
    [x > y]	Returns true (1), if x is greater than y, else false (0)\n
    [lt(x, y)]	Returns true (1), if x is less than y, else false (0)\n
    [x &lt; y]	Returns true (1), if x is less than y, else false (0)\n
    [eq(x, y)]	Returns true (1), if x equals y, else false (0)\n
    [x = y]	Returns true (1), if x equals y, else false (0)\n
    [and(x, y)]	Returns true (1), if both x and y are true (i.e. not 0)\n
    [or(x, y)]	Returns true (1), if at least one of both x and y is true (i.e. not 0)\n
    [ifelse(c, x, y)]	Returns x, if condition c is true (i.e. not 0), else y\n
    [rand_u(x, y)]	Random number, uniform distribution with minimum x and maximum y\n
    [rand_g(x, y)]	Random number, Gaussian distribution with mean x and standard deviation y\n
    ============\n
    Arguments
    ----------
    - TARGET_TEMPLATE [`optional input grid`] : Target System. use this grid's system for output grids
    - FUNCTION [`output grid`] : Function
    - FORMULA [`text`] : Formula. Default: sin(x*x + y*y)
    - X_RANGE [`value range`] : X Range
    - Y_RANGE [`value range`] : Y Range
    - TARGET_DEFINITION [`choice`] : Target Grid System. Available Choices: [0] user defined [1] grid or grid system Default: 0
    - TARGET_USER_SIZE [`floating point number`] : Cellsize. Minimum: 0.000000 Default: 1.000000
    - TARGET_USER_XMIN [`floating point number`] : West. Default: 0.000000
    - TARGET_USER_XMAX [`floating point number`] : East. Default: 100.000000
    - TARGET_USER_YMIN [`floating point number`] : South. Default: 0.000000
    - TARGET_USER_YMAX [`floating point number`] : North. Default: 100.000000
    - TARGET_USER_COLS [`integer number`] : Columns. Minimum: 1 Default: 101 Number of cells in East-West direction.
    - TARGET_USER_ROWS [`integer number`] : Rows. Minimum: 1 Default: 101 Number of cells in North-South direction.
    - TARGET_USER_FLAT [`boolean`] : Rounding. Default: 1 Round bounding coordinates to multiples of cell size. Ignored if cell size has decimal places.
    - TARGET_USER_FITS [`choice`] : Fit. Available Choices: [0] nodes [1] cells Default: 0
    - TARGET_SYSTEM [`grid system`] : Grid System

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_calculus', '4', 'Function Plotter')
    if Tool.is_Okay():
        Tool.Set_Input ('TARGET_TEMPLATE', TARGET_TEMPLATE)
        Tool.Set_Output('FUNCTION', FUNCTION)
        Tool.Set_Option('FORMULA', FORMULA)
        Tool.Set_Option('X_RANGE', X_RANGE)
        Tool.Set_Option('Y_RANGE', Y_RANGE)
        Tool.Set_Option('TARGET_DEFINITION', TARGET_DEFINITION)
        Tool.Set_Option('TARGET_USER_SIZE', TARGET_USER_SIZE)
        Tool.Set_Option('TARGET_USER_XMIN', TARGET_USER_XMIN)
        Tool.Set_Option('TARGET_USER_XMAX', TARGET_USER_XMAX)
        Tool.Set_Option('TARGET_USER_YMIN', TARGET_USER_YMIN)
        Tool.Set_Option('TARGET_USER_YMAX', TARGET_USER_YMAX)
        Tool.Set_Option('TARGET_USER_COLS', TARGET_USER_COLS)
        Tool.Set_Option('TARGET_USER_ROWS', TARGET_USER_ROWS)
        Tool.Set_Option('TARGET_USER_FLAT', TARGET_USER_FLAT)
        Tool.Set_Option('TARGET_USER_FITS', TARGET_USER_FITS)
        Tool.Set_Option('TARGET_SYSTEM', TARGET_SYSTEM)
        return Tool.Execute(Verbose)
    return False

def Geometric_Figures(RESULT=None, CELL_COUNT=None, CELL_SIZE=None, FIGURE=None, PLANE=None, Verbose=2):
    '''
    Geometric Figures
    ----------
    [grid_calculus.5]\n
    Construct grids from geometric figures (planes, cones).\n
    (c) 2001 by Olaf Conrad, Goettingen\n
    email: oconrad@gwdg.de\n
    Arguments
    ----------
    - RESULT [`output grid list`] : Result
    - CELL_COUNT [`integer number`] : Cell Count. Minimum: 2 Default: 100
    - CELL_SIZE [`floating point number`] : Cell Size. Minimum: 0.000000 Default: 1.000000
    - FIGURE [`choice`] : Figure. Available Choices: [0] Cone (up) [1] Cone (down) [2] Plane Default: 0
    - PLANE [`floating point number`] : Direction of Plane [Degree]. Default: 22.500000

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_calculus', '5', 'Geometric Figures')
    if Tool.is_Okay():
        Tool.Set_Output('RESULT', RESULT)
        Tool.Set_Option('CELL_COUNT', CELL_COUNT)
        Tool.Set_Option('CELL_SIZE', CELL_SIZE)
        Tool.Set_Option('FIGURE', FIGURE)
        Tool.Set_Option('PLANE', PLANE)
        return Tool.Execute(Verbose)
    return False

def run_tool_grid_calculus_5(RESULT=None, CELL_COUNT=None, CELL_SIZE=None, FIGURE=None, PLANE=None, Verbose=2):
    '''
    Geometric Figures
    ----------
    [grid_calculus.5]\n
    Construct grids from geometric figures (planes, cones).\n
    (c) 2001 by Olaf Conrad, Goettingen\n
    email: oconrad@gwdg.de\n
    Arguments
    ----------
    - RESULT [`output grid list`] : Result
    - CELL_COUNT [`integer number`] : Cell Count. Minimum: 2 Default: 100
    - CELL_SIZE [`floating point number`] : Cell Size. Minimum: 0.000000 Default: 1.000000
    - FIGURE [`choice`] : Figure. Available Choices: [0] Cone (up) [1] Cone (down) [2] Plane Default: 0
    - PLANE [`floating point number`] : Direction of Plane [Degree]. Default: 22.500000

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_calculus', '5', 'Geometric Figures')
    if Tool.is_Okay():
        Tool.Set_Output('RESULT', RESULT)
        Tool.Set_Option('CELL_COUNT', CELL_COUNT)
        Tool.Set_Option('CELL_SIZE', CELL_SIZE)
        Tool.Set_Option('FIGURE', FIGURE)
        Tool.Set_Option('PLANE', PLANE)
        return Tool.Execute(Verbose)
    return False

def Random_Terrain(TARGET_TEMPLATE=None, TARGET_OUT_GRID=None, RADIUS=None, ITERATIONS=None, TARGET_DEFINITION=None, TARGET_USER_SIZE=None, TARGET_USER_XMIN=None, TARGET_USER_XMAX=None, TARGET_USER_YMIN=None, TARGET_USER_YMAX=None, TARGET_USER_COLS=None, TARGET_USER_ROWS=None, TARGET_USER_FLAT=None, TARGET_USER_FITS=None, TARGET_SYSTEM=None, Verbose=2):
    '''
    Random Terrain
    ----------
    [grid_calculus.6]\n
    (c) 2004 by Victor Olaya. Random Terrain Generation\n
    Arguments
    ----------
    - TARGET_TEMPLATE [`optional input grid`] : Target System. use this grid's system for output grids
    - TARGET_OUT_GRID [`output grid`] : Target Grid
    - RADIUS [`integer number`] : Radius (cells). Minimum: 1 Default: 25
    - ITERATIONS [`integer number`] : Iterations. Minimum: 1 Default: 100
    - TARGET_DEFINITION [`choice`] : Target Grid System. Available Choices: [0] user defined [1] grid or grid system Default: 0
    - TARGET_USER_SIZE [`floating point number`] : Cellsize. Minimum: 0.000000 Default: 1.000000
    - TARGET_USER_XMIN [`floating point number`] : West. Default: 0.000000
    - TARGET_USER_XMAX [`floating point number`] : East. Default: 100.000000
    - TARGET_USER_YMIN [`floating point number`] : South. Default: 0.000000
    - TARGET_USER_YMAX [`floating point number`] : North. Default: 100.000000
    - TARGET_USER_COLS [`integer number`] : Columns. Minimum: 1 Default: 101 Number of cells in East-West direction.
    - TARGET_USER_ROWS [`integer number`] : Rows. Minimum: 1 Default: 101 Number of cells in North-South direction.
    - TARGET_USER_FLAT [`boolean`] : Rounding. Default: 1 Round bounding coordinates to multiples of cell size. Ignored if cell size has decimal places.
    - TARGET_USER_FITS [`choice`] : Fit. Available Choices: [0] nodes [1] cells Default: 0
    - TARGET_SYSTEM [`grid system`] : Grid System

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_calculus', '6', 'Random Terrain')
    if Tool.is_Okay():
        Tool.Set_Input ('TARGET_TEMPLATE', TARGET_TEMPLATE)
        Tool.Set_Output('TARGET_OUT_GRID', TARGET_OUT_GRID)
        Tool.Set_Option('RADIUS', RADIUS)
        Tool.Set_Option('ITERATIONS', ITERATIONS)
        Tool.Set_Option('TARGET_DEFINITION', TARGET_DEFINITION)
        Tool.Set_Option('TARGET_USER_SIZE', TARGET_USER_SIZE)
        Tool.Set_Option('TARGET_USER_XMIN', TARGET_USER_XMIN)
        Tool.Set_Option('TARGET_USER_XMAX', TARGET_USER_XMAX)
        Tool.Set_Option('TARGET_USER_YMIN', TARGET_USER_YMIN)
        Tool.Set_Option('TARGET_USER_YMAX', TARGET_USER_YMAX)
        Tool.Set_Option('TARGET_USER_COLS', TARGET_USER_COLS)
        Tool.Set_Option('TARGET_USER_ROWS', TARGET_USER_ROWS)
        Tool.Set_Option('TARGET_USER_FLAT', TARGET_USER_FLAT)
        Tool.Set_Option('TARGET_USER_FITS', TARGET_USER_FITS)
        Tool.Set_Option('TARGET_SYSTEM', TARGET_SYSTEM)
        return Tool.Execute(Verbose)
    return False

def run_tool_grid_calculus_6(TARGET_TEMPLATE=None, TARGET_OUT_GRID=None, RADIUS=None, ITERATIONS=None, TARGET_DEFINITION=None, TARGET_USER_SIZE=None, TARGET_USER_XMIN=None, TARGET_USER_XMAX=None, TARGET_USER_YMIN=None, TARGET_USER_YMAX=None, TARGET_USER_COLS=None, TARGET_USER_ROWS=None, TARGET_USER_FLAT=None, TARGET_USER_FITS=None, TARGET_SYSTEM=None, Verbose=2):
    '''
    Random Terrain
    ----------
    [grid_calculus.6]\n
    (c) 2004 by Victor Olaya. Random Terrain Generation\n
    Arguments
    ----------
    - TARGET_TEMPLATE [`optional input grid`] : Target System. use this grid's system for output grids
    - TARGET_OUT_GRID [`output grid`] : Target Grid
    - RADIUS [`integer number`] : Radius (cells). Minimum: 1 Default: 25
    - ITERATIONS [`integer number`] : Iterations. Minimum: 1 Default: 100
    - TARGET_DEFINITION [`choice`] : Target Grid System. Available Choices: [0] user defined [1] grid or grid system Default: 0
    - TARGET_USER_SIZE [`floating point number`] : Cellsize. Minimum: 0.000000 Default: 1.000000
    - TARGET_USER_XMIN [`floating point number`] : West. Default: 0.000000
    - TARGET_USER_XMAX [`floating point number`] : East. Default: 100.000000
    - TARGET_USER_YMIN [`floating point number`] : South. Default: 0.000000
    - TARGET_USER_YMAX [`floating point number`] : North. Default: 100.000000
    - TARGET_USER_COLS [`integer number`] : Columns. Minimum: 1 Default: 101 Number of cells in East-West direction.
    - TARGET_USER_ROWS [`integer number`] : Rows. Minimum: 1 Default: 101 Number of cells in North-South direction.
    - TARGET_USER_FLAT [`boolean`] : Rounding. Default: 1 Round bounding coordinates to multiples of cell size. Ignored if cell size has decimal places.
    - TARGET_USER_FITS [`choice`] : Fit. Available Choices: [0] nodes [1] cells Default: 0
    - TARGET_SYSTEM [`grid system`] : Grid System

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_calculus', '6', 'Random Terrain')
    if Tool.is_Okay():
        Tool.Set_Input ('TARGET_TEMPLATE', TARGET_TEMPLATE)
        Tool.Set_Output('TARGET_OUT_GRID', TARGET_OUT_GRID)
        Tool.Set_Option('RADIUS', RADIUS)
        Tool.Set_Option('ITERATIONS', ITERATIONS)
        Tool.Set_Option('TARGET_DEFINITION', TARGET_DEFINITION)
        Tool.Set_Option('TARGET_USER_SIZE', TARGET_USER_SIZE)
        Tool.Set_Option('TARGET_USER_XMIN', TARGET_USER_XMIN)
        Tool.Set_Option('TARGET_USER_XMAX', TARGET_USER_XMAX)
        Tool.Set_Option('TARGET_USER_YMIN', TARGET_USER_YMIN)
        Tool.Set_Option('TARGET_USER_YMAX', TARGET_USER_YMAX)
        Tool.Set_Option('TARGET_USER_COLS', TARGET_USER_COLS)
        Tool.Set_Option('TARGET_USER_ROWS', TARGET_USER_ROWS)
        Tool.Set_Option('TARGET_USER_FLAT', TARGET_USER_FLAT)
        Tool.Set_Option('TARGET_USER_FITS', TARGET_USER_FITS)
        Tool.Set_Option('TARGET_SYSTEM', TARGET_SYSTEM)
        return Tool.Execute(Verbose)
    return False

def Random_Field(TARGET_TEMPLATE=None, OUT_GRID=None, METHOD=None, RANGE=None, MEAN=None, STDDEV=None, TARGET_DEFINITION=None, TARGET_USER_SIZE=None, TARGET_USER_XMIN=None, TARGET_USER_XMAX=None, TARGET_USER_YMIN=None, TARGET_USER_YMAX=None, TARGET_USER_COLS=None, TARGET_USER_ROWS=None, TARGET_USER_FLAT=None, TARGET_USER_FITS=None, TARGET_SYSTEM=None, Verbose=2):
    '''
    Random Field
    ----------
    [grid_calculus.7]\n
    Create a grid with pseudo-random numbers as grid cell values.\n
    Arguments
    ----------
    - TARGET_TEMPLATE [`optional input grid`] : Target System. use this grid's system for output grids
    - OUT_GRID [`output grid`] : Random Field
    - METHOD [`choice`] : Method. Available Choices: [0] Uniform [1] Gaussian Default: 1
    - RANGE [`value range`] : Range
    - MEAN [`floating point number`] : Arithmetic Mean. Default: 0.000000
    - STDDEV [`floating point number`] : Standard Deviation. Minimum: 0.000000 Default: 1.000000
    - TARGET_DEFINITION [`choice`] : Target Grid System. Available Choices: [0] user defined [1] grid or grid system Default: 0
    - TARGET_USER_SIZE [`floating point number`] : Cellsize. Minimum: 0.000000 Default: 1.000000
    - TARGET_USER_XMIN [`floating point number`] : West. Default: 0.000000
    - TARGET_USER_XMAX [`floating point number`] : East. Default: 100.000000
    - TARGET_USER_YMIN [`floating point number`] : South. Default: 0.000000
    - TARGET_USER_YMAX [`floating point number`] : North. Default: 100.000000
    - TARGET_USER_COLS [`integer number`] : Columns. Minimum: 1 Default: 101 Number of cells in East-West direction.
    - TARGET_USER_ROWS [`integer number`] : Rows. Minimum: 1 Default: 101 Number of cells in North-South direction.
    - TARGET_USER_FLAT [`boolean`] : Rounding. Default: 1 Round bounding coordinates to multiples of cell size. Ignored if cell size has decimal places.
    - TARGET_USER_FITS [`choice`] : Fit. Available Choices: [0] nodes [1] cells Default: 0
    - TARGET_SYSTEM [`grid system`] : Grid System

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_calculus', '7', 'Random Field')
    if Tool.is_Okay():
        Tool.Set_Input ('TARGET_TEMPLATE', TARGET_TEMPLATE)
        Tool.Set_Output('OUT_GRID', OUT_GRID)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('RANGE', RANGE)
        Tool.Set_Option('MEAN', MEAN)
        Tool.Set_Option('STDDEV', STDDEV)
        Tool.Set_Option('TARGET_DEFINITION', TARGET_DEFINITION)
        Tool.Set_Option('TARGET_USER_SIZE', TARGET_USER_SIZE)
        Tool.Set_Option('TARGET_USER_XMIN', TARGET_USER_XMIN)
        Tool.Set_Option('TARGET_USER_XMAX', TARGET_USER_XMAX)
        Tool.Set_Option('TARGET_USER_YMIN', TARGET_USER_YMIN)
        Tool.Set_Option('TARGET_USER_YMAX', TARGET_USER_YMAX)
        Tool.Set_Option('TARGET_USER_COLS', TARGET_USER_COLS)
        Tool.Set_Option('TARGET_USER_ROWS', TARGET_USER_ROWS)
        Tool.Set_Option('TARGET_USER_FLAT', TARGET_USER_FLAT)
        Tool.Set_Option('TARGET_USER_FITS', TARGET_USER_FITS)
        Tool.Set_Option('TARGET_SYSTEM', TARGET_SYSTEM)
        return Tool.Execute(Verbose)
    return False

def run_tool_grid_calculus_7(TARGET_TEMPLATE=None, OUT_GRID=None, METHOD=None, RANGE=None, MEAN=None, STDDEV=None, TARGET_DEFINITION=None, TARGET_USER_SIZE=None, TARGET_USER_XMIN=None, TARGET_USER_XMAX=None, TARGET_USER_YMIN=None, TARGET_USER_YMAX=None, TARGET_USER_COLS=None, TARGET_USER_ROWS=None, TARGET_USER_FLAT=None, TARGET_USER_FITS=None, TARGET_SYSTEM=None, Verbose=2):
    '''
    Random Field
    ----------
    [grid_calculus.7]\n
    Create a grid with pseudo-random numbers as grid cell values.\n
    Arguments
    ----------
    - TARGET_TEMPLATE [`optional input grid`] : Target System. use this grid's system for output grids
    - OUT_GRID [`output grid`] : Random Field
    - METHOD [`choice`] : Method. Available Choices: [0] Uniform [1] Gaussian Default: 1
    - RANGE [`value range`] : Range
    - MEAN [`floating point number`] : Arithmetic Mean. Default: 0.000000
    - STDDEV [`floating point number`] : Standard Deviation. Minimum: 0.000000 Default: 1.000000
    - TARGET_DEFINITION [`choice`] : Target Grid System. Available Choices: [0] user defined [1] grid or grid system Default: 0
    - TARGET_USER_SIZE [`floating point number`] : Cellsize. Minimum: 0.000000 Default: 1.000000
    - TARGET_USER_XMIN [`floating point number`] : West. Default: 0.000000
    - TARGET_USER_XMAX [`floating point number`] : East. Default: 100.000000
    - TARGET_USER_YMIN [`floating point number`] : South. Default: 0.000000
    - TARGET_USER_YMAX [`floating point number`] : North. Default: 100.000000
    - TARGET_USER_COLS [`integer number`] : Columns. Minimum: 1 Default: 101 Number of cells in East-West direction.
    - TARGET_USER_ROWS [`integer number`] : Rows. Minimum: 1 Default: 101 Number of cells in North-South direction.
    - TARGET_USER_FLAT [`boolean`] : Rounding. Default: 1 Round bounding coordinates to multiples of cell size. Ignored if cell size has decimal places.
    - TARGET_USER_FITS [`choice`] : Fit. Available Choices: [0] nodes [1] cells Default: 0
    - TARGET_SYSTEM [`grid system`] : Grid System

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_calculus', '7', 'Random Field')
    if Tool.is_Okay():
        Tool.Set_Input ('TARGET_TEMPLATE', TARGET_TEMPLATE)
        Tool.Set_Output('OUT_GRID', OUT_GRID)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('RANGE', RANGE)
        Tool.Set_Option('MEAN', MEAN)
        Tool.Set_Option('STDDEV', STDDEV)
        Tool.Set_Option('TARGET_DEFINITION', TARGET_DEFINITION)
        Tool.Set_Option('TARGET_USER_SIZE', TARGET_USER_SIZE)
        Tool.Set_Option('TARGET_USER_XMIN', TARGET_USER_XMIN)
        Tool.Set_Option('TARGET_USER_XMAX', TARGET_USER_XMAX)
        Tool.Set_Option('TARGET_USER_YMIN', TARGET_USER_YMIN)
        Tool.Set_Option('TARGET_USER_YMAX', TARGET_USER_YMAX)
        Tool.Set_Option('TARGET_USER_COLS', TARGET_USER_COLS)
        Tool.Set_Option('TARGET_USER_ROWS', TARGET_USER_ROWS)
        Tool.Set_Option('TARGET_USER_FLAT', TARGET_USER_FLAT)
        Tool.Set_Option('TARGET_USER_FITS', TARGET_USER_FITS)
        Tool.Set_Option('TARGET_SYSTEM', TARGET_SYSTEM)
        return Tool.Execute(Verbose)
    return False

def Grids_Sum(GRIDS=None, RESULT=None, NODATA=None, Verbose=2):
    '''
    Grids Sum
    ----------
    [grid_calculus.8]\n
    Calculates the sum of all input grids by cellwise addition of their grid values.\n
    Arguments
    ----------
    - GRIDS [`input grid list`] : Summands
    - RESULT [`output grid`] : Sum
    - NODATA [`boolean`] : Count No Data as Zero. Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_calculus', '8', 'Grids Sum')
    if Tool.is_Okay():
        Tool.Set_Input ('GRIDS', GRIDS)
        Tool.Set_Output('RESULT', RESULT)
        Tool.Set_Option('NODATA', NODATA)
        return Tool.Execute(Verbose)
    return False

def run_tool_grid_calculus_8(GRIDS=None, RESULT=None, NODATA=None, Verbose=2):
    '''
    Grids Sum
    ----------
    [grid_calculus.8]\n
    Calculates the sum of all input grids by cellwise addition of their grid values.\n
    Arguments
    ----------
    - GRIDS [`input grid list`] : Summands
    - RESULT [`output grid`] : Sum
    - NODATA [`boolean`] : Count No Data as Zero. Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_calculus', '8', 'Grids Sum')
    if Tool.is_Okay():
        Tool.Set_Input ('GRIDS', GRIDS)
        Tool.Set_Output('RESULT', RESULT)
        Tool.Set_Option('NODATA', NODATA)
        return Tool.Execute(Verbose)
    return False

def Grids_Product(GRIDS=None, RESULT=None, NODATA=None, Verbose=2):
    '''
    Grids Product
    ----------
    [grid_calculus.9]\n
    Calculates the product of all input grids by cellwise multiplication of their grid values.\n
    Arguments
    ----------
    - GRIDS [`input grid list`] : Grids
    - RESULT [`output grid`] : Product
    - NODATA [`boolean`] : Count No Data as Zero. Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_calculus', '9', 'Grids Product')
    if Tool.is_Okay():
        Tool.Set_Input ('GRIDS', GRIDS)
        Tool.Set_Output('RESULT', RESULT)
        Tool.Set_Option('NODATA', NODATA)
        return Tool.Execute(Verbose)
    return False

def run_tool_grid_calculus_9(GRIDS=None, RESULT=None, NODATA=None, Verbose=2):
    '''
    Grids Product
    ----------
    [grid_calculus.9]\n
    Calculates the product of all input grids by cellwise multiplication of their grid values.\n
    Arguments
    ----------
    - GRIDS [`input grid list`] : Grids
    - RESULT [`output grid`] : Product
    - NODATA [`boolean`] : Count No Data as Zero. Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_calculus', '9', 'Grids Product')
    if Tool.is_Okay():
        Tool.Set_Input ('GRIDS', GRIDS)
        Tool.Set_Output('RESULT', RESULT)
        Tool.Set_Option('NODATA', NODATA)
        return Tool.Execute(Verbose)
    return False

def Grid_Standardization(INPUT=None, OUTPUT=None, STRETCH=None, Verbose=2):
    '''
    Grid Standardization
    ----------
    [grid_calculus.10]\n
    Standardize the values of a grid. The standard score (z) is calculated as raw score (x) less arithmetic mean (m) divided by standard deviation (s) and multiplied with the stretch factor (d):\n
    z = d * (x - m) / s\n
    Arguments
    ----------
    - INPUT [`input grid`] : Grid
    - OUTPUT [`output grid`] : Standardized Grid
    - STRETCH [`floating point number`] : Stretch Factor. Default: 1.000000

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_calculus', '10', 'Grid Standardization')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Output('OUTPUT', OUTPUT)
        Tool.Set_Option('STRETCH', STRETCH)
        return Tool.Execute(Verbose)
    return False

def run_tool_grid_calculus_10(INPUT=None, OUTPUT=None, STRETCH=None, Verbose=2):
    '''
    Grid Standardization
    ----------
    [grid_calculus.10]\n
    Standardize the values of a grid. The standard score (z) is calculated as raw score (x) less arithmetic mean (m) divided by standard deviation (s) and multiplied with the stretch factor (d):\n
    z = d * (x - m) / s\n
    Arguments
    ----------
    - INPUT [`input grid`] : Grid
    - OUTPUT [`output grid`] : Standardized Grid
    - STRETCH [`floating point number`] : Stretch Factor. Default: 1.000000

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_calculus', '10', 'Grid Standardization')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Output('OUTPUT', OUTPUT)
        Tool.Set_Option('STRETCH', STRETCH)
        return Tool.Execute(Verbose)
    return False

def Fuzzify(INPUT=None, OUTPUT=None, INC_MIN=None, INC_MAX=None, DEC_MIN=None, DEC_MAX=None, METHOD=None, TRANSITION=None, INVERT=None, AUTOFIT=None, Verbose=2):
    '''
    Fuzzify
    ----------
    [grid_calculus.11]\n
    Translates grid values into fuzzy set membership as preparation for fuzzy set analysis.\n
    Arguments
    ----------
    - INPUT [`input grid`] : Grid
    - OUTPUT [`output grid`] : Fuzzified Grid
    - INC_MIN [`floating point number`] : From. Default: 0.000000
    - INC_MAX [`floating point number`] : To. Default: 0.300000
    - DEC_MIN [`floating point number`] : From. Default: 0.700000
    - DEC_MAX [`floating point number`] : To. Default: 1.000000
    - METHOD [`choice`] : Method. Available Choices: [0] Increase [1] Decrease [2] Increase and Decrease Default: 0
    - TRANSITION [`choice`] : Transition. Available Choices: [0] linear [1] sigmoidal [2] j-shaped Default: 0
    - INVERT [`boolean`] : Invert. Default: 0
    - AUTOFIT [`boolean`] : Adjust. Default: 1 Automatically adjust control points to grid's data range

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_calculus', '11', 'Fuzzify')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Output('OUTPUT', OUTPUT)
        Tool.Set_Option('INC_MIN', INC_MIN)
        Tool.Set_Option('INC_MAX', INC_MAX)
        Tool.Set_Option('DEC_MIN', DEC_MIN)
        Tool.Set_Option('DEC_MAX', DEC_MAX)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('TRANSITION', TRANSITION)
        Tool.Set_Option('INVERT', INVERT)
        Tool.Set_Option('AUTOFIT', AUTOFIT)
        return Tool.Execute(Verbose)
    return False

def run_tool_grid_calculus_11(INPUT=None, OUTPUT=None, INC_MIN=None, INC_MAX=None, DEC_MIN=None, DEC_MAX=None, METHOD=None, TRANSITION=None, INVERT=None, AUTOFIT=None, Verbose=2):
    '''
    Fuzzify
    ----------
    [grid_calculus.11]\n
    Translates grid values into fuzzy set membership as preparation for fuzzy set analysis.\n
    Arguments
    ----------
    - INPUT [`input grid`] : Grid
    - OUTPUT [`output grid`] : Fuzzified Grid
    - INC_MIN [`floating point number`] : From. Default: 0.000000
    - INC_MAX [`floating point number`] : To. Default: 0.300000
    - DEC_MIN [`floating point number`] : From. Default: 0.700000
    - DEC_MAX [`floating point number`] : To. Default: 1.000000
    - METHOD [`choice`] : Method. Available Choices: [0] Increase [1] Decrease [2] Increase and Decrease Default: 0
    - TRANSITION [`choice`] : Transition. Available Choices: [0] linear [1] sigmoidal [2] j-shaped Default: 0
    - INVERT [`boolean`] : Invert. Default: 0
    - AUTOFIT [`boolean`] : Adjust. Default: 1 Automatically adjust control points to grid's data range

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_calculus', '11', 'Fuzzify')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Output('OUTPUT', OUTPUT)
        Tool.Set_Option('INC_MIN', INC_MIN)
        Tool.Set_Option('INC_MAX', INC_MAX)
        Tool.Set_Option('DEC_MIN', DEC_MIN)
        Tool.Set_Option('DEC_MAX', DEC_MAX)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('TRANSITION', TRANSITION)
        Tool.Set_Option('INVERT', INVERT)
        Tool.Set_Option('AUTOFIT', AUTOFIT)
        return Tool.Execute(Verbose)
    return False

def Fuzzy_Intersection_AND(GRIDS=None, AND=None, TYPE=None, Verbose=2):
    '''
    Fuzzy Intersection (AND)
    ----------
    [grid_calculus.12]\n
    Calculates the intersection (min operator) for each grid cell of the selected grids.\n
    e-mail Gianluca Massei: g_massa@libero.it\n
    e-mail Antonio Boggia: boggia@unipg.it\n
    Arguments
    ----------
    - GRIDS [`input grid list`] : Grids
    - AND [`output grid`] : Intersection
    - TYPE [`choice`] : Operator Type. Available Choices: [0] min(a, b) (non-interactive) [1] a * b [2] max(0, a + b - 1) Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_calculus', '12', 'Fuzzy Intersection (AND)')
    if Tool.is_Okay():
        Tool.Set_Input ('GRIDS', GRIDS)
        Tool.Set_Output('AND', AND)
        Tool.Set_Option('TYPE', TYPE)
        return Tool.Execute(Verbose)
    return False

def run_tool_grid_calculus_12(GRIDS=None, AND=None, TYPE=None, Verbose=2):
    '''
    Fuzzy Intersection (AND)
    ----------
    [grid_calculus.12]\n
    Calculates the intersection (min operator) for each grid cell of the selected grids.\n
    e-mail Gianluca Massei: g_massa@libero.it\n
    e-mail Antonio Boggia: boggia@unipg.it\n
    Arguments
    ----------
    - GRIDS [`input grid list`] : Grids
    - AND [`output grid`] : Intersection
    - TYPE [`choice`] : Operator Type. Available Choices: [0] min(a, b) (non-interactive) [1] a * b [2] max(0, a + b - 1) Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_calculus', '12', 'Fuzzy Intersection (AND)')
    if Tool.is_Okay():
        Tool.Set_Input ('GRIDS', GRIDS)
        Tool.Set_Output('AND', AND)
        Tool.Set_Option('TYPE', TYPE)
        return Tool.Execute(Verbose)
    return False

def Fuzzy_Union_OR(GRIDS=None, OR=None, TYPE=None, Verbose=2):
    '''
    Fuzzy Union (OR)
    ----------
    [grid_calculus.13]\n
    Calculates the union (max operator) for each grid cell of the selected grids.\n
    e-mail Gianluca Massei: g_massa@libero.it\n
    e-mail Antonio Boggia: boggia@unipg.it\n
    Arguments
    ----------
    - GRIDS [`input grid list`] : Grids
    - OR [`output grid`] : Union
    - TYPE [`choice`] : Operator Type. Available Choices: [0] max(a, b) (non-interactive) [1] a + b - a * b [2] min(1, a + b) Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_calculus', '13', 'Fuzzy Union (OR)')
    if Tool.is_Okay():
        Tool.Set_Input ('GRIDS', GRIDS)
        Tool.Set_Output('OR', OR)
        Tool.Set_Option('TYPE', TYPE)
        return Tool.Execute(Verbose)
    return False

def run_tool_grid_calculus_13(GRIDS=None, OR=None, TYPE=None, Verbose=2):
    '''
    Fuzzy Union (OR)
    ----------
    [grid_calculus.13]\n
    Calculates the union (max operator) for each grid cell of the selected grids.\n
    e-mail Gianluca Massei: g_massa@libero.it\n
    e-mail Antonio Boggia: boggia@unipg.it\n
    Arguments
    ----------
    - GRIDS [`input grid list`] : Grids
    - OR [`output grid`] : Union
    - TYPE [`choice`] : Operator Type. Available Choices: [0] max(a, b) (non-interactive) [1] a + b - a * b [2] min(1, a + b) Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_calculus', '13', 'Fuzzy Union (OR)')
    if Tool.is_Okay():
        Tool.Set_Input ('GRIDS', GRIDS)
        Tool.Set_Output('OR', OR)
        Tool.Set_Option('TYPE', TYPE)
        return Tool.Execute(Verbose)
    return False

def Metric_Conversions(GRID=None, CONV=None, CONVERSION=None, Verbose=2):
    '''
    Metric Conversions
    ----------
    [grid_calculus.14]\n
    Metric Conversions\n
    Arguments
    ----------
    - GRID [`input grid`] : Grid
    - CONV [`output grid`] : Converted Grid
    - CONVERSION [`choice`] : Conversion. Available Choices: [0] radians to degree [1] degree to radians [2] Celsius to Fahrenheit [3] Fahrenheit to Celsius Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_calculus', '14', 'Metric Conversions')
    if Tool.is_Okay():
        Tool.Set_Input ('GRID', GRID)
        Tool.Set_Output('CONV', CONV)
        Tool.Set_Option('CONVERSION', CONVERSION)
        return Tool.Execute(Verbose)
    return False

def run_tool_grid_calculus_14(GRID=None, CONV=None, CONVERSION=None, Verbose=2):
    '''
    Metric Conversions
    ----------
    [grid_calculus.14]\n
    Metric Conversions\n
    Arguments
    ----------
    - GRID [`input grid`] : Grid
    - CONV [`output grid`] : Converted Grid
    - CONVERSION [`choice`] : Conversion. Available Choices: [0] radians to degree [1] degree to radians [2] Celsius to Fahrenheit [3] Fahrenheit to Celsius Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_calculus', '14', 'Metric Conversions')
    if Tool.is_Okay():
        Tool.Set_Input ('GRID', GRID)
        Tool.Set_Output('CONV', CONV)
        Tool.Set_Option('CONVERSION', CONVERSION)
        return Tool.Execute(Verbose)
    return False

def Gradient_Vector_from_Cartesian_to_Polar_Coordinates(DX=None, DY=None, DIR=None, LEN=None, UNITS=None, SYSTEM=None, SYSTEM_ZERO=None, SYSTEM_ORIENT=None, Verbose=2):
    '''
    Gradient Vector from Cartesian to Polar Coordinates
    ----------
    [grid_calculus.15]\n
    Converts gradient vector from directional components (Cartesian) to polar coordinates (direction or aspect angle and length or tangens of slope).\n
    The tool supports three conventions on how to measure and output the angle of direction:\n
    (a) mathematical: direction angle is zero in East direction and the angle increases counterclockwise\n
    (b) geographical: direction angle is zero in North direction and the angle increases clockwise\n
    (c) zero direction and orientation are user defined\n
    Arguments
    ----------
    - DX [`input grid`] : X Component
    - DY [`input grid`] : Y Component
    - DIR [`output grid`] : Direction
    - LEN [`output grid`] : Length
    - UNITS [`choice`] : Polar Angle Units. Available Choices: [0] radians [1] degree Default: 0
    - SYSTEM [`choice`] : Polar Coordinate System. Available Choices: [0] mathematical [1] geographical [2] user defined Default: 1
    - SYSTEM_ZERO [`floating point number`] : User defined Zero Direction. Minimum: 0.000000 Maximum: 360.000000 Default: 0.000000 given in degree clockwise from North direction
    - SYSTEM_ORIENT [`choice`] : User defined Orientation. Available Choices: [0] clockwise [1] counterclockwise Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_calculus', '15', 'Gradient Vector from Cartesian to Polar Coordinates')
    if Tool.is_Okay():
        Tool.Set_Input ('DX', DX)
        Tool.Set_Input ('DY', DY)
        Tool.Set_Output('DIR', DIR)
        Tool.Set_Output('LEN', LEN)
        Tool.Set_Option('UNITS', UNITS)
        Tool.Set_Option('SYSTEM', SYSTEM)
        Tool.Set_Option('SYSTEM_ZERO', SYSTEM_ZERO)
        Tool.Set_Option('SYSTEM_ORIENT', SYSTEM_ORIENT)
        return Tool.Execute(Verbose)
    return False

def run_tool_grid_calculus_15(DX=None, DY=None, DIR=None, LEN=None, UNITS=None, SYSTEM=None, SYSTEM_ZERO=None, SYSTEM_ORIENT=None, Verbose=2):
    '''
    Gradient Vector from Cartesian to Polar Coordinates
    ----------
    [grid_calculus.15]\n
    Converts gradient vector from directional components (Cartesian) to polar coordinates (direction or aspect angle and length or tangens of slope).\n
    The tool supports three conventions on how to measure and output the angle of direction:\n
    (a) mathematical: direction angle is zero in East direction and the angle increases counterclockwise\n
    (b) geographical: direction angle is zero in North direction and the angle increases clockwise\n
    (c) zero direction and orientation are user defined\n
    Arguments
    ----------
    - DX [`input grid`] : X Component
    - DY [`input grid`] : Y Component
    - DIR [`output grid`] : Direction
    - LEN [`output grid`] : Length
    - UNITS [`choice`] : Polar Angle Units. Available Choices: [0] radians [1] degree Default: 0
    - SYSTEM [`choice`] : Polar Coordinate System. Available Choices: [0] mathematical [1] geographical [2] user defined Default: 1
    - SYSTEM_ZERO [`floating point number`] : User defined Zero Direction. Minimum: 0.000000 Maximum: 360.000000 Default: 0.000000 given in degree clockwise from North direction
    - SYSTEM_ORIENT [`choice`] : User defined Orientation. Available Choices: [0] clockwise [1] counterclockwise Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_calculus', '15', 'Gradient Vector from Cartesian to Polar Coordinates')
    if Tool.is_Okay():
        Tool.Set_Input ('DX', DX)
        Tool.Set_Input ('DY', DY)
        Tool.Set_Output('DIR', DIR)
        Tool.Set_Output('LEN', LEN)
        Tool.Set_Option('UNITS', UNITS)
        Tool.Set_Option('SYSTEM', SYSTEM)
        Tool.Set_Option('SYSTEM_ZERO', SYSTEM_ZERO)
        Tool.Set_Option('SYSTEM_ORIENT', SYSTEM_ORIENT)
        return Tool.Execute(Verbose)
    return False

def Gradient_Vector_from_Polar_to_Cartesian_Coordinates(DIR=None, LEN=None, DX=None, DY=None, UNITS=None, SYSTEM=None, SYSTEM_ZERO=None, SYSTEM_ORIENT=None, Verbose=2):
    '''
    Gradient Vector from Polar to Cartesian Coordinates
    ----------
    [grid_calculus.16]\n
    Converts gradient vector from polar coordinates (direction or aspect angle and length or tangens of slope) to directional components (Cartesian).\n
    The tool supports three conventions on how the angle of direction can be supplied:\n
    (a) mathematical: direction angle is zero in East direction and the angle increases counterclockwise\n
    (b) geographical: direction angle is zero in North direction and the angle increases clockwise\n
    (c) zero direction and orientation are user defined\n
    Arguments
    ----------
    - DIR [`input grid`] : Direction
    - LEN [`input grid`] : Length
    - DX [`output grid`] : X Component
    - DY [`output grid`] : Y Component
    - UNITS [`choice`] : Polar Angle Units. Available Choices: [0] radians [1] degree Default: 0
    - SYSTEM [`choice`] : Polar Coordinate System. Available Choices: [0] mathematical [1] geographical [2] user defined Default: 1
    - SYSTEM_ZERO [`floating point number`] : User defined Zero Direction. Minimum: 0.000000 Maximum: 360.000000 Default: 0.000000 given in degree clockwise from North direction
    - SYSTEM_ORIENT [`choice`] : User defined Orientation. Available Choices: [0] clockwise [1] counterclockwise Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_calculus', '16', 'Gradient Vector from Polar to Cartesian Coordinates')
    if Tool.is_Okay():
        Tool.Set_Input ('DIR', DIR)
        Tool.Set_Input ('LEN', LEN)
        Tool.Set_Output('DX', DX)
        Tool.Set_Output('DY', DY)
        Tool.Set_Option('UNITS', UNITS)
        Tool.Set_Option('SYSTEM', SYSTEM)
        Tool.Set_Option('SYSTEM_ZERO', SYSTEM_ZERO)
        Tool.Set_Option('SYSTEM_ORIENT', SYSTEM_ORIENT)
        return Tool.Execute(Verbose)
    return False

def run_tool_grid_calculus_16(DIR=None, LEN=None, DX=None, DY=None, UNITS=None, SYSTEM=None, SYSTEM_ZERO=None, SYSTEM_ORIENT=None, Verbose=2):
    '''
    Gradient Vector from Polar to Cartesian Coordinates
    ----------
    [grid_calculus.16]\n
    Converts gradient vector from polar coordinates (direction or aspect angle and length or tangens of slope) to directional components (Cartesian).\n
    The tool supports three conventions on how the angle of direction can be supplied:\n
    (a) mathematical: direction angle is zero in East direction and the angle increases counterclockwise\n
    (b) geographical: direction angle is zero in North direction and the angle increases clockwise\n
    (c) zero direction and orientation are user defined\n
    Arguments
    ----------
    - DIR [`input grid`] : Direction
    - LEN [`input grid`] : Length
    - DX [`output grid`] : X Component
    - DY [`output grid`] : Y Component
    - UNITS [`choice`] : Polar Angle Units. Available Choices: [0] radians [1] degree Default: 0
    - SYSTEM [`choice`] : Polar Coordinate System. Available Choices: [0] mathematical [1] geographical [2] user defined Default: 1
    - SYSTEM_ZERO [`floating point number`] : User defined Zero Direction. Minimum: 0.000000 Maximum: 360.000000 Default: 0.000000 given in degree clockwise from North direction
    - SYSTEM_ORIENT [`choice`] : User defined Orientation. Available Choices: [0] clockwise [1] counterclockwise Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_calculus', '16', 'Gradient Vector from Polar to Cartesian Coordinates')
    if Tool.is_Okay():
        Tool.Set_Input ('DIR', DIR)
        Tool.Set_Input ('LEN', LEN)
        Tool.Set_Output('DX', DX)
        Tool.Set_Output('DY', DY)
        Tool.Set_Option('UNITS', UNITS)
        Tool.Set_Option('SYSTEM', SYSTEM)
        Tool.Set_Option('SYSTEM_ZERO', SYSTEM_ZERO)
        Tool.Set_Option('SYSTEM_ORIENT', SYSTEM_ORIENT)
        return Tool.Execute(Verbose)
    return False

def Fractal_Brownian_Noise(TARGET_TEMPLATE=None, OUT_GRID=None, SCALING=None, MAX_SCALE=None, STEPS=None, RANGE=None, TARGET_DEFINITION=None, TARGET_USER_SIZE=None, TARGET_USER_XMIN=None, TARGET_USER_XMAX=None, TARGET_USER_YMIN=None, TARGET_USER_YMAX=None, TARGET_USER_COLS=None, TARGET_USER_ROWS=None, TARGET_USER_FLAT=None, TARGET_USER_FITS=None, TARGET_SYSTEM=None, Verbose=2):
    '''
    Fractal Brownian Noise
    ----------
    [grid_calculus.17]\n
    This tool uses uniform random to create a grid that resembles fractal Brownian noise (FBN). The advantage of FBN noise is that it appears to have texture to the human eye, that resembles the types of textures that are observed in nature; terrains, algae growth, clouds, etc. The degree of texture observed in the FBN grid is dependent upon the sizes of the wavelengths chosen. The wavelengths should be chosen so they increase in size (a doubling of successive wavelengths is a good point to start). The greater the magnitude of the "ramp" of successive wavelengths the greater the texture in the FBN grid.\n
    Arguments
    ----------
    - TARGET_TEMPLATE [`optional input grid`] : Target System. use this grid's system for output grids
    - OUT_GRID [`output grid`] : Fractal Brownian Noise
    - SCALING [`choice`] : Scaling. Available Choices: [0] linear [1] geometric Default: 1
    - MAX_SCALE [`floating point number`] : Maximum Scale. Minimum: 0.000000 Default: 1.000000
    - STEPS [`integer number`] : Steps. Minimum: 1 Default: 8
    - RANGE [`value range`] : Noise Range
    - TARGET_DEFINITION [`choice`] : Target Grid System. Available Choices: [0] user defined [1] grid or grid system Default: 0
    - TARGET_USER_SIZE [`floating point number`] : Cellsize. Minimum: 0.000000 Default: 1.000000
    - TARGET_USER_XMIN [`floating point number`] : West. Default: 0.000000
    - TARGET_USER_XMAX [`floating point number`] : East. Default: 100.000000
    - TARGET_USER_YMIN [`floating point number`] : South. Default: 0.000000
    - TARGET_USER_YMAX [`floating point number`] : North. Default: 100.000000
    - TARGET_USER_COLS [`integer number`] : Columns. Minimum: 1 Default: 101 Number of cells in East-West direction.
    - TARGET_USER_ROWS [`integer number`] : Rows. Minimum: 1 Default: 101 Number of cells in North-South direction.
    - TARGET_USER_FLAT [`boolean`] : Rounding. Default: 1 Round bounding coordinates to multiples of cell size. Ignored if cell size has decimal places.
    - TARGET_USER_FITS [`choice`] : Fit. Available Choices: [0] nodes [1] cells Default: 0
    - TARGET_SYSTEM [`grid system`] : Grid System

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_calculus', '17', 'Fractal Brownian Noise')
    if Tool.is_Okay():
        Tool.Set_Input ('TARGET_TEMPLATE', TARGET_TEMPLATE)
        Tool.Set_Output('OUT_GRID', OUT_GRID)
        Tool.Set_Option('SCALING', SCALING)
        Tool.Set_Option('MAX_SCALE', MAX_SCALE)
        Tool.Set_Option('STEPS', STEPS)
        Tool.Set_Option('RANGE', RANGE)
        Tool.Set_Option('TARGET_DEFINITION', TARGET_DEFINITION)
        Tool.Set_Option('TARGET_USER_SIZE', TARGET_USER_SIZE)
        Tool.Set_Option('TARGET_USER_XMIN', TARGET_USER_XMIN)
        Tool.Set_Option('TARGET_USER_XMAX', TARGET_USER_XMAX)
        Tool.Set_Option('TARGET_USER_YMIN', TARGET_USER_YMIN)
        Tool.Set_Option('TARGET_USER_YMAX', TARGET_USER_YMAX)
        Tool.Set_Option('TARGET_USER_COLS', TARGET_USER_COLS)
        Tool.Set_Option('TARGET_USER_ROWS', TARGET_USER_ROWS)
        Tool.Set_Option('TARGET_USER_FLAT', TARGET_USER_FLAT)
        Tool.Set_Option('TARGET_USER_FITS', TARGET_USER_FITS)
        Tool.Set_Option('TARGET_SYSTEM', TARGET_SYSTEM)
        return Tool.Execute(Verbose)
    return False

def run_tool_grid_calculus_17(TARGET_TEMPLATE=None, OUT_GRID=None, SCALING=None, MAX_SCALE=None, STEPS=None, RANGE=None, TARGET_DEFINITION=None, TARGET_USER_SIZE=None, TARGET_USER_XMIN=None, TARGET_USER_XMAX=None, TARGET_USER_YMIN=None, TARGET_USER_YMAX=None, TARGET_USER_COLS=None, TARGET_USER_ROWS=None, TARGET_USER_FLAT=None, TARGET_USER_FITS=None, TARGET_SYSTEM=None, Verbose=2):
    '''
    Fractal Brownian Noise
    ----------
    [grid_calculus.17]\n
    This tool uses uniform random to create a grid that resembles fractal Brownian noise (FBN). The advantage of FBN noise is that it appears to have texture to the human eye, that resembles the types of textures that are observed in nature; terrains, algae growth, clouds, etc. The degree of texture observed in the FBN grid is dependent upon the sizes of the wavelengths chosen. The wavelengths should be chosen so they increase in size (a doubling of successive wavelengths is a good point to start). The greater the magnitude of the "ramp" of successive wavelengths the greater the texture in the FBN grid.\n
    Arguments
    ----------
    - TARGET_TEMPLATE [`optional input grid`] : Target System. use this grid's system for output grids
    - OUT_GRID [`output grid`] : Fractal Brownian Noise
    - SCALING [`choice`] : Scaling. Available Choices: [0] linear [1] geometric Default: 1
    - MAX_SCALE [`floating point number`] : Maximum Scale. Minimum: 0.000000 Default: 1.000000
    - STEPS [`integer number`] : Steps. Minimum: 1 Default: 8
    - RANGE [`value range`] : Noise Range
    - TARGET_DEFINITION [`choice`] : Target Grid System. Available Choices: [0] user defined [1] grid or grid system Default: 0
    - TARGET_USER_SIZE [`floating point number`] : Cellsize. Minimum: 0.000000 Default: 1.000000
    - TARGET_USER_XMIN [`floating point number`] : West. Default: 0.000000
    - TARGET_USER_XMAX [`floating point number`] : East. Default: 100.000000
    - TARGET_USER_YMIN [`floating point number`] : South. Default: 0.000000
    - TARGET_USER_YMAX [`floating point number`] : North. Default: 100.000000
    - TARGET_USER_COLS [`integer number`] : Columns. Minimum: 1 Default: 101 Number of cells in East-West direction.
    - TARGET_USER_ROWS [`integer number`] : Rows. Minimum: 1 Default: 101 Number of cells in North-South direction.
    - TARGET_USER_FLAT [`boolean`] : Rounding. Default: 1 Round bounding coordinates to multiples of cell size. Ignored if cell size has decimal places.
    - TARGET_USER_FITS [`choice`] : Fit. Available Choices: [0] nodes [1] cells Default: 0
    - TARGET_SYSTEM [`grid system`] : Grid System

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_calculus', '17', 'Fractal Brownian Noise')
    if Tool.is_Okay():
        Tool.Set_Input ('TARGET_TEMPLATE', TARGET_TEMPLATE)
        Tool.Set_Output('OUT_GRID', OUT_GRID)
        Tool.Set_Option('SCALING', SCALING)
        Tool.Set_Option('MAX_SCALE', MAX_SCALE)
        Tool.Set_Option('STEPS', STEPS)
        Tool.Set_Option('RANGE', RANGE)
        Tool.Set_Option('TARGET_DEFINITION', TARGET_DEFINITION)
        Tool.Set_Option('TARGET_USER_SIZE', TARGET_USER_SIZE)
        Tool.Set_Option('TARGET_USER_XMIN', TARGET_USER_XMIN)
        Tool.Set_Option('TARGET_USER_XMAX', TARGET_USER_XMAX)
        Tool.Set_Option('TARGET_USER_YMIN', TARGET_USER_YMIN)
        Tool.Set_Option('TARGET_USER_YMAX', TARGET_USER_YMAX)
        Tool.Set_Option('TARGET_USER_COLS', TARGET_USER_COLS)
        Tool.Set_Option('TARGET_USER_ROWS', TARGET_USER_ROWS)
        Tool.Set_Option('TARGET_USER_FLAT', TARGET_USER_FLAT)
        Tool.Set_Option('TARGET_USER_FITS', TARGET_USER_FITS)
        Tool.Set_Option('TARGET_SYSTEM', TARGET_SYSTEM)
        return Tool.Execute(Verbose)
    return False

def Grid_Division(A=None, B=None, C=None, B_DEFAULT=None, Verbose=2):
    '''
    Grid Division
    ----------
    [grid_calculus.18]\n
    Grid Division\n
    Arguments
    ----------
    - A [`input grid`] : Dividend
    - B [`optional input grid`] : Divisor
    - C [`output grid`] : Quotient
    - B_DEFAULT [`floating point number`] : Default. Default: 1.000000 default value if no grid has been selected

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_calculus', '18', 'Grid Division')
    if Tool.is_Okay():
        Tool.Set_Input ('A', A)
        Tool.Set_Input ('B', B)
        Tool.Set_Output('C', C)
        Tool.Set_Option('B_DEFAULT', B_DEFAULT)
        return Tool.Execute(Verbose)
    return False

def run_tool_grid_calculus_18(A=None, B=None, C=None, B_DEFAULT=None, Verbose=2):
    '''
    Grid Division
    ----------
    [grid_calculus.18]\n
    Grid Division\n
    Arguments
    ----------
    - A [`input grid`] : Dividend
    - B [`optional input grid`] : Divisor
    - C [`output grid`] : Quotient
    - B_DEFAULT [`floating point number`] : Default. Default: 1.000000 default value if no grid has been selected

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_calculus', '18', 'Grid Division')
    if Tool.is_Okay():
        Tool.Set_Input ('A', A)
        Tool.Set_Input ('B', B)
        Tool.Set_Output('C', C)
        Tool.Set_Option('B_DEFAULT', B_DEFAULT)
        return Tool.Execute(Verbose)
    return False

def Spherical_Harmonic_Synthesis(OUTPUT_GRID=None, FILE=None, MINDEGREE=None, MAXDEGREE=None, LAT_START=None, END_LAT=None, LONG_START=None, END_LONG=None, INC=None, Verbose=2):
    '''
    Spherical Harmonic Synthesis
    ----------
    [grid_calculus.19]\n
    Synthesis of a completely normalized spherical harmonic expansion. The coefficients are read from the input file (ASCII file, columns separated by space).\n
    Arguments
    ----------
    - OUTPUT_GRID [`output data object`] : Synthesized Grid. Synthesized Grid
    - FILE [`file path`] : File with Coefficients. ASCII file with columns degree, order, c_lm, s_lm (separated by space)
    - MINDEGREE [`integer number`] : Start Degree. Default: 0 Start Degree
    - MAXDEGREE [`integer number`] : Expansion Degree. Default: 180 Expansion Degree
    - LAT_START [`floating point number`] : Start Latitude. Default: -90.000000 Start Latitude
    - END_LAT [`floating point number`] : End Latitude. Default: 90.000000 End Latitude
    - LONG_START [`floating point number`] : Start Longitude. Default: -180.000000 Start Longitude
    - END_LONG [`floating point number`] : End Longitude. Default: 180.000000 End Longitude
    - INC [`floating point number`] : Latitude / Longitude Increment. Default: 1.000000 Latitude / Longitude Increment

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_calculus', '19', 'Spherical Harmonic Synthesis')
    if Tool.is_Okay():
        Tool.Set_Output('OUTPUT_GRID', OUTPUT_GRID)
        Tool.Set_Option('FILE', FILE)
        Tool.Set_Option('MINDEGREE', MINDEGREE)
        Tool.Set_Option('MAXDEGREE', MAXDEGREE)
        Tool.Set_Option('LAT_START', LAT_START)
        Tool.Set_Option('END_LAT', END_LAT)
        Tool.Set_Option('LONG_START', LONG_START)
        Tool.Set_Option('END_LONG', END_LONG)
        Tool.Set_Option('INC', INC)
        return Tool.Execute(Verbose)
    return False

def run_tool_grid_calculus_19(OUTPUT_GRID=None, FILE=None, MINDEGREE=None, MAXDEGREE=None, LAT_START=None, END_LAT=None, LONG_START=None, END_LONG=None, INC=None, Verbose=2):
    '''
    Spherical Harmonic Synthesis
    ----------
    [grid_calculus.19]\n
    Synthesis of a completely normalized spherical harmonic expansion. The coefficients are read from the input file (ASCII file, columns separated by space).\n
    Arguments
    ----------
    - OUTPUT_GRID [`output data object`] : Synthesized Grid. Synthesized Grid
    - FILE [`file path`] : File with Coefficients. ASCII file with columns degree, order, c_lm, s_lm (separated by space)
    - MINDEGREE [`integer number`] : Start Degree. Default: 0 Start Degree
    - MAXDEGREE [`integer number`] : Expansion Degree. Default: 180 Expansion Degree
    - LAT_START [`floating point number`] : Start Latitude. Default: -90.000000 Start Latitude
    - END_LAT [`floating point number`] : End Latitude. Default: 90.000000 End Latitude
    - LONG_START [`floating point number`] : Start Longitude. Default: -180.000000 Start Longitude
    - END_LONG [`floating point number`] : End Longitude. Default: 180.000000 End Longitude
    - INC [`floating point number`] : Latitude / Longitude Increment. Default: 1.000000 Latitude / Longitude Increment

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_calculus', '19', 'Spherical Harmonic Synthesis')
    if Tool.is_Okay():
        Tool.Set_Output('OUTPUT_GRID', OUTPUT_GRID)
        Tool.Set_Option('FILE', FILE)
        Tool.Set_Option('MINDEGREE', MINDEGREE)
        Tool.Set_Option('MAXDEGREE', MAXDEGREE)
        Tool.Set_Option('LAT_START', LAT_START)
        Tool.Set_Option('END_LAT', END_LAT)
        Tool.Set_Option('LONG_START', LONG_START)
        Tool.Set_Option('END_LONG', END_LONG)
        Tool.Set_Option('INC', INC)
        return Tool.Execute(Verbose)
    return False

def Grid_Collection_Calculator(GRIDS=None, XGRIDS=None, RESULT=None, RESAMPLING=None, FORMULA=None, USE_NODATA=None, TYPE=None, Verbose=2):
    '''
    Grid Collection Calculator
    ----------
    [grid_calculus.20]\n
    The Grid Collection Calculator creates a new grid collection combining existing ones using the given formula. It is assumed that all input grid collections have the same number of grid layers. The variables in the formula begin with the letter 'g' followed by a position index, which corresponds to the order of the grid collections in the input grid collection list (i.e.: g1, g2, g3, ... correspond to the first, second, third, ... grid collection in list). Grid collections from other systems than the default one can be addressed likewise using the letter 'h' (h1, h2, h3, ...), which correspond to the 'Grid collections from different Systems' list.\n
    Example:	 sin(g1) * g2 + 2 * h1\n
    To make complex formulas look more intuitive you have the option to use shortcuts. Shortcuts are defined following the formula separated by semicolons as 'shortcut = expression'.\n
    Example:	 ifelse(lt(NDVI, 0.4), nodata(), NDVI); NDVI = (g1 - g2) / (g1 + g2)\n
    The following operators are available for the formula definition:\n
    ============\n
    [+]	Addition\n
    [-]	Subtraction\n
    [*]	Multiplication\n
    [/]	Division\n
    [abs(x)]	Absolute Value\n
    [mod(x, y)]	Returns the floating point remainder of x/y\n
    [int(x)]	Returns the integer part of floating point value x\n
    [sqr(x)]	Square\n
    [sqrt(x)]	Square Root\n
    [exp(x)]	Exponential\n
    [pow(x, y)]	Returns x raised to the power of y\n
    [x ^ y]	Returns x raised to the power of y\n
    [ln(x)]	Natural Logarithm\n
    [log(x)]	Base 10 Logarithm\n
    [pi()]	Returns the value of Pi\n
    [sin(x)]	Sine, expects radians\n
    [cos(x)]	Cosine, expects radians\n
    [tan(x)]	Tangent, expects radians\n
    [asin(x)]	Arcsine, returns radians\n
    [acos(x)]	Arccosine, returns radians\n
    [atan(x)]	Arctangent, returns radians\n
    [atan2(x, y)]	Arctangent of x/y, returns radians\n
    [min(x, y)]	Returns the minimum of values x and y\n
    [max(x, y)]	Returns the maximum of values x and y\n
    [gt(x, y)]	Returns true (1), if x is greater than y, else false (0)\n
    [x > y]	Returns true (1), if x is greater than y, else false (0)\n
    [lt(x, y)]	Returns true (1), if x is less than y, else false (0)\n
    [x &lt; y]	Returns true (1), if x is less than y, else false (0)\n
    [eq(x, y)]	Returns true (1), if x equals y, else false (0)\n
    [x = y]	Returns true (1), if x equals y, else false (0)\n
    [and(x, y)]	Returns true (1), if both x and y are true (i.e. not 0)\n
    [or(x, y)]	Returns true (1), if at least one of both x and y is true (i.e. not 0)\n
    [ifelse(c, x, y)]	Returns x, if condition c is true (i.e. not 0), else y\n
    [rand_u(x, y)]	Random number, uniform distribution with minimum x and maximum y\n
    [rand_g(x, y)]	Random number, Gaussian distribution with mean x and standard deviation y\n
    [xpos(), ypos()]	The coordinate (x/y) for the center of the currently processed cell\n
    [col(), row()]	The currently processed cell's column/row index\n
    [ncols(), nrows()]	Number of the grid system's columns/rows\n
    [nodata(), nodata(g)]	No-data value of the resulting (empty) or requested grid collection (g = g1...gn, h1...hn)\n
    [cellsize(), cellsize(g)]	Cell size of the resulting (empty) or requested grid collection (g = h1...hn)\n
    [cellarea(), cellarea(g)]	Cell area of the resulting (empty) or requested grid collection (g = h1...hn)\n
    [xmin(), xmin(g)]	Left bound of the resulting (empty) or requested grid collection (g = h1...hn)\n
    [xmax(), xmax(g)]	Right bound of the resulting (empty) or requested grid collection (g = h1...hn)\n
    [xrange(), xrange(g)]	Left to right range of the resulting (empty) or requested grid collection (g = h1...hn)\n
    [ymin(), ymin(g)]	Lower bound of the resulting (empty) or requested grid collection (g = h1...hn)\n
    [ymax(), ymax(g)]	Upper bound of the resulting (empty) or requested grid collection (g = h1...hn)\n
    [yrange(), yrange(g)]	Lower to upper range of the resulting (empty) or requested grid collection (g = h1...hn)\n
    [zmin(g)]	Minimum value of the requested grid collection (g = g1...gn, h1...hn)\n
    [zmax(g)]	Maximum value of the requested grid collection (g = g1...gn, h1...hn)\n
    [zrange(g)]	Value range of the requested grid collection (g = g1...gn, h1...hn)\n
    [zmean(g)]	Mean value of the requested grid collection (g = g1...gn, h1...hn)\n
    [zstddev(g)]	Standard deviation of the requested grid collection (g = g1...gn, h1...hn)\n
    ============\n
    Arguments
    ----------
    - GRIDS [`input grid collection list`] : Grid Collections. in the formula these grid collections are addressed in order of the list as 'g1, g2, g3, ...'
    - XGRIDS [`optional input grid collection list`] : Grid Collections from different Systems. in the formula these grid collections are addressed in order of the list as 'h1, h2, h3, ...'
    - RESULT [`output grid collection`] : Result
    - RESAMPLING [`choice`] : Resampling. Available Choices: [0] Nearest Neighbour [1] Bilinear Interpolation [2] Bicubic Spline Interpolation [3] B-Spline Interpolation Default: 3
    - FORMULA [`text`] : Formula. Default: (g1 - g2) / (g1 + g2)
    - USE_NODATA [`boolean`] : Use No-Data. Default: 0 Check this in order to include no-data cells in the calculation.
    - TYPE [`data type`] : Data Type. Available Choices: [0] bit [1] unsigned 1 byte integer [2] signed 1 byte integer [3] unsigned 2 byte integer [4] signed 2 byte integer [5] unsigned 4 byte integer [6] signed 4 byte integer [7] unsigned 8 byte integer [8] signed 8 byte integer [9] 4 byte floating point number [10] 8 byte floating point number Default: 9

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_calculus', '20', 'Grid Collection Calculator')
    if Tool.is_Okay():
        Tool.Set_Input ('GRIDS', GRIDS)
        Tool.Set_Input ('XGRIDS', XGRIDS)
        Tool.Set_Output('RESULT', RESULT)
        Tool.Set_Option('RESAMPLING', RESAMPLING)
        Tool.Set_Option('FORMULA', FORMULA)
        Tool.Set_Option('USE_NODATA', USE_NODATA)
        Tool.Set_Option('TYPE', TYPE)
        return Tool.Execute(Verbose)
    return False

def run_tool_grid_calculus_20(GRIDS=None, XGRIDS=None, RESULT=None, RESAMPLING=None, FORMULA=None, USE_NODATA=None, TYPE=None, Verbose=2):
    '''
    Grid Collection Calculator
    ----------
    [grid_calculus.20]\n
    The Grid Collection Calculator creates a new grid collection combining existing ones using the given formula. It is assumed that all input grid collections have the same number of grid layers. The variables in the formula begin with the letter 'g' followed by a position index, which corresponds to the order of the grid collections in the input grid collection list (i.e.: g1, g2, g3, ... correspond to the first, second, third, ... grid collection in list). Grid collections from other systems than the default one can be addressed likewise using the letter 'h' (h1, h2, h3, ...), which correspond to the 'Grid collections from different Systems' list.\n
    Example:	 sin(g1) * g2 + 2 * h1\n
    To make complex formulas look more intuitive you have the option to use shortcuts. Shortcuts are defined following the formula separated by semicolons as 'shortcut = expression'.\n
    Example:	 ifelse(lt(NDVI, 0.4), nodata(), NDVI); NDVI = (g1 - g2) / (g1 + g2)\n
    The following operators are available for the formula definition:\n
    ============\n
    [+]	Addition\n
    [-]	Subtraction\n
    [*]	Multiplication\n
    [/]	Division\n
    [abs(x)]	Absolute Value\n
    [mod(x, y)]	Returns the floating point remainder of x/y\n
    [int(x)]	Returns the integer part of floating point value x\n
    [sqr(x)]	Square\n
    [sqrt(x)]	Square Root\n
    [exp(x)]	Exponential\n
    [pow(x, y)]	Returns x raised to the power of y\n
    [x ^ y]	Returns x raised to the power of y\n
    [ln(x)]	Natural Logarithm\n
    [log(x)]	Base 10 Logarithm\n
    [pi()]	Returns the value of Pi\n
    [sin(x)]	Sine, expects radians\n
    [cos(x)]	Cosine, expects radians\n
    [tan(x)]	Tangent, expects radians\n
    [asin(x)]	Arcsine, returns radians\n
    [acos(x)]	Arccosine, returns radians\n
    [atan(x)]	Arctangent, returns radians\n
    [atan2(x, y)]	Arctangent of x/y, returns radians\n
    [min(x, y)]	Returns the minimum of values x and y\n
    [max(x, y)]	Returns the maximum of values x and y\n
    [gt(x, y)]	Returns true (1), if x is greater than y, else false (0)\n
    [x > y]	Returns true (1), if x is greater than y, else false (0)\n
    [lt(x, y)]	Returns true (1), if x is less than y, else false (0)\n
    [x &lt; y]	Returns true (1), if x is less than y, else false (0)\n
    [eq(x, y)]	Returns true (1), if x equals y, else false (0)\n
    [x = y]	Returns true (1), if x equals y, else false (0)\n
    [and(x, y)]	Returns true (1), if both x and y are true (i.e. not 0)\n
    [or(x, y)]	Returns true (1), if at least one of both x and y is true (i.e. not 0)\n
    [ifelse(c, x, y)]	Returns x, if condition c is true (i.e. not 0), else y\n
    [rand_u(x, y)]	Random number, uniform distribution with minimum x and maximum y\n
    [rand_g(x, y)]	Random number, Gaussian distribution with mean x and standard deviation y\n
    [xpos(), ypos()]	The coordinate (x/y) for the center of the currently processed cell\n
    [col(), row()]	The currently processed cell's column/row index\n
    [ncols(), nrows()]	Number of the grid system's columns/rows\n
    [nodata(), nodata(g)]	No-data value of the resulting (empty) or requested grid collection (g = g1...gn, h1...hn)\n
    [cellsize(), cellsize(g)]	Cell size of the resulting (empty) or requested grid collection (g = h1...hn)\n
    [cellarea(), cellarea(g)]	Cell area of the resulting (empty) or requested grid collection (g = h1...hn)\n
    [xmin(), xmin(g)]	Left bound of the resulting (empty) or requested grid collection (g = h1...hn)\n
    [xmax(), xmax(g)]	Right bound of the resulting (empty) or requested grid collection (g = h1...hn)\n
    [xrange(), xrange(g)]	Left to right range of the resulting (empty) or requested grid collection (g = h1...hn)\n
    [ymin(), ymin(g)]	Lower bound of the resulting (empty) or requested grid collection (g = h1...hn)\n
    [ymax(), ymax(g)]	Upper bound of the resulting (empty) or requested grid collection (g = h1...hn)\n
    [yrange(), yrange(g)]	Lower to upper range of the resulting (empty) or requested grid collection (g = h1...hn)\n
    [zmin(g)]	Minimum value of the requested grid collection (g = g1...gn, h1...hn)\n
    [zmax(g)]	Maximum value of the requested grid collection (g = g1...gn, h1...hn)\n
    [zrange(g)]	Value range of the requested grid collection (g = g1...gn, h1...hn)\n
    [zmean(g)]	Mean value of the requested grid collection (g = g1...gn, h1...hn)\n
    [zstddev(g)]	Standard deviation of the requested grid collection (g = g1...gn, h1...hn)\n
    ============\n
    Arguments
    ----------
    - GRIDS [`input grid collection list`] : Grid Collections. in the formula these grid collections are addressed in order of the list as 'g1, g2, g3, ...'
    - XGRIDS [`optional input grid collection list`] : Grid Collections from different Systems. in the formula these grid collections are addressed in order of the list as 'h1, h2, h3, ...'
    - RESULT [`output grid collection`] : Result
    - RESAMPLING [`choice`] : Resampling. Available Choices: [0] Nearest Neighbour [1] Bilinear Interpolation [2] Bicubic Spline Interpolation [3] B-Spline Interpolation Default: 3
    - FORMULA [`text`] : Formula. Default: (g1 - g2) / (g1 + g2)
    - USE_NODATA [`boolean`] : Use No-Data. Default: 0 Check this in order to include no-data cells in the calculation.
    - TYPE [`data type`] : Data Type. Available Choices: [0] bit [1] unsigned 1 byte integer [2] signed 1 byte integer [3] unsigned 2 byte integer [4] signed 2 byte integer [5] unsigned 4 byte integer [6] signed 4 byte integer [7] unsigned 8 byte integer [8] signed 8 byte integer [9] 4 byte floating point number [10] 8 byte floating point number Default: 9

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_calculus', '20', 'Grid Collection Calculator')
    if Tool.is_Okay():
        Tool.Set_Input ('GRIDS', GRIDS)
        Tool.Set_Input ('XGRIDS', XGRIDS)
        Tool.Set_Output('RESULT', RESULT)
        Tool.Set_Option('RESAMPLING', RESAMPLING)
        Tool.Set_Option('FORMULA', FORMULA)
        Tool.Set_Option('USE_NODATA', USE_NODATA)
        Tool.Set_Option('TYPE', TYPE)
        return Tool.Execute(Verbose)
    return False

def Histogram_Matching(GRID=None, REFERENCE=None, MATCHED=None, REFERENCE_GRIDSYSTEM=None, METHOD=None, NCLASSES=None, MAXSAMPLES=None, Verbose=2):
    '''
    Histogram Matching
    ----------
    [grid_calculus.21]\n
    This tool alters the values of a grid so that its value distribution (its histogram), matches that of a reference grid. The first method simply uses arithmetic mean and standard deviation for adjustment, which usually is sufficient for normal distributed values. The second method performs a more precise adjustment based on the grids' histograms.\n
    Arguments
    ----------
    - GRID [`input grid`] : Grid
    - REFERENCE [`input grid`] : Reference Grid
    - MATCHED [`output grid`] : Adjusted Grid
    - REFERENCE_GRIDSYSTEM [`grid system`] : Grid system
    - METHOD [`choice`] : Method. Available Choices: [0] standard deviation [1] histogram Default: 1
    - NCLASSES [`integer number`] : Histogramm Classes. Minimum: 100 Default: 10 Number of histogram classes for internal use.
    - MAXSAMPLES [`integer number`] : Maximum Sample Size. Minimum: 0 Default: 1000000 If set to zero all data will be used to build the histograms.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_calculus', '21', 'Histogram Matching')
    if Tool.is_Okay():
        Tool.Set_Input ('GRID', GRID)
        Tool.Set_Input ('REFERENCE', REFERENCE)
        Tool.Set_Output('MATCHED', MATCHED)
        Tool.Set_Option('REFERENCE_GRIDSYSTEM', REFERENCE_GRIDSYSTEM)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('NCLASSES', NCLASSES)
        Tool.Set_Option('MAXSAMPLES', MAXSAMPLES)
        return Tool.Execute(Verbose)
    return False

def run_tool_grid_calculus_21(GRID=None, REFERENCE=None, MATCHED=None, REFERENCE_GRIDSYSTEM=None, METHOD=None, NCLASSES=None, MAXSAMPLES=None, Verbose=2):
    '''
    Histogram Matching
    ----------
    [grid_calculus.21]\n
    This tool alters the values of a grid so that its value distribution (its histogram), matches that of a reference grid. The first method simply uses arithmetic mean and standard deviation for adjustment, which usually is sufficient for normal distributed values. The second method performs a more precise adjustment based on the grids' histograms.\n
    Arguments
    ----------
    - GRID [`input grid`] : Grid
    - REFERENCE [`input grid`] : Reference Grid
    - MATCHED [`output grid`] : Adjusted Grid
    - REFERENCE_GRIDSYSTEM [`grid system`] : Grid system
    - METHOD [`choice`] : Method. Available Choices: [0] standard deviation [1] histogram Default: 1
    - NCLASSES [`integer number`] : Histogramm Classes. Minimum: 100 Default: 10 Number of histogram classes for internal use.
    - MAXSAMPLES [`integer number`] : Maximum Sample Size. Minimum: 0 Default: 1000000 If set to zero all data will be used to build the histograms.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_calculus', '21', 'Histogram Matching')
    if Tool.is_Okay():
        Tool.Set_Input ('GRID', GRID)
        Tool.Set_Input ('REFERENCE', REFERENCE)
        Tool.Set_Output('MATCHED', MATCHED)
        Tool.Set_Option('REFERENCE_GRIDSYSTEM', REFERENCE_GRIDSYSTEM)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('NCLASSES', NCLASSES)
        Tool.Set_Option('MAXSAMPLES', MAXSAMPLES)
        return Tool.Execute(Verbose)
    return False

def Grid_Addition(A=None, B=None, C=None, B_DEFAULT=None, Verbose=2):
    '''
    Grid Addition
    ----------
    [grid_calculus.22]\n
    Grid Addition\n
    Arguments
    ----------
    - A [`input grid`] : Summand 1
    - B [`optional input grid`] : Summand 2. The grid or values being added.
    - C [`output grid`] : Sum
    - B_DEFAULT [`floating point number`] : Default. Default: 0.000000 default value if no grid has been selected

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_calculus', '22', 'Grid Addition')
    if Tool.is_Okay():
        Tool.Set_Input ('A', A)
        Tool.Set_Input ('B', B)
        Tool.Set_Output('C', C)
        Tool.Set_Option('B_DEFAULT', B_DEFAULT)
        return Tool.Execute(Verbose)
    return False

def run_tool_grid_calculus_22(A=None, B=None, C=None, B_DEFAULT=None, Verbose=2):
    '''
    Grid Addition
    ----------
    [grid_calculus.22]\n
    Grid Addition\n
    Arguments
    ----------
    - A [`input grid`] : Summand 1
    - B [`optional input grid`] : Summand 2. The grid or values being added.
    - C [`output grid`] : Sum
    - B_DEFAULT [`floating point number`] : Default. Default: 0.000000 default value if no grid has been selected

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_calculus', '22', 'Grid Addition')
    if Tool.is_Okay():
        Tool.Set_Input ('A', A)
        Tool.Set_Input ('B', B)
        Tool.Set_Output('C', C)
        Tool.Set_Option('B_DEFAULT', B_DEFAULT)
        return Tool.Execute(Verbose)
    return False

def Grid_Multiplication(A=None, B=None, C=None, B_DEFAULT=None, Verbose=2):
    '''
    Grid Multiplication
    ----------
    [grid_calculus.23]\n
    Grid Multiplication\n
    Arguments
    ----------
    - A [`input grid`] : Multiplicand
    - B [`optional input grid`] : Multiplier
    - C [`output grid`] : Product
    - B_DEFAULT [`floating point number`] : Default. Default: 1.000000 default value if no grid has been selected

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_calculus', '23', 'Grid Multiplication')
    if Tool.is_Okay():
        Tool.Set_Input ('A', A)
        Tool.Set_Input ('B', B)
        Tool.Set_Output('C', C)
        Tool.Set_Option('B_DEFAULT', B_DEFAULT)
        return Tool.Execute(Verbose)
    return False

def run_tool_grid_calculus_23(A=None, B=None, C=None, B_DEFAULT=None, Verbose=2):
    '''
    Grid Multiplication
    ----------
    [grid_calculus.23]\n
    Grid Multiplication\n
    Arguments
    ----------
    - A [`input grid`] : Multiplicand
    - B [`optional input grid`] : Multiplier
    - C [`output grid`] : Product
    - B_DEFAULT [`floating point number`] : Default. Default: 1.000000 default value if no grid has been selected

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_calculus', '23', 'Grid Multiplication')
    if Tool.is_Okay():
        Tool.Set_Input ('A', A)
        Tool.Set_Input ('B', B)
        Tool.Set_Output('C', C)
        Tool.Set_Option('B_DEFAULT', B_DEFAULT)
        return Tool.Execute(Verbose)
    return False

