#! /usr/bin/env python

'''
Python Interface to SAGA Tools Library
----------
- Category : Import/Export
- Name     : GPS Tools
- ID       : io_gps

Description
----------
Tools for GPS data handling.
'''

from PySAGA.helper import Tool_Wrapper

def GPX_to_shapefile(BASEPATH=None, FILE=None, TRACKPOINTS=None, WAYPOINTS=None, ROUTES=None, ADD=None, Verbose=2):
    '''
    GPX to shapefile
    ----------
    [io_gps.0]\n
    Converts a GPX file into a Shapefile (.shp)(c) 2005 by Victor Olaya\n
    email: volaya@ya.com\n
    Arguments
    ----------
    - BASEPATH [`file path`] : Gpx2shp path. Gpx2shp path
    - FILE [`file path`] : GPX file
    - TRACKPOINTS [`boolean`] : Convert track points. Default: 1 Convert track points
    - WAYPOINTS [`boolean`] : Convert way points. Default: 1 Convert way points
    - ROUTES [`boolean`] : Convert routes. Default: 1 Convert routes
    - ADD [`boolean`] : Load shapefile. Default: 1 Load shapefile after conversion

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('io_gps', '0', 'GPX to shapefile')
    if Tool.is_Okay():
        Tool.Set_Option('BASEPATH', BASEPATH)
        Tool.Set_Option('FILE', FILE)
        Tool.Set_Option('TRACKPOINTS', TRACKPOINTS)
        Tool.Set_Option('WAYPOINTS', WAYPOINTS)
        Tool.Set_Option('ROUTES', ROUTES)
        Tool.Set_Option('ADD', ADD)
        return Tool.Execute(Verbose)
    return False

def run_tool_io_gps_0(BASEPATH=None, FILE=None, TRACKPOINTS=None, WAYPOINTS=None, ROUTES=None, ADD=None, Verbose=2):
    '''
    GPX to shapefile
    ----------
    [io_gps.0]\n
    Converts a GPX file into a Shapefile (.shp)(c) 2005 by Victor Olaya\n
    email: volaya@ya.com\n
    Arguments
    ----------
    - BASEPATH [`file path`] : Gpx2shp path. Gpx2shp path
    - FILE [`file path`] : GPX file
    - TRACKPOINTS [`boolean`] : Convert track points. Default: 1 Convert track points
    - WAYPOINTS [`boolean`] : Convert way points. Default: 1 Convert way points
    - ROUTES [`boolean`] : Convert routes. Default: 1 Convert routes
    - ADD [`boolean`] : Load shapefile. Default: 1 Load shapefile after conversion

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('io_gps', '0', 'GPX to shapefile')
    if Tool.is_Okay():
        Tool.Set_Option('BASEPATH', BASEPATH)
        Tool.Set_Option('FILE', FILE)
        Tool.Set_Option('TRACKPOINTS', TRACKPOINTS)
        Tool.Set_Option('WAYPOINTS', WAYPOINTS)
        Tool.Set_Option('ROUTES', ROUTES)
        Tool.Set_Option('ADD', ADD)
        return Tool.Execute(Verbose)
    return False

def GPSBabel(BASEPATH=None, INPUT=None, FORMATIN=None, OUTPUT=None, FORMATOUT=None, Verbose=2):
    '''
    GPSBabel
    ----------
    [io_gps.1]\n
    An interface to the GPSBabel software(c) 2005 by Victor Olaya\n
    email: volaya@ya.com\n
    Arguments
    ----------
    - BASEPATH [`file path`] : GPSBabel path. GPSBabel path
    - INPUT [`file path`] : Input file
    - FORMATIN [`choice`] : Input format. Available Choices: [0] Geocaching.com .loc [1] GPSman [2] GPX XML [3] Magellan protocol [4] Magellan Mapsend [5] Garmin PCX5 [6] Garmin Mapsource [7] gpsutil [8] U.S. Census Bureau Tiger Mapping Service [9] Comma separated values [10] Delorme Topo USA4/XMap Conduit [11] Navitrak DNA marker format [12] MS PocketStreets 2002 Pushpin [13] Cetus for Palm/OS [14] GPSPilot Tracker for Palm/OS [15] Magellan NAV Companion for PalmOS [16] Garmin serial protocol [17] MapTech Exchange Format [18] Holux (gm-100) .wpo Format [19] OziExplorer Waypoint [20] National Geographic Topo .tpg [21] TopoMapPro Places File Default: 0
    - OUTPUT [`file path`] : Output file
    - FORMATOUT [`choice`] : Output format. Available Choices: [0] Geocaching.com .loc [1] GPSman [2] GPX XML [3] Magellan protocol [4] Magellan Mapsend [5] Garmin PCX5 [6] Garmin Mapsource [7] gpsutil [8] U.S. Census Bureau Tiger Mapping Service [9] Comma separated values [10] Delorme Topo USA4/XMap Conduit [11] Navitrak DNA marker format [12] MS PocketStreets 2002 Pushpin [13] Cetus for Palm/OS [14] GPSPilot Tracker for Palm/OS [15] Magellan NAV Companion for PalmOS [16] Garmin serial protocol [17] MapTech Exchange Format [18] Holux (gm-100) .wpo Format [19] OziExplorer Waypoint [20] National Geographic Topo .tpg [21] TopoMapPro Places File Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('io_gps', '1', 'GPSBabel')
    if Tool.is_Okay():
        Tool.Set_Option('BASEPATH', BASEPATH)
        Tool.Set_Option('INPUT', INPUT)
        Tool.Set_Option('FORMATIN', FORMATIN)
        Tool.Set_Option('OUTPUT', OUTPUT)
        Tool.Set_Option('FORMATOUT', FORMATOUT)
        return Tool.Execute(Verbose)
    return False

def run_tool_io_gps_1(BASEPATH=None, INPUT=None, FORMATIN=None, OUTPUT=None, FORMATOUT=None, Verbose=2):
    '''
    GPSBabel
    ----------
    [io_gps.1]\n
    An interface to the GPSBabel software(c) 2005 by Victor Olaya\n
    email: volaya@ya.com\n
    Arguments
    ----------
    - BASEPATH [`file path`] : GPSBabel path. GPSBabel path
    - INPUT [`file path`] : Input file
    - FORMATIN [`choice`] : Input format. Available Choices: [0] Geocaching.com .loc [1] GPSman [2] GPX XML [3] Magellan protocol [4] Magellan Mapsend [5] Garmin PCX5 [6] Garmin Mapsource [7] gpsutil [8] U.S. Census Bureau Tiger Mapping Service [9] Comma separated values [10] Delorme Topo USA4/XMap Conduit [11] Navitrak DNA marker format [12] MS PocketStreets 2002 Pushpin [13] Cetus for Palm/OS [14] GPSPilot Tracker for Palm/OS [15] Magellan NAV Companion for PalmOS [16] Garmin serial protocol [17] MapTech Exchange Format [18] Holux (gm-100) .wpo Format [19] OziExplorer Waypoint [20] National Geographic Topo .tpg [21] TopoMapPro Places File Default: 0
    - OUTPUT [`file path`] : Output file
    - FORMATOUT [`choice`] : Output format. Available Choices: [0] Geocaching.com .loc [1] GPSman [2] GPX XML [3] Magellan protocol [4] Magellan Mapsend [5] Garmin PCX5 [6] Garmin Mapsource [7] gpsutil [8] U.S. Census Bureau Tiger Mapping Service [9] Comma separated values [10] Delorme Topo USA4/XMap Conduit [11] Navitrak DNA marker format [12] MS PocketStreets 2002 Pushpin [13] Cetus for Palm/OS [14] GPSPilot Tracker for Palm/OS [15] Magellan NAV Companion for PalmOS [16] Garmin serial protocol [17] MapTech Exchange Format [18] Holux (gm-100) .wpo Format [19] OziExplorer Waypoint [20] National Geographic Topo .tpg [21] TopoMapPro Places File Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('io_gps', '1', 'GPSBabel')
    if Tool.is_Okay():
        Tool.Set_Option('BASEPATH', BASEPATH)
        Tool.Set_Option('INPUT', INPUT)
        Tool.Set_Option('FORMATIN', FORMATIN)
        Tool.Set_Option('OUTPUT', OUTPUT)
        Tool.Set_Option('FORMATOUT', FORMATOUT)
        return Tool.Execute(Verbose)
    return False

