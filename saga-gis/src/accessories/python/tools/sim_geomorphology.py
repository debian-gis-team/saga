#! /usr/bin/env python

'''
Python Interface to SAGA Tools Library
----------
- Category : Simulation
- Name     : Geomorphology
- ID       : sim_geomorphology

Description
----------
This library contains tools for the simulation of geomorphological processes.
'''

from PySAGA.helper import Tool_Wrapper

def Gravitational_Process_Path_Model(DEM=None, RELEASE_AREAS=None, MATERIAL=None, FRICTION_ANGLE_GRID=None, SLOPE_IMPACT_GRID=None, FRICTION_MU_GRID=None, FRICTION_MASS_TO_DRAG_GRID=None, OBJECTS=None, PROCESS_AREA=None, DEPOSITION=None, MAX_VELOCITY=None, STOP_POSITIONS=None, HAZARD_PATHS=None, HAZARD_SOURCES=None, HAZARD_SOURCES_MATERIAL=None, MATERIAL_FLUX=None, PROCESS_PATH_MODEL=None, RW_SLOPE_THRES=None, RW_EXPONENT=None, RW_PERSISTENCE=None, GPP_ITERATIONS=None, GPP_PROCESSING_ORDER=None, GPP_SEED=None, FRICTION_MODEL=None, FRICTION_THRES_FREE_FALL=None, FRICTION_METHOD_IMPACT=None, FRICTION_IMPACT_REDUCTION=None, FRICTION_ANGLE=None, FRICTION_MU=None, FRICTION_MODE_OF_MOTION=None, FRICTION_MASS_TO_DRAG=None, FRICTION_INIT_VELOCITY=None, DEPOSITION_MODEL=None, DEPOSITION_INITIAL=None, DEPOSITION_SLOPE_THRES=None, DEPOSITION_VELOCITY_THRES=None, DEPOSITION_MAX=None, DEPOSITION_MIN_PATH=None, SINK_MIN_SLOPE=None, Verbose=2):
    '''
    Gravitational Process Path Model
    ----------
    [sim_geomorphology.0]\n
    The Gravitational Process Path (GPP) model can be used to simulate the process path and run-out area of gravitational processes based on a digital terrain model (DTM). The conceptual model combines several components (process path, run-out length, sink filling and material deposition) to simulate the movement of a mass point from an initiation site to the deposition area. For each component several modeling approaches are provided, which makes the tool configurable for different processes such as rockfall, debris flows or snow avalanches.[[The tool can be applied to regional-scale studies such as natural hazard susceptibility mapping but also contains components for scenario-based modeling of single events. Both the modeling approaches and precursor implementations of the tool have proven their applicability in numerous studies, also including geomorphological research questions such as the delineation of sediment cascades or the study of process connectivity.[[Please provide the reference cited below in your work if you are using the GPP model.[[[Addendum:][The article is not clear about the way the impact on the slope is exactly modelled when the 'Shadow Angle' or '1-parameter' friction model is used. Besides the 'Threshold Angle Free Fall' criterion to determine the location of the first impact, it is also assumed that the particle must leave its own release area (given by its ID) in order to impact. This is actually a conceptual design, taking into account that free fall usually occurs in steep rock faces (release areas), and the fact, that such rockfaces are not characterised very well in a 2.5D elevation model. You can work around that conceptual design by providing a grid describing the 'slope impact areas' as input. Using such a grid disables the 'Threshold Angle Free Fall' parameter.[[[New in version 1.1:][Since version 1.1 the model supports the monitoring of potentially endangered objects like infrastructure and reports from which process paths and release areas objects might be hit. In order to enable this backtracking, the user must provide an 'Objects' grid as input. The grid can be used to store different types or classes of objects, using one-hot categorical data encoding for each object class, i.e. powers of ten: 1, 10, 100, 1000, etc. (all other cells NoData). The 'Hazard Paths' and 'Hazard Sources' output grid will store combinations of these numbers if several different classes were hit from a grid cell, allowing to analyse which object classes might be hit from which location.[[[New in version 1.2:][Since version 1.2 the model supports the optional output of a grid with the material flux. This requires a 'Material' grid as input. The grid shows the height of the material that has passed in total through each grid cell.[[[Version 1.3:][Version 1.3 includes a fix for material deposition along the process path (amounts and the update of available material for subsequent runs) and improves the output of material flux.[[[Version 1.4:][Since version 1.4 two separate 'Endangered Objects' output grids are created, one encoding the process path cells, the other only the source cells from which objects have been hit. These optional output parameters also have been renamed ('HAZARD_PATHS', 'HAZARD_SOURCES') to improve the legibility of the parameter interface. A third grid, showing the total amount of material in each source cell that has hit objects from that position, has also been added. This output requires a material grid as input in order to calculate material flux. To determine the total amount, the highest material flux observed to hit an object per path and iteration is summed up and converted back to a material height per cell (comparable to the amount specified in the material input grid.)\n
    Arguments
    ----------
    - DEM [`input grid`] : DEM. Digital elevation model [m].
    - RELEASE_AREAS [`input grid`] : Release Areas. Release areas encoded by unique integer IDs, all other cells NoData [-].
    - MATERIAL [`optional input grid`] : Material. Height of material available in each start cell [m].
    - FRICTION_ANGLE_GRID [`optional input grid`] : Friction Angle. Spatially distributed friction angles [degree]. Optionally used with the Geometric Gradient, Fahrboeschung's angle or Shadow Angle friction model.
    - SLOPE_IMPACT_GRID [`optional input grid`] : Slope Impact Areas. Slope impact grid, impact areas encoded with valid values, all other NoData. Optionally used with the Shadow Angle or the 1-parameter friction model.
    - FRICTION_MU_GRID [`optional input grid`] : Friction Parameter Mu. Spatially distributed friction parameter mu [-], optionally used with the 1-parameter friction model or the PCM Model.
    - FRICTION_MASS_TO_DRAG_GRID [`optional input grid`] : Mass to Drag Ratio. Spatially distributed mass to drag ratio [m], optionally used with the PCM Model.
    - OBJECTS [`optional input grid`] : Objects. Potentially endangered objects (like infrastructure) to monitor, using one-hot categorical data encoding for each object class [1, 10, 100, 1000, ...].
    - PROCESS_AREA [`output grid`] : Process Area. Delineated process area with encoded transition frequencies [count].
    - DEPOSITION [`output grid`] : Deposition. Height of the material deposited in each cell [m]. Optional output in case a grid with material amounts is provided as input.
    - MAX_VELOCITY [`output grid`] : Maximum Velocity. Maximum velocity observed in each cell [m/s]. Optional output of the 1-parameter friction model and the PCM Model.
    - STOP_POSITIONS [`output grid`] : Stopping Positions. Stopping positions, showing cells in which the run-out length has been reached [count].
    - HAZARD_PATHS [`output grid`] : Hazard Paths. Process path cells from which objects were hit. Cell values indicate which object classes were hit [combination of object classes]. Optional output in case a grid with potentially endangered objects is provided as input.
    - HAZARD_SOURCES [`output grid`] : Hazard Sources. Source (release area) cells from which objects were hit. Cell values indicate which object classes were hit [combination of object classes]. Optional output in case a grid with potentially endangered objects is provided as input.
    - HAZARD_SOURCES_MATERIAL [`output grid`] : Hazard Sources Material. Source (release area) cells from which objects were hit. Cell values indicate the material amount [m/cell] that has hit objects from that source cell. Optional output in case grids with material amounts and potentially endangered objects are provided as input.
    - MATERIAL_FLUX [`output grid`] : Material Flux. Amount of material that has passed through each cell [m]. Optional output in case a grid with material amounts is provided as input.
    - PROCESS_PATH_MODEL [`choice`] : Model. Available Choices: [0] Maximum Slope [1] Random Walk Default: 1 Choose a process path model.
    - RW_SLOPE_THRES [`floating point number`] : Slope Threshold. Minimum: 0.001000 Maximum: 90.000000 Default: 40.000000 In case the local slope is greater as this threshold [degree], no lateral spreading is modeled.
    - RW_EXPONENT [`floating point number`] : Exponent. Minimum: 1.000000 Default: 2.000000 The exponent [-] is controlling the amount of lateral spreading in case the local slope is in between zero and the slope threshold.
    - RW_PERSISTENCE [`floating point number`] : Persistence Factor. Minimum: 1.000000 Default: 1.500000 Factor [-] used as weight for the current flow direction. A higher factor reduces abrupt changes in flow direction.
    - GPP_ITERATIONS [`integer number`] : Iterations. Minimum: 1 Default: 1000 The number of model runs from each start cell [-].
    - GPP_PROCESSING_ORDER [`choice`] : Processing Order. Available Choices: [0] RAs in Sequence [1] RAs in Sequence per Iteration [2] RAs in Parallel per Iteration Default: 2 Choose the processing order.
    - GPP_SEED [`integer number`] : Seed Value. Minimum: 1 Default: 1 The seed value used to initialize the pseudo-random number generator. A value of 1 will initialize the generator with the current time, higher numbers will always produce the same succession of values for each seed value [-].
    - FRICTION_MODEL [`choice`] : Model. Available Choices: [0] None [1] Geometric Gradient (Heim 1932) [2] Fahrboeschung Principle (Heim 1932) [3] Shadow Angle (Evans & Hungr 1988) [4] 1-parameter friction model (Scheidegger 1975) [5] PCM Model (Perla et al. 1980) Default: 0 Choose a friction model.
    - FRICTION_THRES_FREE_FALL [`floating point number`] : Threshold Angle Free Fall. Minimum: 0.000000 Default: 60.000000 The minimum slope angle [degree] between start cell and current cell for modeling free fall with the Shadow Angle or the 1-parameter friction model.
    - FRICTION_METHOD_IMPACT [`choice`] : Method Impact. Available Choices: [0] Energy Reduction (Scheidegger 1975) [1] Preserved Component of Velocity (Kirkby & Statham 1975) Default: 0 Choose the velocity calculation on slope impact with the 1-parameter friction model.
    - FRICTION_IMPACT_REDUCTION [`floating point number`] : Reduction. Minimum: 0.000000 Maximum: 100.000000 Default: 75.000000 The energy reduction [%] on slope impact with the 1-parameter friction model.
    - FRICTION_ANGLE [`floating point number`] : Friction Angle. Minimum: 0.000000 Maximum: 90.000000 Default: 30.000000 Friction angle [degree] used as Geometric Gradient, Fahrboeschung's angle or Shadow Angle.
    - FRICTION_MU [`floating point number`] : Mu. Minimum: 0.000000 Default: 0.250000 The (constant) friction parameter mu [-] used with the 1-parameter friction model or the PCM Model.
    - FRICTION_MODE_OF_MOTION [`choice`] : Mode of Motion. Available Choices: [0] Sliding [1] Rolling Default: 0 Choose the mode of motion on hillslope with the 1-parameter friction model.
    - FRICTION_MASS_TO_DRAG [`floating point number`] : Mass to Drag Ratio. Minimum: 0.000000 Default: 200.000000 The (constant) mass to drag ratio [m] used with the PCM Model.
    - FRICTION_INIT_VELOCITY [`floating point number`] : Initial Velocity. Minimum: 0.000000 Default: 1.000000 The initial velocity [m/s] used with the PCM Model.
    - DEPOSITION_MODEL [`choice`] : Model. Available Choices: [0] None [1] On Stop [2] Slope & On Stop [3] Velocity & On Stop [4] min(Slope,Velocity) & On Stop Default: 0 Choose a deposition model.
    - DEPOSITION_INITIAL [`floating point number`] : Initial Deposition on Stop. Minimum: 0.000000 Maximum: 100.000000 Default: 20.000000 The percentage of available material (per run) initially deposited at the stopping position [%].
    - DEPOSITION_SLOPE_THRES [`floating point number`] : Slope Threshold. Minimum: 0.000000 Maximum: 90.000000 Default: 20.000000 The slope angle below which the deposition of material is starting [degree].
    - DEPOSITION_VELOCITY_THRES [`floating point number`] : Velocity Threshold. Minimum: 0.000000 Default: 15.000000 The velocity below which the deposition of material is starting [m/s].
    - DEPOSITION_MAX [`floating point number`] : Maximum Deposition along Path. Minimum: 0.000000 Maximum: 100.000000 Default: 20.000000 The percentage of available material (per run) which is deposited at most (slope or velocity equal zero) [%].
    - DEPOSITION_MIN_PATH [`floating point number`] : Minimum Path Length. Minimum: 0.000000 Default: 100.000000 The minimum path length which has to be reached before material deposition is enabled [m].
    - SINK_MIN_SLOPE [`floating point number`] : Minimum Slope. Minimum: 0.000000 Maximum: 90.000000 Default: 2.500000 The minimum slope to preserve on sink filling [degree].

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('sim_geomorphology', '0', 'Gravitational Process Path Model')
    if Tool.is_Okay():
        Tool.Set_Input ('DEM', DEM)
        Tool.Set_Input ('RELEASE_AREAS', RELEASE_AREAS)
        Tool.Set_Input ('MATERIAL', MATERIAL)
        Tool.Set_Input ('FRICTION_ANGLE_GRID', FRICTION_ANGLE_GRID)
        Tool.Set_Input ('SLOPE_IMPACT_GRID', SLOPE_IMPACT_GRID)
        Tool.Set_Input ('FRICTION_MU_GRID', FRICTION_MU_GRID)
        Tool.Set_Input ('FRICTION_MASS_TO_DRAG_GRID', FRICTION_MASS_TO_DRAG_GRID)
        Tool.Set_Input ('OBJECTS', OBJECTS)
        Tool.Set_Output('PROCESS_AREA', PROCESS_AREA)
        Tool.Set_Output('DEPOSITION', DEPOSITION)
        Tool.Set_Output('MAX_VELOCITY', MAX_VELOCITY)
        Tool.Set_Output('STOP_POSITIONS', STOP_POSITIONS)
        Tool.Set_Output('HAZARD_PATHS', HAZARD_PATHS)
        Tool.Set_Output('HAZARD_SOURCES', HAZARD_SOURCES)
        Tool.Set_Output('HAZARD_SOURCES_MATERIAL', HAZARD_SOURCES_MATERIAL)
        Tool.Set_Output('MATERIAL_FLUX', MATERIAL_FLUX)
        Tool.Set_Option('PROCESS_PATH_MODEL', PROCESS_PATH_MODEL)
        Tool.Set_Option('RW_SLOPE_THRES', RW_SLOPE_THRES)
        Tool.Set_Option('RW_EXPONENT', RW_EXPONENT)
        Tool.Set_Option('RW_PERSISTENCE', RW_PERSISTENCE)
        Tool.Set_Option('GPP_ITERATIONS', GPP_ITERATIONS)
        Tool.Set_Option('GPP_PROCESSING_ORDER', GPP_PROCESSING_ORDER)
        Tool.Set_Option('GPP_SEED', GPP_SEED)
        Tool.Set_Option('FRICTION_MODEL', FRICTION_MODEL)
        Tool.Set_Option('FRICTION_THRES_FREE_FALL', FRICTION_THRES_FREE_FALL)
        Tool.Set_Option('FRICTION_METHOD_IMPACT', FRICTION_METHOD_IMPACT)
        Tool.Set_Option('FRICTION_IMPACT_REDUCTION', FRICTION_IMPACT_REDUCTION)
        Tool.Set_Option('FRICTION_ANGLE', FRICTION_ANGLE)
        Tool.Set_Option('FRICTION_MU', FRICTION_MU)
        Tool.Set_Option('FRICTION_MODE_OF_MOTION', FRICTION_MODE_OF_MOTION)
        Tool.Set_Option('FRICTION_MASS_TO_DRAG', FRICTION_MASS_TO_DRAG)
        Tool.Set_Option('FRICTION_INIT_VELOCITY', FRICTION_INIT_VELOCITY)
        Tool.Set_Option('DEPOSITION_MODEL', DEPOSITION_MODEL)
        Tool.Set_Option('DEPOSITION_INITIAL', DEPOSITION_INITIAL)
        Tool.Set_Option('DEPOSITION_SLOPE_THRES', DEPOSITION_SLOPE_THRES)
        Tool.Set_Option('DEPOSITION_VELOCITY_THRES', DEPOSITION_VELOCITY_THRES)
        Tool.Set_Option('DEPOSITION_MAX', DEPOSITION_MAX)
        Tool.Set_Option('DEPOSITION_MIN_PATH', DEPOSITION_MIN_PATH)
        Tool.Set_Option('SINK_MIN_SLOPE', SINK_MIN_SLOPE)
        return Tool.Execute(Verbose)
    return False

def run_tool_sim_geomorphology_0(DEM=None, RELEASE_AREAS=None, MATERIAL=None, FRICTION_ANGLE_GRID=None, SLOPE_IMPACT_GRID=None, FRICTION_MU_GRID=None, FRICTION_MASS_TO_DRAG_GRID=None, OBJECTS=None, PROCESS_AREA=None, DEPOSITION=None, MAX_VELOCITY=None, STOP_POSITIONS=None, HAZARD_PATHS=None, HAZARD_SOURCES=None, HAZARD_SOURCES_MATERIAL=None, MATERIAL_FLUX=None, PROCESS_PATH_MODEL=None, RW_SLOPE_THRES=None, RW_EXPONENT=None, RW_PERSISTENCE=None, GPP_ITERATIONS=None, GPP_PROCESSING_ORDER=None, GPP_SEED=None, FRICTION_MODEL=None, FRICTION_THRES_FREE_FALL=None, FRICTION_METHOD_IMPACT=None, FRICTION_IMPACT_REDUCTION=None, FRICTION_ANGLE=None, FRICTION_MU=None, FRICTION_MODE_OF_MOTION=None, FRICTION_MASS_TO_DRAG=None, FRICTION_INIT_VELOCITY=None, DEPOSITION_MODEL=None, DEPOSITION_INITIAL=None, DEPOSITION_SLOPE_THRES=None, DEPOSITION_VELOCITY_THRES=None, DEPOSITION_MAX=None, DEPOSITION_MIN_PATH=None, SINK_MIN_SLOPE=None, Verbose=2):
    '''
    Gravitational Process Path Model
    ----------
    [sim_geomorphology.0]\n
    The Gravitational Process Path (GPP) model can be used to simulate the process path and run-out area of gravitational processes based on a digital terrain model (DTM). The conceptual model combines several components (process path, run-out length, sink filling and material deposition) to simulate the movement of a mass point from an initiation site to the deposition area. For each component several modeling approaches are provided, which makes the tool configurable for different processes such as rockfall, debris flows or snow avalanches.[[The tool can be applied to regional-scale studies such as natural hazard susceptibility mapping but also contains components for scenario-based modeling of single events. Both the modeling approaches and precursor implementations of the tool have proven their applicability in numerous studies, also including geomorphological research questions such as the delineation of sediment cascades or the study of process connectivity.[[Please provide the reference cited below in your work if you are using the GPP model.[[[Addendum:][The article is not clear about the way the impact on the slope is exactly modelled when the 'Shadow Angle' or '1-parameter' friction model is used. Besides the 'Threshold Angle Free Fall' criterion to determine the location of the first impact, it is also assumed that the particle must leave its own release area (given by its ID) in order to impact. This is actually a conceptual design, taking into account that free fall usually occurs in steep rock faces (release areas), and the fact, that such rockfaces are not characterised very well in a 2.5D elevation model. You can work around that conceptual design by providing a grid describing the 'slope impact areas' as input. Using such a grid disables the 'Threshold Angle Free Fall' parameter.[[[New in version 1.1:][Since version 1.1 the model supports the monitoring of potentially endangered objects like infrastructure and reports from which process paths and release areas objects might be hit. In order to enable this backtracking, the user must provide an 'Objects' grid as input. The grid can be used to store different types or classes of objects, using one-hot categorical data encoding for each object class, i.e. powers of ten: 1, 10, 100, 1000, etc. (all other cells NoData). The 'Hazard Paths' and 'Hazard Sources' output grid will store combinations of these numbers if several different classes were hit from a grid cell, allowing to analyse which object classes might be hit from which location.[[[New in version 1.2:][Since version 1.2 the model supports the optional output of a grid with the material flux. This requires a 'Material' grid as input. The grid shows the height of the material that has passed in total through each grid cell.[[[Version 1.3:][Version 1.3 includes a fix for material deposition along the process path (amounts and the update of available material for subsequent runs) and improves the output of material flux.[[[Version 1.4:][Since version 1.4 two separate 'Endangered Objects' output grids are created, one encoding the process path cells, the other only the source cells from which objects have been hit. These optional output parameters also have been renamed ('HAZARD_PATHS', 'HAZARD_SOURCES') to improve the legibility of the parameter interface. A third grid, showing the total amount of material in each source cell that has hit objects from that position, has also been added. This output requires a material grid as input in order to calculate material flux. To determine the total amount, the highest material flux observed to hit an object per path and iteration is summed up and converted back to a material height per cell (comparable to the amount specified in the material input grid.)\n
    Arguments
    ----------
    - DEM [`input grid`] : DEM. Digital elevation model [m].
    - RELEASE_AREAS [`input grid`] : Release Areas. Release areas encoded by unique integer IDs, all other cells NoData [-].
    - MATERIAL [`optional input grid`] : Material. Height of material available in each start cell [m].
    - FRICTION_ANGLE_GRID [`optional input grid`] : Friction Angle. Spatially distributed friction angles [degree]. Optionally used with the Geometric Gradient, Fahrboeschung's angle or Shadow Angle friction model.
    - SLOPE_IMPACT_GRID [`optional input grid`] : Slope Impact Areas. Slope impact grid, impact areas encoded with valid values, all other NoData. Optionally used with the Shadow Angle or the 1-parameter friction model.
    - FRICTION_MU_GRID [`optional input grid`] : Friction Parameter Mu. Spatially distributed friction parameter mu [-], optionally used with the 1-parameter friction model or the PCM Model.
    - FRICTION_MASS_TO_DRAG_GRID [`optional input grid`] : Mass to Drag Ratio. Spatially distributed mass to drag ratio [m], optionally used with the PCM Model.
    - OBJECTS [`optional input grid`] : Objects. Potentially endangered objects (like infrastructure) to monitor, using one-hot categorical data encoding for each object class [1, 10, 100, 1000, ...].
    - PROCESS_AREA [`output grid`] : Process Area. Delineated process area with encoded transition frequencies [count].
    - DEPOSITION [`output grid`] : Deposition. Height of the material deposited in each cell [m]. Optional output in case a grid with material amounts is provided as input.
    - MAX_VELOCITY [`output grid`] : Maximum Velocity. Maximum velocity observed in each cell [m/s]. Optional output of the 1-parameter friction model and the PCM Model.
    - STOP_POSITIONS [`output grid`] : Stopping Positions. Stopping positions, showing cells in which the run-out length has been reached [count].
    - HAZARD_PATHS [`output grid`] : Hazard Paths. Process path cells from which objects were hit. Cell values indicate which object classes were hit [combination of object classes]. Optional output in case a grid with potentially endangered objects is provided as input.
    - HAZARD_SOURCES [`output grid`] : Hazard Sources. Source (release area) cells from which objects were hit. Cell values indicate which object classes were hit [combination of object classes]. Optional output in case a grid with potentially endangered objects is provided as input.
    - HAZARD_SOURCES_MATERIAL [`output grid`] : Hazard Sources Material. Source (release area) cells from which objects were hit. Cell values indicate the material amount [m/cell] that has hit objects from that source cell. Optional output in case grids with material amounts and potentially endangered objects are provided as input.
    - MATERIAL_FLUX [`output grid`] : Material Flux. Amount of material that has passed through each cell [m]. Optional output in case a grid with material amounts is provided as input.
    - PROCESS_PATH_MODEL [`choice`] : Model. Available Choices: [0] Maximum Slope [1] Random Walk Default: 1 Choose a process path model.
    - RW_SLOPE_THRES [`floating point number`] : Slope Threshold. Minimum: 0.001000 Maximum: 90.000000 Default: 40.000000 In case the local slope is greater as this threshold [degree], no lateral spreading is modeled.
    - RW_EXPONENT [`floating point number`] : Exponent. Minimum: 1.000000 Default: 2.000000 The exponent [-] is controlling the amount of lateral spreading in case the local slope is in between zero and the slope threshold.
    - RW_PERSISTENCE [`floating point number`] : Persistence Factor. Minimum: 1.000000 Default: 1.500000 Factor [-] used as weight for the current flow direction. A higher factor reduces abrupt changes in flow direction.
    - GPP_ITERATIONS [`integer number`] : Iterations. Minimum: 1 Default: 1000 The number of model runs from each start cell [-].
    - GPP_PROCESSING_ORDER [`choice`] : Processing Order. Available Choices: [0] RAs in Sequence [1] RAs in Sequence per Iteration [2] RAs in Parallel per Iteration Default: 2 Choose the processing order.
    - GPP_SEED [`integer number`] : Seed Value. Minimum: 1 Default: 1 The seed value used to initialize the pseudo-random number generator. A value of 1 will initialize the generator with the current time, higher numbers will always produce the same succession of values for each seed value [-].
    - FRICTION_MODEL [`choice`] : Model. Available Choices: [0] None [1] Geometric Gradient (Heim 1932) [2] Fahrboeschung Principle (Heim 1932) [3] Shadow Angle (Evans & Hungr 1988) [4] 1-parameter friction model (Scheidegger 1975) [5] PCM Model (Perla et al. 1980) Default: 0 Choose a friction model.
    - FRICTION_THRES_FREE_FALL [`floating point number`] : Threshold Angle Free Fall. Minimum: 0.000000 Default: 60.000000 The minimum slope angle [degree] between start cell and current cell for modeling free fall with the Shadow Angle or the 1-parameter friction model.
    - FRICTION_METHOD_IMPACT [`choice`] : Method Impact. Available Choices: [0] Energy Reduction (Scheidegger 1975) [1] Preserved Component of Velocity (Kirkby & Statham 1975) Default: 0 Choose the velocity calculation on slope impact with the 1-parameter friction model.
    - FRICTION_IMPACT_REDUCTION [`floating point number`] : Reduction. Minimum: 0.000000 Maximum: 100.000000 Default: 75.000000 The energy reduction [%] on slope impact with the 1-parameter friction model.
    - FRICTION_ANGLE [`floating point number`] : Friction Angle. Minimum: 0.000000 Maximum: 90.000000 Default: 30.000000 Friction angle [degree] used as Geometric Gradient, Fahrboeschung's angle or Shadow Angle.
    - FRICTION_MU [`floating point number`] : Mu. Minimum: 0.000000 Default: 0.250000 The (constant) friction parameter mu [-] used with the 1-parameter friction model or the PCM Model.
    - FRICTION_MODE_OF_MOTION [`choice`] : Mode of Motion. Available Choices: [0] Sliding [1] Rolling Default: 0 Choose the mode of motion on hillslope with the 1-parameter friction model.
    - FRICTION_MASS_TO_DRAG [`floating point number`] : Mass to Drag Ratio. Minimum: 0.000000 Default: 200.000000 The (constant) mass to drag ratio [m] used with the PCM Model.
    - FRICTION_INIT_VELOCITY [`floating point number`] : Initial Velocity. Minimum: 0.000000 Default: 1.000000 The initial velocity [m/s] used with the PCM Model.
    - DEPOSITION_MODEL [`choice`] : Model. Available Choices: [0] None [1] On Stop [2] Slope & On Stop [3] Velocity & On Stop [4] min(Slope,Velocity) & On Stop Default: 0 Choose a deposition model.
    - DEPOSITION_INITIAL [`floating point number`] : Initial Deposition on Stop. Minimum: 0.000000 Maximum: 100.000000 Default: 20.000000 The percentage of available material (per run) initially deposited at the stopping position [%].
    - DEPOSITION_SLOPE_THRES [`floating point number`] : Slope Threshold. Minimum: 0.000000 Maximum: 90.000000 Default: 20.000000 The slope angle below which the deposition of material is starting [degree].
    - DEPOSITION_VELOCITY_THRES [`floating point number`] : Velocity Threshold. Minimum: 0.000000 Default: 15.000000 The velocity below which the deposition of material is starting [m/s].
    - DEPOSITION_MAX [`floating point number`] : Maximum Deposition along Path. Minimum: 0.000000 Maximum: 100.000000 Default: 20.000000 The percentage of available material (per run) which is deposited at most (slope or velocity equal zero) [%].
    - DEPOSITION_MIN_PATH [`floating point number`] : Minimum Path Length. Minimum: 0.000000 Default: 100.000000 The minimum path length which has to be reached before material deposition is enabled [m].
    - SINK_MIN_SLOPE [`floating point number`] : Minimum Slope. Minimum: 0.000000 Maximum: 90.000000 Default: 2.500000 The minimum slope to preserve on sink filling [degree].

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('sim_geomorphology', '0', 'Gravitational Process Path Model')
    if Tool.is_Okay():
        Tool.Set_Input ('DEM', DEM)
        Tool.Set_Input ('RELEASE_AREAS', RELEASE_AREAS)
        Tool.Set_Input ('MATERIAL', MATERIAL)
        Tool.Set_Input ('FRICTION_ANGLE_GRID', FRICTION_ANGLE_GRID)
        Tool.Set_Input ('SLOPE_IMPACT_GRID', SLOPE_IMPACT_GRID)
        Tool.Set_Input ('FRICTION_MU_GRID', FRICTION_MU_GRID)
        Tool.Set_Input ('FRICTION_MASS_TO_DRAG_GRID', FRICTION_MASS_TO_DRAG_GRID)
        Tool.Set_Input ('OBJECTS', OBJECTS)
        Tool.Set_Output('PROCESS_AREA', PROCESS_AREA)
        Tool.Set_Output('DEPOSITION', DEPOSITION)
        Tool.Set_Output('MAX_VELOCITY', MAX_VELOCITY)
        Tool.Set_Output('STOP_POSITIONS', STOP_POSITIONS)
        Tool.Set_Output('HAZARD_PATHS', HAZARD_PATHS)
        Tool.Set_Output('HAZARD_SOURCES', HAZARD_SOURCES)
        Tool.Set_Output('HAZARD_SOURCES_MATERIAL', HAZARD_SOURCES_MATERIAL)
        Tool.Set_Output('MATERIAL_FLUX', MATERIAL_FLUX)
        Tool.Set_Option('PROCESS_PATH_MODEL', PROCESS_PATH_MODEL)
        Tool.Set_Option('RW_SLOPE_THRES', RW_SLOPE_THRES)
        Tool.Set_Option('RW_EXPONENT', RW_EXPONENT)
        Tool.Set_Option('RW_PERSISTENCE', RW_PERSISTENCE)
        Tool.Set_Option('GPP_ITERATIONS', GPP_ITERATIONS)
        Tool.Set_Option('GPP_PROCESSING_ORDER', GPP_PROCESSING_ORDER)
        Tool.Set_Option('GPP_SEED', GPP_SEED)
        Tool.Set_Option('FRICTION_MODEL', FRICTION_MODEL)
        Tool.Set_Option('FRICTION_THRES_FREE_FALL', FRICTION_THRES_FREE_FALL)
        Tool.Set_Option('FRICTION_METHOD_IMPACT', FRICTION_METHOD_IMPACT)
        Tool.Set_Option('FRICTION_IMPACT_REDUCTION', FRICTION_IMPACT_REDUCTION)
        Tool.Set_Option('FRICTION_ANGLE', FRICTION_ANGLE)
        Tool.Set_Option('FRICTION_MU', FRICTION_MU)
        Tool.Set_Option('FRICTION_MODE_OF_MOTION', FRICTION_MODE_OF_MOTION)
        Tool.Set_Option('FRICTION_MASS_TO_DRAG', FRICTION_MASS_TO_DRAG)
        Tool.Set_Option('FRICTION_INIT_VELOCITY', FRICTION_INIT_VELOCITY)
        Tool.Set_Option('DEPOSITION_MODEL', DEPOSITION_MODEL)
        Tool.Set_Option('DEPOSITION_INITIAL', DEPOSITION_INITIAL)
        Tool.Set_Option('DEPOSITION_SLOPE_THRES', DEPOSITION_SLOPE_THRES)
        Tool.Set_Option('DEPOSITION_VELOCITY_THRES', DEPOSITION_VELOCITY_THRES)
        Tool.Set_Option('DEPOSITION_MAX', DEPOSITION_MAX)
        Tool.Set_Option('DEPOSITION_MIN_PATH', DEPOSITION_MIN_PATH)
        Tool.Set_Option('SINK_MIN_SLOPE', SINK_MIN_SLOPE)
        return Tool.Execute(Verbose)
    return False

