#! /usr/bin/env python

'''
Python Interface to SAGA Tools Library
----------
- Category : Import/Export
- Name     : Web Services
- ID       : io_webservices

Description
----------
Web Services
'''

from PySAGA.helper import Tool_Wrapper

def Geocoding(ADDRESSES=None, LOCATIONS=None, FIELD=None, ADDRESS=None, PROVIDER=None, API_KEY=None, METADATA=None, Verbose=2):
    '''
    Geocoding
    ----------
    [io_webservices.0]\n
    Geocoding of addresses using geocoding services.\n
    Arguments
    ----------
    - ADDRESSES [`optional input table`] : Address List
    - LOCATIONS [`output shapes`] : Locations
    - FIELD [`table field`] : Address Field
    - ADDRESS [`text`] : Single Address. Default: Bundesstrasse 55, Hamburg, Germany
    - PROVIDER [`choice`] : Service Provider. Available Choices: [0] Nominatim (OpenStreetMap) [1] The Data Science Toolkit [2] Google [3] Bing [4] MapQuest Default: 0
    - API_KEY [`text`] : API Key
    - METADATA [`boolean`] : Store Metadata. Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('io_webservices', '0', 'Geocoding')
    if Tool.is_Okay():
        Tool.Set_Input ('ADDRESSES', ADDRESSES)
        Tool.Set_Output('LOCATIONS', LOCATIONS)
        Tool.Set_Option('FIELD', FIELD)
        Tool.Set_Option('ADDRESS', ADDRESS)
        Tool.Set_Option('PROVIDER', PROVIDER)
        Tool.Set_Option('API_KEY', API_KEY)
        Tool.Set_Option('METADATA', METADATA)
        return Tool.Execute(Verbose)
    return False

def run_tool_io_webservices_0(ADDRESSES=None, LOCATIONS=None, FIELD=None, ADDRESS=None, PROVIDER=None, API_KEY=None, METADATA=None, Verbose=2):
    '''
    Geocoding
    ----------
    [io_webservices.0]\n
    Geocoding of addresses using geocoding services.\n
    Arguments
    ----------
    - ADDRESSES [`optional input table`] : Address List
    - LOCATIONS [`output shapes`] : Locations
    - FIELD [`table field`] : Address Field
    - ADDRESS [`text`] : Single Address. Default: Bundesstrasse 55, Hamburg, Germany
    - PROVIDER [`choice`] : Service Provider. Available Choices: [0] Nominatim (OpenStreetMap) [1] The Data Science Toolkit [2] Google [3] Bing [4] MapQuest Default: 0
    - API_KEY [`text`] : API Key
    - METADATA [`boolean`] : Store Metadata. Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('io_webservices', '0', 'Geocoding')
    if Tool.is_Okay():
        Tool.Set_Input ('ADDRESSES', ADDRESSES)
        Tool.Set_Output('LOCATIONS', LOCATIONS)
        Tool.Set_Option('FIELD', FIELD)
        Tool.Set_Option('ADDRESS', ADDRESS)
        Tool.Set_Option('PROVIDER', PROVIDER)
        Tool.Set_Option('API_KEY', API_KEY)
        Tool.Set_Option('METADATA', METADATA)
        return Tool.Execute(Verbose)
    return False

def SRTM_CGIAR_CSI(GRID=None, SHAPES=None, RESULT=None, TILES=None, DELARCHIVE=None, EXTENT=None, GRID_SYSTEM=None, XMIN=None, XMAX=None, YMIN=None, YMAX=None, NX=None, NY=None, BUFFER=None, CELLSIZE=None, CRS_STRING=None, Verbose=2):
    '''
    SRTM (CGIAR CSI)
    ----------
    [io_webservices.1]\n
    This tool provides easy-to-use access to the 'NASA Shuttle Radar Topography Mission Global 3 arc second' elevation data (about 90 meter resolution) as provided by the CGIAR CSI server. It uses a local database in the chosen directory which provides the original tiles. If the tiles covering the requested area are not found in this directory the tool tries to download these from the CGIAR CSI server.\n
    Arguments
    ----------
    - GRID [`input grid`] : Grid
    - SHAPES [`input shapes`] : Shapes
    - RESULT [`output data object`] : Grid
    - TILES [`file path`] : Local Tiles Directory. Download location for tiles. If requested tile is already present download will be skipped.
    - DELARCHIVE [`boolean`] : Delete Archive Files. Default: 1 Do not keep archive files after download
    - EXTENT [`choice`] : Extent. Available Choices: [0] user defined [1] shapes extent [2] grid system extent [3] grid system Default: 0
    - GRID_SYSTEM [`grid system`] : Grid System
    - XMIN [`floating point number`] : West. Default: 270360.000000
    - XMAX [`floating point number`] : East. Default: 931320.000000
    - YMIN [`floating point number`] : South. Default: 5225850.000000
    - YMAX [`floating point number`] : North. Default: 6111540.000000
    - NX [`integer number`] : Columns. Minimum: 1 Default: 7345
    - NY [`integer number`] : Rows. Minimum: 1 Default: 9842
    - BUFFER [`floating point number`] : Buffer. Minimum: 0.000000 Default: 0.000000 add buffer (map units) to extent
    - CELLSIZE [`floating point number`] : Cellsize. Minimum: 0.000100 Default: 90.000000
    - CRS_STRING [`text`] : Coordinate System Definition. Supported formats comprise PROJ and WKT strings, object codes (e.g. "EPSG:4326").

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('io_webservices', '1', 'SRTM (CGIAR CSI)')
    if Tool.is_Okay():
        Tool.Set_Input ('GRID', GRID)
        Tool.Set_Input ('SHAPES', SHAPES)
        Tool.Set_Output('RESULT', RESULT)
        Tool.Set_Option('TILES', TILES)
        Tool.Set_Option('DELARCHIVE', DELARCHIVE)
        Tool.Set_Option('EXTENT', EXTENT)
        Tool.Set_Option('GRID_SYSTEM', GRID_SYSTEM)
        Tool.Set_Option('XMIN', XMIN)
        Tool.Set_Option('XMAX', XMAX)
        Tool.Set_Option('YMIN', YMIN)
        Tool.Set_Option('YMAX', YMAX)
        Tool.Set_Option('NX', NX)
        Tool.Set_Option('NY', NY)
        Tool.Set_Option('BUFFER', BUFFER)
        Tool.Set_Option('CELLSIZE', CELLSIZE)
        Tool.Set_Option('CRS_STRING', CRS_STRING)
        return Tool.Execute(Verbose)
    return False

def run_tool_io_webservices_1(GRID=None, SHAPES=None, RESULT=None, TILES=None, DELARCHIVE=None, EXTENT=None, GRID_SYSTEM=None, XMIN=None, XMAX=None, YMIN=None, YMAX=None, NX=None, NY=None, BUFFER=None, CELLSIZE=None, CRS_STRING=None, Verbose=2):
    '''
    SRTM (CGIAR CSI)
    ----------
    [io_webservices.1]\n
    This tool provides easy-to-use access to the 'NASA Shuttle Radar Topography Mission Global 3 arc second' elevation data (about 90 meter resolution) as provided by the CGIAR CSI server. It uses a local database in the chosen directory which provides the original tiles. If the tiles covering the requested area are not found in this directory the tool tries to download these from the CGIAR CSI server.\n
    Arguments
    ----------
    - GRID [`input grid`] : Grid
    - SHAPES [`input shapes`] : Shapes
    - RESULT [`output data object`] : Grid
    - TILES [`file path`] : Local Tiles Directory. Download location for tiles. If requested tile is already present download will be skipped.
    - DELARCHIVE [`boolean`] : Delete Archive Files. Default: 1 Do not keep archive files after download
    - EXTENT [`choice`] : Extent. Available Choices: [0] user defined [1] shapes extent [2] grid system extent [3] grid system Default: 0
    - GRID_SYSTEM [`grid system`] : Grid System
    - XMIN [`floating point number`] : West. Default: 270360.000000
    - XMAX [`floating point number`] : East. Default: 931320.000000
    - YMIN [`floating point number`] : South. Default: 5225850.000000
    - YMAX [`floating point number`] : North. Default: 6111540.000000
    - NX [`integer number`] : Columns. Minimum: 1 Default: 7345
    - NY [`integer number`] : Rows. Minimum: 1 Default: 9842
    - BUFFER [`floating point number`] : Buffer. Minimum: 0.000000 Default: 0.000000 add buffer (map units) to extent
    - CELLSIZE [`floating point number`] : Cellsize. Minimum: 0.000100 Default: 90.000000
    - CRS_STRING [`text`] : Coordinate System Definition. Supported formats comprise PROJ and WKT strings, object codes (e.g. "EPSG:4326").

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('io_webservices', '1', 'SRTM (CGIAR CSI)')
    if Tool.is_Okay():
        Tool.Set_Input ('GRID', GRID)
        Tool.Set_Input ('SHAPES', SHAPES)
        Tool.Set_Output('RESULT', RESULT)
        Tool.Set_Option('TILES', TILES)
        Tool.Set_Option('DELARCHIVE', DELARCHIVE)
        Tool.Set_Option('EXTENT', EXTENT)
        Tool.Set_Option('GRID_SYSTEM', GRID_SYSTEM)
        Tool.Set_Option('XMIN', XMIN)
        Tool.Set_Option('XMAX', XMAX)
        Tool.Set_Option('YMIN', YMIN)
        Tool.Set_Option('YMAX', YMAX)
        Tool.Set_Option('NX', NX)
        Tool.Set_Option('NY', NY)
        Tool.Set_Option('BUFFER', BUFFER)
        Tool.Set_Option('CELLSIZE', CELLSIZE)
        Tool.Set_Option('CRS_STRING', CRS_STRING)
        return Tool.Execute(Verbose)
    return False

def Copernicus_DEM(GRID=None, SHAPES=None, RESULT=None, TILES=None, DELARCHIVE=None, EXTENT=None, GRID_SYSTEM=None, XMIN=None, XMAX=None, YMIN=None, YMAX=None, NX=None, NY=None, BUFFER=None, CELLSIZE=None, CRS_STRING=None, MASK=None, Verbose=2):
    '''
    Copernicus DEM
    ----------
    [io_webservices.3]\n
    This tool provides easy-to-use access to the 'Copernicus DEM' global elevation data with 1 arcsec resolution (about 30 meter). It uses a local database in the chosen directory which provides the original tiles. If the tiles covering the requested area are not found in the directory the tool tries to download these from the Copernicus server.\n
    Arguments
    ----------
    - GRID [`input grid`] : Grid
    - SHAPES [`input shapes`] : Shapes
    - RESULT [`output data object`] : Grid
    - TILES [`file path`] : Local Tiles Directory. Download location for tiles. If requested tile is already present download will be skipped.
    - DELARCHIVE [`boolean`] : Delete Archive Files. Default: 1 Do not keep archive files after download
    - EXTENT [`choice`] : Extent. Available Choices: [0] user defined [1] shapes extent [2] grid system extent [3] grid system Default: 0
    - GRID_SYSTEM [`grid system`] : Grid System
    - XMIN [`floating point number`] : West. Default: 270360.000000
    - XMAX [`floating point number`] : East. Default: 931320.000000
    - YMIN [`floating point number`] : South. Default: 5225850.000000
    - YMAX [`floating point number`] : North. Default: 6111540.000000
    - NX [`integer number`] : Columns. Minimum: 1 Default: 7345
    - NY [`integer number`] : Rows. Minimum: 1 Default: 9842
    - BUFFER [`floating point number`] : Buffer. Minimum: 0.000000 Default: 0.000000 add buffer (map units) to extent
    - CELLSIZE [`floating point number`] : Cellsize. Minimum: 0.000100 Default: 90.000000
    - CRS_STRING [`text`] : Coordinate System Definition. Supported formats comprise PROJ and WKT strings, object codes (e.g. "EPSG:4326").
    - MASK [`boolean`] : Water Mask. Default: 1 Applies ocean water mask.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('io_webservices', '3', 'Copernicus DEM')
    if Tool.is_Okay():
        Tool.Set_Input ('GRID', GRID)
        Tool.Set_Input ('SHAPES', SHAPES)
        Tool.Set_Output('RESULT', RESULT)
        Tool.Set_Option('TILES', TILES)
        Tool.Set_Option('DELARCHIVE', DELARCHIVE)
        Tool.Set_Option('EXTENT', EXTENT)
        Tool.Set_Option('GRID_SYSTEM', GRID_SYSTEM)
        Tool.Set_Option('XMIN', XMIN)
        Tool.Set_Option('XMAX', XMAX)
        Tool.Set_Option('YMIN', YMIN)
        Tool.Set_Option('YMAX', YMAX)
        Tool.Set_Option('NX', NX)
        Tool.Set_Option('NY', NY)
        Tool.Set_Option('BUFFER', BUFFER)
        Tool.Set_Option('CELLSIZE', CELLSIZE)
        Tool.Set_Option('CRS_STRING', CRS_STRING)
        Tool.Set_Option('MASK', MASK)
        return Tool.Execute(Verbose)
    return False

def run_tool_io_webservices_3(GRID=None, SHAPES=None, RESULT=None, TILES=None, DELARCHIVE=None, EXTENT=None, GRID_SYSTEM=None, XMIN=None, XMAX=None, YMIN=None, YMAX=None, NX=None, NY=None, BUFFER=None, CELLSIZE=None, CRS_STRING=None, MASK=None, Verbose=2):
    '''
    Copernicus DEM
    ----------
    [io_webservices.3]\n
    This tool provides easy-to-use access to the 'Copernicus DEM' global elevation data with 1 arcsec resolution (about 30 meter). It uses a local database in the chosen directory which provides the original tiles. If the tiles covering the requested area are not found in the directory the tool tries to download these from the Copernicus server.\n
    Arguments
    ----------
    - GRID [`input grid`] : Grid
    - SHAPES [`input shapes`] : Shapes
    - RESULT [`output data object`] : Grid
    - TILES [`file path`] : Local Tiles Directory. Download location for tiles. If requested tile is already present download will be skipped.
    - DELARCHIVE [`boolean`] : Delete Archive Files. Default: 1 Do not keep archive files after download
    - EXTENT [`choice`] : Extent. Available Choices: [0] user defined [1] shapes extent [2] grid system extent [3] grid system Default: 0
    - GRID_SYSTEM [`grid system`] : Grid System
    - XMIN [`floating point number`] : West. Default: 270360.000000
    - XMAX [`floating point number`] : East. Default: 931320.000000
    - YMIN [`floating point number`] : South. Default: 5225850.000000
    - YMAX [`floating point number`] : North. Default: 6111540.000000
    - NX [`integer number`] : Columns. Minimum: 1 Default: 7345
    - NY [`integer number`] : Rows. Minimum: 1 Default: 9842
    - BUFFER [`floating point number`] : Buffer. Minimum: 0.000000 Default: 0.000000 add buffer (map units) to extent
    - CELLSIZE [`floating point number`] : Cellsize. Minimum: 0.000100 Default: 90.000000
    - CRS_STRING [`text`] : Coordinate System Definition. Supported formats comprise PROJ and WKT strings, object codes (e.g. "EPSG:4326").
    - MASK [`boolean`] : Water Mask. Default: 1 Applies ocean water mask.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('io_webservices', '3', 'Copernicus DEM')
    if Tool.is_Okay():
        Tool.Set_Input ('GRID', GRID)
        Tool.Set_Input ('SHAPES', SHAPES)
        Tool.Set_Output('RESULT', RESULT)
        Tool.Set_Option('TILES', TILES)
        Tool.Set_Option('DELARCHIVE', DELARCHIVE)
        Tool.Set_Option('EXTENT', EXTENT)
        Tool.Set_Option('GRID_SYSTEM', GRID_SYSTEM)
        Tool.Set_Option('XMIN', XMIN)
        Tool.Set_Option('XMAX', XMAX)
        Tool.Set_Option('YMIN', YMIN)
        Tool.Set_Option('YMAX', YMAX)
        Tool.Set_Option('NX', NX)
        Tool.Set_Option('NY', NY)
        Tool.Set_Option('BUFFER', BUFFER)
        Tool.Set_Option('CELLSIZE', CELLSIZE)
        Tool.Set_Option('CRS_STRING', CRS_STRING)
        Tool.Set_Option('MASK', MASK)
        return Tool.Execute(Verbose)
    return False

