#! /usr/bin/env python

'''
Python Interface to SAGA Tools Library
----------
- Category : Climate & Weather
- Name     : Climate & Weather Tools
- ID       : climate_tools

Description
----------
Tools for the processing and analysis of climate and weather data.
'''

from PySAGA.helper import Tool_Wrapper

def Multi_Level_to_Surface_Interpolation(VARIABLE=None, SURFACE=None, X_GRIDS=None, X_GRIDS_CHECK=None, RESULT=None, X_SOURCE=None, X_TABLE=None, H_METHOD=None, V_METHOD=None, COEFFICIENTS=None, LINEAR_SORTED=None, SPLINE_ALL=None, TREND_ORDER=None, SYSTEM=None, Verbose=2):
    '''
    Multi Level to Surface Interpolation
    ----------
    [climate_tools.0]\n
    Multi Level to Surface Interpolation\n
    Arguments
    ----------
    - VARIABLE [`input grid list`] : Variable
    - SURFACE [`input grid`] : Surface
    - X_GRIDS [`optional input grid list`] : Level Heights
    - X_GRIDS_CHECK [`optional input grid`] : Minimum Height. if set, only values with level heights above DEM will be used
    - RESULT [`output grid`] : Interpolation
    - X_SOURCE [`choice`] : Get Heights from .... Available Choices: [0] table [1] grid list Default: 1
    - X_TABLE [`static table`] : Level Heights. 1 Fields: - 1. [8 byte floating point number] Height 
    - H_METHOD [`choice`] : Horizontal Interpolation Method. Available Choices: [0] Nearest Neighbour [1] Bilinear Interpolation [2] Bicubic Spline Interpolation [3] B-Spline Interpolation Default: 3
    - V_METHOD [`choice`] : Vertical Interpolation Method. Available Choices: [0] linear [1] spline [2] polynomial trend Default: 0
    - COEFFICIENTS [`boolean`] : Coefficient Interpolation. Default: 0
    - LINEAR_SORTED [`boolean`] : Sorted Levels. Default: 0
    - SPLINE_ALL [`boolean`] : Pre-analyze. Default: 0
    - TREND_ORDER [`integer number`] : Polynomial Order. Minimum: 1 Default: 3
    - SYSTEM [`grid system`] : Grid system

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('climate_tools', '0', 'Multi Level to Surface Interpolation')
    if Tool.is_Okay():
        Tool.Set_Input ('VARIABLE', VARIABLE)
        Tool.Set_Input ('SURFACE', SURFACE)
        Tool.Set_Input ('X_GRIDS', X_GRIDS)
        Tool.Set_Input ('X_GRIDS_CHECK', X_GRIDS_CHECK)
        Tool.Set_Output('RESULT', RESULT)
        Tool.Set_Option('X_SOURCE', X_SOURCE)
        Tool.Set_Option('X_TABLE', X_TABLE)
        Tool.Set_Option('H_METHOD', H_METHOD)
        Tool.Set_Option('V_METHOD', V_METHOD)
        Tool.Set_Option('COEFFICIENTS', COEFFICIENTS)
        Tool.Set_Option('LINEAR_SORTED', LINEAR_SORTED)
        Tool.Set_Option('SPLINE_ALL', SPLINE_ALL)
        Tool.Set_Option('TREND_ORDER', TREND_ORDER)
        Tool.Set_Option('SYSTEM', SYSTEM)
        return Tool.Execute(Verbose)
    return False

def run_tool_climate_tools_0(VARIABLE=None, SURFACE=None, X_GRIDS=None, X_GRIDS_CHECK=None, RESULT=None, X_SOURCE=None, X_TABLE=None, H_METHOD=None, V_METHOD=None, COEFFICIENTS=None, LINEAR_SORTED=None, SPLINE_ALL=None, TREND_ORDER=None, SYSTEM=None, Verbose=2):
    '''
    Multi Level to Surface Interpolation
    ----------
    [climate_tools.0]\n
    Multi Level to Surface Interpolation\n
    Arguments
    ----------
    - VARIABLE [`input grid list`] : Variable
    - SURFACE [`input grid`] : Surface
    - X_GRIDS [`optional input grid list`] : Level Heights
    - X_GRIDS_CHECK [`optional input grid`] : Minimum Height. if set, only values with level heights above DEM will be used
    - RESULT [`output grid`] : Interpolation
    - X_SOURCE [`choice`] : Get Heights from .... Available Choices: [0] table [1] grid list Default: 1
    - X_TABLE [`static table`] : Level Heights. 1 Fields: - 1. [8 byte floating point number] Height 
    - H_METHOD [`choice`] : Horizontal Interpolation Method. Available Choices: [0] Nearest Neighbour [1] Bilinear Interpolation [2] Bicubic Spline Interpolation [3] B-Spline Interpolation Default: 3
    - V_METHOD [`choice`] : Vertical Interpolation Method. Available Choices: [0] linear [1] spline [2] polynomial trend Default: 0
    - COEFFICIENTS [`boolean`] : Coefficient Interpolation. Default: 0
    - LINEAR_SORTED [`boolean`] : Sorted Levels. Default: 0
    - SPLINE_ALL [`boolean`] : Pre-analyze. Default: 0
    - TREND_ORDER [`integer number`] : Polynomial Order. Minimum: 1 Default: 3
    - SYSTEM [`grid system`] : Grid system

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('climate_tools', '0', 'Multi Level to Surface Interpolation')
    if Tool.is_Okay():
        Tool.Set_Input ('VARIABLE', VARIABLE)
        Tool.Set_Input ('SURFACE', SURFACE)
        Tool.Set_Input ('X_GRIDS', X_GRIDS)
        Tool.Set_Input ('X_GRIDS_CHECK', X_GRIDS_CHECK)
        Tool.Set_Output('RESULT', RESULT)
        Tool.Set_Option('X_SOURCE', X_SOURCE)
        Tool.Set_Option('X_TABLE', X_TABLE)
        Tool.Set_Option('H_METHOD', H_METHOD)
        Tool.Set_Option('V_METHOD', V_METHOD)
        Tool.Set_Option('COEFFICIENTS', COEFFICIENTS)
        Tool.Set_Option('LINEAR_SORTED', LINEAR_SORTED)
        Tool.Set_Option('SPLINE_ALL', SPLINE_ALL)
        Tool.Set_Option('TREND_ORDER', TREND_ORDER)
        Tool.Set_Option('SYSTEM', SYSTEM)
        return Tool.Execute(Verbose)
    return False

def Multi_Level_to_Points_Interpolation(VARIABLE=None, POINTS=None, X_GRIDS=None, X_GRIDS_CHECK=None, RESULT=None, X_SOURCE=None, X_TABLE=None, H_METHOD=None, V_METHOD=None, COEFFICIENTS=None, LINEAR_SORTED=None, SPLINE_ALL=None, TREND_ORDER=None, ZFIELD=None, NAME=None, Verbose=2):
    '''
    Multi Level to Points Interpolation
    ----------
    [climate_tools.1]\n
    Multi Level to Points Interpolation\n
    Arguments
    ----------
    - VARIABLE [`input grid list`] : Variable
    - POINTS [`input shapes`] : Points
    - X_GRIDS [`optional input grid list`] : Level Heights
    - X_GRIDS_CHECK [`optional input grid`] : Minimum Height. if set, only values with level heights above DEM will be used
    - RESULT [`output shapes`] : Result
    - X_SOURCE [`choice`] : Get Heights from .... Available Choices: [0] table [1] grid list Default: 1
    - X_TABLE [`static table`] : Level Heights. 1 Fields: - 1. [8 byte floating point number] Height 
    - H_METHOD [`choice`] : Horizontal Interpolation Method. Available Choices: [0] Nearest Neighbour [1] Bilinear Interpolation [2] Bicubic Spline Interpolation [3] B-Spline Interpolation Default: 3
    - V_METHOD [`choice`] : Vertical Interpolation Method. Available Choices: [0] linear [1] spline [2] polynomial trend Default: 0
    - COEFFICIENTS [`boolean`] : Coefficient Interpolation. Default: 0
    - LINEAR_SORTED [`boolean`] : Sorted Levels. Default: 0
    - SPLINE_ALL [`boolean`] : Pre-analyze. Default: 0
    - TREND_ORDER [`integer number`] : Polynomial Order. Minimum: 1 Default: 3
    - ZFIELD [`table field`] : Height
    - NAME [`text`] : Field Name. Default: Variable

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('climate_tools', '1', 'Multi Level to Points Interpolation')
    if Tool.is_Okay():
        Tool.Set_Input ('VARIABLE', VARIABLE)
        Tool.Set_Input ('POINTS', POINTS)
        Tool.Set_Input ('X_GRIDS', X_GRIDS)
        Tool.Set_Input ('X_GRIDS_CHECK', X_GRIDS_CHECK)
        Tool.Set_Output('RESULT', RESULT)
        Tool.Set_Option('X_SOURCE', X_SOURCE)
        Tool.Set_Option('X_TABLE', X_TABLE)
        Tool.Set_Option('H_METHOD', H_METHOD)
        Tool.Set_Option('V_METHOD', V_METHOD)
        Tool.Set_Option('COEFFICIENTS', COEFFICIENTS)
        Tool.Set_Option('LINEAR_SORTED', LINEAR_SORTED)
        Tool.Set_Option('SPLINE_ALL', SPLINE_ALL)
        Tool.Set_Option('TREND_ORDER', TREND_ORDER)
        Tool.Set_Option('ZFIELD', ZFIELD)
        Tool.Set_Option('NAME', NAME)
        return Tool.Execute(Verbose)
    return False

def run_tool_climate_tools_1(VARIABLE=None, POINTS=None, X_GRIDS=None, X_GRIDS_CHECK=None, RESULT=None, X_SOURCE=None, X_TABLE=None, H_METHOD=None, V_METHOD=None, COEFFICIENTS=None, LINEAR_SORTED=None, SPLINE_ALL=None, TREND_ORDER=None, ZFIELD=None, NAME=None, Verbose=2):
    '''
    Multi Level to Points Interpolation
    ----------
    [climate_tools.1]\n
    Multi Level to Points Interpolation\n
    Arguments
    ----------
    - VARIABLE [`input grid list`] : Variable
    - POINTS [`input shapes`] : Points
    - X_GRIDS [`optional input grid list`] : Level Heights
    - X_GRIDS_CHECK [`optional input grid`] : Minimum Height. if set, only values with level heights above DEM will be used
    - RESULT [`output shapes`] : Result
    - X_SOURCE [`choice`] : Get Heights from .... Available Choices: [0] table [1] grid list Default: 1
    - X_TABLE [`static table`] : Level Heights. 1 Fields: - 1. [8 byte floating point number] Height 
    - H_METHOD [`choice`] : Horizontal Interpolation Method. Available Choices: [0] Nearest Neighbour [1] Bilinear Interpolation [2] Bicubic Spline Interpolation [3] B-Spline Interpolation Default: 3
    - V_METHOD [`choice`] : Vertical Interpolation Method. Available Choices: [0] linear [1] spline [2] polynomial trend Default: 0
    - COEFFICIENTS [`boolean`] : Coefficient Interpolation. Default: 0
    - LINEAR_SORTED [`boolean`] : Sorted Levels. Default: 0
    - SPLINE_ALL [`boolean`] : Pre-analyze. Default: 0
    - TREND_ORDER [`integer number`] : Polynomial Order. Minimum: 1 Default: 3
    - ZFIELD [`table field`] : Height
    - NAME [`text`] : Field Name. Default: Variable

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('climate_tools', '1', 'Multi Level to Points Interpolation')
    if Tool.is_Okay():
        Tool.Set_Input ('VARIABLE', VARIABLE)
        Tool.Set_Input ('POINTS', POINTS)
        Tool.Set_Input ('X_GRIDS', X_GRIDS)
        Tool.Set_Input ('X_GRIDS_CHECK', X_GRIDS_CHECK)
        Tool.Set_Output('RESULT', RESULT)
        Tool.Set_Option('X_SOURCE', X_SOURCE)
        Tool.Set_Option('X_TABLE', X_TABLE)
        Tool.Set_Option('H_METHOD', H_METHOD)
        Tool.Set_Option('V_METHOD', V_METHOD)
        Tool.Set_Option('COEFFICIENTS', COEFFICIENTS)
        Tool.Set_Option('LINEAR_SORTED', LINEAR_SORTED)
        Tool.Set_Option('SPLINE_ALL', SPLINE_ALL)
        Tool.Set_Option('TREND_ORDER', TREND_ORDER)
        Tool.Set_Option('ZFIELD', ZFIELD)
        Tool.Set_Option('NAME', NAME)
        return Tool.Execute(Verbose)
    return False

def Earths_Orbital_Parameters(ORBPAR=None, START=None, STOP=None, STEP=None, Verbose=2):
    '''
    Earth's Orbital Parameters
    ----------
    [climate_tools.2]\n
    Orbital parameters used here are based on the work of Andre L. Berger and its implementation from the NASA Goddard Institute for Space Studies (GISS). Berger's orbital parameters are considered to be valid for approximately 1 million years.\n
    Arguments
    ----------
    - ORBPAR [`output table`] : Earth's Orbital Parameters
    - START [`floating point number`] : Start [ka]. Default: -200.000000
    - STOP [`floating point number`] : Stop [ka]. Default: 2.000000
    - STEP [`floating point number`] : Step [ka]. Minimum: 0.001000 Default: 1.000000

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('climate_tools', '2', 'Earth\'s Orbital Parameters')
    if Tool.is_Okay():
        Tool.Set_Output('ORBPAR', ORBPAR)
        Tool.Set_Option('START', START)
        Tool.Set_Option('STOP', STOP)
        Tool.Set_Option('STEP', STEP)
        return Tool.Execute(Verbose)
    return False

def run_tool_climate_tools_2(ORBPAR=None, START=None, STOP=None, STEP=None, Verbose=2):
    '''
    Earth's Orbital Parameters
    ----------
    [climate_tools.2]\n
    Orbital parameters used here are based on the work of Andre L. Berger and its implementation from the NASA Goddard Institute for Space Studies (GISS). Berger's orbital parameters are considered to be valid for approximately 1 million years.\n
    Arguments
    ----------
    - ORBPAR [`output table`] : Earth's Orbital Parameters
    - START [`floating point number`] : Start [ka]. Default: -200.000000
    - STOP [`floating point number`] : Stop [ka]. Default: 2.000000
    - STEP [`floating point number`] : Step [ka]. Minimum: 0.001000 Default: 1.000000

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('climate_tools', '2', 'Earth\'s Orbital Parameters')
    if Tool.is_Okay():
        Tool.Set_Output('ORBPAR', ORBPAR)
        Tool.Set_Option('START', START)
        Tool.Set_Option('STOP', STOP)
        Tool.Set_Option('STEP', STEP)
        return Tool.Execute(Verbose)
    return False

def Annual_Course_of_Daily_Insolation(SOLARRAD=None, START=None, STOP=None, STEP=None, LAT=None, Verbose=2):
    '''
    Annual Course of Daily Insolation
    ----------
    [climate_tools.3]\n
    Orbital parameters used here are based on the work of Andre L. Berger and its implementation from the NASA Goddard Institute for Space Studies (GISS). Berger's orbital parameters are considered to be valid for approximately 1 million years.\n
    Arguments
    ----------
    - SOLARRAD [`output table`] : Solar Radiation
    - START [`floating point number`] : Start [ka]. Default: -200.000000
    - STOP [`floating point number`] : Stop [ka]. Default: 2.000000
    - STEP [`floating point number`] : Step [ka]. Minimum: 0.001000 Default: 1.000000
    - LAT [`floating point number`] : Latitude [Degree]. Minimum: -90.000000 Maximum: 90.000000 Default: 53.000000

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('climate_tools', '3', 'Annual Course of Daily Insolation')
    if Tool.is_Okay():
        Tool.Set_Output('SOLARRAD', SOLARRAD)
        Tool.Set_Option('START', START)
        Tool.Set_Option('STOP', STOP)
        Tool.Set_Option('STEP', STEP)
        Tool.Set_Option('LAT', LAT)
        return Tool.Execute(Verbose)
    return False

def run_tool_climate_tools_3(SOLARRAD=None, START=None, STOP=None, STEP=None, LAT=None, Verbose=2):
    '''
    Annual Course of Daily Insolation
    ----------
    [climate_tools.3]\n
    Orbital parameters used here are based on the work of Andre L. Berger and its implementation from the NASA Goddard Institute for Space Studies (GISS). Berger's orbital parameters are considered to be valid for approximately 1 million years.\n
    Arguments
    ----------
    - SOLARRAD [`output table`] : Solar Radiation
    - START [`floating point number`] : Start [ka]. Default: -200.000000
    - STOP [`floating point number`] : Stop [ka]. Default: 2.000000
    - STEP [`floating point number`] : Step [ka]. Minimum: 0.001000 Default: 1.000000
    - LAT [`floating point number`] : Latitude [Degree]. Minimum: -90.000000 Maximum: 90.000000 Default: 53.000000

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('climate_tools', '3', 'Annual Course of Daily Insolation')
    if Tool.is_Okay():
        Tool.Set_Output('SOLARRAD', SOLARRAD)
        Tool.Set_Option('START', START)
        Tool.Set_Option('STOP', STOP)
        Tool.Set_Option('STEP', STEP)
        Tool.Set_Option('LAT', LAT)
        return Tool.Execute(Verbose)
    return False

def Daily_Insolation_over_Latitude(SOLARRAD=None, START=None, STOP=None, STEP=None, DLAT=None, DAY=None, Verbose=2):
    '''
    Daily Insolation over Latitude
    ----------
    [climate_tools.4]\n
    Orbital parameters used here are based on the work of Andre L. Berger and its implementation from the NASA Goddard Institute for Space Studies (GISS). Berger's orbital parameters are considered to be valid for approximately 1 million years.\n
    Arguments
    ----------
    - SOLARRAD [`output table`] : Solar Radiation
    - START [`floating point number`] : Start [ka]. Default: -200.000000
    - STOP [`floating point number`] : Stop [ka]. Default: 2.000000
    - STEP [`floating point number`] : Step [ka]. Minimum: 0.001000 Default: 1.000000
    - DLAT [`integer number`] : Latitude Increment [Degree]. Minimum: 1 Maximum: 90 Default: 5
    - DAY [`integer number`] : Day of Year. Minimum: 0 Maximum: 366 Default: 181

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('climate_tools', '4', 'Daily Insolation over Latitude')
    if Tool.is_Okay():
        Tool.Set_Output('SOLARRAD', SOLARRAD)
        Tool.Set_Option('START', START)
        Tool.Set_Option('STOP', STOP)
        Tool.Set_Option('STEP', STEP)
        Tool.Set_Option('DLAT', DLAT)
        Tool.Set_Option('DAY', DAY)
        return Tool.Execute(Verbose)
    return False

def run_tool_climate_tools_4(SOLARRAD=None, START=None, STOP=None, STEP=None, DLAT=None, DAY=None, Verbose=2):
    '''
    Daily Insolation over Latitude
    ----------
    [climate_tools.4]\n
    Orbital parameters used here are based on the work of Andre L. Berger and its implementation from the NASA Goddard Institute for Space Studies (GISS). Berger's orbital parameters are considered to be valid for approximately 1 million years.\n
    Arguments
    ----------
    - SOLARRAD [`output table`] : Solar Radiation
    - START [`floating point number`] : Start [ka]. Default: -200.000000
    - STOP [`floating point number`] : Stop [ka]. Default: 2.000000
    - STEP [`floating point number`] : Step [ka]. Minimum: 0.001000 Default: 1.000000
    - DLAT [`integer number`] : Latitude Increment [Degree]. Minimum: 1 Maximum: 90 Default: 5
    - DAY [`integer number`] : Day of Year. Minimum: 0 Maximum: 366 Default: 181

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('climate_tools', '4', 'Daily Insolation over Latitude')
    if Tool.is_Okay():
        Tool.Set_Output('SOLARRAD', SOLARRAD)
        Tool.Set_Option('START', START)
        Tool.Set_Option('STOP', STOP)
        Tool.Set_Option('STEP', STEP)
        Tool.Set_Option('DLAT', DLAT)
        Tool.Set_Option('DAY', DAY)
        return Tool.Execute(Verbose)
    return False

def Monthly_Global_by_Latitude(ALBEDO=None, SOLARRAD=None, FIELD=None, YEAR=None, DLAT=None, Verbose=2):
    '''
    Monthly Global by Latitude
    ----------
    [climate_tools.5]\n
    Orbital parameters used here are based on the work of Andre L. Berger and its implementation from the NASA Goddard Institute for Space Studies (GISS). Berger's orbital parameters are considered to be valid for approximately 1 million years.\n
    Arguments
    ----------
    - ALBEDO [`optional input table`] : Albedo
    - SOLARRAD [`output table`] : Solar Radiation
    - FIELD [`table field`] : Field
    - YEAR [`floating point number`] : Year [ka]. Default: 2.000000
    - DLAT [`integer number`] : Latitude Increment [Degree]. Minimum: 1 Maximum: 90 Default: 5

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('climate_tools', '5', 'Monthly Global by Latitude')
    if Tool.is_Okay():
        Tool.Set_Input ('ALBEDO', ALBEDO)
        Tool.Set_Output('SOLARRAD', SOLARRAD)
        Tool.Set_Option('FIELD', FIELD)
        Tool.Set_Option('YEAR', YEAR)
        Tool.Set_Option('DLAT', DLAT)
        return Tool.Execute(Verbose)
    return False

def run_tool_climate_tools_5(ALBEDO=None, SOLARRAD=None, FIELD=None, YEAR=None, DLAT=None, Verbose=2):
    '''
    Monthly Global by Latitude
    ----------
    [climate_tools.5]\n
    Orbital parameters used here are based on the work of Andre L. Berger and its implementation from the NASA Goddard Institute for Space Studies (GISS). Berger's orbital parameters are considered to be valid for approximately 1 million years.\n
    Arguments
    ----------
    - ALBEDO [`optional input table`] : Albedo
    - SOLARRAD [`output table`] : Solar Radiation
    - FIELD [`table field`] : Field
    - YEAR [`floating point number`] : Year [ka]. Default: 2.000000
    - DLAT [`integer number`] : Latitude Increment [Degree]. Minimum: 1 Maximum: 90 Default: 5

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('climate_tools', '5', 'Monthly Global by Latitude')
    if Tool.is_Okay():
        Tool.Set_Input ('ALBEDO', ALBEDO)
        Tool.Set_Output('SOLARRAD', SOLARRAD)
        Tool.Set_Option('FIELD', FIELD)
        Tool.Set_Option('YEAR', YEAR)
        Tool.Set_Option('DLAT', DLAT)
        return Tool.Execute(Verbose)
    return False

def Evapotranspiration_Table(TABLE=None, RESULT=None, T=None, T_MIN=None, T_MAX=None, RH=None, SR=None, WS=None, P=None, DATE=None, ET=None, LAT=None, METHOD=None, Verbose=2):
    '''
    Evapotranspiration (Table)
    ----------
    [climate_tools.6]\n
    Estimation of daily potential evapotranspiration from weather data with different methods. Besides mean daily temperature it depends on the chosen method which additional data has to be provided. In order to estimate extraterrestrial net radiation some of the methods need to know the location's geographic latitude and the date's Julian day number.\n
    Arguments
    ----------
    - TABLE [`input table`] : Data
    - RESULT [`output table`] : Results
    - T [`table field`] : Mean Temperature. [°C]
    - T_MIN [`table field`] : Minimum Temperature. [°C]
    - T_MAX [`table field`] : Maximum Temperature. [°C]
    - RH [`table field`] : Relative Humidity. [%]
    - SR [`table field`] : Solar Radiation. daily sum of global horizontal irradiance [J/cm²]
    - WS [`table field`] : Wind Speed. daily mean of wind speed at 2m above ground [m/s]
    - P [`table field`] : Air Pressure. [kPa]
    - DATE [`table field`] : Date
    - ET [`table field`] : Evapotranspiration
    - LAT [`floating point number`] : Latitude. Minimum: -90.000000 Maximum: 90.000000 Default: 53.000000
    - METHOD [`choice`] : Method. Available Choices: [0] Turc [1] Hargreave [2] Penman (simplified) [3] Penman-Monteith Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('climate_tools', '6', 'Evapotranspiration (Table)')
    if Tool.is_Okay():
        Tool.Set_Input ('TABLE', TABLE)
        Tool.Set_Output('RESULT', RESULT)
        Tool.Set_Option('T', T)
        Tool.Set_Option('T_MIN', T_MIN)
        Tool.Set_Option('T_MAX', T_MAX)
        Tool.Set_Option('RH', RH)
        Tool.Set_Option('SR', SR)
        Tool.Set_Option('WS', WS)
        Tool.Set_Option('P', P)
        Tool.Set_Option('DATE', DATE)
        Tool.Set_Option('ET', ET)
        Tool.Set_Option('LAT', LAT)
        Tool.Set_Option('METHOD', METHOD)
        return Tool.Execute(Verbose)
    return False

def run_tool_climate_tools_6(TABLE=None, RESULT=None, T=None, T_MIN=None, T_MAX=None, RH=None, SR=None, WS=None, P=None, DATE=None, ET=None, LAT=None, METHOD=None, Verbose=2):
    '''
    Evapotranspiration (Table)
    ----------
    [climate_tools.6]\n
    Estimation of daily potential evapotranspiration from weather data with different methods. Besides mean daily temperature it depends on the chosen method which additional data has to be provided. In order to estimate extraterrestrial net radiation some of the methods need to know the location's geographic latitude and the date's Julian day number.\n
    Arguments
    ----------
    - TABLE [`input table`] : Data
    - RESULT [`output table`] : Results
    - T [`table field`] : Mean Temperature. [°C]
    - T_MIN [`table field`] : Minimum Temperature. [°C]
    - T_MAX [`table field`] : Maximum Temperature. [°C]
    - RH [`table field`] : Relative Humidity. [%]
    - SR [`table field`] : Solar Radiation. daily sum of global horizontal irradiance [J/cm²]
    - WS [`table field`] : Wind Speed. daily mean of wind speed at 2m above ground [m/s]
    - P [`table field`] : Air Pressure. [kPa]
    - DATE [`table field`] : Date
    - ET [`table field`] : Evapotranspiration
    - LAT [`floating point number`] : Latitude. Minimum: -90.000000 Maximum: 90.000000 Default: 53.000000
    - METHOD [`choice`] : Method. Available Choices: [0] Turc [1] Hargreave [2] Penman (simplified) [3] Penman-Monteith Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('climate_tools', '6', 'Evapotranspiration (Table)')
    if Tool.is_Okay():
        Tool.Set_Input ('TABLE', TABLE)
        Tool.Set_Output('RESULT', RESULT)
        Tool.Set_Option('T', T)
        Tool.Set_Option('T_MIN', T_MIN)
        Tool.Set_Option('T_MAX', T_MAX)
        Tool.Set_Option('RH', RH)
        Tool.Set_Option('SR', SR)
        Tool.Set_Option('WS', WS)
        Tool.Set_Option('P', P)
        Tool.Set_Option('DATE', DATE)
        Tool.Set_Option('ET', ET)
        Tool.Set_Option('LAT', LAT)
        Tool.Set_Option('METHOD', METHOD)
        return Tool.Execute(Verbose)
    return False

def Daily_to_Hourly_Evapotranspiration(DAYS=None, HOURS=None, JD=None, ET=None, P=None, LAT=None, Verbose=2):
    '''
    Daily to Hourly Evapotranspiration
    ----------
    [climate_tools.7]\n
    Derive hourly from daily evapotranspiration using sinusoidal distribution.\n
    Arguments
    ----------
    - DAYS [`input table`] : Daily Data
    - HOURS [`output table`] : Hourly Data
    - JD [`table field`] : Julian Day
    - ET [`table field`] : Evapotranspiration
    - P [`table field`] : Precipitation
    - LAT [`floating point number`] : Latitude. Minimum: -90.000000 Maximum: 90.000000 Default: 53.000000

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('climate_tools', '7', 'Daily to Hourly Evapotranspiration')
    if Tool.is_Okay():
        Tool.Set_Input ('DAYS', DAYS)
        Tool.Set_Output('HOURS', HOURS)
        Tool.Set_Option('JD', JD)
        Tool.Set_Option('ET', ET)
        Tool.Set_Option('P', P)
        Tool.Set_Option('LAT', LAT)
        return Tool.Execute(Verbose)
    return False

def run_tool_climate_tools_7(DAYS=None, HOURS=None, JD=None, ET=None, P=None, LAT=None, Verbose=2):
    '''
    Daily to Hourly Evapotranspiration
    ----------
    [climate_tools.7]\n
    Derive hourly from daily evapotranspiration using sinusoidal distribution.\n
    Arguments
    ----------
    - DAYS [`input table`] : Daily Data
    - HOURS [`output table`] : Hourly Data
    - JD [`table field`] : Julian Day
    - ET [`table field`] : Evapotranspiration
    - P [`table field`] : Precipitation
    - LAT [`floating point number`] : Latitude. Minimum: -90.000000 Maximum: 90.000000 Default: 53.000000

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('climate_tools', '7', 'Daily to Hourly Evapotranspiration')
    if Tool.is_Okay():
        Tool.Set_Input ('DAYS', DAYS)
        Tool.Set_Output('HOURS', HOURS)
        Tool.Set_Option('JD', JD)
        Tool.Set_Option('ET', ET)
        Tool.Set_Option('P', P)
        Tool.Set_Option('LAT', LAT)
        return Tool.Execute(Verbose)
    return False

def Evapotranspiration_Grid(T=None, T_MIN=None, T_MAX=None, RH=None, SR=None, WS=None, P=None, ET=None, T_DEFAULT=None, T_MIN_DEFAULT=None, T_MAX_DEFAULT=None, RH_DEFAULT=None, SR_DEFAULT=None, WS_DEFAULT=None, P_DEFAULT=None, METHOD=None, SR_EST=None, SUNSHINE=None, TIME=None, MONTH=None, DAY=None, LAT=None, Verbose=2):
    '''
    Evapotranspiration (Grid)
    ----------
    [climate_tools.8]\n
    Estimation of daily potential evapotranspiration from weather data with different methods. Besides mean daily temperature it depends on the chosen method which additional data has to be provided. In order to estimate extraterrestrial net radiation some of the methods need to know the location's geographic latitude and the date's Julian day number.\n
    Arguments
    ----------
    - T [`optional input grid`] : Mean Temperature. [°C]
    - T_MIN [`optional input grid`] : Minimum Temperature. [°C]
    - T_MAX [`optional input grid`] : Maximum Temperature. [°C]
    - RH [`optional input grid`] : Relative Humidity. [%]
    - SR [`optional input grid`] : Solar Radiation. [J/cm²]
    - WS [`optional input grid`] : Wind Speed. [m/s]
    - P [`optional input grid`] : Air Pressure. [kPa]
    - ET [`output grid`] : Potential Evapotranspiration
    - T_DEFAULT [`floating point number`] : Default. Minimum: -273.150000 Default: 10.000000 default value if no grid has been selected
    - T_MIN_DEFAULT [`floating point number`] : Default. Minimum: -273.150000 Default: 0.000000 default value if no grid has been selected
    - T_MAX_DEFAULT [`floating point number`] : Default. Minimum: -273.150000 Default: 20.000000 default value if no grid has been selected
    - RH_DEFAULT [`floating point number`] : Default. Minimum: 0.000000 Maximum: 100.000000 Default: 50.000000 default value if no grid has been selected
    - SR_DEFAULT [`floating point number`] : Default. Minimum: 0.000000 Default: 2.000000 default value if no grid has been selected
    - WS_DEFAULT [`floating point number`] : Default. Minimum: 0.000000 Default: 5.000000 default value if no grid has been selected
    - P_DEFAULT [`floating point number`] : Default. Minimum: 0.000000 Default: 101.300000 default value if no grid has been selected
    - METHOD [`choice`] : Method. Available Choices: [0] Turc [1] Hargreave [2] Penman (simplified) [3] Penman-Monteith Default: 0
    - SR_EST [`boolean`] : Estimate Solar Radiation. Default: 0 Estimate solar radiation from date, latitudinal position and sunshine duration as percentage of its potential maximum.
    - SUNSHINE [`floating point number`] : Sunshine Duration. Minimum: 0.000000 Maximum: 100.000000 Default: 50.000000 Daily sunshine duration as percentage of its potential maximum.
    - TIME [`choice`] : Time. Available Choices: [0] day [1] month Default: 0
    - MONTH [`choice`] : Month. Available Choices: [0] January [1] February [2] March [3] April [4] May [5] June [6] July [7] August [8] September [9] October [10] November [11] December Default: 0
    - DAY [`integer number`] : Day of Month. Minimum: 1 Maximum: 31 Default: 20
    - LAT [`floating point number`] : Latitude. Minimum: -90.000000 Maximum: 90.000000 Default: 53.000000 [Degree]

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('climate_tools', '8', 'Evapotranspiration (Grid)')
    if Tool.is_Okay():
        Tool.Set_Input ('T', T)
        Tool.Set_Input ('T_MIN', T_MIN)
        Tool.Set_Input ('T_MAX', T_MAX)
        Tool.Set_Input ('RH', RH)
        Tool.Set_Input ('SR', SR)
        Tool.Set_Input ('WS', WS)
        Tool.Set_Input ('P', P)
        Tool.Set_Output('ET', ET)
        Tool.Set_Option('T_DEFAULT', T_DEFAULT)
        Tool.Set_Option('T_MIN_DEFAULT', T_MIN_DEFAULT)
        Tool.Set_Option('T_MAX_DEFAULT', T_MAX_DEFAULT)
        Tool.Set_Option('RH_DEFAULT', RH_DEFAULT)
        Tool.Set_Option('SR_DEFAULT', SR_DEFAULT)
        Tool.Set_Option('WS_DEFAULT', WS_DEFAULT)
        Tool.Set_Option('P_DEFAULT', P_DEFAULT)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('SR_EST', SR_EST)
        Tool.Set_Option('SUNSHINE', SUNSHINE)
        Tool.Set_Option('TIME', TIME)
        Tool.Set_Option('MONTH', MONTH)
        Tool.Set_Option('DAY', DAY)
        Tool.Set_Option('LAT', LAT)
        return Tool.Execute(Verbose)
    return False

def run_tool_climate_tools_8(T=None, T_MIN=None, T_MAX=None, RH=None, SR=None, WS=None, P=None, ET=None, T_DEFAULT=None, T_MIN_DEFAULT=None, T_MAX_DEFAULT=None, RH_DEFAULT=None, SR_DEFAULT=None, WS_DEFAULT=None, P_DEFAULT=None, METHOD=None, SR_EST=None, SUNSHINE=None, TIME=None, MONTH=None, DAY=None, LAT=None, Verbose=2):
    '''
    Evapotranspiration (Grid)
    ----------
    [climate_tools.8]\n
    Estimation of daily potential evapotranspiration from weather data with different methods. Besides mean daily temperature it depends on the chosen method which additional data has to be provided. In order to estimate extraterrestrial net radiation some of the methods need to know the location's geographic latitude and the date's Julian day number.\n
    Arguments
    ----------
    - T [`optional input grid`] : Mean Temperature. [°C]
    - T_MIN [`optional input grid`] : Minimum Temperature. [°C]
    - T_MAX [`optional input grid`] : Maximum Temperature. [°C]
    - RH [`optional input grid`] : Relative Humidity. [%]
    - SR [`optional input grid`] : Solar Radiation. [J/cm²]
    - WS [`optional input grid`] : Wind Speed. [m/s]
    - P [`optional input grid`] : Air Pressure. [kPa]
    - ET [`output grid`] : Potential Evapotranspiration
    - T_DEFAULT [`floating point number`] : Default. Minimum: -273.150000 Default: 10.000000 default value if no grid has been selected
    - T_MIN_DEFAULT [`floating point number`] : Default. Minimum: -273.150000 Default: 0.000000 default value if no grid has been selected
    - T_MAX_DEFAULT [`floating point number`] : Default. Minimum: -273.150000 Default: 20.000000 default value if no grid has been selected
    - RH_DEFAULT [`floating point number`] : Default. Minimum: 0.000000 Maximum: 100.000000 Default: 50.000000 default value if no grid has been selected
    - SR_DEFAULT [`floating point number`] : Default. Minimum: 0.000000 Default: 2.000000 default value if no grid has been selected
    - WS_DEFAULT [`floating point number`] : Default. Minimum: 0.000000 Default: 5.000000 default value if no grid has been selected
    - P_DEFAULT [`floating point number`] : Default. Minimum: 0.000000 Default: 101.300000 default value if no grid has been selected
    - METHOD [`choice`] : Method. Available Choices: [0] Turc [1] Hargreave [2] Penman (simplified) [3] Penman-Monteith Default: 0
    - SR_EST [`boolean`] : Estimate Solar Radiation. Default: 0 Estimate solar radiation from date, latitudinal position and sunshine duration as percentage of its potential maximum.
    - SUNSHINE [`floating point number`] : Sunshine Duration. Minimum: 0.000000 Maximum: 100.000000 Default: 50.000000 Daily sunshine duration as percentage of its potential maximum.
    - TIME [`choice`] : Time. Available Choices: [0] day [1] month Default: 0
    - MONTH [`choice`] : Month. Available Choices: [0] January [1] February [2] March [3] April [4] May [5] June [6] July [7] August [8] September [9] October [10] November [11] December Default: 0
    - DAY [`integer number`] : Day of Month. Minimum: 1 Maximum: 31 Default: 20
    - LAT [`floating point number`] : Latitude. Minimum: -90.000000 Maximum: 90.000000 Default: 53.000000 [Degree]

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('climate_tools', '8', 'Evapotranspiration (Grid)')
    if Tool.is_Okay():
        Tool.Set_Input ('T', T)
        Tool.Set_Input ('T_MIN', T_MIN)
        Tool.Set_Input ('T_MAX', T_MAX)
        Tool.Set_Input ('RH', RH)
        Tool.Set_Input ('SR', SR)
        Tool.Set_Input ('WS', WS)
        Tool.Set_Input ('P', P)
        Tool.Set_Output('ET', ET)
        Tool.Set_Option('T_DEFAULT', T_DEFAULT)
        Tool.Set_Option('T_MIN_DEFAULT', T_MIN_DEFAULT)
        Tool.Set_Option('T_MAX_DEFAULT', T_MAX_DEFAULT)
        Tool.Set_Option('RH_DEFAULT', RH_DEFAULT)
        Tool.Set_Option('SR_DEFAULT', SR_DEFAULT)
        Tool.Set_Option('WS_DEFAULT', WS_DEFAULT)
        Tool.Set_Option('P_DEFAULT', P_DEFAULT)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('SR_EST', SR_EST)
        Tool.Set_Option('SUNSHINE', SUNSHINE)
        Tool.Set_Option('TIME', TIME)
        Tool.Set_Option('MONTH', MONTH)
        Tool.Set_Option('DAY', DAY)
        Tool.Set_Option('LAT', LAT)
        return Tool.Execute(Verbose)
    return False

def Sunrise_and_Sunset(TARGET=None, SUNRISE=None, SUNSET=None, LENGTH=None, DAY=None, TIME=None, Verbose=2):
    '''
    Sunrise and Sunset
    ----------
    [climate_tools.9]\n
    This tool calculates the time of sunrise and sunset and the resulting day length for each cell of the target grid. The target grid needs to provide information about its coordinate system.\n
    Arguments
    ----------
    - TARGET [`input grid`] : Target System
    - SUNRISE [`output grid`] : Sunrise
    - SUNSET [`output grid`] : Sunset
    - LENGTH [`output grid`] : Day Length
    - DAY [`date`] : Day of Month. Default: 2025-01-20
    - TIME [`choice`] : Time. Available Choices: [0] local [1] world Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('climate_tools', '9', 'Sunrise and Sunset')
    if Tool.is_Okay():
        Tool.Set_Input ('TARGET', TARGET)
        Tool.Set_Output('SUNRISE', SUNRISE)
        Tool.Set_Output('SUNSET', SUNSET)
        Tool.Set_Output('LENGTH', LENGTH)
        Tool.Set_Option('DAY', DAY)
        Tool.Set_Option('TIME', TIME)
        return Tool.Execute(Verbose)
    return False

def run_tool_climate_tools_9(TARGET=None, SUNRISE=None, SUNSET=None, LENGTH=None, DAY=None, TIME=None, Verbose=2):
    '''
    Sunrise and Sunset
    ----------
    [climate_tools.9]\n
    This tool calculates the time of sunrise and sunset and the resulting day length for each cell of the target grid. The target grid needs to provide information about its coordinate system.\n
    Arguments
    ----------
    - TARGET [`input grid`] : Target System
    - SUNRISE [`output grid`] : Sunrise
    - SUNSET [`output grid`] : Sunset
    - LENGTH [`output grid`] : Day Length
    - DAY [`date`] : Day of Month. Default: 2025-01-20
    - TIME [`choice`] : Time. Available Choices: [0] local [1] world Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('climate_tools', '9', 'Sunrise and Sunset')
    if Tool.is_Okay():
        Tool.Set_Input ('TARGET', TARGET)
        Tool.Set_Output('SUNRISE', SUNRISE)
        Tool.Set_Output('SUNSET', SUNSET)
        Tool.Set_Output('LENGTH', LENGTH)
        Tool.Set_Option('DAY', DAY)
        Tool.Set_Option('TIME', TIME)
        return Tool.Execute(Verbose)
    return False

def Bioclimatic_Variables(TMEAN=None, TMIN=None, TMAX=None, P=None, BIO_01=None, BIO_02=None, BIO_03=None, BIO_04=None, BIO_05=None, BIO_06=None, BIO_07=None, BIO_08=None, BIO_09=None, BIO_10=None, BIO_11=None, BIO_12=None, BIO_13=None, BIO_14=None, BIO_15=None, BIO_16=None, BIO_17=None, BIO_18=None, BIO_19=None, QUARTER_COLDEST=None, QUARTER_WARMEST=None, QUARTER_DRIEST=None, QUARTER_WETTEST=None, SEASONALITY=None, Verbose=2):
    '''
    Bioclimatic Variables
    ----------
    [climate_tools.10]\n
    This tool calculates biologically meaningful variables from monthly climate data (mean, minimum and maximum temperature and precipitation sum), as provided by climate related projects, e.g.:\n
    (-) [ CHELSA - Climatologies at High resolution for the Earthâ€™s Land Surface Area](https://chelsa-climate.org/bioclim/)\n
    (-) [ WorldClim - Global climate and weather data](https://worldclim.org/data/bioclim.html)\n
    It follows a short definition of the provided bioclimatic variables:\n
    (-) [Annual Mean Temperature:] The mean of all the monthly mean temperatures. Each monthly mean temperature is the mean of that month's maximum and minimum temperature.\n
    (-) [Mean Diurnal Range:] The annual mean of all the monthly diurnal temperature ranges. Each monthly diurnal range is the difference between that month's maximum and minimum temperature.\n
    (-) [Isothermality:] The mean diurnal range (parameter 2) divided by the annual temperature range (parameter 7).\n
    (-) [Temperature Seasonality:] returns either\n
    (-)  the temperature coefficient of variation as the standard deviation of the monthly mean temperatures expressed as a percentage of the mean of those temperatures (i.e. the annual mean). For this calculation, the mean in degrees Kelvin is used. This avoids the possibility of having to divide by zero, but does mean that the values are usually quite small.\n
    (-)  the standard deviation of the monthly mean temperatures.\n
    (-) [Maximum Temperature of Warmest Period:] The highest temperature of any monthly maximum temperature.\n
    (-) [Minimum Temperature of Coldest Period:] The lowest temperature of any monthly minimum temperature.\n
    (-) [Temperature Annual Range:] The difference between the Maximum Temperature of Warmest Period and the Minimum Temperature of Coldest Period.\n
    (-) [Mean Temperature of Wettest Quarter:] The wettest quarter of the year is determined (to the nearest month), and the mean temperature of this period is calculated.\n
    (-) [Mean Temperature of Driest Quarter:] The driest quarter of the year is determined (to the nearest month), and the mean temperature of this period is calculated.\n
    (-) [Mean Temperature of Warmest Quarter:] The warmest quarter of the year is determined (to the nearest month), and the mean temperature of this period is calculated.\n
    (-) [Mean Temperature of Coldest Quarter:] The coldest quarter of the year is determined (to the nearest month), and the mean temperature of this period is calculated.\n
    (-) [Annual Precipitation:] The sum of all the monthly precipitation estimates.\n
    (-) [Precipitation of Wettest Period:] The precipitation of the wettest month.\n
    (-) [Precipitation of Driest Period:] The precipitation of the driest month.\n
    (-) [Precipitation Seasonality:] The Coefficient of Variation is the standard deviation of the monthly precipitation estimates expressed as a percentage of the mean of those estimates (i.e. the annual mean).\n
    (-) [Precipitation of Wettest Quarter:] The wettest quarter of the year is determined (to the nearest month), and the total precipitation over this period is calculated.\n
    (-) [Precipitation of Driest Quarter:] The driest quarter of the year is determined (to the nearest month), and the total precipitation over this period is calculated.\n
    (-) [Precipitation of Warmest Quarter:] The warmest quarter of the year is determined (to the nearest month), and the total precipitation over this period is calculated.\n
    (-) [Precipitation of Coldest Quarter:] The coldest quarter of the year is determined (to the nearest month), and the total precipitation over this period is calculated.\n
    The quarterly parameters are not aligned to any calendar quarters. BioClim's definition of a quarter is any consecutive 3 months. For example, the driest quarter will be the 3 consecutive months that are drier than any other set of 3 consecutive months.\n
    Arguments
    ----------
    - TMEAN [`input grid list`] : Mean Temperature
    - TMIN [`input grid list`] : Minimum Temperature
    - TMAX [`input grid list`] : Maximum Temperature
    - P [`input grid list`] : Precipitation
    - BIO_01 [`output grid`] : Annual Mean Temperature
    - BIO_02 [`output grid`] : Mean Diurnal Range
    - BIO_03 [`output grid`] : Isothermality
    - BIO_04 [`output grid`] : Temperature Seasonality
    - BIO_05 [`output grid`] : Maximum Temperature of Warmest Month
    - BIO_06 [`output grid`] : Minimum Temperature of Coldest Month
    - BIO_07 [`output grid`] : Temperature Annual Range
    - BIO_08 [`output grid`] : Mean Temperature of Wettest Quarter
    - BIO_09 [`output grid`] : Mean Temperature of Driest Quarter
    - BIO_10 [`output grid`] : Mean Temperature of Warmest Quarter
    - BIO_11 [`output grid`] : Mean Temperature of Coldest Quarter
    - BIO_12 [`output grid`] : Annual Precipitation
    - BIO_13 [`output grid`] : Precipitation of Wettest Month
    - BIO_14 [`output grid`] : Precipitation of Driest Month
    - BIO_15 [`output grid`] : Precipitation Seasonality
    - BIO_16 [`output grid`] : Precipitation of Wettest Quarter
    - BIO_17 [`output grid`] : Precipitation of Driest Quarter
    - BIO_18 [`output grid`] : Precipitation of Warmest Quarter
    - BIO_19 [`output grid`] : Precipitation of Coldest Quarter
    - QUARTER_COLDEST [`output grid`] : Coldest Quarter. Result is the number of the 2nd of three consecutive months (Dec-Jan-Feb=1, Jan-Feb-Mar=2, ...).
    - QUARTER_WARMEST [`output grid`] : Warmest Quarter. Result is the number of the 2nd of three consecutive months (Dec-Jan-Feb=1, Jan-Feb-Mar=2, ...).
    - QUARTER_DRIEST [`output grid`] : Driest Quarter. Result is the number of the 2nd of three consecutive months (Dec-Jan-Feb=1, Jan-Feb-Mar=2, ...).
    - QUARTER_WETTEST [`output grid`] : Wettest Quarter. Result is the number of the 2nd of three consecutive months (Dec-Jan-Feb=1, Jan-Feb-Mar=2, ...).
    - SEASONALITY [`choice`] : Temperature Seasonality. Available Choices: [0] Coefficient of Variation [1] Standard Deviation Default: 1

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('climate_tools', '10', 'Bioclimatic Variables')
    if Tool.is_Okay():
        Tool.Set_Input ('TMEAN', TMEAN)
        Tool.Set_Input ('TMIN', TMIN)
        Tool.Set_Input ('TMAX', TMAX)
        Tool.Set_Input ('P', P)
        Tool.Set_Output('BIO_01', BIO_01)
        Tool.Set_Output('BIO_02', BIO_02)
        Tool.Set_Output('BIO_03', BIO_03)
        Tool.Set_Output('BIO_04', BIO_04)
        Tool.Set_Output('BIO_05', BIO_05)
        Tool.Set_Output('BIO_06', BIO_06)
        Tool.Set_Output('BIO_07', BIO_07)
        Tool.Set_Output('BIO_08', BIO_08)
        Tool.Set_Output('BIO_09', BIO_09)
        Tool.Set_Output('BIO_10', BIO_10)
        Tool.Set_Output('BIO_11', BIO_11)
        Tool.Set_Output('BIO_12', BIO_12)
        Tool.Set_Output('BIO_13', BIO_13)
        Tool.Set_Output('BIO_14', BIO_14)
        Tool.Set_Output('BIO_15', BIO_15)
        Tool.Set_Output('BIO_16', BIO_16)
        Tool.Set_Output('BIO_17', BIO_17)
        Tool.Set_Output('BIO_18', BIO_18)
        Tool.Set_Output('BIO_19', BIO_19)
        Tool.Set_Output('QUARTER_COLDEST', QUARTER_COLDEST)
        Tool.Set_Output('QUARTER_WARMEST', QUARTER_WARMEST)
        Tool.Set_Output('QUARTER_DRIEST', QUARTER_DRIEST)
        Tool.Set_Output('QUARTER_WETTEST', QUARTER_WETTEST)
        Tool.Set_Option('SEASONALITY', SEASONALITY)
        return Tool.Execute(Verbose)
    return False

def run_tool_climate_tools_10(TMEAN=None, TMIN=None, TMAX=None, P=None, BIO_01=None, BIO_02=None, BIO_03=None, BIO_04=None, BIO_05=None, BIO_06=None, BIO_07=None, BIO_08=None, BIO_09=None, BIO_10=None, BIO_11=None, BIO_12=None, BIO_13=None, BIO_14=None, BIO_15=None, BIO_16=None, BIO_17=None, BIO_18=None, BIO_19=None, QUARTER_COLDEST=None, QUARTER_WARMEST=None, QUARTER_DRIEST=None, QUARTER_WETTEST=None, SEASONALITY=None, Verbose=2):
    '''
    Bioclimatic Variables
    ----------
    [climate_tools.10]\n
    This tool calculates biologically meaningful variables from monthly climate data (mean, minimum and maximum temperature and precipitation sum), as provided by climate related projects, e.g.:\n
    (-) [ CHELSA - Climatologies at High resolution for the Earthâ€™s Land Surface Area](https://chelsa-climate.org/bioclim/)\n
    (-) [ WorldClim - Global climate and weather data](https://worldclim.org/data/bioclim.html)\n
    It follows a short definition of the provided bioclimatic variables:\n
    (-) [Annual Mean Temperature:] The mean of all the monthly mean temperatures. Each monthly mean temperature is the mean of that month's maximum and minimum temperature.\n
    (-) [Mean Diurnal Range:] The annual mean of all the monthly diurnal temperature ranges. Each monthly diurnal range is the difference between that month's maximum and minimum temperature.\n
    (-) [Isothermality:] The mean diurnal range (parameter 2) divided by the annual temperature range (parameter 7).\n
    (-) [Temperature Seasonality:] returns either\n
    (-)  the temperature coefficient of variation as the standard deviation of the monthly mean temperatures expressed as a percentage of the mean of those temperatures (i.e. the annual mean). For this calculation, the mean in degrees Kelvin is used. This avoids the possibility of having to divide by zero, but does mean that the values are usually quite small.\n
    (-)  the standard deviation of the monthly mean temperatures.\n
    (-) [Maximum Temperature of Warmest Period:] The highest temperature of any monthly maximum temperature.\n
    (-) [Minimum Temperature of Coldest Period:] The lowest temperature of any monthly minimum temperature.\n
    (-) [Temperature Annual Range:] The difference between the Maximum Temperature of Warmest Period and the Minimum Temperature of Coldest Period.\n
    (-) [Mean Temperature of Wettest Quarter:] The wettest quarter of the year is determined (to the nearest month), and the mean temperature of this period is calculated.\n
    (-) [Mean Temperature of Driest Quarter:] The driest quarter of the year is determined (to the nearest month), and the mean temperature of this period is calculated.\n
    (-) [Mean Temperature of Warmest Quarter:] The warmest quarter of the year is determined (to the nearest month), and the mean temperature of this period is calculated.\n
    (-) [Mean Temperature of Coldest Quarter:] The coldest quarter of the year is determined (to the nearest month), and the mean temperature of this period is calculated.\n
    (-) [Annual Precipitation:] The sum of all the monthly precipitation estimates.\n
    (-) [Precipitation of Wettest Period:] The precipitation of the wettest month.\n
    (-) [Precipitation of Driest Period:] The precipitation of the driest month.\n
    (-) [Precipitation Seasonality:] The Coefficient of Variation is the standard deviation of the monthly precipitation estimates expressed as a percentage of the mean of those estimates (i.e. the annual mean).\n
    (-) [Precipitation of Wettest Quarter:] The wettest quarter of the year is determined (to the nearest month), and the total precipitation over this period is calculated.\n
    (-) [Precipitation of Driest Quarter:] The driest quarter of the year is determined (to the nearest month), and the total precipitation over this period is calculated.\n
    (-) [Precipitation of Warmest Quarter:] The warmest quarter of the year is determined (to the nearest month), and the total precipitation over this period is calculated.\n
    (-) [Precipitation of Coldest Quarter:] The coldest quarter of the year is determined (to the nearest month), and the total precipitation over this period is calculated.\n
    The quarterly parameters are not aligned to any calendar quarters. BioClim's definition of a quarter is any consecutive 3 months. For example, the driest quarter will be the 3 consecutive months that are drier than any other set of 3 consecutive months.\n
    Arguments
    ----------
    - TMEAN [`input grid list`] : Mean Temperature
    - TMIN [`input grid list`] : Minimum Temperature
    - TMAX [`input grid list`] : Maximum Temperature
    - P [`input grid list`] : Precipitation
    - BIO_01 [`output grid`] : Annual Mean Temperature
    - BIO_02 [`output grid`] : Mean Diurnal Range
    - BIO_03 [`output grid`] : Isothermality
    - BIO_04 [`output grid`] : Temperature Seasonality
    - BIO_05 [`output grid`] : Maximum Temperature of Warmest Month
    - BIO_06 [`output grid`] : Minimum Temperature of Coldest Month
    - BIO_07 [`output grid`] : Temperature Annual Range
    - BIO_08 [`output grid`] : Mean Temperature of Wettest Quarter
    - BIO_09 [`output grid`] : Mean Temperature of Driest Quarter
    - BIO_10 [`output grid`] : Mean Temperature of Warmest Quarter
    - BIO_11 [`output grid`] : Mean Temperature of Coldest Quarter
    - BIO_12 [`output grid`] : Annual Precipitation
    - BIO_13 [`output grid`] : Precipitation of Wettest Month
    - BIO_14 [`output grid`] : Precipitation of Driest Month
    - BIO_15 [`output grid`] : Precipitation Seasonality
    - BIO_16 [`output grid`] : Precipitation of Wettest Quarter
    - BIO_17 [`output grid`] : Precipitation of Driest Quarter
    - BIO_18 [`output grid`] : Precipitation of Warmest Quarter
    - BIO_19 [`output grid`] : Precipitation of Coldest Quarter
    - QUARTER_COLDEST [`output grid`] : Coldest Quarter. Result is the number of the 2nd of three consecutive months (Dec-Jan-Feb=1, Jan-Feb-Mar=2, ...).
    - QUARTER_WARMEST [`output grid`] : Warmest Quarter. Result is the number of the 2nd of three consecutive months (Dec-Jan-Feb=1, Jan-Feb-Mar=2, ...).
    - QUARTER_DRIEST [`output grid`] : Driest Quarter. Result is the number of the 2nd of three consecutive months (Dec-Jan-Feb=1, Jan-Feb-Mar=2, ...).
    - QUARTER_WETTEST [`output grid`] : Wettest Quarter. Result is the number of the 2nd of three consecutive months (Dec-Jan-Feb=1, Jan-Feb-Mar=2, ...).
    - SEASONALITY [`choice`] : Temperature Seasonality. Available Choices: [0] Coefficient of Variation [1] Standard Deviation Default: 1

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('climate_tools', '10', 'Bioclimatic Variables')
    if Tool.is_Okay():
        Tool.Set_Input ('TMEAN', TMEAN)
        Tool.Set_Input ('TMIN', TMIN)
        Tool.Set_Input ('TMAX', TMAX)
        Tool.Set_Input ('P', P)
        Tool.Set_Output('BIO_01', BIO_01)
        Tool.Set_Output('BIO_02', BIO_02)
        Tool.Set_Output('BIO_03', BIO_03)
        Tool.Set_Output('BIO_04', BIO_04)
        Tool.Set_Output('BIO_05', BIO_05)
        Tool.Set_Output('BIO_06', BIO_06)
        Tool.Set_Output('BIO_07', BIO_07)
        Tool.Set_Output('BIO_08', BIO_08)
        Tool.Set_Output('BIO_09', BIO_09)
        Tool.Set_Output('BIO_10', BIO_10)
        Tool.Set_Output('BIO_11', BIO_11)
        Tool.Set_Output('BIO_12', BIO_12)
        Tool.Set_Output('BIO_13', BIO_13)
        Tool.Set_Output('BIO_14', BIO_14)
        Tool.Set_Output('BIO_15', BIO_15)
        Tool.Set_Output('BIO_16', BIO_16)
        Tool.Set_Output('BIO_17', BIO_17)
        Tool.Set_Output('BIO_18', BIO_18)
        Tool.Set_Output('BIO_19', BIO_19)
        Tool.Set_Output('QUARTER_COLDEST', QUARTER_COLDEST)
        Tool.Set_Output('QUARTER_WARMEST', QUARTER_WARMEST)
        Tool.Set_Output('QUARTER_DRIEST', QUARTER_DRIEST)
        Tool.Set_Output('QUARTER_WETTEST', QUARTER_WETTEST)
        Tool.Set_Option('SEASONALITY', SEASONALITY)
        return Tool.Execute(Verbose)
    return False

def Tree_Growth_Season(T=None, TMIN=None, TMAX=None, P=None, SWC=None, SMT=None, SMP=None, LGS=None, FIRST=None, LAST=None, TLH=None, SWC_DEFAULT=None, SWC_SURFACE=None, SW1_RESIST=None, LAT_DEF=None, DT_MIN=None, SW_MIN=None, LGS_MIN=None, SMT_MIN=None, TLH_MAX_DIFF=None, Verbose=2):
    '''
    Tree Growth Season
    ----------
    [climate_tools.11]\n
    The 'Tree Growth Season' tool estimates the potential number of days suitable for tree growth as well as the average temperature for these days. The estimation needs monthly data of mean, minimum, and maximum temperature and precipitation. Internally a soil water balance model is run on a daily basis. Using the given thresholds a relative tree line height can optionally be estimated.\n
    Arguments
    ----------
    - T [`input grid list`] : Mean Temperature. Monthly averages of daily mean temperature (12 grids, January to December) [°C]
    - TMIN [`input grid list`] : Minimum Temperature. Monthly averages of daily minimum temperature (12 grids, January to December) [°C]
    - TMAX [`input grid list`] : Maximum Temperature. Monthly averages of daily maximum temperature (12 grids, January to December) [°C]
    - P [`input grid list`] : Precipitation. Monthly sums of precipitation (12 grids, January to December) [mm / m²]
    - SWC [`optional input grid`] : Soil Water Capacity of Profile. Total soil water capacity (mm H₂O).
    - SMT [`output grid`] : Mean Temperature. Mean temperature of the growing season.
    - SMP [`output grid`] : Precipitation Sum. Precipitation sum of the growing season.
    - LGS [`output grid`] : Length. Number of days of the growing season.
    - FIRST [`output grid`] : First Growing Day. First growing day of the year (1-365).
    - LAST [`output grid`] : Last Growing Day. Last degree day of the year (1-365).
    - TLH [`output grid`] : Tree Line Height. Estimated relative tree line height.
    - SWC_DEFAULT [`floating point number`] : Default. Minimum: 0.000000 Default: 220.000000 default value if no grid has been selected
    - SWC_SURFACE [`floating point number`] : Top Soil Water Capacity. Minimum: 0.000000 Default: 10.000000
    - SW1_RESIST [`floating point number`] : Transpiration Resistance. Minimum: 0.010000 Default: 1.000000
    - LAT_DEF [`floating point number`] : Default Latitude. Minimum: -90.000000 Maximum: 90.000000 Default: 0.000000
    - DT_MIN [`floating point number`] : Threshold Temperature. Default: 0.900000 Threshold temperature (C) that constrains the growing season.
    - SW_MIN [`floating point number`] : Minimum Soil Water Content (Percent). Minimum: 0.000000 Default: 2.000000
    - LGS_MIN [`integer number`] : Minimum Length. Minimum: 1 Default: 94 Minimum length (days) of the growing season.
    - SMT_MIN [`floating point number`] : Minimum Mean Temperature. Default: 6.400000 Minimum mean temperature (Celsius) for all days of the growing season.
    - TLH_MAX_DIFF [`floating point number`] : Maximum Tree Line Height Difference. Minimum: 0.000000 Default: 3000.000000

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('climate_tools', '11', 'Tree Growth Season')
    if Tool.is_Okay():
        Tool.Set_Input ('T', T)
        Tool.Set_Input ('TMIN', TMIN)
        Tool.Set_Input ('TMAX', TMAX)
        Tool.Set_Input ('P', P)
        Tool.Set_Input ('SWC', SWC)
        Tool.Set_Output('SMT', SMT)
        Tool.Set_Output('SMP', SMP)
        Tool.Set_Output('LGS', LGS)
        Tool.Set_Output('FIRST', FIRST)
        Tool.Set_Output('LAST', LAST)
        Tool.Set_Output('TLH', TLH)
        Tool.Set_Option('SWC_DEFAULT', SWC_DEFAULT)
        Tool.Set_Option('SWC_SURFACE', SWC_SURFACE)
        Tool.Set_Option('SW1_RESIST', SW1_RESIST)
        Tool.Set_Option('LAT_DEF', LAT_DEF)
        Tool.Set_Option('DT_MIN', DT_MIN)
        Tool.Set_Option('SW_MIN', SW_MIN)
        Tool.Set_Option('LGS_MIN', LGS_MIN)
        Tool.Set_Option('SMT_MIN', SMT_MIN)
        Tool.Set_Option('TLH_MAX_DIFF', TLH_MAX_DIFF)
        return Tool.Execute(Verbose)
    return False

def run_tool_climate_tools_11(T=None, TMIN=None, TMAX=None, P=None, SWC=None, SMT=None, SMP=None, LGS=None, FIRST=None, LAST=None, TLH=None, SWC_DEFAULT=None, SWC_SURFACE=None, SW1_RESIST=None, LAT_DEF=None, DT_MIN=None, SW_MIN=None, LGS_MIN=None, SMT_MIN=None, TLH_MAX_DIFF=None, Verbose=2):
    '''
    Tree Growth Season
    ----------
    [climate_tools.11]\n
    The 'Tree Growth Season' tool estimates the potential number of days suitable for tree growth as well as the average temperature for these days. The estimation needs monthly data of mean, minimum, and maximum temperature and precipitation. Internally a soil water balance model is run on a daily basis. Using the given thresholds a relative tree line height can optionally be estimated.\n
    Arguments
    ----------
    - T [`input grid list`] : Mean Temperature. Monthly averages of daily mean temperature (12 grids, January to December) [°C]
    - TMIN [`input grid list`] : Minimum Temperature. Monthly averages of daily minimum temperature (12 grids, January to December) [°C]
    - TMAX [`input grid list`] : Maximum Temperature. Monthly averages of daily maximum temperature (12 grids, January to December) [°C]
    - P [`input grid list`] : Precipitation. Monthly sums of precipitation (12 grids, January to December) [mm / m²]
    - SWC [`optional input grid`] : Soil Water Capacity of Profile. Total soil water capacity (mm H₂O).
    - SMT [`output grid`] : Mean Temperature. Mean temperature of the growing season.
    - SMP [`output grid`] : Precipitation Sum. Precipitation sum of the growing season.
    - LGS [`output grid`] : Length. Number of days of the growing season.
    - FIRST [`output grid`] : First Growing Day. First growing day of the year (1-365).
    - LAST [`output grid`] : Last Growing Day. Last degree day of the year (1-365).
    - TLH [`output grid`] : Tree Line Height. Estimated relative tree line height.
    - SWC_DEFAULT [`floating point number`] : Default. Minimum: 0.000000 Default: 220.000000 default value if no grid has been selected
    - SWC_SURFACE [`floating point number`] : Top Soil Water Capacity. Minimum: 0.000000 Default: 10.000000
    - SW1_RESIST [`floating point number`] : Transpiration Resistance. Minimum: 0.010000 Default: 1.000000
    - LAT_DEF [`floating point number`] : Default Latitude. Minimum: -90.000000 Maximum: 90.000000 Default: 0.000000
    - DT_MIN [`floating point number`] : Threshold Temperature. Default: 0.900000 Threshold temperature (C) that constrains the growing season.
    - SW_MIN [`floating point number`] : Minimum Soil Water Content (Percent). Minimum: 0.000000 Default: 2.000000
    - LGS_MIN [`integer number`] : Minimum Length. Minimum: 1 Default: 94 Minimum length (days) of the growing season.
    - SMT_MIN [`floating point number`] : Minimum Mean Temperature. Default: 6.400000 Minimum mean temperature (Celsius) for all days of the growing season.
    - TLH_MAX_DIFF [`floating point number`] : Maximum Tree Line Height Difference. Minimum: 0.000000 Default: 3000.000000

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('climate_tools', '11', 'Tree Growth Season')
    if Tool.is_Okay():
        Tool.Set_Input ('T', T)
        Tool.Set_Input ('TMIN', TMIN)
        Tool.Set_Input ('TMAX', TMAX)
        Tool.Set_Input ('P', P)
        Tool.Set_Input ('SWC', SWC)
        Tool.Set_Output('SMT', SMT)
        Tool.Set_Output('SMP', SMP)
        Tool.Set_Output('LGS', LGS)
        Tool.Set_Output('FIRST', FIRST)
        Tool.Set_Output('LAST', LAST)
        Tool.Set_Output('TLH', TLH)
        Tool.Set_Option('SWC_DEFAULT', SWC_DEFAULT)
        Tool.Set_Option('SWC_SURFACE', SWC_SURFACE)
        Tool.Set_Option('SW1_RESIST', SW1_RESIST)
        Tool.Set_Option('LAT_DEF', LAT_DEF)
        Tool.Set_Option('DT_MIN', DT_MIN)
        Tool.Set_Option('SW_MIN', SW_MIN)
        Tool.Set_Option('LGS_MIN', LGS_MIN)
        Tool.Set_Option('SMT_MIN', SMT_MIN)
        Tool.Set_Option('TLH_MAX_DIFF', TLH_MAX_DIFF)
        return Tool.Execute(Verbose)
    return False

def Wind_Effect_Correction(BOUNDARY=None, WIND=None, OBSERVED=None, B_GRID=None, WINDCORR=None, B_SOURCE=None, B_CONST=None, B_MAX=None, B_STEPS=None, KERNEL_TYPE=None, KERNEL_SIZE=None, Verbose=2):
    '''
    Wind Effect Correction
    ----------
    [climate_tools.13]\n
    Wind effect correction using generalized logistic functions.\n
    Arguments
    ----------
    - BOUNDARY [`input grid`] : Boundary Layer. The absolute vertical distance to the boundary layer.
    - WIND [`input grid`] : Wind Effect
    - OBSERVED [`input grid`] : Observations. Observations used for local scaling factor calibration (e.g. precipitation, cloudiness).
    - B_GRID [`output grid`] : Calibrated Scaling Factor. Calibrated scaling factor used in the wind effect correction.
    - WINDCORR [`output grid`] : Corrected Wind Effect
    - B_SOURCE [`choice`] : Scaling Factor. Available Choices: [0] constant [1] calibrate Default: 1
    - B_CONST [`floating point number`] : Constant Scaling Factor. Minimum: 0.000000 Default: 0.010000 Constant scaling factor used in the wind effect correction.
    - B_MAX [`floating point number`] : Maximum Scaling Factor. Minimum: 0.000000 Default: 0.050000
    - B_STEPS [`integer number`] : Number of Steps. Minimum: 1 Default: 10
    - KERNEL_TYPE [`choice`] : Kernel Type. Available Choices: [0] Square [1] Circle Default: 1 Kernel specification used to request observations for local scaling factor calibration.
    - KERNEL_SIZE [`integer number`] : Kernel Size. Minimum: 1 Default: 2 Kernel specification used to request observations for local scaling factor calibration.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('climate_tools', '13', 'Wind Effect Correction')
    if Tool.is_Okay():
        Tool.Set_Input ('BOUNDARY', BOUNDARY)
        Tool.Set_Input ('WIND', WIND)
        Tool.Set_Input ('OBSERVED', OBSERVED)
        Tool.Set_Output('B_GRID', B_GRID)
        Tool.Set_Output('WINDCORR', WINDCORR)
        Tool.Set_Option('B_SOURCE', B_SOURCE)
        Tool.Set_Option('B_CONST', B_CONST)
        Tool.Set_Option('B_MAX', B_MAX)
        Tool.Set_Option('B_STEPS', B_STEPS)
        Tool.Set_Option('KERNEL_TYPE', KERNEL_TYPE)
        Tool.Set_Option('KERNEL_SIZE', KERNEL_SIZE)
        return Tool.Execute(Verbose)
    return False

def run_tool_climate_tools_13(BOUNDARY=None, WIND=None, OBSERVED=None, B_GRID=None, WINDCORR=None, B_SOURCE=None, B_CONST=None, B_MAX=None, B_STEPS=None, KERNEL_TYPE=None, KERNEL_SIZE=None, Verbose=2):
    '''
    Wind Effect Correction
    ----------
    [climate_tools.13]\n
    Wind effect correction using generalized logistic functions.\n
    Arguments
    ----------
    - BOUNDARY [`input grid`] : Boundary Layer. The absolute vertical distance to the boundary layer.
    - WIND [`input grid`] : Wind Effect
    - OBSERVED [`input grid`] : Observations. Observations used for local scaling factor calibration (e.g. precipitation, cloudiness).
    - B_GRID [`output grid`] : Calibrated Scaling Factor. Calibrated scaling factor used in the wind effect correction.
    - WINDCORR [`output grid`] : Corrected Wind Effect
    - B_SOURCE [`choice`] : Scaling Factor. Available Choices: [0] constant [1] calibrate Default: 1
    - B_CONST [`floating point number`] : Constant Scaling Factor. Minimum: 0.000000 Default: 0.010000 Constant scaling factor used in the wind effect correction.
    - B_MAX [`floating point number`] : Maximum Scaling Factor. Minimum: 0.000000 Default: 0.050000
    - B_STEPS [`integer number`] : Number of Steps. Minimum: 1 Default: 10
    - KERNEL_TYPE [`choice`] : Kernel Type. Available Choices: [0] Square [1] Circle Default: 1 Kernel specification used to request observations for local scaling factor calibration.
    - KERNEL_SIZE [`integer number`] : Kernel Size. Minimum: 1 Default: 2 Kernel specification used to request observations for local scaling factor calibration.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('climate_tools', '13', 'Wind Effect Correction')
    if Tool.is_Okay():
        Tool.Set_Input ('BOUNDARY', BOUNDARY)
        Tool.Set_Input ('WIND', WIND)
        Tool.Set_Input ('OBSERVED', OBSERVED)
        Tool.Set_Output('B_GRID', B_GRID)
        Tool.Set_Output('WINDCORR', WINDCORR)
        Tool.Set_Option('B_SOURCE', B_SOURCE)
        Tool.Set_Option('B_CONST', B_CONST)
        Tool.Set_Option('B_MAX', B_MAX)
        Tool.Set_Option('B_STEPS', B_STEPS)
        Tool.Set_Option('KERNEL_TYPE', KERNEL_TYPE)
        Tool.Set_Option('KERNEL_SIZE', KERNEL_SIZE)
        return Tool.Execute(Verbose)
    return False

def Frost_Change_Frequency(TMIN=None, TMAX=None, FREQUENCY=None, DT_MEAN=None, DT_MAX=None, DT_STDV=None, TMIN_MEAN=None, TMIN_MIN=None, Verbose=2):
    '''
    Frost Change Frequency
    ----------
    [climate_tools.14]\n
    This tool calculates statistics about the frost change frequency either from monthly or daily minimum and maximum temperatures. In case of monthly observations these will be spline interpolated to gain a daily resolution.\n
    Arguments
    ----------
    - TMIN [`input grid list`] : Minimum Temperature. Monthly (12) or daily (365) temperature observations.
    - TMAX [`input grid list`] : Maximum Temperature. Monthly (12) or daily (365) temperature observations.
    - FREQUENCY [`output grid`] : Frost Change Frequency. The number of days with frost change.
    - DT_MEAN [`output grid`] : Mean Temperature Span. Mean daily temperature span of frost change days.
    - DT_MAX [`output grid`] : Maximum Temperature Span. Maximum daily temperature span of frost change days.
    - DT_STDV [`output grid`] : Standard Deviation of Temperature Span. Standard deviation of daily temperature span of frost change days.
    - TMIN_MEAN [`output grid`] : Mean Minimum Temperature. Mean daily minimum temperature of frost change days.
    - TMIN_MIN [`output grid`] : Minimum Temperature. Coldest daily minimum temperature of all frost change days.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('climate_tools', '14', 'Frost Change Frequency')
    if Tool.is_Okay():
        Tool.Set_Input ('TMIN', TMIN)
        Tool.Set_Input ('TMAX', TMAX)
        Tool.Set_Output('FREQUENCY', FREQUENCY)
        Tool.Set_Output('DT_MEAN', DT_MEAN)
        Tool.Set_Output('DT_MAX', DT_MAX)
        Tool.Set_Output('DT_STDV', DT_STDV)
        Tool.Set_Output('TMIN_MEAN', TMIN_MEAN)
        Tool.Set_Output('TMIN_MIN', TMIN_MIN)
        return Tool.Execute(Verbose)
    return False

def run_tool_climate_tools_14(TMIN=None, TMAX=None, FREQUENCY=None, DT_MEAN=None, DT_MAX=None, DT_STDV=None, TMIN_MEAN=None, TMIN_MIN=None, Verbose=2):
    '''
    Frost Change Frequency
    ----------
    [climate_tools.14]\n
    This tool calculates statistics about the frost change frequency either from monthly or daily minimum and maximum temperatures. In case of monthly observations these will be spline interpolated to gain a daily resolution.\n
    Arguments
    ----------
    - TMIN [`input grid list`] : Minimum Temperature. Monthly (12) or daily (365) temperature observations.
    - TMAX [`input grid list`] : Maximum Temperature. Monthly (12) or daily (365) temperature observations.
    - FREQUENCY [`output grid`] : Frost Change Frequency. The number of days with frost change.
    - DT_MEAN [`output grid`] : Mean Temperature Span. Mean daily temperature span of frost change days.
    - DT_MAX [`output grid`] : Maximum Temperature Span. Maximum daily temperature span of frost change days.
    - DT_STDV [`output grid`] : Standard Deviation of Temperature Span. Standard deviation of daily temperature span of frost change days.
    - TMIN_MEAN [`output grid`] : Mean Minimum Temperature. Mean daily minimum temperature of frost change days.
    - TMIN_MIN [`output grid`] : Minimum Temperature. Coldest daily minimum temperature of all frost change days.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('climate_tools', '14', 'Frost Change Frequency')
    if Tool.is_Okay():
        Tool.Set_Input ('TMIN', TMIN)
        Tool.Set_Input ('TMAX', TMAX)
        Tool.Set_Output('FREQUENCY', FREQUENCY)
        Tool.Set_Output('DT_MEAN', DT_MEAN)
        Tool.Set_Output('DT_MAX', DT_MAX)
        Tool.Set_Output('DT_STDV', DT_STDV)
        Tool.Set_Output('TMIN_MEAN', TMIN_MEAN)
        Tool.Set_Output('TMIN_MIN', TMIN_MIN)
        return Tool.Execute(Verbose)
    return False

def Thermic_Belt_Classification(GSL=None, GST=None, FROST=None, ATB=None, NIVAL_TEMP=None, TREE_TEMP=None, Verbose=2):
    '''
    Thermic Belt Classification
    ----------
    [climate_tools.15]\n
    Calculates the thermal belts based on mean temperature and length of the growing season.\n
    Arguments
    ----------
    - GSL [`input grid`] : Growing Season Length. Growing season length given as number of days of a year.
    - GST [`input grid`] : Mean Temperature. Mean temperature of the growing season.
    - FROST [`input grid`] : Frost occurrence. Frost occurrence as binary factor.
    - ATB [`output grid`] : Thermal Belts
    - NIVAL_TEMP [`floating point number`] : Minimum Temperature Nival. Default: 3.500000 Minimum Temperature for nival belt.
    - TREE_TEMP [`floating point number`] : Minimum Temperature Treeline. Default: 6.400000 Minimum Temperature for treeline.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('climate_tools', '15', 'Thermic Belt Classification')
    if Tool.is_Okay():
        Tool.Set_Input ('GSL', GSL)
        Tool.Set_Input ('GST', GST)
        Tool.Set_Input ('FROST', FROST)
        Tool.Set_Output('ATB', ATB)
        Tool.Set_Option('NIVAL_TEMP', NIVAL_TEMP)
        Tool.Set_Option('TREE_TEMP', TREE_TEMP)
        return Tool.Execute(Verbose)
    return False

def run_tool_climate_tools_15(GSL=None, GST=None, FROST=None, ATB=None, NIVAL_TEMP=None, TREE_TEMP=None, Verbose=2):
    '''
    Thermic Belt Classification
    ----------
    [climate_tools.15]\n
    Calculates the thermal belts based on mean temperature and length of the growing season.\n
    Arguments
    ----------
    - GSL [`input grid`] : Growing Season Length. Growing season length given as number of days of a year.
    - GST [`input grid`] : Mean Temperature. Mean temperature of the growing season.
    - FROST [`input grid`] : Frost occurrence. Frost occurrence as binary factor.
    - ATB [`output grid`] : Thermal Belts
    - NIVAL_TEMP [`floating point number`] : Minimum Temperature Nival. Default: 3.500000 Minimum Temperature for nival belt.
    - TREE_TEMP [`floating point number`] : Minimum Temperature Treeline. Default: 6.400000 Minimum Temperature for treeline.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('climate_tools', '15', 'Thermic Belt Classification')
    if Tool.is_Okay():
        Tool.Set_Input ('GSL', GSL)
        Tool.Set_Input ('GST', GST)
        Tool.Set_Input ('FROST', FROST)
        Tool.Set_Output('ATB', ATB)
        Tool.Set_Option('NIVAL_TEMP', NIVAL_TEMP)
        Tool.Set_Option('TREE_TEMP', TREE_TEMP)
        return Tool.Execute(Verbose)
    return False

def Snow_Cover(T=None, P=None, DAYS=None, MEAN=None, MAXIMUM=None, QUANTILE=None, QUANT_VAL=None, TIME=None, MONTH=None, Verbose=2):
    '''
    Snow Cover
    ----------
    [climate_tools.17]\n
    The 'Snow Cover' tool uses a simple model to estimate snow cover statistics from climate data. When temperature falls below zero any precipitation is accumulated as snow. Temperatures above zero will diminish accumulated snow successively until it is gone completely. Simulation is done on a daily basis. If you supply the tool with monthly averages, temperatures will be interpolated using a spline and precipitation will be split into separate events. The latter is done with respect to the monthly mean temperature, i.e. the higher the temperature the more concentrated are precipitation events and vice versa.\n
    Arguments
    ----------
    - T [`input grid list`] : Mean Temperature
    - P [`input grid list`] : Precipitation
    - DAYS [`output grid`] : Snow Cover Days
    - MEAN [`output grid`] : Average
    - MAXIMUM [`output grid`] : Maximum
    - QUANTILE [`output grid`] : Quantile
    - QUANT_VAL [`floating point number`] : Value. Minimum: 0.000000 Maximum: 100.000000 Default: 50.000000 Quantile specified as percentage.
    - TIME [`choice`] : Time. Available Choices: [0] Year [1] January - March [2] April - June [3] July - September [4] October - December [5] Single Month Default: 0
    - MONTH [`choice`] : Month. Available Choices: [0] January [1] February [2] March [3] April [4] May [5] June [6] July [7] August [8] September [9] October [10] November [11] December Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('climate_tools', '17', 'Snow Cover')
    if Tool.is_Okay():
        Tool.Set_Input ('T', T)
        Tool.Set_Input ('P', P)
        Tool.Set_Output('DAYS', DAYS)
        Tool.Set_Output('MEAN', MEAN)
        Tool.Set_Output('MAXIMUM', MAXIMUM)
        Tool.Set_Output('QUANTILE', QUANTILE)
        Tool.Set_Option('QUANT_VAL', QUANT_VAL)
        Tool.Set_Option('TIME', TIME)
        Tool.Set_Option('MONTH', MONTH)
        return Tool.Execute(Verbose)
    return False

def run_tool_climate_tools_17(T=None, P=None, DAYS=None, MEAN=None, MAXIMUM=None, QUANTILE=None, QUANT_VAL=None, TIME=None, MONTH=None, Verbose=2):
    '''
    Snow Cover
    ----------
    [climate_tools.17]\n
    The 'Snow Cover' tool uses a simple model to estimate snow cover statistics from climate data. When temperature falls below zero any precipitation is accumulated as snow. Temperatures above zero will diminish accumulated snow successively until it is gone completely. Simulation is done on a daily basis. If you supply the tool with monthly averages, temperatures will be interpolated using a spline and precipitation will be split into separate events. The latter is done with respect to the monthly mean temperature, i.e. the higher the temperature the more concentrated are precipitation events and vice versa.\n
    Arguments
    ----------
    - T [`input grid list`] : Mean Temperature
    - P [`input grid list`] : Precipitation
    - DAYS [`output grid`] : Snow Cover Days
    - MEAN [`output grid`] : Average
    - MAXIMUM [`output grid`] : Maximum
    - QUANTILE [`output grid`] : Quantile
    - QUANT_VAL [`floating point number`] : Value. Minimum: 0.000000 Maximum: 100.000000 Default: 50.000000 Quantile specified as percentage.
    - TIME [`choice`] : Time. Available Choices: [0] Year [1] January - March [2] April - June [3] July - September [4] October - December [5] Single Month Default: 0
    - MONTH [`choice`] : Month. Available Choices: [0] January [1] February [2] March [3] April [4] May [5] June [6] July [7] August [8] September [9] October [10] November [11] December Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('climate_tools', '17', 'Snow Cover')
    if Tool.is_Okay():
        Tool.Set_Input ('T', T)
        Tool.Set_Input ('P', P)
        Tool.Set_Output('DAYS', DAYS)
        Tool.Set_Output('MEAN', MEAN)
        Tool.Set_Output('MAXIMUM', MAXIMUM)
        Tool.Set_Output('QUANTILE', QUANTILE)
        Tool.Set_Option('QUANT_VAL', QUANT_VAL)
        Tool.Set_Option('TIME', TIME)
        Tool.Set_Option('MONTH', MONTH)
        return Tool.Execute(Verbose)
    return False

def Growing_Degree_Days(TMEAN=None, NGDD=None, TSUM=None, FIRST=None, LAST=None, TARGET=None, TBASE=None, TTARGET=None, Verbose=2):
    '''
    Growing Degree Days
    ----------
    [climate_tools.18]\n
    This tool calculates growing degree days from daily or from spline interpolated monthly observations for a given threshold. It also calculates the julian day at which a specific target temperature sum is reached.\n
    Arguments
    ----------
    - TMEAN [`input grid list`] : Mean Monthly Temperatures. Monthly (12) temperature observations.
    - NGDD [`output grid`] : Number of Days above Base Temperature. Number of days above base temperature.
    - TSUM [`output grid`] : Growing Degree Days. Integral of daily temperature above base temperature.
    - FIRST [`output grid`] : First Growing Degree Day. First growing degree day of the year (1-365).
    - LAST [`output grid`] : Last Growing Degree Day. Last growing degree day of the year (1-365).
    - TARGET [`output grid`] : Degree Sum Target Day. Day of the year at which temperature sum is reached (1-365).
    - TBASE [`floating point number`] : Base Temperature. Default: 10.000000 Base temperature in degree Celsius
    - TTARGET [`floating point number`] : Degree Sum. Default: 100.000000 Target temperature sum in degree Celsius.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('climate_tools', '18', 'Growing Degree Days')
    if Tool.is_Okay():
        Tool.Set_Input ('TMEAN', TMEAN)
        Tool.Set_Output('NGDD', NGDD)
        Tool.Set_Output('TSUM', TSUM)
        Tool.Set_Output('FIRST', FIRST)
        Tool.Set_Output('LAST', LAST)
        Tool.Set_Output('TARGET', TARGET)
        Tool.Set_Option('TBASE', TBASE)
        Tool.Set_Option('TTARGET', TTARGET)
        return Tool.Execute(Verbose)
    return False

def run_tool_climate_tools_18(TMEAN=None, NGDD=None, TSUM=None, FIRST=None, LAST=None, TARGET=None, TBASE=None, TTARGET=None, Verbose=2):
    '''
    Growing Degree Days
    ----------
    [climate_tools.18]\n
    This tool calculates growing degree days from daily or from spline interpolated monthly observations for a given threshold. It also calculates the julian day at which a specific target temperature sum is reached.\n
    Arguments
    ----------
    - TMEAN [`input grid list`] : Mean Monthly Temperatures. Monthly (12) temperature observations.
    - NGDD [`output grid`] : Number of Days above Base Temperature. Number of days above base temperature.
    - TSUM [`output grid`] : Growing Degree Days. Integral of daily temperature above base temperature.
    - FIRST [`output grid`] : First Growing Degree Day. First growing degree day of the year (1-365).
    - LAST [`output grid`] : Last Growing Degree Day. Last growing degree day of the year (1-365).
    - TARGET [`output grid`] : Degree Sum Target Day. Day of the year at which temperature sum is reached (1-365).
    - TBASE [`floating point number`] : Base Temperature. Default: 10.000000 Base temperature in degree Celsius
    - TTARGET [`floating point number`] : Degree Sum. Default: 100.000000 Target temperature sum in degree Celsius.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('climate_tools', '18', 'Growing Degree Days')
    if Tool.is_Okay():
        Tool.Set_Input ('TMEAN', TMEAN)
        Tool.Set_Output('NGDD', NGDD)
        Tool.Set_Output('TSUM', TSUM)
        Tool.Set_Output('FIRST', FIRST)
        Tool.Set_Output('LAST', LAST)
        Tool.Set_Output('TARGET', TARGET)
        Tool.Set_Option('TBASE', TBASE)
        Tool.Set_Option('TTARGET', TTARGET)
        return Tool.Execute(Verbose)
    return False

def Climate_Classification(T=None, P=None, CLASSES=None, METHOD=None, Verbose=2):
    '''
    Climate Classification
    ----------
    [climate_tools.19]\n
    This tool applies a climate classification scheme using monthly mean temperature and precipitation data. Currently implemented classification schemes are Koeppen-Geiger (1936), Thornthwaite (1931), and Troll-Paffen (1964). Because of some less precise definitions the Troll-Paffen scheme still needs some revisions.\n
    Arguments
    ----------
    - T [`input grid list`] : Temperature
    - P [`input grid list`] : Precipitation
    - CLASSES [`output grid`] : Climate Classification
    - METHOD [`choice`] : Classification. Available Choices: [0] Koeppen-Geiger [1] Koeppen-Geiger without As/Aw differentiation [2] Koeppen-Geiger after Peel et al. (2007) [3] Wissmann (1939) [4] Thornthwaite (1931) [5] Troll-Paffen Default: 1

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('climate_tools', '19', 'Climate Classification')
    if Tool.is_Okay():
        Tool.Set_Input ('T', T)
        Tool.Set_Input ('P', P)
        Tool.Set_Output('CLASSES', CLASSES)
        Tool.Set_Option('METHOD', METHOD)
        return Tool.Execute(Verbose)
    return False

def run_tool_climate_tools_19(T=None, P=None, CLASSES=None, METHOD=None, Verbose=2):
    '''
    Climate Classification
    ----------
    [climate_tools.19]\n
    This tool applies a climate classification scheme using monthly mean temperature and precipitation data. Currently implemented classification schemes are Koeppen-Geiger (1936), Thornthwaite (1931), and Troll-Paffen (1964). Because of some less precise definitions the Troll-Paffen scheme still needs some revisions.\n
    Arguments
    ----------
    - T [`input grid list`] : Temperature
    - P [`input grid list`] : Precipitation
    - CLASSES [`output grid`] : Climate Classification
    - METHOD [`choice`] : Classification. Available Choices: [0] Koeppen-Geiger [1] Koeppen-Geiger without As/Aw differentiation [2] Koeppen-Geiger after Peel et al. (2007) [3] Wissmann (1939) [4] Thornthwaite (1931) [5] Troll-Paffen Default: 1

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('climate_tools', '19', 'Climate Classification')
    if Tool.is_Okay():
        Tool.Set_Input ('T', T)
        Tool.Set_Input ('P', P)
        Tool.Set_Output('CLASSES', CLASSES)
        Tool.Set_Option('METHOD', METHOD)
        return Tool.Execute(Verbose)
    return False

def Soil_Water_Balance_Annual(T=None, TMIN=None, TMAX=None, P=None, SWC=None, SNOW=None, ETP=None, SW_0=None, SW_1=None, SWC_DEFAULT=None, SWC_SURFACE=None, SW1_RESIST=None, LAT_DEF=None, Verbose=2):
    '''
    Soil Water Balance (Annual)
    ----------
    [climate_tools.20]\n
    This tool calculates the water balance for the selected position on a daily basis. Needed input is monthly data of mean, minimum, and maximum temperature as well as precipitation.\n
    Arguments
    ----------
    - T [`input grid list`] : Mean Temperature
    - TMIN [`input grid list`] : Minimum Temperature
    - TMAX [`input grid list`] : Maximum Temperature
    - P [`input grid list`] : Precipitation
    - SWC [`optional input grid`] : Soil Water Capacity of Profile. Total soil water capacity (mm H2O).
    - SNOW [`output grid collection`] : Snow Depth
    - ETP [`output grid collection`] : Evapotranspiration
    - SW_0 [`output grid collection`] : Soil Water (Upper Layer)
    - SW_1 [`output grid collection`] : Soil Water (Lower Layer)
    - SWC_DEFAULT [`floating point number`] : Default. Minimum: 0.000000 Default: 220.000000 default value if no grid has been selected
    - SWC_SURFACE [`floating point number`] : Top Soil Water Capacity. Minimum: 0.000000 Default: 10.000000
    - SW1_RESIST [`floating point number`] : Transpiration Resistance. Minimum: 0.010000 Default: 1.000000
    - LAT_DEF [`floating point number`] : Default Latitude. Minimum: -90.000000 Maximum: 90.000000 Default: 0.000000

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('climate_tools', '20', 'Soil Water Balance (Annual)')
    if Tool.is_Okay():
        Tool.Set_Input ('T', T)
        Tool.Set_Input ('TMIN', TMIN)
        Tool.Set_Input ('TMAX', TMAX)
        Tool.Set_Input ('P', P)
        Tool.Set_Input ('SWC', SWC)
        Tool.Set_Output('SNOW', SNOW)
        Tool.Set_Output('ETP', ETP)
        Tool.Set_Output('SW_0', SW_0)
        Tool.Set_Output('SW_1', SW_1)
        Tool.Set_Option('SWC_DEFAULT', SWC_DEFAULT)
        Tool.Set_Option('SWC_SURFACE', SWC_SURFACE)
        Tool.Set_Option('SW1_RESIST', SW1_RESIST)
        Tool.Set_Option('LAT_DEF', LAT_DEF)
        return Tool.Execute(Verbose)
    return False

def run_tool_climate_tools_20(T=None, TMIN=None, TMAX=None, P=None, SWC=None, SNOW=None, ETP=None, SW_0=None, SW_1=None, SWC_DEFAULT=None, SWC_SURFACE=None, SW1_RESIST=None, LAT_DEF=None, Verbose=2):
    '''
    Soil Water Balance (Annual)
    ----------
    [climate_tools.20]\n
    This tool calculates the water balance for the selected position on a daily basis. Needed input is monthly data of mean, minimum, and maximum temperature as well as precipitation.\n
    Arguments
    ----------
    - T [`input grid list`] : Mean Temperature
    - TMIN [`input grid list`] : Minimum Temperature
    - TMAX [`input grid list`] : Maximum Temperature
    - P [`input grid list`] : Precipitation
    - SWC [`optional input grid`] : Soil Water Capacity of Profile. Total soil water capacity (mm H2O).
    - SNOW [`output grid collection`] : Snow Depth
    - ETP [`output grid collection`] : Evapotranspiration
    - SW_0 [`output grid collection`] : Soil Water (Upper Layer)
    - SW_1 [`output grid collection`] : Soil Water (Lower Layer)
    - SWC_DEFAULT [`floating point number`] : Default. Minimum: 0.000000 Default: 220.000000 default value if no grid has been selected
    - SWC_SURFACE [`floating point number`] : Top Soil Water Capacity. Minimum: 0.000000 Default: 10.000000
    - SW1_RESIST [`floating point number`] : Transpiration Resistance. Minimum: 0.010000 Default: 1.000000
    - LAT_DEF [`floating point number`] : Default Latitude. Minimum: -90.000000 Maximum: 90.000000 Default: 0.000000

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('climate_tools', '20', 'Soil Water Balance (Annual)')
    if Tool.is_Okay():
        Tool.Set_Input ('T', T)
        Tool.Set_Input ('TMIN', TMIN)
        Tool.Set_Input ('TMAX', TMAX)
        Tool.Set_Input ('P', P)
        Tool.Set_Input ('SWC', SWC)
        Tool.Set_Output('SNOW', SNOW)
        Tool.Set_Output('ETP', ETP)
        Tool.Set_Output('SW_0', SW_0)
        Tool.Set_Output('SW_1', SW_1)
        Tool.Set_Option('SWC_DEFAULT', SWC_DEFAULT)
        Tool.Set_Option('SWC_SURFACE', SWC_SURFACE)
        Tool.Set_Option('SW1_RESIST', SW1_RESIST)
        Tool.Set_Option('LAT_DEF', LAT_DEF)
        return Tool.Execute(Verbose)
    return False

def PhenIps_Table(WEATHER=None, PHENOLOGY=None, SUMMARY=None, ATMEAN=None, ATMAX=None, SIREL=None, LATITUDE=None, LIMIT=None, DTOPTIMUM=None, DTMINIMUM=None, FAMINIMUM=None, DAYLENGTH=None, DDMINIMUM=None, DDTOTAL=None, RISK_DAYMAX=None, RISK_DECAY=None, YD_BEGIN=None, YD_END_ONSET=None, YD_END=None, Verbose=2):
    '''
    PhenIps (Table)
    ----------
    [climate_tools.21]\n
    A comprehensive phenology model of Ips typographus (L.) (Col., Scolytinae) as a tool for hazard rating of bark beetle infestation.\n
    Arguments
    ----------
    - WEATHER [`input table`] : Weather Data
    - PHENOLOGY [`output table`] : Phenology
    - SUMMARY [`output table`] : Summary
    - ATMEAN [`table field`] : Mean Temperature. Â°C
    - ATMAX [`table field`] : Maximum Temperature. Â°C
    - SIREL [`table field`] : Solar Irradiation. Wh/mÂ²
    - LATITUDE [`floating point number`] : Latitude. Minimum: -90.000000 Maximum: 90.000000 Default: 50.000000
    - LIMIT [`boolean`] : Limit. Default: 1 Limits state output to a maximum value of 1.
    - DTOPTIMUM [`floating point number`] : Developmental Optimum Temperature. Default: 30.400000 Degree Celsius
    - DTMINIMUM [`floating point number`] : Developmental Minimum Temperature. Default: 8.300000 Degree Celsius
    - FAMINIMUM [`floating point number`] : Minimum Temperature for Flight Activity. Default: 16.500000 Degree Celsius
    - DAYLENGTH [`floating point number`] : Minimum Day Length for Flight Activity. Minimum: 0.000000 Maximum: 24.000000 Default: 14.500000 Hours
    - DDMINIMUM [`floating point number`] : Minimum Thermal Sum for Infestation. Minimum: 0.000000 Default: 140.000000 Degree Days
    - DDTOTAL [`floating point number`] : Thermal Sum for Total Development. Minimum: 0.000000 Default: 557.000000 Degree Days
    - RISK_DAYMAX [`floating point number`] : Day of Maximum Risk after Onset. Minimum: 0.000000 Default: 5.000000 Days
    - RISK_DECAY [`floating point number`] : Decay of Risk after Maximum. Minimum: 1.000000 Default: 10.000000 Days
    - YD_BEGIN [`date`] : Begin of Parental Development. Default: 2025-04-01
    - YD_END_ONSET [`date`] : End of Breeding. Default: 2025-08-31
    - YD_END [`date`] : End of Development. Default: 2025-10-31

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('climate_tools', '21', 'PhenIps (Table)')
    if Tool.is_Okay():
        Tool.Set_Input ('WEATHER', WEATHER)
        Tool.Set_Output('PHENOLOGY', PHENOLOGY)
        Tool.Set_Output('SUMMARY', SUMMARY)
        Tool.Set_Option('ATmean', ATMEAN)
        Tool.Set_Option('ATmax', ATMAX)
        Tool.Set_Option('SIrel', SIREL)
        Tool.Set_Option('LATITUDE', LATITUDE)
        Tool.Set_Option('LIMIT', LIMIT)
        Tool.Set_Option('DToptimum', DTOPTIMUM)
        Tool.Set_Option('DTminimum', DTMINIMUM)
        Tool.Set_Option('FAminimum', FAMINIMUM)
        Tool.Set_Option('DayLength', DAYLENGTH)
        Tool.Set_Option('DDminimum', DDMINIMUM)
        Tool.Set_Option('DDtotal', DDTOTAL)
        Tool.Set_Option('Risk_DayMax', RISK_DAYMAX)
        Tool.Set_Option('Risk_Decay', RISK_DECAY)
        Tool.Set_Option('YD_Begin', YD_BEGIN)
        Tool.Set_Option('YD_End_Onset', YD_END_ONSET)
        Tool.Set_Option('YD_End', YD_END)
        return Tool.Execute(Verbose)
    return False

def run_tool_climate_tools_21(WEATHER=None, PHENOLOGY=None, SUMMARY=None, ATMEAN=None, ATMAX=None, SIREL=None, LATITUDE=None, LIMIT=None, DTOPTIMUM=None, DTMINIMUM=None, FAMINIMUM=None, DAYLENGTH=None, DDMINIMUM=None, DDTOTAL=None, RISK_DAYMAX=None, RISK_DECAY=None, YD_BEGIN=None, YD_END_ONSET=None, YD_END=None, Verbose=2):
    '''
    PhenIps (Table)
    ----------
    [climate_tools.21]\n
    A comprehensive phenology model of Ips typographus (L.) (Col., Scolytinae) as a tool for hazard rating of bark beetle infestation.\n
    Arguments
    ----------
    - WEATHER [`input table`] : Weather Data
    - PHENOLOGY [`output table`] : Phenology
    - SUMMARY [`output table`] : Summary
    - ATMEAN [`table field`] : Mean Temperature. Â°C
    - ATMAX [`table field`] : Maximum Temperature. Â°C
    - SIREL [`table field`] : Solar Irradiation. Wh/mÂ²
    - LATITUDE [`floating point number`] : Latitude. Minimum: -90.000000 Maximum: 90.000000 Default: 50.000000
    - LIMIT [`boolean`] : Limit. Default: 1 Limits state output to a maximum value of 1.
    - DTOPTIMUM [`floating point number`] : Developmental Optimum Temperature. Default: 30.400000 Degree Celsius
    - DTMINIMUM [`floating point number`] : Developmental Minimum Temperature. Default: 8.300000 Degree Celsius
    - FAMINIMUM [`floating point number`] : Minimum Temperature for Flight Activity. Default: 16.500000 Degree Celsius
    - DAYLENGTH [`floating point number`] : Minimum Day Length for Flight Activity. Minimum: 0.000000 Maximum: 24.000000 Default: 14.500000 Hours
    - DDMINIMUM [`floating point number`] : Minimum Thermal Sum for Infestation. Minimum: 0.000000 Default: 140.000000 Degree Days
    - DDTOTAL [`floating point number`] : Thermal Sum for Total Development. Minimum: 0.000000 Default: 557.000000 Degree Days
    - RISK_DAYMAX [`floating point number`] : Day of Maximum Risk after Onset. Minimum: 0.000000 Default: 5.000000 Days
    - RISK_DECAY [`floating point number`] : Decay of Risk after Maximum. Minimum: 1.000000 Default: 10.000000 Days
    - YD_BEGIN [`date`] : Begin of Parental Development. Default: 2025-04-01
    - YD_END_ONSET [`date`] : End of Breeding. Default: 2025-08-31
    - YD_END [`date`] : End of Development. Default: 2025-10-31

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('climate_tools', '21', 'PhenIps (Table)')
    if Tool.is_Okay():
        Tool.Set_Input ('WEATHER', WEATHER)
        Tool.Set_Output('PHENOLOGY', PHENOLOGY)
        Tool.Set_Output('SUMMARY', SUMMARY)
        Tool.Set_Option('ATmean', ATMEAN)
        Tool.Set_Option('ATmax', ATMAX)
        Tool.Set_Option('SIrel', SIREL)
        Tool.Set_Option('LATITUDE', LATITUDE)
        Tool.Set_Option('LIMIT', LIMIT)
        Tool.Set_Option('DToptimum', DTOPTIMUM)
        Tool.Set_Option('DTminimum', DTMINIMUM)
        Tool.Set_Option('FAminimum', FAMINIMUM)
        Tool.Set_Option('DayLength', DAYLENGTH)
        Tool.Set_Option('DDminimum', DDMINIMUM)
        Tool.Set_Option('DDtotal', DDTOTAL)
        Tool.Set_Option('Risk_DayMax', RISK_DAYMAX)
        Tool.Set_Option('Risk_Decay', RISK_DECAY)
        Tool.Set_Option('YD_Begin', YD_BEGIN)
        Tool.Set_Option('YD_End_Onset', YD_END_ONSET)
        Tool.Set_Option('YD_End', YD_END)
        return Tool.Execute(Verbose)
    return False

def PhenIps_Grids_Annual(ATMEAN=None, ATMAX=None, SIREL=None, LAT_GRID=None, GENERATIONS=None, ONSET=None, ONSET_FILIAL_1=None, ONSET_SISTER_1=None, ONSET_FILIAL_2=None, ONSET_SISTER_2=None, ONSET_FILIAL_3=None, ONSET_SISTER_3=None, BTSUM_FILIAL_1=None, BTSUM_SISTER_1=None, BTSUM_FILIAL_2=None, BTSUM_SISTER_2=None, BTSUM_FILIAL_3=None, BTSUM_SISTER_3=None, LAT_CONST=None, DTOPTIMUM=None, DTMINIMUM=None, FAMINIMUM=None, DAYLENGTH=None, DDMINIMUM=None, DDTOTAL=None, RISK_DAYMAX=None, RISK_DECAY=None, YD_BEGIN=None, YD_END_ONSET=None, YD_END=None, Verbose=2):
    '''
    PhenIps (Grids, Annual)
    ----------
    [climate_tools.22]\n
    A comprehensive phenology model of Ips typographus (L.) (Col., Scolytinae) as a tool for hazard rating of bark beetle infestation.\n
    Arguments
    ----------
    - ATMEAN [`input grid list`] : Mean Temperature. Â°C
    - ATMAX [`input grid list`] : Maximum Temperature. Â°C
    - SIREL [`input grid list`] : Solar Irradiation. Wh/mÂ²
    - LAT_GRID [`optional input grid`] : Latitude
    - GENERATIONS [`output grid`] : Potential Number of Generations
    - ONSET [`output grid`] : Onset Day of Infestation
    - ONSET_FILIAL_1 [`output grid`] : Onset Day, 1. Filial Generation
    - ONSET_SISTER_1 [`output grid`] : Onset Day, 1. Sister Generation
    - ONSET_FILIAL_2 [`output grid`] : Onset Day, 2. Filial Generation
    - ONSET_SISTER_2 [`output grid`] : Onset Day, 2. Sister Generation
    - ONSET_FILIAL_3 [`output grid`] : Onset Day, 3. Filial Generation
    - ONSET_SISTER_3 [`output grid`] : Onset Day, 3. Sister Generation
    - BTSUM_FILIAL_1 [`output grid`] : State, 1. Filial Generation
    - BTSUM_SISTER_1 [`output grid`] : State, 1. Sister Generation
    - BTSUM_FILIAL_2 [`output grid`] : State, 2. Filial Generation
    - BTSUM_SISTER_2 [`output grid`] : State, 2. Sister Generation
    - BTSUM_FILIAL_3 [`output grid`] : State, 3. Filial Generation
    - BTSUM_SISTER_3 [`output grid`] : State, 3. Sister Generation
    - LAT_CONST [`floating point number`] : Latitude. Minimum: -90.000000 Maximum: 90.000000 Default: 50.000000
    - DTOPTIMUM [`floating point number`] : Developmental Optimum Temperature. Default: 30.400000 Degree Celsius
    - DTMINIMUM [`floating point number`] : Developmental Minimum Temperature. Default: 8.300000 Degree Celsius
    - FAMINIMUM [`floating point number`] : Minimum Temperature for Flight Activity. Default: 16.500000 Degree Celsius
    - DAYLENGTH [`floating point number`] : Minimum Day Length for Flight Activity. Minimum: 0.000000 Maximum: 24.000000 Default: 14.500000 Hours
    - DDMINIMUM [`floating point number`] : Minimum Thermal Sum for Infestation. Minimum: 0.000000 Default: 140.000000 Degree Days
    - DDTOTAL [`floating point number`] : Thermal Sum for Total Development. Minimum: 0.000000 Default: 557.000000 Degree Days
    - RISK_DAYMAX [`floating point number`] : Day of Maximum Risk after Onset. Minimum: 0.000000 Default: 5.000000 Days
    - RISK_DECAY [`floating point number`] : Decay of Risk after Maximum. Minimum: 1.000000 Default: 10.000000 Days
    - YD_BEGIN [`date`] : Begin of Parental Development. Default: 2025-04-01
    - YD_END_ONSET [`date`] : End of Breeding. Default: 2025-08-31
    - YD_END [`date`] : End of Development. Default: 2025-10-31

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('climate_tools', '22', 'PhenIps (Grids, Annual)')
    if Tool.is_Okay():
        Tool.Set_Input ('ATmean', ATMEAN)
        Tool.Set_Input ('ATmax', ATMAX)
        Tool.Set_Input ('SIrel', SIREL)
        Tool.Set_Input ('LAT_GRID', LAT_GRID)
        Tool.Set_Output('GENERATIONS', GENERATIONS)
        Tool.Set_Output('ONSET', ONSET)
        Tool.Set_Output('ONSET_FILIAL_1', ONSET_FILIAL_1)
        Tool.Set_Output('ONSET_SISTER_1', ONSET_SISTER_1)
        Tool.Set_Output('ONSET_FILIAL_2', ONSET_FILIAL_2)
        Tool.Set_Output('ONSET_SISTER_2', ONSET_SISTER_2)
        Tool.Set_Output('ONSET_FILIAL_3', ONSET_FILIAL_3)
        Tool.Set_Output('ONSET_SISTER_3', ONSET_SISTER_3)
        Tool.Set_Output('BTSUM_FILIAL_1', BTSUM_FILIAL_1)
        Tool.Set_Output('BTSUM_SISTER_1', BTSUM_SISTER_1)
        Tool.Set_Output('BTSUM_FILIAL_2', BTSUM_FILIAL_2)
        Tool.Set_Output('BTSUM_SISTER_2', BTSUM_SISTER_2)
        Tool.Set_Output('BTSUM_FILIAL_3', BTSUM_FILIAL_3)
        Tool.Set_Output('BTSUM_SISTER_3', BTSUM_SISTER_3)
        Tool.Set_Option('LAT_CONST', LAT_CONST)
        Tool.Set_Option('DToptimum', DTOPTIMUM)
        Tool.Set_Option('DTminimum', DTMINIMUM)
        Tool.Set_Option('FAminimum', FAMINIMUM)
        Tool.Set_Option('DayLength', DAYLENGTH)
        Tool.Set_Option('DDminimum', DDMINIMUM)
        Tool.Set_Option('DDtotal', DDTOTAL)
        Tool.Set_Option('Risk_DayMax', RISK_DAYMAX)
        Tool.Set_Option('Risk_Decay', RISK_DECAY)
        Tool.Set_Option('YD_Begin', YD_BEGIN)
        Tool.Set_Option('YD_End_Onset', YD_END_ONSET)
        Tool.Set_Option('YD_End', YD_END)
        return Tool.Execute(Verbose)
    return False

def run_tool_climate_tools_22(ATMEAN=None, ATMAX=None, SIREL=None, LAT_GRID=None, GENERATIONS=None, ONSET=None, ONSET_FILIAL_1=None, ONSET_SISTER_1=None, ONSET_FILIAL_2=None, ONSET_SISTER_2=None, ONSET_FILIAL_3=None, ONSET_SISTER_3=None, BTSUM_FILIAL_1=None, BTSUM_SISTER_1=None, BTSUM_FILIAL_2=None, BTSUM_SISTER_2=None, BTSUM_FILIAL_3=None, BTSUM_SISTER_3=None, LAT_CONST=None, DTOPTIMUM=None, DTMINIMUM=None, FAMINIMUM=None, DAYLENGTH=None, DDMINIMUM=None, DDTOTAL=None, RISK_DAYMAX=None, RISK_DECAY=None, YD_BEGIN=None, YD_END_ONSET=None, YD_END=None, Verbose=2):
    '''
    PhenIps (Grids, Annual)
    ----------
    [climate_tools.22]\n
    A comprehensive phenology model of Ips typographus (L.) (Col., Scolytinae) as a tool for hazard rating of bark beetle infestation.\n
    Arguments
    ----------
    - ATMEAN [`input grid list`] : Mean Temperature. Â°C
    - ATMAX [`input grid list`] : Maximum Temperature. Â°C
    - SIREL [`input grid list`] : Solar Irradiation. Wh/mÂ²
    - LAT_GRID [`optional input grid`] : Latitude
    - GENERATIONS [`output grid`] : Potential Number of Generations
    - ONSET [`output grid`] : Onset Day of Infestation
    - ONSET_FILIAL_1 [`output grid`] : Onset Day, 1. Filial Generation
    - ONSET_SISTER_1 [`output grid`] : Onset Day, 1. Sister Generation
    - ONSET_FILIAL_2 [`output grid`] : Onset Day, 2. Filial Generation
    - ONSET_SISTER_2 [`output grid`] : Onset Day, 2. Sister Generation
    - ONSET_FILIAL_3 [`output grid`] : Onset Day, 3. Filial Generation
    - ONSET_SISTER_3 [`output grid`] : Onset Day, 3. Sister Generation
    - BTSUM_FILIAL_1 [`output grid`] : State, 1. Filial Generation
    - BTSUM_SISTER_1 [`output grid`] : State, 1. Sister Generation
    - BTSUM_FILIAL_2 [`output grid`] : State, 2. Filial Generation
    - BTSUM_SISTER_2 [`output grid`] : State, 2. Sister Generation
    - BTSUM_FILIAL_3 [`output grid`] : State, 3. Filial Generation
    - BTSUM_SISTER_3 [`output grid`] : State, 3. Sister Generation
    - LAT_CONST [`floating point number`] : Latitude. Minimum: -90.000000 Maximum: 90.000000 Default: 50.000000
    - DTOPTIMUM [`floating point number`] : Developmental Optimum Temperature. Default: 30.400000 Degree Celsius
    - DTMINIMUM [`floating point number`] : Developmental Minimum Temperature. Default: 8.300000 Degree Celsius
    - FAMINIMUM [`floating point number`] : Minimum Temperature for Flight Activity. Default: 16.500000 Degree Celsius
    - DAYLENGTH [`floating point number`] : Minimum Day Length for Flight Activity. Minimum: 0.000000 Maximum: 24.000000 Default: 14.500000 Hours
    - DDMINIMUM [`floating point number`] : Minimum Thermal Sum for Infestation. Minimum: 0.000000 Default: 140.000000 Degree Days
    - DDTOTAL [`floating point number`] : Thermal Sum for Total Development. Minimum: 0.000000 Default: 557.000000 Degree Days
    - RISK_DAYMAX [`floating point number`] : Day of Maximum Risk after Onset. Minimum: 0.000000 Default: 5.000000 Days
    - RISK_DECAY [`floating point number`] : Decay of Risk after Maximum. Minimum: 1.000000 Default: 10.000000 Days
    - YD_BEGIN [`date`] : Begin of Parental Development. Default: 2025-04-01
    - YD_END_ONSET [`date`] : End of Breeding. Default: 2025-08-31
    - YD_END [`date`] : End of Development. Default: 2025-10-31

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('climate_tools', '22', 'PhenIps (Grids, Annual)')
    if Tool.is_Okay():
        Tool.Set_Input ('ATmean', ATMEAN)
        Tool.Set_Input ('ATmax', ATMAX)
        Tool.Set_Input ('SIrel', SIREL)
        Tool.Set_Input ('LAT_GRID', LAT_GRID)
        Tool.Set_Output('GENERATIONS', GENERATIONS)
        Tool.Set_Output('ONSET', ONSET)
        Tool.Set_Output('ONSET_FILIAL_1', ONSET_FILIAL_1)
        Tool.Set_Output('ONSET_SISTER_1', ONSET_SISTER_1)
        Tool.Set_Output('ONSET_FILIAL_2', ONSET_FILIAL_2)
        Tool.Set_Output('ONSET_SISTER_2', ONSET_SISTER_2)
        Tool.Set_Output('ONSET_FILIAL_3', ONSET_FILIAL_3)
        Tool.Set_Output('ONSET_SISTER_3', ONSET_SISTER_3)
        Tool.Set_Output('BTSUM_FILIAL_1', BTSUM_FILIAL_1)
        Tool.Set_Output('BTSUM_SISTER_1', BTSUM_SISTER_1)
        Tool.Set_Output('BTSUM_FILIAL_2', BTSUM_FILIAL_2)
        Tool.Set_Output('BTSUM_SISTER_2', BTSUM_SISTER_2)
        Tool.Set_Output('BTSUM_FILIAL_3', BTSUM_FILIAL_3)
        Tool.Set_Output('BTSUM_SISTER_3', BTSUM_SISTER_3)
        Tool.Set_Option('LAT_CONST', LAT_CONST)
        Tool.Set_Option('DToptimum', DTOPTIMUM)
        Tool.Set_Option('DTminimum', DTMINIMUM)
        Tool.Set_Option('FAminimum', FAMINIMUM)
        Tool.Set_Option('DayLength', DAYLENGTH)
        Tool.Set_Option('DDminimum', DDMINIMUM)
        Tool.Set_Option('DDtotal', DDTOTAL)
        Tool.Set_Option('Risk_DayMax', RISK_DAYMAX)
        Tool.Set_Option('Risk_Decay', RISK_DECAY)
        Tool.Set_Option('YD_Begin', YD_BEGIN)
        Tool.Set_Option('YD_End_Onset', YD_END_ONSET)
        Tool.Set_Option('YD_End', YD_END)
        return Tool.Execute(Verbose)
    return False

def PhenIps_Grids_Days(ATMEAN=None, ATMAX=None, SIREL=None, LAT_GRID=None, GENERATIONS=None, ONSET=None, ONSET_FILIAL_1=None, ONSET_SISTER_1=None, ONSET_FILIAL_2=None, ONSET_SISTER_2=None, ONSET_FILIAL_3=None, ONSET_SISTER_3=None, BTSUM_FILIAL_1=None, BTSUM_SISTER_1=None, BTSUM_FILIAL_2=None, BTSUM_SISTER_2=None, BTSUM_FILIAL_3=None, BTSUM_SISTER_3=None, ATSUM_EFF=None, RISK=None, LAT_CONST=None, DTOPTIMUM=None, DTMINIMUM=None, FAMINIMUM=None, DAYLENGTH=None, DDMINIMUM=None, DDTOTAL=None, RISK_DAYMAX=None, RISK_DECAY=None, YD_BEGIN=None, YD_END_ONSET=None, YD_END=None, RESET=None, DAY=None, Verbose=2):
    '''
    PhenIps (Grids, Days)
    ----------
    [climate_tools.23]\n
    A comprehensive phenology model of Ips typographus (L.) (Col., Scolytinae) as a tool for hazard rating of bark beetle infestation.\n
    Arguments
    ----------
    - ATMEAN [`input grid list`] : Mean Temperature. Â°C
    - ATMAX [`input grid list`] : Maximum Temperature. Â°C
    - SIREL [`input grid list`] : Solar Irradiation. Wh/mÂ²
    - LAT_GRID [`optional input grid`] : Latitude
    - GENERATIONS [`output grid`] : Potential Number of Generations
    - ONSET [`output grid`] : Onset Day of Infestation
    - ONSET_FILIAL_1 [`output grid`] : Onset Day, 1. Filial Generation
    - ONSET_SISTER_1 [`output grid`] : Onset Day, 1. Sister Generation
    - ONSET_FILIAL_2 [`output grid`] : Onset Day, 2. Filial Generation
    - ONSET_SISTER_2 [`output grid`] : Onset Day, 2. Sister Generation
    - ONSET_FILIAL_3 [`output grid`] : Onset Day, 3. Filial Generation
    - ONSET_SISTER_3 [`output grid`] : Onset Day, 3. Sister Generation
    - BTSUM_FILIAL_1 [`output grid`] : State, 1. Filial Generation
    - BTSUM_SISTER_1 [`output grid`] : State, 1. Sister Generation
    - BTSUM_FILIAL_2 [`output grid`] : State, 2. Filial Generation
    - BTSUM_SISTER_2 [`output grid`] : State, 2. Sister Generation
    - BTSUM_FILIAL_3 [`output grid`] : State, 3. Filial Generation
    - BTSUM_SISTER_3 [`output grid`] : State, 3. Sister Generation
    - ATSUM_EFF [`output grid`] : Air Temperature Sum
    - RISK [`output grid`] : Risk
    - LAT_CONST [`floating point number`] : Latitude. Minimum: -90.000000 Maximum: 90.000000 Default: 50.000000
    - DTOPTIMUM [`floating point number`] : Developmental Optimum Temperature. Default: 30.400000 Degree Celsius
    - DTMINIMUM [`floating point number`] : Developmental Minimum Temperature. Default: 8.300000 Degree Celsius
    - FAMINIMUM [`floating point number`] : Minimum Temperature for Flight Activity. Default: 16.500000 Degree Celsius
    - DAYLENGTH [`floating point number`] : Minimum Day Length for Flight Activity. Minimum: 0.000000 Maximum: 24.000000 Default: 14.500000 Hours
    - DDMINIMUM [`floating point number`] : Minimum Thermal Sum for Infestation. Minimum: 0.000000 Default: 140.000000 Degree Days
    - DDTOTAL [`floating point number`] : Thermal Sum for Total Development. Minimum: 0.000000 Default: 557.000000 Degree Days
    - RISK_DAYMAX [`floating point number`] : Day of Maximum Risk after Onset. Minimum: 0.000000 Default: 5.000000 Days
    - RISK_DECAY [`floating point number`] : Decay of Risk after Maximum. Minimum: 1.000000 Default: 10.000000 Days
    - YD_BEGIN [`date`] : Begin of Parental Development. Default: 2025-04-01
    - YD_END_ONSET [`date`] : End of Breeding. Default: 2025-08-31
    - YD_END [`date`] : End of Development. Default: 2025-10-31
    - RESET [`boolean`] : Reset. Default: 1
    - DAY [`date`] : Start Day. Default: 2025-01-20

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('climate_tools', '23', 'PhenIps (Grids, Days)')
    if Tool.is_Okay():
        Tool.Set_Input ('ATmean', ATMEAN)
        Tool.Set_Input ('ATmax', ATMAX)
        Tool.Set_Input ('SIrel', SIREL)
        Tool.Set_Input ('LAT_GRID', LAT_GRID)
        Tool.Set_Output('GENERATIONS', GENERATIONS)
        Tool.Set_Output('ONSET', ONSET)
        Tool.Set_Output('ONSET_FILIAL_1', ONSET_FILIAL_1)
        Tool.Set_Output('ONSET_SISTER_1', ONSET_SISTER_1)
        Tool.Set_Output('ONSET_FILIAL_2', ONSET_FILIAL_2)
        Tool.Set_Output('ONSET_SISTER_2', ONSET_SISTER_2)
        Tool.Set_Output('ONSET_FILIAL_3', ONSET_FILIAL_3)
        Tool.Set_Output('ONSET_SISTER_3', ONSET_SISTER_3)
        Tool.Set_Output('BTSUM_FILIAL_1', BTSUM_FILIAL_1)
        Tool.Set_Output('BTSUM_SISTER_1', BTSUM_SISTER_1)
        Tool.Set_Output('BTSUM_FILIAL_2', BTSUM_FILIAL_2)
        Tool.Set_Output('BTSUM_SISTER_2', BTSUM_SISTER_2)
        Tool.Set_Output('BTSUM_FILIAL_3', BTSUM_FILIAL_3)
        Tool.Set_Output('BTSUM_SISTER_3', BTSUM_SISTER_3)
        Tool.Set_Output('ATSUM_EFF', ATSUM_EFF)
        Tool.Set_Output('RISK', RISK)
        Tool.Set_Option('LAT_CONST', LAT_CONST)
        Tool.Set_Option('DToptimum', DTOPTIMUM)
        Tool.Set_Option('DTminimum', DTMINIMUM)
        Tool.Set_Option('FAminimum', FAMINIMUM)
        Tool.Set_Option('DayLength', DAYLENGTH)
        Tool.Set_Option('DDminimum', DDMINIMUM)
        Tool.Set_Option('DDtotal', DDTOTAL)
        Tool.Set_Option('Risk_DayMax', RISK_DAYMAX)
        Tool.Set_Option('Risk_Decay', RISK_DECAY)
        Tool.Set_Option('YD_Begin', YD_BEGIN)
        Tool.Set_Option('YD_End_Onset', YD_END_ONSET)
        Tool.Set_Option('YD_End', YD_END)
        Tool.Set_Option('RESET', RESET)
        Tool.Set_Option('DAY', DAY)
        return Tool.Execute(Verbose)
    return False

def run_tool_climate_tools_23(ATMEAN=None, ATMAX=None, SIREL=None, LAT_GRID=None, GENERATIONS=None, ONSET=None, ONSET_FILIAL_1=None, ONSET_SISTER_1=None, ONSET_FILIAL_2=None, ONSET_SISTER_2=None, ONSET_FILIAL_3=None, ONSET_SISTER_3=None, BTSUM_FILIAL_1=None, BTSUM_SISTER_1=None, BTSUM_FILIAL_2=None, BTSUM_SISTER_2=None, BTSUM_FILIAL_3=None, BTSUM_SISTER_3=None, ATSUM_EFF=None, RISK=None, LAT_CONST=None, DTOPTIMUM=None, DTMINIMUM=None, FAMINIMUM=None, DAYLENGTH=None, DDMINIMUM=None, DDTOTAL=None, RISK_DAYMAX=None, RISK_DECAY=None, YD_BEGIN=None, YD_END_ONSET=None, YD_END=None, RESET=None, DAY=None, Verbose=2):
    '''
    PhenIps (Grids, Days)
    ----------
    [climate_tools.23]\n
    A comprehensive phenology model of Ips typographus (L.) (Col., Scolytinae) as a tool for hazard rating of bark beetle infestation.\n
    Arguments
    ----------
    - ATMEAN [`input grid list`] : Mean Temperature. Â°C
    - ATMAX [`input grid list`] : Maximum Temperature. Â°C
    - SIREL [`input grid list`] : Solar Irradiation. Wh/mÂ²
    - LAT_GRID [`optional input grid`] : Latitude
    - GENERATIONS [`output grid`] : Potential Number of Generations
    - ONSET [`output grid`] : Onset Day of Infestation
    - ONSET_FILIAL_1 [`output grid`] : Onset Day, 1. Filial Generation
    - ONSET_SISTER_1 [`output grid`] : Onset Day, 1. Sister Generation
    - ONSET_FILIAL_2 [`output grid`] : Onset Day, 2. Filial Generation
    - ONSET_SISTER_2 [`output grid`] : Onset Day, 2. Sister Generation
    - ONSET_FILIAL_3 [`output grid`] : Onset Day, 3. Filial Generation
    - ONSET_SISTER_3 [`output grid`] : Onset Day, 3. Sister Generation
    - BTSUM_FILIAL_1 [`output grid`] : State, 1. Filial Generation
    - BTSUM_SISTER_1 [`output grid`] : State, 1. Sister Generation
    - BTSUM_FILIAL_2 [`output grid`] : State, 2. Filial Generation
    - BTSUM_SISTER_2 [`output grid`] : State, 2. Sister Generation
    - BTSUM_FILIAL_3 [`output grid`] : State, 3. Filial Generation
    - BTSUM_SISTER_3 [`output grid`] : State, 3. Sister Generation
    - ATSUM_EFF [`output grid`] : Air Temperature Sum
    - RISK [`output grid`] : Risk
    - LAT_CONST [`floating point number`] : Latitude. Minimum: -90.000000 Maximum: 90.000000 Default: 50.000000
    - DTOPTIMUM [`floating point number`] : Developmental Optimum Temperature. Default: 30.400000 Degree Celsius
    - DTMINIMUM [`floating point number`] : Developmental Minimum Temperature. Default: 8.300000 Degree Celsius
    - FAMINIMUM [`floating point number`] : Minimum Temperature for Flight Activity. Default: 16.500000 Degree Celsius
    - DAYLENGTH [`floating point number`] : Minimum Day Length for Flight Activity. Minimum: 0.000000 Maximum: 24.000000 Default: 14.500000 Hours
    - DDMINIMUM [`floating point number`] : Minimum Thermal Sum for Infestation. Minimum: 0.000000 Default: 140.000000 Degree Days
    - DDTOTAL [`floating point number`] : Thermal Sum for Total Development. Minimum: 0.000000 Default: 557.000000 Degree Days
    - RISK_DAYMAX [`floating point number`] : Day of Maximum Risk after Onset. Minimum: 0.000000 Default: 5.000000 Days
    - RISK_DECAY [`floating point number`] : Decay of Risk after Maximum. Minimum: 1.000000 Default: 10.000000 Days
    - YD_BEGIN [`date`] : Begin of Parental Development. Default: 2025-04-01
    - YD_END_ONSET [`date`] : End of Breeding. Default: 2025-08-31
    - YD_END [`date`] : End of Development. Default: 2025-10-31
    - RESET [`boolean`] : Reset. Default: 1
    - DAY [`date`] : Start Day. Default: 2025-01-20

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('climate_tools', '23', 'PhenIps (Grids, Days)')
    if Tool.is_Okay():
        Tool.Set_Input ('ATmean', ATMEAN)
        Tool.Set_Input ('ATmax', ATMAX)
        Tool.Set_Input ('SIrel', SIREL)
        Tool.Set_Input ('LAT_GRID', LAT_GRID)
        Tool.Set_Output('GENERATIONS', GENERATIONS)
        Tool.Set_Output('ONSET', ONSET)
        Tool.Set_Output('ONSET_FILIAL_1', ONSET_FILIAL_1)
        Tool.Set_Output('ONSET_SISTER_1', ONSET_SISTER_1)
        Tool.Set_Output('ONSET_FILIAL_2', ONSET_FILIAL_2)
        Tool.Set_Output('ONSET_SISTER_2', ONSET_SISTER_2)
        Tool.Set_Output('ONSET_FILIAL_3', ONSET_FILIAL_3)
        Tool.Set_Output('ONSET_SISTER_3', ONSET_SISTER_3)
        Tool.Set_Output('BTSUM_FILIAL_1', BTSUM_FILIAL_1)
        Tool.Set_Output('BTSUM_SISTER_1', BTSUM_SISTER_1)
        Tool.Set_Output('BTSUM_FILIAL_2', BTSUM_FILIAL_2)
        Tool.Set_Output('BTSUM_SISTER_2', BTSUM_SISTER_2)
        Tool.Set_Output('BTSUM_FILIAL_3', BTSUM_FILIAL_3)
        Tool.Set_Output('BTSUM_SISTER_3', BTSUM_SISTER_3)
        Tool.Set_Output('ATSUM_EFF', ATSUM_EFF)
        Tool.Set_Output('RISK', RISK)
        Tool.Set_Option('LAT_CONST', LAT_CONST)
        Tool.Set_Option('DToptimum', DTOPTIMUM)
        Tool.Set_Option('DTminimum', DTMINIMUM)
        Tool.Set_Option('FAminimum', FAMINIMUM)
        Tool.Set_Option('DayLength', DAYLENGTH)
        Tool.Set_Option('DDminimum', DDMINIMUM)
        Tool.Set_Option('DDtotal', DDTOTAL)
        Tool.Set_Option('Risk_DayMax', RISK_DAYMAX)
        Tool.Set_Option('Risk_Decay', RISK_DECAY)
        Tool.Set_Option('YD_Begin', YD_BEGIN)
        Tool.Set_Option('YD_End_Onset', YD_END_ONSET)
        Tool.Set_Option('YD_End', YD_END)
        Tool.Set_Option('RESET', RESET)
        Tool.Set_Option('DAY', DAY)
        return Tool.Execute(Verbose)
    return False

def Soil_Water_Balance_Days(TAVG=None, TMIN=None, TMAX=None, PSUM=None, LAT_GRID=None, SWC=None, SNOW=None, SW_0=None, SW_1=None, LAT_CONST=None, SWC_DEFAULT=None, SWC_SURFACE=None, SWT_RESIST=None, RESET=None, DAY=None, Verbose=2):
    '''
    Soil Water Balance (Days)
    ----------
    [climate_tools.24]\n
    A Simple Soil Water Balance Model\n
    Arguments
    ----------
    - TAVG [`input grid list`] : Mean Temperature
    - TMIN [`input grid list`] : Minimum Temperature
    - TMAX [`input grid list`] : Maximum Temperature
    - PSUM [`input grid list`] : Precipitation
    - LAT_GRID [`optional input grid`] : Latitude
    - SWC [`optional input grid`] : Soil Water Capacity of Profile. Total soil water capacity (mm H2O).
    - SNOW [`output grid`] : Snow Depth
    - SW_0 [`output grid`] : Soil Water (Upper Layer)
    - SW_1 [`output grid`] : Soil Water (Lower Layer)
    - LAT_CONST [`floating point number`] : Default Latitude. Minimum: -90.000000 Maximum: 90.000000 Default: 50.000000
    - SWC_DEFAULT [`floating point number`] : Default. Minimum: 0.000000 Default: 220.000000 default value if no grid has been selected
    - SWC_SURFACE [`floating point number`] : Top Soil Water Capacity. Minimum: 0.000000 Default: 30.000000
    - SWT_RESIST [`floating point number`] : Transpiration Resistance. Minimum: 0.010000 Default: 0.500000
    - RESET [`boolean`] : Reset. Default: 1
    - DAY [`date`] : Start Day. Default: 2025-01-20

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('climate_tools', '24', 'Soil Water Balance (Days)')
    if Tool.is_Okay():
        Tool.Set_Input ('TAVG', TAVG)
        Tool.Set_Input ('TMIN', TMIN)
        Tool.Set_Input ('TMAX', TMAX)
        Tool.Set_Input ('PSUM', PSUM)
        Tool.Set_Input ('LAT_GRID', LAT_GRID)
        Tool.Set_Input ('SWC', SWC)
        Tool.Set_Output('SNOW', SNOW)
        Tool.Set_Output('SW_0', SW_0)
        Tool.Set_Output('SW_1', SW_1)
        Tool.Set_Option('LAT_CONST', LAT_CONST)
        Tool.Set_Option('SWC_DEFAULT', SWC_DEFAULT)
        Tool.Set_Option('SWC_SURFACE', SWC_SURFACE)
        Tool.Set_Option('SWT_RESIST', SWT_RESIST)
        Tool.Set_Option('RESET', RESET)
        Tool.Set_Option('DAY', DAY)
        return Tool.Execute(Verbose)
    return False

def run_tool_climate_tools_24(TAVG=None, TMIN=None, TMAX=None, PSUM=None, LAT_GRID=None, SWC=None, SNOW=None, SW_0=None, SW_1=None, LAT_CONST=None, SWC_DEFAULT=None, SWC_SURFACE=None, SWT_RESIST=None, RESET=None, DAY=None, Verbose=2):
    '''
    Soil Water Balance (Days)
    ----------
    [climate_tools.24]\n
    A Simple Soil Water Balance Model\n
    Arguments
    ----------
    - TAVG [`input grid list`] : Mean Temperature
    - TMIN [`input grid list`] : Minimum Temperature
    - TMAX [`input grid list`] : Maximum Temperature
    - PSUM [`input grid list`] : Precipitation
    - LAT_GRID [`optional input grid`] : Latitude
    - SWC [`optional input grid`] : Soil Water Capacity of Profile. Total soil water capacity (mm H2O).
    - SNOW [`output grid`] : Snow Depth
    - SW_0 [`output grid`] : Soil Water (Upper Layer)
    - SW_1 [`output grid`] : Soil Water (Lower Layer)
    - LAT_CONST [`floating point number`] : Default Latitude. Minimum: -90.000000 Maximum: 90.000000 Default: 50.000000
    - SWC_DEFAULT [`floating point number`] : Default. Minimum: 0.000000 Default: 220.000000 default value if no grid has been selected
    - SWC_SURFACE [`floating point number`] : Top Soil Water Capacity. Minimum: 0.000000 Default: 30.000000
    - SWT_RESIST [`floating point number`] : Transpiration Resistance. Minimum: 0.010000 Default: 0.500000
    - RESET [`boolean`] : Reset. Default: 1
    - DAY [`date`] : Start Day. Default: 2025-01-20

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('climate_tools', '24', 'Soil Water Balance (Days)')
    if Tool.is_Okay():
        Tool.Set_Input ('TAVG', TAVG)
        Tool.Set_Input ('TMIN', TMIN)
        Tool.Set_Input ('TMAX', TMAX)
        Tool.Set_Input ('PSUM', PSUM)
        Tool.Set_Input ('LAT_GRID', LAT_GRID)
        Tool.Set_Input ('SWC', SWC)
        Tool.Set_Output('SNOW', SNOW)
        Tool.Set_Output('SW_0', SW_0)
        Tool.Set_Output('SW_1', SW_1)
        Tool.Set_Option('LAT_CONST', LAT_CONST)
        Tool.Set_Option('SWC_DEFAULT', SWC_DEFAULT)
        Tool.Set_Option('SWC_SURFACE', SWC_SURFACE)
        Tool.Set_Option('SWT_RESIST', SWT_RESIST)
        Tool.Set_Option('RESET', RESET)
        Tool.Set_Option('DAY', DAY)
        return Tool.Execute(Verbose)
    return False

def Cloud_Overlap(COVERS=None, HEIGHTS=None, GROUND=None, WIND=None, CBASE=None, COVER=None, BLOCKS=None, INTERVAL=None, MINCOVER=None, Verbose=2):
    '''
    Cloud Overlap
    ----------
    [climate_tools.25]\n
    This tool calculates cloud overlay based on the maximum random overlap assumption for atmospheric cloud layers above ground. Alpha is a constant and a further parameter is the minimum cloud fraction, at which a cloud is identified as such.\n
    Arguments
    ----------
    - COVERS [`input grid list`] : Cloud Fractions. grid stack of cloud fractions
    - HEIGHTS [`input grid list`] : Heights. grid stack of geopotential level heights
    - GROUND [`input grid`] : Surface Elevation
    - WIND [`input grid`] : Wind effect
    - CBASE [`input grid`] : Cloud Base
    - COVER [`output grid`] : Total Cloud Cover. statistics
    - BLOCKS [`output grid`] : Number of Cloud Blocks
    - INTERVAL [`floating point number`] : Interval. Minimum: 1.000000 Default: 100.000000 Vertical resolution for internal interpolation given in meters.
    - MINCOVER [`floating point number`] : Minimum Cloud Cover Fraction. Minimum: -1.000000 Maximum: 1.000000 Default: 0.100000 Minimum cloud cover fraction at which a cloud is identified as such

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('climate_tools', '25', 'Cloud Overlap')
    if Tool.is_Okay():
        Tool.Set_Input ('COVERS', COVERS)
        Tool.Set_Input ('HEIGHTS', HEIGHTS)
        Tool.Set_Input ('GROUND', GROUND)
        Tool.Set_Input ('WIND', WIND)
        Tool.Set_Input ('CBASE', CBASE)
        Tool.Set_Output('COVER', COVER)
        Tool.Set_Output('BLOCKS', BLOCKS)
        Tool.Set_Option('INTERVAL', INTERVAL)
        Tool.Set_Option('MINCOVER', MINCOVER)
        return Tool.Execute(Verbose)
    return False

def run_tool_climate_tools_25(COVERS=None, HEIGHTS=None, GROUND=None, WIND=None, CBASE=None, COVER=None, BLOCKS=None, INTERVAL=None, MINCOVER=None, Verbose=2):
    '''
    Cloud Overlap
    ----------
    [climate_tools.25]\n
    This tool calculates cloud overlay based on the maximum random overlap assumption for atmospheric cloud layers above ground. Alpha is a constant and a further parameter is the minimum cloud fraction, at which a cloud is identified as such.\n
    Arguments
    ----------
    - COVERS [`input grid list`] : Cloud Fractions. grid stack of cloud fractions
    - HEIGHTS [`input grid list`] : Heights. grid stack of geopotential level heights
    - GROUND [`input grid`] : Surface Elevation
    - WIND [`input grid`] : Wind effect
    - CBASE [`input grid`] : Cloud Base
    - COVER [`output grid`] : Total Cloud Cover. statistics
    - BLOCKS [`output grid`] : Number of Cloud Blocks
    - INTERVAL [`floating point number`] : Interval. Minimum: 1.000000 Default: 100.000000 Vertical resolution for internal interpolation given in meters.
    - MINCOVER [`floating point number`] : Minimum Cloud Cover Fraction. Minimum: -1.000000 Maximum: 1.000000 Default: 0.100000 Minimum cloud cover fraction at which a cloud is identified as such

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('climate_tools', '25', 'Cloud Overlap')
    if Tool.is_Okay():
        Tool.Set_Input ('COVERS', COVERS)
        Tool.Set_Input ('HEIGHTS', HEIGHTS)
        Tool.Set_Input ('GROUND', GROUND)
        Tool.Set_Input ('WIND', WIND)
        Tool.Set_Input ('CBASE', CBASE)
        Tool.Set_Output('COVER', COVER)
        Tool.Set_Output('BLOCKS', BLOCKS)
        Tool.Set_Option('INTERVAL', INTERVAL)
        Tool.Set_Option('MINCOVER', MINCOVER)
        return Tool.Execute(Verbose)
    return False

def Temperature_Lapse_Rates(TEMP=None, TGROUND=None, LAPSE=None, TEXTREME=None, TIME=None, EXTREME=None, Verbose=2):
    '''
    Temperature Lapse Rates
    ----------
    [climate_tools.26]\n
    This tool selects daily temperature lapse rates for minimum and maximum temperatures from hourly lapse rates by selecting the time at which minimum or maximum temperatures occurred and then returns the respective lapse rate.\n
    Arguments
    ----------
    - TEMP [`input grid list`] : Atmospheric Lapse Rates. grid stack of hourly atmospheric lapse rates
    - TGROUND [`input grid list`] : Surface Temperature
    - LAPSE [`output grid`] : Temperature Lapse Rate at Extreme
    - TEXTREME [`output grid`] : Daily Extreme Temperature
    - TIME [`output grid`] : Hour of Daily Extreme Temperature
    - EXTREME [`choice`] : Temperature extreme. Available Choices: [0] minimum [1] maximum Default: 1

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('climate_tools', '26', 'Temperature Lapse Rates')
    if Tool.is_Okay():
        Tool.Set_Input ('TEMP', TEMP)
        Tool.Set_Input ('TGROUND', TGROUND)
        Tool.Set_Output('LAPSE', LAPSE)
        Tool.Set_Output('TEXTREME', TEXTREME)
        Tool.Set_Output('TIME', TIME)
        Tool.Set_Option('EXTREME', EXTREME)
        return Tool.Execute(Verbose)
    return False

def run_tool_climate_tools_26(TEMP=None, TGROUND=None, LAPSE=None, TEXTREME=None, TIME=None, EXTREME=None, Verbose=2):
    '''
    Temperature Lapse Rates
    ----------
    [climate_tools.26]\n
    This tool selects daily temperature lapse rates for minimum and maximum temperatures from hourly lapse rates by selecting the time at which minimum or maximum temperatures occurred and then returns the respective lapse rate.\n
    Arguments
    ----------
    - TEMP [`input grid list`] : Atmospheric Lapse Rates. grid stack of hourly atmospheric lapse rates
    - TGROUND [`input grid list`] : Surface Temperature
    - LAPSE [`output grid`] : Temperature Lapse Rate at Extreme
    - TEXTREME [`output grid`] : Daily Extreme Temperature
    - TIME [`output grid`] : Hour of Daily Extreme Temperature
    - EXTREME [`choice`] : Temperature extreme. Available Choices: [0] minimum [1] maximum Default: 1

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('climate_tools', '26', 'Temperature Lapse Rates')
    if Tool.is_Okay():
        Tool.Set_Input ('TEMP', TEMP)
        Tool.Set_Input ('TGROUND', TGROUND)
        Tool.Set_Output('LAPSE', LAPSE)
        Tool.Set_Output('TEXTREME', TEXTREME)
        Tool.Set_Output('TIME', TIME)
        Tool.Set_Option('EXTREME', EXTREME)
        return Tool.Execute(Verbose)
    return False

def Air_Pressure_Adjustment(DEM=None, P=None, Z=None, T=None, L=None, P_ADJ=None, P_GRIDSYSTEM=None, P_DEFAULT=None, Z_GRIDSYSTEM=None, Z_DEFAULT=None, T_GRIDSYSTEM=None, T_DEFAULT=None, L_GRIDSYSTEM=None, L_DEFAULT=None, Verbose=2):
    '''
    Air Pressure Adjustment
    ----------
    [climate_tools.27]\n
    This tool adjusts air pressure values to the elevation using the barometric formula. Default values refer to the international standard atmosphere.\n
    Arguments
    ----------
    - DEM [`input grid`] : Elevation. [m]
    - P [`optional input grid`] : Air Pressure. [hPa]
    - Z [`optional input grid`] : Air Pressure Elevation. [m]
    - T [`optional input grid`] : Temperature. [Celsius]
    - L [`optional input grid`] : Temperature Lapse Rate. [K/m]
    - P_ADJ [`output grid`] : Adjusted Air Pressure
    - P_GRIDSYSTEM [`grid system`] : Grid system
    - P_DEFAULT [`floating point number`] : Default. Default: 1013.250000 default value if no grid has been selected
    - Z_GRIDSYSTEM [`grid system`] : Grid system
    - Z_DEFAULT [`floating point number`] : Default. Default: 0.000000 default value if no grid has been selected
    - T_GRIDSYSTEM [`grid system`] : Grid system
    - T_DEFAULT [`floating point number`] : Default. Default: 0.000000 default value if no grid has been selected
    - L_GRIDSYSTEM [`grid system`] : Grid system
    - L_DEFAULT [`floating point number`] : Default. Default: 0.006500 default value if no grid has been selected

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('climate_tools', '27', 'Air Pressure Adjustment')
    if Tool.is_Okay():
        Tool.Set_Input ('DEM', DEM)
        Tool.Set_Input ('P', P)
        Tool.Set_Input ('Z', Z)
        Tool.Set_Input ('T', T)
        Tool.Set_Input ('L', L)
        Tool.Set_Output('P_ADJ', P_ADJ)
        Tool.Set_Option('P_GRIDSYSTEM', P_GRIDSYSTEM)
        Tool.Set_Option('P_DEFAULT', P_DEFAULT)
        Tool.Set_Option('Z_GRIDSYSTEM', Z_GRIDSYSTEM)
        Tool.Set_Option('Z_DEFAULT', Z_DEFAULT)
        Tool.Set_Option('T_GRIDSYSTEM', T_GRIDSYSTEM)
        Tool.Set_Option('T_DEFAULT', T_DEFAULT)
        Tool.Set_Option('L_GRIDSYSTEM', L_GRIDSYSTEM)
        Tool.Set_Option('L_DEFAULT', L_DEFAULT)
        return Tool.Execute(Verbose)
    return False

def run_tool_climate_tools_27(DEM=None, P=None, Z=None, T=None, L=None, P_ADJ=None, P_GRIDSYSTEM=None, P_DEFAULT=None, Z_GRIDSYSTEM=None, Z_DEFAULT=None, T_GRIDSYSTEM=None, T_DEFAULT=None, L_GRIDSYSTEM=None, L_DEFAULT=None, Verbose=2):
    '''
    Air Pressure Adjustment
    ----------
    [climate_tools.27]\n
    This tool adjusts air pressure values to the elevation using the barometric formula. Default values refer to the international standard atmosphere.\n
    Arguments
    ----------
    - DEM [`input grid`] : Elevation. [m]
    - P [`optional input grid`] : Air Pressure. [hPa]
    - Z [`optional input grid`] : Air Pressure Elevation. [m]
    - T [`optional input grid`] : Temperature. [Celsius]
    - L [`optional input grid`] : Temperature Lapse Rate. [K/m]
    - P_ADJ [`output grid`] : Adjusted Air Pressure
    - P_GRIDSYSTEM [`grid system`] : Grid system
    - P_DEFAULT [`floating point number`] : Default. Default: 1013.250000 default value if no grid has been selected
    - Z_GRIDSYSTEM [`grid system`] : Grid system
    - Z_DEFAULT [`floating point number`] : Default. Default: 0.000000 default value if no grid has been selected
    - T_GRIDSYSTEM [`grid system`] : Grid system
    - T_DEFAULT [`floating point number`] : Default. Default: 0.000000 default value if no grid has been selected
    - L_GRIDSYSTEM [`grid system`] : Grid system
    - L_DEFAULT [`floating point number`] : Default. Default: 0.006500 default value if no grid has been selected

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('climate_tools', '27', 'Air Pressure Adjustment')
    if Tool.is_Okay():
        Tool.Set_Input ('DEM', DEM)
        Tool.Set_Input ('P', P)
        Tool.Set_Input ('Z', Z)
        Tool.Set_Input ('T', T)
        Tool.Set_Input ('L', L)
        Tool.Set_Output('P_ADJ', P_ADJ)
        Tool.Set_Option('P_GRIDSYSTEM', P_GRIDSYSTEM)
        Tool.Set_Option('P_DEFAULT', P_DEFAULT)
        Tool.Set_Option('Z_GRIDSYSTEM', Z_GRIDSYSTEM)
        Tool.Set_Option('Z_DEFAULT', Z_DEFAULT)
        Tool.Set_Option('T_GRIDSYSTEM', T_GRIDSYSTEM)
        Tool.Set_Option('T_DEFAULT', T_DEFAULT)
        Tool.Set_Option('L_GRIDSYSTEM', L_GRIDSYSTEM)
        Tool.Set_Option('L_DEFAULT', L_DEFAULT)
        return Tool.Execute(Verbose)
    return False

def Land_Surface_Temperature(IRRADIANCE=None, ALBEDO=None, EMISSIVITY=None, CONVECTION=None, T_AIR=None, T_SKY=None, T_INITIAL=None, LST=None, IRRADIANCE_DEFAULT=None, ALBEDO_DEFAULT=None, EMISSIVITY_DEFAULT=None, CONVECTION_DEFAULT=None, T_AIR_DEFAULT=None, T_SKY_DEFAULT=None, T_INITIAL_DEFAULT=None, UNIT=None, ITERATIONS=None, Verbose=2):
    '''
    Land Surface Temperature
    ----------
    [climate_tools.28]\n
    This tool estimates the land surface temperature by combining global solar radiation, albedo, and the Stefan-Boltzmann Law. This is an implementation of the approach proposed by Hofierka et al. (2020).\n
    Arguments
    ----------
    - IRRADIANCE [`optional input grid`] : Global Irradiance. [W/m²]
    - ALBEDO [`optional input grid`] : Albedo. Surface reflectance [0 <= albedo <= 1]
    - EMISSIVITY [`optional input grid`] : Emissivity. Thermal emissivity [0 <= emissivity <= 1]
    - CONVECTION [`optional input grid`] : Convection Coefficient. Convection heat transfer coefficient [W/m²/K].
    - T_AIR [`optional input grid`] : Ambient Air Temperature. [Kelvin]
    - T_SKY [`optional input grid`] : Radiant Sky Temperature. [Kelvin]
    - T_INITIAL [`optional input grid`] : Initial Temperature Estimation. Initial estimation of land surface temperature [Kelvin] (e.g., 300).
    - LST [`output grid`] : Land Surface Temperature. [Kelvin]
    - IRRADIANCE_DEFAULT [`floating point number`] : Default. Minimum: 0.000000 Default: 5.000000 default value if no grid has been selected
    - ALBEDO_DEFAULT [`floating point number`] : Default. Minimum: 0.000000 Maximum: 1.000000 Default: 0.500000 default value if no grid has been selected
    - EMISSIVITY_DEFAULT [`floating point number`] : Default. Minimum: 0.000000 Maximum: 1.000000 Default: 0.500000 default value if no grid has been selected
    - CONVECTION_DEFAULT [`floating point number`] : Default. Minimum: 0.000000 Default: 10.000000 default value if no grid has been selected
    - T_AIR_DEFAULT [`floating point number`] : Default. Minimum: 0.000000 Default: 300.000000 default value if no grid has been selected
    - T_SKY_DEFAULT [`floating point number`] : Default. Minimum: 0.000000 Default: 280.000000 default value if no grid has been selected
    - T_INITIAL_DEFAULT [`floating point number`] : Default. Minimum: 0.000000 Default: 300.000000 default value if no grid has been selected
    - UNIT [`choice`] : Unit. Available Choices: [0] Kelvin [1] Celsius Default: 0
    - ITERATIONS [`integer number`] : Iterations. Minimum: 1 Default: 10

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('climate_tools', '28', 'Land Surface Temperature')
    if Tool.is_Okay():
        Tool.Set_Input ('IRRADIANCE', IRRADIANCE)
        Tool.Set_Input ('ALBEDO', ALBEDO)
        Tool.Set_Input ('EMISSIVITY', EMISSIVITY)
        Tool.Set_Input ('CONVECTION', CONVECTION)
        Tool.Set_Input ('T_AIR', T_AIR)
        Tool.Set_Input ('T_SKY', T_SKY)
        Tool.Set_Input ('T_INITIAL', T_INITIAL)
        Tool.Set_Output('LST', LST)
        Tool.Set_Option('IRRADIANCE_DEFAULT', IRRADIANCE_DEFAULT)
        Tool.Set_Option('ALBEDO_DEFAULT', ALBEDO_DEFAULT)
        Tool.Set_Option('EMISSIVITY_DEFAULT', EMISSIVITY_DEFAULT)
        Tool.Set_Option('CONVECTION_DEFAULT', CONVECTION_DEFAULT)
        Tool.Set_Option('T_AIR_DEFAULT', T_AIR_DEFAULT)
        Tool.Set_Option('T_SKY_DEFAULT', T_SKY_DEFAULT)
        Tool.Set_Option('T_INITIAL_DEFAULT', T_INITIAL_DEFAULT)
        Tool.Set_Option('UNIT', UNIT)
        Tool.Set_Option('ITERATIONS', ITERATIONS)
        return Tool.Execute(Verbose)
    return False

def run_tool_climate_tools_28(IRRADIANCE=None, ALBEDO=None, EMISSIVITY=None, CONVECTION=None, T_AIR=None, T_SKY=None, T_INITIAL=None, LST=None, IRRADIANCE_DEFAULT=None, ALBEDO_DEFAULT=None, EMISSIVITY_DEFAULT=None, CONVECTION_DEFAULT=None, T_AIR_DEFAULT=None, T_SKY_DEFAULT=None, T_INITIAL_DEFAULT=None, UNIT=None, ITERATIONS=None, Verbose=2):
    '''
    Land Surface Temperature
    ----------
    [climate_tools.28]\n
    This tool estimates the land surface temperature by combining global solar radiation, albedo, and the Stefan-Boltzmann Law. This is an implementation of the approach proposed by Hofierka et al. (2020).\n
    Arguments
    ----------
    - IRRADIANCE [`optional input grid`] : Global Irradiance. [W/m²]
    - ALBEDO [`optional input grid`] : Albedo. Surface reflectance [0 <= albedo <= 1]
    - EMISSIVITY [`optional input grid`] : Emissivity. Thermal emissivity [0 <= emissivity <= 1]
    - CONVECTION [`optional input grid`] : Convection Coefficient. Convection heat transfer coefficient [W/m²/K].
    - T_AIR [`optional input grid`] : Ambient Air Temperature. [Kelvin]
    - T_SKY [`optional input grid`] : Radiant Sky Temperature. [Kelvin]
    - T_INITIAL [`optional input grid`] : Initial Temperature Estimation. Initial estimation of land surface temperature [Kelvin] (e.g., 300).
    - LST [`output grid`] : Land Surface Temperature. [Kelvin]
    - IRRADIANCE_DEFAULT [`floating point number`] : Default. Minimum: 0.000000 Default: 5.000000 default value if no grid has been selected
    - ALBEDO_DEFAULT [`floating point number`] : Default. Minimum: 0.000000 Maximum: 1.000000 Default: 0.500000 default value if no grid has been selected
    - EMISSIVITY_DEFAULT [`floating point number`] : Default. Minimum: 0.000000 Maximum: 1.000000 Default: 0.500000 default value if no grid has been selected
    - CONVECTION_DEFAULT [`floating point number`] : Default. Minimum: 0.000000 Default: 10.000000 default value if no grid has been selected
    - T_AIR_DEFAULT [`floating point number`] : Default. Minimum: 0.000000 Default: 300.000000 default value if no grid has been selected
    - T_SKY_DEFAULT [`floating point number`] : Default. Minimum: 0.000000 Default: 280.000000 default value if no grid has been selected
    - T_INITIAL_DEFAULT [`floating point number`] : Default. Minimum: 0.000000 Default: 300.000000 default value if no grid has been selected
    - UNIT [`choice`] : Unit. Available Choices: [0] Kelvin [1] Celsius Default: 0
    - ITERATIONS [`integer number`] : Iterations. Minimum: 1 Default: 10

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('climate_tools', '28', 'Land Surface Temperature')
    if Tool.is_Okay():
        Tool.Set_Input ('IRRADIANCE', IRRADIANCE)
        Tool.Set_Input ('ALBEDO', ALBEDO)
        Tool.Set_Input ('EMISSIVITY', EMISSIVITY)
        Tool.Set_Input ('CONVECTION', CONVECTION)
        Tool.Set_Input ('T_AIR', T_AIR)
        Tool.Set_Input ('T_SKY', T_SKY)
        Tool.Set_Input ('T_INITIAL', T_INITIAL)
        Tool.Set_Output('LST', LST)
        Tool.Set_Option('IRRADIANCE_DEFAULT', IRRADIANCE_DEFAULT)
        Tool.Set_Option('ALBEDO_DEFAULT', ALBEDO_DEFAULT)
        Tool.Set_Option('EMISSIVITY_DEFAULT', EMISSIVITY_DEFAULT)
        Tool.Set_Option('CONVECTION_DEFAULT', CONVECTION_DEFAULT)
        Tool.Set_Option('T_AIR_DEFAULT', T_AIR_DEFAULT)
        Tool.Set_Option('T_SKY_DEFAULT', T_SKY_DEFAULT)
        Tool.Set_Option('T_INITIAL_DEFAULT', T_INITIAL_DEFAULT)
        Tool.Set_Option('UNIT', UNIT)
        Tool.Set_Option('ITERATIONS', ITERATIONS)
        return Tool.Execute(Verbose)
    return False

def Air_Humidity_Conversions(T=None, P=None, IN_VP=None, IN_SH=None, IN_RH=None, IN_DP=None, OUT_VPSAT=None, OUT_VP=None, OUT_VPDIF=None, OUT_RH=None, OUT_SH=None, OUT_DP=None, OUT_DPDIF=None, T_DEFAULT=None, P_DEFAULT=None, IN_VP_DEFAULT=None, IN_SH_DEFAULT=None, IN_RH_DEFAULT=None, IN_DP_DEFAULT=None, CONVERSION=None, VPSAT_METHOD=None, Verbose=2):
    '''
    Air Humidity Conversions
    ----------
    [climate_tools.29]\n
    Conversions of air moisture content between various units.\n
    Arguments
    ----------
    - T [`optional input grid`] : Temperature. [Celsius]
    - P [`optional input grid`] : Air Pressure. [hPa]
    - IN_VP [`optional input grid`] : Vapor Pressure. [hPa]
    - IN_SH [`optional input grid`] : Specific Humidity. [g/kg]
    - IN_RH [`optional input grid`] : Relative Humidity. [%]
    - IN_DP [`optional input grid`] : Dew Point. [Celsius]
    - OUT_VPSAT [`output grid`] : Saturation Pressure. [hPa]
    - OUT_VP [`output grid`] : Vapor Pressure. [hPa]
    - OUT_VPDIF [`output grid`] : Vapor Pressure Deficit. [hPa]
    - OUT_RH [`output grid`] : Relative Humidity. [%]
    - OUT_SH [`output grid`] : Specific Humidity. [g/kg]
    - OUT_DP [`output grid`] : Dew Point. [Celsius]
    - OUT_DPDIF [`output grid`] : Dew Point Difference. [Celsius]
    - T_DEFAULT [`floating point number`] : Default. Minimum: -273.150000 Default: 25.000000 default value if no grid has been selected
    - P_DEFAULT [`floating point number`] : Default. Minimum: 0.000000 Default: 1013.250000 default value if no grid has been selected
    - IN_VP_DEFAULT [`floating point number`] : Default. Minimum: 0.000000 Default: 15.000000 default value if no grid has been selected
    - IN_SH_DEFAULT [`floating point number`] : Default. Minimum: 0.000000 Default: 10.000000 default value if no grid has been selected
    - IN_RH_DEFAULT [`floating point number`] : Default. Minimum: 0.000000 Maximum: 100.000000 Default: 50.000000 default value if no grid has been selected
    - IN_DP_DEFAULT [`floating point number`] : Default. Minimum: -273.150000 Default: 14.000000 default value if no grid has been selected
    - CONVERSION [`choice`] : Conversion from.... Available Choices: [0] Vapor Pressure [1] Specific Humidity [2] Relative Humidity [3] Dew Point Default: 0
    - VPSAT_METHOD [`choice`] : Saturation Pressure. Available Choices: [0] Magnus [1] Lowe & Ficke Default: 0 Formula used to estimate vapor pressure at saturation.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('climate_tools', '29', 'Air Humidity Conversions')
    if Tool.is_Okay():
        Tool.Set_Input ('T', T)
        Tool.Set_Input ('P', P)
        Tool.Set_Input ('IN_VP', IN_VP)
        Tool.Set_Input ('IN_SH', IN_SH)
        Tool.Set_Input ('IN_RH', IN_RH)
        Tool.Set_Input ('IN_DP', IN_DP)
        Tool.Set_Output('OUT_VPSAT', OUT_VPSAT)
        Tool.Set_Output('OUT_VP', OUT_VP)
        Tool.Set_Output('OUT_VPDIF', OUT_VPDIF)
        Tool.Set_Output('OUT_RH', OUT_RH)
        Tool.Set_Output('OUT_SH', OUT_SH)
        Tool.Set_Output('OUT_DP', OUT_DP)
        Tool.Set_Output('OUT_DPDIF', OUT_DPDIF)
        Tool.Set_Option('T_DEFAULT', T_DEFAULT)
        Tool.Set_Option('P_DEFAULT', P_DEFAULT)
        Tool.Set_Option('IN_VP_DEFAULT', IN_VP_DEFAULT)
        Tool.Set_Option('IN_SH_DEFAULT', IN_SH_DEFAULT)
        Tool.Set_Option('IN_RH_DEFAULT', IN_RH_DEFAULT)
        Tool.Set_Option('IN_DP_DEFAULT', IN_DP_DEFAULT)
        Tool.Set_Option('CONVERSION', CONVERSION)
        Tool.Set_Option('VPSAT_METHOD', VPSAT_METHOD)
        return Tool.Execute(Verbose)
    return False

def run_tool_climate_tools_29(T=None, P=None, IN_VP=None, IN_SH=None, IN_RH=None, IN_DP=None, OUT_VPSAT=None, OUT_VP=None, OUT_VPDIF=None, OUT_RH=None, OUT_SH=None, OUT_DP=None, OUT_DPDIF=None, T_DEFAULT=None, P_DEFAULT=None, IN_VP_DEFAULT=None, IN_SH_DEFAULT=None, IN_RH_DEFAULT=None, IN_DP_DEFAULT=None, CONVERSION=None, VPSAT_METHOD=None, Verbose=2):
    '''
    Air Humidity Conversions
    ----------
    [climate_tools.29]\n
    Conversions of air moisture content between various units.\n
    Arguments
    ----------
    - T [`optional input grid`] : Temperature. [Celsius]
    - P [`optional input grid`] : Air Pressure. [hPa]
    - IN_VP [`optional input grid`] : Vapor Pressure. [hPa]
    - IN_SH [`optional input grid`] : Specific Humidity. [g/kg]
    - IN_RH [`optional input grid`] : Relative Humidity. [%]
    - IN_DP [`optional input grid`] : Dew Point. [Celsius]
    - OUT_VPSAT [`output grid`] : Saturation Pressure. [hPa]
    - OUT_VP [`output grid`] : Vapor Pressure. [hPa]
    - OUT_VPDIF [`output grid`] : Vapor Pressure Deficit. [hPa]
    - OUT_RH [`output grid`] : Relative Humidity. [%]
    - OUT_SH [`output grid`] : Specific Humidity. [g/kg]
    - OUT_DP [`output grid`] : Dew Point. [Celsius]
    - OUT_DPDIF [`output grid`] : Dew Point Difference. [Celsius]
    - T_DEFAULT [`floating point number`] : Default. Minimum: -273.150000 Default: 25.000000 default value if no grid has been selected
    - P_DEFAULT [`floating point number`] : Default. Minimum: 0.000000 Default: 1013.250000 default value if no grid has been selected
    - IN_VP_DEFAULT [`floating point number`] : Default. Minimum: 0.000000 Default: 15.000000 default value if no grid has been selected
    - IN_SH_DEFAULT [`floating point number`] : Default. Minimum: 0.000000 Default: 10.000000 default value if no grid has been selected
    - IN_RH_DEFAULT [`floating point number`] : Default. Minimum: 0.000000 Maximum: 100.000000 Default: 50.000000 default value if no grid has been selected
    - IN_DP_DEFAULT [`floating point number`] : Default. Minimum: -273.150000 Default: 14.000000 default value if no grid has been selected
    - CONVERSION [`choice`] : Conversion from.... Available Choices: [0] Vapor Pressure [1] Specific Humidity [2] Relative Humidity [3] Dew Point Default: 0
    - VPSAT_METHOD [`choice`] : Saturation Pressure. Available Choices: [0] Magnus [1] Lowe & Ficke Default: 0 Formula used to estimate vapor pressure at saturation.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('climate_tools', '29', 'Air Humidity Conversions')
    if Tool.is_Okay():
        Tool.Set_Input ('T', T)
        Tool.Set_Input ('P', P)
        Tool.Set_Input ('IN_VP', IN_VP)
        Tool.Set_Input ('IN_SH', IN_SH)
        Tool.Set_Input ('IN_RH', IN_RH)
        Tool.Set_Input ('IN_DP', IN_DP)
        Tool.Set_Output('OUT_VPSAT', OUT_VPSAT)
        Tool.Set_Output('OUT_VP', OUT_VP)
        Tool.Set_Output('OUT_VPDIF', OUT_VPDIF)
        Tool.Set_Output('OUT_RH', OUT_RH)
        Tool.Set_Output('OUT_SH', OUT_SH)
        Tool.Set_Output('OUT_DP', OUT_DP)
        Tool.Set_Output('OUT_DPDIF', OUT_DPDIF)
        Tool.Set_Option('T_DEFAULT', T_DEFAULT)
        Tool.Set_Option('P_DEFAULT', P_DEFAULT)
        Tool.Set_Option('IN_VP_DEFAULT', IN_VP_DEFAULT)
        Tool.Set_Option('IN_SH_DEFAULT', IN_SH_DEFAULT)
        Tool.Set_Option('IN_RH_DEFAULT', IN_RH_DEFAULT)
        Tool.Set_Option('IN_DP_DEFAULT', IN_DP_DEFAULT)
        Tool.Set_Option('CONVERSION', CONVERSION)
        Tool.Set_Option('VPSAT_METHOD', VPSAT_METHOD)
        return Tool.Execute(Verbose)
    return False

def Lapse_Rate_Based_Temperature_Downscaling(LORES_DEM=None, LORES_T=None, LORES_LAPSE=None, HIRES_DEM=None, LORES_SLT=None, HIRES_T=None, REGRS_SUMMARY=None, LORES_GRID_SYSTEM=None, HIRES_GRID_SYSTEM=None, LAPSE_METHOD=None, REGRS_LAPSE=None, LIMIT_LAPSE=None, CONST_LAPSE=None, Verbose=2):
    '''
    Lapse Rate Based Temperature Downscaling
    ----------
    [climate_tools.30]\n
    The Lapse Rate Based Temperature Downscaling is quite simple, but might perform well for mountainous regions, where the altitudinal gradient is the main driver for local temperature variation. First, a given lapse rate is used to estimate sea level temperatures from elevation and temperature data at a coarse resolution. Second, the same lapse rate is used to estimate the terrain surface temperature using higher resoluted elevation data and the spline interpolated sea level temperatures from the previous step. The lapse rates can be defined as one constant value valid for the whole area of interest, or as varying value as defined by an additional input grid. Alternatively a constant lapse rate can be estimated from the coarse resolution input with a regression analysis.\n
    Arguments
    ----------
    - LORES_DEM [`input grid`] : Elevation
    - LORES_T [`input grid`] : Temperature
    - LORES_LAPSE [`input grid`] : Lapse Rate
    - HIRES_DEM [`input grid`] : Elevation
    - LORES_SLT [`output grid`] : Sea Level Temperature
    - HIRES_T [`output grid`] : Temperature
    - REGRS_SUMMARY [`output table`] : Regression Summary
    - LORES_GRID_SYSTEM [`grid system`] : Coarse Resolution
    - HIRES_GRID_SYSTEM [`grid system`] : High Resolution
    - LAPSE_METHOD [`choice`] : Lapse Rate. Available Choices: [0] constant lapse rate [1] constant lapse rate from regression [2] varying lapse rate from grid Default: 1
    - REGRS_LAPSE [`choice`] : Regression. Available Choices: [0] elevation [1] elevation and position [2] elevation and position (2nd order polynom) Default: 2
    - LIMIT_LAPSE [`boolean`] : Limit Minimum Lapse Rate. Default: 0 If set, lapse rates from regression are limited to a minimum as specified by the constant lapse rate parameter.
    - CONST_LAPSE [`floating point number`] : Constant Lapse Rate. Default: 0.600000 Constant lapse rate in degree of temperature per 100 meter.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('climate_tools', '30', 'Lapse Rate Based Temperature Downscaling')
    if Tool.is_Okay():
        Tool.Set_Input ('LORES_DEM', LORES_DEM)
        Tool.Set_Input ('LORES_T', LORES_T)
        Tool.Set_Input ('LORES_LAPSE', LORES_LAPSE)
        Tool.Set_Input ('HIRES_DEM', HIRES_DEM)
        Tool.Set_Output('LORES_SLT', LORES_SLT)
        Tool.Set_Output('HIRES_T', HIRES_T)
        Tool.Set_Output('REGRS_SUMMARY', REGRS_SUMMARY)
        Tool.Set_Option('LORES_GRID_SYSTEM', LORES_GRID_SYSTEM)
        Tool.Set_Option('HIRES_GRID_SYSTEM', HIRES_GRID_SYSTEM)
        Tool.Set_Option('LAPSE_METHOD', LAPSE_METHOD)
        Tool.Set_Option('REGRS_LAPSE', REGRS_LAPSE)
        Tool.Set_Option('LIMIT_LAPSE', LIMIT_LAPSE)
        Tool.Set_Option('CONST_LAPSE', CONST_LAPSE)
        return Tool.Execute(Verbose)
    return False

def run_tool_climate_tools_30(LORES_DEM=None, LORES_T=None, LORES_LAPSE=None, HIRES_DEM=None, LORES_SLT=None, HIRES_T=None, REGRS_SUMMARY=None, LORES_GRID_SYSTEM=None, HIRES_GRID_SYSTEM=None, LAPSE_METHOD=None, REGRS_LAPSE=None, LIMIT_LAPSE=None, CONST_LAPSE=None, Verbose=2):
    '''
    Lapse Rate Based Temperature Downscaling
    ----------
    [climate_tools.30]\n
    The Lapse Rate Based Temperature Downscaling is quite simple, but might perform well for mountainous regions, where the altitudinal gradient is the main driver for local temperature variation. First, a given lapse rate is used to estimate sea level temperatures from elevation and temperature data at a coarse resolution. Second, the same lapse rate is used to estimate the terrain surface temperature using higher resoluted elevation data and the spline interpolated sea level temperatures from the previous step. The lapse rates can be defined as one constant value valid for the whole area of interest, or as varying value as defined by an additional input grid. Alternatively a constant lapse rate can be estimated from the coarse resolution input with a regression analysis.\n
    Arguments
    ----------
    - LORES_DEM [`input grid`] : Elevation
    - LORES_T [`input grid`] : Temperature
    - LORES_LAPSE [`input grid`] : Lapse Rate
    - HIRES_DEM [`input grid`] : Elevation
    - LORES_SLT [`output grid`] : Sea Level Temperature
    - HIRES_T [`output grid`] : Temperature
    - REGRS_SUMMARY [`output table`] : Regression Summary
    - LORES_GRID_SYSTEM [`grid system`] : Coarse Resolution
    - HIRES_GRID_SYSTEM [`grid system`] : High Resolution
    - LAPSE_METHOD [`choice`] : Lapse Rate. Available Choices: [0] constant lapse rate [1] constant lapse rate from regression [2] varying lapse rate from grid Default: 1
    - REGRS_LAPSE [`choice`] : Regression. Available Choices: [0] elevation [1] elevation and position [2] elevation and position (2nd order polynom) Default: 2
    - LIMIT_LAPSE [`boolean`] : Limit Minimum Lapse Rate. Default: 0 If set, lapse rates from regression are limited to a minimum as specified by the constant lapse rate parameter.
    - CONST_LAPSE [`floating point number`] : Constant Lapse Rate. Default: 0.600000 Constant lapse rate in degree of temperature per 100 meter.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('climate_tools', '30', 'Lapse Rate Based Temperature Downscaling')
    if Tool.is_Okay():
        Tool.Set_Input ('LORES_DEM', LORES_DEM)
        Tool.Set_Input ('LORES_T', LORES_T)
        Tool.Set_Input ('LORES_LAPSE', LORES_LAPSE)
        Tool.Set_Input ('HIRES_DEM', HIRES_DEM)
        Tool.Set_Output('LORES_SLT', LORES_SLT)
        Tool.Set_Output('HIRES_T', HIRES_T)
        Tool.Set_Output('REGRS_SUMMARY', REGRS_SUMMARY)
        Tool.Set_Option('LORES_GRID_SYSTEM', LORES_GRID_SYSTEM)
        Tool.Set_Option('HIRES_GRID_SYSTEM', HIRES_GRID_SYSTEM)
        Tool.Set_Option('LAPSE_METHOD', LAPSE_METHOD)
        Tool.Set_Option('REGRS_LAPSE', REGRS_LAPSE)
        Tool.Set_Option('LIMIT_LAPSE', LIMIT_LAPSE)
        Tool.Set_Option('CONST_LAPSE', CONST_LAPSE)
        return Tool.Execute(Verbose)
    return False

def Daily_Solar_Radiation(LATITUDE=None, SOLARRAD=None, MONTH=None, DAY=None, SUNSHINE=None, Verbose=2):
    '''
    Daily Solar Radiation
    ----------
    [climate_tools.31]\n
    This tool calculates the daily solar radiation (Rg) based on the date and the latitudinal position for incoming top of atmosphere radiation (R0) estimation and the sunshine duration (Sd) provided as percentage of its potential maximum (S0). It uses a simple empiric formula:\n
    Rg = R0 * (0.19 + 0.55 * Sd/S0)\n
    Arguments
    ----------
    - LATITUDE [`input grid`] : Latitude. [Degree]
    - SOLARRAD [`output grid`] : Solar Radiation. [J/cm²]
    - MONTH [`choice`] : Month. Available Choices: [0] January [1] February [2] March [3] April [4] May [5] June [6] July [7] August [8] September [9] October [10] November [11] December Default: 0
    - DAY [`integer number`] : Day of Month. Minimum: 1 Maximum: 31 Default: 20
    - SUNSHINE [`floating point number`] : Sunshine Duration. Minimum: 0.000000 Maximum: 100.000000 Default: 50.000000 Daily sunshine duration as percentage of its potential maximum.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('climate_tools', '31', 'Daily Solar Radiation')
    if Tool.is_Okay():
        Tool.Set_Input ('LATITUDE', LATITUDE)
        Tool.Set_Output('SOLARRAD', SOLARRAD)
        Tool.Set_Option('MONTH', MONTH)
        Tool.Set_Option('DAY', DAY)
        Tool.Set_Option('SUNSHINE', SUNSHINE)
        return Tool.Execute(Verbose)
    return False

def run_tool_climate_tools_31(LATITUDE=None, SOLARRAD=None, MONTH=None, DAY=None, SUNSHINE=None, Verbose=2):
    '''
    Daily Solar Radiation
    ----------
    [climate_tools.31]\n
    This tool calculates the daily solar radiation (Rg) based on the date and the latitudinal position for incoming top of atmosphere radiation (R0) estimation and the sunshine duration (Sd) provided as percentage of its potential maximum (S0). It uses a simple empiric formula:\n
    Rg = R0 * (0.19 + 0.55 * Sd/S0)\n
    Arguments
    ----------
    - LATITUDE [`input grid`] : Latitude. [Degree]
    - SOLARRAD [`output grid`] : Solar Radiation. [J/cm²]
    - MONTH [`choice`] : Month. Available Choices: [0] January [1] February [2] March [3] April [4] May [5] June [6] July [7] August [8] September [9] October [10] November [11] December Default: 0
    - DAY [`integer number`] : Day of Month. Minimum: 1 Maximum: 31 Default: 20
    - SUNSHINE [`floating point number`] : Sunshine Duration. Minimum: 0.000000 Maximum: 100.000000 Default: 50.000000 Daily sunshine duration as percentage of its potential maximum.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('climate_tools', '31', 'Daily Solar Radiation')
    if Tool.is_Okay():
        Tool.Set_Input ('LATITUDE', LATITUDE)
        Tool.Set_Output('SOLARRAD', SOLARRAD)
        Tool.Set_Option('MONTH', MONTH)
        Tool.Set_Option('DAY', DAY)
        Tool.Set_Option('SUNSHINE', SUNSHINE)
        return Tool.Execute(Verbose)
    return False

def Lapse_Rate_Based_Temperature_Interpolation(POINTS=None, DEM=None, TEMPERATURE=None, SLT=None, REGRS_SUMMARY=None, FIELD_T=None, FIELD_Z=None, INTERPOLATION=None, IDW_POWER=None, LAPSE_METHOD=None, REGRS_LAPSE=None, LIMIT_LAPSE=None, CONST_LAPSE=None, Verbose=2):
    '''
    Lapse Rate Based Temperature Interpolation
    ----------
    [climate_tools.32]\n
    The Lapse Rate Based Temperature Interpolation is quite simple, but might perform well for mountainous regions, where the altitudinal gradient is the main driver for local temperature variation. First, a given lapse rate is used to estimate sea level temperatures from elevation and temperature data at a coarse resolution. Second, the same lapse rate is used to estimate the terrain surface temperature using higher resoluted elevation data and the spline interpolated sea level temperatures from the previous step. The lapse rates can be defined as one constant value valid for the whole area of interest, or as varying value as defined by an additional input grid. Alternatively a constant lapse rate can be estimated from the coarse resolution input with a regression analysis.\n
    Arguments
    ----------
    - POINTS [`input shapes`] : Observations
    - DEM [`input grid`] : Elevation
    - TEMPERATURE [`output grid`] : Temperature
    - SLT [`output grid`] : Sea Level Temperature
    - REGRS_SUMMARY [`output table`] : Regression Summary
    - FIELD_T [`table field`] : Temperature
    - FIELD_Z [`table field`] : Elevation
    - INTERPOLATION [`choice`] : Interpolation. Available Choices: [0] Multilevel B-Spline Interpolation [1] Inverse Distance Weighted Default: 0
    - IDW_POWER [`floating point number`] : Power. Default: 2.000000
    - LAPSE_METHOD [`choice`] : Lapse Rate. Available Choices: [0] user defined lapse rate [1] lapse rate from regression Default: 1
    - REGRS_LAPSE [`choice`] : Regression. Available Choices: [0] elevation [1] elevation and position [2] elevation and position (2nd order polynom) Default: 1
    - LIMIT_LAPSE [`boolean`] : Limit Minimum Lapse Rate. Default: 0 If set, lapse rates from regression are limited to a minimum as specified by the constant lapse rate parameter.
    - CONST_LAPSE [`floating point number`] : Constant Lapse Rate. Default: 0.600000 Constant lapse rate in degree of temperature per 100 meter.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('climate_tools', '32', 'Lapse Rate Based Temperature Interpolation')
    if Tool.is_Okay():
        Tool.Set_Input ('POINTS', POINTS)
        Tool.Set_Input ('DEM', DEM)
        Tool.Set_Output('TEMPERATURE', TEMPERATURE)
        Tool.Set_Output('SLT', SLT)
        Tool.Set_Output('REGRS_SUMMARY', REGRS_SUMMARY)
        Tool.Set_Option('FIELD_T', FIELD_T)
        Tool.Set_Option('FIELD_Z', FIELD_Z)
        Tool.Set_Option('INTERPOLATION', INTERPOLATION)
        Tool.Set_Option('IDW_POWER', IDW_POWER)
        Tool.Set_Option('LAPSE_METHOD', LAPSE_METHOD)
        Tool.Set_Option('REGRS_LAPSE', REGRS_LAPSE)
        Tool.Set_Option('LIMIT_LAPSE', LIMIT_LAPSE)
        Tool.Set_Option('CONST_LAPSE', CONST_LAPSE)
        return Tool.Execute(Verbose)
    return False

def run_tool_climate_tools_32(POINTS=None, DEM=None, TEMPERATURE=None, SLT=None, REGRS_SUMMARY=None, FIELD_T=None, FIELD_Z=None, INTERPOLATION=None, IDW_POWER=None, LAPSE_METHOD=None, REGRS_LAPSE=None, LIMIT_LAPSE=None, CONST_LAPSE=None, Verbose=2):
    '''
    Lapse Rate Based Temperature Interpolation
    ----------
    [climate_tools.32]\n
    The Lapse Rate Based Temperature Interpolation is quite simple, but might perform well for mountainous regions, where the altitudinal gradient is the main driver for local temperature variation. First, a given lapse rate is used to estimate sea level temperatures from elevation and temperature data at a coarse resolution. Second, the same lapse rate is used to estimate the terrain surface temperature using higher resoluted elevation data and the spline interpolated sea level temperatures from the previous step. The lapse rates can be defined as one constant value valid for the whole area of interest, or as varying value as defined by an additional input grid. Alternatively a constant lapse rate can be estimated from the coarse resolution input with a regression analysis.\n
    Arguments
    ----------
    - POINTS [`input shapes`] : Observations
    - DEM [`input grid`] : Elevation
    - TEMPERATURE [`output grid`] : Temperature
    - SLT [`output grid`] : Sea Level Temperature
    - REGRS_SUMMARY [`output table`] : Regression Summary
    - FIELD_T [`table field`] : Temperature
    - FIELD_Z [`table field`] : Elevation
    - INTERPOLATION [`choice`] : Interpolation. Available Choices: [0] Multilevel B-Spline Interpolation [1] Inverse Distance Weighted Default: 0
    - IDW_POWER [`floating point number`] : Power. Default: 2.000000
    - LAPSE_METHOD [`choice`] : Lapse Rate. Available Choices: [0] user defined lapse rate [1] lapse rate from regression Default: 1
    - REGRS_LAPSE [`choice`] : Regression. Available Choices: [0] elevation [1] elevation and position [2] elevation and position (2nd order polynom) Default: 1
    - LIMIT_LAPSE [`boolean`] : Limit Minimum Lapse Rate. Default: 0 If set, lapse rates from regression are limited to a minimum as specified by the constant lapse rate parameter.
    - CONST_LAPSE [`floating point number`] : Constant Lapse Rate. Default: 0.600000 Constant lapse rate in degree of temperature per 100 meter.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('climate_tools', '32', 'Lapse Rate Based Temperature Interpolation')
    if Tool.is_Okay():
        Tool.Set_Input ('POINTS', POINTS)
        Tool.Set_Input ('DEM', DEM)
        Tool.Set_Output('TEMPERATURE', TEMPERATURE)
        Tool.Set_Output('SLT', SLT)
        Tool.Set_Output('REGRS_SUMMARY', REGRS_SUMMARY)
        Tool.Set_Option('FIELD_T', FIELD_T)
        Tool.Set_Option('FIELD_Z', FIELD_Z)
        Tool.Set_Option('INTERPOLATION', INTERPOLATION)
        Tool.Set_Option('IDW_POWER', IDW_POWER)
        Tool.Set_Option('LAPSE_METHOD', LAPSE_METHOD)
        Tool.Set_Option('REGRS_LAPSE', REGRS_LAPSE)
        Tool.Set_Option('LIMIT_LAPSE', LIMIT_LAPSE)
        Tool.Set_Option('CONST_LAPSE', CONST_LAPSE)
        return Tool.Execute(Verbose)
    return False

def Solar_Position(TABLE=None, LATITUDE=None, DATE_FROM=None, DATE_TO=None, DATE_STEP=None, TIME_FROM=None, TIME_TO=None, TIME_STEP=None, Verbose=2):
    '''
    Solar Position
    ----------
    [climate_tools.33]\n
    This tool calculates the solar position for requested latitude, date range and daily time range.\n
    Arguments
    ----------
    - TABLE [`output table`] : Solar Position
    - LATITUDE [`degree`] : Latitude. Minimum: -90.000000 Maximum: 90.000000 Default: 53.000000
    - DATE_FROM [`date`] : From. Default: 2000-01-01
    - DATE_TO [`date`] : To. Default: 2000-12-31
    - DATE_STEP [`integer number`] : Step. Minimum: 0 Default: 5
    - TIME_FROM [`floating point number`] : From. Default: 1.000000
    - TIME_TO [`floating point number`] : To. Default: 24.000000
    - TIME_STEP [`floating point number`] : Step. Minimum: 0.000000 Default: 1.000000

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('climate_tools', '33', 'Solar Position')
    if Tool.is_Okay():
        Tool.Set_Output('TABLE', TABLE)
        Tool.Set_Option('LATITUDE', LATITUDE)
        Tool.Set_Option('DATE_FROM', DATE_FROM)
        Tool.Set_Option('DATE_TO', DATE_TO)
        Tool.Set_Option('DATE_STEP', DATE_STEP)
        Tool.Set_Option('TIME_FROM', TIME_FROM)
        Tool.Set_Option('TIME_TO', TIME_TO)
        Tool.Set_Option('TIME_STEP', TIME_STEP)
        return Tool.Execute(Verbose)
    return False

def run_tool_climate_tools_33(TABLE=None, LATITUDE=None, DATE_FROM=None, DATE_TO=None, DATE_STEP=None, TIME_FROM=None, TIME_TO=None, TIME_STEP=None, Verbose=2):
    '''
    Solar Position
    ----------
    [climate_tools.33]\n
    This tool calculates the solar position for requested latitude, date range and daily time range.\n
    Arguments
    ----------
    - TABLE [`output table`] : Solar Position
    - LATITUDE [`degree`] : Latitude. Minimum: -90.000000 Maximum: 90.000000 Default: 53.000000
    - DATE_FROM [`date`] : From. Default: 2000-01-01
    - DATE_TO [`date`] : To. Default: 2000-12-31
    - DATE_STEP [`integer number`] : Step. Minimum: 0 Default: 5
    - TIME_FROM [`floating point number`] : From. Default: 1.000000
    - TIME_TO [`floating point number`] : To. Default: 24.000000
    - TIME_STEP [`floating point number`] : Step. Minimum: 0.000000 Default: 1.000000

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('climate_tools', '33', 'Solar Position')
    if Tool.is_Okay():
        Tool.Set_Output('TABLE', TABLE)
        Tool.Set_Option('LATITUDE', LATITUDE)
        Tool.Set_Option('DATE_FROM', DATE_FROM)
        Tool.Set_Option('DATE_TO', DATE_TO)
        Tool.Set_Option('DATE_STEP', DATE_STEP)
        Tool.Set_Option('TIME_FROM', TIME_FROM)
        Tool.Set_Option('TIME_TO', TIME_TO)
        Tool.Set_Option('TIME_STEP', TIME_STEP)
        return Tool.Execute(Verbose)
    return False

def Lapse_Rate_Based_Temperature_Downscaling_Bulk_Processing(LORES_DEM=None, LORES_LAPSE=None, LORES_T=None, HIRES_DEM=None, HIRES_T=None, LORES_GRID_SYSTEM=None, HIRES_GRID_SYSTEM=None, LAPSE_METHOD=None, CONST_LAPSE=None, REGRS_LAPSE=None, LIMIT_LAPSE=None, MINIM_LAPSE=None, Verbose=2):
    '''
    Lapse Rate Based Temperature Downscaling (Bulk Processing)
    ----------
    [climate_tools.temperature_downscaling]\n
    The 'Lapse Rate Based Temperature Downscaling' is quite simple, but might perform well for mountainous regions, where the altitudinal gradient is the main driver for local temperature variation. First, a given lapse rate is used to estimate sea level temperatures from elevation and temperature data at a coarse resolution. Second, the same lapse rate is used to estimate the terrain surface temperature using higher resoluted elevation data and the spline interpolated sea level temperatures from the previous step.\n
    \n
    Arguments
    ----------
    - LORES_DEM [`input grid`] : Elevation
    - LORES_LAPSE [`input grid`] : Lapse Rate
    - LORES_T [`input grid list`] : Temperature
    - HIRES_DEM [`input grid`] : Elevation
    - HIRES_T [`output grid list`] : Temperature
    - LORES_GRID_SYSTEM [`grid system`] : Coarse Resolution
    - HIRES_GRID_SYSTEM [`grid system`] : High Resolution
    - LAPSE_METHOD [`choice`] : Lapse Rate. Available Choices: [0] constant lapse rate [1] constant lapse rate from regression [2] varying lapse rate from grid Default: 0
    - CONST_LAPSE [`floating point number`] : Constant or Minimum Lapse Rate. Minimum: 0.000000 Default: 0.600000 constant or minimum limit lapse rate in degree of temperature per 100 meter.
    - REGRS_LAPSE [`choice`] : Regression. Available Choices: [0] elevation [1] elevation and position [2] elevation and position (2nd order polynom) Default: 0
    - LIMIT_LAPSE [`boolean`] : Limit Minimum Lapse Rate. Default: 0 If set, lapse rates from regression are limited to a minimum.
    - MINIM_LAPSE [`floating point number`] : Minimum Lapse Rate. Minimum: 0.000000 Default: 0.200000 minimum lapse rate in degree of temperature per 100 meter.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('climate_tools', 'temperature_downscaling', 'Lapse Rate Based Temperature Downscaling (Bulk Processing)')
    if Tool.is_Okay():
        Tool.Set_Input ('LORES_DEM', LORES_DEM)
        Tool.Set_Input ('LORES_LAPSE', LORES_LAPSE)
        Tool.Set_Input ('LORES_T', LORES_T)
        Tool.Set_Input ('HIRES_DEM', HIRES_DEM)
        Tool.Set_Output('HIRES_T', HIRES_T)
        Tool.Set_Option('LORES_GRID_SYSTEM', LORES_GRID_SYSTEM)
        Tool.Set_Option('HIRES_GRID_SYSTEM', HIRES_GRID_SYSTEM)
        Tool.Set_Option('LAPSE_METHOD', LAPSE_METHOD)
        Tool.Set_Option('CONST_LAPSE', CONST_LAPSE)
        Tool.Set_Option('REGRS_LAPSE', REGRS_LAPSE)
        Tool.Set_Option('LIMIT_LAPSE', LIMIT_LAPSE)
        Tool.Set_Option('MINIM_LAPSE', MINIM_LAPSE)
        return Tool.Execute(Verbose)
    return False

def run_tool_climate_tools_temperature_downscaling(LORES_DEM=None, LORES_LAPSE=None, LORES_T=None, HIRES_DEM=None, HIRES_T=None, LORES_GRID_SYSTEM=None, HIRES_GRID_SYSTEM=None, LAPSE_METHOD=None, CONST_LAPSE=None, REGRS_LAPSE=None, LIMIT_LAPSE=None, MINIM_LAPSE=None, Verbose=2):
    '''
    Lapse Rate Based Temperature Downscaling (Bulk Processing)
    ----------
    [climate_tools.temperature_downscaling]\n
    The 'Lapse Rate Based Temperature Downscaling' is quite simple, but might perform well for mountainous regions, where the altitudinal gradient is the main driver for local temperature variation. First, a given lapse rate is used to estimate sea level temperatures from elevation and temperature data at a coarse resolution. Second, the same lapse rate is used to estimate the terrain surface temperature using higher resoluted elevation data and the spline interpolated sea level temperatures from the previous step.\n
    \n
    Arguments
    ----------
    - LORES_DEM [`input grid`] : Elevation
    - LORES_LAPSE [`input grid`] : Lapse Rate
    - LORES_T [`input grid list`] : Temperature
    - HIRES_DEM [`input grid`] : Elevation
    - HIRES_T [`output grid list`] : Temperature
    - LORES_GRID_SYSTEM [`grid system`] : Coarse Resolution
    - HIRES_GRID_SYSTEM [`grid system`] : High Resolution
    - LAPSE_METHOD [`choice`] : Lapse Rate. Available Choices: [0] constant lapse rate [1] constant lapse rate from regression [2] varying lapse rate from grid Default: 0
    - CONST_LAPSE [`floating point number`] : Constant or Minimum Lapse Rate. Minimum: 0.000000 Default: 0.600000 constant or minimum limit lapse rate in degree of temperature per 100 meter.
    - REGRS_LAPSE [`choice`] : Regression. Available Choices: [0] elevation [1] elevation and position [2] elevation and position (2nd order polynom) Default: 0
    - LIMIT_LAPSE [`boolean`] : Limit Minimum Lapse Rate. Default: 0 If set, lapse rates from regression are limited to a minimum.
    - MINIM_LAPSE [`floating point number`] : Minimum Lapse Rate. Minimum: 0.000000 Default: 0.200000 minimum lapse rate in degree of temperature per 100 meter.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('climate_tools', 'temperature_downscaling', 'Lapse Rate Based Temperature Downscaling (Bulk Processing)')
    if Tool.is_Okay():
        Tool.Set_Input ('LORES_DEM', LORES_DEM)
        Tool.Set_Input ('LORES_LAPSE', LORES_LAPSE)
        Tool.Set_Input ('LORES_T', LORES_T)
        Tool.Set_Input ('HIRES_DEM', HIRES_DEM)
        Tool.Set_Output('HIRES_T', HIRES_T)
        Tool.Set_Option('LORES_GRID_SYSTEM', LORES_GRID_SYSTEM)
        Tool.Set_Option('HIRES_GRID_SYSTEM', HIRES_GRID_SYSTEM)
        Tool.Set_Option('LAPSE_METHOD', LAPSE_METHOD)
        Tool.Set_Option('CONST_LAPSE', CONST_LAPSE)
        Tool.Set_Option('REGRS_LAPSE', REGRS_LAPSE)
        Tool.Set_Option('LIMIT_LAPSE', LIMIT_LAPSE)
        Tool.Set_Option('MINIM_LAPSE', MINIM_LAPSE)
        return Tool.Execute(Verbose)
    return False

