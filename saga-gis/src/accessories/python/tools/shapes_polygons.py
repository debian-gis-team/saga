#! /usr/bin/env python

'''
Python Interface to SAGA Tools Library
----------
- Category : Shapes
- Name     : Polygons
- ID       : shapes_polygons

Description
----------
Tools for polygons.
'''

from PySAGA.helper import Tool_Wrapper

def Polygon_Centroids(POLYGONS=None, CENTROIDS=None, METHOD=None, INSIDE=None, Verbose=2):
    '''
    Polygon Centroids
    ----------
    [shapes_polygons.1]\n
    Creates a points layer containing the centroids of the input polygon layer.\n
    Arguments
    ----------
    - POLYGONS [`input shapes`] : Polygons
    - CENTROIDS [`output shapes`] : Centroids
    - METHOD [`boolean`] : Centroids for each part. Default: 0
    - INSIDE [`boolean`] : Force Inside. Default: 0 If a centroid falls outside its polygon, then move it to the closest boundary.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_polygons', '1', 'Polygon Centroids')
    if Tool.is_Okay():
        Tool.Set_Input ('POLYGONS', POLYGONS)
        Tool.Set_Output('CENTROIDS', CENTROIDS)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('INSIDE', INSIDE)
        return Tool.Execute(Verbose)
    return False

def run_tool_shapes_polygons_1(POLYGONS=None, CENTROIDS=None, METHOD=None, INSIDE=None, Verbose=2):
    '''
    Polygon Centroids
    ----------
    [shapes_polygons.1]\n
    Creates a points layer containing the centroids of the input polygon layer.\n
    Arguments
    ----------
    - POLYGONS [`input shapes`] : Polygons
    - CENTROIDS [`output shapes`] : Centroids
    - METHOD [`boolean`] : Centroids for each part. Default: 0
    - INSIDE [`boolean`] : Force Inside. Default: 0 If a centroid falls outside its polygon, then move it to the closest boundary.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_polygons', '1', 'Polygon Centroids')
    if Tool.is_Okay():
        Tool.Set_Input ('POLYGONS', POLYGONS)
        Tool.Set_Output('CENTROIDS', CENTROIDS)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('INSIDE', INSIDE)
        return Tool.Execute(Verbose)
    return False

def Polygon_Properties(POLYGONS=None, OUTPUT=None, FIELDS=None, BPARTS=None, BPOINTS=None, BEXTENT=None, BCENTER=None, BLENGTH=None, BAREA=None, SCALING=None, Verbose=2):
    '''
    Polygon Properties
    ----------
    [shapes_polygons.2]\n
    Add general and geometric properties of polygons to its attributes.\n
    Arguments
    ----------
    - POLYGONS [`input shapes`] : Polygons
    - OUTPUT [`output shapes`] : Polygons with Property Attributes. If not set property attributes will be added to the original layer.
    - FIELDS [`table fields`] : Copy Attributes. Select one or more attributes to be copied to the target layer.
    - BPARTS [`boolean`] : Number of Parts. Default: 0
    - BPOINTS [`boolean`] : Number of Vertices. Default: 0
    - BEXTENT [`boolean`] : Extent. Default: 0
    - BCENTER [`boolean`] : Centroid. Default: 0
    - BLENGTH [`boolean`] : Perimeter. Default: 1
    - BAREA [`boolean`] : Area. Default: 1
    - SCALING [`floating point number`] : Scaling. Minimum: 0.000000 Default: 1.000000 Scaling factor for perimeter and area (squared). meter to feet = 1 / 0.3048 = 3.2808

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_polygons', '2', 'Polygon Properties')
    if Tool.is_Okay():
        Tool.Set_Input ('POLYGONS', POLYGONS)
        Tool.Set_Output('OUTPUT', OUTPUT)
        Tool.Set_Option('FIELDS', FIELDS)
        Tool.Set_Option('BPARTS', BPARTS)
        Tool.Set_Option('BPOINTS', BPOINTS)
        Tool.Set_Option('BEXTENT', BEXTENT)
        Tool.Set_Option('BCENTER', BCENTER)
        Tool.Set_Option('BLENGTH', BLENGTH)
        Tool.Set_Option('BAREA', BAREA)
        Tool.Set_Option('SCALING', SCALING)
        return Tool.Execute(Verbose)
    return False

def run_tool_shapes_polygons_2(POLYGONS=None, OUTPUT=None, FIELDS=None, BPARTS=None, BPOINTS=None, BEXTENT=None, BCENTER=None, BLENGTH=None, BAREA=None, SCALING=None, Verbose=2):
    '''
    Polygon Properties
    ----------
    [shapes_polygons.2]\n
    Add general and geometric properties of polygons to its attributes.\n
    Arguments
    ----------
    - POLYGONS [`input shapes`] : Polygons
    - OUTPUT [`output shapes`] : Polygons with Property Attributes. If not set property attributes will be added to the original layer.
    - FIELDS [`table fields`] : Copy Attributes. Select one or more attributes to be copied to the target layer.
    - BPARTS [`boolean`] : Number of Parts. Default: 0
    - BPOINTS [`boolean`] : Number of Vertices. Default: 0
    - BEXTENT [`boolean`] : Extent. Default: 0
    - BCENTER [`boolean`] : Centroid. Default: 0
    - BLENGTH [`boolean`] : Perimeter. Default: 1
    - BAREA [`boolean`] : Area. Default: 1
    - SCALING [`floating point number`] : Scaling. Minimum: 0.000000 Default: 1.000000 Scaling factor for perimeter and area (squared). meter to feet = 1 / 0.3048 = 3.2808

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_polygons', '2', 'Polygon Properties')
    if Tool.is_Okay():
        Tool.Set_Input ('POLYGONS', POLYGONS)
        Tool.Set_Output('OUTPUT', OUTPUT)
        Tool.Set_Option('FIELDS', FIELDS)
        Tool.Set_Option('BPARTS', BPARTS)
        Tool.Set_Option('BPOINTS', BPOINTS)
        Tool.Set_Option('BEXTENT', BEXTENT)
        Tool.Set_Option('BCENTER', BCENTER)
        Tool.Set_Option('BLENGTH', BLENGTH)
        Tool.Set_Option('BAREA', BAREA)
        Tool.Set_Option('SCALING', SCALING)
        return Tool.Execute(Verbose)
    return False

def Convert_Lines_to_Polygons(LINES=None, POLYGONS=None, RINGS=None, SINGLE=None, MERGE=None, SPLIT=None, Verbose=2):
    '''
    Convert Lines to Polygons
    ----------
    [shapes_polygons.3]\n
    Converts lines to polygons. Line arcs are closed to polygons simply by connecting the last point with the first. Optionally single parts of polylines can be merged into one polygon part if these share start/end vertices.\n
    Arguments
    ----------
    - LINES [`input shapes`] : Lines
    - POLYGONS [`output shapes`] : Polygons
    - RINGS [`boolean`] : Rings. Default: 0 Only convert closed rings, i.e. first and last line vertex must be identical.
    - SINGLE [`boolean`] : Create One Single Multipart Polygon. Default: 0
    - MERGE [`boolean`] : Merge Connected Line Parts. Default: 0 Treat single polyline parts as one line if connected, i.e. parts share end/start vertices.
    - SPLIT [`boolean`] : Line Parts as Individual Polygons. Default: 0 If checked polyline parts become individual polygons. Applies only if single multipart polygon output is unchecked.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_polygons', '3', 'Convert Lines to Polygons')
    if Tool.is_Okay():
        Tool.Set_Input ('LINES', LINES)
        Tool.Set_Output('POLYGONS', POLYGONS)
        Tool.Set_Option('RINGS', RINGS)
        Tool.Set_Option('SINGLE', SINGLE)
        Tool.Set_Option('MERGE', MERGE)
        Tool.Set_Option('SPLIT', SPLIT)
        return Tool.Execute(Verbose)
    return False

def run_tool_shapes_polygons_3(LINES=None, POLYGONS=None, RINGS=None, SINGLE=None, MERGE=None, SPLIT=None, Verbose=2):
    '''
    Convert Lines to Polygons
    ----------
    [shapes_polygons.3]\n
    Converts lines to polygons. Line arcs are closed to polygons simply by connecting the last point with the first. Optionally single parts of polylines can be merged into one polygon part if these share start/end vertices.\n
    Arguments
    ----------
    - LINES [`input shapes`] : Lines
    - POLYGONS [`output shapes`] : Polygons
    - RINGS [`boolean`] : Rings. Default: 0 Only convert closed rings, i.e. first and last line vertex must be identical.
    - SINGLE [`boolean`] : Create One Single Multipart Polygon. Default: 0
    - MERGE [`boolean`] : Merge Connected Line Parts. Default: 0 Treat single polyline parts as one line if connected, i.e. parts share end/start vertices.
    - SPLIT [`boolean`] : Line Parts as Individual Polygons. Default: 0 If checked polyline parts become individual polygons. Applies only if single multipart polygon output is unchecked.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_polygons', '3', 'Convert Lines to Polygons')
    if Tool.is_Okay():
        Tool.Set_Input ('LINES', LINES)
        Tool.Set_Output('POLYGONS', POLYGONS)
        Tool.Set_Option('RINGS', RINGS)
        Tool.Set_Option('SINGLE', SINGLE)
        Tool.Set_Option('MERGE', MERGE)
        Tool.Set_Option('SPLIT', SPLIT)
        return Tool.Execute(Verbose)
    return False

def Point_Statistics_for_Polygons(POINTS=None, POLYGONS=None, STATISTICS=None, FIELDS=None, SUM=None, AVG=None, VAR=None, DEV=None, MIN=None, MAX=None, NUM=None, FIELD_NAME=None, Verbose=2):
    '''
    Point Statistics for Polygons
    ----------
    [shapes_polygons.4]\n
    Calculates statistics over all points falling in a polygon.\n
    Arguments
    ----------
    - POINTS [`input shapes`] : Points
    - POLYGONS [`input shapes`] : Polygons
    - STATISTICS [`output shapes`] : Statistics
    - FIELDS [`table fields`] : Attributes
    - SUM [`boolean`] : Sum. Default: 0
    - AVG [`boolean`] : Mean. Default: 1
    - VAR [`boolean`] : Variance. Default: 0
    - DEV [`boolean`] : Deviation. Default: 0
    - MIN [`boolean`] : Minimum. Default: 0
    - MAX [`boolean`] : Maximum. Default: 0
    - NUM [`boolean`] : Count. Default: 0
    - FIELD_NAME [`choice`] : Field Naming. Available Choices: [0] variable type + original name [1] original name + variable type [2] original name [3] variable type Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_polygons', '4', 'Point Statistics for Polygons')
    if Tool.is_Okay():
        Tool.Set_Input ('POINTS', POINTS)
        Tool.Set_Input ('POLYGONS', POLYGONS)
        Tool.Set_Output('STATISTICS', STATISTICS)
        Tool.Set_Option('FIELDS', FIELDS)
        Tool.Set_Option('SUM', SUM)
        Tool.Set_Option('AVG', AVG)
        Tool.Set_Option('VAR', VAR)
        Tool.Set_Option('DEV', DEV)
        Tool.Set_Option('MIN', MIN)
        Tool.Set_Option('MAX', MAX)
        Tool.Set_Option('NUM', NUM)
        Tool.Set_Option('FIELD_NAME', FIELD_NAME)
        return Tool.Execute(Verbose)
    return False

def run_tool_shapes_polygons_4(POINTS=None, POLYGONS=None, STATISTICS=None, FIELDS=None, SUM=None, AVG=None, VAR=None, DEV=None, MIN=None, MAX=None, NUM=None, FIELD_NAME=None, Verbose=2):
    '''
    Point Statistics for Polygons
    ----------
    [shapes_polygons.4]\n
    Calculates statistics over all points falling in a polygon.\n
    Arguments
    ----------
    - POINTS [`input shapes`] : Points
    - POLYGONS [`input shapes`] : Polygons
    - STATISTICS [`output shapes`] : Statistics
    - FIELDS [`table fields`] : Attributes
    - SUM [`boolean`] : Sum. Default: 0
    - AVG [`boolean`] : Mean. Default: 1
    - VAR [`boolean`] : Variance. Default: 0
    - DEV [`boolean`] : Deviation. Default: 0
    - MIN [`boolean`] : Minimum. Default: 0
    - MAX [`boolean`] : Maximum. Default: 0
    - NUM [`boolean`] : Count. Default: 0
    - FIELD_NAME [`choice`] : Field Naming. Available Choices: [0] variable type + original name [1] original name + variable type [2] original name [3] variable type Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_polygons', '4', 'Point Statistics for Polygons')
    if Tool.is_Okay():
        Tool.Set_Input ('POINTS', POINTS)
        Tool.Set_Input ('POLYGONS', POLYGONS)
        Tool.Set_Output('STATISTICS', STATISTICS)
        Tool.Set_Option('FIELDS', FIELDS)
        Tool.Set_Option('SUM', SUM)
        Tool.Set_Option('AVG', AVG)
        Tool.Set_Option('VAR', VAR)
        Tool.Set_Option('DEV', DEV)
        Tool.Set_Option('MIN', MIN)
        Tool.Set_Option('MAX', MAX)
        Tool.Set_Option('NUM', NUM)
        Tool.Set_Option('FIELD_NAME', FIELD_NAME)
        return Tool.Execute(Verbose)
    return False

def Polygon_Dissolve(POLYGONS=None, DISSOLVED=None, FIELDS=None, STATISTICS=None, STAT_SUM=None, STAT_AVG=None, STAT_MIN=None, STAT_MAX=None, STAT_RNG=None, STAT_DEV=None, STAT_VAR=None, STAT_LST=None, STAT_NUM=None, STAT_NAMING=None, BND_KEEP=None, MIN_AREA=None, SPLIT_DISTINCT=None, Verbose=2):
    '''
    Polygon Dissolve
    ----------
    [shapes_polygons.5]\n
    Merges polygons, which share the same attribute value, and (optionally) dissolves borders between adjacent polygon parts. If no attribute or combination of attributes is chosen, all polygons will be merged. Uses the free and open source software library [Clipper] created by Angus Johnson.\n
    Arguments
    ----------
    - POLYGONS [`input shapes`] : Polygons
    - DISSOLVED [`output shapes`] : Dissolved Polygons
    - FIELDS [`table fields`] : Dissolve Field(s)
    - STATISTICS [`table fields`] : Statistics Field(s)
    - STAT_SUM [`boolean`] : Sum. Default: 0
    - STAT_AVG [`boolean`] : Mean. Default: 1
    - STAT_MIN [`boolean`] : Minimum. Default: 0
    - STAT_MAX [`boolean`] : Maximum. Default: 0
    - STAT_RNG [`boolean`] : Range. Default: 0
    - STAT_DEV [`boolean`] : Deviation. Default: 0
    - STAT_VAR [`boolean`] : Variance. Default: 0
    - STAT_LST [`boolean`] : Listing. Default: 0
    - STAT_NUM [`boolean`] : Count. Default: 0
    - STAT_NAMING [`choice`] : Field Naming. Available Choices: [0] variable type + original name [1] original name + variable type [2] original name [3] variable type Default: 0
    - BND_KEEP [`boolean`] : Keep Boundaries. Default: 0
    - MIN_AREA [`floating point number`] : Minimum Area. Minimum: 0.000000 Default: 0.000000
    - SPLIT_DISTINCT [`boolean`] : Split Distinct Polygons. Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_polygons', '5', 'Polygon Dissolve')
    if Tool.is_Okay():
        Tool.Set_Input ('POLYGONS', POLYGONS)
        Tool.Set_Output('DISSOLVED', DISSOLVED)
        Tool.Set_Option('FIELDS', FIELDS)
        Tool.Set_Option('STATISTICS', STATISTICS)
        Tool.Set_Option('STAT_SUM', STAT_SUM)
        Tool.Set_Option('STAT_AVG', STAT_AVG)
        Tool.Set_Option('STAT_MIN', STAT_MIN)
        Tool.Set_Option('STAT_MAX', STAT_MAX)
        Tool.Set_Option('STAT_RNG', STAT_RNG)
        Tool.Set_Option('STAT_DEV', STAT_DEV)
        Tool.Set_Option('STAT_VAR', STAT_VAR)
        Tool.Set_Option('STAT_LST', STAT_LST)
        Tool.Set_Option('STAT_NUM', STAT_NUM)
        Tool.Set_Option('STAT_NAMING', STAT_NAMING)
        Tool.Set_Option('BND_KEEP', BND_KEEP)
        Tool.Set_Option('MIN_AREA', MIN_AREA)
        Tool.Set_Option('SPLIT_DISTINCT', SPLIT_DISTINCT)
        return Tool.Execute(Verbose)
    return False

def run_tool_shapes_polygons_5(POLYGONS=None, DISSOLVED=None, FIELDS=None, STATISTICS=None, STAT_SUM=None, STAT_AVG=None, STAT_MIN=None, STAT_MAX=None, STAT_RNG=None, STAT_DEV=None, STAT_VAR=None, STAT_LST=None, STAT_NUM=None, STAT_NAMING=None, BND_KEEP=None, MIN_AREA=None, SPLIT_DISTINCT=None, Verbose=2):
    '''
    Polygon Dissolve
    ----------
    [shapes_polygons.5]\n
    Merges polygons, which share the same attribute value, and (optionally) dissolves borders between adjacent polygon parts. If no attribute or combination of attributes is chosen, all polygons will be merged. Uses the free and open source software library [Clipper] created by Angus Johnson.\n
    Arguments
    ----------
    - POLYGONS [`input shapes`] : Polygons
    - DISSOLVED [`output shapes`] : Dissolved Polygons
    - FIELDS [`table fields`] : Dissolve Field(s)
    - STATISTICS [`table fields`] : Statistics Field(s)
    - STAT_SUM [`boolean`] : Sum. Default: 0
    - STAT_AVG [`boolean`] : Mean. Default: 1
    - STAT_MIN [`boolean`] : Minimum. Default: 0
    - STAT_MAX [`boolean`] : Maximum. Default: 0
    - STAT_RNG [`boolean`] : Range. Default: 0
    - STAT_DEV [`boolean`] : Deviation. Default: 0
    - STAT_VAR [`boolean`] : Variance. Default: 0
    - STAT_LST [`boolean`] : Listing. Default: 0
    - STAT_NUM [`boolean`] : Count. Default: 0
    - STAT_NAMING [`choice`] : Field Naming. Available Choices: [0] variable type + original name [1] original name + variable type [2] original name [3] variable type Default: 0
    - BND_KEEP [`boolean`] : Keep Boundaries. Default: 0
    - MIN_AREA [`floating point number`] : Minimum Area. Minimum: 0.000000 Default: 0.000000
    - SPLIT_DISTINCT [`boolean`] : Split Distinct Polygons. Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_polygons', '5', 'Polygon Dissolve')
    if Tool.is_Okay():
        Tool.Set_Input ('POLYGONS', POLYGONS)
        Tool.Set_Output('DISSOLVED', DISSOLVED)
        Tool.Set_Option('FIELDS', FIELDS)
        Tool.Set_Option('STATISTICS', STATISTICS)
        Tool.Set_Option('STAT_SUM', STAT_SUM)
        Tool.Set_Option('STAT_AVG', STAT_AVG)
        Tool.Set_Option('STAT_MIN', STAT_MIN)
        Tool.Set_Option('STAT_MAX', STAT_MAX)
        Tool.Set_Option('STAT_RNG', STAT_RNG)
        Tool.Set_Option('STAT_DEV', STAT_DEV)
        Tool.Set_Option('STAT_VAR', STAT_VAR)
        Tool.Set_Option('STAT_LST', STAT_LST)
        Tool.Set_Option('STAT_NUM', STAT_NUM)
        Tool.Set_Option('STAT_NAMING', STAT_NAMING)
        Tool.Set_Option('BND_KEEP', BND_KEEP)
        Tool.Set_Option('MIN_AREA', MIN_AREA)
        Tool.Set_Option('SPLIT_DISTINCT', SPLIT_DISTINCT)
        return Tool.Execute(Verbose)
    return False

def Convert_PolygonLine_Vertices_to_Points(SHAPES=None, POINTS=None, Verbose=2):
    '''
    Convert Polygon/Line Vertices to Points
    ----------
    [shapes_polygons.6]\n
    Convert Polygon/Line Vertices to Points\n
    Arguments
    ----------
    - SHAPES [`input shapes`] : Shapes
    - POINTS [`output shapes`] : Points

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_polygons', '6', 'Convert Polygon/Line Vertices to Points')
    if Tool.is_Okay():
        Tool.Set_Input ('SHAPES', SHAPES)
        Tool.Set_Output('POINTS', POINTS)
        return Tool.Execute(Verbose)
    return False

def run_tool_shapes_polygons_6(SHAPES=None, POINTS=None, Verbose=2):
    '''
    Convert Polygon/Line Vertices to Points
    ----------
    [shapes_polygons.6]\n
    Convert Polygon/Line Vertices to Points\n
    Arguments
    ----------
    - SHAPES [`input shapes`] : Shapes
    - POINTS [`output shapes`] : Points

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_polygons', '6', 'Convert Polygon/Line Vertices to Points')
    if Tool.is_Okay():
        Tool.Set_Input ('SHAPES', SHAPES)
        Tool.Set_Output('POINTS', POINTS)
        return Tool.Execute(Verbose)
    return False

def Polygon_Shape_Indices(SHAPES=None, INDEX=None, DMAX=None, GYROS=None, FERET=None, FERET_DIRS=None, Verbose=2):
    '''
    Polygon Shape Indices
    ----------
    [shapes_polygons.7]\n
    The tool calculates various indices describing the shape of polygons, mostly based on area, perimeter and maximum diameter. If the optional output 'Shape Indices' is not created, the tool attaches the attributes to the input dataset. Otherwise a new dataset is created and attributes existing in the input dataset are dropped.\n
    (-) [A] area\n
    (-) [P] perimeter\n
    (-) [P/A] interior edge ratio\n
    (-) [P/sqrt(A)]\n
    (-) [Deqpc] equivalent projected circle diameter (=2*sqrt(A/pi))\n
    (-) [Sphericity] the ratio of the perimeter of the equivalent circle to the real perimeter (=(2*sqrt(A*pi))/P)\n
    (-) [Shape Index] the inverse of the sphericity (=P/(2*sqrt(A*pi)))\n
    (-) [Dmax] maximum diameter calculated as maximum distance between two polygon part's vertices\n
    (-) [DmaxDir] direction of maximum diameter\n
    (-) [Dmax/A]\n
    (-) [Dmax/sqrt(A)]\n
    (-) [Dgyros] diameter of gyration, calculated as twice the maximum vertex distance to its polygon part's centroid\n
    (-) [Fmax] maximum Feret diameter\n
    (-) [FmaxDir] direction of the maximum Feret diameter\n
    (-) [Fmin] minimum Feret diameter\n
    (-) [FminDir] direction of the minimum Feret diameter\n
    (-) [Fmean] mean Feret diameter\n
    (-) [Fmax90] the Feret diameter measured at an angle of 90 degrees to that of the Fmax direction\n
    (-) [Fmin90] the Feret diameter measured at an angle of 90 degrees to that of the Fmin direction\n
    (-) [Fvol] the diameter of a sphere having the same volume as the cylinder constructed by Fmin as the cylinder diameter and Fmax as its length\n
    Arguments
    ----------
    - SHAPES [`input shapes`] : Shapes
    - INDEX [`output shapes`] : Shape Indices. Polygon shapefile with the calculated indices.
    - DMAX [`output shapes`] : Maximum Diameter. Line shapefile showing the maximum diameter.
    - GYROS [`boolean`] : Diameter of Gyration. Default: 0
    - FERET [`boolean`] : Feret Diameters. Default: 0
    - FERET_DIRS [`integer number`] : Number of Directions. Minimum: 2 Default: 18 Number of directions (0-90) to be analyzed.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_polygons', '7', 'Polygon Shape Indices')
    if Tool.is_Okay():
        Tool.Set_Input ('SHAPES', SHAPES)
        Tool.Set_Output('INDEX', INDEX)
        Tool.Set_Output('DMAX', DMAX)
        Tool.Set_Option('GYROS', GYROS)
        Tool.Set_Option('FERET', FERET)
        Tool.Set_Option('FERET_DIRS', FERET_DIRS)
        return Tool.Execute(Verbose)
    return False

def run_tool_shapes_polygons_7(SHAPES=None, INDEX=None, DMAX=None, GYROS=None, FERET=None, FERET_DIRS=None, Verbose=2):
    '''
    Polygon Shape Indices
    ----------
    [shapes_polygons.7]\n
    The tool calculates various indices describing the shape of polygons, mostly based on area, perimeter and maximum diameter. If the optional output 'Shape Indices' is not created, the tool attaches the attributes to the input dataset. Otherwise a new dataset is created and attributes existing in the input dataset are dropped.\n
    (-) [A] area\n
    (-) [P] perimeter\n
    (-) [P/A] interior edge ratio\n
    (-) [P/sqrt(A)]\n
    (-) [Deqpc] equivalent projected circle diameter (=2*sqrt(A/pi))\n
    (-) [Sphericity] the ratio of the perimeter of the equivalent circle to the real perimeter (=(2*sqrt(A*pi))/P)\n
    (-) [Shape Index] the inverse of the sphericity (=P/(2*sqrt(A*pi)))\n
    (-) [Dmax] maximum diameter calculated as maximum distance between two polygon part's vertices\n
    (-) [DmaxDir] direction of maximum diameter\n
    (-) [Dmax/A]\n
    (-) [Dmax/sqrt(A)]\n
    (-) [Dgyros] diameter of gyration, calculated as twice the maximum vertex distance to its polygon part's centroid\n
    (-) [Fmax] maximum Feret diameter\n
    (-) [FmaxDir] direction of the maximum Feret diameter\n
    (-) [Fmin] minimum Feret diameter\n
    (-) [FminDir] direction of the minimum Feret diameter\n
    (-) [Fmean] mean Feret diameter\n
    (-) [Fmax90] the Feret diameter measured at an angle of 90 degrees to that of the Fmax direction\n
    (-) [Fmin90] the Feret diameter measured at an angle of 90 degrees to that of the Fmin direction\n
    (-) [Fvol] the diameter of a sphere having the same volume as the cylinder constructed by Fmin as the cylinder diameter and Fmax as its length\n
    Arguments
    ----------
    - SHAPES [`input shapes`] : Shapes
    - INDEX [`output shapes`] : Shape Indices. Polygon shapefile with the calculated indices.
    - DMAX [`output shapes`] : Maximum Diameter. Line shapefile showing the maximum diameter.
    - GYROS [`boolean`] : Diameter of Gyration. Default: 0
    - FERET [`boolean`] : Feret Diameters. Default: 0
    - FERET_DIRS [`integer number`] : Number of Directions. Minimum: 2 Default: 18 Number of directions (0-90) to be analyzed.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_polygons', '7', 'Polygon Shape Indices')
    if Tool.is_Okay():
        Tool.Set_Input ('SHAPES', SHAPES)
        Tool.Set_Output('INDEX', INDEX)
        Tool.Set_Output('DMAX', DMAX)
        Tool.Set_Option('GYROS', GYROS)
        Tool.Set_Option('FERET', FERET)
        Tool.Set_Option('FERET_DIRS', FERET_DIRS)
        return Tool.Execute(Verbose)
    return False

def PolygonLine_Intersection(POLYGONS=None, LINES=None, INTERSECT=None, SPLIT_PARTS=None, METHOD=None, Verbose=2):
    '''
    Polygon-Line Intersection
    ----------
    [shapes_polygons.8]\n
    Polygon-line intersection. Splits polygons with lines. Complex self-intersecting lines might result in unwanted artifacts. In this case the method option line-by-line might improve the result.\n
    Arguments
    ----------
    - POLYGONS [`input shapes`] : Polygons
    - LINES [`input shapes`] : Lines
    - INTERSECT [`output shapes`] : Intersection
    - SPLIT_PARTS [`boolean`] : Split Parts. Default: 1
    - METHOD [`choice`] : Method. Available Choices: [0] all lines at once [1] line-by-line Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_polygons', '8', 'Polygon-Line Intersection')
    if Tool.is_Okay():
        Tool.Set_Input ('POLYGONS', POLYGONS)
        Tool.Set_Input ('LINES', LINES)
        Tool.Set_Output('INTERSECT', INTERSECT)
        Tool.Set_Option('SPLIT_PARTS', SPLIT_PARTS)
        Tool.Set_Option('METHOD', METHOD)
        return Tool.Execute(Verbose)
    return False

def run_tool_shapes_polygons_8(POLYGONS=None, LINES=None, INTERSECT=None, SPLIT_PARTS=None, METHOD=None, Verbose=2):
    '''
    Polygon-Line Intersection
    ----------
    [shapes_polygons.8]\n
    Polygon-line intersection. Splits polygons with lines. Complex self-intersecting lines might result in unwanted artifacts. In this case the method option line-by-line might improve the result.\n
    Arguments
    ----------
    - POLYGONS [`input shapes`] : Polygons
    - LINES [`input shapes`] : Lines
    - INTERSECT [`output shapes`] : Intersection
    - SPLIT_PARTS [`boolean`] : Split Parts. Default: 1
    - METHOD [`choice`] : Method. Available Choices: [0] all lines at once [1] line-by-line Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_polygons', '8', 'Polygon-Line Intersection')
    if Tool.is_Okay():
        Tool.Set_Input ('POLYGONS', POLYGONS)
        Tool.Set_Input ('LINES', LINES)
        Tool.Set_Output('INTERSECT', INTERSECT)
        Tool.Set_Option('SPLIT_PARTS', SPLIT_PARTS)
        Tool.Set_Option('METHOD', METHOD)
        return Tool.Execute(Verbose)
    return False

def Polygons_to_Edges_and_Nodes(POLYGONS=None, EDGES=None, NODES=None, Verbose=2):
    '''
    Polygons to Edges and Nodes
    ----------
    [shapes_polygons.9]\n
    Polygons to Edges and Nodes\n
    Arguments
    ----------
    - POLYGONS [`input shapes`] : Polygons
    - EDGES [`output shapes`] : Edges
    - NODES [`output shapes`] : Nodes

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_polygons', '9', 'Polygons to Edges and Nodes')
    if Tool.is_Okay():
        Tool.Set_Input ('POLYGONS', POLYGONS)
        Tool.Set_Output('EDGES', EDGES)
        Tool.Set_Output('NODES', NODES)
        return Tool.Execute(Verbose)
    return False

def run_tool_shapes_polygons_9(POLYGONS=None, EDGES=None, NODES=None, Verbose=2):
    '''
    Polygons to Edges and Nodes
    ----------
    [shapes_polygons.9]\n
    Polygons to Edges and Nodes\n
    Arguments
    ----------
    - POLYGONS [`input shapes`] : Polygons
    - EDGES [`output shapes`] : Edges
    - NODES [`output shapes`] : Nodes

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_polygons', '9', 'Polygons to Edges and Nodes')
    if Tool.is_Okay():
        Tool.Set_Input ('POLYGONS', POLYGONS)
        Tool.Set_Output('EDGES', EDGES)
        Tool.Set_Output('NODES', NODES)
        return Tool.Execute(Verbose)
    return False

def Polygon_Parts_to_Separate_Polygons(POLYGONS=None, PARTS=None, LAKES=None, Verbose=2):
    '''
    Polygon Parts to Separate Polygons
    ----------
    [shapes_polygons.10]\n
    Splits parts of multipart polygons into separate polygons. This can be done only for islands (outer rings) or for all parts (inner and outer rings) by checking the 'lakes' option.\n
    Arguments
    ----------
    - POLYGONS [`input shapes`] : Polygons
    - PARTS [`output shapes`] : Polygon Parts
    - LAKES [`boolean`] : Lakes. Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_polygons', '10', 'Polygon Parts to Separate Polygons')
    if Tool.is_Okay():
        Tool.Set_Input ('POLYGONS', POLYGONS)
        Tool.Set_Output('PARTS', PARTS)
        Tool.Set_Option('LAKES', LAKES)
        return Tool.Execute(Verbose)
    return False

def run_tool_shapes_polygons_10(POLYGONS=None, PARTS=None, LAKES=None, Verbose=2):
    '''
    Polygon Parts to Separate Polygons
    ----------
    [shapes_polygons.10]\n
    Splits parts of multipart polygons into separate polygons. This can be done only for islands (outer rings) or for all parts (inner and outer rings) by checking the 'lakes' option.\n
    Arguments
    ----------
    - POLYGONS [`input shapes`] : Polygons
    - PARTS [`output shapes`] : Polygon Parts
    - LAKES [`boolean`] : Lakes. Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_polygons', '10', 'Polygon Parts to Separate Polygons')
    if Tool.is_Okay():
        Tool.Set_Input ('POLYGONS', POLYGONS)
        Tool.Set_Output('PARTS', PARTS)
        Tool.Set_Option('LAKES', LAKES)
        return Tool.Execute(Verbose)
    return False

def Polygon_Clipping(CLIP=None, S_INPUT=None, M_INPUT=None, S_OUTPUT=None, M_OUTPUT=None, DISSOLVE=None, MULTIPLE=None, Verbose=2):
    '''
    Polygon Clipping
    ----------
    [shapes_polygons.11]\n
    Clipping of vector layers with a polygon layer.\n
    Uses the free and open source software library [Clipper] created by Angus Johnson.\n
    Arguments
    ----------
    - CLIP [`input shapes`] : Clip Features
    - S_INPUT [`input shapes`] : Input Features
    - M_INPUT [`input shapes list`] : Input Features
    - S_OUTPUT [`output shapes`] : Output Features
    - M_OUTPUT [`output shapes list`] : Output Features
    - DISSOLVE [`boolean`] : Dissolve Clip Features. Default: 1
    - MULTIPLE [`boolean`] : Multiple Input Features. Default: 1

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_polygons', '11', 'Polygon Clipping')
    if Tool.is_Okay():
        Tool.Set_Input ('CLIP', CLIP)
        Tool.Set_Input ('S_INPUT', S_INPUT)
        Tool.Set_Input ('M_INPUT', M_INPUT)
        Tool.Set_Output('S_OUTPUT', S_OUTPUT)
        Tool.Set_Output('M_OUTPUT', M_OUTPUT)
        Tool.Set_Option('DISSOLVE', DISSOLVE)
        Tool.Set_Option('MULTIPLE', MULTIPLE)
        return Tool.Execute(Verbose)
    return False

def run_tool_shapes_polygons_11(CLIP=None, S_INPUT=None, M_INPUT=None, S_OUTPUT=None, M_OUTPUT=None, DISSOLVE=None, MULTIPLE=None, Verbose=2):
    '''
    Polygon Clipping
    ----------
    [shapes_polygons.11]\n
    Clipping of vector layers with a polygon layer.\n
    Uses the free and open source software library [Clipper] created by Angus Johnson.\n
    Arguments
    ----------
    - CLIP [`input shapes`] : Clip Features
    - S_INPUT [`input shapes`] : Input Features
    - M_INPUT [`input shapes list`] : Input Features
    - S_OUTPUT [`output shapes`] : Output Features
    - M_OUTPUT [`output shapes list`] : Output Features
    - DISSOLVE [`boolean`] : Dissolve Clip Features. Default: 1
    - MULTIPLE [`boolean`] : Multiple Input Features. Default: 1

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_polygons', '11', 'Polygon Clipping')
    if Tool.is_Okay():
        Tool.Set_Input ('CLIP', CLIP)
        Tool.Set_Input ('S_INPUT', S_INPUT)
        Tool.Set_Input ('M_INPUT', M_INPUT)
        Tool.Set_Output('S_OUTPUT', S_OUTPUT)
        Tool.Set_Output('M_OUTPUT', M_OUTPUT)
        Tool.Set_Option('DISSOLVE', DISSOLVE)
        Tool.Set_Option('MULTIPLE', MULTIPLE)
        return Tool.Execute(Verbose)
    return False

def Polygon_SelfIntersection(POLYGONS=None, INTERSECT=None, ID=None, Verbose=2):
    '''
    Polygon Self-Intersection
    ----------
    [shapes_polygons.12]\n
    This tool identifies self-intersection in polygons. The Intersecting areas are added as new polygons to the dataset, leaving the input areas with the geometric difference. The new polygons are labeled with the identifier of the intersecting polygons separated by a '|' character. The identifier can be set with the "Identifier"-field option, otherwise the identifier is just the polygon index.\n
    Uses the free and open source software library [Clipper] created by Angus Johnson.\n
    [Clipper Homepage](http://www.angusj.com/delphi/clipper.php)\n
    [Clipper at SourceForge](http://sourceforge.net/projects/polyclipping/)\n
    Arguments
    ----------
    - POLYGONS [`input shapes`] : Polygons
    - INTERSECT [`output shapes`] : Intersection
    - ID [`table field`] : Identifier

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_polygons', '12', 'Polygon Self-Intersection')
    if Tool.is_Okay():
        Tool.Set_Input ('POLYGONS', POLYGONS)
        Tool.Set_Output('INTERSECT', INTERSECT)
        Tool.Set_Option('ID', ID)
        return Tool.Execute(Verbose)
    return False

def run_tool_shapes_polygons_12(POLYGONS=None, INTERSECT=None, ID=None, Verbose=2):
    '''
    Polygon Self-Intersection
    ----------
    [shapes_polygons.12]\n
    This tool identifies self-intersection in polygons. The Intersecting areas are added as new polygons to the dataset, leaving the input areas with the geometric difference. The new polygons are labeled with the identifier of the intersecting polygons separated by a '|' character. The identifier can be set with the "Identifier"-field option, otherwise the identifier is just the polygon index.\n
    Uses the free and open source software library [Clipper] created by Angus Johnson.\n
    [Clipper Homepage](http://www.angusj.com/delphi/clipper.php)\n
    [Clipper at SourceForge](http://sourceforge.net/projects/polyclipping/)\n
    Arguments
    ----------
    - POLYGONS [`input shapes`] : Polygons
    - INTERSECT [`output shapes`] : Intersection
    - ID [`table field`] : Identifier

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_polygons', '12', 'Polygon Self-Intersection')
    if Tool.is_Okay():
        Tool.Set_Input ('POLYGONS', POLYGONS)
        Tool.Set_Output('INTERSECT', INTERSECT)
        Tool.Set_Option('ID', ID)
        return Tool.Execute(Verbose)
    return False

def Intersect(A=None, B=None, RESULT=None, SPLIT=None, Verbose=2):
    '''
    Intersect
    ----------
    [shapes_polygons.14]\n
    Calculates the geometric intersection of the overlaid polygon layers, i.e. layer A and layer B.\n
    Uses the free and open source software library [Clipper] created by Angus Johnson.\n
    [Clipper Homepage](http://www.angusj.com/delphi/clipper.php)\n
    [Clipper at SourceForge](http://sourceforge.net/projects/polyclipping/)\n
    Arguments
    ----------
    - A [`input shapes`] : Layer A
    - B [`input shapes`] : Layer B
    - RESULT [`output shapes`] : Intersect
    - SPLIT [`boolean`] : Split Parts. Default: 1 Set true if you want multipart polygons to become separate polygons.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_polygons', '14', 'Intersect')
    if Tool.is_Okay():
        Tool.Set_Input ('A', A)
        Tool.Set_Input ('B', B)
        Tool.Set_Output('RESULT', RESULT)
        Tool.Set_Option('SPLIT', SPLIT)
        return Tool.Execute(Verbose)
    return False

def run_tool_shapes_polygons_14(A=None, B=None, RESULT=None, SPLIT=None, Verbose=2):
    '''
    Intersect
    ----------
    [shapes_polygons.14]\n
    Calculates the geometric intersection of the overlaid polygon layers, i.e. layer A and layer B.\n
    Uses the free and open source software library [Clipper] created by Angus Johnson.\n
    [Clipper Homepage](http://www.angusj.com/delphi/clipper.php)\n
    [Clipper at SourceForge](http://sourceforge.net/projects/polyclipping/)\n
    Arguments
    ----------
    - A [`input shapes`] : Layer A
    - B [`input shapes`] : Layer B
    - RESULT [`output shapes`] : Intersect
    - SPLIT [`boolean`] : Split Parts. Default: 1 Set true if you want multipart polygons to become separate polygons.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_polygons', '14', 'Intersect')
    if Tool.is_Okay():
        Tool.Set_Input ('A', A)
        Tool.Set_Input ('B', B)
        Tool.Set_Output('RESULT', RESULT)
        Tool.Set_Option('SPLIT', SPLIT)
        return Tool.Execute(Verbose)
    return False

def Difference(A=None, B=None, RESULT=None, SPLIT=None, Verbose=2):
    '''
    Difference
    ----------
    [shapes_polygons.15]\n
    Calculates the geometric difference of the overlaid polygon layers, i.e. layer A less layer B. Sometimes referred to as 'Erase' command.\n
    Uses the free and open source software library [Clipper] created by Angus Johnson.\n
    [Clipper Homepage](http://www.angusj.com/delphi/clipper.php)\n
    [Clipper at SourceForge](http://sourceforge.net/projects/polyclipping/)\n
    Arguments
    ----------
    - A [`input shapes`] : Layer A
    - B [`input shapes`] : Layer B
    - RESULT [`output shapes`] : Difference
    - SPLIT [`boolean`] : Split Parts. Default: 1 Set true if you want multipart polygons to become separate polygons.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_polygons', '15', 'Difference')
    if Tool.is_Okay():
        Tool.Set_Input ('A', A)
        Tool.Set_Input ('B', B)
        Tool.Set_Output('RESULT', RESULT)
        Tool.Set_Option('SPLIT', SPLIT)
        return Tool.Execute(Verbose)
    return False

def run_tool_shapes_polygons_15(A=None, B=None, RESULT=None, SPLIT=None, Verbose=2):
    '''
    Difference
    ----------
    [shapes_polygons.15]\n
    Calculates the geometric difference of the overlaid polygon layers, i.e. layer A less layer B. Sometimes referred to as 'Erase' command.\n
    Uses the free and open source software library [Clipper] created by Angus Johnson.\n
    [Clipper Homepage](http://www.angusj.com/delphi/clipper.php)\n
    [Clipper at SourceForge](http://sourceforge.net/projects/polyclipping/)\n
    Arguments
    ----------
    - A [`input shapes`] : Layer A
    - B [`input shapes`] : Layer B
    - RESULT [`output shapes`] : Difference
    - SPLIT [`boolean`] : Split Parts. Default: 1 Set true if you want multipart polygons to become separate polygons.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_polygons', '15', 'Difference')
    if Tool.is_Okay():
        Tool.Set_Input ('A', A)
        Tool.Set_Input ('B', B)
        Tool.Set_Output('RESULT', RESULT)
        Tool.Set_Option('SPLIT', SPLIT)
        return Tool.Execute(Verbose)
    return False

def Symmetrical_Difference(A=None, B=None, RESULT=None, SPLIT=None, Verbose=2):
    '''
    Symmetrical Difference
    ----------
    [shapes_polygons.16]\n
    Calculates the symmetrical geometric difference of the overlaid polygon layers, i.e. layer A less layer B plus layer B less layer A.\n
    Uses the free and open source software library [Clipper] created by Angus Johnson.\n
    [Clipper Homepage](http://www.angusj.com/delphi/clipper.php)\n
    [Clipper at SourceForge](http://sourceforge.net/projects/polyclipping/)\n
    Arguments
    ----------
    - A [`input shapes`] : Layer A
    - B [`input shapes`] : Layer B
    - RESULT [`output shapes`] : Symmetrical Difference
    - SPLIT [`boolean`] : Split Parts. Default: 1 Set true if you want multipart polygons to become separate polygons.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_polygons', '16', 'Symmetrical Difference')
    if Tool.is_Okay():
        Tool.Set_Input ('A', A)
        Tool.Set_Input ('B', B)
        Tool.Set_Output('RESULT', RESULT)
        Tool.Set_Option('SPLIT', SPLIT)
        return Tool.Execute(Verbose)
    return False

def run_tool_shapes_polygons_16(A=None, B=None, RESULT=None, SPLIT=None, Verbose=2):
    '''
    Symmetrical Difference
    ----------
    [shapes_polygons.16]\n
    Calculates the symmetrical geometric difference of the overlaid polygon layers, i.e. layer A less layer B plus layer B less layer A.\n
    Uses the free and open source software library [Clipper] created by Angus Johnson.\n
    [Clipper Homepage](http://www.angusj.com/delphi/clipper.php)\n
    [Clipper at SourceForge](http://sourceforge.net/projects/polyclipping/)\n
    Arguments
    ----------
    - A [`input shapes`] : Layer A
    - B [`input shapes`] : Layer B
    - RESULT [`output shapes`] : Symmetrical Difference
    - SPLIT [`boolean`] : Split Parts. Default: 1 Set true if you want multipart polygons to become separate polygons.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_polygons', '16', 'Symmetrical Difference')
    if Tool.is_Okay():
        Tool.Set_Input ('A', A)
        Tool.Set_Input ('B', B)
        Tool.Set_Output('RESULT', RESULT)
        Tool.Set_Option('SPLIT', SPLIT)
        return Tool.Execute(Verbose)
    return False

def Union(A=None, B=None, RESULT=None, SPLIT=None, Verbose=2):
    '''
    Union
    ----------
    [shapes_polygons.17]\n
    Calculates the geometric union of the overlaid polygon layers, i.e. the intersection plus the symmetrical difference of layers A and B.\n
    Uses the free and open source software library [Clipper] created by Angus Johnson.\n
    [Clipper Homepage](http://www.angusj.com/delphi/clipper.php)\n
    [Clipper at SourceForge](http://sourceforge.net/projects/polyclipping/)\n
    Arguments
    ----------
    - A [`input shapes`] : Layer A
    - B [`input shapes`] : Layer B
    - RESULT [`output shapes`] : Union
    - SPLIT [`boolean`] : Split Parts. Default: 1 Set true if you want multipart polygons to become separate polygons.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_polygons', '17', 'Union')
    if Tool.is_Okay():
        Tool.Set_Input ('A', A)
        Tool.Set_Input ('B', B)
        Tool.Set_Output('RESULT', RESULT)
        Tool.Set_Option('SPLIT', SPLIT)
        return Tool.Execute(Verbose)
    return False

def run_tool_shapes_polygons_17(A=None, B=None, RESULT=None, SPLIT=None, Verbose=2):
    '''
    Union
    ----------
    [shapes_polygons.17]\n
    Calculates the geometric union of the overlaid polygon layers, i.e. the intersection plus the symmetrical difference of layers A and B.\n
    Uses the free and open source software library [Clipper] created by Angus Johnson.\n
    [Clipper Homepage](http://www.angusj.com/delphi/clipper.php)\n
    [Clipper at SourceForge](http://sourceforge.net/projects/polyclipping/)\n
    Arguments
    ----------
    - A [`input shapes`] : Layer A
    - B [`input shapes`] : Layer B
    - RESULT [`output shapes`] : Union
    - SPLIT [`boolean`] : Split Parts. Default: 1 Set true if you want multipart polygons to become separate polygons.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_polygons', '17', 'Union')
    if Tool.is_Okay():
        Tool.Set_Input ('A', A)
        Tool.Set_Input ('B', B)
        Tool.Set_Output('RESULT', RESULT)
        Tool.Set_Option('SPLIT', SPLIT)
        return Tool.Execute(Verbose)
    return False

def Update(A=None, B=None, RESULT=None, SPLIT=None, Verbose=2):
    '''
    Update
    ----------
    [shapes_polygons.18]\n
    Updates features of layer A with the features of layer B, i.e. all features of layer B will be supplemented with the difference of layer A less layer B plus. It is assumed, that both input layers share the same attribute structure.\n
    Uses the free and open source software library [Clipper] created by Angus Johnson.\n
    [Clipper Homepage](http://www.angusj.com/delphi/clipper.php)\n
    [Clipper at SourceForge](http://sourceforge.net/projects/polyclipping/)\n
    Arguments
    ----------
    - A [`input shapes`] : Layer A
    - B [`input shapes`] : Layer B
    - RESULT [`output shapes`] : Update
    - SPLIT [`boolean`] : Split Parts. Default: 1 Set true if you want multipart polygons to become separate polygons.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_polygons', '18', 'Update')
    if Tool.is_Okay():
        Tool.Set_Input ('A', A)
        Tool.Set_Input ('B', B)
        Tool.Set_Output('RESULT', RESULT)
        Tool.Set_Option('SPLIT', SPLIT)
        return Tool.Execute(Verbose)
    return False

def run_tool_shapes_polygons_18(A=None, B=None, RESULT=None, SPLIT=None, Verbose=2):
    '''
    Update
    ----------
    [shapes_polygons.18]\n
    Updates features of layer A with the features of layer B, i.e. all features of layer B will be supplemented with the difference of layer A less layer B plus. It is assumed, that both input layers share the same attribute structure.\n
    Uses the free and open source software library [Clipper] created by Angus Johnson.\n
    [Clipper Homepage](http://www.angusj.com/delphi/clipper.php)\n
    [Clipper at SourceForge](http://sourceforge.net/projects/polyclipping/)\n
    Arguments
    ----------
    - A [`input shapes`] : Layer A
    - B [`input shapes`] : Layer B
    - RESULT [`output shapes`] : Update
    - SPLIT [`boolean`] : Split Parts. Default: 1 Set true if you want multipart polygons to become separate polygons.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_polygons', '18', 'Update')
    if Tool.is_Okay():
        Tool.Set_Input ('A', A)
        Tool.Set_Input ('B', B)
        Tool.Set_Output('RESULT', RESULT)
        Tool.Set_Option('SPLIT', SPLIT)
        return Tool.Execute(Verbose)
    return False

def Identity(A=None, B=None, RESULT=None, SPLIT=None, Verbose=2):
    '''
    Identity
    ----------
    [shapes_polygons.19]\n
    Calculates the geometric intersection between both layers and adds the difference of layer A less layer B.\n
    Uses the free and open source software library [Clipper] created by Angus Johnson.\n
    [Clipper Homepage](http://www.angusj.com/delphi/clipper.php)\n
    [Clipper at SourceForge](http://sourceforge.net/projects/polyclipping/)\n
    Arguments
    ----------
    - A [`input shapes`] : Layer A
    - B [`input shapes`] : Layer B
    - RESULT [`output shapes`] : Identity
    - SPLIT [`boolean`] : Split Parts. Default: 1 Set true if you want multipart polygons to become separate polygons.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_polygons', '19', 'Identity')
    if Tool.is_Okay():
        Tool.Set_Input ('A', A)
        Tool.Set_Input ('B', B)
        Tool.Set_Output('RESULT', RESULT)
        Tool.Set_Option('SPLIT', SPLIT)
        return Tool.Execute(Verbose)
    return False

def run_tool_shapes_polygons_19(A=None, B=None, RESULT=None, SPLIT=None, Verbose=2):
    '''
    Identity
    ----------
    [shapes_polygons.19]\n
    Calculates the geometric intersection between both layers and adds the difference of layer A less layer B.\n
    Uses the free and open source software library [Clipper] created by Angus Johnson.\n
    [Clipper Homepage](http://www.angusj.com/delphi/clipper.php)\n
    [Clipper at SourceForge](http://sourceforge.net/projects/polyclipping/)\n
    Arguments
    ----------
    - A [`input shapes`] : Layer A
    - B [`input shapes`] : Layer B
    - RESULT [`output shapes`] : Identity
    - SPLIT [`boolean`] : Split Parts. Default: 1 Set true if you want multipart polygons to become separate polygons.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_polygons', '19', 'Identity')
    if Tool.is_Okay():
        Tool.Set_Input ('A', A)
        Tool.Set_Input ('B', B)
        Tool.Set_Output('RESULT', RESULT)
        Tool.Set_Option('SPLIT', SPLIT)
        return Tool.Execute(Verbose)
    return False

def Add_Point_Attributes_to_Polygons(INPUT=None, POINTS=None, OUTPUT=None, FIELDS=None, ADD_LOCATION_INFO=None, Verbose=2):
    '''
    Add Point Attributes to Polygons
    ----------
    [shapes_polygons.20]\n
    Spatial join for polygons. Retrieves for each polygon the selected attributes from that point, which is contained in the polygon. In case a polygon contains more than one point, the last point wins.\n
    Optionally, the tool allows one to attach the geometrical properties (x,y(z,m)) of each point as additional attributes.\n
    Arguments
    ----------
    - INPUT [`input shapes`] : Polygons. Input polygon shapefile
    - POINTS [`input shapes`] : Points. Input point shapefile
    - OUTPUT [`output shapes`] : Result. Optional output polygon shapefile
    - FIELDS [`table fields`] : Attributes. Attributes to add. Select none to add all
    - ADD_LOCATION_INFO [`boolean`] : Add Location Info. Default: 0 Add location information from points (x,y,(z,m))

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_polygons', '20', 'Add Point Attributes to Polygons')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Input ('POINTS', POINTS)
        Tool.Set_Output('OUTPUT', OUTPUT)
        Tool.Set_Option('FIELDS', FIELDS)
        Tool.Set_Option('ADD_LOCATION_INFO', ADD_LOCATION_INFO)
        return Tool.Execute(Verbose)
    return False

def run_tool_shapes_polygons_20(INPUT=None, POINTS=None, OUTPUT=None, FIELDS=None, ADD_LOCATION_INFO=None, Verbose=2):
    '''
    Add Point Attributes to Polygons
    ----------
    [shapes_polygons.20]\n
    Spatial join for polygons. Retrieves for each polygon the selected attributes from that point, which is contained in the polygon. In case a polygon contains more than one point, the last point wins.\n
    Optionally, the tool allows one to attach the geometrical properties (x,y(z,m)) of each point as additional attributes.\n
    Arguments
    ----------
    - INPUT [`input shapes`] : Polygons. Input polygon shapefile
    - POINTS [`input shapes`] : Points. Input point shapefile
    - OUTPUT [`output shapes`] : Result. Optional output polygon shapefile
    - FIELDS [`table fields`] : Attributes. Attributes to add. Select none to add all
    - ADD_LOCATION_INFO [`boolean`] : Add Location Info. Default: 0 Add location information from points (x,y,(z,m))

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_polygons', '20', 'Add Point Attributes to Polygons')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Input ('POINTS', POINTS)
        Tool.Set_Output('OUTPUT', OUTPUT)
        Tool.Set_Option('FIELDS', FIELDS)
        Tool.Set_Option('ADD_LOCATION_INFO', ADD_LOCATION_INFO)
        return Tool.Execute(Verbose)
    return False

def Flatten_Polygon_Layer(INPUT=None, OUTPUT=None, Verbose=2):
    '''
    Flatten Polygon Layer
    ----------
    [shapes_polygons.21]\n
    Removes invalid polygons, i.e. polygons with less than three vertices, and merges polygons belonging spatially together, i.e. forming outer and inner rings. Inner rings are not preserved as separate polygon, but become new part of the polygon forming the outer ring.\n
    Arguments
    ----------
    - INPUT [`input shapes`] : Input
    - OUTPUT [`output shapes`] : Output

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_polygons', '21', 'Flatten Polygon Layer')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Output('OUTPUT', OUTPUT)
        return Tool.Execute(Verbose)
    return False

def run_tool_shapes_polygons_21(INPUT=None, OUTPUT=None, Verbose=2):
    '''
    Flatten Polygon Layer
    ----------
    [shapes_polygons.21]\n
    Removes invalid polygons, i.e. polygons with less than three vertices, and merges polygons belonging spatially together, i.e. forming outer and inner rings. Inner rings are not preserved as separate polygon, but become new part of the polygon forming the outer ring.\n
    Arguments
    ----------
    - INPUT [`input shapes`] : Input
    - OUTPUT [`output shapes`] : Output

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_polygons', '21', 'Flatten Polygon Layer')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Output('OUTPUT', OUTPUT)
        return Tool.Execute(Verbose)
    return False

def Shared_Polygon_Edges(POLYGONS=None, EDGES=None, ATTRIBUTE=None, VERTICES=None, EPSILON=None, DOUBLE=None, Verbose=2):
    '''
    Shared Polygon Edges
    ----------
    [shapes_polygons.22]\n
    Shared Polygon Edges\n
    Arguments
    ----------
    - POLYGONS [`input shapes`] : Polygons
    - EDGES [`output shapes`] : Edges
    - ATTRIBUTE [`table field`] : Attribute
    - VERTICES [`boolean`] : Check Vertices. Default: 0
    - EPSILON [`floating point number`] : Tolerance. Minimum: 0.000000 Default: 0.000010
    - DOUBLE [`boolean`] : Double Edges. Default: 0 give output of an edge twice, i.e. once for each of the two adjacent polygons

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_polygons', '22', 'Shared Polygon Edges')
    if Tool.is_Okay():
        Tool.Set_Input ('POLYGONS', POLYGONS)
        Tool.Set_Output('EDGES', EDGES)
        Tool.Set_Option('ATTRIBUTE', ATTRIBUTE)
        Tool.Set_Option('VERTICES', VERTICES)
        Tool.Set_Option('EPSILON', EPSILON)
        Tool.Set_Option('DOUBLE', DOUBLE)
        return Tool.Execute(Verbose)
    return False

def run_tool_shapes_polygons_22(POLYGONS=None, EDGES=None, ATTRIBUTE=None, VERTICES=None, EPSILON=None, DOUBLE=None, Verbose=2):
    '''
    Shared Polygon Edges
    ----------
    [shapes_polygons.22]\n
    Shared Polygon Edges\n
    Arguments
    ----------
    - POLYGONS [`input shapes`] : Polygons
    - EDGES [`output shapes`] : Edges
    - ATTRIBUTE [`table field`] : Attribute
    - VERTICES [`boolean`] : Check Vertices. Default: 0
    - EPSILON [`floating point number`] : Tolerance. Minimum: 0.000000 Default: 0.000010
    - DOUBLE [`boolean`] : Double Edges. Default: 0 give output of an edge twice, i.e. once for each of the two adjacent polygons

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_polygons', '22', 'Shared Polygon Edges')
    if Tool.is_Okay():
        Tool.Set_Input ('POLYGONS', POLYGONS)
        Tool.Set_Output('EDGES', EDGES)
        Tool.Set_Option('ATTRIBUTE', ATTRIBUTE)
        Tool.Set_Option('VERTICES', VERTICES)
        Tool.Set_Option('EPSILON', EPSILON)
        Tool.Set_Option('DOUBLE', DOUBLE)
        return Tool.Execute(Verbose)
    return False

def Polygon_Generalization(POLYGONS=None, GENERALIZED=None, THRESHOLD=None, JOIN_TO=None, VERTICES=None, EPSILON=None, Verbose=2):
    '''
    Polygon Generalization
    ----------
    [shapes_polygons.23]\n
    A simple generalization tool for polygons. The tool joins polygons with an area size smaller than the specified threshold to a neighbouring polygon. Either the neighbouring polygon with the largest area or the one with which the largest edge length is shared wins.\n
    Arguments
    ----------
    - POLYGONS [`input shapes`] : Shapes. The input polygons.
    - GENERALIZED [`output shapes`] : Generalized Shapes. The generalized output polygons.
    - THRESHOLD [`floating point number`] : Area Threshold. Minimum: 0.000000 Default: 100.000000 The maximum area of a polygon to get joined [map units squared].
    - JOIN_TO [`choice`] : Join to Neighbour with .... Available Choices: [0] largest area [1] largest shared edge length Default: 0 Choose the method to determine the winner polygon.
    - VERTICES [`boolean`] : Check Vertices. Default: 0
    - EPSILON [`floating point number`] : Tolerance. Minimum: 0.000000 Default: 0.000010

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_polygons', '23', 'Polygon Generalization')
    if Tool.is_Okay():
        Tool.Set_Input ('POLYGONS', POLYGONS)
        Tool.Set_Output('GENERALIZED', GENERALIZED)
        Tool.Set_Option('THRESHOLD', THRESHOLD)
        Tool.Set_Option('JOIN_TO', JOIN_TO)
        Tool.Set_Option('VERTICES', VERTICES)
        Tool.Set_Option('EPSILON', EPSILON)
        return Tool.Execute(Verbose)
    return False

def run_tool_shapes_polygons_23(POLYGONS=None, GENERALIZED=None, THRESHOLD=None, JOIN_TO=None, VERTICES=None, EPSILON=None, Verbose=2):
    '''
    Polygon Generalization
    ----------
    [shapes_polygons.23]\n
    A simple generalization tool for polygons. The tool joins polygons with an area size smaller than the specified threshold to a neighbouring polygon. Either the neighbouring polygon with the largest area or the one with which the largest edge length is shared wins.\n
    Arguments
    ----------
    - POLYGONS [`input shapes`] : Shapes. The input polygons.
    - GENERALIZED [`output shapes`] : Generalized Shapes. The generalized output polygons.
    - THRESHOLD [`floating point number`] : Area Threshold. Minimum: 0.000000 Default: 100.000000 The maximum area of a polygon to get joined [map units squared].
    - JOIN_TO [`choice`] : Join to Neighbour with .... Available Choices: [0] largest area [1] largest shared edge length Default: 0 Choose the method to determine the winner polygon.
    - VERTICES [`boolean`] : Check Vertices. Default: 0
    - EPSILON [`floating point number`] : Tolerance. Minimum: 0.000000 Default: 0.000010

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_polygons', '23', 'Polygon Generalization')
    if Tool.is_Okay():
        Tool.Set_Input ('POLYGONS', POLYGONS)
        Tool.Set_Output('GENERALIZED', GENERALIZED)
        Tool.Set_Option('THRESHOLD', THRESHOLD)
        Tool.Set_Option('JOIN_TO', JOIN_TO)
        Tool.Set_Option('VERTICES', VERTICES)
        Tool.Set_Option('EPSILON', EPSILON)
        return Tool.Execute(Verbose)
    return False

def Invert_Ring_Ordering(POLYGONS=None, INVERTED=None, Verbose=2):
    '''
    Invert Ring Ordering
    ----------
    [shapes_polygons.24]\n
    The tool allows one to invert the ring ordering of polygons, i.e. to change their orientation.\n
    Arguments
    ----------
    - POLYGONS [`input shapes`] : Polygons. The input polygon shapes layer.
    - INVERTED [`output shapes`] : Inverted Polygons. The polygon shapes layer with the inverted ring ordering.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_polygons', '24', 'Invert Ring Ordering')
    if Tool.is_Okay():
        Tool.Set_Input ('POLYGONS', POLYGONS)
        Tool.Set_Output('INVERTED', INVERTED)
        return Tool.Execute(Verbose)
    return False

def run_tool_shapes_polygons_24(POLYGONS=None, INVERTED=None, Verbose=2):
    '''
    Invert Ring Ordering
    ----------
    [shapes_polygons.24]\n
    The tool allows one to invert the ring ordering of polygons, i.e. to change their orientation.\n
    Arguments
    ----------
    - POLYGONS [`input shapes`] : Polygons. The input polygon shapes layer.
    - INVERTED [`output shapes`] : Inverted Polygons. The polygon shapes layer with the inverted ring ordering.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_polygons', '24', 'Invert Ring Ordering')
    if Tool.is_Okay():
        Tool.Set_Input ('POLYGONS', POLYGONS)
        Tool.Set_Output('INVERTED', INVERTED)
        return Tool.Execute(Verbose)
    return False

def Total_Length_of_Lines_per_Polygon(POLYGONS=None, LINES=None, Verbose=2):
    '''
    Total Length of Lines per Polygon
    ----------
    [shapes_polygons.length_of_lines]\n
    Calculates the total length of all input line segments intersecting each polygon and adds the information as new attribute field to the polygon layer.\n
    Arguments
    ----------
    - POLYGONS [`input shapes`] : Polygons
    - LINES [`input shapes`] : Lines

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_polygons', 'length_of_lines', 'Total Length of Lines per Polygon')
    if Tool.is_Okay():
        Tool.Set_Input ('POLYGONS', POLYGONS)
        Tool.Set_Input ('LINES', LINES)
        return Tool.Execute(Verbose)
    return False

def run_tool_shapes_polygons_length_of_lines(POLYGONS=None, LINES=None, Verbose=2):
    '''
    Total Length of Lines per Polygon
    ----------
    [shapes_polygons.length_of_lines]\n
    Calculates the total length of all input line segments intersecting each polygon and adds the information as new attribute field to the polygon layer.\n
    Arguments
    ----------
    - POLYGONS [`input shapes`] : Polygons
    - LINES [`input shapes`] : Lines

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_polygons', 'length_of_lines', 'Total Length of Lines per Polygon')
    if Tool.is_Okay():
        Tool.Set_Input ('POLYGONS', POLYGONS)
        Tool.Set_Input ('LINES', LINES)
        return Tool.Execute(Verbose)
    return False

def Largest_Circles_in_Polygons(POLYGONS=None, CIRCLES=None, RESOLUTION=None, Verbose=2):
    '''
    Largest Circles in Polygons
    ----------
    [shapes_polygons.max_interior_circles]\n
    created from history\n
    Arguments
    ----------
    - POLYGONS [`input shapes`] : Polygons
    - CIRCLES [`output shapes`] : Circles
    - RESOLUTION [`floating point number`] : Resolution. Minimum: 0.000000 Default: 10.000000

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_polygons', 'max_interior_circles', 'Largest Circles in Polygons')
    if Tool.is_Okay():
        Tool.Set_Input ('POLYGONS', POLYGONS)
        Tool.Set_Output('CIRCLES', CIRCLES)
        Tool.Set_Option('RESOLUTION', RESOLUTION)
        return Tool.Execute(Verbose)
    return False

def run_tool_shapes_polygons_max_interior_circles(POLYGONS=None, CIRCLES=None, RESOLUTION=None, Verbose=2):
    '''
    Largest Circles in Polygons
    ----------
    [shapes_polygons.max_interior_circles]\n
    created from history\n
    Arguments
    ----------
    - POLYGONS [`input shapes`] : Polygons
    - CIRCLES [`output shapes`] : Circles
    - RESOLUTION [`floating point number`] : Resolution. Minimum: 0.000000 Default: 10.000000

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_polygons', 'max_interior_circles', 'Largest Circles in Polygons')
    if Tool.is_Okay():
        Tool.Set_Input ('POLYGONS', POLYGONS)
        Tool.Set_Output('CIRCLES', CIRCLES)
        Tool.Set_Option('RESOLUTION', RESOLUTION)
        return Tool.Execute(Verbose)
    return False

def Remove_Boundary_Polygons(POLYGONS=None, RESULT=None, METHOD=None, Verbose=2):
    '''
    Remove Boundary Polygons
    ----------
    [shapes_polygons.remove_from_boundary]\n
    Removes all non-interior polygons from an input polygons layer, i.e. those polygons that are not completely surrounded by other polygons. Useful to exclude boundary effects. The simpler and faster method uses only the rectangular layer's extent to distinguish between interior and boundary polygons.\n
    Arguments
    ----------
    - POLYGONS [`input shapes`] : Polygons
    - RESULT [`output shapes`] : Result
    - METHOD [`choice`] : Method. Available Choices: [0] Extent [1] Polygon Boundary Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_polygons', 'remove_from_boundary', 'Remove Boundary Polygons')
    if Tool.is_Okay():
        Tool.Set_Input ('POLYGONS', POLYGONS)
        Tool.Set_Output('RESULT', RESULT)
        Tool.Set_Option('METHOD', METHOD)
        return Tool.Execute(Verbose)
    return False

def run_tool_shapes_polygons_remove_from_boundary(POLYGONS=None, RESULT=None, METHOD=None, Verbose=2):
    '''
    Remove Boundary Polygons
    ----------
    [shapes_polygons.remove_from_boundary]\n
    Removes all non-interior polygons from an input polygons layer, i.e. those polygons that are not completely surrounded by other polygons. Useful to exclude boundary effects. The simpler and faster method uses only the rectangular layer's extent to distinguish between interior and boundary polygons.\n
    Arguments
    ----------
    - POLYGONS [`input shapes`] : Polygons
    - RESULT [`output shapes`] : Result
    - METHOD [`choice`] : Method. Available Choices: [0] Extent [1] Polygon Boundary Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_polygons', 'remove_from_boundary', 'Remove Boundary Polygons')
    if Tool.is_Okay():
        Tool.Set_Input ('POLYGONS', POLYGONS)
        Tool.Set_Output('RESULT', RESULT)
        Tool.Set_Option('METHOD', METHOD)
        return Tool.Execute(Verbose)
    return False

