#! /usr/bin/env python

'''
Python Interface to SAGA Tools Library
----------
- Category : Reports
- Name     : HTML
- ID       : docs_html

Description
----------
Reports and document creation in Hypertext Mark Up Language (HTML) format.
'''

from PySAGA.helper import Tool_Wrapper

def SVG_Interactive_Map(LIST=None, SHAPES=None, FILENAME=None, Verbose=2):
    '''
    SVG Interactive Map
    ----------
    [docs_html.1]\n
    SVG Interactive Map\n
    Arguments
    ----------
    - LIST [`input shapes list`] : Shapes Layers
    - SHAPES [`optional input shapes`] : [FLD] Index Layer
    - FILENAME [`file path`] : [FLD] SVG File

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('docs_html', '1', 'SVG Interactive Map')
    if Tool.is_Okay():
        Tool.Set_Input ('LIST', LIST)
        Tool.Set_Input ('SHAPES', SHAPES)
        Tool.Set_Option('FILENAME', FILENAME)
        return Tool.Execute(Verbose)
    return False

def run_tool_docs_html_1(LIST=None, SHAPES=None, FILENAME=None, Verbose=2):
    '''
    SVG Interactive Map
    ----------
    [docs_html.1]\n
    SVG Interactive Map\n
    Arguments
    ----------
    - LIST [`input shapes list`] : Shapes Layers
    - SHAPES [`optional input shapes`] : [FLD] Index Layer
    - FILENAME [`file path`] : [FLD] SVG File

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('docs_html', '1', 'SVG Interactive Map')
    if Tool.is_Okay():
        Tool.Set_Input ('LIST', LIST)
        Tool.Set_Input ('SHAPES', SHAPES)
        Tool.Set_Option('FILENAME', FILENAME)
        return Tool.Execute(Verbose)
    return False

