#! /usr/bin/env python

'''
Python Interface to SAGA Tools Library
----------
- Category : Import/Export
- Name     : ODBC/OTL
- ID       : db_odbc

Description
----------
Database access via Open Data Base Connection (ODBC) interface. Based on the OTL (Oracle, Odbc and DB2-CLI Template Library), Version 4.0: [http://otl.sourceforge.net/](http://otl.sourceforge.net/)
'''

from PySAGA.helper import Tool_Wrapper

def Connect_to_ODBC_Source(DSN=None, USER=None, PASSWORD=None, Verbose=2):
    '''
    Connect to ODBC Source
    ----------
    [db_odbc.0]\n
    Connects to an ODBC source.\n
    Arguments
    ----------
    - DSN [`choice`] : Data Source. Available Choices: [0] The ODBC Data Source Name. Default: 0
    - USER [`text`] : User
    - PASSWORD [`text`] : Password

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('db_odbc', '0', 'Connect to ODBC Source')
    if Tool.is_Okay():
        Tool.Set_Option('DSN', DSN)
        Tool.Set_Option('USER', USER)
        Tool.Set_Option('PASSWORD', PASSWORD)
        return Tool.Execute(Verbose)
    return False

def run_tool_db_odbc_0(DSN=None, USER=None, PASSWORD=None, Verbose=2):
    '''
    Connect to ODBC Source
    ----------
    [db_odbc.0]\n
    Connects to an ODBC source.\n
    Arguments
    ----------
    - DSN [`choice`] : Data Source. Available Choices: [0] The ODBC Data Source Name. Default: 0
    - USER [`text`] : User
    - PASSWORD [`text`] : Password

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('db_odbc', '0', 'Connect to ODBC Source')
    if Tool.is_Okay():
        Tool.Set_Option('DSN', DSN)
        Tool.Set_Option('USER', USER)
        Tool.Set_Option('PASSWORD', PASSWORD)
        return Tool.Execute(Verbose)
    return False

def Disconnect_from_ODBC_Source(CONNECTION=None, TRANSACT=None, Verbose=2):
    '''
    Disconnect from ODBC Source
    ----------
    [db_odbc.1]\n
    Disconnects an ODBC source connection.\n
    Arguments
    ----------
    - CONNECTION [`choice`] : Connection. Available Choices: Default: 0
    - TRANSACT [`choice`] : Transactions. Available Choices: [0] rollback [1] commit Default: 1

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('db_odbc', '1', 'Disconnect from ODBC Source')
    if Tool.is_Okay():
        Tool.Set_Option('CONNECTION', CONNECTION)
        Tool.Set_Option('TRANSACT', TRANSACT)
        return Tool.Execute(Verbose)
    return False

def run_tool_db_odbc_1(CONNECTION=None, TRANSACT=None, Verbose=2):
    '''
    Disconnect from ODBC Source
    ----------
    [db_odbc.1]\n
    Disconnects an ODBC source connection.\n
    Arguments
    ----------
    - CONNECTION [`choice`] : Connection. Available Choices: Default: 0
    - TRANSACT [`choice`] : Transactions. Available Choices: [0] rollback [1] commit Default: 1

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('db_odbc', '1', 'Disconnect from ODBC Source')
    if Tool.is_Okay():
        Tool.Set_Option('CONNECTION', CONNECTION)
        Tool.Set_Option('TRANSACT', TRANSACT)
        return Tool.Execute(Verbose)
    return False

def CommitRollback_Transaction(CONNECTION=None, SOURCE=None, TRANSACT=None, Verbose=2):
    '''
    Commit/Rollback Transaction
    ----------
    [db_odbc.2]\n
    Execute a commit or rollback on open transactions with ODBC source.\n
    Arguments
    ----------
    - CONNECTION [`choice`] : Connection. Available Choices: Default: 0
    - SOURCE [`choice`] : Source. Available Choices: Default: 0
    - TRANSACT [`choice`] : Transactions. Available Choices: [0] rollback [1] commit Default: 1

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('db_odbc', '2', 'Commit/Rollback Transaction')
    if Tool.is_Okay():
        Tool.Set_Option('CONNECTION', CONNECTION)
        Tool.Set_Option('SOURCE', SOURCE)
        Tool.Set_Option('TRANSACT', TRANSACT)
        return Tool.Execute(Verbose)
    return False

def run_tool_db_odbc_2(CONNECTION=None, SOURCE=None, TRANSACT=None, Verbose=2):
    '''
    Commit/Rollback Transaction
    ----------
    [db_odbc.2]\n
    Execute a commit or rollback on open transactions with ODBC source.\n
    Arguments
    ----------
    - CONNECTION [`choice`] : Connection. Available Choices: Default: 0
    - SOURCE [`choice`] : Source. Available Choices: Default: 0
    - TRANSACT [`choice`] : Transactions. Available Choices: [0] rollback [1] commit Default: 1

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('db_odbc', '2', 'Commit/Rollback Transaction')
    if Tool.is_Okay():
        Tool.Set_Option('CONNECTION', CONNECTION)
        Tool.Set_Option('SOURCE', SOURCE)
        Tool.Set_Option('TRANSACT', TRANSACT)
        return Tool.Execute(Verbose)
    return False

def Execute_SQL(CONNECTION=None, SQL=None, COMMIT=None, STOP=None, Verbose=2):
    '''
    Execute SQL
    ----------
    [db_odbc.3]\n
    Execute SQL commands on a connected ODBC source. Separate different commands with a semicolon (';').\n
    Arguments
    ----------
    - CONNECTION [`choice`] : Connection. Available Choices: Default: 0
    - SQL [`long text`] : SQL Statement. Default: CREATE TABLE myTable1 (Col1 VARCHAR(255) PRIMARY KEY, Col2 INTEGER);
INSERT INTO myTable1 (Col1, Col2) VALUES('First Value', 1);
DROP TABLE myTable1;

    - COMMIT [`boolean`] : Commit. Default: 1
    - STOP [`boolean`] : Stop on Error. Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('db_odbc', '3', 'Execute SQL')
    if Tool.is_Okay():
        Tool.Set_Option('CONNECTION', CONNECTION)
        Tool.Set_Option('SQL', SQL)
        Tool.Set_Option('COMMIT', COMMIT)
        Tool.Set_Option('STOP', STOP)
        return Tool.Execute(Verbose)
    return False

def run_tool_db_odbc_3(CONNECTION=None, SQL=None, COMMIT=None, STOP=None, Verbose=2):
    '''
    Execute SQL
    ----------
    [db_odbc.3]\n
    Execute SQL commands on a connected ODBC source. Separate different commands with a semicolon (';').\n
    Arguments
    ----------
    - CONNECTION [`choice`] : Connection. Available Choices: Default: 0
    - SQL [`long text`] : SQL Statement. Default: CREATE TABLE myTable1 (Col1 VARCHAR(255) PRIMARY KEY, Col2 INTEGER);
INSERT INTO myTable1 (Col1, Col2) VALUES('First Value', 1);
DROP TABLE myTable1;

    - COMMIT [`boolean`] : Commit. Default: 1
    - STOP [`boolean`] : Stop on Error. Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('db_odbc', '3', 'Execute SQL')
    if Tool.is_Okay():
        Tool.Set_Option('CONNECTION', CONNECTION)
        Tool.Set_Option('SQL', SQL)
        Tool.Set_Option('COMMIT', COMMIT)
        Tool.Set_Option('STOP', STOP)
        return Tool.Execute(Verbose)
    return False

def List_Table_Fields(FIELDS=None, CONNECTION=None, TABLE=None, Verbose=2):
    '''
    List Table Fields
    ----------
    [db_odbc.4]\n
    Loads table information from ODBC data source.\n
    Arguments
    ----------
    - FIELDS [`output table`] : Fields
    - CONNECTION [`choice`] : Connection. Available Choices: Default: 0
    - TABLE [`choice`] : Table Source. Available Choices: Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('db_odbc', '4', 'List Table Fields')
    if Tool.is_Okay():
        Tool.Set_Output('FIELDS', FIELDS)
        Tool.Set_Option('CONNECTION', CONNECTION)
        Tool.Set_Option('TABLE', TABLE)
        return Tool.Execute(Verbose)
    return False

def run_tool_db_odbc_4(FIELDS=None, CONNECTION=None, TABLE=None, Verbose=2):
    '''
    List Table Fields
    ----------
    [db_odbc.4]\n
    Loads table information from ODBC data source.\n
    Arguments
    ----------
    - FIELDS [`output table`] : Fields
    - CONNECTION [`choice`] : Connection. Available Choices: Default: 0
    - TABLE [`choice`] : Table Source. Available Choices: Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('db_odbc', '4', 'List Table Fields')
    if Tool.is_Okay():
        Tool.Set_Output('FIELDS', FIELDS)
        Tool.Set_Option('CONNECTION', CONNECTION)
        Tool.Set_Option('TABLE', TABLE)
        return Tool.Execute(Verbose)
    return False

def Import_Table(TABLE=None, CONNECTION=None, SOURCE=None, Verbose=2):
    '''
    Import Table
    ----------
    [db_odbc.5]\n
    Imports a table from a database via ODBC.\n
    Arguments
    ----------
    - TABLE [`output table`] : Table
    - CONNECTION [`choice`] : Connection. Available Choices: Default: 0
    - SOURCE [`choice`] : Table Source. Available Choices: Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('db_odbc', '5', 'Import Table')
    if Tool.is_Okay():
        Tool.Set_Output('TABLE', TABLE)
        Tool.Set_Option('CONNECTION', CONNECTION)
        Tool.Set_Option('SOURCE', SOURCE)
        return Tool.Execute(Verbose)
    return False

def run_tool_db_odbc_5(TABLE=None, CONNECTION=None, SOURCE=None, Verbose=2):
    '''
    Import Table
    ----------
    [db_odbc.5]\n
    Imports a table from a database via ODBC.\n
    Arguments
    ----------
    - TABLE [`output table`] : Table
    - CONNECTION [`choice`] : Connection. Available Choices: Default: 0
    - SOURCE [`choice`] : Table Source. Available Choices: Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('db_odbc', '5', 'Import Table')
    if Tool.is_Okay():
        Tool.Set_Output('TABLE', TABLE)
        Tool.Set_Option('CONNECTION', CONNECTION)
        Tool.Set_Option('SOURCE', SOURCE)
        return Tool.Execute(Verbose)
    return False

def Export_Table(TABLE=None, CONNECTION=None, TABLE_PK=None, TABLE_NN=None, TABLE_UQ=None, NAME=None, EXISTS=None, Verbose=2):
    '''
    Export Table
    ----------
    [db_odbc.6]\n
    Exports a table to a database via ODBC.\n
    Arguments
    ----------
    - TABLE [`input table`] : Table
    - CONNECTION [`choice`] : Connection. Available Choices: Default: 0
    - TABLE_PK [`table fields`] : Primary Key
    - TABLE_NN [`table fields`] : Not Null
    - TABLE_UQ [`table fields`] : Unique
    - NAME [`text`] : Table Name
    - EXISTS [`choice`] : If table exists.... Available Choices: [0] abort export [1] replace existing table [2] append records, if table structure allows Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('db_odbc', '6', 'Export Table')
    if Tool.is_Okay():
        Tool.Set_Input ('TABLE', TABLE)
        Tool.Set_Option('CONNECTION', CONNECTION)
        Tool.Set_Option('TABLE_PK', TABLE_PK)
        Tool.Set_Option('TABLE_NN', TABLE_NN)
        Tool.Set_Option('TABLE_UQ', TABLE_UQ)
        Tool.Set_Option('NAME', NAME)
        Tool.Set_Option('EXISTS', EXISTS)
        return Tool.Execute(Verbose)
    return False

def run_tool_db_odbc_6(TABLE=None, CONNECTION=None, TABLE_PK=None, TABLE_NN=None, TABLE_UQ=None, NAME=None, EXISTS=None, Verbose=2):
    '''
    Export Table
    ----------
    [db_odbc.6]\n
    Exports a table to a database via ODBC.\n
    Arguments
    ----------
    - TABLE [`input table`] : Table
    - CONNECTION [`choice`] : Connection. Available Choices: Default: 0
    - TABLE_PK [`table fields`] : Primary Key
    - TABLE_NN [`table fields`] : Not Null
    - TABLE_UQ [`table fields`] : Unique
    - NAME [`text`] : Table Name
    - EXISTS [`choice`] : If table exists.... Available Choices: [0] abort export [1] replace existing table [2] append records, if table structure allows Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('db_odbc', '6', 'Export Table')
    if Tool.is_Okay():
        Tool.Set_Input ('TABLE', TABLE)
        Tool.Set_Option('CONNECTION', CONNECTION)
        Tool.Set_Option('TABLE_PK', TABLE_PK)
        Tool.Set_Option('TABLE_NN', TABLE_NN)
        Tool.Set_Option('TABLE_UQ', TABLE_UQ)
        Tool.Set_Option('NAME', NAME)
        Tool.Set_Option('EXISTS', EXISTS)
        return Tool.Execute(Verbose)
    return False

def Drop_Table(CONNECTION=None, TABLE=None, Verbose=2):
    '''
    Drop Table
    ----------
    [db_odbc.7]\n
    Deletes a table from a database via ODBC.\n
    Arguments
    ----------
    - CONNECTION [`choice`] : Connection. Available Choices: Default: 0
    - TABLE [`choice`] : Table. Available Choices: Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('db_odbc', '7', 'Drop Table')
    if Tool.is_Okay():
        Tool.Set_Option('CONNECTION', CONNECTION)
        Tool.Set_Option('TABLE', TABLE)
        return Tool.Execute(Verbose)
    return False

def run_tool_db_odbc_7(CONNECTION=None, TABLE=None, Verbose=2):
    '''
    Drop Table
    ----------
    [db_odbc.7]\n
    Deletes a table from a database via ODBC.\n
    Arguments
    ----------
    - CONNECTION [`choice`] : Connection. Available Choices: Default: 0
    - TABLE [`choice`] : Table. Available Choices: Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('db_odbc', '7', 'Drop Table')
    if Tool.is_Okay():
        Tool.Set_Option('CONNECTION', CONNECTION)
        Tool.Set_Option('TABLE', TABLE)
        return Tool.Execute(Verbose)
    return False

def Import_Table_from_Query(TABLE=None, CONNECTION=None, TABLES=None, FIELDS=None, WHERE=None, GROUP=None, HAVING=None, ORDER=None, DISTINCT=None, Verbose=2):
    '''
    Import Table from Query
    ----------
    [db_odbc.8]\n
    Import a table from a database through ODBC via SQL query.\n
    > SELECT [DISTINCT|ALL] 'Fields' FROM 'Tables' WHERE 'Where' [GROUP BY 'Group' [HAVING 'Having']] [ORDER BY 'Order']\n
    Arguments
    ----------
    - TABLE [`output table`] : Table
    - CONNECTION [`choice`] : Connection. Available Choices: Default: 0
    - TABLES [`text`] : Tables
    - FIELDS [`text`] : Fields. Default: *
    - WHERE [`text`] : Where
    - GROUP [`text`] : Group
    - HAVING [`text`] : Having
    - ORDER [`text`] : Order
    - DISTINCT [`boolean`] : Distinct. Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('db_odbc', '8', 'Import Table from Query')
    if Tool.is_Okay():
        Tool.Set_Output('TABLE', TABLE)
        Tool.Set_Option('CONNECTION', CONNECTION)
        Tool.Set_Option('TABLES', TABLES)
        Tool.Set_Option('FIELDS', FIELDS)
        Tool.Set_Option('WHERE', WHERE)
        Tool.Set_Option('GROUP', GROUP)
        Tool.Set_Option('HAVING', HAVING)
        Tool.Set_Option('ORDER', ORDER)
        Tool.Set_Option('DISTINCT', DISTINCT)
        return Tool.Execute(Verbose)
    return False

def run_tool_db_odbc_8(TABLE=None, CONNECTION=None, TABLES=None, FIELDS=None, WHERE=None, GROUP=None, HAVING=None, ORDER=None, DISTINCT=None, Verbose=2):
    '''
    Import Table from Query
    ----------
    [db_odbc.8]\n
    Import a table from a database through ODBC via SQL query.\n
    > SELECT [DISTINCT|ALL] 'Fields' FROM 'Tables' WHERE 'Where' [GROUP BY 'Group' [HAVING 'Having']] [ORDER BY 'Order']\n
    Arguments
    ----------
    - TABLE [`output table`] : Table
    - CONNECTION [`choice`] : Connection. Available Choices: Default: 0
    - TABLES [`text`] : Tables
    - FIELDS [`text`] : Fields. Default: *
    - WHERE [`text`] : Where
    - GROUP [`text`] : Group
    - HAVING [`text`] : Having
    - ORDER [`text`] : Order
    - DISTINCT [`boolean`] : Distinct. Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('db_odbc', '8', 'Import Table from Query')
    if Tool.is_Okay():
        Tool.Set_Output('TABLE', TABLE)
        Tool.Set_Option('CONNECTION', CONNECTION)
        Tool.Set_Option('TABLES', TABLES)
        Tool.Set_Option('FIELDS', FIELDS)
        Tool.Set_Option('WHERE', WHERE)
        Tool.Set_Option('GROUP', GROUP)
        Tool.Set_Option('HAVING', HAVING)
        Tool.Set_Option('ORDER', ORDER)
        Tool.Set_Option('DISTINCT', DISTINCT)
        return Tool.Execute(Verbose)
    return False

def List_Data_Sources(SOURCES=None, CONNECTED=None, Verbose=2):
    '''
    List Data Sources
    ----------
    [db_odbc.9]\n
    Lists all ODBC sources.\n
    Arguments
    ----------
    - SOURCES [`output table`] : Data Sources
    - CONNECTED [`boolean`] : Only List Connected Sources. Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('db_odbc', '9', 'List Data Sources')
    if Tool.is_Okay():
        Tool.Set_Output('SOURCES', SOURCES)
        Tool.Set_Option('CONNECTED', CONNECTED)
        return Tool.Execute(Verbose)
    return False

def run_tool_db_odbc_9(SOURCES=None, CONNECTED=None, Verbose=2):
    '''
    List Data Sources
    ----------
    [db_odbc.9]\n
    Lists all ODBC sources.\n
    Arguments
    ----------
    - SOURCES [`output table`] : Data Sources
    - CONNECTED [`boolean`] : Only List Connected Sources. Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('db_odbc', '9', 'List Data Sources')
    if Tool.is_Okay():
        Tool.Set_Output('SOURCES', SOURCES)
        Tool.Set_Option('CONNECTED', CONNECTED)
        return Tool.Execute(Verbose)
    return False

def List_Tables(TABLES=None, CONNECTION=None, Verbose=2):
    '''
    List Tables
    ----------
    [db_odbc.10]\n
    Lists all tables of an ODBC data source.\n
    Arguments
    ----------
    - TABLES [`output table`] : Tables
    - CONNECTION [`choice`] : Connection. Available Choices: Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('db_odbc', '10', 'List Tables')
    if Tool.is_Okay():
        Tool.Set_Output('TABLES', TABLES)
        Tool.Set_Option('CONNECTION', CONNECTION)
        return Tool.Execute(Verbose)
    return False

def run_tool_db_odbc_10(TABLES=None, CONNECTION=None, Verbose=2):
    '''
    List Tables
    ----------
    [db_odbc.10]\n
    Lists all tables of an ODBC data source.\n
    Arguments
    ----------
    - TABLES [`output table`] : Tables
    - CONNECTION [`choice`] : Connection. Available Choices: Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('db_odbc', '10', 'List Tables')
    if Tool.is_Okay():
        Tool.Set_Output('TABLES', TABLES)
        Tool.Set_Option('CONNECTION', CONNECTION)
        return Tool.Execute(Verbose)
    return False

def Disconnect_All(TRANSACT=None, Verbose=2):
    '''
    Disconnect All
    ----------
    [db_odbc.11]\n
    Disconnects all connected ODBC sources.\n
    Arguments
    ----------
    - TRANSACT [`choice`] : Transactions. Available Choices: [0] rollback [1] commit Default: 1

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('db_odbc', '11', 'Disconnect All')
    if Tool.is_Okay():
        Tool.Set_Option('TRANSACT', TRANSACT)
        return Tool.Execute(Verbose)
    return False

def run_tool_db_odbc_11(TRANSACT=None, Verbose=2):
    '''
    Disconnect All
    ----------
    [db_odbc.11]\n
    Disconnects all connected ODBC sources.\n
    Arguments
    ----------
    - TRANSACT [`choice`] : Transactions. Available Choices: [0] rollback [1] commit Default: 1

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('db_odbc', '11', 'Disconnect All')
    if Tool.is_Okay():
        Tool.Set_Option('TRANSACT', TRANSACT)
        return Tool.Execute(Verbose)
    return False

