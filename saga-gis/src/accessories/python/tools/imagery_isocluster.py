#! /usr/bin/env python

'''
Python Interface to SAGA Tools Library
----------
- Category : Imagery
- Name     : ISODATA Clustering
- ID       : imagery_isocluster

Description
----------
ISODATA Clustering tools
'''

from PySAGA.helper import Tool_Wrapper

def ISODATA_Clustering_for_Grids(FEATURES=None, CLUSTER=None, STATISTICS=None, NORMALIZE=None, ITERATIONS=None, CLUSTER_INI=None, CLUSTER_MAX=None, SAMPLES_MIN=None, INITIALIZE=None, Verbose=2):
    '''
    ISODATA Clustering for Grids
    ----------
    [imagery_isocluster.0]\n
    This tool executes the Isodata unsupervised classification - clustering algorithm. Isodata stands for Iterative Self-Organizing Data Analysis Techniques. This is a more sophisticated algorithm which allows the number of clusters to be automatically adjusted during the iteration by merging similar clusters and splitting clusters with large standard deviations. The tool is based on Christos Iosifidis' Isodata implementation.\n
    Arguments
    ----------
    - FEATURES [`input grid list`] : Features
    - CLUSTER [`output grid`] : Clusters
    - STATISTICS [`output table`] : Statistics
    - NORMALIZE [`boolean`] : Normalize. Default: 0
    - ITERATIONS [`integer number`] : Maximum Number of Iterations. Minimum: 3 Default: 20
    - CLUSTER_INI [`integer number`] : Initial Number of Clusters. Minimum: 0 Default: 5
    - CLUSTER_MAX [`integer number`] : Maximum Number of Clusters. Minimum: 3 Default: 16
    - SAMPLES_MIN [`integer number`] : Minimum Number of Samples in a Cluster. Minimum: 2 Default: 5
    - INITIALIZE [`choice`] : Start Partition. Available Choices: [0] random [1] periodical [2] keep values Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('imagery_isocluster', '0', 'ISODATA Clustering for Grids')
    if Tool.is_Okay():
        Tool.Set_Input ('FEATURES', FEATURES)
        Tool.Set_Output('CLUSTER', CLUSTER)
        Tool.Set_Output('STATISTICS', STATISTICS)
        Tool.Set_Option('NORMALIZE', NORMALIZE)
        Tool.Set_Option('ITERATIONS', ITERATIONS)
        Tool.Set_Option('CLUSTER_INI', CLUSTER_INI)
        Tool.Set_Option('CLUSTER_MAX', CLUSTER_MAX)
        Tool.Set_Option('SAMPLES_MIN', SAMPLES_MIN)
        Tool.Set_Option('INITIALIZE', INITIALIZE)
        return Tool.Execute(Verbose)
    return False

def run_tool_imagery_isocluster_0(FEATURES=None, CLUSTER=None, STATISTICS=None, NORMALIZE=None, ITERATIONS=None, CLUSTER_INI=None, CLUSTER_MAX=None, SAMPLES_MIN=None, INITIALIZE=None, Verbose=2):
    '''
    ISODATA Clustering for Grids
    ----------
    [imagery_isocluster.0]\n
    This tool executes the Isodata unsupervised classification - clustering algorithm. Isodata stands for Iterative Self-Organizing Data Analysis Techniques. This is a more sophisticated algorithm which allows the number of clusters to be automatically adjusted during the iteration by merging similar clusters and splitting clusters with large standard deviations. The tool is based on Christos Iosifidis' Isodata implementation.\n
    Arguments
    ----------
    - FEATURES [`input grid list`] : Features
    - CLUSTER [`output grid`] : Clusters
    - STATISTICS [`output table`] : Statistics
    - NORMALIZE [`boolean`] : Normalize. Default: 0
    - ITERATIONS [`integer number`] : Maximum Number of Iterations. Minimum: 3 Default: 20
    - CLUSTER_INI [`integer number`] : Initial Number of Clusters. Minimum: 0 Default: 5
    - CLUSTER_MAX [`integer number`] : Maximum Number of Clusters. Minimum: 3 Default: 16
    - SAMPLES_MIN [`integer number`] : Minimum Number of Samples in a Cluster. Minimum: 2 Default: 5
    - INITIALIZE [`choice`] : Start Partition. Available Choices: [0] random [1] periodical [2] keep values Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('imagery_isocluster', '0', 'ISODATA Clustering for Grids')
    if Tool.is_Okay():
        Tool.Set_Input ('FEATURES', FEATURES)
        Tool.Set_Output('CLUSTER', CLUSTER)
        Tool.Set_Output('STATISTICS', STATISTICS)
        Tool.Set_Option('NORMALIZE', NORMALIZE)
        Tool.Set_Option('ITERATIONS', ITERATIONS)
        Tool.Set_Option('CLUSTER_INI', CLUSTER_INI)
        Tool.Set_Option('CLUSTER_MAX', CLUSTER_MAX)
        Tool.Set_Option('SAMPLES_MIN', SAMPLES_MIN)
        Tool.Set_Option('INITIALIZE', INITIALIZE)
        return Tool.Execute(Verbose)
    return False

