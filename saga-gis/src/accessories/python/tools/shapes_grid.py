#! /usr/bin/env python

'''
Python Interface to SAGA Tools Library
----------
- Category : Shapes
- Name     : Shapes-Grid Tools
- ID       : shapes_grid

Description
----------
Tools related to gridded and vector data (conversions, combinations, etc.).
'''

from PySAGA.helper import Tool_Wrapper

def Add_Grid_Values_to_Points(SHAPES=None, GRIDS=None, RESULT=None, RESAMPLING=None, Verbose=2):
    '''
    Add Grid Values to Points
    ----------
    [shapes_grid.0]\n
    Spatial Join: Retrieves information from the selected grids at the positions of the points of the selected points layer and adds it to the resulting layer.\n
    Arguments
    ----------
    - SHAPES [`input shapes`] : Points
    - GRIDS [`input grid list`] : Grids
    - RESULT [`output shapes`] : Result
    - RESAMPLING [`choice`] : Resampling. Available Choices: [0] Nearest Neighbour [1] Bilinear Interpolation [2] Bicubic Spline Interpolation [3] B-Spline Interpolation Default: 3

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_grid', '0', 'Add Grid Values to Points')
    if Tool.is_Okay():
        Tool.Set_Input ('SHAPES', SHAPES)
        Tool.Set_Input ('GRIDS', GRIDS)
        Tool.Set_Output('RESULT', RESULT)
        Tool.Set_Option('RESAMPLING', RESAMPLING)
        return Tool.Execute(Verbose)
    return False

def run_tool_shapes_grid_0(SHAPES=None, GRIDS=None, RESULT=None, RESAMPLING=None, Verbose=2):
    '''
    Add Grid Values to Points
    ----------
    [shapes_grid.0]\n
    Spatial Join: Retrieves information from the selected grids at the positions of the points of the selected points layer and adds it to the resulting layer.\n
    Arguments
    ----------
    - SHAPES [`input shapes`] : Points
    - GRIDS [`input grid list`] : Grids
    - RESULT [`output shapes`] : Result
    - RESAMPLING [`choice`] : Resampling. Available Choices: [0] Nearest Neighbour [1] Bilinear Interpolation [2] Bicubic Spline Interpolation [3] B-Spline Interpolation Default: 3

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_grid', '0', 'Add Grid Values to Points')
    if Tool.is_Okay():
        Tool.Set_Input ('SHAPES', SHAPES)
        Tool.Set_Input ('GRIDS', GRIDS)
        Tool.Set_Output('RESULT', RESULT)
        Tool.Set_Option('RESAMPLING', RESAMPLING)
        return Tool.Execute(Verbose)
    return False

def Add_Grid_Values_to_Shapes(SHAPES=None, GRIDS=None, RESULT=None, RESAMPLING=None, Verbose=2):
    '''
    Add Grid Values to Shapes
    ----------
    [shapes_grid.1]\n
    Spatial Join: Retrieves information from the selected grids at the positions of the shapes of the selected shapes layer and adds it to the resulting shapes layer. For points this is similar to 'Add Grid Values to Points' tool. For lines and polygons average values will be calculated from interfering grid cells. For polygons the 'Grid Statistics for Polygons' tool offers more advanced options.\n
    Arguments
    ----------
    - SHAPES [`input shapes`] : Shapes
    - GRIDS [`input grid list`] : Grids
    - RESULT [`output shapes`] : Result
    - RESAMPLING [`choice`] : Resampling. Available Choices: [0] Nearest Neighbour [1] Bilinear Interpolation [2] Bicubic Spline Interpolation [3] B-Spline Interpolation Default: 3

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_grid', '1', 'Add Grid Values to Shapes')
    if Tool.is_Okay():
        Tool.Set_Input ('SHAPES', SHAPES)
        Tool.Set_Input ('GRIDS', GRIDS)
        Tool.Set_Output('RESULT', RESULT)
        Tool.Set_Option('RESAMPLING', RESAMPLING)
        return Tool.Execute(Verbose)
    return False

def run_tool_shapes_grid_1(SHAPES=None, GRIDS=None, RESULT=None, RESAMPLING=None, Verbose=2):
    '''
    Add Grid Values to Shapes
    ----------
    [shapes_grid.1]\n
    Spatial Join: Retrieves information from the selected grids at the positions of the shapes of the selected shapes layer and adds it to the resulting shapes layer. For points this is similar to 'Add Grid Values to Points' tool. For lines and polygons average values will be calculated from interfering grid cells. For polygons the 'Grid Statistics for Polygons' tool offers more advanced options.\n
    Arguments
    ----------
    - SHAPES [`input shapes`] : Shapes
    - GRIDS [`input grid list`] : Grids
    - RESULT [`output shapes`] : Result
    - RESAMPLING [`choice`] : Resampling. Available Choices: [0] Nearest Neighbour [1] Bilinear Interpolation [2] Bicubic Spline Interpolation [3] B-Spline Interpolation Default: 3

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_grid', '1', 'Add Grid Values to Shapes')
    if Tool.is_Okay():
        Tool.Set_Input ('SHAPES', SHAPES)
        Tool.Set_Input ('GRIDS', GRIDS)
        Tool.Set_Output('RESULT', RESULT)
        Tool.Set_Option('RESAMPLING', RESAMPLING)
        return Tool.Execute(Verbose)
    return False

def Grid_Statistics_for_Polygons(GRIDS=None, POLYGONS=None, RESULT=None, NAMING=None, METHOD=None, PARALLELIZED=None, COUNT=None, MIN=None, MAX=None, RANGE=None, SUM=None, MEAN=None, VAR=None, STDDEV=None, GINI=None, QUANTILES=None, Verbose=2):
    '''
    Grid Statistics for Polygons
    ----------
    [shapes_grid.2]\n
    Zonal grid statistics. For each polygon statistics based on all covered grid cells will be calculated.\n
    Arguments
    ----------
    - GRIDS [`input grid list`] : Grids
    - POLYGONS [`input shapes`] : Polygons
    - RESULT [`output shapes`] : Statistics
    - NAMING [`choice`] : Field Naming. Available Choices: [0] grid number [1] grid name Default: 1
    - METHOD [`choice`] : Method. Available Choices: [0] simple and fast [1] polygon wise (cell centers) [2] polygon wise (cell area) [3] polygon wise (cell area weighted) Default: 0
    - PARALLELIZED [`boolean`] : Use Multiple Cores. Default: 0
    - COUNT [`boolean`] : Number of Cells. Default: 1
    - MIN [`boolean`] : Minimum. Default: 1
    - MAX [`boolean`] : Maximum. Default: 1
    - RANGE [`boolean`] : Range. Default: 1
    - SUM [`boolean`] : Sum. Default: 1
    - MEAN [`boolean`] : Mean. Default: 1
    - VAR [`boolean`] : Variance. Default: 1
    - STDDEV [`boolean`] : Standard Deviation. Default: 1
    - GINI [`boolean`] : Gini. Default: 0
    - QUANTILES [`text`] : Percentiles. Separate the desired percentiles by semicolon, e.g. "5; 25; 50; 75; 95"

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_grid', '2', 'Grid Statistics for Polygons')
    if Tool.is_Okay():
        Tool.Set_Input ('GRIDS', GRIDS)
        Tool.Set_Input ('POLYGONS', POLYGONS)
        Tool.Set_Output('RESULT', RESULT)
        Tool.Set_Option('NAMING', NAMING)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('PARALLELIZED', PARALLELIZED)
        Tool.Set_Option('COUNT', COUNT)
        Tool.Set_Option('MIN', MIN)
        Tool.Set_Option('MAX', MAX)
        Tool.Set_Option('RANGE', RANGE)
        Tool.Set_Option('SUM', SUM)
        Tool.Set_Option('MEAN', MEAN)
        Tool.Set_Option('VAR', VAR)
        Tool.Set_Option('STDDEV', STDDEV)
        Tool.Set_Option('GINI', GINI)
        Tool.Set_Option('QUANTILES', QUANTILES)
        return Tool.Execute(Verbose)
    return False

def run_tool_shapes_grid_2(GRIDS=None, POLYGONS=None, RESULT=None, NAMING=None, METHOD=None, PARALLELIZED=None, COUNT=None, MIN=None, MAX=None, RANGE=None, SUM=None, MEAN=None, VAR=None, STDDEV=None, GINI=None, QUANTILES=None, Verbose=2):
    '''
    Grid Statistics for Polygons
    ----------
    [shapes_grid.2]\n
    Zonal grid statistics. For each polygon statistics based on all covered grid cells will be calculated.\n
    Arguments
    ----------
    - GRIDS [`input grid list`] : Grids
    - POLYGONS [`input shapes`] : Polygons
    - RESULT [`output shapes`] : Statistics
    - NAMING [`choice`] : Field Naming. Available Choices: [0] grid number [1] grid name Default: 1
    - METHOD [`choice`] : Method. Available Choices: [0] simple and fast [1] polygon wise (cell centers) [2] polygon wise (cell area) [3] polygon wise (cell area weighted) Default: 0
    - PARALLELIZED [`boolean`] : Use Multiple Cores. Default: 0
    - COUNT [`boolean`] : Number of Cells. Default: 1
    - MIN [`boolean`] : Minimum. Default: 1
    - MAX [`boolean`] : Maximum. Default: 1
    - RANGE [`boolean`] : Range. Default: 1
    - SUM [`boolean`] : Sum. Default: 1
    - MEAN [`boolean`] : Mean. Default: 1
    - VAR [`boolean`] : Variance. Default: 1
    - STDDEV [`boolean`] : Standard Deviation. Default: 1
    - GINI [`boolean`] : Gini. Default: 0
    - QUANTILES [`text`] : Percentiles. Separate the desired percentiles by semicolon, e.g. "5; 25; 50; 75; 95"

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_grid', '2', 'Grid Statistics for Polygons')
    if Tool.is_Okay():
        Tool.Set_Input ('GRIDS', GRIDS)
        Tool.Set_Input ('POLYGONS', POLYGONS)
        Tool.Set_Output('RESULT', RESULT)
        Tool.Set_Option('NAMING', NAMING)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('PARALLELIZED', PARALLELIZED)
        Tool.Set_Option('COUNT', COUNT)
        Tool.Set_Option('MIN', MIN)
        Tool.Set_Option('MAX', MAX)
        Tool.Set_Option('RANGE', RANGE)
        Tool.Set_Option('SUM', SUM)
        Tool.Set_Option('MEAN', MEAN)
        Tool.Set_Option('VAR', VAR)
        Tool.Set_Option('STDDEV', STDDEV)
        Tool.Set_Option('GINI', GINI)
        Tool.Set_Option('QUANTILES', QUANTILES)
        return Tool.Execute(Verbose)
    return False

def Grid_Cells_to_PointsPolygons(GRIDS=None, POLYGONS=None, POINTS=None, CELLS=None, ATTRIBUTE=None, NODATA=None, TYPE=None, Verbose=2):
    '''
    Grid Cells to Points/Polygons
    ----------
    [shapes_grid.3]\n
    This tool saves grid cell values to point (grid nodes) or polygon (grid cells) shapes. Optionally only points can be saved, which are contained by polygons of the specified polygons layer. In addition, it is possible to exclude all cells that are coded NoData in the first grid of the grid list.\n
    Arguments
    ----------
    - GRIDS [`input grid list`] : Grids
    - POLYGONS [`optional input shapes`] : Polygons
    - POINTS [`output shapes`] : Points
    - CELLS [`output shapes`] : Points
    - ATTRIBUTE [`table field`] : Attribute
    - NODATA [`choice`] : No-Data Cells. Available Choices: [0] include all cells [1] include cell if at least one grid provides data [2] exclude cell if at least one grid does not provide data Default: 0
    - TYPE [`choice`] : Type. Available Choices: [0] nodes [1] cells Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_grid', '3', 'Grid Cells to Points/Polygons')
    if Tool.is_Okay():
        Tool.Set_Input ('GRIDS', GRIDS)
        Tool.Set_Input ('POLYGONS', POLYGONS)
        Tool.Set_Output('POINTS', POINTS)
        Tool.Set_Output('CELLS', CELLS)
        Tool.Set_Option('ATTRIBUTE', ATTRIBUTE)
        Tool.Set_Option('NODATA', NODATA)
        Tool.Set_Option('TYPE', TYPE)
        return Tool.Execute(Verbose)
    return False

def run_tool_shapes_grid_3(GRIDS=None, POLYGONS=None, POINTS=None, CELLS=None, ATTRIBUTE=None, NODATA=None, TYPE=None, Verbose=2):
    '''
    Grid Cells to Points/Polygons
    ----------
    [shapes_grid.3]\n
    This tool saves grid cell values to point (grid nodes) or polygon (grid cells) shapes. Optionally only points can be saved, which are contained by polygons of the specified polygons layer. In addition, it is possible to exclude all cells that are coded NoData in the first grid of the grid list.\n
    Arguments
    ----------
    - GRIDS [`input grid list`] : Grids
    - POLYGONS [`optional input shapes`] : Polygons
    - POINTS [`output shapes`] : Points
    - CELLS [`output shapes`] : Points
    - ATTRIBUTE [`table field`] : Attribute
    - NODATA [`choice`] : No-Data Cells. Available Choices: [0] include all cells [1] include cell if at least one grid provides data [2] exclude cell if at least one grid does not provide data Default: 0
    - TYPE [`choice`] : Type. Available Choices: [0] nodes [1] cells Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_grid', '3', 'Grid Cells to Points/Polygons')
    if Tool.is_Okay():
        Tool.Set_Input ('GRIDS', GRIDS)
        Tool.Set_Input ('POLYGONS', POLYGONS)
        Tool.Set_Output('POINTS', POINTS)
        Tool.Set_Output('CELLS', CELLS)
        Tool.Set_Option('ATTRIBUTE', ATTRIBUTE)
        Tool.Set_Option('NODATA', NODATA)
        Tool.Set_Option('TYPE', TYPE)
        return Tool.Execute(Verbose)
    return False

def Grid_Values_to_Points_randomly(GRID=None, POINTS=None, FREQ=None, Verbose=2):
    '''
    Grid Values to Points (randomly)
    ----------
    [shapes_grid.4]\n
    Extract randomly points from gridded data.\n
    Arguments
    ----------
    - GRID [`input grid`] : Grid
    - POINTS [`output shapes`] : Points
    - FREQ [`integer number`] : Frequency. Minimum: 1 Default: 100 One per x

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_grid', '4', 'Grid Values to Points (randomly)')
    if Tool.is_Okay():
        Tool.Set_Input ('GRID', GRID)
        Tool.Set_Output('POINTS', POINTS)
        Tool.Set_Option('FREQ', FREQ)
        return Tool.Execute(Verbose)
    return False

def run_tool_shapes_grid_4(GRID=None, POINTS=None, FREQ=None, Verbose=2):
    '''
    Grid Values to Points (randomly)
    ----------
    [shapes_grid.4]\n
    Extract randomly points from gridded data.\n
    Arguments
    ----------
    - GRID [`input grid`] : Grid
    - POINTS [`output shapes`] : Points
    - FREQ [`integer number`] : Frequency. Minimum: 1 Default: 100 One per x

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_grid', '4', 'Grid Values to Points (randomly)')
    if Tool.is_Okay():
        Tool.Set_Input ('GRID', GRID)
        Tool.Set_Output('POINTS', POINTS)
        Tool.Set_Option('FREQ', FREQ)
        return Tool.Execute(Verbose)
    return False

def Contour_Lines_from_Grid(GRID=None, CONTOUR=None, POLYGONS=None, GRID_GRIDSYSTEM=None, VERTEX=None, LINE_OMP=None, LINE_PARTS=None, POLY_OMP=None, POLY_PARTS=None, PRECISION=None, SCALE=None, BOUNDARY=None, MINLENGTH=None, INTERVALS=None, ZMIN=None, ZMAX=None, ZSTEP=None, ZLIST=None, Verbose=2):
    '''
    Contour Lines from Grid
    ----------
    [shapes_grid.5]\n
    Derive contour lines (isolines) from a grid.\n
    Arguments
    ----------
    - GRID [`input grid`] : Grid
    - CONTOUR [`output shapes`] : Contour
    - POLYGONS [`output shapes`] : Polygons
    - GRID_GRIDSYSTEM [`grid system`] : Grid system
    - VERTEX [`choice`] : Vertex Type. Available Choices: [0] x, y [1] x, y, z Default: 0
    - LINE_OMP [`boolean`] : Parallel Processing. Default: 1
    - LINE_PARTS [`boolean`] : Split Line Parts. Default: 1
    - POLY_OMP [`boolean`] : Parallel Processing. Default: 1
    - POLY_PARTS [`boolean`] : Split Polygon Parts. Default: 1
    - PRECISION [`boolean`] : Coordinate Precision Fix. Default: 0 Check to avoid coordinate precision issues with polygon construction, particularly helpful with geographic coordinates or in general with comparatively small cell sizes.
    - SCALE [`floating point number`] : Interpolation Scale. Minimum: 0.000000 Default: 1.000000 set greater one for line smoothing
    - BOUNDARY [`boolean`] : Boundary Extension. Default: 0 Extend contours beyond boundary. Internally input grid is extrapolated by one cell size in each direction (top, down, left, right).
    - MINLENGTH [`floating point number`] : Minimum Length. Minimum: 0.000000 Default: 0.000000 Contour line segments with minimum length [map units] or less will be removed from resulting data set.
    - INTERVALS [`choice`] : Interval Definition. Available Choices: [0] single value [1] equal intervals [2] from list Default: 1
    - ZMIN [`floating point number`] : Base Contour Value. Default: 0.000000
    - ZMAX [`floating point number`] : Maximum Contour Value. Default: 1000.000000
    - ZSTEP [`floating point number`] : Contour Interval. Minimum: 0.000000 Default: 100.000000
    - ZLIST [`text`] : Contour Values. Default: 0, 10, 20, 50, 100, 200, 500, 1000 List of comma separated values.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_grid', '5', 'Contour Lines from Grid')
    if Tool.is_Okay():
        Tool.Set_Input ('GRID', GRID)
        Tool.Set_Output('CONTOUR', CONTOUR)
        Tool.Set_Output('POLYGONS', POLYGONS)
        Tool.Set_Option('GRID_GRIDSYSTEM', GRID_GRIDSYSTEM)
        Tool.Set_Option('VERTEX', VERTEX)
        Tool.Set_Option('LINE_OMP', LINE_OMP)
        Tool.Set_Option('LINE_PARTS', LINE_PARTS)
        Tool.Set_Option('POLY_OMP', POLY_OMP)
        Tool.Set_Option('POLY_PARTS', POLY_PARTS)
        Tool.Set_Option('PRECISION', PRECISION)
        Tool.Set_Option('SCALE', SCALE)
        Tool.Set_Option('BOUNDARY', BOUNDARY)
        Tool.Set_Option('MINLENGTH', MINLENGTH)
        Tool.Set_Option('INTERVALS', INTERVALS)
        Tool.Set_Option('ZMIN', ZMIN)
        Tool.Set_Option('ZMAX', ZMAX)
        Tool.Set_Option('ZSTEP', ZSTEP)
        Tool.Set_Option('ZLIST', ZLIST)
        return Tool.Execute(Verbose)
    return False

def run_tool_shapes_grid_5(GRID=None, CONTOUR=None, POLYGONS=None, GRID_GRIDSYSTEM=None, VERTEX=None, LINE_OMP=None, LINE_PARTS=None, POLY_OMP=None, POLY_PARTS=None, PRECISION=None, SCALE=None, BOUNDARY=None, MINLENGTH=None, INTERVALS=None, ZMIN=None, ZMAX=None, ZSTEP=None, ZLIST=None, Verbose=2):
    '''
    Contour Lines from Grid
    ----------
    [shapes_grid.5]\n
    Derive contour lines (isolines) from a grid.\n
    Arguments
    ----------
    - GRID [`input grid`] : Grid
    - CONTOUR [`output shapes`] : Contour
    - POLYGONS [`output shapes`] : Polygons
    - GRID_GRIDSYSTEM [`grid system`] : Grid system
    - VERTEX [`choice`] : Vertex Type. Available Choices: [0] x, y [1] x, y, z Default: 0
    - LINE_OMP [`boolean`] : Parallel Processing. Default: 1
    - LINE_PARTS [`boolean`] : Split Line Parts. Default: 1
    - POLY_OMP [`boolean`] : Parallel Processing. Default: 1
    - POLY_PARTS [`boolean`] : Split Polygon Parts. Default: 1
    - PRECISION [`boolean`] : Coordinate Precision Fix. Default: 0 Check to avoid coordinate precision issues with polygon construction, particularly helpful with geographic coordinates or in general with comparatively small cell sizes.
    - SCALE [`floating point number`] : Interpolation Scale. Minimum: 0.000000 Default: 1.000000 set greater one for line smoothing
    - BOUNDARY [`boolean`] : Boundary Extension. Default: 0 Extend contours beyond boundary. Internally input grid is extrapolated by one cell size in each direction (top, down, left, right).
    - MINLENGTH [`floating point number`] : Minimum Length. Minimum: 0.000000 Default: 0.000000 Contour line segments with minimum length [map units] or less will be removed from resulting data set.
    - INTERVALS [`choice`] : Interval Definition. Available Choices: [0] single value [1] equal intervals [2] from list Default: 1
    - ZMIN [`floating point number`] : Base Contour Value. Default: 0.000000
    - ZMAX [`floating point number`] : Maximum Contour Value. Default: 1000.000000
    - ZSTEP [`floating point number`] : Contour Interval. Minimum: 0.000000 Default: 100.000000
    - ZLIST [`text`] : Contour Values. Default: 0, 10, 20, 50, 100, 200, 500, 1000 List of comma separated values.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_grid', '5', 'Contour Lines from Grid')
    if Tool.is_Okay():
        Tool.Set_Input ('GRID', GRID)
        Tool.Set_Output('CONTOUR', CONTOUR)
        Tool.Set_Output('POLYGONS', POLYGONS)
        Tool.Set_Option('GRID_GRIDSYSTEM', GRID_GRIDSYSTEM)
        Tool.Set_Option('VERTEX', VERTEX)
        Tool.Set_Option('LINE_OMP', LINE_OMP)
        Tool.Set_Option('LINE_PARTS', LINE_PARTS)
        Tool.Set_Option('POLY_OMP', POLY_OMP)
        Tool.Set_Option('POLY_PARTS', POLY_PARTS)
        Tool.Set_Option('PRECISION', PRECISION)
        Tool.Set_Option('SCALE', SCALE)
        Tool.Set_Option('BOUNDARY', BOUNDARY)
        Tool.Set_Option('MINLENGTH', MINLENGTH)
        Tool.Set_Option('INTERVALS', INTERVALS)
        Tool.Set_Option('ZMIN', ZMIN)
        Tool.Set_Option('ZMAX', ZMAX)
        Tool.Set_Option('ZSTEP', ZSTEP)
        Tool.Set_Option('ZLIST', ZLIST)
        return Tool.Execute(Verbose)
    return False

def Vectorizing_Grid_Classes(GRID=None, POLYGONS=None, CLASS_ALL=None, CLASS_ID=None, SPLIT=None, ALLVERTICES=None, Verbose=2):
    '''
    Vectorizing Grid Classes
    ----------
    [shapes_grid.6]\n
    Vectorising grid classes.\n
    Arguments
    ----------
    - GRID [`input grid`] : Grid
    - POLYGONS [`output shapes`] : Polygons
    - CLASS_ALL [`choice`] : Class Selection. Available Choices: [0] one single class specified by class identifier [1] all classes Default: 1
    - CLASS_ID [`floating point number`] : Class Identifier. Default: 1.000000
    - SPLIT [`choice`] : Vectorised class as.... Available Choices: [0] one single (multi-)polygon object [1] each island as separated polygon Default: 0
    - ALLVERTICES [`boolean`] : Keep Vertices on Straight Lines. Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_grid', '6', 'Vectorizing Grid Classes')
    if Tool.is_Okay():
        Tool.Set_Input ('GRID', GRID)
        Tool.Set_Output('POLYGONS', POLYGONS)
        Tool.Set_Option('CLASS_ALL', CLASS_ALL)
        Tool.Set_Option('CLASS_ID', CLASS_ID)
        Tool.Set_Option('SPLIT', SPLIT)
        Tool.Set_Option('ALLVERTICES', ALLVERTICES)
        return Tool.Execute(Verbose)
    return False

def run_tool_shapes_grid_6(GRID=None, POLYGONS=None, CLASS_ALL=None, CLASS_ID=None, SPLIT=None, ALLVERTICES=None, Verbose=2):
    '''
    Vectorizing Grid Classes
    ----------
    [shapes_grid.6]\n
    Vectorising grid classes.\n
    Arguments
    ----------
    - GRID [`input grid`] : Grid
    - POLYGONS [`output shapes`] : Polygons
    - CLASS_ALL [`choice`] : Class Selection. Available Choices: [0] one single class specified by class identifier [1] all classes Default: 1
    - CLASS_ID [`floating point number`] : Class Identifier. Default: 1.000000
    - SPLIT [`choice`] : Vectorised class as.... Available Choices: [0] one single (multi-)polygon object [1] each island as separated polygon Default: 0
    - ALLVERTICES [`boolean`] : Keep Vertices on Straight Lines. Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_grid', '6', 'Vectorizing Grid Classes')
    if Tool.is_Okay():
        Tool.Set_Input ('GRID', GRID)
        Tool.Set_Output('POLYGONS', POLYGONS)
        Tool.Set_Option('CLASS_ALL', CLASS_ALL)
        Tool.Set_Option('CLASS_ID', CLASS_ID)
        Tool.Set_Option('SPLIT', SPLIT)
        Tool.Set_Option('ALLVERTICES', ALLVERTICES)
        return Tool.Execute(Verbose)
    return False

def Clip_Grid_with_Polygon(INPUT=None, POLYGONS=None, OUTPUT=None, EXTENT=None, Verbose=2):
    '''
    Clip Grid with Polygon
    ----------
    [shapes_grid.7]\n
    Clips the input grid with a polygon shapefile. Select polygons from the shapefile prior to tool execution in case you like to use only a subset from the shapefile for clipping.\n
    Arguments
    ----------
    - INPUT [`input grid list`] : Input
    - POLYGONS [`input shapes`] : Polygons
    - OUTPUT [`output grid list`] : Output
    - EXTENT [`choice`] : Target Extent. Available Choices: [0] original [1] polygons [2] crop to data Default: 1

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_grid', '7', 'Clip Grid with Polygon')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Input ('POLYGONS', POLYGONS)
        Tool.Set_Output('OUTPUT', OUTPUT)
        Tool.Set_Option('EXTENT', EXTENT)
        return Tool.Execute(Verbose)
    return False

def run_tool_shapes_grid_7(INPUT=None, POLYGONS=None, OUTPUT=None, EXTENT=None, Verbose=2):
    '''
    Clip Grid with Polygon
    ----------
    [shapes_grid.7]\n
    Clips the input grid with a polygon shapefile. Select polygons from the shapefile prior to tool execution in case you like to use only a subset from the shapefile for clipping.\n
    Arguments
    ----------
    - INPUT [`input grid list`] : Input
    - POLYGONS [`input shapes`] : Polygons
    - OUTPUT [`output grid list`] : Output
    - EXTENT [`choice`] : Target Extent. Available Choices: [0] original [1] polygons [2] crop to data Default: 1

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_grid', '7', 'Clip Grid with Polygon')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Input ('POLYGONS', POLYGONS)
        Tool.Set_Output('OUTPUT', OUTPUT)
        Tool.Set_Option('EXTENT', EXTENT)
        return Tool.Execute(Verbose)
    return False

def Grid_Statistics_for_Points(GRIDS=None, POINTS=None, RESULT=None, KERNEL_TYPE=None, KERNEL_SIZE=None, NAMING=None, COUNT=None, MIN=None, MAX=None, RANGE=None, SUM=None, MEAN=None, VAR=None, STDDEV=None, QUANTILE=None, Verbose=2):
    '''
    Grid Statistics for Points
    ----------
    [shapes_grid.8]\n
    For each given point statistics based on all grid cells in the defined neighbourhood will be calculated.\n
    Arguments
    ----------
    - GRIDS [`input grid list`] : Grids
    - POINTS [`input shapes`] : Points
    - RESULT [`output shapes`] : Statistics
    - KERNEL_TYPE [`choice`] : Kernel Type. Available Choices: [0] square [1] circle Default: 0
    - KERNEL_SIZE [`integer number`] : Kernel Size. Minimum: 1 Default: 1 kernel size defined as radius number of cells
    - NAMING [`choice`] : Field Naming. Available Choices: [0] grid number [1] grid name Default: 1
    - COUNT [`boolean`] : Number of Cells. Default: 1
    - MIN [`boolean`] : Minimum. Default: 1
    - MAX [`boolean`] : Maximum. Default: 1
    - RANGE [`boolean`] : Range. Default: 1
    - SUM [`boolean`] : Sum. Default: 1
    - MEAN [`boolean`] : Mean. Default: 1
    - VAR [`boolean`] : Variance. Default: 1
    - STDDEV [`boolean`] : Standard Deviation. Default: 1
    - QUANTILE [`integer number`] : Percentile. Minimum: 0 Maximum: 50 Default: 0 Calculate distribution percentiles. Value specifies interval (median=50, quartiles=25, deciles=10, ...). Set to zero to omit percentile calculation.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_grid', '8', 'Grid Statistics for Points')
    if Tool.is_Okay():
        Tool.Set_Input ('GRIDS', GRIDS)
        Tool.Set_Input ('POINTS', POINTS)
        Tool.Set_Output('RESULT', RESULT)
        Tool.Set_Option('KERNEL_TYPE', KERNEL_TYPE)
        Tool.Set_Option('KERNEL_SIZE', KERNEL_SIZE)
        Tool.Set_Option('NAMING', NAMING)
        Tool.Set_Option('COUNT', COUNT)
        Tool.Set_Option('MIN', MIN)
        Tool.Set_Option('MAX', MAX)
        Tool.Set_Option('RANGE', RANGE)
        Tool.Set_Option('SUM', SUM)
        Tool.Set_Option('MEAN', MEAN)
        Tool.Set_Option('VAR', VAR)
        Tool.Set_Option('STDDEV', STDDEV)
        Tool.Set_Option('QUANTILE', QUANTILE)
        return Tool.Execute(Verbose)
    return False

def run_tool_shapes_grid_8(GRIDS=None, POINTS=None, RESULT=None, KERNEL_TYPE=None, KERNEL_SIZE=None, NAMING=None, COUNT=None, MIN=None, MAX=None, RANGE=None, SUM=None, MEAN=None, VAR=None, STDDEV=None, QUANTILE=None, Verbose=2):
    '''
    Grid Statistics for Points
    ----------
    [shapes_grid.8]\n
    For each given point statistics based on all grid cells in the defined neighbourhood will be calculated.\n
    Arguments
    ----------
    - GRIDS [`input grid list`] : Grids
    - POINTS [`input shapes`] : Points
    - RESULT [`output shapes`] : Statistics
    - KERNEL_TYPE [`choice`] : Kernel Type. Available Choices: [0] square [1] circle Default: 0
    - KERNEL_SIZE [`integer number`] : Kernel Size. Minimum: 1 Default: 1 kernel size defined as radius number of cells
    - NAMING [`choice`] : Field Naming. Available Choices: [0] grid number [1] grid name Default: 1
    - COUNT [`boolean`] : Number of Cells. Default: 1
    - MIN [`boolean`] : Minimum. Default: 1
    - MAX [`boolean`] : Maximum. Default: 1
    - RANGE [`boolean`] : Range. Default: 1
    - SUM [`boolean`] : Sum. Default: 1
    - MEAN [`boolean`] : Mean. Default: 1
    - VAR [`boolean`] : Variance. Default: 1
    - STDDEV [`boolean`] : Standard Deviation. Default: 1
    - QUANTILE [`integer number`] : Percentile. Minimum: 0 Maximum: 50 Default: 0 Calculate distribution percentiles. Value specifies interval (median=50, quartiles=25, deciles=10, ...). Set to zero to omit percentile calculation.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_grid', '8', 'Grid Statistics for Points')
    if Tool.is_Okay():
        Tool.Set_Input ('GRIDS', GRIDS)
        Tool.Set_Input ('POINTS', POINTS)
        Tool.Set_Output('RESULT', RESULT)
        Tool.Set_Option('KERNEL_TYPE', KERNEL_TYPE)
        Tool.Set_Option('KERNEL_SIZE', KERNEL_SIZE)
        Tool.Set_Option('NAMING', NAMING)
        Tool.Set_Option('COUNT', COUNT)
        Tool.Set_Option('MIN', MIN)
        Tool.Set_Option('MAX', MAX)
        Tool.Set_Option('RANGE', RANGE)
        Tool.Set_Option('SUM', SUM)
        Tool.Set_Option('MEAN', MEAN)
        Tool.Set_Option('VAR', VAR)
        Tool.Set_Option('STDDEV', STDDEV)
        Tool.Set_Option('QUANTILE', QUANTILE)
        return Tool.Execute(Verbose)
    return False

def Local_Minima_and_Maxima(GRID=None, MINIMA=None, MAXIMA=None, IDENTITY=None, ABSOLUTE=None, BOUNDARY=None, Verbose=2):
    '''
    Local Minima and Maxima
    ----------
    [shapes_grid.9]\n
    Extracts local grid value minima and maxima of to vector points.\n
    Arguments
    ----------
    - GRID [`input grid`] : Grid
    - MINIMA [`output shapes`] : Minima
    - MAXIMA [`output shapes`] : Maxima
    - IDENTITY [`boolean`] : Identical Values. Default: 0 If set, neighbour cells with the same value as the center cell are interpreted to be lower for minima and higher for maxima identification.
    - ABSOLUTE [`boolean`] : Absolute. Default: 0 If set, only the grid's absolute maximum/minimum point is returned.
    - BOUNDARY [`boolean`] : Boundary Cells. Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_grid', '9', 'Local Minima and Maxima')
    if Tool.is_Okay():
        Tool.Set_Input ('GRID', GRID)
        Tool.Set_Output('MINIMA', MINIMA)
        Tool.Set_Output('MAXIMA', MAXIMA)
        Tool.Set_Option('IDENTITY', IDENTITY)
        Tool.Set_Option('ABSOLUTE', ABSOLUTE)
        Tool.Set_Option('BOUNDARY', BOUNDARY)
        return Tool.Execute(Verbose)
    return False

def run_tool_shapes_grid_9(GRID=None, MINIMA=None, MAXIMA=None, IDENTITY=None, ABSOLUTE=None, BOUNDARY=None, Verbose=2):
    '''
    Local Minima and Maxima
    ----------
    [shapes_grid.9]\n
    Extracts local grid value minima and maxima of to vector points.\n
    Arguments
    ----------
    - GRID [`input grid`] : Grid
    - MINIMA [`output shapes`] : Minima
    - MAXIMA [`output shapes`] : Maxima
    - IDENTITY [`boolean`] : Identical Values. Default: 0 If set, neighbour cells with the same value as the center cell are interpreted to be lower for minima and higher for maxima identification.
    - ABSOLUTE [`boolean`] : Absolute. Default: 0 If set, only the grid's absolute maximum/minimum point is returned.
    - BOUNDARY [`boolean`] : Boundary Cells. Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_grid', '9', 'Local Minima and Maxima')
    if Tool.is_Okay():
        Tool.Set_Input ('GRID', GRID)
        Tool.Set_Output('MINIMA', MINIMA)
        Tool.Set_Output('MAXIMA', MAXIMA)
        Tool.Set_Option('IDENTITY', IDENTITY)
        Tool.Set_Option('ABSOLUTE', ABSOLUTE)
        Tool.Set_Option('BOUNDARY', BOUNDARY)
        return Tool.Execute(Verbose)
    return False

def Grid_System_Extent(EXTENT=None, GRID_SYSTEM=None, BORDER=None, Verbose=2):
    '''
    Grid System Extent
    ----------
    [shapes_grid.10]\n
    Creates a polygon (rectangle) from a grid system's extent.\n
    Arguments
    ----------
    - EXTENT [`output shapes`] : Extent
    - GRID_SYSTEM [`grid system`] : Grid System
    - BORDER [`choice`] : Border. Available Choices: [0] grid cells [1] grid nodes Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_grid', '10', 'Grid System Extent')
    if Tool.is_Okay():
        Tool.Set_Output('EXTENT', EXTENT)
        Tool.Set_Option('GRID_SYSTEM', GRID_SYSTEM)
        Tool.Set_Option('BORDER', BORDER)
        return Tool.Execute(Verbose)
    return False

def run_tool_shapes_grid_10(EXTENT=None, GRID_SYSTEM=None, BORDER=None, Verbose=2):
    '''
    Grid System Extent
    ----------
    [shapes_grid.10]\n
    Creates a polygon (rectangle) from a grid system's extent.\n
    Arguments
    ----------
    - EXTENT [`output shapes`] : Extent
    - GRID_SYSTEM [`grid system`] : Grid System
    - BORDER [`choice`] : Border. Available Choices: [0] grid cells [1] grid nodes Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_grid', '10', 'Grid System Extent')
    if Tool.is_Okay():
        Tool.Set_Output('EXTENT', EXTENT)
        Tool.Set_Option('GRID_SYSTEM', GRID_SYSTEM)
        Tool.Set_Option('BORDER', BORDER)
        return Tool.Execute(Verbose)
    return False

def Clip_Grid_with_Rectangle(INPUT=None, SHAPES=None, OUTPUT=None, INPUT_GRIDSYSTEM=None, BORDER=None, Verbose=2):
    '''
    Clip Grid with Rectangle
    ----------
    [shapes_grid.11]\n
    Clips the input grid with the (rectangular) extent of a shapefile. The clipped grid will have the extent of the shapefile.\n
    Select shapes from the shapefile prior to tool execution in case you like to use only a subset from the shapefile for clipping.\n
    Arguments
    ----------
    - INPUT [`input grid`] : Input. The grid to clip.
    - SHAPES [`input shapes`] : Extent. The shapefile to use for clipping.
    - OUTPUT [`output data object`] : Output. The clipped grid.
    - INPUT_GRIDSYSTEM [`grid system`] : Grid system
    - BORDER [`choice`] : Border. Available Choices: [0] grid cells [1] grid nodes [2] align to grid system Default: 0 Set grid extent to grid cells (pixel as area), to grid nodes (pixel as point) or align to grid system.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_grid', '11', 'Clip Grid with Rectangle')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Input ('SHAPES', SHAPES)
        Tool.Set_Output('OUTPUT', OUTPUT)
        Tool.Set_Option('INPUT_GRIDSYSTEM', INPUT_GRIDSYSTEM)
        Tool.Set_Option('BORDER', BORDER)
        return Tool.Execute(Verbose)
    return False

def run_tool_shapes_grid_11(INPUT=None, SHAPES=None, OUTPUT=None, INPUT_GRIDSYSTEM=None, BORDER=None, Verbose=2):
    '''
    Clip Grid with Rectangle
    ----------
    [shapes_grid.11]\n
    Clips the input grid with the (rectangular) extent of a shapefile. The clipped grid will have the extent of the shapefile.\n
    Select shapes from the shapefile prior to tool execution in case you like to use only a subset from the shapefile for clipping.\n
    Arguments
    ----------
    - INPUT [`input grid`] : Input. The grid to clip.
    - SHAPES [`input shapes`] : Extent. The shapefile to use for clipping.
    - OUTPUT [`output data object`] : Output. The clipped grid.
    - INPUT_GRIDSYSTEM [`grid system`] : Grid system
    - BORDER [`choice`] : Border. Available Choices: [0] grid cells [1] grid nodes [2] align to grid system Default: 0 Set grid extent to grid cells (pixel as area), to grid nodes (pixel as point) or align to grid system.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_grid', '11', 'Clip Grid with Rectangle')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Input ('SHAPES', SHAPES)
        Tool.Set_Output('OUTPUT', OUTPUT)
        Tool.Set_Option('INPUT_GRIDSYSTEM', INPUT_GRIDSYSTEM)
        Tool.Set_Option('BORDER', BORDER)
        return Tool.Execute(Verbose)
    return False

def Gradient_Vectors_from_Surface(SURFACE=None, VECTORS=None, STEP=None, SIZE=None, AGGR=None, STYLE=None, Verbose=2):
    '''
    Gradient Vectors from Surface
    ----------
    [shapes_grid.15]\n
    Create lines indicating the gradient.\n
    Arguments
    ----------
    - SURFACE [`input grid`] : Surface
    - VECTORS [`output shapes`] : Gradient Vectors
    - STEP [`integer number`] : Step. Minimum: 1 Default: 1
    - SIZE [`value range`] : Size Range. size range as percentage of step
    - AGGR [`choice`] : Aggregation. Available Choices: [0] nearest neighbour [1] mean value Default: 1 how to request values if step size is more than one cell
    - STYLE [`choice`] : Style. Available Choices: [0] simple line [1] arrow [2] arrow (centered to cell) Default: 2

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_grid', '15', 'Gradient Vectors from Surface')
    if Tool.is_Okay():
        Tool.Set_Input ('SURFACE', SURFACE)
        Tool.Set_Output('VECTORS', VECTORS)
        Tool.Set_Option('STEP', STEP)
        Tool.Set_Option('SIZE', SIZE)
        Tool.Set_Option('AGGR', AGGR)
        Tool.Set_Option('STYLE', STYLE)
        return Tool.Execute(Verbose)
    return False

def run_tool_shapes_grid_15(SURFACE=None, VECTORS=None, STEP=None, SIZE=None, AGGR=None, STYLE=None, Verbose=2):
    '''
    Gradient Vectors from Surface
    ----------
    [shapes_grid.15]\n
    Create lines indicating the gradient.\n
    Arguments
    ----------
    - SURFACE [`input grid`] : Surface
    - VECTORS [`output shapes`] : Gradient Vectors
    - STEP [`integer number`] : Step. Minimum: 1 Default: 1
    - SIZE [`value range`] : Size Range. size range as percentage of step
    - AGGR [`choice`] : Aggregation. Available Choices: [0] nearest neighbour [1] mean value Default: 1 how to request values if step size is more than one cell
    - STYLE [`choice`] : Style. Available Choices: [0] simple line [1] arrow [2] arrow (centered to cell) Default: 2

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_grid', '15', 'Gradient Vectors from Surface')
    if Tool.is_Okay():
        Tool.Set_Input ('SURFACE', SURFACE)
        Tool.Set_Output('VECTORS', VECTORS)
        Tool.Set_Option('STEP', STEP)
        Tool.Set_Option('SIZE', SIZE)
        Tool.Set_Option('AGGR', AGGR)
        Tool.Set_Option('STYLE', STYLE)
        return Tool.Execute(Verbose)
    return False

def Gradient_Vectors_from_Direction_and_Length(DIR=None, LEN=None, VECTORS=None, STEP=None, SIZE=None, AGGR=None, STYLE=None, Verbose=2):
    '''
    Gradient Vectors from Direction and Length
    ----------
    [shapes_grid.16]\n
    Create lines indicating the gradient.\n
    Arguments
    ----------
    - DIR [`input grid`] : Direction
    - LEN [`input grid`] : Length
    - VECTORS [`output shapes`] : Gradient Vectors
    - STEP [`integer number`] : Step. Minimum: 1 Default: 1
    - SIZE [`value range`] : Size Range. size range as percentage of step
    - AGGR [`choice`] : Aggregation. Available Choices: [0] nearest neighbour [1] mean value Default: 1 how to request values if step size is more than one cell
    - STYLE [`choice`] : Style. Available Choices: [0] simple line [1] arrow [2] arrow (centered to cell) Default: 2

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_grid', '16', 'Gradient Vectors from Direction and Length')
    if Tool.is_Okay():
        Tool.Set_Input ('DIR', DIR)
        Tool.Set_Input ('LEN', LEN)
        Tool.Set_Output('VECTORS', VECTORS)
        Tool.Set_Option('STEP', STEP)
        Tool.Set_Option('SIZE', SIZE)
        Tool.Set_Option('AGGR', AGGR)
        Tool.Set_Option('STYLE', STYLE)
        return Tool.Execute(Verbose)
    return False

def run_tool_shapes_grid_16(DIR=None, LEN=None, VECTORS=None, STEP=None, SIZE=None, AGGR=None, STYLE=None, Verbose=2):
    '''
    Gradient Vectors from Direction and Length
    ----------
    [shapes_grid.16]\n
    Create lines indicating the gradient.\n
    Arguments
    ----------
    - DIR [`input grid`] : Direction
    - LEN [`input grid`] : Length
    - VECTORS [`output shapes`] : Gradient Vectors
    - STEP [`integer number`] : Step. Minimum: 1 Default: 1
    - SIZE [`value range`] : Size Range. size range as percentage of step
    - AGGR [`choice`] : Aggregation. Available Choices: [0] nearest neighbour [1] mean value Default: 1 how to request values if step size is more than one cell
    - STYLE [`choice`] : Style. Available Choices: [0] simple line [1] arrow [2] arrow (centered to cell) Default: 2

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_grid', '16', 'Gradient Vectors from Direction and Length')
    if Tool.is_Okay():
        Tool.Set_Input ('DIR', DIR)
        Tool.Set_Input ('LEN', LEN)
        Tool.Set_Output('VECTORS', VECTORS)
        Tool.Set_Option('STEP', STEP)
        Tool.Set_Option('SIZE', SIZE)
        Tool.Set_Option('AGGR', AGGR)
        Tool.Set_Option('STYLE', STYLE)
        return Tool.Execute(Verbose)
    return False

def Gradient_Vectors_from_Directional_Components(X=None, Y=None, VECTORS=None, STEP=None, SIZE=None, AGGR=None, STYLE=None, Verbose=2):
    '''
    Gradient Vectors from Directional Components
    ----------
    [shapes_grid.17]\n
    Create lines indicating the gradient.\n
    Arguments
    ----------
    - X [`input grid`] : X Component
    - Y [`input grid`] : Y Component
    - VECTORS [`output shapes`] : Gradient Vectors
    - STEP [`integer number`] : Step. Minimum: 1 Default: 1
    - SIZE [`value range`] : Size Range. size range as percentage of step
    - AGGR [`choice`] : Aggregation. Available Choices: [0] nearest neighbour [1] mean value Default: 1 how to request values if step size is more than one cell
    - STYLE [`choice`] : Style. Available Choices: [0] simple line [1] arrow [2] arrow (centered to cell) Default: 2

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_grid', '17', 'Gradient Vectors from Directional Components')
    if Tool.is_Okay():
        Tool.Set_Input ('X', X)
        Tool.Set_Input ('Y', Y)
        Tool.Set_Output('VECTORS', VECTORS)
        Tool.Set_Option('STEP', STEP)
        Tool.Set_Option('SIZE', SIZE)
        Tool.Set_Option('AGGR', AGGR)
        Tool.Set_Option('STYLE', STYLE)
        return Tool.Execute(Verbose)
    return False

def run_tool_shapes_grid_17(X=None, Y=None, VECTORS=None, STEP=None, SIZE=None, AGGR=None, STYLE=None, Verbose=2):
    '''
    Gradient Vectors from Directional Components
    ----------
    [shapes_grid.17]\n
    Create lines indicating the gradient.\n
    Arguments
    ----------
    - X [`input grid`] : X Component
    - Y [`input grid`] : Y Component
    - VECTORS [`output shapes`] : Gradient Vectors
    - STEP [`integer number`] : Step. Minimum: 1 Default: 1
    - SIZE [`value range`] : Size Range. size range as percentage of step
    - AGGR [`choice`] : Aggregation. Available Choices: [0] nearest neighbour [1] mean value Default: 1 how to request values if step size is more than one cell
    - STYLE [`choice`] : Style. Available Choices: [0] simple line [1] arrow [2] arrow (centered to cell) Default: 2

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_grid', '17', 'Gradient Vectors from Directional Components')
    if Tool.is_Okay():
        Tool.Set_Input ('X', X)
        Tool.Set_Input ('Y', Y)
        Tool.Set_Output('VECTORS', VECTORS)
        Tool.Set_Option('STEP', STEP)
        Tool.Set_Option('SIZE', SIZE)
        Tool.Set_Option('AGGR', AGGR)
        Tool.Set_Option('STYLE', STYLE)
        return Tool.Execute(Verbose)
    return False

def Grid_Classes_Area_for_Polygons(POLYGONS=None, GRID=None, GRID_LUT=None, RESULT=None, PROCESS=None, METHOD=None, OUTPUT=None, GRID_VALUES=None, GRID_LUT_MIN=None, GRID_LUT_MAX=None, GRID_LUT_NAM=None, Verbose=2):
    '''
    Grid Classes Area for Polygons
    ----------
    [shapes_grid.18]\n
    Calculates for each polygon the area covered by each grid class.\n
    Arguments
    ----------
    - POLYGONS [`input shapes`] : Polygons
    - GRID [`input grid`] : Classes
    - GRID_LUT [`optional input table`] : Look-up Table
    - RESULT [`output shapes`] : Result
    - PROCESS [`choice`] : Processing Order. Available Choices: [0] cell by cell [1] polygon by polygon Default: 1 If only a small part of the grid area is covered by polygons, polygon by polygon processing might give an enormous performance boost.
    - METHOD [`choice`] : Cell Area Intersection. Available Choices: [0] cell center [1] cell area Default: 0
    - OUTPUT [`choice`] : Output Measurement. Available Choices: [0] total area [1] percentage Default: 0
    - GRID_VALUES [`choice`] : Class Definition. Available Choices: [0] values are class identifiers [1] use look-up table Default: 0
    - GRID_LUT_MIN [`table field`] : Value
    - GRID_LUT_MAX [`table field`] : Value (Maximum)
    - GRID_LUT_NAM [`table field`] : Name

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_grid', '18', 'Grid Classes Area for Polygons')
    if Tool.is_Okay():
        Tool.Set_Input ('POLYGONS', POLYGONS)
        Tool.Set_Input ('GRID', GRID)
        Tool.Set_Input ('GRID_LUT', GRID_LUT)
        Tool.Set_Output('RESULT', RESULT)
        Tool.Set_Option('PROCESS', PROCESS)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('OUTPUT', OUTPUT)
        Tool.Set_Option('GRID_VALUES', GRID_VALUES)
        Tool.Set_Option('GRID_LUT_MIN', GRID_LUT_MIN)
        Tool.Set_Option('GRID_LUT_MAX', GRID_LUT_MAX)
        Tool.Set_Option('GRID_LUT_NAM', GRID_LUT_NAM)
        return Tool.Execute(Verbose)
    return False

def run_tool_shapes_grid_18(POLYGONS=None, GRID=None, GRID_LUT=None, RESULT=None, PROCESS=None, METHOD=None, OUTPUT=None, GRID_VALUES=None, GRID_LUT_MIN=None, GRID_LUT_MAX=None, GRID_LUT_NAM=None, Verbose=2):
    '''
    Grid Classes Area for Polygons
    ----------
    [shapes_grid.18]\n
    Calculates for each polygon the area covered by each grid class.\n
    Arguments
    ----------
    - POLYGONS [`input shapes`] : Polygons
    - GRID [`input grid`] : Classes
    - GRID_LUT [`optional input table`] : Look-up Table
    - RESULT [`output shapes`] : Result
    - PROCESS [`choice`] : Processing Order. Available Choices: [0] cell by cell [1] polygon by polygon Default: 1 If only a small part of the grid area is covered by polygons, polygon by polygon processing might give an enormous performance boost.
    - METHOD [`choice`] : Cell Area Intersection. Available Choices: [0] cell center [1] cell area Default: 0
    - OUTPUT [`choice`] : Output Measurement. Available Choices: [0] total area [1] percentage Default: 0
    - GRID_VALUES [`choice`] : Class Definition. Available Choices: [0] values are class identifiers [1] use look-up table Default: 0
    - GRID_LUT_MIN [`table field`] : Value
    - GRID_LUT_MAX [`table field`] : Value (Maximum)
    - GRID_LUT_NAM [`table field`] : Name

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_grid', '18', 'Grid Classes Area for Polygons')
    if Tool.is_Okay():
        Tool.Set_Input ('POLYGONS', POLYGONS)
        Tool.Set_Input ('GRID', GRID)
        Tool.Set_Input ('GRID_LUT', GRID_LUT)
        Tool.Set_Output('RESULT', RESULT)
        Tool.Set_Option('PROCESS', PROCESS)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('OUTPUT', OUTPUT)
        Tool.Set_Option('GRID_VALUES', GRID_VALUES)
        Tool.Set_Option('GRID_LUT_MIN', GRID_LUT_MIN)
        Tool.Set_Option('GRID_LUT_MAX', GRID_LUT_MAX)
        Tool.Set_Option('GRID_LUT_NAM', GRID_LUT_NAM)
        return Tool.Execute(Verbose)
    return False

def Boundary_Cells_to_Polygons(GRID=None, POLYGONS=None, BOUNDARY_CELLS=None, BOUNDARY_VALUE=None, ALLVERTICES=None, Verbose=2):
    '''
    Boundary Cells to Polygons
    ----------
    [shapes_grid.19]\n
    The 'Boundary Cells to Polygons' tool to constructs polygons taking all no-data cells (or those cells having a specified value) as potential boundary cells surrounding each polygon.\n
    Arguments
    ----------
    - GRID [`input grid`] : Grid
    - POLYGONS [`output shapes`] : Polygons
    - BOUNDARY_CELLS [`choice`] : Boundary Cells. Available Choices: [0] no data [1] value Default: 0
    - BOUNDARY_VALUE [`floating point number`] : Value. Default: 0.000000
    - ALLVERTICES [`boolean`] : Keep Vertices on Straight Lines. Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_grid', '19', 'Boundary Cells to Polygons')
    if Tool.is_Okay():
        Tool.Set_Input ('GRID', GRID)
        Tool.Set_Output('POLYGONS', POLYGONS)
        Tool.Set_Option('BOUNDARY_CELLS', BOUNDARY_CELLS)
        Tool.Set_Option('BOUNDARY_VALUE', BOUNDARY_VALUE)
        Tool.Set_Option('ALLVERTICES', ALLVERTICES)
        return Tool.Execute(Verbose)
    return False

def run_tool_shapes_grid_19(GRID=None, POLYGONS=None, BOUNDARY_CELLS=None, BOUNDARY_VALUE=None, ALLVERTICES=None, Verbose=2):
    '''
    Boundary Cells to Polygons
    ----------
    [shapes_grid.19]\n
    The 'Boundary Cells to Polygons' tool to constructs polygons taking all no-data cells (or those cells having a specified value) as potential boundary cells surrounding each polygon.\n
    Arguments
    ----------
    - GRID [`input grid`] : Grid
    - POLYGONS [`output shapes`] : Polygons
    - BOUNDARY_CELLS [`choice`] : Boundary Cells. Available Choices: [0] no data [1] value Default: 0
    - BOUNDARY_VALUE [`floating point number`] : Value. Default: 0.000000
    - ALLVERTICES [`boolean`] : Keep Vertices on Straight Lines. Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_grid', '19', 'Boundary Cells to Polygons')
    if Tool.is_Okay():
        Tool.Set_Input ('GRID', GRID)
        Tool.Set_Output('POLYGONS', POLYGONS)
        Tool.Set_Option('BOUNDARY_CELLS', BOUNDARY_CELLS)
        Tool.Set_Option('BOUNDARY_VALUE', BOUNDARY_VALUE)
        Tool.Set_Option('ALLVERTICES', ALLVERTICES)
        return Tool.Execute(Verbose)
    return False

def Line_Direction(INPUT=None, TARGET_TEMPLATE=None, GRID=None, ORDER_FIELD=None, TARGET_DEFINITION=None, TARGET_USER_SIZE=None, TARGET_USER_XMIN=None, TARGET_USER_XMAX=None, TARGET_USER_YMIN=None, TARGET_USER_YMAX=None, TARGET_USER_COLS=None, TARGET_USER_ROWS=None, TARGET_USER_FLAT=None, TARGET_USER_FITS=None, TARGET_SYSTEM=None, Verbose=2):
    '''
    Line Direction
    ----------
    [shapes_grid.20]\n
    Creates a grid of the direction (fat) of a line shapes.\n
    Arguments
    ----------
    - INPUT [`input shapes`] : Shapes
    - TARGET_TEMPLATE [`optional input grid`] : Target System. use this grid's system for output grids
    - GRID [`output grid`] : Grid
    - ORDER_FIELD [`table field`] : Order Field. Field for order in which the shape will be sorted prior to rasterization.
    - TARGET_DEFINITION [`choice`] : Target Grid System. Available Choices: [0] user defined [1] grid or grid system Default: 0
    - TARGET_USER_SIZE [`floating point number`] : Cellsize. Minimum: 0.000000 Default: 1.000000
    - TARGET_USER_XMIN [`floating point number`] : West. Default: 0.000000
    - TARGET_USER_XMAX [`floating point number`] : East. Default: 100.000000
    - TARGET_USER_YMIN [`floating point number`] : South. Default: 0.000000
    - TARGET_USER_YMAX [`floating point number`] : North. Default: 100.000000
    - TARGET_USER_COLS [`integer number`] : Columns. Minimum: 1 Default: 101 Number of cells in East-West direction.
    - TARGET_USER_ROWS [`integer number`] : Rows. Minimum: 1 Default: 101 Number of cells in North-South direction.
    - TARGET_USER_FLAT [`boolean`] : Rounding. Default: 1 Round bounding coordinates to multiples of cell size. Ignored if cell size has decimal places.
    - TARGET_USER_FITS [`choice`] : Fit. Available Choices: [0] nodes [1] cells Default: 0
    - TARGET_SYSTEM [`grid system`] : Grid System

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_grid', '20', 'Line Direction')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Input ('TARGET_TEMPLATE', TARGET_TEMPLATE)
        Tool.Set_Output('GRID', GRID)
        Tool.Set_Option('ORDER_FIELD', ORDER_FIELD)
        Tool.Set_Option('TARGET_DEFINITION', TARGET_DEFINITION)
        Tool.Set_Option('TARGET_USER_SIZE', TARGET_USER_SIZE)
        Tool.Set_Option('TARGET_USER_XMIN', TARGET_USER_XMIN)
        Tool.Set_Option('TARGET_USER_XMAX', TARGET_USER_XMAX)
        Tool.Set_Option('TARGET_USER_YMIN', TARGET_USER_YMIN)
        Tool.Set_Option('TARGET_USER_YMAX', TARGET_USER_YMAX)
        Tool.Set_Option('TARGET_USER_COLS', TARGET_USER_COLS)
        Tool.Set_Option('TARGET_USER_ROWS', TARGET_USER_ROWS)
        Tool.Set_Option('TARGET_USER_FLAT', TARGET_USER_FLAT)
        Tool.Set_Option('TARGET_USER_FITS', TARGET_USER_FITS)
        Tool.Set_Option('TARGET_SYSTEM', TARGET_SYSTEM)
        return Tool.Execute(Verbose)
    return False

def run_tool_shapes_grid_20(INPUT=None, TARGET_TEMPLATE=None, GRID=None, ORDER_FIELD=None, TARGET_DEFINITION=None, TARGET_USER_SIZE=None, TARGET_USER_XMIN=None, TARGET_USER_XMAX=None, TARGET_USER_YMIN=None, TARGET_USER_YMAX=None, TARGET_USER_COLS=None, TARGET_USER_ROWS=None, TARGET_USER_FLAT=None, TARGET_USER_FITS=None, TARGET_SYSTEM=None, Verbose=2):
    '''
    Line Direction
    ----------
    [shapes_grid.20]\n
    Creates a grid of the direction (fat) of a line shapes.\n
    Arguments
    ----------
    - INPUT [`input shapes`] : Shapes
    - TARGET_TEMPLATE [`optional input grid`] : Target System. use this grid's system for output grids
    - GRID [`output grid`] : Grid
    - ORDER_FIELD [`table field`] : Order Field. Field for order in which the shape will be sorted prior to rasterization.
    - TARGET_DEFINITION [`choice`] : Target Grid System. Available Choices: [0] user defined [1] grid or grid system Default: 0
    - TARGET_USER_SIZE [`floating point number`] : Cellsize. Minimum: 0.000000 Default: 1.000000
    - TARGET_USER_XMIN [`floating point number`] : West. Default: 0.000000
    - TARGET_USER_XMAX [`floating point number`] : East. Default: 100.000000
    - TARGET_USER_YMIN [`floating point number`] : South. Default: 0.000000
    - TARGET_USER_YMAX [`floating point number`] : North. Default: 100.000000
    - TARGET_USER_COLS [`integer number`] : Columns. Minimum: 1 Default: 101 Number of cells in East-West direction.
    - TARGET_USER_ROWS [`integer number`] : Rows. Minimum: 1 Default: 101 Number of cells in North-South direction.
    - TARGET_USER_FLAT [`boolean`] : Rounding. Default: 1 Round bounding coordinates to multiples of cell size. Ignored if cell size has decimal places.
    - TARGET_USER_FITS [`choice`] : Fit. Available Choices: [0] nodes [1] cells Default: 0
    - TARGET_SYSTEM [`grid system`] : Grid System

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_grid', '20', 'Line Direction')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Input ('TARGET_TEMPLATE', TARGET_TEMPLATE)
        Tool.Set_Output('GRID', GRID)
        Tool.Set_Option('ORDER_FIELD', ORDER_FIELD)
        Tool.Set_Option('TARGET_DEFINITION', TARGET_DEFINITION)
        Tool.Set_Option('TARGET_USER_SIZE', TARGET_USER_SIZE)
        Tool.Set_Option('TARGET_USER_XMIN', TARGET_USER_XMIN)
        Tool.Set_Option('TARGET_USER_XMAX', TARGET_USER_XMAX)
        Tool.Set_Option('TARGET_USER_YMIN', TARGET_USER_YMIN)
        Tool.Set_Option('TARGET_USER_YMAX', TARGET_USER_YMAX)
        Tool.Set_Option('TARGET_USER_COLS', TARGET_USER_COLS)
        Tool.Set_Option('TARGET_USER_ROWS', TARGET_USER_ROWS)
        Tool.Set_Option('TARGET_USER_FLAT', TARGET_USER_FLAT)
        Tool.Set_Option('TARGET_USER_FITS', TARGET_USER_FITS)
        Tool.Set_Option('TARGET_SYSTEM', TARGET_SYSTEM)
        return Tool.Execute(Verbose)
    return False

def Grid_Values_and_Polygon_Attributes_to_Points(GRID=None, POLYGONS=None, POINTS=None, GRID_GRIDSYSTEM=None, ATTRIBUTE=None, Verbose=2):
    '''
    Grid Values and Polygon Attributes to Points
    ----------
    [shapes_grid.grid_and_polygon_to_points]\n
    Converts a grid to a points table with additional polygon attribute.\n
    Arguments
    ----------
    - GRID [`input grid`] : Grid
    - POLYGONS [`input shapes`] : Polygons
    - POINTS [`output shapes`] : Points Table
    - GRID_GRIDSYSTEM [`grid system`] : Grid system
    - ATTRIBUTE [`table field`] : Attribute

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_grid', 'grid_and_polygon_to_points', 'Grid Values and Polygon Attributes to Points')
    if Tool.is_Okay():
        Tool.Set_Input ('GRID', GRID)
        Tool.Set_Input ('POLYGONS', POLYGONS)
        Tool.Set_Output('POINTS', POINTS)
        Tool.Set_Option('GRID_GRIDSYSTEM', GRID_GRIDSYSTEM)
        Tool.Set_Option('ATTRIBUTE', ATTRIBUTE)
        return Tool.Execute(Verbose)
    return False

def run_tool_shapes_grid_grid_and_polygon_to_points(GRID=None, POLYGONS=None, POINTS=None, GRID_GRIDSYSTEM=None, ATTRIBUTE=None, Verbose=2):
    '''
    Grid Values and Polygon Attributes to Points
    ----------
    [shapes_grid.grid_and_polygon_to_points]\n
    Converts a grid to a points table with additional polygon attribute.\n
    Arguments
    ----------
    - GRID [`input grid`] : Grid
    - POLYGONS [`input shapes`] : Polygons
    - POINTS [`output shapes`] : Points Table
    - GRID_GRIDSYSTEM [`grid system`] : Grid system
    - ATTRIBUTE [`table field`] : Attribute

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_grid', 'grid_and_polygon_to_points', 'Grid Values and Polygon Attributes to Points')
    if Tool.is_Okay():
        Tool.Set_Input ('GRID', GRID)
        Tool.Set_Input ('POLYGONS', POLYGONS)
        Tool.Set_Output('POINTS', POINTS)
        Tool.Set_Option('GRID_GRIDSYSTEM', GRID_GRIDSYSTEM)
        Tool.Set_Option('ATTRIBUTE', ATTRIBUTE)
        return Tool.Execute(Verbose)
    return False

