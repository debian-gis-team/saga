#! /usr/bin/env python

'''
Python Interface to SAGA Tools Library
----------
- Category : Simulation
- Name     : Cellular Automata
- ID       : sim_cellular_automata

Description
----------
Cellular Automata
'''

from PySAGA.helper import Tool_Wrapper

def Conways_Game_of_Life(TARGET_TEMPLATE=None, LIFE=None, TARGET_DEFINITION=None, TARGET_USER_SIZE=None, TARGET_USER_XMIN=None, TARGET_USER_XMAX=None, TARGET_USER_YMIN=None, TARGET_USER_YMAX=None, TARGET_USER_COLS=None, TARGET_USER_ROWS=None, TARGET_USER_FLAT=None, TARGET_USER_FITS=None, TARGET_SYSTEM=None, REFRESH=None, FADECOLOR=None, Verbose=2):
    '''
    Conway's Game of Life
    ----------
    [sim_cellular_automata.0]\n
    Conway's Game of Life - a classical cellular automat.\n
    Arguments
    ----------
    - TARGET_TEMPLATE [`optional input grid`] : Target System. use this grid's system for output grids
    - LIFE [`output grid`] : Life
    - TARGET_DEFINITION [`choice`] : Target Grid System. Available Choices: [0] user defined [1] grid or grid system Default: 0
    - TARGET_USER_SIZE [`floating point number`] : Cellsize. Minimum: 0.000000 Default: 1.000000
    - TARGET_USER_XMIN [`floating point number`] : West. Default: 0.000000
    - TARGET_USER_XMAX [`floating point number`] : East. Default: 100.000000
    - TARGET_USER_YMIN [`floating point number`] : South. Default: 0.000000
    - TARGET_USER_YMAX [`floating point number`] : North. Default: 100.000000
    - TARGET_USER_COLS [`integer number`] : Columns. Minimum: 1 Default: 101 Number of cells in East-West direction.
    - TARGET_USER_ROWS [`integer number`] : Rows. Minimum: 1 Default: 101 Number of cells in North-South direction.
    - TARGET_USER_FLAT [`boolean`] : Rounding. Default: 1 Round bounding coordinates to multiples of cell size. Ignored if cell size has decimal places.
    - TARGET_USER_FITS [`choice`] : Fit. Available Choices: [0] nodes [1] cells Default: 0
    - TARGET_SYSTEM [`grid system`] : Grid System
    - REFRESH [`choice`] : Refresh. Available Choices: [0] no refresh [1] random [2] virus 1 [3] virus 2 Default: 1
    - FADECOLOR [`integer number`] : Fade Color Count. Minimum: 3 Maximum: 255 Default: 64

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('sim_cellular_automata', '0', 'Conway\'s Game of Life')
    if Tool.is_Okay():
        Tool.Set_Input ('TARGET_TEMPLATE', TARGET_TEMPLATE)
        Tool.Set_Output('LIFE', LIFE)
        Tool.Set_Option('TARGET_DEFINITION', TARGET_DEFINITION)
        Tool.Set_Option('TARGET_USER_SIZE', TARGET_USER_SIZE)
        Tool.Set_Option('TARGET_USER_XMIN', TARGET_USER_XMIN)
        Tool.Set_Option('TARGET_USER_XMAX', TARGET_USER_XMAX)
        Tool.Set_Option('TARGET_USER_YMIN', TARGET_USER_YMIN)
        Tool.Set_Option('TARGET_USER_YMAX', TARGET_USER_YMAX)
        Tool.Set_Option('TARGET_USER_COLS', TARGET_USER_COLS)
        Tool.Set_Option('TARGET_USER_ROWS', TARGET_USER_ROWS)
        Tool.Set_Option('TARGET_USER_FLAT', TARGET_USER_FLAT)
        Tool.Set_Option('TARGET_USER_FITS', TARGET_USER_FITS)
        Tool.Set_Option('TARGET_SYSTEM', TARGET_SYSTEM)
        Tool.Set_Option('REFRESH', REFRESH)
        Tool.Set_Option('FADECOLOR', FADECOLOR)
        return Tool.Execute(Verbose)
    return False

def run_tool_sim_cellular_automata_0(TARGET_TEMPLATE=None, LIFE=None, TARGET_DEFINITION=None, TARGET_USER_SIZE=None, TARGET_USER_XMIN=None, TARGET_USER_XMAX=None, TARGET_USER_YMIN=None, TARGET_USER_YMAX=None, TARGET_USER_COLS=None, TARGET_USER_ROWS=None, TARGET_USER_FLAT=None, TARGET_USER_FITS=None, TARGET_SYSTEM=None, REFRESH=None, FADECOLOR=None, Verbose=2):
    '''
    Conway's Game of Life
    ----------
    [sim_cellular_automata.0]\n
    Conway's Game of Life - a classical cellular automat.\n
    Arguments
    ----------
    - TARGET_TEMPLATE [`optional input grid`] : Target System. use this grid's system for output grids
    - LIFE [`output grid`] : Life
    - TARGET_DEFINITION [`choice`] : Target Grid System. Available Choices: [0] user defined [1] grid or grid system Default: 0
    - TARGET_USER_SIZE [`floating point number`] : Cellsize. Minimum: 0.000000 Default: 1.000000
    - TARGET_USER_XMIN [`floating point number`] : West. Default: 0.000000
    - TARGET_USER_XMAX [`floating point number`] : East. Default: 100.000000
    - TARGET_USER_YMIN [`floating point number`] : South. Default: 0.000000
    - TARGET_USER_YMAX [`floating point number`] : North. Default: 100.000000
    - TARGET_USER_COLS [`integer number`] : Columns. Minimum: 1 Default: 101 Number of cells in East-West direction.
    - TARGET_USER_ROWS [`integer number`] : Rows. Minimum: 1 Default: 101 Number of cells in North-South direction.
    - TARGET_USER_FLAT [`boolean`] : Rounding. Default: 1 Round bounding coordinates to multiples of cell size. Ignored if cell size has decimal places.
    - TARGET_USER_FITS [`choice`] : Fit. Available Choices: [0] nodes [1] cells Default: 0
    - TARGET_SYSTEM [`grid system`] : Grid System
    - REFRESH [`choice`] : Refresh. Available Choices: [0] no refresh [1] random [2] virus 1 [3] virus 2 Default: 1
    - FADECOLOR [`integer number`] : Fade Color Count. Minimum: 3 Maximum: 255 Default: 64

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('sim_cellular_automata', '0', 'Conway\'s Game of Life')
    if Tool.is_Okay():
        Tool.Set_Input ('TARGET_TEMPLATE', TARGET_TEMPLATE)
        Tool.Set_Output('LIFE', LIFE)
        Tool.Set_Option('TARGET_DEFINITION', TARGET_DEFINITION)
        Tool.Set_Option('TARGET_USER_SIZE', TARGET_USER_SIZE)
        Tool.Set_Option('TARGET_USER_XMIN', TARGET_USER_XMIN)
        Tool.Set_Option('TARGET_USER_XMAX', TARGET_USER_XMAX)
        Tool.Set_Option('TARGET_USER_YMIN', TARGET_USER_YMIN)
        Tool.Set_Option('TARGET_USER_YMAX', TARGET_USER_YMAX)
        Tool.Set_Option('TARGET_USER_COLS', TARGET_USER_COLS)
        Tool.Set_Option('TARGET_USER_ROWS', TARGET_USER_ROWS)
        Tool.Set_Option('TARGET_USER_FLAT', TARGET_USER_FLAT)
        Tool.Set_Option('TARGET_USER_FITS', TARGET_USER_FITS)
        Tool.Set_Option('TARGET_SYSTEM', TARGET_SYSTEM)
        Tool.Set_Option('REFRESH', REFRESH)
        Tool.Set_Option('FADECOLOR', FADECOLOR)
        return Tool.Execute(Verbose)
    return False

def WaTor(TARGET_TEMPLATE=None, GRID=None, TABLE=None, TARGET_DEFINITION=None, TARGET_USER_SIZE=None, TARGET_USER_XMIN=None, TARGET_USER_XMAX=None, TARGET_USER_YMIN=None, TARGET_USER_YMAX=None, TARGET_USER_COLS=None, TARGET_USER_ROWS=None, TARGET_USER_FLAT=None, TARGET_USER_FITS=None, TARGET_SYSTEM=None, REFRESH=None, INIT_FISH=None, INIT_SHARK=None, FISH_BIRTH=None, SHARK_BIRTH=None, SHARK_STARVE=None, Verbose=2):
    '''
    Wa-Tor
    ----------
    [sim_cellular_automata.1]\n
    Wa-Tor - an ecological simulation of predator-prey populations - is based upon A. K. Dewdney's 'Computer Recreations' article in the December 1984 issue of Scientific American.\n
    Arguments
    ----------
    - TARGET_TEMPLATE [`optional input grid`] : Target System. use this grid's system for output grids
    - GRID [`output grid`] : Wa-Tor
    - TABLE [`output table`] : Cycles
    - TARGET_DEFINITION [`choice`] : Target Grid System. Available Choices: [0] user defined [1] grid or grid system Default: 0
    - TARGET_USER_SIZE [`floating point number`] : Cellsize. Minimum: 0.000000 Default: 1.000000
    - TARGET_USER_XMIN [`floating point number`] : West. Default: 0.000000
    - TARGET_USER_XMAX [`floating point number`] : East. Default: 100.000000
    - TARGET_USER_YMIN [`floating point number`] : South. Default: 0.000000
    - TARGET_USER_YMAX [`floating point number`] : North. Default: 100.000000
    - TARGET_USER_COLS [`integer number`] : Columns. Minimum: 1 Default: 101 Number of cells in East-West direction.
    - TARGET_USER_ROWS [`integer number`] : Rows. Minimum: 1 Default: 101 Number of cells in North-South direction.
    - TARGET_USER_FLAT [`boolean`] : Rounding. Default: 1 Round bounding coordinates to multiples of cell size. Ignored if cell size has decimal places.
    - TARGET_USER_FITS [`choice`] : Fit. Available Choices: [0] nodes [1] cells Default: 0
    - TARGET_SYSTEM [`grid system`] : Grid System
    - REFRESH [`boolean`] : Refresh. Default: 1
    - INIT_FISH [`floating point number`] : Initial Number of Fishes [%]. Minimum: 0.000000 Maximum: 100.000000 Default: 30.000000
    - INIT_SHARK [`floating point number`] : Initial Number of Sharks [%]. Minimum: 0.000000 Maximum: 100.000000 Default: 7.500000
    - FISH_BIRTH [`integer number`] : Birth Rate of Fishes. Minimum: 0 Default: 3
    - SHARK_BIRTH [`integer number`] : Birth Rate of Sharks. Minimum: 0 Default: 12
    - SHARK_STARVE [`integer number`] : Max. Starvation Time for Sharks. Minimum: 0 Default: 4

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('sim_cellular_automata', '1', 'Wa-Tor')
    if Tool.is_Okay():
        Tool.Set_Input ('TARGET_TEMPLATE', TARGET_TEMPLATE)
        Tool.Set_Output('GRID', GRID)
        Tool.Set_Output('TABLE', TABLE)
        Tool.Set_Option('TARGET_DEFINITION', TARGET_DEFINITION)
        Tool.Set_Option('TARGET_USER_SIZE', TARGET_USER_SIZE)
        Tool.Set_Option('TARGET_USER_XMIN', TARGET_USER_XMIN)
        Tool.Set_Option('TARGET_USER_XMAX', TARGET_USER_XMAX)
        Tool.Set_Option('TARGET_USER_YMIN', TARGET_USER_YMIN)
        Tool.Set_Option('TARGET_USER_YMAX', TARGET_USER_YMAX)
        Tool.Set_Option('TARGET_USER_COLS', TARGET_USER_COLS)
        Tool.Set_Option('TARGET_USER_ROWS', TARGET_USER_ROWS)
        Tool.Set_Option('TARGET_USER_FLAT', TARGET_USER_FLAT)
        Tool.Set_Option('TARGET_USER_FITS', TARGET_USER_FITS)
        Tool.Set_Option('TARGET_SYSTEM', TARGET_SYSTEM)
        Tool.Set_Option('REFRESH', REFRESH)
        Tool.Set_Option('INIT_FISH', INIT_FISH)
        Tool.Set_Option('INIT_SHARK', INIT_SHARK)
        Tool.Set_Option('FISH_BIRTH', FISH_BIRTH)
        Tool.Set_Option('SHARK_BIRTH', SHARK_BIRTH)
        Tool.Set_Option('SHARK_STARVE', SHARK_STARVE)
        return Tool.Execute(Verbose)
    return False

def run_tool_sim_cellular_automata_1(TARGET_TEMPLATE=None, GRID=None, TABLE=None, TARGET_DEFINITION=None, TARGET_USER_SIZE=None, TARGET_USER_XMIN=None, TARGET_USER_XMAX=None, TARGET_USER_YMIN=None, TARGET_USER_YMAX=None, TARGET_USER_COLS=None, TARGET_USER_ROWS=None, TARGET_USER_FLAT=None, TARGET_USER_FITS=None, TARGET_SYSTEM=None, REFRESH=None, INIT_FISH=None, INIT_SHARK=None, FISH_BIRTH=None, SHARK_BIRTH=None, SHARK_STARVE=None, Verbose=2):
    '''
    Wa-Tor
    ----------
    [sim_cellular_automata.1]\n
    Wa-Tor - an ecological simulation of predator-prey populations - is based upon A. K. Dewdney's 'Computer Recreations' article in the December 1984 issue of Scientific American.\n
    Arguments
    ----------
    - TARGET_TEMPLATE [`optional input grid`] : Target System. use this grid's system for output grids
    - GRID [`output grid`] : Wa-Tor
    - TABLE [`output table`] : Cycles
    - TARGET_DEFINITION [`choice`] : Target Grid System. Available Choices: [0] user defined [1] grid or grid system Default: 0
    - TARGET_USER_SIZE [`floating point number`] : Cellsize. Minimum: 0.000000 Default: 1.000000
    - TARGET_USER_XMIN [`floating point number`] : West. Default: 0.000000
    - TARGET_USER_XMAX [`floating point number`] : East. Default: 100.000000
    - TARGET_USER_YMIN [`floating point number`] : South. Default: 0.000000
    - TARGET_USER_YMAX [`floating point number`] : North. Default: 100.000000
    - TARGET_USER_COLS [`integer number`] : Columns. Minimum: 1 Default: 101 Number of cells in East-West direction.
    - TARGET_USER_ROWS [`integer number`] : Rows. Minimum: 1 Default: 101 Number of cells in North-South direction.
    - TARGET_USER_FLAT [`boolean`] : Rounding. Default: 1 Round bounding coordinates to multiples of cell size. Ignored if cell size has decimal places.
    - TARGET_USER_FITS [`choice`] : Fit. Available Choices: [0] nodes [1] cells Default: 0
    - TARGET_SYSTEM [`grid system`] : Grid System
    - REFRESH [`boolean`] : Refresh. Default: 1
    - INIT_FISH [`floating point number`] : Initial Number of Fishes [%]. Minimum: 0.000000 Maximum: 100.000000 Default: 30.000000
    - INIT_SHARK [`floating point number`] : Initial Number of Sharks [%]. Minimum: 0.000000 Maximum: 100.000000 Default: 7.500000
    - FISH_BIRTH [`integer number`] : Birth Rate of Fishes. Minimum: 0 Default: 3
    - SHARK_BIRTH [`integer number`] : Birth Rate of Sharks. Minimum: 0 Default: 12
    - SHARK_STARVE [`integer number`] : Max. Starvation Time for Sharks. Minimum: 0 Default: 4

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('sim_cellular_automata', '1', 'Wa-Tor')
    if Tool.is_Okay():
        Tool.Set_Input ('TARGET_TEMPLATE', TARGET_TEMPLATE)
        Tool.Set_Output('GRID', GRID)
        Tool.Set_Output('TABLE', TABLE)
        Tool.Set_Option('TARGET_DEFINITION', TARGET_DEFINITION)
        Tool.Set_Option('TARGET_USER_SIZE', TARGET_USER_SIZE)
        Tool.Set_Option('TARGET_USER_XMIN', TARGET_USER_XMIN)
        Tool.Set_Option('TARGET_USER_XMAX', TARGET_USER_XMAX)
        Tool.Set_Option('TARGET_USER_YMIN', TARGET_USER_YMIN)
        Tool.Set_Option('TARGET_USER_YMAX', TARGET_USER_YMAX)
        Tool.Set_Option('TARGET_USER_COLS', TARGET_USER_COLS)
        Tool.Set_Option('TARGET_USER_ROWS', TARGET_USER_ROWS)
        Tool.Set_Option('TARGET_USER_FLAT', TARGET_USER_FLAT)
        Tool.Set_Option('TARGET_USER_FITS', TARGET_USER_FITS)
        Tool.Set_Option('TARGET_SYSTEM', TARGET_SYSTEM)
        Tool.Set_Option('REFRESH', REFRESH)
        Tool.Set_Option('INIT_FISH', INIT_FISH)
        Tool.Set_Option('INIT_SHARK', INIT_SHARK)
        Tool.Set_Option('FISH_BIRTH', FISH_BIRTH)
        Tool.Set_Option('SHARK_BIRTH', SHARK_BIRTH)
        Tool.Set_Option('SHARK_STARVE', SHARK_STARVE)
        return Tool.Execute(Verbose)
    return False

def Hodgepodge_Machine(TARGET_TEMPLATE=None, GRID=None, TARGET_DEFINITION=None, TARGET_USER_SIZE=None, TARGET_USER_XMIN=None, TARGET_USER_XMAX=None, TARGET_USER_YMIN=None, TARGET_USER_YMAX=None, TARGET_USER_COLS=None, TARGET_USER_ROWS=None, TARGET_USER_FLAT=None, TARGET_USER_FITS=None, TARGET_SYSTEM=None, REFRESH=None, RADIUS=None, NSTATES=None, TSICK=None, TINFECTED=None, SPEED=None, Verbose=2):
    '''
    Hodgepodge Machine
    ----------
    [sim_cellular_automata.2]\n
    The hodgepodge machine.\n
    Arguments
    ----------
    - TARGET_TEMPLATE [`optional input grid`] : Target System. use this grid's system for output grids
    - GRID [`output grid`] : Hodgepodge
    - TARGET_DEFINITION [`choice`] : Target Grid System. Available Choices: [0] user defined [1] grid or grid system Default: 0
    - TARGET_USER_SIZE [`floating point number`] : Cellsize. Minimum: 0.000000 Default: 1.000000
    - TARGET_USER_XMIN [`floating point number`] : West. Default: 0.000000
    - TARGET_USER_XMAX [`floating point number`] : East. Default: 100.000000
    - TARGET_USER_YMIN [`floating point number`] : South. Default: 0.000000
    - TARGET_USER_YMAX [`floating point number`] : North. Default: 100.000000
    - TARGET_USER_COLS [`integer number`] : Columns. Minimum: 1 Default: 101 Number of cells in East-West direction.
    - TARGET_USER_ROWS [`integer number`] : Rows. Minimum: 1 Default: 101 Number of cells in North-South direction.
    - TARGET_USER_FLAT [`boolean`] : Rounding. Default: 1 Round bounding coordinates to multiples of cell size. Ignored if cell size has decimal places.
    - TARGET_USER_FITS [`choice`] : Fit. Available Choices: [0] nodes [1] cells Default: 0
    - TARGET_SYSTEM [`grid system`] : Grid System
    - REFRESH [`boolean`] : Refresh. Default: 1
    - RADIUS [`integer number`] : Radius. Minimum: 1 Default: 1
    - NSTATES [`integer number`] : Number of Infection States. Minimum: 1 Default: 150
    - TSICK [`integer number`] : Threshold Sickness. Minimum: 1 Default: 1
    - TINFECTED [`integer number`] : Threshold Infected. Minimum: 1 Default: 3
    - SPEED [`integer number`] : Infection Speed. Minimum: 1 Default: 23

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('sim_cellular_automata', '2', 'Hodgepodge Machine')
    if Tool.is_Okay():
        Tool.Set_Input ('TARGET_TEMPLATE', TARGET_TEMPLATE)
        Tool.Set_Output('GRID', GRID)
        Tool.Set_Option('TARGET_DEFINITION', TARGET_DEFINITION)
        Tool.Set_Option('TARGET_USER_SIZE', TARGET_USER_SIZE)
        Tool.Set_Option('TARGET_USER_XMIN', TARGET_USER_XMIN)
        Tool.Set_Option('TARGET_USER_XMAX', TARGET_USER_XMAX)
        Tool.Set_Option('TARGET_USER_YMIN', TARGET_USER_YMIN)
        Tool.Set_Option('TARGET_USER_YMAX', TARGET_USER_YMAX)
        Tool.Set_Option('TARGET_USER_COLS', TARGET_USER_COLS)
        Tool.Set_Option('TARGET_USER_ROWS', TARGET_USER_ROWS)
        Tool.Set_Option('TARGET_USER_FLAT', TARGET_USER_FLAT)
        Tool.Set_Option('TARGET_USER_FITS', TARGET_USER_FITS)
        Tool.Set_Option('TARGET_SYSTEM', TARGET_SYSTEM)
        Tool.Set_Option('REFRESH', REFRESH)
        Tool.Set_Option('RADIUS', RADIUS)
        Tool.Set_Option('NSTATES', NSTATES)
        Tool.Set_Option('TSICK', TSICK)
        Tool.Set_Option('TINFECTED', TINFECTED)
        Tool.Set_Option('SPEED', SPEED)
        return Tool.Execute(Verbose)
    return False

def run_tool_sim_cellular_automata_2(TARGET_TEMPLATE=None, GRID=None, TARGET_DEFINITION=None, TARGET_USER_SIZE=None, TARGET_USER_XMIN=None, TARGET_USER_XMAX=None, TARGET_USER_YMIN=None, TARGET_USER_YMAX=None, TARGET_USER_COLS=None, TARGET_USER_ROWS=None, TARGET_USER_FLAT=None, TARGET_USER_FITS=None, TARGET_SYSTEM=None, REFRESH=None, RADIUS=None, NSTATES=None, TSICK=None, TINFECTED=None, SPEED=None, Verbose=2):
    '''
    Hodgepodge Machine
    ----------
    [sim_cellular_automata.2]\n
    The hodgepodge machine.\n
    Arguments
    ----------
    - TARGET_TEMPLATE [`optional input grid`] : Target System. use this grid's system for output grids
    - GRID [`output grid`] : Hodgepodge
    - TARGET_DEFINITION [`choice`] : Target Grid System. Available Choices: [0] user defined [1] grid or grid system Default: 0
    - TARGET_USER_SIZE [`floating point number`] : Cellsize. Minimum: 0.000000 Default: 1.000000
    - TARGET_USER_XMIN [`floating point number`] : West. Default: 0.000000
    - TARGET_USER_XMAX [`floating point number`] : East. Default: 100.000000
    - TARGET_USER_YMIN [`floating point number`] : South. Default: 0.000000
    - TARGET_USER_YMAX [`floating point number`] : North. Default: 100.000000
    - TARGET_USER_COLS [`integer number`] : Columns. Minimum: 1 Default: 101 Number of cells in East-West direction.
    - TARGET_USER_ROWS [`integer number`] : Rows. Minimum: 1 Default: 101 Number of cells in North-South direction.
    - TARGET_USER_FLAT [`boolean`] : Rounding. Default: 1 Round bounding coordinates to multiples of cell size. Ignored if cell size has decimal places.
    - TARGET_USER_FITS [`choice`] : Fit. Available Choices: [0] nodes [1] cells Default: 0
    - TARGET_SYSTEM [`grid system`] : Grid System
    - REFRESH [`boolean`] : Refresh. Default: 1
    - RADIUS [`integer number`] : Radius. Minimum: 1 Default: 1
    - NSTATES [`integer number`] : Number of Infection States. Minimum: 1 Default: 150
    - TSICK [`integer number`] : Threshold Sickness. Minimum: 1 Default: 1
    - TINFECTED [`integer number`] : Threshold Infected. Minimum: 1 Default: 3
    - SPEED [`integer number`] : Infection Speed. Minimum: 1 Default: 23

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('sim_cellular_automata', '2', 'Hodgepodge Machine')
    if Tool.is_Okay():
        Tool.Set_Input ('TARGET_TEMPLATE', TARGET_TEMPLATE)
        Tool.Set_Output('GRID', GRID)
        Tool.Set_Option('TARGET_DEFINITION', TARGET_DEFINITION)
        Tool.Set_Option('TARGET_USER_SIZE', TARGET_USER_SIZE)
        Tool.Set_Option('TARGET_USER_XMIN', TARGET_USER_XMIN)
        Tool.Set_Option('TARGET_USER_XMAX', TARGET_USER_XMAX)
        Tool.Set_Option('TARGET_USER_YMIN', TARGET_USER_YMIN)
        Tool.Set_Option('TARGET_USER_YMAX', TARGET_USER_YMAX)
        Tool.Set_Option('TARGET_USER_COLS', TARGET_USER_COLS)
        Tool.Set_Option('TARGET_USER_ROWS', TARGET_USER_ROWS)
        Tool.Set_Option('TARGET_USER_FLAT', TARGET_USER_FLAT)
        Tool.Set_Option('TARGET_USER_FITS', TARGET_USER_FITS)
        Tool.Set_Option('TARGET_SYSTEM', TARGET_SYSTEM)
        Tool.Set_Option('REFRESH', REFRESH)
        Tool.Set_Option('RADIUS', RADIUS)
        Tool.Set_Option('NSTATES', NSTATES)
        Tool.Set_Option('TSICK', TSICK)
        Tool.Set_Option('TINFECTED', TINFECTED)
        Tool.Set_Option('SPEED', SPEED)
        return Tool.Execute(Verbose)
    return False

