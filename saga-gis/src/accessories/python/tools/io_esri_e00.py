#! /usr/bin/env python

'''
Python Interface to SAGA Tools Library
----------
- Category : Import/Export
- Name     : ESRI E00
- ID       : io_esri_e00

Description
----------
Import and export filter for ESRI's E00 file exchange format.
'''

from PySAGA.helper import Tool_Wrapper

def Import_ESRI_E00_File(TABLES=None, SHAPES=None, GRIDS=None, FILE=None, BBND=None, BTIC=None, BTABLES=None, Verbose=2):
    '''
    Import ESRI E00 File
    ----------
    [io_esri_e00.0]\n
    Import data sets from ESRI's E00 interchange format.\n
    This import filter is based on the E00 format analysis of the GRASS GIS tool 'm.in.e00' written by Michel J. Wurtz. Go to the [GRASS GIS Hompage](http://grass.itc.it/) for more information.\n
    The ['E00Compr' library](http://avce00.maptools.org/e00compr/index.html) written by Daniel Morissette has been used for e00 file access, so that compressed e00 files also can be read.\n
    Arguments
    ----------
    - TABLES [`output table list`] : Tables
    - SHAPES [`output shapes list`] : Shapes
    - GRIDS [`output grid list`] : Grids
    - FILE [`file path`] : File
    - BBND [`boolean`] : Import Extents. Default: 0
    - BTIC [`boolean`] : Import Tick Points. Default: 0
    - BTABLES [`boolean`] : Import Tables. Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('io_esri_e00', '0', 'Import ESRI E00 File')
    if Tool.is_Okay():
        Tool.Set_Output('TABLES', TABLES)
        Tool.Set_Output('SHAPES', SHAPES)
        Tool.Set_Output('GRIDS', GRIDS)
        Tool.Set_Option('FILE', FILE)
        Tool.Set_Option('BBND', BBND)
        Tool.Set_Option('BTIC', BTIC)
        Tool.Set_Option('BTABLES', BTABLES)
        return Tool.Execute(Verbose)
    return False

def run_tool_io_esri_e00_0(TABLES=None, SHAPES=None, GRIDS=None, FILE=None, BBND=None, BTIC=None, BTABLES=None, Verbose=2):
    '''
    Import ESRI E00 File
    ----------
    [io_esri_e00.0]\n
    Import data sets from ESRI's E00 interchange format.\n
    This import filter is based on the E00 format analysis of the GRASS GIS tool 'm.in.e00' written by Michel J. Wurtz. Go to the [GRASS GIS Hompage](http://grass.itc.it/) for more information.\n
    The ['E00Compr' library](http://avce00.maptools.org/e00compr/index.html) written by Daniel Morissette has been used for e00 file access, so that compressed e00 files also can be read.\n
    Arguments
    ----------
    - TABLES [`output table list`] : Tables
    - SHAPES [`output shapes list`] : Shapes
    - GRIDS [`output grid list`] : Grids
    - FILE [`file path`] : File
    - BBND [`boolean`] : Import Extents. Default: 0
    - BTIC [`boolean`] : Import Tick Points. Default: 0
    - BTABLES [`boolean`] : Import Tables. Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('io_esri_e00', '0', 'Import ESRI E00 File')
    if Tool.is_Okay():
        Tool.Set_Output('TABLES', TABLES)
        Tool.Set_Output('SHAPES', SHAPES)
        Tool.Set_Output('GRIDS', GRIDS)
        Tool.Set_Option('FILE', FILE)
        Tool.Set_Option('BBND', BBND)
        Tool.Set_Option('BTIC', BTIC)
        Tool.Set_Option('BTABLES', BTABLES)
        return Tool.Execute(Verbose)
    return False

