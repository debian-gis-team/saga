#! /usr/bin/env python

'''
Python Interface to SAGA Tools Library
----------
- Category : Simulation
- Name     : Modelling the Human Impact on Nature
- ID       : sim_ecosystems_hugget

Description
----------
Numerical models for ecological processes. Examples have been taken from:
Hugget, R.J. (1993): 'Modelling the Human Impact on Nature', Oxford University Press.
'''

from PySAGA.helper import Tool_Wrapper

def A_Simple_Litter_System(TABLE=None, TIME_SPAN=None, TIME_STEP=None, C_INIT=None, PRESETS=None, C_INPUT=None, C_OUTPUT=None, Verbose=2):
    '''
    A Simple Litter System
    ----------
    [sim_ecosystems_hugget.0]\n
    A simple numerical litter system model demonstrating the usage of the Euler method. Litter carbon storage (C) is calculated in dependency of litter fall rate (Cinput) and rate constant for litter loss (Closs) as:\n
    (-) C(t + 1) = C(t) + (Cinput - Closs * C(t)) * dt\n
    Typical values:\n
    ============\n
    Tropical Rainforest	Temperate forest	Boreal forest\n
    Litter fall rate [g/m<sup>2</sup>/yr]	500	240	50\n
    Litter loss rate [1/yr]	2.0	0.4	0.05\n
    ============\n
    Arguments
    ----------
    - TABLE [`output table`] : Litter
    - TIME_SPAN [`floating point number`] : Time Span. Minimum: 0.000000 Default: 50.000000 [yrs]
    - TIME_STEP [`floating point number`] : Time Interval. Minimum: 0.000000 Default: 0.100000 [yrs]
    - C_INIT [`floating point number`] : Initial Litter Storage. Minimum: 0.000000 Default: 0.000000 [g/m2]
    - PRESETS [`choice`] : Presets. Available Choices: [0] Tropical forest [1] Temperate forest [2] Boreal forest [3] Adjust parameters manually Default: 3
    - C_INPUT [`floating point number`] : Litterfall Rate. Minimum: 0.000000 Default: 240.000000 [g/m2/yr]
    - C_OUTPUT [`floating point number`] : Rate Constant for Litter Loss. Minimum: 0.000000 Default: 0.400000 [1/yr]

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('sim_ecosystems_hugget', '0', 'A Simple Litter System')
    if Tool.is_Okay():
        Tool.Set_Output('TABLE', TABLE)
        Tool.Set_Option('TIME_SPAN', TIME_SPAN)
        Tool.Set_Option('TIME_STEP', TIME_STEP)
        Tool.Set_Option('C_INIT', C_INIT)
        Tool.Set_Option('PRESETS', PRESETS)
        Tool.Set_Option('C_INPUT', C_INPUT)
        Tool.Set_Option('C_OUTPUT', C_OUTPUT)
        return Tool.Execute(Verbose)
    return False

def run_tool_sim_ecosystems_hugget_0(TABLE=None, TIME_SPAN=None, TIME_STEP=None, C_INIT=None, PRESETS=None, C_INPUT=None, C_OUTPUT=None, Verbose=2):
    '''
    A Simple Litter System
    ----------
    [sim_ecosystems_hugget.0]\n
    A simple numerical litter system model demonstrating the usage of the Euler method. Litter carbon storage (C) is calculated in dependency of litter fall rate (Cinput) and rate constant for litter loss (Closs) as:\n
    (-) C(t + 1) = C(t) + (Cinput - Closs * C(t)) * dt\n
    Typical values:\n
    ============\n
    Tropical Rainforest	Temperate forest	Boreal forest\n
    Litter fall rate [g/m<sup>2</sup>/yr]	500	240	50\n
    Litter loss rate [1/yr]	2.0	0.4	0.05\n
    ============\n
    Arguments
    ----------
    - TABLE [`output table`] : Litter
    - TIME_SPAN [`floating point number`] : Time Span. Minimum: 0.000000 Default: 50.000000 [yrs]
    - TIME_STEP [`floating point number`] : Time Interval. Minimum: 0.000000 Default: 0.100000 [yrs]
    - C_INIT [`floating point number`] : Initial Litter Storage. Minimum: 0.000000 Default: 0.000000 [g/m2]
    - PRESETS [`choice`] : Presets. Available Choices: [0] Tropical forest [1] Temperate forest [2] Boreal forest [3] Adjust parameters manually Default: 3
    - C_INPUT [`floating point number`] : Litterfall Rate. Minimum: 0.000000 Default: 240.000000 [g/m2/yr]
    - C_OUTPUT [`floating point number`] : Rate Constant for Litter Loss. Minimum: 0.000000 Default: 0.400000 [1/yr]

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('sim_ecosystems_hugget', '0', 'A Simple Litter System')
    if Tool.is_Okay():
        Tool.Set_Output('TABLE', TABLE)
        Tool.Set_Option('TIME_SPAN', TIME_SPAN)
        Tool.Set_Option('TIME_STEP', TIME_STEP)
        Tool.Set_Option('C_INIT', C_INIT)
        Tool.Set_Option('PRESETS', PRESETS)
        Tool.Set_Option('C_INPUT', C_INPUT)
        Tool.Set_Option('C_OUTPUT', C_OUTPUT)
        return Tool.Execute(Verbose)
    return False

def Carbon_Cycle_Simulation_for_Terrestrial_Biomass(TABLE=None, TIME_SPAN=None, TIME_STEP=None, PRESETS=None, PRIMPROD=None, P_LEAV=None, P_BRAN=None, P_STEM=None, P_ROOT=None, K_LEAV_LITT=None, K_BRAN_LITT=None, K_STEM_LITT=None, K_ROOT_HUMU=None, K_LITT_HUMU=None, K_HUMU_COAL=None, K_COAL_ENVI=None, CHUMIFY=None, CCARBON=None, Verbose=2):
    '''
    Carbon Cycle Simulation for Terrestrial Biomass
    ----------
    [sim_ecosystems_hugget.1]\n
    Simulation of the Carbon Cycle in Terrestrial Biomass.\n
    Arguments
    ----------
    - TABLE [`output table`] : Terrestrial Carbon
    - TIME_SPAN [`floating point number`] : Time Span. Minimum: 0.000000 Default: 500.000000 [yrs]
    - TIME_STEP [`floating point number`] : Time Interval. Minimum: 0.000000 Default: 0.100000 [yrs]
    - PRESETS [`choice`] : Presets. Available Choices: [0] Tropical forest [1] Temperate forest [2] Grassland [3] Agricultural area [4] Human area [5] Tundra and semi-desert [6] Adjust parameters manually Default: 0
    - PRIMPROD [`floating point number`] : Net Primary Production. Minimum: 0.000000 Default: 27.800000 [Gt C / yr]
    - P_LEAV [`floating point number`] : Leaves. Minimum: 0.000000 Default: 0.300000 fraction
    - P_BRAN [`floating point number`] : Branches. Minimum: 0.000000 Default: 0.200000 fraction
    - P_STEM [`floating point number`] : Stems. Minimum: 0.000000 Default: 0.300000 fraction
    - P_ROOT [`floating point number`] : Roots. Minimum: 0.000000 Default: 0.200000 fraction
    - K_LEAV_LITT [`floating point number`] : Leaves to Litter. Minimum: 0.000000 Default: 1.000000 [1 / yr]
    - K_BRAN_LITT [`floating point number`] : Branches to Litter. Minimum: 0.000000 Default: 0.100000 [1 / yr]
    - K_STEM_LITT [`floating point number`] : Stems to Litter. Minimum: 0.000000 Default: 0.033000 [1 / yr]
    - K_ROOT_HUMU [`floating point number`] : Roots to Humus. Minimum: 0.000000 Default: 0.100000 [1 / yr]
    - K_LITT_HUMU [`floating point number`] : Litter to Humus. Minimum: 0.000000 Default: 1.000000 [1 / yr]
    - K_HUMU_COAL [`floating point number`] : Humus to Charcoal. Minimum: 0.000000 Default: 0.100000 [1 / yr]
    - K_COAL_ENVI [`floating point number`] : Charcoal to Environment. Minimum: 0.000000 Default: 0.002000 [1 / yr]
    - CHUMIFY [`floating point number`] : Humification. Minimum: 0.000000 Default: 0.400000 fraction
    - CCARBON [`floating point number`] : Carbonization. Minimum: 0.000000 Default: 0.050000 fraction

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('sim_ecosystems_hugget', '1', 'Carbon Cycle Simulation for Terrestrial Biomass')
    if Tool.is_Okay():
        Tool.Set_Output('TABLE', TABLE)
        Tool.Set_Option('TIME_SPAN', TIME_SPAN)
        Tool.Set_Option('TIME_STEP', TIME_STEP)
        Tool.Set_Option('PRESETS', PRESETS)
        Tool.Set_Option('PRIMPROD', PRIMPROD)
        Tool.Set_Option('P_LEAV', P_LEAV)
        Tool.Set_Option('P_BRAN', P_BRAN)
        Tool.Set_Option('P_STEM', P_STEM)
        Tool.Set_Option('P_ROOT', P_ROOT)
        Tool.Set_Option('K_LEAV_LITT', K_LEAV_LITT)
        Tool.Set_Option('K_BRAN_LITT', K_BRAN_LITT)
        Tool.Set_Option('K_STEM_LITT', K_STEM_LITT)
        Tool.Set_Option('K_ROOT_HUMU', K_ROOT_HUMU)
        Tool.Set_Option('K_LITT_HUMU', K_LITT_HUMU)
        Tool.Set_Option('K_HUMU_COAL', K_HUMU_COAL)
        Tool.Set_Option('K_COAL_ENVI', K_COAL_ENVI)
        Tool.Set_Option('CHUMIFY', CHUMIFY)
        Tool.Set_Option('CCARBON', CCARBON)
        return Tool.Execute(Verbose)
    return False

def run_tool_sim_ecosystems_hugget_1(TABLE=None, TIME_SPAN=None, TIME_STEP=None, PRESETS=None, PRIMPROD=None, P_LEAV=None, P_BRAN=None, P_STEM=None, P_ROOT=None, K_LEAV_LITT=None, K_BRAN_LITT=None, K_STEM_LITT=None, K_ROOT_HUMU=None, K_LITT_HUMU=None, K_HUMU_COAL=None, K_COAL_ENVI=None, CHUMIFY=None, CCARBON=None, Verbose=2):
    '''
    Carbon Cycle Simulation for Terrestrial Biomass
    ----------
    [sim_ecosystems_hugget.1]\n
    Simulation of the Carbon Cycle in Terrestrial Biomass.\n
    Arguments
    ----------
    - TABLE [`output table`] : Terrestrial Carbon
    - TIME_SPAN [`floating point number`] : Time Span. Minimum: 0.000000 Default: 500.000000 [yrs]
    - TIME_STEP [`floating point number`] : Time Interval. Minimum: 0.000000 Default: 0.100000 [yrs]
    - PRESETS [`choice`] : Presets. Available Choices: [0] Tropical forest [1] Temperate forest [2] Grassland [3] Agricultural area [4] Human area [5] Tundra and semi-desert [6] Adjust parameters manually Default: 0
    - PRIMPROD [`floating point number`] : Net Primary Production. Minimum: 0.000000 Default: 27.800000 [Gt C / yr]
    - P_LEAV [`floating point number`] : Leaves. Minimum: 0.000000 Default: 0.300000 fraction
    - P_BRAN [`floating point number`] : Branches. Minimum: 0.000000 Default: 0.200000 fraction
    - P_STEM [`floating point number`] : Stems. Minimum: 0.000000 Default: 0.300000 fraction
    - P_ROOT [`floating point number`] : Roots. Minimum: 0.000000 Default: 0.200000 fraction
    - K_LEAV_LITT [`floating point number`] : Leaves to Litter. Minimum: 0.000000 Default: 1.000000 [1 / yr]
    - K_BRAN_LITT [`floating point number`] : Branches to Litter. Minimum: 0.000000 Default: 0.100000 [1 / yr]
    - K_STEM_LITT [`floating point number`] : Stems to Litter. Minimum: 0.000000 Default: 0.033000 [1 / yr]
    - K_ROOT_HUMU [`floating point number`] : Roots to Humus. Minimum: 0.000000 Default: 0.100000 [1 / yr]
    - K_LITT_HUMU [`floating point number`] : Litter to Humus. Minimum: 0.000000 Default: 1.000000 [1 / yr]
    - K_HUMU_COAL [`floating point number`] : Humus to Charcoal. Minimum: 0.000000 Default: 0.100000 [1 / yr]
    - K_COAL_ENVI [`floating point number`] : Charcoal to Environment. Minimum: 0.000000 Default: 0.002000 [1 / yr]
    - CHUMIFY [`floating point number`] : Humification. Minimum: 0.000000 Default: 0.400000 fraction
    - CCARBON [`floating point number`] : Carbonization. Minimum: 0.000000 Default: 0.050000 fraction

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('sim_ecosystems_hugget', '1', 'Carbon Cycle Simulation for Terrestrial Biomass')
    if Tool.is_Okay():
        Tool.Set_Output('TABLE', TABLE)
        Tool.Set_Option('TIME_SPAN', TIME_SPAN)
        Tool.Set_Option('TIME_STEP', TIME_STEP)
        Tool.Set_Option('PRESETS', PRESETS)
        Tool.Set_Option('PRIMPROD', PRIMPROD)
        Tool.Set_Option('P_LEAV', P_LEAV)
        Tool.Set_Option('P_BRAN', P_BRAN)
        Tool.Set_Option('P_STEM', P_STEM)
        Tool.Set_Option('P_ROOT', P_ROOT)
        Tool.Set_Option('K_LEAV_LITT', K_LEAV_LITT)
        Tool.Set_Option('K_BRAN_LITT', K_BRAN_LITT)
        Tool.Set_Option('K_STEM_LITT', K_STEM_LITT)
        Tool.Set_Option('K_ROOT_HUMU', K_ROOT_HUMU)
        Tool.Set_Option('K_LITT_HUMU', K_LITT_HUMU)
        Tool.Set_Option('K_HUMU_COAL', K_HUMU_COAL)
        Tool.Set_Option('K_COAL_ENVI', K_COAL_ENVI)
        Tool.Set_Option('CHUMIFY', CHUMIFY)
        Tool.Set_Option('CCARBON', CCARBON)
        return Tool.Execute(Verbose)
    return False

def Spatially_Distributed_Simulation_of_Soil_Nitrogen_Dynamics(DEM=None, NSTORE=None, TIME_SPAN=None, TIME_STEP=None, NINIT=None, NRAIN=None, UPDATE=None, Verbose=2):
    '''
    Spatially Distributed Simulation of Soil Nitrogen Dynamics
    ----------
    [sim_ecosystems_hugget.2]\n
    A simple spatially distributed simulation model of soil nitrogen dynamics.\n
    Arguments
    ----------
    - DEM [`input grid`] : Elevation
    - NSTORE [`output grid`] : Soil Nitrogen
    - TIME_SPAN [`floating point number`] : Time Span. Minimum: 0.000000 Default: 100.000000 [yrs]
    - TIME_STEP [`floating point number`] : Time Interval. Minimum: 0.000000 Default: 1.000000 [yrs]
    - NINIT [`floating point number`] : Initial Nitrogen Content. Minimum: 0.000000 Default: 5000.000000 [kg/ha]
    - NRAIN [`floating point number`] : Nitrogen in Rainfall. Minimum: 0.000000 Default: 16.000000 [kg/ha/yr]
    - UPDATE [`boolean`] : Update View. Default: 1

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('sim_ecosystems_hugget', '2', 'Spatially Distributed Simulation of Soil Nitrogen Dynamics')
    if Tool.is_Okay():
        Tool.Set_Input ('DEM', DEM)
        Tool.Set_Output('NSTORE', NSTORE)
        Tool.Set_Option('TIME_SPAN', TIME_SPAN)
        Tool.Set_Option('TIME_STEP', TIME_STEP)
        Tool.Set_Option('NINIT', NINIT)
        Tool.Set_Option('NRAIN', NRAIN)
        Tool.Set_Option('UPDATE', UPDATE)
        return Tool.Execute(Verbose)
    return False

def run_tool_sim_ecosystems_hugget_2(DEM=None, NSTORE=None, TIME_SPAN=None, TIME_STEP=None, NINIT=None, NRAIN=None, UPDATE=None, Verbose=2):
    '''
    Spatially Distributed Simulation of Soil Nitrogen Dynamics
    ----------
    [sim_ecosystems_hugget.2]\n
    A simple spatially distributed simulation model of soil nitrogen dynamics.\n
    Arguments
    ----------
    - DEM [`input grid`] : Elevation
    - NSTORE [`output grid`] : Soil Nitrogen
    - TIME_SPAN [`floating point number`] : Time Span. Minimum: 0.000000 Default: 100.000000 [yrs]
    - TIME_STEP [`floating point number`] : Time Interval. Minimum: 0.000000 Default: 1.000000 [yrs]
    - NINIT [`floating point number`] : Initial Nitrogen Content. Minimum: 0.000000 Default: 5000.000000 [kg/ha]
    - NRAIN [`floating point number`] : Nitrogen in Rainfall. Minimum: 0.000000 Default: 16.000000 [kg/ha/yr]
    - UPDATE [`boolean`] : Update View. Default: 1

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('sim_ecosystems_hugget', '2', 'Spatially Distributed Simulation of Soil Nitrogen Dynamics')
    if Tool.is_Okay():
        Tool.Set_Input ('DEM', DEM)
        Tool.Set_Output('NSTORE', NSTORE)
        Tool.Set_Option('TIME_SPAN', TIME_SPAN)
        Tool.Set_Option('TIME_STEP', TIME_STEP)
        Tool.Set_Option('NINIT', NINIT)
        Tool.Set_Option('NRAIN', NRAIN)
        Tool.Set_Option('UPDATE', UPDATE)
        return Tool.Execute(Verbose)
    return False

