#! /usr/bin/env python

'''
Python Interface to SAGA Tools Library
----------
- Category : Imagery
- Name     : Segmentation
- ID       : imagery_segmentation

Description
----------
Image segmentation algorithms.
'''

from PySAGA.helper import Tool_Wrapper

def Watershed_Segmentation(GRID=None, SEGMENTS=None, SEEDS=None, BORDERS=None, OUTPUT=None, DOWN=None, JOIN=None, THRESHOLD=None, EDGE=None, BBORDERS=None, Verbose=2):
    '''
    Watershed Segmentation
    ----------
    [imagery_segmentation.0]\n
    Watershed segmentation.\n
    Arguments
    ----------
    - GRID [`input grid`] : Grid
    - SEGMENTS [`output grid`] : Segments
    - SEEDS [`output shapes`] : Seed Points
    - BORDERS [`output data object`] : Borders
    - OUTPUT [`choice`] : Output. Available Choices: [0] Seed Value [1] Segment ID Default: 1 The values of the resultant grid can be either the seed value (e.g. the local maximum) or the enumerated segment id.
    - DOWN [`choice`] : Method. Available Choices: [0] Minima [1] Maxima Default: 1 Choose if you want to segmentate either on minima or on maxima.
    - JOIN [`choice`] : Join Segments based on Threshold Value. Available Choices: [0] do not join [1] seed to saddle difference [2] seeds difference Default: 0 Join segments based on threshold value.
    - THRESHOLD [`floating point number`] : Threshold. Minimum: 0.000000 Default: 0.000000 Specify a threshold value as minimum difference between neighboured segments.
    - EDGE [`boolean`] : Allow Edge Pixels to be Seeds. Default: 1
    - BBORDERS [`boolean`] : Borders. Default: 0 Create borders between segments as new grid.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('imagery_segmentation', '0', 'Watershed Segmentation')
    if Tool.is_Okay():
        Tool.Set_Input ('GRID', GRID)
        Tool.Set_Output('SEGMENTS', SEGMENTS)
        Tool.Set_Output('SEEDS', SEEDS)
        Tool.Set_Output('BORDERS', BORDERS)
        Tool.Set_Option('OUTPUT', OUTPUT)
        Tool.Set_Option('DOWN', DOWN)
        Tool.Set_Option('JOIN', JOIN)
        Tool.Set_Option('THRESHOLD', THRESHOLD)
        Tool.Set_Option('EDGE', EDGE)
        Tool.Set_Option('BBORDERS', BBORDERS)
        return Tool.Execute(Verbose)
    return False

def run_tool_imagery_segmentation_0(GRID=None, SEGMENTS=None, SEEDS=None, BORDERS=None, OUTPUT=None, DOWN=None, JOIN=None, THRESHOLD=None, EDGE=None, BBORDERS=None, Verbose=2):
    '''
    Watershed Segmentation
    ----------
    [imagery_segmentation.0]\n
    Watershed segmentation.\n
    Arguments
    ----------
    - GRID [`input grid`] : Grid
    - SEGMENTS [`output grid`] : Segments
    - SEEDS [`output shapes`] : Seed Points
    - BORDERS [`output data object`] : Borders
    - OUTPUT [`choice`] : Output. Available Choices: [0] Seed Value [1] Segment ID Default: 1 The values of the resultant grid can be either the seed value (e.g. the local maximum) or the enumerated segment id.
    - DOWN [`choice`] : Method. Available Choices: [0] Minima [1] Maxima Default: 1 Choose if you want to segmentate either on minima or on maxima.
    - JOIN [`choice`] : Join Segments based on Threshold Value. Available Choices: [0] do not join [1] seed to saddle difference [2] seeds difference Default: 0 Join segments based on threshold value.
    - THRESHOLD [`floating point number`] : Threshold. Minimum: 0.000000 Default: 0.000000 Specify a threshold value as minimum difference between neighboured segments.
    - EDGE [`boolean`] : Allow Edge Pixels to be Seeds. Default: 1
    - BBORDERS [`boolean`] : Borders. Default: 0 Create borders between segments as new grid.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('imagery_segmentation', '0', 'Watershed Segmentation')
    if Tool.is_Okay():
        Tool.Set_Input ('GRID', GRID)
        Tool.Set_Output('SEGMENTS', SEGMENTS)
        Tool.Set_Output('SEEDS', SEEDS)
        Tool.Set_Output('BORDERS', BORDERS)
        Tool.Set_Option('OUTPUT', OUTPUT)
        Tool.Set_Option('DOWN', DOWN)
        Tool.Set_Option('JOIN', JOIN)
        Tool.Set_Option('THRESHOLD', THRESHOLD)
        Tool.Set_Option('EDGE', EDGE)
        Tool.Set_Option('BBORDERS', BBORDERS)
        return Tool.Execute(Verbose)
    return False

def Grid_Skeletonization(INPUT=None, RESULT=None, VECTOR=None, METHOD=None, INIT_METHOD=None, INIT_THRESHOLD=None, CONVERGENCE=None, Verbose=2):
    '''
    Grid Skeletonization
    ----------
    [imagery_segmentation.1]\n
    Simple skeletonisation methods for grids.\n
    Arguments
    ----------
    - INPUT [`input grid`] : Grid
    - RESULT [`output grid`] : Skeleton
    - VECTOR [`output shapes`] : Skeleton
    - METHOD [`choice`] : Method. Available Choices: [0] Standard [1] Hilditch's Algorithm [2] Channel Skeleton Default: 0
    - INIT_METHOD [`choice`] : Initialisation. Available Choices: [0] less than [1] greater than [2] less than or equal [3] greater than or equal Default: 1 Initialize a grid cell as potential skeleton member based on given threshold.
    - INIT_THRESHOLD [`floating point number`] : Threshold. Default: 0.000000
    - CONVERGENCE [`integer number`] : Convergence. Minimum: 0 Default: 3

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('imagery_segmentation', '1', 'Grid Skeletonization')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Output('RESULT', RESULT)
        Tool.Set_Output('VECTOR', VECTOR)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('INIT_METHOD', INIT_METHOD)
        Tool.Set_Option('INIT_THRESHOLD', INIT_THRESHOLD)
        Tool.Set_Option('CONVERGENCE', CONVERGENCE)
        return Tool.Execute(Verbose)
    return False

def run_tool_imagery_segmentation_1(INPUT=None, RESULT=None, VECTOR=None, METHOD=None, INIT_METHOD=None, INIT_THRESHOLD=None, CONVERGENCE=None, Verbose=2):
    '''
    Grid Skeletonization
    ----------
    [imagery_segmentation.1]\n
    Simple skeletonisation methods for grids.\n
    Arguments
    ----------
    - INPUT [`input grid`] : Grid
    - RESULT [`output grid`] : Skeleton
    - VECTOR [`output shapes`] : Skeleton
    - METHOD [`choice`] : Method. Available Choices: [0] Standard [1] Hilditch's Algorithm [2] Channel Skeleton Default: 0
    - INIT_METHOD [`choice`] : Initialisation. Available Choices: [0] less than [1] greater than [2] less than or equal [3] greater than or equal Default: 1 Initialize a grid cell as potential skeleton member based on given threshold.
    - INIT_THRESHOLD [`floating point number`] : Threshold. Default: 0.000000
    - CONVERGENCE [`integer number`] : Convergence. Minimum: 0 Default: 3

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('imagery_segmentation', '1', 'Grid Skeletonization')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Output('RESULT', RESULT)
        Tool.Set_Output('VECTOR', VECTOR)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('INIT_METHOD', INIT_METHOD)
        Tool.Set_Option('INIT_THRESHOLD', INIT_THRESHOLD)
        Tool.Set_Option('CONVERGENCE', CONVERGENCE)
        return Tool.Execute(Verbose)
    return False

def Seed_Generation(FEATURES=None, VARIANCE=None, SEED_GRID=None, SEED_POINTS=None, SEED_TYPE=None, METHOD=None, BAND_WIDTH=None, NORMALIZE=None, DW_WEIGHTING=None, DW_IDW_POWER=None, DW_BANDWIDTH=None, Verbose=2):
    '''
    Seed Generation
    ----------
    [imagery_segmentation.2]\n
    The tool allows one to create seed points from a stack of input features. Such seed points can be used, for example, as input in the 'Seeded Region Growing' tool.\n
    Arguments
    ----------
    - FEATURES [`input grid list`] : Features
    - VARIANCE [`output grid`] : Variance
    - SEED_GRID [`output grid`] : Seeds Grid
    - SEED_POINTS [`output shapes`] : Seed Points
    - SEED_TYPE [`choice`] : Seed Type. Available Choices: [0] minima of variance [1] maxima of variance Default: 0
    - METHOD [`choice`] : Method. Available Choices: [0] band width smoothing [1] band width search Default: 0
    - BAND_WIDTH [`floating point number`] : Bandwidth (Cells). Minimum: 1.000000 Default: 10.000000
    - NORMALIZE [`boolean`] : Normalize Features. Default: 0 Standardize the input features, i.e. rescale the input data (features) such that the mean equals 0 and the standard deviation equals 1. This is helpful when the input features have different scales, units or outliers.
    - DW_WEIGHTING [`choice`] : Weighting Function. Available Choices: [0] no distance weighting [1] inverse distance to a power [2] exponential [3] gaussian Default: 3
    - DW_IDW_POWER [`floating point number`] : Power. Minimum: 0.000000 Default: 2.000000
    - DW_BANDWIDTH [`floating point number`] : Bandwidth. Minimum: 0.000000 Default: 5.000000 Bandwidth for exponential and Gaussian weighting

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('imagery_segmentation', '2', 'Seed Generation')
    if Tool.is_Okay():
        Tool.Set_Input ('FEATURES', FEATURES)
        Tool.Set_Output('VARIANCE', VARIANCE)
        Tool.Set_Output('SEED_GRID', SEED_GRID)
        Tool.Set_Output('SEED_POINTS', SEED_POINTS)
        Tool.Set_Option('SEED_TYPE', SEED_TYPE)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('BAND_WIDTH', BAND_WIDTH)
        Tool.Set_Option('NORMALIZE', NORMALIZE)
        Tool.Set_Option('DW_WEIGHTING', DW_WEIGHTING)
        Tool.Set_Option('DW_IDW_POWER', DW_IDW_POWER)
        Tool.Set_Option('DW_BANDWIDTH', DW_BANDWIDTH)
        return Tool.Execute(Verbose)
    return False

def run_tool_imagery_segmentation_2(FEATURES=None, VARIANCE=None, SEED_GRID=None, SEED_POINTS=None, SEED_TYPE=None, METHOD=None, BAND_WIDTH=None, NORMALIZE=None, DW_WEIGHTING=None, DW_IDW_POWER=None, DW_BANDWIDTH=None, Verbose=2):
    '''
    Seed Generation
    ----------
    [imagery_segmentation.2]\n
    The tool allows one to create seed points from a stack of input features. Such seed points can be used, for example, as input in the 'Seeded Region Growing' tool.\n
    Arguments
    ----------
    - FEATURES [`input grid list`] : Features
    - VARIANCE [`output grid`] : Variance
    - SEED_GRID [`output grid`] : Seeds Grid
    - SEED_POINTS [`output shapes`] : Seed Points
    - SEED_TYPE [`choice`] : Seed Type. Available Choices: [0] minima of variance [1] maxima of variance Default: 0
    - METHOD [`choice`] : Method. Available Choices: [0] band width smoothing [1] band width search Default: 0
    - BAND_WIDTH [`floating point number`] : Bandwidth (Cells). Minimum: 1.000000 Default: 10.000000
    - NORMALIZE [`boolean`] : Normalize Features. Default: 0 Standardize the input features, i.e. rescale the input data (features) such that the mean equals 0 and the standard deviation equals 1. This is helpful when the input features have different scales, units or outliers.
    - DW_WEIGHTING [`choice`] : Weighting Function. Available Choices: [0] no distance weighting [1] inverse distance to a power [2] exponential [3] gaussian Default: 3
    - DW_IDW_POWER [`floating point number`] : Power. Minimum: 0.000000 Default: 2.000000
    - DW_BANDWIDTH [`floating point number`] : Bandwidth. Minimum: 0.000000 Default: 5.000000 Bandwidth for exponential and Gaussian weighting

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('imagery_segmentation', '2', 'Seed Generation')
    if Tool.is_Okay():
        Tool.Set_Input ('FEATURES', FEATURES)
        Tool.Set_Output('VARIANCE', VARIANCE)
        Tool.Set_Output('SEED_GRID', SEED_GRID)
        Tool.Set_Output('SEED_POINTS', SEED_POINTS)
        Tool.Set_Option('SEED_TYPE', SEED_TYPE)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('BAND_WIDTH', BAND_WIDTH)
        Tool.Set_Option('NORMALIZE', NORMALIZE)
        Tool.Set_Option('DW_WEIGHTING', DW_WEIGHTING)
        Tool.Set_Option('DW_IDW_POWER', DW_IDW_POWER)
        Tool.Set_Option('DW_BANDWIDTH', DW_BANDWIDTH)
        return Tool.Execute(Verbose)
    return False

def Seeded_Region_Growing(SEEDS=None, FEATURES=None, SEGMENTS=None, SIMILARITY=None, TABLE=None, NORMALIZE=None, NEIGHBOUR=None, METHOD=None, SIG_1=None, SIG_2=None, THRESHOLD=None, REFRESH=None, LEAFSIZE=None, Verbose=2):
    '''
    Seeded Region Growing
    ----------
    [imagery_segmentation.3]\n
    The tool allows one to apply a seeded region growing algorithm to a stack of input features and thus to segmentize the data for object extraction. The required seed points can be created with the 'Seed Generation' tool, for example. The derived segments can be used, for example, for object based classification.\n
    Arguments
    ----------
    - SEEDS [`input grid`] : Seeds
    - FEATURES [`input grid list`] : Features
    - SEGMENTS [`output grid`] : Segments
    - SIMILARITY [`output grid`] : Similarity
    - TABLE [`output table`] : Seeds
    - NORMALIZE [`boolean`] : Normalize Features. Default: 0 Standardize the input features, i.e. rescale the input data (features) such that the mean equals 0 and the standard deviation equals 1. This is helpful when the input features have different scales, units or outliers.
    - NEIGHBOUR [`choice`] : Neighbourhood. Available Choices: [0] 4 (von Neumann) [1] 8 (Moore) Default: 0
    - METHOD [`choice`] : Method. Available Choices: [0] feature space and position [1] feature space Default: 0
    - SIG_1 [`floating point number`] : Variance in Feature Space. Minimum: 0.000000 Default: 1.000000
    - SIG_2 [`floating point number`] : Variance in Position Space. Minimum: 0.000000 Default: 1.000000
    - THRESHOLD [`floating point number`] : Similarity Threshold. Minimum: 0.000000 Default: 0.000000
    - REFRESH [`boolean`] : Refresh. Default: 0
    - LEAFSIZE [`integer number`] : Leaf Size (for Speed Optimisation). Minimum: 2 Default: 256

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('imagery_segmentation', '3', 'Seeded Region Growing')
    if Tool.is_Okay():
        Tool.Set_Input ('SEEDS', SEEDS)
        Tool.Set_Input ('FEATURES', FEATURES)
        Tool.Set_Output('SEGMENTS', SEGMENTS)
        Tool.Set_Output('SIMILARITY', SIMILARITY)
        Tool.Set_Output('TABLE', TABLE)
        Tool.Set_Option('NORMALIZE', NORMALIZE)
        Tool.Set_Option('NEIGHBOUR', NEIGHBOUR)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('SIG_1', SIG_1)
        Tool.Set_Option('SIG_2', SIG_2)
        Tool.Set_Option('THRESHOLD', THRESHOLD)
        Tool.Set_Option('REFRESH', REFRESH)
        Tool.Set_Option('LEAFSIZE', LEAFSIZE)
        return Tool.Execute(Verbose)
    return False

def run_tool_imagery_segmentation_3(SEEDS=None, FEATURES=None, SEGMENTS=None, SIMILARITY=None, TABLE=None, NORMALIZE=None, NEIGHBOUR=None, METHOD=None, SIG_1=None, SIG_2=None, THRESHOLD=None, REFRESH=None, LEAFSIZE=None, Verbose=2):
    '''
    Seeded Region Growing
    ----------
    [imagery_segmentation.3]\n
    The tool allows one to apply a seeded region growing algorithm to a stack of input features and thus to segmentize the data for object extraction. The required seed points can be created with the 'Seed Generation' tool, for example. The derived segments can be used, for example, for object based classification.\n
    Arguments
    ----------
    - SEEDS [`input grid`] : Seeds
    - FEATURES [`input grid list`] : Features
    - SEGMENTS [`output grid`] : Segments
    - SIMILARITY [`output grid`] : Similarity
    - TABLE [`output table`] : Seeds
    - NORMALIZE [`boolean`] : Normalize Features. Default: 0 Standardize the input features, i.e. rescale the input data (features) such that the mean equals 0 and the standard deviation equals 1. This is helpful when the input features have different scales, units or outliers.
    - NEIGHBOUR [`choice`] : Neighbourhood. Available Choices: [0] 4 (von Neumann) [1] 8 (Moore) Default: 0
    - METHOD [`choice`] : Method. Available Choices: [0] feature space and position [1] feature space Default: 0
    - SIG_1 [`floating point number`] : Variance in Feature Space. Minimum: 0.000000 Default: 1.000000
    - SIG_2 [`floating point number`] : Variance in Position Space. Minimum: 0.000000 Default: 1.000000
    - THRESHOLD [`floating point number`] : Similarity Threshold. Minimum: 0.000000 Default: 0.000000
    - REFRESH [`boolean`] : Refresh. Default: 0
    - LEAFSIZE [`integer number`] : Leaf Size (for Speed Optimisation). Minimum: 2 Default: 256

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('imagery_segmentation', '3', 'Seeded Region Growing')
    if Tool.is_Okay():
        Tool.Set_Input ('SEEDS', SEEDS)
        Tool.Set_Input ('FEATURES', FEATURES)
        Tool.Set_Output('SEGMENTS', SEGMENTS)
        Tool.Set_Output('SIMILARITY', SIMILARITY)
        Tool.Set_Output('TABLE', TABLE)
        Tool.Set_Option('NORMALIZE', NORMALIZE)
        Tool.Set_Option('NEIGHBOUR', NEIGHBOUR)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('SIG_1', SIG_1)
        Tool.Set_Option('SIG_2', SIG_2)
        Tool.Set_Option('THRESHOLD', THRESHOLD)
        Tool.Set_Option('REFRESH', REFRESH)
        Tool.Set_Option('LEAFSIZE', LEAFSIZE)
        return Tool.Execute(Verbose)
    return False

def Superpixel_Segmentation(FEATURES=None, POLYGONS=None, SUPERPIXELS=None, NORMALIZE=None, MAX_ITERATIONS=None, REGULARIZATION=None, SIZE=None, SIZE_MIN=None, SUPERPIXELS_DO=None, POSTPROCESSING=None, NCLUSTER=None, SPLIT_CLUSTERS=None, Verbose=2):
    '''
    Superpixel Segmentation
    ----------
    [imagery_segmentation.4]\n
    The Superpixel Segmentation tool implements the 'Simple Linear Iterative Clustering' (SLIC) algorithm, an image segmentation method described in Achanta et al. (2010).\n
    SLIC is a simple and efficient method to decompose an image in visually homogeneous regions. It is based on a spatially localized version of k-means clustering. Similar to mean shift or quick shift, each pixel is associated to a feature vector.\n
    This tool is follows the SLIC implementation created by Vedaldi and Fulkerson as part of the VLFeat library.\n
    Arguments
    ----------
    - FEATURES [`input grid list`] : Features
    - POLYGONS [`output shapes`] : Segments
    - SUPERPIXELS [`output grid list`] : Superpixels
    - NORMALIZE [`boolean`] : Normalize. Default: 0
    - MAX_ITERATIONS [`integer number`] : Maximum Iterations. Minimum: 1 Default: 100
    - REGULARIZATION [`floating point number`] : Regularization. Minimum: 0.000000 Default: 1.000000
    - SIZE [`integer number`] : Region Size. Minimum: 1 Default: 10 Starting 'cell size' of the superpixels given as number of cells.
    - SIZE_MIN [`integer number`] : Minimum Region Size. Minimum: 1 Default: 1 In postprocessing join segments, which cover less cells than specified here, to a larger neighbour segment.
    - SUPERPIXELS_DO [`boolean`] : Create Superpixel Grids. Default: 0
    - POSTPROCESSING [`choice`] : Post-Processing. Available Choices: [0] none [1] unsupervised classification Default: 0
    - NCLUSTER [`integer number`] : Number of Clusters. Minimum: 2 Default: 12
    - SPLIT_CLUSTERS [`boolean`] : Split Clusters. Default: 1

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('imagery_segmentation', '4', 'Superpixel Segmentation')
    if Tool.is_Okay():
        Tool.Set_Input ('FEATURES', FEATURES)
        Tool.Set_Output('POLYGONS', POLYGONS)
        Tool.Set_Output('SUPERPIXELS', SUPERPIXELS)
        Tool.Set_Option('NORMALIZE', NORMALIZE)
        Tool.Set_Option('MAX_ITERATIONS', MAX_ITERATIONS)
        Tool.Set_Option('REGULARIZATION', REGULARIZATION)
        Tool.Set_Option('SIZE', SIZE)
        Tool.Set_Option('SIZE_MIN', SIZE_MIN)
        Tool.Set_Option('SUPERPIXELS_DO', SUPERPIXELS_DO)
        Tool.Set_Option('POSTPROCESSING', POSTPROCESSING)
        Tool.Set_Option('NCLUSTER', NCLUSTER)
        Tool.Set_Option('SPLIT_CLUSTERS', SPLIT_CLUSTERS)
        return Tool.Execute(Verbose)
    return False

def run_tool_imagery_segmentation_4(FEATURES=None, POLYGONS=None, SUPERPIXELS=None, NORMALIZE=None, MAX_ITERATIONS=None, REGULARIZATION=None, SIZE=None, SIZE_MIN=None, SUPERPIXELS_DO=None, POSTPROCESSING=None, NCLUSTER=None, SPLIT_CLUSTERS=None, Verbose=2):
    '''
    Superpixel Segmentation
    ----------
    [imagery_segmentation.4]\n
    The Superpixel Segmentation tool implements the 'Simple Linear Iterative Clustering' (SLIC) algorithm, an image segmentation method described in Achanta et al. (2010).\n
    SLIC is a simple and efficient method to decompose an image in visually homogeneous regions. It is based on a spatially localized version of k-means clustering. Similar to mean shift or quick shift, each pixel is associated to a feature vector.\n
    This tool is follows the SLIC implementation created by Vedaldi and Fulkerson as part of the VLFeat library.\n
    Arguments
    ----------
    - FEATURES [`input grid list`] : Features
    - POLYGONS [`output shapes`] : Segments
    - SUPERPIXELS [`output grid list`] : Superpixels
    - NORMALIZE [`boolean`] : Normalize. Default: 0
    - MAX_ITERATIONS [`integer number`] : Maximum Iterations. Minimum: 1 Default: 100
    - REGULARIZATION [`floating point number`] : Regularization. Minimum: 0.000000 Default: 1.000000
    - SIZE [`integer number`] : Region Size. Minimum: 1 Default: 10 Starting 'cell size' of the superpixels given as number of cells.
    - SIZE_MIN [`integer number`] : Minimum Region Size. Minimum: 1 Default: 1 In postprocessing join segments, which cover less cells than specified here, to a larger neighbour segment.
    - SUPERPIXELS_DO [`boolean`] : Create Superpixel Grids. Default: 0
    - POSTPROCESSING [`choice`] : Post-Processing. Available Choices: [0] none [1] unsupervised classification Default: 0
    - NCLUSTER [`integer number`] : Number of Clusters. Minimum: 2 Default: 12
    - SPLIT_CLUSTERS [`boolean`] : Split Clusters. Default: 1

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('imagery_segmentation', '4', 'Superpixel Segmentation')
    if Tool.is_Okay():
        Tool.Set_Input ('FEATURES', FEATURES)
        Tool.Set_Output('POLYGONS', POLYGONS)
        Tool.Set_Output('SUPERPIXELS', SUPERPIXELS)
        Tool.Set_Option('NORMALIZE', NORMALIZE)
        Tool.Set_Option('MAX_ITERATIONS', MAX_ITERATIONS)
        Tool.Set_Option('REGULARIZATION', REGULARIZATION)
        Tool.Set_Option('SIZE', SIZE)
        Tool.Set_Option('SIZE_MIN', SIZE_MIN)
        Tool.Set_Option('SUPERPIXELS_DO', SUPERPIXELS_DO)
        Tool.Set_Option('POSTPROCESSING', POSTPROCESSING)
        Tool.Set_Option('NCLUSTER', NCLUSTER)
        Tool.Set_Option('SPLIT_CLUSTERS', SPLIT_CLUSTERS)
        return Tool.Execute(Verbose)
    return False

def Connected_Component_Labeling(INPUT=None, OUTPUT=None, NEIGHBOUR=None, Verbose=2):
    '''
    Connected Component Labeling
    ----------
    [imagery_segmentation.5]\n
    The tool allows one to label subsets of connected components with a unique identifier. Connected component labeling (CCL) is an operator which turns a binary image into a symbolic image in which the label assigned to each pixel is an integer uniquely identifying the connected component to which that pixel belongs (Shapiro 1996).\n
    The tool takes a grid as input and treats it as a binary image. The foreground is defined by all cell values greater than zero, the background by NoData cells and all cell values less than one. Connectivity can be determined by analysing either a 4-connected or a 8-connected neighborhood.\n
    Arguments
    ----------
    - INPUT [`input grid`] : Input. Grid to analyse.
    - OUTPUT [`output grid`] : Output. Output grid with labeled components.
    - NEIGHBOUR [`choice`] : Neighbourhood. Available Choices: [0] 4 (von Neumann) [1] 8 (Moore) Default: 1

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('imagery_segmentation', '5', 'Connected Component Labeling')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Output('OUTPUT', OUTPUT)
        Tool.Set_Option('NEIGHBOUR', NEIGHBOUR)
        return Tool.Execute(Verbose)
    return False

def run_tool_imagery_segmentation_5(INPUT=None, OUTPUT=None, NEIGHBOUR=None, Verbose=2):
    '''
    Connected Component Labeling
    ----------
    [imagery_segmentation.5]\n
    The tool allows one to label subsets of connected components with a unique identifier. Connected component labeling (CCL) is an operator which turns a binary image into a symbolic image in which the label assigned to each pixel is an integer uniquely identifying the connected component to which that pixel belongs (Shapiro 1996).\n
    The tool takes a grid as input and treats it as a binary image. The foreground is defined by all cell values greater than zero, the background by NoData cells and all cell values less than one. Connectivity can be determined by analysing either a 4-connected or a 8-connected neighborhood.\n
    Arguments
    ----------
    - INPUT [`input grid`] : Input. Grid to analyse.
    - OUTPUT [`output grid`] : Output. Output grid with labeled components.
    - NEIGHBOUR [`choice`] : Neighbourhood. Available Choices: [0] 4 (von Neumann) [1] 8 (Moore) Default: 1

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('imagery_segmentation', '5', 'Connected Component Labeling')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Output('OUTPUT', OUTPUT)
        Tool.Set_Option('NEIGHBOUR', NEIGHBOUR)
        return Tool.Execute(Verbose)
    return False

def Object_Based_Image_Segmentation(FEATURES=None, SAMPLES=None, OBJECTS=None, GRID_SYSTEM=None, NORMALIZE=None, SEEDS_BAND_WIDTH=None, RGA_NEIGHBOUR=None, RGA_METHOD=None, RGA_SIG_1=None, RGA_SIG_2=None, RGA_SIMILARITY=None, MAJORITY_RADIUS=None, CLASSIFICATION=None, SPLIT_POLYGONS=None, NCLUSTER=None, CLASSIFIER=None, Verbose=2):
    '''
    Object Based Image Segmentation
    ----------
    [imagery_segmentation.obia]\n
    This 'Object Based Image Segmentation' tool chain combines a number of tools for an easy derivation of geo-objects as polygons and is typically applied to satellite imagery. Segmentation is done using a 'Seeded Region Growing Algorithm'. Optionally the resulting polygons can be grouped by an unsupervised classification (k-means cluster analysis) or supervised classification (needs classified feature samples as additional input), both is performed on the basis of zonal feature grid statistics for each polygon object.\n
    \n
    Arguments
    ----------
    - FEATURES [`input grid list`] : Features
    - SAMPLES [`input table`] : Training Samples. Training samples for supervised classification. Provide a class identifier in the first field followed by sample data corresponding to the selected feature attributes
    - OBJECTS [`output shapes`] : Segments
    - GRID_SYSTEM [`grid system`] : Grid System
    - NORMALIZE [`boolean`] : Normalize. Default: 0
    - SEEDS_BAND_WIDTH [`floating point number`] : Band Width for Seed Point Generation. Default: 2.000000 Increase band width to get less seed points.
    - RGA_NEIGHBOUR [`choice`] : Neighbourhood. Available Choices: [0] 4 (Neumann) [1] 8 (Moore) Default: 0
    - RGA_METHOD [`choice`] : Distance. Available Choices: [0] feature space and position [1] feature space Default: 0
    - RGA_SIG_1 [`floating point number`] : Variance in Feature Space. Minimum: 0.000000 Default: 1.000000
    - RGA_SIG_2 [`floating point number`] : Variance in Position Space. Minimum: 0.000000 Default: 1.000000
    - RGA_SIMILARITY [`floating point number`] : Similarity Threshold. Minimum: 0.000000 Default: 0.000000
    - MAJORITY_RADIUS [`integer number`] : Generalization. Default: 1 Applies a majority filter with given search radius to the segments grid. Is skipped if set to zero.
    - CLASSIFICATION [`choice`] : Classification. Available Choices: [0] none [1] cluster analysis [2] supervised classification Default: 0
    - SPLIT_POLYGONS [`choice`] : Split Distinct Polygon Parts. Available Choices: [0] no [1] yes Default: 0
    - NCLUSTER [`integer number`] : Number of Clusters. Default: 10
    - CLASSIFIER [`choice`] : Method. Available Choices: [0] Binary Encoding [1] Parallelepiped [2] Minimum Distance [3] Mahalanobis Distance [4] Maximum Likelihood [5] Spectral Angle Mapping Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('imagery_segmentation', 'obia', 'Object Based Image Segmentation')
    if Tool.is_Okay():
        Tool.Set_Input ('FEATURES', FEATURES)
        Tool.Set_Input ('SAMPLES', SAMPLES)
        Tool.Set_Output('OBJECTS', OBJECTS)
        Tool.Set_Option('GRID_SYSTEM', GRID_SYSTEM)
        Tool.Set_Option('NORMALIZE', NORMALIZE)
        Tool.Set_Option('SEEDS_BAND_WIDTH', SEEDS_BAND_WIDTH)
        Tool.Set_Option('RGA_NEIGHBOUR', RGA_NEIGHBOUR)
        Tool.Set_Option('RGA_METHOD', RGA_METHOD)
        Tool.Set_Option('RGA_SIG_1', RGA_SIG_1)
        Tool.Set_Option('RGA_SIG_2', RGA_SIG_2)
        Tool.Set_Option('RGA_SIMILARITY', RGA_SIMILARITY)
        Tool.Set_Option('MAJORITY_RADIUS', MAJORITY_RADIUS)
        Tool.Set_Option('CLASSIFICATION', CLASSIFICATION)
        Tool.Set_Option('SPLIT_POLYGONS', SPLIT_POLYGONS)
        Tool.Set_Option('NCLUSTER', NCLUSTER)
        Tool.Set_Option('CLASSIFIER', CLASSIFIER)
        return Tool.Execute(Verbose)
    return False

def run_tool_imagery_segmentation_obia(FEATURES=None, SAMPLES=None, OBJECTS=None, GRID_SYSTEM=None, NORMALIZE=None, SEEDS_BAND_WIDTH=None, RGA_NEIGHBOUR=None, RGA_METHOD=None, RGA_SIG_1=None, RGA_SIG_2=None, RGA_SIMILARITY=None, MAJORITY_RADIUS=None, CLASSIFICATION=None, SPLIT_POLYGONS=None, NCLUSTER=None, CLASSIFIER=None, Verbose=2):
    '''
    Object Based Image Segmentation
    ----------
    [imagery_segmentation.obia]\n
    This 'Object Based Image Segmentation' tool chain combines a number of tools for an easy derivation of geo-objects as polygons and is typically applied to satellite imagery. Segmentation is done using a 'Seeded Region Growing Algorithm'. Optionally the resulting polygons can be grouped by an unsupervised classification (k-means cluster analysis) or supervised classification (needs classified feature samples as additional input), both is performed on the basis of zonal feature grid statistics for each polygon object.\n
    \n
    Arguments
    ----------
    - FEATURES [`input grid list`] : Features
    - SAMPLES [`input table`] : Training Samples. Training samples for supervised classification. Provide a class identifier in the first field followed by sample data corresponding to the selected feature attributes
    - OBJECTS [`output shapes`] : Segments
    - GRID_SYSTEM [`grid system`] : Grid System
    - NORMALIZE [`boolean`] : Normalize. Default: 0
    - SEEDS_BAND_WIDTH [`floating point number`] : Band Width for Seed Point Generation. Default: 2.000000 Increase band width to get less seed points.
    - RGA_NEIGHBOUR [`choice`] : Neighbourhood. Available Choices: [0] 4 (Neumann) [1] 8 (Moore) Default: 0
    - RGA_METHOD [`choice`] : Distance. Available Choices: [0] feature space and position [1] feature space Default: 0
    - RGA_SIG_1 [`floating point number`] : Variance in Feature Space. Minimum: 0.000000 Default: 1.000000
    - RGA_SIG_2 [`floating point number`] : Variance in Position Space. Minimum: 0.000000 Default: 1.000000
    - RGA_SIMILARITY [`floating point number`] : Similarity Threshold. Minimum: 0.000000 Default: 0.000000
    - MAJORITY_RADIUS [`integer number`] : Generalization. Default: 1 Applies a majority filter with given search radius to the segments grid. Is skipped if set to zero.
    - CLASSIFICATION [`choice`] : Classification. Available Choices: [0] none [1] cluster analysis [2] supervised classification Default: 0
    - SPLIT_POLYGONS [`choice`] : Split Distinct Polygon Parts. Available Choices: [0] no [1] yes Default: 0
    - NCLUSTER [`integer number`] : Number of Clusters. Default: 10
    - CLASSIFIER [`choice`] : Method. Available Choices: [0] Binary Encoding [1] Parallelepiped [2] Minimum Distance [3] Mahalanobis Distance [4] Maximum Likelihood [5] Spectral Angle Mapping Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('imagery_segmentation', 'obia', 'Object Based Image Segmentation')
    if Tool.is_Okay():
        Tool.Set_Input ('FEATURES', FEATURES)
        Tool.Set_Input ('SAMPLES', SAMPLES)
        Tool.Set_Output('OBJECTS', OBJECTS)
        Tool.Set_Option('GRID_SYSTEM', GRID_SYSTEM)
        Tool.Set_Option('NORMALIZE', NORMALIZE)
        Tool.Set_Option('SEEDS_BAND_WIDTH', SEEDS_BAND_WIDTH)
        Tool.Set_Option('RGA_NEIGHBOUR', RGA_NEIGHBOUR)
        Tool.Set_Option('RGA_METHOD', RGA_METHOD)
        Tool.Set_Option('RGA_SIG_1', RGA_SIG_1)
        Tool.Set_Option('RGA_SIG_2', RGA_SIG_2)
        Tool.Set_Option('RGA_SIMILARITY', RGA_SIMILARITY)
        Tool.Set_Option('MAJORITY_RADIUS', MAJORITY_RADIUS)
        Tool.Set_Option('CLASSIFICATION', CLASSIFICATION)
        Tool.Set_Option('SPLIT_POLYGONS', SPLIT_POLYGONS)
        Tool.Set_Option('NCLUSTER', NCLUSTER)
        Tool.Set_Option('CLASSIFIER', CLASSIFIER)
        return Tool.Execute(Verbose)
    return False

