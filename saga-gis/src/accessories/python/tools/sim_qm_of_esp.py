#! /usr/bin/env python

'''
Python Interface to SAGA Tools Library
----------
- Category : Simulation
- Name     : QM of ESP
- ID       : sim_qm_of_esp

Description
----------
Quantitative Modeling of Earth Surface Processes.
SAGA implementations following the examples from the text book:
Pelletier, J.D. (2008): Quantitative Modeling of Earth Surface Processes. Cambridge, 295p. [doi:10.1017/CBO9780511813849](https://doi.org/10.1017/CBO9780511813849)
'''

from PySAGA.helper import Tool_Wrapper

def Diffusive_Hillslope_Evolution_FTCS(DEM=None, MODEL=None, DIFF=None, UPDATE=None, KAPPA=None, DURATION=None, TIMESTEP=None, DTIME=None, NEIGHBOURS=None, Verbose=2):
    '''
    Diffusive Hillslope Evolution (FTCS)
    ----------
    [sim_qm_of_esp.0]\n
    Simulation of diffusive hillslope evolution using a Forward-Time-Centered-Space (FTCS) method.\n
    ____________\n
    This tool implements suggested code examples from the text book 'Quantitative Modeling of Earth Surface Processes' (Pelletier 2008) and serves as demonstration on code adaptions for the SAGA API. Note that this tool may be of limited use for operational purposes!\n
    Arguments
    ----------
    - DEM [`input grid`] : Elevation
    - MODEL [`output grid`] : Modelled Elevation
    - DIFF [`output grid`] : Elevation Difference
    - UPDATE [`boolean`] : Update. Default: 1
    - KAPPA [`floating point number`] : Diffusivity [m2 / kyr]. Minimum: 0.000000 Default: 1.000000
    - DURATION [`floating point number`] : Simulation Time [kyr]. Minimum: 0.000000 Default: 100.000000
    - TIMESTEP [`choice`] : Time Step. Available Choices: [0] user defined [1] automatically Default: 1
    - DTIME [`floating point number`] : Time Step [kyr]. Minimum: 0.000000 Default: 10.000000
    - NEIGHBOURS [`choice`] : Neighbourhood. Available Choices: [0] Neumann [1] Moore Default: 1

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('sim_qm_of_esp', '0', 'Diffusive Hillslope Evolution (FTCS)')
    if Tool.is_Okay():
        Tool.Set_Input ('DEM', DEM)
        Tool.Set_Output('MODEL', MODEL)
        Tool.Set_Output('DIFF', DIFF)
        Tool.Set_Option('UPDATE', UPDATE)
        Tool.Set_Option('KAPPA', KAPPA)
        Tool.Set_Option('DURATION', DURATION)
        Tool.Set_Option('TIMESTEP', TIMESTEP)
        Tool.Set_Option('DTIME', DTIME)
        Tool.Set_Option('NEIGHBOURS', NEIGHBOURS)
        return Tool.Execute(Verbose)
    return False

def run_tool_sim_qm_of_esp_0(DEM=None, MODEL=None, DIFF=None, UPDATE=None, KAPPA=None, DURATION=None, TIMESTEP=None, DTIME=None, NEIGHBOURS=None, Verbose=2):
    '''
    Diffusive Hillslope Evolution (FTCS)
    ----------
    [sim_qm_of_esp.0]\n
    Simulation of diffusive hillslope evolution using a Forward-Time-Centered-Space (FTCS) method.\n
    ____________\n
    This tool implements suggested code examples from the text book 'Quantitative Modeling of Earth Surface Processes' (Pelletier 2008) and serves as demonstration on code adaptions for the SAGA API. Note that this tool may be of limited use for operational purposes!\n
    Arguments
    ----------
    - DEM [`input grid`] : Elevation
    - MODEL [`output grid`] : Modelled Elevation
    - DIFF [`output grid`] : Elevation Difference
    - UPDATE [`boolean`] : Update. Default: 1
    - KAPPA [`floating point number`] : Diffusivity [m2 / kyr]. Minimum: 0.000000 Default: 1.000000
    - DURATION [`floating point number`] : Simulation Time [kyr]. Minimum: 0.000000 Default: 100.000000
    - TIMESTEP [`choice`] : Time Step. Available Choices: [0] user defined [1] automatically Default: 1
    - DTIME [`floating point number`] : Time Step [kyr]. Minimum: 0.000000 Default: 10.000000
    - NEIGHBOURS [`choice`] : Neighbourhood. Available Choices: [0] Neumann [1] Moore Default: 1

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('sim_qm_of_esp', '0', 'Diffusive Hillslope Evolution (FTCS)')
    if Tool.is_Okay():
        Tool.Set_Input ('DEM', DEM)
        Tool.Set_Output('MODEL', MODEL)
        Tool.Set_Output('DIFF', DIFF)
        Tool.Set_Option('UPDATE', UPDATE)
        Tool.Set_Option('KAPPA', KAPPA)
        Tool.Set_Option('DURATION', DURATION)
        Tool.Set_Option('TIMESTEP', TIMESTEP)
        Tool.Set_Option('DTIME', DTIME)
        Tool.Set_Option('NEIGHBOURS', NEIGHBOURS)
        return Tool.Execute(Verbose)
    return False

def Fill_Sinks_QM_of_ESP(DEM=None, FILLED=None, SINKS=None, DZFILL=None, Verbose=2):
    '''
    Fill Sinks (QM of ESP)
    ----------
    [sim_qm_of_esp.1]\n
    Filling in pits and flats in a DEM.\n
    ____________\n
    This tool implements suggested code examples from the text book 'Quantitative Modeling of Earth Surface Processes' (Pelletier 2008) and serves as demonstration on code adaptions for the SAGA API. Note that this tool may be of limited use for operational purposes!\n
    Arguments
    ----------
    - DEM [`input grid`] : DEM
    - FILLED [`output grid`] : DEM without Sinks
    - SINKS [`output grid`] : Sinks
    - DZFILL [`floating point number`] : Fill Increment. Minimum: 0.000000 Default: 0.010000

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('sim_qm_of_esp', '1', 'Fill Sinks (QM of ESP)')
    if Tool.is_Okay():
        Tool.Set_Input ('DEM', DEM)
        Tool.Set_Output('FILLED', FILLED)
        Tool.Set_Output('SINKS', SINKS)
        Tool.Set_Option('DZFILL', DZFILL)
        return Tool.Execute(Verbose)
    return False

def run_tool_sim_qm_of_esp_1(DEM=None, FILLED=None, SINKS=None, DZFILL=None, Verbose=2):
    '''
    Fill Sinks (QM of ESP)
    ----------
    [sim_qm_of_esp.1]\n
    Filling in pits and flats in a DEM.\n
    ____________\n
    This tool implements suggested code examples from the text book 'Quantitative Modeling of Earth Surface Processes' (Pelletier 2008) and serves as demonstration on code adaptions for the SAGA API. Note that this tool may be of limited use for operational purposes!\n
    Arguments
    ----------
    - DEM [`input grid`] : DEM
    - FILLED [`output grid`] : DEM without Sinks
    - SINKS [`output grid`] : Sinks
    - DZFILL [`floating point number`] : Fill Increment. Minimum: 0.000000 Default: 0.010000

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('sim_qm_of_esp', '1', 'Fill Sinks (QM of ESP)')
    if Tool.is_Okay():
        Tool.Set_Input ('DEM', DEM)
        Tool.Set_Output('FILLED', FILLED)
        Tool.Set_Output('SINKS', SINKS)
        Tool.Set_Option('DZFILL', DZFILL)
        return Tool.Execute(Verbose)
    return False

def Flow_Accumulation_QM_of_ESP(DEM=None, FLOW=None, PREPROC=None, DZFILL=None, Verbose=2):
    '''
    Flow Accumulation (QM of ESP)
    ----------
    [sim_qm_of_esp.2]\n
    Calculation of flow accumulation, aka upslope contributing area, with the multiple flow direction method after Freeman (1991).\n
    ____________\n
    This tool implements suggested code examples from the text book 'Quantitative Modeling of Earth Surface Processes' (Pelletier 2008) and serves as demonstration on code adaptions for the SAGA API. Note that this tool may be of limited use for operational purposes!\n
    Arguments
    ----------
    - DEM [`input grid`] : DEM
    - FLOW [`output grid`] : Contributing Area
    - PREPROC [`choice`] : Preprocessing. Available Choices: [0] none [1] fill sinks temporarily [2] fill sinks permanently Default: 1
    - DZFILL [`floating point number`] : Fill Increment. Minimum: 0.000000 Default: 0.010000

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('sim_qm_of_esp', '2', 'Flow Accumulation (QM of ESP)')
    if Tool.is_Okay():
        Tool.Set_Input ('DEM', DEM)
        Tool.Set_Output('FLOW', FLOW)
        Tool.Set_Option('PREPROC', PREPROC)
        Tool.Set_Option('DZFILL', DZFILL)
        return Tool.Execute(Verbose)
    return False

def run_tool_sim_qm_of_esp_2(DEM=None, FLOW=None, PREPROC=None, DZFILL=None, Verbose=2):
    '''
    Flow Accumulation (QM of ESP)
    ----------
    [sim_qm_of_esp.2]\n
    Calculation of flow accumulation, aka upslope contributing area, with the multiple flow direction method after Freeman (1991).\n
    ____________\n
    This tool implements suggested code examples from the text book 'Quantitative Modeling of Earth Surface Processes' (Pelletier 2008) and serves as demonstration on code adaptions for the SAGA API. Note that this tool may be of limited use for operational purposes!\n
    Arguments
    ----------
    - DEM [`input grid`] : DEM
    - FLOW [`output grid`] : Contributing Area
    - PREPROC [`choice`] : Preprocessing. Available Choices: [0] none [1] fill sinks temporarily [2] fill sinks permanently Default: 1
    - DZFILL [`floating point number`] : Fill Increment. Minimum: 0.000000 Default: 0.010000

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('sim_qm_of_esp', '2', 'Flow Accumulation (QM of ESP)')
    if Tool.is_Okay():
        Tool.Set_Input ('DEM', DEM)
        Tool.Set_Output('FLOW', FLOW)
        Tool.Set_Option('PREPROC', PREPROC)
        Tool.Set_Option('DZFILL', DZFILL)
        return Tool.Execute(Verbose)
    return False

def Successive_Flow_Routing(DEM=None, FLOW=None, ITERATIONS=None, RUNOFF=None, MANNING=None, Verbose=2):
    '''
    Successive Flow Routing
    ----------
    [sim_qm_of_esp.3]\n
    Calculation of flow accumulation, aka upslope contributing area, with the multiple flow direction method after Freeman (1991).\n
    ____________\n
    This tool implements suggested code examples from the text book 'Quantitative Modeling of Earth Surface Processes' (Pelletier 2008) and serves as demonstration on code adaptions for the SAGA API. Note that this tool may be of limited use for operational purposes!\n
    Arguments
    ----------
    - DEM [`input grid`] : DEM
    - FLOW [`output grid`] : Flow
    - ITERATIONS [`integer number`] : Iterations. Minimum: 1 Default: 100
    - RUNOFF [`floating point number`] : Runoff. Minimum: 0.000000 Default: 1.000000
    - MANNING [`floating point number`] : Manning's Roughness. Default: 0.200000

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('sim_qm_of_esp', '3', 'Successive Flow Routing')
    if Tool.is_Okay():
        Tool.Set_Input ('DEM', DEM)
        Tool.Set_Output('FLOW', FLOW)
        Tool.Set_Option('ITERATIONS', ITERATIONS)
        Tool.Set_Option('RUNOFF', RUNOFF)
        Tool.Set_Option('MANNING', MANNING)
        return Tool.Execute(Verbose)
    return False

def run_tool_sim_qm_of_esp_3(DEM=None, FLOW=None, ITERATIONS=None, RUNOFF=None, MANNING=None, Verbose=2):
    '''
    Successive Flow Routing
    ----------
    [sim_qm_of_esp.3]\n
    Calculation of flow accumulation, aka upslope contributing area, with the multiple flow direction method after Freeman (1991).\n
    ____________\n
    This tool implements suggested code examples from the text book 'Quantitative Modeling of Earth Surface Processes' (Pelletier 2008) and serves as demonstration on code adaptions for the SAGA API. Note that this tool may be of limited use for operational purposes!\n
    Arguments
    ----------
    - DEM [`input grid`] : DEM
    - FLOW [`output grid`] : Flow
    - ITERATIONS [`integer number`] : Iterations. Minimum: 1 Default: 100
    - RUNOFF [`floating point number`] : Runoff. Minimum: 0.000000 Default: 1.000000
    - MANNING [`floating point number`] : Manning's Roughness. Default: 0.200000

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('sim_qm_of_esp', '3', 'Successive Flow Routing')
    if Tool.is_Okay():
        Tool.Set_Input ('DEM', DEM)
        Tool.Set_Output('FLOW', FLOW)
        Tool.Set_Option('ITERATIONS', ITERATIONS)
        Tool.Set_Option('RUNOFF', RUNOFF)
        Tool.Set_Option('MANNING', MANNING)
        return Tool.Execute(Verbose)
    return False

def Diffusive_Hillslope_Evolution_ADI(DEM=None, CHANNELS=None, MODEL=None, DIFF=None, UPDATE=None, KAPPA=None, DURATION=None, TIMESTEP=None, DTIME=None, Verbose=2):
    '''
    Diffusive Hillslope Evolution (ADI)
    ----------
    [sim_qm_of_esp.4]\n
    Simulation of diffusive hillslope evolution using an Alternating-Direction-Implicit (ADI) method.\n
    ____________\n
    This tool implements suggested code examples from the text book 'Quantitative Modeling of Earth Surface Processes' (Pelletier 2008) and serves as demonstration on code adaptions for the SAGA API. Note that this tool may be of limited use for operational purposes!\n
    Arguments
    ----------
    - DEM [`input grid`] : Elevation
    - CHANNELS [`optional input grid`] : Channel Mask. use a zero value for hillslopes, any other value for channel cells.
    - MODEL [`output grid`] : Modelled Elevation
    - DIFF [`output grid`] : Elevation Difference
    - UPDATE [`boolean`] : Update. Default: 1
    - KAPPA [`floating point number`] : Diffusivity [m2 / kyr]. Minimum: 0.000000 Default: 10.000000
    - DURATION [`floating point number`] : Simulation Time [kyr]. Minimum: 0.000000 Default: 10000.000000
    - TIMESTEP [`choice`] : Time Step. Available Choices: [0] user defined [1] automatically Default: 0
    - DTIME [`floating point number`] : Time Step [kyr]. Minimum: 0.000000 Default: 1000.000000

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('sim_qm_of_esp', '4', 'Diffusive Hillslope Evolution (ADI)')
    if Tool.is_Okay():
        Tool.Set_Input ('DEM', DEM)
        Tool.Set_Input ('CHANNELS', CHANNELS)
        Tool.Set_Output('MODEL', MODEL)
        Tool.Set_Output('DIFF', DIFF)
        Tool.Set_Option('UPDATE', UPDATE)
        Tool.Set_Option('KAPPA', KAPPA)
        Tool.Set_Option('DURATION', DURATION)
        Tool.Set_Option('TIMESTEP', TIMESTEP)
        Tool.Set_Option('DTIME', DTIME)
        return Tool.Execute(Verbose)
    return False

def run_tool_sim_qm_of_esp_4(DEM=None, CHANNELS=None, MODEL=None, DIFF=None, UPDATE=None, KAPPA=None, DURATION=None, TIMESTEP=None, DTIME=None, Verbose=2):
    '''
    Diffusive Hillslope Evolution (ADI)
    ----------
    [sim_qm_of_esp.4]\n
    Simulation of diffusive hillslope evolution using an Alternating-Direction-Implicit (ADI) method.\n
    ____________\n
    This tool implements suggested code examples from the text book 'Quantitative Modeling of Earth Surface Processes' (Pelletier 2008) and serves as demonstration on code adaptions for the SAGA API. Note that this tool may be of limited use for operational purposes!\n
    Arguments
    ----------
    - DEM [`input grid`] : Elevation
    - CHANNELS [`optional input grid`] : Channel Mask. use a zero value for hillslopes, any other value for channel cells.
    - MODEL [`output grid`] : Modelled Elevation
    - DIFF [`output grid`] : Elevation Difference
    - UPDATE [`boolean`] : Update. Default: 1
    - KAPPA [`floating point number`] : Diffusivity [m2 / kyr]. Minimum: 0.000000 Default: 10.000000
    - DURATION [`floating point number`] : Simulation Time [kyr]. Minimum: 0.000000 Default: 10000.000000
    - TIMESTEP [`choice`] : Time Step. Available Choices: [0] user defined [1] automatically Default: 0
    - DTIME [`floating point number`] : Time Step [kyr]. Minimum: 0.000000 Default: 1000.000000

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('sim_qm_of_esp', '4', 'Diffusive Hillslope Evolution (ADI)')
    if Tool.is_Okay():
        Tool.Set_Input ('DEM', DEM)
        Tool.Set_Input ('CHANNELS', CHANNELS)
        Tool.Set_Output('MODEL', MODEL)
        Tool.Set_Output('DIFF', DIFF)
        Tool.Set_Option('UPDATE', UPDATE)
        Tool.Set_Option('KAPPA', KAPPA)
        Tool.Set_Option('DURATION', DURATION)
        Tool.Set_Option('TIMESTEP', TIMESTEP)
        Tool.Set_Option('DTIME', DTIME)
        return Tool.Execute(Verbose)
    return False

