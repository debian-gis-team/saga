#! /usr/bin/env python

'''
Python Interface to SAGA Tools Library
----------
- Category : Spatial and Geostatistics
- Name     : Grids
- ID       : statistics_grid

Description
----------
Tools for spatial and geostatistical analyses.
'''

from PySAGA.helper import Tool_Wrapper

def Fast_Representativeness(INPUT=None, RESULT=None, RESULT_LOD=None, SEEDS=None, LOD=None, Verbose=2):
    '''
    Fast Representativeness
    ----------
    [statistics_grid.0]\n
    A fast representativeness algorithm. Resulting seeds might be used with 'Fast Region Growing'.\n
    References:\n
    Boehner, J., Selige, T., Ringeler, A. (2006): Image segmentation using representativeness analysis and region growing. In: Boehner, J., McCloy, K.R., Strobl, J. [Eds.]:  SAGA â€“ Analysis and Modelling Applications. Goettinger Geographische Abhandlungen, Vol.115, [pdf](http://downloads.sourceforge.net/saga-gis/gga115_03.pdf)\n
    Arguments
    ----------
    - INPUT [`input grid`] : Input. Input for tool calculations.
    - RESULT [`output grid`] : Output. Output of tool calculations.
    - RESULT_LOD [`output grid`] : Output Lod. Output of tool calculations.
    - SEEDS [`output grid`] : Output Seeds. Output of tool calculations.
    - LOD [`floating point number`] : Level of Generalisation. Default: 16.000000

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('statistics_grid', '0', 'Fast Representativeness')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Output('RESULT', RESULT)
        Tool.Set_Output('RESULT_LOD', RESULT_LOD)
        Tool.Set_Output('SEEDS', SEEDS)
        Tool.Set_Option('LOD', LOD)
        return Tool.Execute(Verbose)
    return False

def run_tool_statistics_grid_0(INPUT=None, RESULT=None, RESULT_LOD=None, SEEDS=None, LOD=None, Verbose=2):
    '''
    Fast Representativeness
    ----------
    [statistics_grid.0]\n
    A fast representativeness algorithm. Resulting seeds might be used with 'Fast Region Growing'.\n
    References:\n
    Boehner, J., Selige, T., Ringeler, A. (2006): Image segmentation using representativeness analysis and region growing. In: Boehner, J., McCloy, K.R., Strobl, J. [Eds.]:  SAGA â€“ Analysis and Modelling Applications. Goettinger Geographische Abhandlungen, Vol.115, [pdf](http://downloads.sourceforge.net/saga-gis/gga115_03.pdf)\n
    Arguments
    ----------
    - INPUT [`input grid`] : Input. Input for tool calculations.
    - RESULT [`output grid`] : Output. Output of tool calculations.
    - RESULT_LOD [`output grid`] : Output Lod. Output of tool calculations.
    - SEEDS [`output grid`] : Output Seeds. Output of tool calculations.
    - LOD [`floating point number`] : Level of Generalisation. Default: 16.000000

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('statistics_grid', '0', 'Fast Representativeness')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Output('RESULT', RESULT)
        Tool.Set_Output('RESULT_LOD', RESULT_LOD)
        Tool.Set_Output('SEEDS', SEEDS)
        Tool.Set_Option('LOD', LOD)
        return Tool.Execute(Verbose)
    return False

def Focal_Statistics(GRID=None, MEAN=None, MIN=None, MAX=None, RANGE=None, STDDEV=None, VARIANCE=None, SUM=None, DIFF=None, DEVMEAN=None, PERCENT=None, MEDIAN=None, MINORITY=None, MAJORITY=None, BCENTER=None, KERNEL_TYPE=None, KERNEL_INNER=None, KERNEL_RADIUS=None, KERNEL_DIRECTION=None, KERNEL_TOLERANCE=None, DW_WEIGHTING=None, DW_IDW_POWER=None, DW_BANDWIDTH=None, Verbose=2):
    '''
    Focal Statistics
    ----------
    [statistics_grid.1]\n
    Based on its neighbourhood this tool calculates for each grid cell various statistical measures. According to Wilson & Gallant (2000) this tool was named 'Residual Analysis (Grid)' in earlier versions.\n
    Arguments
    ----------
    - GRID [`input grid`] : Grid
    - MEAN [`output grid`] : Mean Value
    - MIN [`output grid`] : Minimum Value
    - MAX [`output grid`] : Maximum Value
    - RANGE [`output grid`] : Value Range
    - STDDEV [`output grid`] : Standard Deviation
    - VARIANCE [`output grid`] : Variance
    - SUM [`output grid`] : Sum
    - DIFF [`output grid`] : Difference from Mean Value
    - DEVMEAN [`output grid`] : Deviation from Mean Value
    - PERCENT [`output grid`] : Percentile
    - MEDIAN [`output grid`] : Median
    - MINORITY [`output grid`] : Minority
    - MAJORITY [`output grid`] : Majority
    - BCENTER [`boolean`] : Include Center Cell. Default: 1
    - KERNEL_TYPE [`choice`] : Kernel Type. Available Choices: [0] Square [1] Circle [2] Annulus [3] Sector Default: 1 The kernel's shape.
    - KERNEL_INNER [`integer number`] : Inner Radius. Minimum: 0 Default: 0 cells
    - KERNEL_RADIUS [`integer number`] : Radius. Minimum: 0 Default: 2 cells
    - KERNEL_DIRECTION [`floating point number`] : Direction. Minimum: -360.000000 Maximum: 360.000000 Default: 0.000000 degree
    - KERNEL_TOLERANCE [`floating point number`] : Tolerance. Minimum: 0.000000 Maximum: 180.000000 Default: 5.000000 degree
    - DW_WEIGHTING [`choice`] : Weighting Function. Available Choices: [0] no distance weighting [1] inverse distance to a power [2] exponential [3] gaussian Default: 0
    - DW_IDW_POWER [`floating point number`] : Power. Minimum: 0.000000 Default: 2.000000
    - DW_BANDWIDTH [`floating point number`] : Bandwidth. Minimum: 0.000000 Default: 1.000000 Bandwidth for exponential and Gaussian weighting

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('statistics_grid', '1', 'Focal Statistics')
    if Tool.is_Okay():
        Tool.Set_Input ('GRID', GRID)
        Tool.Set_Output('MEAN', MEAN)
        Tool.Set_Output('MIN', MIN)
        Tool.Set_Output('MAX', MAX)
        Tool.Set_Output('RANGE', RANGE)
        Tool.Set_Output('STDDEV', STDDEV)
        Tool.Set_Output('VARIANCE', VARIANCE)
        Tool.Set_Output('SUM', SUM)
        Tool.Set_Output('DIFF', DIFF)
        Tool.Set_Output('DEVMEAN', DEVMEAN)
        Tool.Set_Output('PERCENT', PERCENT)
        Tool.Set_Output('MEDIAN', MEDIAN)
        Tool.Set_Output('MINORITY', MINORITY)
        Tool.Set_Output('MAJORITY', MAJORITY)
        Tool.Set_Option('BCENTER', BCENTER)
        Tool.Set_Option('KERNEL_TYPE', KERNEL_TYPE)
        Tool.Set_Option('KERNEL_INNER', KERNEL_INNER)
        Tool.Set_Option('KERNEL_RADIUS', KERNEL_RADIUS)
        Tool.Set_Option('KERNEL_DIRECTION', KERNEL_DIRECTION)
        Tool.Set_Option('KERNEL_TOLERANCE', KERNEL_TOLERANCE)
        Tool.Set_Option('DW_WEIGHTING', DW_WEIGHTING)
        Tool.Set_Option('DW_IDW_POWER', DW_IDW_POWER)
        Tool.Set_Option('DW_BANDWIDTH', DW_BANDWIDTH)
        return Tool.Execute(Verbose)
    return False

def run_tool_statistics_grid_1(GRID=None, MEAN=None, MIN=None, MAX=None, RANGE=None, STDDEV=None, VARIANCE=None, SUM=None, DIFF=None, DEVMEAN=None, PERCENT=None, MEDIAN=None, MINORITY=None, MAJORITY=None, BCENTER=None, KERNEL_TYPE=None, KERNEL_INNER=None, KERNEL_RADIUS=None, KERNEL_DIRECTION=None, KERNEL_TOLERANCE=None, DW_WEIGHTING=None, DW_IDW_POWER=None, DW_BANDWIDTH=None, Verbose=2):
    '''
    Focal Statistics
    ----------
    [statistics_grid.1]\n
    Based on its neighbourhood this tool calculates for each grid cell various statistical measures. According to Wilson & Gallant (2000) this tool was named 'Residual Analysis (Grid)' in earlier versions.\n
    Arguments
    ----------
    - GRID [`input grid`] : Grid
    - MEAN [`output grid`] : Mean Value
    - MIN [`output grid`] : Minimum Value
    - MAX [`output grid`] : Maximum Value
    - RANGE [`output grid`] : Value Range
    - STDDEV [`output grid`] : Standard Deviation
    - VARIANCE [`output grid`] : Variance
    - SUM [`output grid`] : Sum
    - DIFF [`output grid`] : Difference from Mean Value
    - DEVMEAN [`output grid`] : Deviation from Mean Value
    - PERCENT [`output grid`] : Percentile
    - MEDIAN [`output grid`] : Median
    - MINORITY [`output grid`] : Minority
    - MAJORITY [`output grid`] : Majority
    - BCENTER [`boolean`] : Include Center Cell. Default: 1
    - KERNEL_TYPE [`choice`] : Kernel Type. Available Choices: [0] Square [1] Circle [2] Annulus [3] Sector Default: 1 The kernel's shape.
    - KERNEL_INNER [`integer number`] : Inner Radius. Minimum: 0 Default: 0 cells
    - KERNEL_RADIUS [`integer number`] : Radius. Minimum: 0 Default: 2 cells
    - KERNEL_DIRECTION [`floating point number`] : Direction. Minimum: -360.000000 Maximum: 360.000000 Default: 0.000000 degree
    - KERNEL_TOLERANCE [`floating point number`] : Tolerance. Minimum: 0.000000 Maximum: 180.000000 Default: 5.000000 degree
    - DW_WEIGHTING [`choice`] : Weighting Function. Available Choices: [0] no distance weighting [1] inverse distance to a power [2] exponential [3] gaussian Default: 0
    - DW_IDW_POWER [`floating point number`] : Power. Minimum: 0.000000 Default: 2.000000
    - DW_BANDWIDTH [`floating point number`] : Bandwidth. Minimum: 0.000000 Default: 1.000000 Bandwidth for exponential and Gaussian weighting

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('statistics_grid', '1', 'Focal Statistics')
    if Tool.is_Okay():
        Tool.Set_Input ('GRID', GRID)
        Tool.Set_Output('MEAN', MEAN)
        Tool.Set_Output('MIN', MIN)
        Tool.Set_Output('MAX', MAX)
        Tool.Set_Output('RANGE', RANGE)
        Tool.Set_Output('STDDEV', STDDEV)
        Tool.Set_Output('VARIANCE', VARIANCE)
        Tool.Set_Output('SUM', SUM)
        Tool.Set_Output('DIFF', DIFF)
        Tool.Set_Output('DEVMEAN', DEVMEAN)
        Tool.Set_Output('PERCENT', PERCENT)
        Tool.Set_Output('MEDIAN', MEDIAN)
        Tool.Set_Output('MINORITY', MINORITY)
        Tool.Set_Output('MAJORITY', MAJORITY)
        Tool.Set_Option('BCENTER', BCENTER)
        Tool.Set_Option('KERNEL_TYPE', KERNEL_TYPE)
        Tool.Set_Option('KERNEL_INNER', KERNEL_INNER)
        Tool.Set_Option('KERNEL_RADIUS', KERNEL_RADIUS)
        Tool.Set_Option('KERNEL_DIRECTION', KERNEL_DIRECTION)
        Tool.Set_Option('KERNEL_TOLERANCE', KERNEL_TOLERANCE)
        Tool.Set_Option('DW_WEIGHTING', DW_WEIGHTING)
        Tool.Set_Option('DW_IDW_POWER', DW_IDW_POWER)
        Tool.Set_Option('DW_BANDWIDTH', DW_BANDWIDTH)
        return Tool.Execute(Verbose)
    return False

def Representativeness_Grid(INPUT=None, RESULT=None, RADIUS=None, EXPONENT=None, Verbose=2):
    '''
    Representativeness (Grid)
    ----------
    [statistics_grid.2]\n
    Representativeness - calculation of the variance within a given search radius.\n
    Arguments
    ----------
    - INPUT [`input grid`] : Grid
    - RESULT [`output grid`] : Representativeness
    - RADIUS [`integer number`] : Radius (Cells). Minimum: 1 Default: 10
    - EXPONENT [`floating point number`] : Exponent. Minimum: 0.000000 Default: 1.000000

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('statistics_grid', '2', 'Representativeness (Grid)')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Output('RESULT', RESULT)
        Tool.Set_Option('RADIUS', RADIUS)
        Tool.Set_Option('EXPONENT', EXPONENT)
        return Tool.Execute(Verbose)
    return False

def run_tool_statistics_grid_2(INPUT=None, RESULT=None, RADIUS=None, EXPONENT=None, Verbose=2):
    '''
    Representativeness (Grid)
    ----------
    [statistics_grid.2]\n
    Representativeness - calculation of the variance within a given search radius.\n
    Arguments
    ----------
    - INPUT [`input grid`] : Grid
    - RESULT [`output grid`] : Representativeness
    - RADIUS [`integer number`] : Radius (Cells). Minimum: 1 Default: 10
    - EXPONENT [`floating point number`] : Exponent. Minimum: 0.000000 Default: 1.000000

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('statistics_grid', '2', 'Representativeness (Grid)')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Output('RESULT', RESULT)
        Tool.Set_Option('RADIUS', RADIUS)
        Tool.Set_Option('EXPONENT', EXPONENT)
        return Tool.Execute(Verbose)
    return False

def Radius_of_Variance_Grid(INPUT=None, RESULT=None, STDDEV=None, RADIUS=None, OUTPUT=None, Verbose=2):
    '''
    Radius of Variance (Grid)
    ----------
    [statistics_grid.3]\n
    Find the radius within which the cell values exceed the given variance criterium. This tool is closely related to the representativeness calculation (variance within given search radius). For easier usage, the variance criterium is entered as standard deviation value.\n
    Arguments
    ----------
    - INPUT [`input grid`] : Grid
    - RESULT [`output grid`] : Radius
    - STDDEV [`floating point number`] : Standard Deviation. Minimum: 0.000000 Default: 1.000000
    - RADIUS [`integer number`] : Maximum Search Radius (cells). Minimum: 1 Default: 20
    - OUTPUT [`choice`] : Type of Output. Available Choices: [0] Cells [1] Map Units Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('statistics_grid', '3', 'Radius of Variance (Grid)')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Output('RESULT', RESULT)
        Tool.Set_Option('STDDEV', STDDEV)
        Tool.Set_Option('RADIUS', RADIUS)
        Tool.Set_Option('OUTPUT', OUTPUT)
        return Tool.Execute(Verbose)
    return False

def run_tool_statistics_grid_3(INPUT=None, RESULT=None, STDDEV=None, RADIUS=None, OUTPUT=None, Verbose=2):
    '''
    Radius of Variance (Grid)
    ----------
    [statistics_grid.3]\n
    Find the radius within which the cell values exceed the given variance criterium. This tool is closely related to the representativeness calculation (variance within given search radius). For easier usage, the variance criterium is entered as standard deviation value.\n
    Arguments
    ----------
    - INPUT [`input grid`] : Grid
    - RESULT [`output grid`] : Radius
    - STDDEV [`floating point number`] : Standard Deviation. Minimum: 0.000000 Default: 1.000000
    - RADIUS [`integer number`] : Maximum Search Radius (cells). Minimum: 1 Default: 20
    - OUTPUT [`choice`] : Type of Output. Available Choices: [0] Cells [1] Map Units Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('statistics_grid', '3', 'Radius of Variance (Grid)')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Output('RESULT', RESULT)
        Tool.Set_Option('STDDEV', STDDEV)
        Tool.Set_Option('RADIUS', RADIUS)
        Tool.Set_Option('OUTPUT', OUTPUT)
        return Tool.Execute(Verbose)
    return False

def Statistics_for_Grids(GRIDS=None, WEIGHTS=None, MEAN=None, MIN=None, MAX=None, RANGE=None, SUM=None, SUM2=None, VAR=None, STDDEV=None, STDDEVLO=None, STDDEVHI=None, PCTL=None, RESAMPLING=None, PCTL_VAL=None, Verbose=2):
    '''
    Statistics for Grids
    ----------
    [statistics_grid.4]\n
    Calculates statistical properties (arithmetic mean, minimum, maximum, variance, standard deviation) for each cell position for the values of the selected grids.\n
    Optionally you can supply a list of grids with weights. If you want to use weights, the number of value and weight grids have to be the same Value and weight grids are associated by their order in the lists. Weight grids have not to share the grid system of the value grids. In case that no weight can be obtained from a weight grid for value, that value will be ignored.\n
    Arguments
    ----------
    - GRIDS [`input grid list`] : Values
    - WEIGHTS [`optional input grid list`] : Weights
    - MEAN [`output grid`] : Arithmetic Mean
    - MIN [`output grid`] : Minimum
    - MAX [`output grid`] : Maximum
    - RANGE [`output grid`] : Range
    - SUM [`output grid`] : Sum
    - SUM2 [`output grid`] : Sum2
    - VAR [`output grid`] : Variance
    - STDDEV [`output grid`] : Standard Deviation
    - STDDEVLO [`output grid`] : Mean less Standard Deviation
    - STDDEVHI [`output grid`] : Mean plus Standard Deviation
    - PCTL [`output grid`] : Percentile
    - RESAMPLING [`choice`] : Resampling. Available Choices: [0] Nearest Neighbour [1] Bilinear Interpolation [2] Bicubic Spline Interpolation [3] B-Spline Interpolation Default: 0
    - PCTL_VAL [`floating point number`] : Percentile. Minimum: 0.000000 Maximum: 100.000000 Default: 50.000000

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('statistics_grid', '4', 'Statistics for Grids')
    if Tool.is_Okay():
        Tool.Set_Input ('GRIDS', GRIDS)
        Tool.Set_Input ('WEIGHTS', WEIGHTS)
        Tool.Set_Output('MEAN', MEAN)
        Tool.Set_Output('MIN', MIN)
        Tool.Set_Output('MAX', MAX)
        Tool.Set_Output('RANGE', RANGE)
        Tool.Set_Output('SUM', SUM)
        Tool.Set_Output('SUM2', SUM2)
        Tool.Set_Output('VAR', VAR)
        Tool.Set_Output('STDDEV', STDDEV)
        Tool.Set_Output('STDDEVLO', STDDEVLO)
        Tool.Set_Output('STDDEVHI', STDDEVHI)
        Tool.Set_Output('PCTL', PCTL)
        Tool.Set_Option('RESAMPLING', RESAMPLING)
        Tool.Set_Option('PCTL_VAL', PCTL_VAL)
        return Tool.Execute(Verbose)
    return False

def run_tool_statistics_grid_4(GRIDS=None, WEIGHTS=None, MEAN=None, MIN=None, MAX=None, RANGE=None, SUM=None, SUM2=None, VAR=None, STDDEV=None, STDDEVLO=None, STDDEVHI=None, PCTL=None, RESAMPLING=None, PCTL_VAL=None, Verbose=2):
    '''
    Statistics for Grids
    ----------
    [statistics_grid.4]\n
    Calculates statistical properties (arithmetic mean, minimum, maximum, variance, standard deviation) for each cell position for the values of the selected grids.\n
    Optionally you can supply a list of grids with weights. If you want to use weights, the number of value and weight grids have to be the same Value and weight grids are associated by their order in the lists. Weight grids have not to share the grid system of the value grids. In case that no weight can be obtained from a weight grid for value, that value will be ignored.\n
    Arguments
    ----------
    - GRIDS [`input grid list`] : Values
    - WEIGHTS [`optional input grid list`] : Weights
    - MEAN [`output grid`] : Arithmetic Mean
    - MIN [`output grid`] : Minimum
    - MAX [`output grid`] : Maximum
    - RANGE [`output grid`] : Range
    - SUM [`output grid`] : Sum
    - SUM2 [`output grid`] : Sum2
    - VAR [`output grid`] : Variance
    - STDDEV [`output grid`] : Standard Deviation
    - STDDEVLO [`output grid`] : Mean less Standard Deviation
    - STDDEVHI [`output grid`] : Mean plus Standard Deviation
    - PCTL [`output grid`] : Percentile
    - RESAMPLING [`choice`] : Resampling. Available Choices: [0] Nearest Neighbour [1] Bilinear Interpolation [2] Bicubic Spline Interpolation [3] B-Spline Interpolation Default: 0
    - PCTL_VAL [`floating point number`] : Percentile. Minimum: 0.000000 Maximum: 100.000000 Default: 50.000000

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('statistics_grid', '4', 'Statistics for Grids')
    if Tool.is_Okay():
        Tool.Set_Input ('GRIDS', GRIDS)
        Tool.Set_Input ('WEIGHTS', WEIGHTS)
        Tool.Set_Output('MEAN', MEAN)
        Tool.Set_Output('MIN', MIN)
        Tool.Set_Output('MAX', MAX)
        Tool.Set_Output('RANGE', RANGE)
        Tool.Set_Output('SUM', SUM)
        Tool.Set_Output('SUM2', SUM2)
        Tool.Set_Output('VAR', VAR)
        Tool.Set_Output('STDDEV', STDDEV)
        Tool.Set_Output('STDDEVLO', STDDEVLO)
        Tool.Set_Output('STDDEVHI', STDDEVHI)
        Tool.Set_Output('PCTL', PCTL)
        Tool.Set_Option('RESAMPLING', RESAMPLING)
        Tool.Set_Option('PCTL_VAL', PCTL_VAL)
        return Tool.Execute(Verbose)
    return False

def Zonal_Grid_Statistics(ZONES=None, CATLIST=None, STATLIST=None, ASPECT=None, UCU=None, OUTTAB=None, SHORTNAMES=None, Verbose=2):
    '''
    Zonal Grid Statistics
    ----------
    [statistics_grid.5]\n
    The tool allows one to calculate zonal statistics over a set of input grids and reports the statistics in a table.\n
    The tool first creates a contingency table of unique condition units (UCUs) on which the statistics are calculated. These UCUs are delineated from a zonal grid (e.g. sub catchments) and optional categorical grids (e.g. landcover, soil, ...). The derived UCUs can be output as a grid dataset.\n
    The tool then calculates descriptive statistics (n, min, max, mean, standard deviation and sum) for each UCU from (optional) grids with continuous data (e.g. slope). A grid storing aspect must be treated specially (circular statistics), please use the "Aspect" input parameter for such a grid.\n
    The tool has four different modes of operation:\n
    (1) only a zonal grid is used as input. This results in a simple contingency table with the number of grid cells in each zone.\n
    (2) a zonal grid and additional categorical grids are used as input. This results in a contingency table with the number of cells in each UCU.\n
    (3) a zonal grid and additional grids with continuous data are used as input. This results in a contingency table with the number of cells in each zone and the corresponding statistics for each continuous grid.\n
    (4) a zonal grid, additional categorical grids and additional grids with continuous data are used as input. This results in a contingency table with the number of cells in each UCU and the corresponding statistics for each continuous grid.\n
    Depending on the mode of operation, the output table contains information about the categorical combination of each UCU, the number of cells in each UCU and the statistics for each UCU. A typical output table may look like this:\n
    ============\n
    ID UCU	ID Zone	ID 1stCat	ID 2ndCat	Count UCU	N 1stCont	MIN 1stCont	MAX 1stCont	MEAN 1stCont	STDDEV 1stCont	SUM 1stCont\n
    1      	0      	2        	6        	6        	6        	708.5      	862.0      	734.5       	62.5          	4406.8\n
    2      	0      	3        	4        	106      	106      	829.1      	910.1      	848.8       	28.5          	89969.0\n
    ============\n
    Note: in the case you like to convert some one of the statistics back to a grid dataset, you can use the following procedure. Run the tool and output the UCU grid. Then edit the output table to match your needs (delete all fields besides the UCU identifier and the fields you like to create grids from). Then use both datasets in the "Grids from Classified Grid and Table" tool.\n
    Arguments
    ----------
    - ZONES [`input grid`] : Zone Grid. The grid defining the zones to analyze [NoData;categorical values].
    - CATLIST [`optional input grid list`] : Categorical Grids. Additional grids used to delineate the UCUs [NoData;categorical values].
    - STATLIST [`optional input grid list`] : Grids to Analyse. The grids with continuous data for which the statistics are calculated [NoData;continuous values].
    - ASPECT [`optional input grid`] : Aspect. A grid encoding the aspect of each cell [radians].
    - UCU [`output grid`] : UCUs. The derived unique condition areas (UCU).
    - OUTTAB [`output table`] : Zonal Statistics. The summary table with the statistics for each UCU.
    - SHORTNAMES [`boolean`] : Short Field Names. Default: 1 Shorten the field names to ten characters (as this is the limit for field names in shapefiles).

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('statistics_grid', '5', 'Zonal Grid Statistics')
    if Tool.is_Okay():
        Tool.Set_Input ('ZONES', ZONES)
        Tool.Set_Input ('CATLIST', CATLIST)
        Tool.Set_Input ('STATLIST', STATLIST)
        Tool.Set_Input ('ASPECT', ASPECT)
        Tool.Set_Output('UCU', UCU)
        Tool.Set_Output('OUTTAB', OUTTAB)
        Tool.Set_Option('SHORTNAMES', SHORTNAMES)
        return Tool.Execute(Verbose)
    return False

def run_tool_statistics_grid_5(ZONES=None, CATLIST=None, STATLIST=None, ASPECT=None, UCU=None, OUTTAB=None, SHORTNAMES=None, Verbose=2):
    '''
    Zonal Grid Statistics
    ----------
    [statistics_grid.5]\n
    The tool allows one to calculate zonal statistics over a set of input grids and reports the statistics in a table.\n
    The tool first creates a contingency table of unique condition units (UCUs) on which the statistics are calculated. These UCUs are delineated from a zonal grid (e.g. sub catchments) and optional categorical grids (e.g. landcover, soil, ...). The derived UCUs can be output as a grid dataset.\n
    The tool then calculates descriptive statistics (n, min, max, mean, standard deviation and sum) for each UCU from (optional) grids with continuous data (e.g. slope). A grid storing aspect must be treated specially (circular statistics), please use the "Aspect" input parameter for such a grid.\n
    The tool has four different modes of operation:\n
    (1) only a zonal grid is used as input. This results in a simple contingency table with the number of grid cells in each zone.\n
    (2) a zonal grid and additional categorical grids are used as input. This results in a contingency table with the number of cells in each UCU.\n
    (3) a zonal grid and additional grids with continuous data are used as input. This results in a contingency table with the number of cells in each zone and the corresponding statistics for each continuous grid.\n
    (4) a zonal grid, additional categorical grids and additional grids with continuous data are used as input. This results in a contingency table with the number of cells in each UCU and the corresponding statistics for each continuous grid.\n
    Depending on the mode of operation, the output table contains information about the categorical combination of each UCU, the number of cells in each UCU and the statistics for each UCU. A typical output table may look like this:\n
    ============\n
    ID UCU	ID Zone	ID 1stCat	ID 2ndCat	Count UCU	N 1stCont	MIN 1stCont	MAX 1stCont	MEAN 1stCont	STDDEV 1stCont	SUM 1stCont\n
    1      	0      	2        	6        	6        	6        	708.5      	862.0      	734.5       	62.5          	4406.8\n
    2      	0      	3        	4        	106      	106      	829.1      	910.1      	848.8       	28.5          	89969.0\n
    ============\n
    Note: in the case you like to convert some one of the statistics back to a grid dataset, you can use the following procedure. Run the tool and output the UCU grid. Then edit the output table to match your needs (delete all fields besides the UCU identifier and the fields you like to create grids from). Then use both datasets in the "Grids from Classified Grid and Table" tool.\n
    Arguments
    ----------
    - ZONES [`input grid`] : Zone Grid. The grid defining the zones to analyze [NoData;categorical values].
    - CATLIST [`optional input grid list`] : Categorical Grids. Additional grids used to delineate the UCUs [NoData;categorical values].
    - STATLIST [`optional input grid list`] : Grids to Analyse. The grids with continuous data for which the statistics are calculated [NoData;continuous values].
    - ASPECT [`optional input grid`] : Aspect. A grid encoding the aspect of each cell [radians].
    - UCU [`output grid`] : UCUs. The derived unique condition areas (UCU).
    - OUTTAB [`output table`] : Zonal Statistics. The summary table with the statistics for each UCU.
    - SHORTNAMES [`boolean`] : Short Field Names. Default: 1 Shorten the field names to ten characters (as this is the limit for field names in shapefiles).

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('statistics_grid', '5', 'Zonal Grid Statistics')
    if Tool.is_Okay():
        Tool.Set_Input ('ZONES', ZONES)
        Tool.Set_Input ('CATLIST', CATLIST)
        Tool.Set_Input ('STATLIST', STATLIST)
        Tool.Set_Input ('ASPECT', ASPECT)
        Tool.Set_Output('UCU', UCU)
        Tool.Set_Output('OUTTAB', OUTTAB)
        Tool.Set_Option('SHORTNAMES', SHORTNAMES)
        return Tool.Execute(Verbose)
    return False

def Directional_Statistics_for_Single_Grid(GRID=None, POINTS=None, MEAN=None, DIFMEAN=None, MIN=None, MAX=None, RANGE=None, VAR=None, STDDEV=None, STDDEVLO=None, STDDEVHI=None, DEVMEAN=None, PERCENT=None, POINTS_OUT=None, DIRECTION=None, TOLERANCE=None, MAXDISTANCE=None, DW_WEIGHTING=None, DW_IDW_POWER=None, DW_BANDWIDTH=None, Verbose=2):
    '''
    Directional Statistics for Single Grid
    ----------
    [statistics_grid.6]\n
    Calculates for each cell statistical properties (arithmetic mean, minimum, maximum, variance, standard deviation) of all cells lying in given direction based on the input grid.\n
    Arguments
    ----------
    - GRID [`input grid`] : Grid
    - POINTS [`optional input shapes`] : Points
    - MEAN [`output grid`] : Arithmetic Mean
    - DIFMEAN [`output grid`] : Difference from Arithmetic Mean
    - MIN [`output grid`] : Minimum
    - MAX [`output grid`] : Maximum
    - RANGE [`output grid`] : Range
    - VAR [`output grid`] : Variance
    - STDDEV [`output grid`] : Standard Deviation
    - STDDEVLO [`output grid`] : Mean less Standard Deviation
    - STDDEVHI [`output grid`] : Mean plus Standard Deviation
    - DEVMEAN [`output grid`] : Deviation from Arithmetic Mean
    - PERCENT [`output grid`] : Percentile
    - POINTS_OUT [`output shapes`] : Directional Statistics for Points
    - DIRECTION [`floating point number`] : Direction [Degree]. Default: 0.000000
    - TOLERANCE [`floating point number`] : Tolerance [Degree]. Minimum: 0.000000 Maximum: 45.000000 Default: 0.000000
    - MAXDISTANCE [`integer number`] : Maximum Distance [Cells]. Minimum: 0 Default: 0 Maximum distance parameter is ignored if set to zero (default).
    - DW_WEIGHTING [`choice`] : Weighting Function. Available Choices: [0] no distance weighting [1] inverse distance to a power [2] exponential [3] gaussian Default: 3
    - DW_IDW_POWER [`floating point number`] : Power. Minimum: 0.000000 Default: 2.000000
    - DW_BANDWIDTH [`floating point number`] : Bandwidth. Minimum: 0.000000 Default: 25.000000 Bandwidth specified as number of cells.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('statistics_grid', '6', 'Directional Statistics for Single Grid')
    if Tool.is_Okay():
        Tool.Set_Input ('GRID', GRID)
        Tool.Set_Input ('POINTS', POINTS)
        Tool.Set_Output('MEAN', MEAN)
        Tool.Set_Output('DIFMEAN', DIFMEAN)
        Tool.Set_Output('MIN', MIN)
        Tool.Set_Output('MAX', MAX)
        Tool.Set_Output('RANGE', RANGE)
        Tool.Set_Output('VAR', VAR)
        Tool.Set_Output('STDDEV', STDDEV)
        Tool.Set_Output('STDDEVLO', STDDEVLO)
        Tool.Set_Output('STDDEVHI', STDDEVHI)
        Tool.Set_Output('DEVMEAN', DEVMEAN)
        Tool.Set_Output('PERCENT', PERCENT)
        Tool.Set_Output('POINTS_OUT', POINTS_OUT)
        Tool.Set_Option('DIRECTION', DIRECTION)
        Tool.Set_Option('TOLERANCE', TOLERANCE)
        Tool.Set_Option('MAXDISTANCE', MAXDISTANCE)
        Tool.Set_Option('DW_WEIGHTING', DW_WEIGHTING)
        Tool.Set_Option('DW_IDW_POWER', DW_IDW_POWER)
        Tool.Set_Option('DW_BANDWIDTH', DW_BANDWIDTH)
        return Tool.Execute(Verbose)
    return False

def run_tool_statistics_grid_6(GRID=None, POINTS=None, MEAN=None, DIFMEAN=None, MIN=None, MAX=None, RANGE=None, VAR=None, STDDEV=None, STDDEVLO=None, STDDEVHI=None, DEVMEAN=None, PERCENT=None, POINTS_OUT=None, DIRECTION=None, TOLERANCE=None, MAXDISTANCE=None, DW_WEIGHTING=None, DW_IDW_POWER=None, DW_BANDWIDTH=None, Verbose=2):
    '''
    Directional Statistics for Single Grid
    ----------
    [statistics_grid.6]\n
    Calculates for each cell statistical properties (arithmetic mean, minimum, maximum, variance, standard deviation) of all cells lying in given direction based on the input grid.\n
    Arguments
    ----------
    - GRID [`input grid`] : Grid
    - POINTS [`optional input shapes`] : Points
    - MEAN [`output grid`] : Arithmetic Mean
    - DIFMEAN [`output grid`] : Difference from Arithmetic Mean
    - MIN [`output grid`] : Minimum
    - MAX [`output grid`] : Maximum
    - RANGE [`output grid`] : Range
    - VAR [`output grid`] : Variance
    - STDDEV [`output grid`] : Standard Deviation
    - STDDEVLO [`output grid`] : Mean less Standard Deviation
    - STDDEVHI [`output grid`] : Mean plus Standard Deviation
    - DEVMEAN [`output grid`] : Deviation from Arithmetic Mean
    - PERCENT [`output grid`] : Percentile
    - POINTS_OUT [`output shapes`] : Directional Statistics for Points
    - DIRECTION [`floating point number`] : Direction [Degree]. Default: 0.000000
    - TOLERANCE [`floating point number`] : Tolerance [Degree]. Minimum: 0.000000 Maximum: 45.000000 Default: 0.000000
    - MAXDISTANCE [`integer number`] : Maximum Distance [Cells]. Minimum: 0 Default: 0 Maximum distance parameter is ignored if set to zero (default).
    - DW_WEIGHTING [`choice`] : Weighting Function. Available Choices: [0] no distance weighting [1] inverse distance to a power [2] exponential [3] gaussian Default: 3
    - DW_IDW_POWER [`floating point number`] : Power. Minimum: 0.000000 Default: 2.000000
    - DW_BANDWIDTH [`floating point number`] : Bandwidth. Minimum: 0.000000 Default: 25.000000 Bandwidth specified as number of cells.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('statistics_grid', '6', 'Directional Statistics for Single Grid')
    if Tool.is_Okay():
        Tool.Set_Input ('GRID', GRID)
        Tool.Set_Input ('POINTS', POINTS)
        Tool.Set_Output('MEAN', MEAN)
        Tool.Set_Output('DIFMEAN', DIFMEAN)
        Tool.Set_Output('MIN', MIN)
        Tool.Set_Output('MAX', MAX)
        Tool.Set_Output('RANGE', RANGE)
        Tool.Set_Output('VAR', VAR)
        Tool.Set_Output('STDDEV', STDDEV)
        Tool.Set_Output('STDDEVLO', STDDEVLO)
        Tool.Set_Output('STDDEVHI', STDDEVHI)
        Tool.Set_Output('DEVMEAN', DEVMEAN)
        Tool.Set_Output('PERCENT', PERCENT)
        Tool.Set_Output('POINTS_OUT', POINTS_OUT)
        Tool.Set_Option('DIRECTION', DIRECTION)
        Tool.Set_Option('TOLERANCE', TOLERANCE)
        Tool.Set_Option('MAXDISTANCE', MAXDISTANCE)
        Tool.Set_Option('DW_WEIGHTING', DW_WEIGHTING)
        Tool.Set_Option('DW_IDW_POWER', DW_IDW_POWER)
        Tool.Set_Option('DW_BANDWIDTH', DW_BANDWIDTH)
        return Tool.Execute(Verbose)
    return False

def Global_Morans_I_for_Grids(GRID=None, RESULT=None, CONTIGUITY=None, DIALOG=None, Verbose=2):
    '''
    Global Moran's I for Grids
    ----------
    [statistics_grid.7]\n
    Global spatial autocorrelation for grids calculated as Moran's I.\n
    Arguments
    ----------
    - GRID [`input grid`] : Grid
    - RESULT [`output table`] : Result
    - CONTIGUITY [`choice`] : Case of contiguity. Available Choices: [0] Rook [1] Queen Default: 1 Choose case: Rook's case contiguity compares only cell which share an edge. Queen's case contiguity compares also cells which share just corners.
    - DIALOG [`boolean`] : Show Result in Dialog. Default: 1

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('statistics_grid', '7', 'Global Moran\'s I for Grids')
    if Tool.is_Okay():
        Tool.Set_Input ('GRID', GRID)
        Tool.Set_Output('RESULT', RESULT)
        Tool.Set_Option('CONTIGUITY', CONTIGUITY)
        Tool.Set_Option('DIALOG', DIALOG)
        return Tool.Execute(Verbose)
    return False

def run_tool_statistics_grid_7(GRID=None, RESULT=None, CONTIGUITY=None, DIALOG=None, Verbose=2):
    '''
    Global Moran's I for Grids
    ----------
    [statistics_grid.7]\n
    Global spatial autocorrelation for grids calculated as Moran's I.\n
    Arguments
    ----------
    - GRID [`input grid`] : Grid
    - RESULT [`output table`] : Result
    - CONTIGUITY [`choice`] : Case of contiguity. Available Choices: [0] Rook [1] Queen Default: 1 Choose case: Rook's case contiguity compares only cell which share an edge. Queen's case contiguity compares also cells which share just corners.
    - DIALOG [`boolean`] : Show Result in Dialog. Default: 1

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('statistics_grid', '7', 'Global Moran\'s I for Grids')
    if Tool.is_Okay():
        Tool.Set_Input ('GRID', GRID)
        Tool.Set_Output('RESULT', RESULT)
        Tool.Set_Option('CONTIGUITY', CONTIGUITY)
        Tool.Set_Option('DIALOG', DIALOG)
        return Tool.Execute(Verbose)
    return False

def Principal_Component_Analysis(GRIDS=None, EIGEN_INPUT=None, PCA=None, EIGEN=None, METHOD=None, COMPONENTS=None, OVERWRITE=None, Verbose=2):
    '''
    Principal Component Analysis
    ----------
    [statistics_grid.8]\n
    Principal Component Analysis (PCA) for grids. PCA implementation is based on F.Murtagh's code as provided by the StatLib web site.\n
    Arguments
    ----------
    - GRIDS [`input grid list`] : Grids
    - EIGEN_INPUT [`optional input table`] : Eigen Vectors. Use Eigen vectors from this table instead of calculating these from the input grids.
    - PCA [`output grid list`] : Principal Components
    - EIGEN [`output table`] : Eigen Vectors. Store calculated Eigen vectors to this table, e.g. for later use with forward or inverse PCA.
    - METHOD [`choice`] : Method. Available Choices: [0] correlation matrix [1] variance-covariance matrix [2] sums-of-squares-and-cross-products matrix Default: 1
    - COMPONENTS [`integer number`] : Number of Components. Minimum: 0 Default: 3 number of first components in the output; set to zero to get all
    - OVERWRITE [`boolean`] : Overwrite Previous Results. Default: 1

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('statistics_grid', '8', 'Principal Component Analysis')
    if Tool.is_Okay():
        Tool.Set_Input ('GRIDS', GRIDS)
        Tool.Set_Input ('EIGEN_INPUT', EIGEN_INPUT)
        Tool.Set_Output('PCA', PCA)
        Tool.Set_Output('EIGEN', EIGEN)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('COMPONENTS', COMPONENTS)
        Tool.Set_Option('OVERWRITE', OVERWRITE)
        return Tool.Execute(Verbose)
    return False

def run_tool_statistics_grid_8(GRIDS=None, EIGEN_INPUT=None, PCA=None, EIGEN=None, METHOD=None, COMPONENTS=None, OVERWRITE=None, Verbose=2):
    '''
    Principal Component Analysis
    ----------
    [statistics_grid.8]\n
    Principal Component Analysis (PCA) for grids. PCA implementation is based on F.Murtagh's code as provided by the StatLib web site.\n
    Arguments
    ----------
    - GRIDS [`input grid list`] : Grids
    - EIGEN_INPUT [`optional input table`] : Eigen Vectors. Use Eigen vectors from this table instead of calculating these from the input grids.
    - PCA [`output grid list`] : Principal Components
    - EIGEN [`output table`] : Eigen Vectors. Store calculated Eigen vectors to this table, e.g. for later use with forward or inverse PCA.
    - METHOD [`choice`] : Method. Available Choices: [0] correlation matrix [1] variance-covariance matrix [2] sums-of-squares-and-cross-products matrix Default: 1
    - COMPONENTS [`integer number`] : Number of Components. Minimum: 0 Default: 3 number of first components in the output; set to zero to get all
    - OVERWRITE [`boolean`] : Overwrite Previous Results. Default: 1

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('statistics_grid', '8', 'Principal Component Analysis')
    if Tool.is_Okay():
        Tool.Set_Input ('GRIDS', GRIDS)
        Tool.Set_Input ('EIGEN_INPUT', EIGEN_INPUT)
        Tool.Set_Output('PCA', PCA)
        Tool.Set_Output('EIGEN', EIGEN)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('COMPONENTS', COMPONENTS)
        Tool.Set_Option('OVERWRITE', OVERWRITE)
        return Tool.Execute(Verbose)
    return False

def MultiBand_Variation(BANDS=None, MEAN=None, STDDEV=None, DIFF=None, RADIUS=None, DW_WEIGHTING=None, DW_IDW_POWER=None, DW_BANDWIDTH=None, Verbose=2):
    '''
    Multi-Band Variation
    ----------
    [statistics_grid.9]\n
    Calculates for each cell the spectral variation based on feature space distances to the centroid for all cells in specified neighbourhood. The average distance has been used for Spectral Variation Hypothesis (SVH).\n
    Arguments
    ----------
    - BANDS [`input grid list`] : Bands
    - MEAN [`output grid`] : Mean Distance
    - STDDEV [`output grid`] : Standard Deviation
    - DIFF [`output grid`] : Distance
    - RADIUS [`integer number`] : Radius [Cells]. Minimum: 1 Default: 1
    - DW_WEIGHTING [`choice`] : Weighting Function. Available Choices: [0] no distance weighting [1] inverse distance to a power [2] exponential [3] gaussian Default: 0
    - DW_IDW_POWER [`floating point number`] : Power. Minimum: 0.000000 Default: 2.000000
    - DW_BANDWIDTH [`floating point number`] : Bandwidth. Minimum: 0.000000 Default: 1.000000 Bandwidth for exponential and Gaussian weighting

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('statistics_grid', '9', 'Multi-Band Variation')
    if Tool.is_Okay():
        Tool.Set_Input ('BANDS', BANDS)
        Tool.Set_Output('MEAN', MEAN)
        Tool.Set_Output('STDDEV', STDDEV)
        Tool.Set_Output('DIFF', DIFF)
        Tool.Set_Option('RADIUS', RADIUS)
        Tool.Set_Option('DW_WEIGHTING', DW_WEIGHTING)
        Tool.Set_Option('DW_IDW_POWER', DW_IDW_POWER)
        Tool.Set_Option('DW_BANDWIDTH', DW_BANDWIDTH)
        return Tool.Execute(Verbose)
    return False

def run_tool_statistics_grid_9(BANDS=None, MEAN=None, STDDEV=None, DIFF=None, RADIUS=None, DW_WEIGHTING=None, DW_IDW_POWER=None, DW_BANDWIDTH=None, Verbose=2):
    '''
    Multi-Band Variation
    ----------
    [statistics_grid.9]\n
    Calculates for each cell the spectral variation based on feature space distances to the centroid for all cells in specified neighbourhood. The average distance has been used for Spectral Variation Hypothesis (SVH).\n
    Arguments
    ----------
    - BANDS [`input grid list`] : Bands
    - MEAN [`output grid`] : Mean Distance
    - STDDEV [`output grid`] : Standard Deviation
    - DIFF [`output grid`] : Distance
    - RADIUS [`integer number`] : Radius [Cells]. Minimum: 1 Default: 1
    - DW_WEIGHTING [`choice`] : Weighting Function. Available Choices: [0] no distance weighting [1] inverse distance to a power [2] exponential [3] gaussian Default: 0
    - DW_IDW_POWER [`floating point number`] : Power. Minimum: 0.000000 Default: 2.000000
    - DW_BANDWIDTH [`floating point number`] : Bandwidth. Minimum: 0.000000 Default: 1.000000 Bandwidth for exponential and Gaussian weighting

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('statistics_grid', '9', 'Multi-Band Variation')
    if Tool.is_Okay():
        Tool.Set_Input ('BANDS', BANDS)
        Tool.Set_Output('MEAN', MEAN)
        Tool.Set_Output('STDDEV', STDDEV)
        Tool.Set_Output('DIFF', DIFF)
        Tool.Set_Option('RADIUS', RADIUS)
        Tool.Set_Option('DW_WEIGHTING', DW_WEIGHTING)
        Tool.Set_Option('DW_IDW_POWER', DW_IDW_POWER)
        Tool.Set_Option('DW_BANDWIDTH', DW_BANDWIDTH)
        return Tool.Execute(Verbose)
    return False

def Inverse_Principal_Components_Rotation(PCA=None, EIGEN=None, GRIDS=None, Verbose=2):
    '''
    Inverse Principal Components Rotation
    ----------
    [statistics_grid.10]\n
    Inverse principal components rotation for grids.\n
    Arguments
    ----------
    - PCA [`input grid list`] : Principal Components
    - EIGEN [`input table`] : Eigen Vectors
    - GRIDS [`output grid list`] : Grids

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('statistics_grid', '10', 'Inverse Principal Components Rotation')
    if Tool.is_Okay():
        Tool.Set_Input ('PCA', PCA)
        Tool.Set_Input ('EIGEN', EIGEN)
        Tool.Set_Output('GRIDS', GRIDS)
        return Tool.Execute(Verbose)
    return False

def run_tool_statistics_grid_10(PCA=None, EIGEN=None, GRIDS=None, Verbose=2):
    '''
    Inverse Principal Components Rotation
    ----------
    [statistics_grid.10]\n
    Inverse principal components rotation for grids.\n
    Arguments
    ----------
    - PCA [`input grid list`] : Principal Components
    - EIGEN [`input table`] : Eigen Vectors
    - GRIDS [`output grid list`] : Grids

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('statistics_grid', '10', 'Inverse Principal Components Rotation')
    if Tool.is_Okay():
        Tool.Set_Input ('PCA', PCA)
        Tool.Set_Input ('EIGEN', EIGEN)
        Tool.Set_Output('GRIDS', GRIDS)
        return Tool.Execute(Verbose)
    return False

def Longitudinal_Grid_Statistics(GRID=None, STATS=None, Verbose=2):
    '''
    Longitudinal Grid Statistics
    ----------
    [statistics_grid.11]\n
    Longitudinal Grid Statistics\n
    Arguments
    ----------
    - GRID [`input grid`] : Grid
    - STATS [`output table`] : Latitudinal Statistics

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('statistics_grid', '11', 'Longitudinal Grid Statistics')
    if Tool.is_Okay():
        Tool.Set_Input ('GRID', GRID)
        Tool.Set_Output('STATS', STATS)
        return Tool.Execute(Verbose)
    return False

def run_tool_statistics_grid_11(GRID=None, STATS=None, Verbose=2):
    '''
    Longitudinal Grid Statistics
    ----------
    [statistics_grid.11]\n
    Longitudinal Grid Statistics\n
    Arguments
    ----------
    - GRID [`input grid`] : Grid
    - STATS [`output table`] : Latitudinal Statistics

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('statistics_grid', '11', 'Longitudinal Grid Statistics')
    if Tool.is_Okay():
        Tool.Set_Input ('GRID', GRID)
        Tool.Set_Output('STATS', STATS)
        return Tool.Execute(Verbose)
    return False

def Meridional_Grid_Statistics(GRID=None, STATS=None, Verbose=2):
    '''
    Meridional Grid Statistics
    ----------
    [statistics_grid.12]\n
    Meridional Grid Statistics\n
    Arguments
    ----------
    - GRID [`input grid`] : Grid
    - STATS [`output table`] : Meridional Statistics

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('statistics_grid', '12', 'Meridional Grid Statistics')
    if Tool.is_Okay():
        Tool.Set_Input ('GRID', GRID)
        Tool.Set_Output('STATS', STATS)
        return Tool.Execute(Verbose)
    return False

def run_tool_statistics_grid_12(GRID=None, STATS=None, Verbose=2):
    '''
    Meridional Grid Statistics
    ----------
    [statistics_grid.12]\n
    Meridional Grid Statistics\n
    Arguments
    ----------
    - GRID [`input grid`] : Grid
    - STATS [`output table`] : Meridional Statistics

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('statistics_grid', '12', 'Meridional Grid Statistics')
    if Tool.is_Okay():
        Tool.Set_Input ('GRID', GRID)
        Tool.Set_Output('STATS', STATS)
        return Tool.Execute(Verbose)
    return False

def Save_Grid_Statistics_to_Table(GRIDS=None, STATS=None, DATA_CELLS=None, NODATA_CELLS=None, CELLSIZE=None, MEAN=None, MIN=None, MAX=None, RANGE=None, SUM=None, SUM2=None, VAR=None, STDDEV=None, STDDEVLO=None, STDDEVHI=None, PCTL_VAL=None, PCTL_HST=None, SAMPLES=None, Verbose=2):
    '''
    Save Grid Statistics to Table
    ----------
    [statistics_grid.13]\n
    Calculates statistical properties (arithmetic mean, minimum, maximum, variance, standard deviation) for each of the given grids and saves it to a table.\n
    Arguments
    ----------
    - GRIDS [`input grid list`] : Grids
    - STATS [`output table`] : Statistics for Grids
    - DATA_CELLS [`boolean`] : Number of Data Cells. Default: 0
    - NODATA_CELLS [`boolean`] : Number of No-Data Cells. Default: 0
    - CELLSIZE [`boolean`] : Cellsize. Default: 0
    - MEAN [`boolean`] : Arithmetic Mean. Default: 1
    - MIN [`boolean`] : Minimum. Default: 1
    - MAX [`boolean`] : Maximum. Default: 1
    - RANGE [`boolean`] : Range. Default: 0
    - SUM [`boolean`] : Sum. Default: 0
    - SUM2 [`boolean`] : Sum of Squares. Default: 0
    - VAR [`boolean`] : Variance. Default: 1
    - STDDEV [`boolean`] : Standard Deviation. Default: 1
    - STDDEVLO [`boolean`] : Mean less Standard Deviation. Default: 0
    - STDDEVHI [`boolean`] : Mean plus Standard Deviation. Default: 0
    - PCTL_VAL [`text`] : Percentiles. Default: 5; 25; 50; 75; 95 Separate the desired percentiles by semicolon
    - PCTL_HST [`boolean`] : From Histogram. Default: 1
    - SAMPLES [`floating point number`] : Sample Size. Minimum: 0.000000 Maximum: 100.000000 Default: 0.000000 Minimum sample size [percent] used to calculate statistics. Ignored, if set to zero.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('statistics_grid', '13', 'Save Grid Statistics to Table')
    if Tool.is_Okay():
        Tool.Set_Input ('GRIDS', GRIDS)
        Tool.Set_Output('STATS', STATS)
        Tool.Set_Option('DATA_CELLS', DATA_CELLS)
        Tool.Set_Option('NODATA_CELLS', NODATA_CELLS)
        Tool.Set_Option('CELLSIZE', CELLSIZE)
        Tool.Set_Option('MEAN', MEAN)
        Tool.Set_Option('MIN', MIN)
        Tool.Set_Option('MAX', MAX)
        Tool.Set_Option('RANGE', RANGE)
        Tool.Set_Option('SUM', SUM)
        Tool.Set_Option('SUM2', SUM2)
        Tool.Set_Option('VAR', VAR)
        Tool.Set_Option('STDDEV', STDDEV)
        Tool.Set_Option('STDDEVLO', STDDEVLO)
        Tool.Set_Option('STDDEVHI', STDDEVHI)
        Tool.Set_Option('PCTL_VAL', PCTL_VAL)
        Tool.Set_Option('PCTL_HST', PCTL_HST)
        Tool.Set_Option('SAMPLES', SAMPLES)
        return Tool.Execute(Verbose)
    return False

def run_tool_statistics_grid_13(GRIDS=None, STATS=None, DATA_CELLS=None, NODATA_CELLS=None, CELLSIZE=None, MEAN=None, MIN=None, MAX=None, RANGE=None, SUM=None, SUM2=None, VAR=None, STDDEV=None, STDDEVLO=None, STDDEVHI=None, PCTL_VAL=None, PCTL_HST=None, SAMPLES=None, Verbose=2):
    '''
    Save Grid Statistics to Table
    ----------
    [statistics_grid.13]\n
    Calculates statistical properties (arithmetic mean, minimum, maximum, variance, standard deviation) for each of the given grids and saves it to a table.\n
    Arguments
    ----------
    - GRIDS [`input grid list`] : Grids
    - STATS [`output table`] : Statistics for Grids
    - DATA_CELLS [`boolean`] : Number of Data Cells. Default: 0
    - NODATA_CELLS [`boolean`] : Number of No-Data Cells. Default: 0
    - CELLSIZE [`boolean`] : Cellsize. Default: 0
    - MEAN [`boolean`] : Arithmetic Mean. Default: 1
    - MIN [`boolean`] : Minimum. Default: 1
    - MAX [`boolean`] : Maximum. Default: 1
    - RANGE [`boolean`] : Range. Default: 0
    - SUM [`boolean`] : Sum. Default: 0
    - SUM2 [`boolean`] : Sum of Squares. Default: 0
    - VAR [`boolean`] : Variance. Default: 1
    - STDDEV [`boolean`] : Standard Deviation. Default: 1
    - STDDEVLO [`boolean`] : Mean less Standard Deviation. Default: 0
    - STDDEVHI [`boolean`] : Mean plus Standard Deviation. Default: 0
    - PCTL_VAL [`text`] : Percentiles. Default: 5; 25; 50; 75; 95 Separate the desired percentiles by semicolon
    - PCTL_HST [`boolean`] : From Histogram. Default: 1
    - SAMPLES [`floating point number`] : Sample Size. Minimum: 0.000000 Maximum: 100.000000 Default: 0.000000 Minimum sample size [percent] used to calculate statistics. Ignored, if set to zero.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('statistics_grid', '13', 'Save Grid Statistics to Table')
    if Tool.is_Okay():
        Tool.Set_Input ('GRIDS', GRIDS)
        Tool.Set_Output('STATS', STATS)
        Tool.Set_Option('DATA_CELLS', DATA_CELLS)
        Tool.Set_Option('NODATA_CELLS', NODATA_CELLS)
        Tool.Set_Option('CELLSIZE', CELLSIZE)
        Tool.Set_Option('MEAN', MEAN)
        Tool.Set_Option('MIN', MIN)
        Tool.Set_Option('MAX', MAX)
        Tool.Set_Option('RANGE', RANGE)
        Tool.Set_Option('SUM', SUM)
        Tool.Set_Option('SUM2', SUM2)
        Tool.Set_Option('VAR', VAR)
        Tool.Set_Option('STDDEV', STDDEV)
        Tool.Set_Option('STDDEVLO', STDDEVLO)
        Tool.Set_Option('STDDEVHI', STDDEVHI)
        Tool.Set_Option('PCTL_VAL', PCTL_VAL)
        Tool.Set_Option('PCTL_HST', PCTL_HST)
        Tool.Set_Option('SAMPLES', SAMPLES)
        return Tool.Execute(Verbose)
    return False

def Categorical_Coincidence(GRIDS=None, CATEGORIES=None, COINCIDENCE=None, MAJ_COUNT=None, MAJ_VALUE=None, RADIUS=None, Verbose=2):
    '''
    Categorical Coincidence
    ----------
    [statistics_grid.14]\n
    Calculates for each cell the categorical coincidence, which can be useful to compare different classifications.\n
    Arguments
    ----------
    - GRIDS [`input grid list`] : Grids
    - CATEGORIES [`output grid`] : Number of Categories
    - COINCIDENCE [`output grid`] : Coincidence
    - MAJ_COUNT [`output grid`] : Dominance of Majority
    - MAJ_VALUE [`output grid`] : Value of Majority
    - RADIUS [`integer number`] : Radius [Cells]. Minimum: 0 Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('statistics_grid', '14', 'Categorical Coincidence')
    if Tool.is_Okay():
        Tool.Set_Input ('GRIDS', GRIDS)
        Tool.Set_Output('CATEGORIES', CATEGORIES)
        Tool.Set_Output('COINCIDENCE', COINCIDENCE)
        Tool.Set_Output('MAJ_COUNT', MAJ_COUNT)
        Tool.Set_Output('MAJ_VALUE', MAJ_VALUE)
        Tool.Set_Option('RADIUS', RADIUS)
        return Tool.Execute(Verbose)
    return False

def run_tool_statistics_grid_14(GRIDS=None, CATEGORIES=None, COINCIDENCE=None, MAJ_COUNT=None, MAJ_VALUE=None, RADIUS=None, Verbose=2):
    '''
    Categorical Coincidence
    ----------
    [statistics_grid.14]\n
    Calculates for each cell the categorical coincidence, which can be useful to compare different classifications.\n
    Arguments
    ----------
    - GRIDS [`input grid list`] : Grids
    - CATEGORIES [`output grid`] : Number of Categories
    - COINCIDENCE [`output grid`] : Coincidence
    - MAJ_COUNT [`output grid`] : Dominance of Majority
    - MAJ_VALUE [`output grid`] : Value of Majority
    - RADIUS [`integer number`] : Radius [Cells]. Minimum: 0 Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('statistics_grid', '14', 'Categorical Coincidence')
    if Tool.is_Okay():
        Tool.Set_Input ('GRIDS', GRIDS)
        Tool.Set_Output('CATEGORIES', CATEGORIES)
        Tool.Set_Output('COINCIDENCE', COINCIDENCE)
        Tool.Set_Output('MAJ_COUNT', MAJ_COUNT)
        Tool.Set_Output('MAJ_VALUE', MAJ_VALUE)
        Tool.Set_Option('RADIUS', RADIUS)
        return Tool.Execute(Verbose)
    return False

def Focal_PCA_on_a_Grid(GRID=None, BASE=None, PCA=None, EIGEN=None, COMPONENTS=None, BASE_OUT=None, OVERWRITE=None, KERNEL_TYPE=None, KERNEL_RADIUS=None, METHOD=None, Verbose=2):
    '''
    Focal PCA on a Grid
    ----------
    [statistics_grid.15]\n
    This tool uses the difference in cell values of a center cell and its neighbours (as specified by the kernel) as features for a Principal Component Analysis (PCA). This method has been used by Thomas and Herzfeld (2004) to parameterize the topography for a subsequent regionalization of climate variables with the principal components as predictors in a regression model.\n
    Arguments
    ----------
    - GRID [`input grid`] : Grid
    - BASE [`output grid list`] : Base Topographies
    - PCA [`output grid list`] : Principal Components
    - EIGEN [`output table`] : Eigen Vectors
    - COMPONENTS [`integer number`] : Number of Components. Minimum: 1 Default: 7 number of first components in the output; set to zero to get all
    - BASE_OUT [`boolean`] : Output of Base Topographies. Default: 0
    - OVERWRITE [`boolean`] : Overwrite Previous Results. Default: 1
    - KERNEL_TYPE [`choice`] : Kernel Type. Available Choices: [0] Square [1] Circle Default: 1
    - KERNEL_RADIUS [`integer number`] : Kernel Radius. Minimum: 1 Default: 2 Kernel radius in cells.
    - METHOD [`choice`] : Method. Available Choices: [0] correlation matrix [1] variance-covariance matrix [2] sums-of-squares-and-cross-products matrix Default: 1

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('statistics_grid', '15', 'Focal PCA on a Grid')
    if Tool.is_Okay():
        Tool.Set_Input ('GRID', GRID)
        Tool.Set_Output('BASE', BASE)
        Tool.Set_Output('PCA', PCA)
        Tool.Set_Output('EIGEN', EIGEN)
        Tool.Set_Option('COMPONENTS', COMPONENTS)
        Tool.Set_Option('BASE_OUT', BASE_OUT)
        Tool.Set_Option('OVERWRITE', OVERWRITE)
        Tool.Set_Option('KERNEL_TYPE', KERNEL_TYPE)
        Tool.Set_Option('KERNEL_RADIUS', KERNEL_RADIUS)
        Tool.Set_Option('METHOD', METHOD)
        return Tool.Execute(Verbose)
    return False

def run_tool_statistics_grid_15(GRID=None, BASE=None, PCA=None, EIGEN=None, COMPONENTS=None, BASE_OUT=None, OVERWRITE=None, KERNEL_TYPE=None, KERNEL_RADIUS=None, METHOD=None, Verbose=2):
    '''
    Focal PCA on a Grid
    ----------
    [statistics_grid.15]\n
    This tool uses the difference in cell values of a center cell and its neighbours (as specified by the kernel) as features for a Principal Component Analysis (PCA). This method has been used by Thomas and Herzfeld (2004) to parameterize the topography for a subsequent regionalization of climate variables with the principal components as predictors in a regression model.\n
    Arguments
    ----------
    - GRID [`input grid`] : Grid
    - BASE [`output grid list`] : Base Topographies
    - PCA [`output grid list`] : Principal Components
    - EIGEN [`output table`] : Eigen Vectors
    - COMPONENTS [`integer number`] : Number of Components. Minimum: 1 Default: 7 number of first components in the output; set to zero to get all
    - BASE_OUT [`boolean`] : Output of Base Topographies. Default: 0
    - OVERWRITE [`boolean`] : Overwrite Previous Results. Default: 1
    - KERNEL_TYPE [`choice`] : Kernel Type. Available Choices: [0] Square [1] Circle Default: 1
    - KERNEL_RADIUS [`integer number`] : Kernel Radius. Minimum: 1 Default: 2 Kernel radius in cells.
    - METHOD [`choice`] : Method. Available Choices: [0] correlation matrix [1] variance-covariance matrix [2] sums-of-squares-and-cross-products matrix Default: 1

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('statistics_grid', '15', 'Focal PCA on a Grid')
    if Tool.is_Okay():
        Tool.Set_Input ('GRID', GRID)
        Tool.Set_Output('BASE', BASE)
        Tool.Set_Output('PCA', PCA)
        Tool.Set_Output('EIGEN', EIGEN)
        Tool.Set_Option('COMPONENTS', COMPONENTS)
        Tool.Set_Option('BASE_OUT', BASE_OUT)
        Tool.Set_Option('OVERWRITE', OVERWRITE)
        Tool.Set_Option('KERNEL_TYPE', KERNEL_TYPE)
        Tool.Set_Option('KERNEL_RADIUS', KERNEL_RADIUS)
        Tool.Set_Option('METHOD', METHOD)
        return Tool.Execute(Verbose)
    return False

def Statistics_for_Grids_from_Files(COUNT=None, MIN=None, MAX=None, RANGE=None, SUM=None, SUM2=None, MEAN=None, VAR=None, STDDEV=None, HISTOGRAM=None, QUANTILES=None, GRID_SYSTEM=None, FILES=None, HCLASSES=None, HRANGE=None, CUMULATIVE=None, QUANTVALS=None, Verbose=2):
    '''
    Statistics for Grids from Files
    ----------
    [statistics_grid.16]\n
    Calculates statistical properties (arithmetic mean, minimum, maximum, variance, standard deviation) for each cell position for the values of the selected grids. This tool works file based to allow the processing of a large number of grids.\n
    Arguments
    ----------
    - COUNT [`output grid`] : Number of Values
    - MIN [`output grid`] : Minimum
    - MAX [`output grid`] : Maximum
    - RANGE [`output grid`] : Range
    - SUM [`output grid`] : Sum
    - SUM2 [`output grid`] : Sum of Squares
    - MEAN [`output grid`] : Arithmetic Mean
    - VAR [`output grid`] : Variance
    - STDDEV [`output grid`] : Standard Deviation
    - HISTOGRAM [`output grid collection`] : Histogram
    - QUANTILES [`output grid list`] : Percentiles
    - GRID_SYSTEM [`grid system`] : Grid System
    - FILES [`file path`] : Grid Files
    - HCLASSES [`integer number`] : Number of Classes. Minimum: 2 Default: 20
    - HRANGE [`choice`] : Range. Available Choices: [0] overall [1] cell Default: 1 Minimum and maximum values used to define the histogram classes.
    - CUMULATIVE [`boolean`] : Cumulative. Default: 0
    - QUANTVALS [`text`] : Percentiles. Default: 5; 25; 50; 75; 95 Separate the desired percentiles by semicolon

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('statistics_grid', '16', 'Statistics for Grids from Files')
    if Tool.is_Okay():
        Tool.Set_Output('COUNT', COUNT)
        Tool.Set_Output('MIN', MIN)
        Tool.Set_Output('MAX', MAX)
        Tool.Set_Output('RANGE', RANGE)
        Tool.Set_Output('SUM', SUM)
        Tool.Set_Output('SUM2', SUM2)
        Tool.Set_Output('MEAN', MEAN)
        Tool.Set_Output('VAR', VAR)
        Tool.Set_Output('STDDEV', STDDEV)
        Tool.Set_Output('HISTOGRAM', HISTOGRAM)
        Tool.Set_Output('QUANTILES', QUANTILES)
        Tool.Set_Option('GRID_SYSTEM', GRID_SYSTEM)
        Tool.Set_Option('FILES', FILES)
        Tool.Set_Option('HCLASSES', HCLASSES)
        Tool.Set_Option('HRANGE', HRANGE)
        Tool.Set_Option('CUMULATIVE', CUMULATIVE)
        Tool.Set_Option('QUANTVALS', QUANTVALS)
        return Tool.Execute(Verbose)
    return False

def run_tool_statistics_grid_16(COUNT=None, MIN=None, MAX=None, RANGE=None, SUM=None, SUM2=None, MEAN=None, VAR=None, STDDEV=None, HISTOGRAM=None, QUANTILES=None, GRID_SYSTEM=None, FILES=None, HCLASSES=None, HRANGE=None, CUMULATIVE=None, QUANTVALS=None, Verbose=2):
    '''
    Statistics for Grids from Files
    ----------
    [statistics_grid.16]\n
    Calculates statistical properties (arithmetic mean, minimum, maximum, variance, standard deviation) for each cell position for the values of the selected grids. This tool works file based to allow the processing of a large number of grids.\n
    Arguments
    ----------
    - COUNT [`output grid`] : Number of Values
    - MIN [`output grid`] : Minimum
    - MAX [`output grid`] : Maximum
    - RANGE [`output grid`] : Range
    - SUM [`output grid`] : Sum
    - SUM2 [`output grid`] : Sum of Squares
    - MEAN [`output grid`] : Arithmetic Mean
    - VAR [`output grid`] : Variance
    - STDDEV [`output grid`] : Standard Deviation
    - HISTOGRAM [`output grid collection`] : Histogram
    - QUANTILES [`output grid list`] : Percentiles
    - GRID_SYSTEM [`grid system`] : Grid System
    - FILES [`file path`] : Grid Files
    - HCLASSES [`integer number`] : Number of Classes. Minimum: 2 Default: 20
    - HRANGE [`choice`] : Range. Available Choices: [0] overall [1] cell Default: 1 Minimum and maximum values used to define the histogram classes.
    - CUMULATIVE [`boolean`] : Cumulative. Default: 0
    - QUANTVALS [`text`] : Percentiles. Default: 5; 25; 50; 75; 95 Separate the desired percentiles by semicolon

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('statistics_grid', '16', 'Statistics for Grids from Files')
    if Tool.is_Okay():
        Tool.Set_Output('COUNT', COUNT)
        Tool.Set_Output('MIN', MIN)
        Tool.Set_Output('MAX', MAX)
        Tool.Set_Output('RANGE', RANGE)
        Tool.Set_Output('SUM', SUM)
        Tool.Set_Output('SUM2', SUM2)
        Tool.Set_Output('MEAN', MEAN)
        Tool.Set_Output('VAR', VAR)
        Tool.Set_Output('STDDEV', STDDEV)
        Tool.Set_Output('HISTOGRAM', HISTOGRAM)
        Tool.Set_Output('QUANTILES', QUANTILES)
        Tool.Set_Option('GRID_SYSTEM', GRID_SYSTEM)
        Tool.Set_Option('FILES', FILES)
        Tool.Set_Option('HCLASSES', HCLASSES)
        Tool.Set_Option('HRANGE', HRANGE)
        Tool.Set_Option('CUMULATIVE', CUMULATIVE)
        Tool.Set_Option('QUANTVALS', QUANTVALS)
        return Tool.Execute(Verbose)
    return False

def Build_Statistics_for_Grids(GRIDS=None, COUNT=None, SUM=None, SUM2=None, MIN=None, MAX=None, HISTOGRAM=None, HCLASSES=None, HMIN=None, HMAX=None, Verbose=2):
    '''
    Build Statistics for Grids
    ----------
    [statistics_grid.17]\n
    This tool collects cell-wise basic statistical information from the given input grids. The collected statistics can be used as input for the 'Evaluate Statistics for Grids' tool. You can use this tool with the 'Reset' flag set to false (not available in command line mode) or the 'Add Statistics for Grids' tool to successively add statistical information from further grids by subsequent calls. These three tools (build, add, evaluate) have been designed to inspect a large number of grids that could otherwise not be evaluated simultaneously due to memory restrictions.\n
    Arguments
    ----------
    - GRIDS [`input grid list`] : Grids
    - COUNT [`output grid`] : Number of Values
    - SUM [`output grid`] : Sum
    - SUM2 [`output grid`] : Sum of Squares
    - MIN [`output grid`] : Minimum
    - MAX [`output grid`] : Maximum
    - HISTOGRAM [`output grid collection`] : Histogram
    - HCLASSES [`integer number`] : Number of Classes. Minimum: 2 Default: 20
    - HMIN [`floating point number`] : Minimum. Default: 0.000000
    - HMAX [`floating point number`] : Maximum. Default: 0.000000

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('statistics_grid', '17', 'Build Statistics for Grids')
    if Tool.is_Okay():
        Tool.Set_Input ('GRIDS', GRIDS)
        Tool.Set_Output('COUNT', COUNT)
        Tool.Set_Output('SUM', SUM)
        Tool.Set_Output('SUM2', SUM2)
        Tool.Set_Output('MIN', MIN)
        Tool.Set_Output('MAX', MAX)
        Tool.Set_Output('HISTOGRAM', HISTOGRAM)
        Tool.Set_Option('HCLASSES', HCLASSES)
        Tool.Set_Option('HMIN', HMIN)
        Tool.Set_Option('HMAX', HMAX)
        return Tool.Execute(Verbose)
    return False

def run_tool_statistics_grid_17(GRIDS=None, COUNT=None, SUM=None, SUM2=None, MIN=None, MAX=None, HISTOGRAM=None, HCLASSES=None, HMIN=None, HMAX=None, Verbose=2):
    '''
    Build Statistics for Grids
    ----------
    [statistics_grid.17]\n
    This tool collects cell-wise basic statistical information from the given input grids. The collected statistics can be used as input for the 'Evaluate Statistics for Grids' tool. You can use this tool with the 'Reset' flag set to false (not available in command line mode) or the 'Add Statistics for Grids' tool to successively add statistical information from further grids by subsequent calls. These three tools (build, add, evaluate) have been designed to inspect a large number of grids that could otherwise not be evaluated simultaneously due to memory restrictions.\n
    Arguments
    ----------
    - GRIDS [`input grid list`] : Grids
    - COUNT [`output grid`] : Number of Values
    - SUM [`output grid`] : Sum
    - SUM2 [`output grid`] : Sum of Squares
    - MIN [`output grid`] : Minimum
    - MAX [`output grid`] : Maximum
    - HISTOGRAM [`output grid collection`] : Histogram
    - HCLASSES [`integer number`] : Number of Classes. Minimum: 2 Default: 20
    - HMIN [`floating point number`] : Minimum. Default: 0.000000
    - HMAX [`floating point number`] : Maximum. Default: 0.000000

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('statistics_grid', '17', 'Build Statistics for Grids')
    if Tool.is_Okay():
        Tool.Set_Input ('GRIDS', GRIDS)
        Tool.Set_Output('COUNT', COUNT)
        Tool.Set_Output('SUM', SUM)
        Tool.Set_Output('SUM2', SUM2)
        Tool.Set_Output('MIN', MIN)
        Tool.Set_Output('MAX', MAX)
        Tool.Set_Output('HISTOGRAM', HISTOGRAM)
        Tool.Set_Option('HCLASSES', HCLASSES)
        Tool.Set_Option('HMIN', HMIN)
        Tool.Set_Option('HMAX', HMAX)
        return Tool.Execute(Verbose)
    return False

def Evaluate_Statistics_for_Grids(COUNT=None, SUM=None, SUM2=None, MIN=None, MAX=None, HISTOGRAM=None, RANGE=None, MEAN=None, VAR=None, STDDEV=None, QUANTILES=None, QUANTVALS=None, Verbose=2):
    '''
    Evaluate Statistics for Grids
    ----------
    [statistics_grid.18]\n
    Calculates statistical properties (arithmetic mean, range, variance, standard deviation, percentiles) on a cell-wise base. This tool takes input about basic statistical information as it can be collected with the 'Build/Add Statistics for Grids' tools. These three tools (build, add, evaluate) have been designed to inspect a large number of grids that could otherwise not be evaluated simultaneously due to memory restrictions.\n
    Arguments
    ----------
    - COUNT [`optional input grid`] : Number of Values
    - SUM [`optional input grid`] : Sum
    - SUM2 [`optional input grid`] : Sum of Squares
    - MIN [`optional input grid`] : Minimum
    - MAX [`optional input grid`] : Maximum
    - HISTOGRAM [`optional input grid collection`] : Histogram
    - RANGE [`output grid`] : Range
    - MEAN [`output grid`] : Arithmetic Mean
    - VAR [`output grid`] : Variance
    - STDDEV [`output grid`] : Standard Deviation
    - QUANTILES [`output grid list`] : Percentiles
    - QUANTVALS [`text`] : Percentiles. Default: 5; 25; 50; 75; 95 Separate the desired percentiles by semicolon

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('statistics_grid', '18', 'Evaluate Statistics for Grids')
    if Tool.is_Okay():
        Tool.Set_Input ('COUNT', COUNT)
        Tool.Set_Input ('SUM', SUM)
        Tool.Set_Input ('SUM2', SUM2)
        Tool.Set_Input ('MIN', MIN)
        Tool.Set_Input ('MAX', MAX)
        Tool.Set_Input ('HISTOGRAM', HISTOGRAM)
        Tool.Set_Output('RANGE', RANGE)
        Tool.Set_Output('MEAN', MEAN)
        Tool.Set_Output('VAR', VAR)
        Tool.Set_Output('STDDEV', STDDEV)
        Tool.Set_Output('QUANTILES', QUANTILES)
        Tool.Set_Option('QUANTVALS', QUANTVALS)
        return Tool.Execute(Verbose)
    return False

def run_tool_statistics_grid_18(COUNT=None, SUM=None, SUM2=None, MIN=None, MAX=None, HISTOGRAM=None, RANGE=None, MEAN=None, VAR=None, STDDEV=None, QUANTILES=None, QUANTVALS=None, Verbose=2):
    '''
    Evaluate Statistics for Grids
    ----------
    [statistics_grid.18]\n
    Calculates statistical properties (arithmetic mean, range, variance, standard deviation, percentiles) on a cell-wise base. This tool takes input about basic statistical information as it can be collected with the 'Build/Add Statistics for Grids' tools. These three tools (build, add, evaluate) have been designed to inspect a large number of grids that could otherwise not be evaluated simultaneously due to memory restrictions.\n
    Arguments
    ----------
    - COUNT [`optional input grid`] : Number of Values
    - SUM [`optional input grid`] : Sum
    - SUM2 [`optional input grid`] : Sum of Squares
    - MIN [`optional input grid`] : Minimum
    - MAX [`optional input grid`] : Maximum
    - HISTOGRAM [`optional input grid collection`] : Histogram
    - RANGE [`output grid`] : Range
    - MEAN [`output grid`] : Arithmetic Mean
    - VAR [`output grid`] : Variance
    - STDDEV [`output grid`] : Standard Deviation
    - QUANTILES [`output grid list`] : Percentiles
    - QUANTVALS [`text`] : Percentiles. Default: 5; 25; 50; 75; 95 Separate the desired percentiles by semicolon

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('statistics_grid', '18', 'Evaluate Statistics for Grids')
    if Tool.is_Okay():
        Tool.Set_Input ('COUNT', COUNT)
        Tool.Set_Input ('SUM', SUM)
        Tool.Set_Input ('SUM2', SUM2)
        Tool.Set_Input ('MIN', MIN)
        Tool.Set_Input ('MAX', MAX)
        Tool.Set_Input ('HISTOGRAM', HISTOGRAM)
        Tool.Set_Output('RANGE', RANGE)
        Tool.Set_Output('MEAN', MEAN)
        Tool.Set_Output('VAR', VAR)
        Tool.Set_Output('STDDEV', STDDEV)
        Tool.Set_Output('QUANTILES', QUANTILES)
        Tool.Set_Option('QUANTVALS', QUANTVALS)
        return Tool.Execute(Verbose)
    return False

def Add_Statistics_for_Grids(GRIDS=None, COUNT=None, SUM=None, SUM2=None, MIN=None, MAX=None, HISTOGRAM=None, Verbose=2):
    '''
    Add Statistics for Grids
    ----------
    [statistics_grid.19]\n
    This tool allows collecting successively cell-wise statistical information from grids by subsequent calls. The targeted data sets, particularly the histogram, should have been created with 'Build Statistics for Grids' tool. The collected information can be used consequently as input for the 'Evaluate Statistics for Grids' tool. These three tools (build, add, evaluate) have been designed to inspect a large number of grids that could otherwise not be evaluated simultaneously due to memory restrictions.\n
    Arguments
    ----------
    - GRIDS [`input grid list`] : Grids
    - COUNT [`optional input grid`] : Number of Values
    - SUM [`optional input grid`] : Sum
    - SUM2 [`optional input grid`] : Sum of Squares
    - MIN [`optional input grid`] : Minimum
    - MAX [`optional input grid`] : Maximum
    - HISTOGRAM [`optional input grid collection`] : Histogram

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('statistics_grid', '19', 'Add Statistics for Grids')
    if Tool.is_Okay():
        Tool.Set_Input ('GRIDS', GRIDS)
        Tool.Set_Input ('COUNT', COUNT)
        Tool.Set_Input ('SUM', SUM)
        Tool.Set_Input ('SUM2', SUM2)
        Tool.Set_Input ('MIN', MIN)
        Tool.Set_Input ('MAX', MAX)
        Tool.Set_Input ('HISTOGRAM', HISTOGRAM)
        return Tool.Execute(Verbose)
    return False

def run_tool_statistics_grid_19(GRIDS=None, COUNT=None, SUM=None, SUM2=None, MIN=None, MAX=None, HISTOGRAM=None, Verbose=2):
    '''
    Add Statistics for Grids
    ----------
    [statistics_grid.19]\n
    This tool allows collecting successively cell-wise statistical information from grids by subsequent calls. The targeted data sets, particularly the histogram, should have been created with 'Build Statistics for Grids' tool. The collected information can be used consequently as input for the 'Evaluate Statistics for Grids' tool. These three tools (build, add, evaluate) have been designed to inspect a large number of grids that could otherwise not be evaluated simultaneously due to memory restrictions.\n
    Arguments
    ----------
    - GRIDS [`input grid list`] : Grids
    - COUNT [`optional input grid`] : Number of Values
    - SUM [`optional input grid`] : Sum
    - SUM2 [`optional input grid`] : Sum of Squares
    - MIN [`optional input grid`] : Minimum
    - MAX [`optional input grid`] : Maximum
    - HISTOGRAM [`optional input grid collection`] : Histogram

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('statistics_grid', '19', 'Add Statistics for Grids')
    if Tool.is_Okay():
        Tool.Set_Input ('GRIDS', GRIDS)
        Tool.Set_Input ('COUNT', COUNT)
        Tool.Set_Input ('SUM', SUM)
        Tool.Set_Input ('SUM2', SUM2)
        Tool.Set_Input ('MIN', MIN)
        Tool.Set_Input ('MAX', MAX)
        Tool.Set_Input ('HISTOGRAM', HISTOGRAM)
        return Tool.Execute(Verbose)
    return False

def Unique_Value_Statistics_for_Grids(GRIDS=None, MAJORITY=None, MAJORITY_COUNT=None, MINORITY=None, MINORITY_COUNT=None, NUNIQUES=None, UNAMBIGUOUS=None, Verbose=2):
    '''
    Unique Value Statistics for Grids
    ----------
    [statistics_grid.20]\n
    This tool analyzes for each cell position the uniquely appearing values of the input grids. Output is the number of unique values, the most frequent value (the majority), and the least frequent value (minority).\n
    Arguments
    ----------
    - GRIDS [`input grid list`] : Values
    - MAJORITY [`output grid`] : Majority
    - MAJORITY_COUNT [`output grid`] : Majority Count
    - MINORITY [`output grid`] : Minority
    - MINORITY_COUNT [`output grid`] : Minority Count
    - NUNIQUES [`output grid`] : Number of Unique Values
    - UNAMBIGUOUS [`boolean`] : Unambiguous. Default: 0 Set no-data if more than one value reaches the same majority count.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('statistics_grid', '20', 'Unique Value Statistics for Grids')
    if Tool.is_Okay():
        Tool.Set_Input ('GRIDS', GRIDS)
        Tool.Set_Output('MAJORITY', MAJORITY)
        Tool.Set_Output('MAJORITY_COUNT', MAJORITY_COUNT)
        Tool.Set_Output('MINORITY', MINORITY)
        Tool.Set_Output('MINORITY_COUNT', MINORITY_COUNT)
        Tool.Set_Output('NUNIQUES', NUNIQUES)
        Tool.Set_Option('UNAMBIGUOUS', UNAMBIGUOUS)
        return Tool.Execute(Verbose)
    return False

def run_tool_statistics_grid_20(GRIDS=None, MAJORITY=None, MAJORITY_COUNT=None, MINORITY=None, MINORITY_COUNT=None, NUNIQUES=None, UNAMBIGUOUS=None, Verbose=2):
    '''
    Unique Value Statistics for Grids
    ----------
    [statistics_grid.20]\n
    This tool analyzes for each cell position the uniquely appearing values of the input grids. Output is the number of unique values, the most frequent value (the majority), and the least frequent value (minority).\n
    Arguments
    ----------
    - GRIDS [`input grid list`] : Values
    - MAJORITY [`output grid`] : Majority
    - MAJORITY_COUNT [`output grid`] : Majority Count
    - MINORITY [`output grid`] : Minority
    - MINORITY_COUNT [`output grid`] : Minority Count
    - NUNIQUES [`output grid`] : Number of Unique Values
    - UNAMBIGUOUS [`boolean`] : Unambiguous. Default: 0 Set no-data if more than one value reaches the same majority count.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('statistics_grid', '20', 'Unique Value Statistics for Grids')
    if Tool.is_Okay():
        Tool.Set_Input ('GRIDS', GRIDS)
        Tool.Set_Output('MAJORITY', MAJORITY)
        Tool.Set_Output('MAJORITY_COUNT', MAJORITY_COUNT)
        Tool.Set_Output('MINORITY', MINORITY)
        Tool.Set_Output('MINORITY_COUNT', MINORITY_COUNT)
        Tool.Set_Output('NUNIQUES', NUNIQUES)
        Tool.Set_Option('UNAMBIGUOUS', UNAMBIGUOUS)
        return Tool.Execute(Verbose)
    return False

def Grid_Histogram(GRID=None, HISTOGRAM=None, CLASSIFY=None, BINS=None, RANGE=None, LUT=None, UNCLASSED=None, PARALLEL=None, MAXSAMPLES=None, Verbose=2):
    '''
    Grid Histogram
    ----------
    [statistics_grid.21]\n
    This tool creates a histogram for the supplied grid using the specified classification.\n
    Arguments
    ----------
    - GRID [`input grid`] : Grid
    - HISTOGRAM [`output table`] : Histogram
    - CLASSIFY [`choice`] : Classification. Available Choices: [0] value range and number of classes [1] lookup table Default: 0
    - BINS [`integer number`] : Number of Classes. Minimum: 1 Default: 64
    - RANGE [`value range`] : Value Range
    - LUT [`static table`] : Lookup Table. 2 Fields: - 1. [8 byte floating point number] Minimum - 2. [8 byte floating point number] Maximum 
    - UNCLASSED [`boolean`] : Report Unclassified Cells. Default: 0
    - PARALLEL [`boolean`] : Parallelized. Default: 1
    - MAXSAMPLES [`floating point number`] : Maximum Samples. Minimum: 0.000000 Maximum: 100.000000 Default: 100.000000 Maximum number of samples [percent].

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('statistics_grid', '21', 'Grid Histogram')
    if Tool.is_Okay():
        Tool.Set_Input ('GRID', GRID)
        Tool.Set_Output('HISTOGRAM', HISTOGRAM)
        Tool.Set_Option('CLASSIFY', CLASSIFY)
        Tool.Set_Option('BINS', BINS)
        Tool.Set_Option('RANGE', RANGE)
        Tool.Set_Option('LUT', LUT)
        Tool.Set_Option('UNCLASSED', UNCLASSED)
        Tool.Set_Option('PARALLEL', PARALLEL)
        Tool.Set_Option('MAXSAMPLES', MAXSAMPLES)
        return Tool.Execute(Verbose)
    return False

def run_tool_statistics_grid_21(GRID=None, HISTOGRAM=None, CLASSIFY=None, BINS=None, RANGE=None, LUT=None, UNCLASSED=None, PARALLEL=None, MAXSAMPLES=None, Verbose=2):
    '''
    Grid Histogram
    ----------
    [statistics_grid.21]\n
    This tool creates a histogram for the supplied grid using the specified classification.\n
    Arguments
    ----------
    - GRID [`input grid`] : Grid
    - HISTOGRAM [`output table`] : Histogram
    - CLASSIFY [`choice`] : Classification. Available Choices: [0] value range and number of classes [1] lookup table Default: 0
    - BINS [`integer number`] : Number of Classes. Minimum: 1 Default: 64
    - RANGE [`value range`] : Value Range
    - LUT [`static table`] : Lookup Table. 2 Fields: - 1. [8 byte floating point number] Minimum - 2. [8 byte floating point number] Maximum 
    - UNCLASSED [`boolean`] : Report Unclassified Cells. Default: 0
    - PARALLEL [`boolean`] : Parallelized. Default: 1
    - MAXSAMPLES [`floating point number`] : Maximum Samples. Minimum: 0.000000 Maximum: 100.000000 Default: 100.000000 Maximum number of samples [percent].

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('statistics_grid', '21', 'Grid Histogram')
    if Tool.is_Okay():
        Tool.Set_Input ('GRID', GRID)
        Tool.Set_Output('HISTOGRAM', HISTOGRAM)
        Tool.Set_Option('CLASSIFY', CLASSIFY)
        Tool.Set_Option('BINS', BINS)
        Tool.Set_Option('RANGE', RANGE)
        Tool.Set_Option('LUT', LUT)
        Tool.Set_Option('UNCLASSED', UNCLASSED)
        Tool.Set_Option('PARALLEL', PARALLEL)
        Tool.Set_Option('MAXSAMPLES', MAXSAMPLES)
        return Tool.Execute(Verbose)
    return False

def Directional_Grid_Statistics(GRID=None, MEAN=None, GRID_SYSTEM=None, BEGIN=None, END=None, STEP=None, TOLERANCE=None, MAXDISTANCE=None, DW_WEIGHTING=None, DW_IDW_POWER=None, DW_BANDWIDTH=None, Verbose=2):
    '''
    Directional Grid Statistics
    ----------
    [statistics_grid.directional_grid_statistics]\n
    Directional Grid Statistics\n
    Arguments
    ----------
    - GRID [`input grid`] : Grid
    - MEAN [`output grid list`] : Arithmetic Mean
    - GRID_SYSTEM [`grid system`] : Grid System
    - BEGIN [`floating point number`] : Start Direction. Default: 0.000000 [degree]
    - END [`floating point number`] : Stop Direction. Default: 359.000000 [degree]
    - STEP [`floating point number`] : Directional Step. Minimum: 1.000000 Default: 45.000000 [degree]
    - TOLERANCE [`floating point number`] : Tolerance. Default: 0.000000 [degree]
    - MAXDISTANCE [`integer number`] : Maximum Distance. Default: 0 Maximum distance [cells], ignored if set to zero.
    - DW_WEIGHTING [`choice`] : Weighting. Available Choices: [0] no distance weighting [1] inverse distance to a power [2] exponential [3] gaussian weighting Default: 0
    - DW_IDW_POWER [`floating point number`] : Inverse Distance Weighting Power. Default: 2.000000
    - DW_BANDWIDTH [`floating point number`] : Gaussian and Exponential Weighting Bandwidth. Minimum: 0.000000 Default: 25.000000 [cells]

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('statistics_grid', 'directional_grid_statistics', 'Directional Grid Statistics')
    if Tool.is_Okay():
        Tool.Set_Input ('GRID', GRID)
        Tool.Set_Output('MEAN', MEAN)
        Tool.Set_Option('GRID_SYSTEM', GRID_SYSTEM)
        Tool.Set_Option('BEGIN', BEGIN)
        Tool.Set_Option('END', END)
        Tool.Set_Option('STEP', STEP)
        Tool.Set_Option('TOLERANCE', TOLERANCE)
        Tool.Set_Option('MAXDISTANCE', MAXDISTANCE)
        Tool.Set_Option('DW_WEIGHTING', DW_WEIGHTING)
        Tool.Set_Option('DW_IDW_POWER', DW_IDW_POWER)
        Tool.Set_Option('DW_BANDWIDTH', DW_BANDWIDTH)
        return Tool.Execute(Verbose)
    return False

def run_tool_statistics_grid_directional_grid_statistics(GRID=None, MEAN=None, GRID_SYSTEM=None, BEGIN=None, END=None, STEP=None, TOLERANCE=None, MAXDISTANCE=None, DW_WEIGHTING=None, DW_IDW_POWER=None, DW_BANDWIDTH=None, Verbose=2):
    '''
    Directional Grid Statistics
    ----------
    [statistics_grid.directional_grid_statistics]\n
    Directional Grid Statistics\n
    Arguments
    ----------
    - GRID [`input grid`] : Grid
    - MEAN [`output grid list`] : Arithmetic Mean
    - GRID_SYSTEM [`grid system`] : Grid System
    - BEGIN [`floating point number`] : Start Direction. Default: 0.000000 [degree]
    - END [`floating point number`] : Stop Direction. Default: 359.000000 [degree]
    - STEP [`floating point number`] : Directional Step. Minimum: 1.000000 Default: 45.000000 [degree]
    - TOLERANCE [`floating point number`] : Tolerance. Default: 0.000000 [degree]
    - MAXDISTANCE [`integer number`] : Maximum Distance. Default: 0 Maximum distance [cells], ignored if set to zero.
    - DW_WEIGHTING [`choice`] : Weighting. Available Choices: [0] no distance weighting [1] inverse distance to a power [2] exponential [3] gaussian weighting Default: 0
    - DW_IDW_POWER [`floating point number`] : Inverse Distance Weighting Power. Default: 2.000000
    - DW_BANDWIDTH [`floating point number`] : Gaussian and Exponential Weighting Bandwidth. Minimum: 0.000000 Default: 25.000000 [cells]

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('statistics_grid', 'directional_grid_statistics', 'Directional Grid Statistics')
    if Tool.is_Okay():
        Tool.Set_Input ('GRID', GRID)
        Tool.Set_Output('MEAN', MEAN)
        Tool.Set_Option('GRID_SYSTEM', GRID_SYSTEM)
        Tool.Set_Option('BEGIN', BEGIN)
        Tool.Set_Option('END', END)
        Tool.Set_Option('STEP', STEP)
        Tool.Set_Option('TOLERANCE', TOLERANCE)
        Tool.Set_Option('MAXDISTANCE', MAXDISTANCE)
        Tool.Set_Option('DW_WEIGHTING', DW_WEIGHTING)
        Tool.Set_Option('DW_IDW_POWER', DW_IDW_POWER)
        Tool.Set_Option('DW_BANDWIDTH', DW_BANDWIDTH)
        return Tool.Execute(Verbose)
    return False

