#! /usr/bin/env python

'''
Python Interface to SAGA Tools Library
----------
- Category : Table
- Name     : Tools
- ID       : table_tools

Description
----------
Tools for the creation and manipulation of tables.
'''

from PySAGA.helper import Tool_Wrapper

def Create_New_Table(TABLE=None, NAME=None, NFIELDS=None, FIELDS_NAME0=None, FIELDS_TYPE0=None, FIELDS_NAME1=None, FIELDS_TYPE1=None, Verbose=2):
    '''
    Create New Table
    ----------
    [table_tools.0]\n
    Creates a new empty table.\n
    Possible field types are:\n
    (-) unsigned 1 byte integer\n
    (-) signed 1 byte integer\n
    (-) unsigned 2 byte integer\n
    (-) signed 2 byte integer\n
    (-) unsigned 4 byte integer\n
    (-) signed 4 byte integer\n
    (-) unsigned 8 byte integer\n
    (-) signed 8 byte integer\n
    (-) 4 byte floating point number\n
    (-) 8 byte floating point number\n
    (-) string\n
    (-) date\n
    (-) color\n
    (-) binary\n
    Arguments
    ----------
    - TABLE [`output table`] : Table
    - NAME [`text`] : Name. Default: New Table
    - NFIELDS [`integer number`] : Number of Attributes. Minimum: 1 Default: 2
    - FIELDS_NAME0 [`text`] : Field 1. Default: Field 1 Name
    - FIELDS_TYPE0 [`data type`] : Type. Available Choices: [0] string [1] date [2] color [3] unsigned 1 byte integer [4] signed 1 byte integer [5] unsigned 2 byte integer [6] signed 2 byte integer [7] unsigned 4 byte integer [8] signed 4 byte integer [9] unsigned 8 byte integer [10] signed 8 byte integer [11] 4 byte floating point number [12] 8 byte floating point number [13] binary Default: 0 Type
    - FIELDS_NAME1 [`text`] : Field 2. Default: Field 2 Name
    - FIELDS_TYPE1 [`data type`] : Type. Available Choices: [0] string [1] date [2] color [3] unsigned 1 byte integer [4] signed 1 byte integer [5] unsigned 2 byte integer [6] signed 2 byte integer [7] unsigned 4 byte integer [8] signed 4 byte integer [9] unsigned 8 byte integer [10] signed 8 byte integer [11] 4 byte floating point number [12] 8 byte floating point number [13] binary Default: 0 Type

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('table_tools', '0', 'Create New Table')
    if Tool.is_Okay():
        Tool.Set_Output('TABLE', TABLE)
        Tool.Set_Option('NAME', NAME)
        Tool.Set_Option('NFIELDS', NFIELDS)
        Tool.Set_Option('FIELDS.NAME0', FIELDS_NAME0)
        Tool.Set_Option('FIELDS.TYPE0', FIELDS_TYPE0)
        Tool.Set_Option('FIELDS.NAME1', FIELDS_NAME1)
        Tool.Set_Option('FIELDS.TYPE1', FIELDS_TYPE1)
        return Tool.Execute(Verbose)
    return False

def run_tool_table_tools_0(TABLE=None, NAME=None, NFIELDS=None, FIELDS_NAME0=None, FIELDS_TYPE0=None, FIELDS_NAME1=None, FIELDS_TYPE1=None, Verbose=2):
    '''
    Create New Table
    ----------
    [table_tools.0]\n
    Creates a new empty table.\n
    Possible field types are:\n
    (-) unsigned 1 byte integer\n
    (-) signed 1 byte integer\n
    (-) unsigned 2 byte integer\n
    (-) signed 2 byte integer\n
    (-) unsigned 4 byte integer\n
    (-) signed 4 byte integer\n
    (-) unsigned 8 byte integer\n
    (-) signed 8 byte integer\n
    (-) 4 byte floating point number\n
    (-) 8 byte floating point number\n
    (-) string\n
    (-) date\n
    (-) color\n
    (-) binary\n
    Arguments
    ----------
    - TABLE [`output table`] : Table
    - NAME [`text`] : Name. Default: New Table
    - NFIELDS [`integer number`] : Number of Attributes. Minimum: 1 Default: 2
    - FIELDS_NAME0 [`text`] : Field 1. Default: Field 1 Name
    - FIELDS_TYPE0 [`data type`] : Type. Available Choices: [0] string [1] date [2] color [3] unsigned 1 byte integer [4] signed 1 byte integer [5] unsigned 2 byte integer [6] signed 2 byte integer [7] unsigned 4 byte integer [8] signed 4 byte integer [9] unsigned 8 byte integer [10] signed 8 byte integer [11] 4 byte floating point number [12] 8 byte floating point number [13] binary Default: 0 Type
    - FIELDS_NAME1 [`text`] : Field 2. Default: Field 2 Name
    - FIELDS_TYPE1 [`data type`] : Type. Available Choices: [0] string [1] date [2] color [3] unsigned 1 byte integer [4] signed 1 byte integer [5] unsigned 2 byte integer [6] signed 2 byte integer [7] unsigned 4 byte integer [8] signed 4 byte integer [9] unsigned 8 byte integer [10] signed 8 byte integer [11] 4 byte floating point number [12] 8 byte floating point number [13] binary Default: 0 Type

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('table_tools', '0', 'Create New Table')
    if Tool.is_Okay():
        Tool.Set_Output('TABLE', TABLE)
        Tool.Set_Option('NAME', NAME)
        Tool.Set_Option('NFIELDS', NFIELDS)
        Tool.Set_Option('FIELDS.NAME0', FIELDS_NAME0)
        Tool.Set_Option('FIELDS.TYPE0', FIELDS_TYPE0)
        Tool.Set_Option('FIELDS.NAME1', FIELDS_NAME1)
        Tool.Set_Option('FIELDS.TYPE1', FIELDS_TYPE1)
        return Tool.Execute(Verbose)
    return False

def Transpose_Table(INPUT=None, OUTPUT=None, TYPE=None, Verbose=2):
    '''
    Transpose Table
    ----------
    [table_tools.1]\n
    Transposes a table, i.e. swap the rows and columns.\n
    Arguments
    ----------
    - INPUT [`input table`] : Input
    - OUTPUT [`output table`] : Output
    - TYPE [`data type`] : Data Type. Available Choices: [0] string [1] unsigned 1 byte integer [2] signed 1 byte integer [3] unsigned 2 byte integer [4] signed 2 byte integer [5] unsigned 4 byte integer [6] signed 4 byte integer [7] unsigned 8 byte integer [8] signed 8 byte integer [9] 4 byte floating point number [10] 8 byte floating point number Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('table_tools', '1', 'Transpose Table')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Output('OUTPUT', OUTPUT)
        Tool.Set_Option('TYPE', TYPE)
        return Tool.Execute(Verbose)
    return False

def run_tool_table_tools_1(INPUT=None, OUTPUT=None, TYPE=None, Verbose=2):
    '''
    Transpose Table
    ----------
    [table_tools.1]\n
    Transposes a table, i.e. swap the rows and columns.\n
    Arguments
    ----------
    - INPUT [`input table`] : Input
    - OUTPUT [`output table`] : Output
    - TYPE [`data type`] : Data Type. Available Choices: [0] string [1] unsigned 1 byte integer [2] signed 1 byte integer [3] unsigned 2 byte integer [4] signed 2 byte integer [5] unsigned 4 byte integer [6] signed 4 byte integer [7] unsigned 8 byte integer [8] signed 8 byte integer [9] 4 byte floating point number [10] 8 byte floating point number Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('table_tools', '1', 'Transpose Table')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Output('OUTPUT', OUTPUT)
        Tool.Set_Option('TYPE', TYPE)
        return Tool.Execute(Verbose)
    return False

def Field_Enumeration(INPUT=None, RESULT_TABLE=None, RESULT_SHAPES=None, FIELD=None, ENUM=None, NAME=None, ORDER=None, Verbose=2):
    '''
    Field Enumeration
    ----------
    [table_tools.2]\n
    Enumeration of a table attribute, i.e. a unique identifier is assigned to identical values of the chosen attribute field. If no attribute is chosen, a simple enumeration is done for all records, and this with respect to the sorting order if the dataset has been indexed.\n
    Arguments
    ----------
    - INPUT [`input table`] : Input
    - RESULT_TABLE [`output table`] : Result
    - RESULT_SHAPES [`output shapes`] : Result
    - FIELD [`table field`] : Attribute
    - ENUM [`table field`] : Enumeration
    - NAME [`text`] : Enumeration Field Name. Default: ENUM
    - ORDER [`choice`] : Order. Available Choices: [0] ascending [1] descending Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('table_tools', '2', 'Field Enumeration')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Output('RESULT_TABLE', RESULT_TABLE)
        Tool.Set_Output('RESULT_SHAPES', RESULT_SHAPES)
        Tool.Set_Option('FIELD', FIELD)
        Tool.Set_Option('ENUM', ENUM)
        Tool.Set_Option('NAME', NAME)
        Tool.Set_Option('ORDER', ORDER)
        return Tool.Execute(Verbose)
    return False

def run_tool_table_tools_2(INPUT=None, RESULT_TABLE=None, RESULT_SHAPES=None, FIELD=None, ENUM=None, NAME=None, ORDER=None, Verbose=2):
    '''
    Field Enumeration
    ----------
    [table_tools.2]\n
    Enumeration of a table attribute, i.e. a unique identifier is assigned to identical values of the chosen attribute field. If no attribute is chosen, a simple enumeration is done for all records, and this with respect to the sorting order if the dataset has been indexed.\n
    Arguments
    ----------
    - INPUT [`input table`] : Input
    - RESULT_TABLE [`output table`] : Result
    - RESULT_SHAPES [`output shapes`] : Result
    - FIELD [`table field`] : Attribute
    - ENUM [`table field`] : Enumeration
    - NAME [`text`] : Enumeration Field Name. Default: ENUM
    - ORDER [`choice`] : Order. Available Choices: [0] ascending [1] descending Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('table_tools', '2', 'Field Enumeration')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Output('RESULT_TABLE', RESULT_TABLE)
        Tool.Set_Output('RESULT_SHAPES', RESULT_SHAPES)
        Tool.Set_Option('FIELD', FIELD)
        Tool.Set_Option('ENUM', ENUM)
        Tool.Set_Option('NAME', NAME)
        Tool.Set_Option('ORDER', ORDER)
        return Tool.Execute(Verbose)
    return False

def Join_Attributes_from_a_Table(TABLE_A=None, TABLE_B=None, UNJOINED=None, RESULT_TABLE=None, RESULT_SHAPES=None, ID_A=None, ID_B=None, FIELDS_ALL=None, FIELDS=None, KEEP_ALL=None, CMP_CASE=None, Verbose=2):
    '''
    Join Attributes from a Table
    ----------
    [table_tools.3]\n
    Joins two tables using key attributes.\n
    Arguments
    ----------
    - TABLE_A [`input table`] : Input Table
    - TABLE_B [`input table`] : Join Table
    - UNJOINED [`output table`] : Unjoined Records. Collect unjoined records from join table.
    - RESULT_TABLE [`output table`] : Result
    - RESULT_SHAPES [`output shapes`] : Result
    - ID_A [`table field`] : Input Join Field
    - ID_B [`table field`] : Join Table Field
    - FIELDS_ALL [`boolean`] : Add All Fields. Default: 1
    - FIELDS [`table fields`] : Fields
    - KEEP_ALL [`boolean`] : Keep All. Default: 1
    - CMP_CASE [`boolean`] : Case Sensitive String Comparison. Default: 1

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('table_tools', '3', 'Join Attributes from a Table')
    if Tool.is_Okay():
        Tool.Set_Input ('TABLE_A', TABLE_A)
        Tool.Set_Input ('TABLE_B', TABLE_B)
        Tool.Set_Output('UNJOINED', UNJOINED)
        Tool.Set_Output('RESULT_TABLE', RESULT_TABLE)
        Tool.Set_Output('RESULT_SHAPES', RESULT_SHAPES)
        Tool.Set_Option('ID_A', ID_A)
        Tool.Set_Option('ID_B', ID_B)
        Tool.Set_Option('FIELDS_ALL', FIELDS_ALL)
        Tool.Set_Option('FIELDS', FIELDS)
        Tool.Set_Option('KEEP_ALL', KEEP_ALL)
        Tool.Set_Option('CMP_CASE', CMP_CASE)
        return Tool.Execute(Verbose)
    return False

def run_tool_table_tools_3(TABLE_A=None, TABLE_B=None, UNJOINED=None, RESULT_TABLE=None, RESULT_SHAPES=None, ID_A=None, ID_B=None, FIELDS_ALL=None, FIELDS=None, KEEP_ALL=None, CMP_CASE=None, Verbose=2):
    '''
    Join Attributes from a Table
    ----------
    [table_tools.3]\n
    Joins two tables using key attributes.\n
    Arguments
    ----------
    - TABLE_A [`input table`] : Input Table
    - TABLE_B [`input table`] : Join Table
    - UNJOINED [`output table`] : Unjoined Records. Collect unjoined records from join table.
    - RESULT_TABLE [`output table`] : Result
    - RESULT_SHAPES [`output shapes`] : Result
    - ID_A [`table field`] : Input Join Field
    - ID_B [`table field`] : Join Table Field
    - FIELDS_ALL [`boolean`] : Add All Fields. Default: 1
    - FIELDS [`table fields`] : Fields
    - KEEP_ALL [`boolean`] : Keep All. Default: 1
    - CMP_CASE [`boolean`] : Case Sensitive String Comparison. Default: 1

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('table_tools', '3', 'Join Attributes from a Table')
    if Tool.is_Okay():
        Tool.Set_Input ('TABLE_A', TABLE_A)
        Tool.Set_Input ('TABLE_B', TABLE_B)
        Tool.Set_Output('UNJOINED', UNJOINED)
        Tool.Set_Output('RESULT_TABLE', RESULT_TABLE)
        Tool.Set_Output('RESULT_SHAPES', RESULT_SHAPES)
        Tool.Set_Option('ID_A', ID_A)
        Tool.Set_Option('ID_B', ID_B)
        Tool.Set_Option('FIELDS_ALL', FIELDS_ALL)
        Tool.Set_Option('FIELDS', FIELDS)
        Tool.Set_Option('KEEP_ALL', KEEP_ALL)
        Tool.Set_Option('CMP_CASE', CMP_CASE)
        return Tool.Execute(Verbose)
    return False

def Change_Date_Format(TABLE=None, OUTPUT=None, FIELD=None, FMT_IN=None, SEP_IN=None, FMT_OUT=None, SEP_OUT=None, Verbose=2):
    '''
    Change Date Format
    ----------
    [table_tools.5]\n
    Change Date Format\n
    Arguments
    ----------
    - TABLE [`input table`] : Table
    - OUTPUT [`output table`] : Output
    - FIELD [`table field`] : Date Field
    - FMT_IN [`choice`] : Input Format. Available Choices: [0] dd:mm:yyyy [1] yyyy:mm:dd [2] ddmmyyyy, fix size [3] yyyymmdd, fix size [4] ddmmyy, fix size [5] yymmdd, fix size [6] Julian Day [7] Unix Time Default: 0
    - SEP_IN [`text`] : Separator. Default: :
    - FMT_OUT [`choice`] : Output Format. Available Choices: [0] dd:mm:yyyy [1] yyyy:mm:dd [2] dd:mm:yy [3] yy:mm:dd [4] Julian Day [5] Date [6] ISO Date and Time Default: 0
    - SEP_OUT [`text`] : Separator. Default: :

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('table_tools', '5', 'Change Date Format')
    if Tool.is_Okay():
        Tool.Set_Input ('TABLE', TABLE)
        Tool.Set_Output('OUTPUT', OUTPUT)
        Tool.Set_Option('FIELD', FIELD)
        Tool.Set_Option('FMT_IN', FMT_IN)
        Tool.Set_Option('SEP_IN', SEP_IN)
        Tool.Set_Option('FMT_OUT', FMT_OUT)
        Tool.Set_Option('SEP_OUT', SEP_OUT)
        return Tool.Execute(Verbose)
    return False

def run_tool_table_tools_5(TABLE=None, OUTPUT=None, FIELD=None, FMT_IN=None, SEP_IN=None, FMT_OUT=None, SEP_OUT=None, Verbose=2):
    '''
    Change Date Format
    ----------
    [table_tools.5]\n
    Change Date Format\n
    Arguments
    ----------
    - TABLE [`input table`] : Table
    - OUTPUT [`output table`] : Output
    - FIELD [`table field`] : Date Field
    - FMT_IN [`choice`] : Input Format. Available Choices: [0] dd:mm:yyyy [1] yyyy:mm:dd [2] ddmmyyyy, fix size [3] yyyymmdd, fix size [4] ddmmyy, fix size [5] yymmdd, fix size [6] Julian Day [7] Unix Time Default: 0
    - SEP_IN [`text`] : Separator. Default: :
    - FMT_OUT [`choice`] : Output Format. Available Choices: [0] dd:mm:yyyy [1] yyyy:mm:dd [2] dd:mm:yy [3] yy:mm:dd [4] Julian Day [5] Date [6] ISO Date and Time Default: 0
    - SEP_OUT [`text`] : Separator. Default: :

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('table_tools', '5', 'Change Date Format')
    if Tool.is_Okay():
        Tool.Set_Input ('TABLE', TABLE)
        Tool.Set_Output('OUTPUT', OUTPUT)
        Tool.Set_Option('FIELD', FIELD)
        Tool.Set_Option('FMT_IN', FMT_IN)
        Tool.Set_Option('SEP_IN', SEP_IN)
        Tool.Set_Option('FMT_OUT', FMT_OUT)
        Tool.Set_Option('SEP_OUT', SEP_OUT)
        return Tool.Execute(Verbose)
    return False

def Change_Time_Format(TABLE=None, OUTPUT=None, FIELD=None, FMT_IN=None, FMT_OUT=None, Verbose=2):
    '''
    Change Time Format
    ----------
    [table_tools.6]\n
    Change Time Format\n
    Arguments
    ----------
    - TABLE [`input table`] : Table
    - OUTPUT [`output table`] : Output
    - FIELD [`table field`] : Time Field
    - FMT_IN [`choice`] : Input Format. Available Choices: [0] hh.mm.ss [1] hh:mm:ss [2] hhmmss, fix size [3] hours [4] minutes [5] seconds Default: 1
    - FMT_OUT [`choice`] : Output Format. Available Choices: [0] hh.mm.ss [1] hh:mm:ss [2] hhmmss, fix size [3] hours [4] minutes [5] seconds Default: 5

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('table_tools', '6', 'Change Time Format')
    if Tool.is_Okay():
        Tool.Set_Input ('TABLE', TABLE)
        Tool.Set_Output('OUTPUT', OUTPUT)
        Tool.Set_Option('FIELD', FIELD)
        Tool.Set_Option('FMT_IN', FMT_IN)
        Tool.Set_Option('FMT_OUT', FMT_OUT)
        return Tool.Execute(Verbose)
    return False

def run_tool_table_tools_6(TABLE=None, OUTPUT=None, FIELD=None, FMT_IN=None, FMT_OUT=None, Verbose=2):
    '''
    Change Time Format
    ----------
    [table_tools.6]\n
    Change Time Format\n
    Arguments
    ----------
    - TABLE [`input table`] : Table
    - OUTPUT [`output table`] : Output
    - FIELD [`table field`] : Time Field
    - FMT_IN [`choice`] : Input Format. Available Choices: [0] hh.mm.ss [1] hh:mm:ss [2] hhmmss, fix size [3] hours [4] minutes [5] seconds Default: 1
    - FMT_OUT [`choice`] : Output Format. Available Choices: [0] hh.mm.ss [1] hh:mm:ss [2] hhmmss, fix size [3] hours [4] minutes [5] seconds Default: 5

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('table_tools', '6', 'Change Time Format')
    if Tool.is_Okay():
        Tool.Set_Input ('TABLE', TABLE)
        Tool.Set_Output('OUTPUT', OUTPUT)
        Tool.Set_Option('FIELD', FIELD)
        Tool.Set_Option('FMT_IN', FMT_IN)
        Tool.Set_Option('FMT_OUT', FMT_OUT)
        return Tool.Execute(Verbose)
    return False

def Change_Field_Type(TABLE=None, OUTPUT=None, FIELD=None, TYPE=None, Verbose=2):
    '''
    Change Field Type
    ----------
    [table_tools.7]\n
    With this tool you can change the data type of a table's attribute field.\n
    Arguments
    ----------
    - TABLE [`input table`] : Table
    - OUTPUT [`output table`] : Output
    - FIELD [`table field`] : Field
    - TYPE [`data type`] : Data Type. Available Choices: [0] string [1] date [2] color [3] unsigned 1 byte integer [4] signed 1 byte integer [5] unsigned 2 byte integer [6] signed 2 byte integer [7] unsigned 4 byte integer [8] signed 4 byte integer [9] unsigned 8 byte integer [10] signed 8 byte integer [11] 4 byte floating point number [12] 8 byte floating point number [13] binary Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('table_tools', '7', 'Change Field Type')
    if Tool.is_Okay():
        Tool.Set_Input ('TABLE', TABLE)
        Tool.Set_Output('OUTPUT', OUTPUT)
        Tool.Set_Option('FIELD', FIELD)
        Tool.Set_Option('TYPE', TYPE)
        return Tool.Execute(Verbose)
    return False

def run_tool_table_tools_7(TABLE=None, OUTPUT=None, FIELD=None, TYPE=None, Verbose=2):
    '''
    Change Field Type
    ----------
    [table_tools.7]\n
    With this tool you can change the data type of a table's attribute field.\n
    Arguments
    ----------
    - TABLE [`input table`] : Table
    - OUTPUT [`output table`] : Output
    - FIELD [`table field`] : Field
    - TYPE [`data type`] : Data Type. Available Choices: [0] string [1] date [2] color [3] unsigned 1 byte integer [4] signed 1 byte integer [5] unsigned 2 byte integer [6] signed 2 byte integer [7] unsigned 4 byte integer [8] signed 4 byte integer [9] unsigned 8 byte integer [10] signed 8 byte integer [11] 4 byte floating point number [12] 8 byte floating point number [13] binary Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('table_tools', '7', 'Change Field Type')
    if Tool.is_Okay():
        Tool.Set_Input ('TABLE', TABLE)
        Tool.Set_Output('OUTPUT', OUTPUT)
        Tool.Set_Option('FIELD', FIELD)
        Tool.Set_Option('TYPE', TYPE)
        return Tool.Execute(Verbose)
    return False

def Append_Fields_from_another_Table(INPUT=None, APPEND=None, OUTPUT=None, Verbose=2):
    '''
    Append Fields from another Table
    ----------
    [table_tools.8]\n
    Append Fields from another Table\n
    Arguments
    ----------
    - INPUT [`input table`] : Table
    - APPEND [`input table`] : Append Rows from ...
    - OUTPUT [`output table`] : Result

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('table_tools', '8', 'Append Fields from another Table')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Input ('APPEND', APPEND)
        Tool.Set_Output('OUTPUT', OUTPUT)
        return Tool.Execute(Verbose)
    return False

def run_tool_table_tools_8(INPUT=None, APPEND=None, OUTPUT=None, Verbose=2):
    '''
    Append Fields from another Table
    ----------
    [table_tools.8]\n
    Append Fields from another Table\n
    Arguments
    ----------
    - INPUT [`input table`] : Table
    - APPEND [`input table`] : Append Rows from ...
    - OUTPUT [`output table`] : Result

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('table_tools', '8', 'Append Fields from another Table')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Input ('APPEND', APPEND)
        Tool.Set_Output('OUTPUT', OUTPUT)
        return Tool.Execute(Verbose)
    return False

def Change_Color_Format(TABLE=None, OUTPUT=None, OUTPUT_SHP=None, OUTPUT_PC=None, FIELD_RGB=None, FIELD_RED=None, FIELD_GREEN=None, FIELD_BLUE=None, MODE=None, ATTR_SUFFIX=None, COLOR_DEPTH=None, NORM=None, NORM_RANGE=None, NORM_STDDEV=None, Verbose=2):
    '''
    Change Color Format
    ----------
    [table_tools.9]\n
    This tool allows one to convert table fields with RGB coded values to separate R, G, B components and vice versa. The tool can process attributes of tables, shapes or point clouds.\n
    Arguments
    ----------
    - TABLE [`input table`] : Table. The input table.
    - OUTPUT [`output table`] : Output. The output table.
    - OUTPUT_SHP [`output shapes`] : Output. The output shapes.
    - OUTPUT_PC [`output point cloud`] : Output. The output point cloud.
    - FIELD_RGB [`table field`] : RGB. The field with RGB coded values.
    - FIELD_RED [`table field`] : Red. The field with R values.
    - FIELD_GREEN [`table field`] : Green. The field with G values.
    - FIELD_BLUE [`table field`] : Blue. The field with B values.
    - MODE [`choice`] : Mode of Operation. Available Choices: [0] RGB to R, G, B [1] R, G, B to RGB Default: 0 Choose the mode of operation.
    - ATTR_SUFFIX [`text`] : Attribute Suffix. Optional suffix for output attribute names.
    - COLOR_DEPTH [`choice`] : Color Depth. Available Choices: [0] 8 bit [1] 16 bit Default: 0 Choose the color depth of the red, green, blue values, either 8 bit [0-255] or 16 bit [0-65535].
    - NORM [`choice`] : Normalization. Available Choices: [0] none [1] minimum - maximum [2] standard deviation Default: 0
    - NORM_RANGE [`floating point number`] : Percent. Minimum: 0.000000 Maximum: 50.000000 Default: 0.000000
    - NORM_STDDEV [`floating point number`] : Standard Deviation. Minimum: 0.000000 Default: 2.000000

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('table_tools', '9', 'Change Color Format')
    if Tool.is_Okay():
        Tool.Set_Input ('TABLE', TABLE)
        Tool.Set_Output('OUTPUT', OUTPUT)
        Tool.Set_Output('OUTPUT_SHP', OUTPUT_SHP)
        Tool.Set_Output('OUTPUT_PC', OUTPUT_PC)
        Tool.Set_Option('FIELD_RGB', FIELD_RGB)
        Tool.Set_Option('FIELD_RED', FIELD_RED)
        Tool.Set_Option('FIELD_GREEN', FIELD_GREEN)
        Tool.Set_Option('FIELD_BLUE', FIELD_BLUE)
        Tool.Set_Option('MODE', MODE)
        Tool.Set_Option('ATTR_SUFFIX', ATTR_SUFFIX)
        Tool.Set_Option('COLOR_DEPTH', COLOR_DEPTH)
        Tool.Set_Option('NORM', NORM)
        Tool.Set_Option('NORM_RANGE', NORM_RANGE)
        Tool.Set_Option('NORM_STDDEV', NORM_STDDEV)
        return Tool.Execute(Verbose)
    return False

def run_tool_table_tools_9(TABLE=None, OUTPUT=None, OUTPUT_SHP=None, OUTPUT_PC=None, FIELD_RGB=None, FIELD_RED=None, FIELD_GREEN=None, FIELD_BLUE=None, MODE=None, ATTR_SUFFIX=None, COLOR_DEPTH=None, NORM=None, NORM_RANGE=None, NORM_STDDEV=None, Verbose=2):
    '''
    Change Color Format
    ----------
    [table_tools.9]\n
    This tool allows one to convert table fields with RGB coded values to separate R, G, B components and vice versa. The tool can process attributes of tables, shapes or point clouds.\n
    Arguments
    ----------
    - TABLE [`input table`] : Table. The input table.
    - OUTPUT [`output table`] : Output. The output table.
    - OUTPUT_SHP [`output shapes`] : Output. The output shapes.
    - OUTPUT_PC [`output point cloud`] : Output. The output point cloud.
    - FIELD_RGB [`table field`] : RGB. The field with RGB coded values.
    - FIELD_RED [`table field`] : Red. The field with R values.
    - FIELD_GREEN [`table field`] : Green. The field with G values.
    - FIELD_BLUE [`table field`] : Blue. The field with B values.
    - MODE [`choice`] : Mode of Operation. Available Choices: [0] RGB to R, G, B [1] R, G, B to RGB Default: 0 Choose the mode of operation.
    - ATTR_SUFFIX [`text`] : Attribute Suffix. Optional suffix for output attribute names.
    - COLOR_DEPTH [`choice`] : Color Depth. Available Choices: [0] 8 bit [1] 16 bit Default: 0 Choose the color depth of the red, green, blue values, either 8 bit [0-255] or 16 bit [0-65535].
    - NORM [`choice`] : Normalization. Available Choices: [0] none [1] minimum - maximum [2] standard deviation Default: 0
    - NORM_RANGE [`floating point number`] : Percent. Minimum: 0.000000 Maximum: 50.000000 Default: 0.000000
    - NORM_STDDEV [`floating point number`] : Standard Deviation. Minimum: 0.000000 Default: 2.000000

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('table_tools', '9', 'Change Color Format')
    if Tool.is_Okay():
        Tool.Set_Input ('TABLE', TABLE)
        Tool.Set_Output('OUTPUT', OUTPUT)
        Tool.Set_Output('OUTPUT_SHP', OUTPUT_SHP)
        Tool.Set_Output('OUTPUT_PC', OUTPUT_PC)
        Tool.Set_Option('FIELD_RGB', FIELD_RGB)
        Tool.Set_Option('FIELD_RED', FIELD_RED)
        Tool.Set_Option('FIELD_GREEN', FIELD_GREEN)
        Tool.Set_Option('FIELD_BLUE', FIELD_BLUE)
        Tool.Set_Option('MODE', MODE)
        Tool.Set_Option('ATTR_SUFFIX', ATTR_SUFFIX)
        Tool.Set_Option('COLOR_DEPTH', COLOR_DEPTH)
        Tool.Set_Option('NORM', NORM)
        Tool.Set_Option('NORM_RANGE', NORM_RANGE)
        Tool.Set_Option('NORM_STDDEV', NORM_STDDEV)
        return Tool.Execute(Verbose)
    return False

def Replace_Text(TABLE=None, OUT_TABLE=None, OUT_SHAPES=None, FIELD=None, REPLACE=None, Verbose=2):
    '''
    Replace Text
    ----------
    [table_tools.10]\n
    For the selected attribute or, if not specified, for all text attributes this tool replaces text strings with replacements as defined in table 'Text Replacements'.\n
    Arguments
    ----------
    - TABLE [`input table`] : Table
    - OUT_TABLE [`output table`] : Table with Text Replacements
    - OUT_SHAPES [`output shapes`] : Shapes with Text Replacements
    - FIELD [`table field`] : Attribute
    - REPLACE [`static table`] : Text Replacements. 2 Fields: - 1. [string] Original - 2. [string] Replacement 

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('table_tools', '10', 'Replace Text')
    if Tool.is_Okay():
        Tool.Set_Input ('TABLE', TABLE)
        Tool.Set_Output('OUT_TABLE', OUT_TABLE)
        Tool.Set_Output('OUT_SHAPES', OUT_SHAPES)
        Tool.Set_Option('FIELD', FIELD)
        Tool.Set_Option('REPLACE', REPLACE)
        return Tool.Execute(Verbose)
    return False

def run_tool_table_tools_10(TABLE=None, OUT_TABLE=None, OUT_SHAPES=None, FIELD=None, REPLACE=None, Verbose=2):
    '''
    Replace Text
    ----------
    [table_tools.10]\n
    For the selected attribute or, if not specified, for all text attributes this tool replaces text strings with replacements as defined in table 'Text Replacements'.\n
    Arguments
    ----------
    - TABLE [`input table`] : Table
    - OUT_TABLE [`output table`] : Table with Text Replacements
    - OUT_SHAPES [`output shapes`] : Shapes with Text Replacements
    - FIELD [`table field`] : Attribute
    - REPLACE [`static table`] : Text Replacements. 2 Fields: - 1. [string] Original - 2. [string] Replacement 

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('table_tools', '10', 'Replace Text')
    if Tool.is_Okay():
        Tool.Set_Input ('TABLE', TABLE)
        Tool.Set_Output('OUT_TABLE', OUT_TABLE)
        Tool.Set_Output('OUT_SHAPES', OUT_SHAPES)
        Tool.Set_Option('FIELD', FIELD)
        Tool.Set_Option('REPLACE', REPLACE)
        return Tool.Execute(Verbose)
    return False

def Delete_Fields(TABLE=None, OUT_TABLE=None, OUT_SHAPES=None, FIELDS=None, Verbose=2):
    '''
    Delete Fields
    ----------
    [table_tools.11]\n
    Deletes selected fields from a table or shapefile.\n
    Arguments
    ----------
    - TABLE [`input table`] : Table. Input table or shapefile
    - OUT_TABLE [`output table`] : Output table with field(s) deleted
    - OUT_SHAPES [`output shapes`] : Output shapes with field(s) deleted
    - FIELDS [`table fields`] : Fields

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('table_tools', '11', 'Delete Fields')
    if Tool.is_Okay():
        Tool.Set_Input ('TABLE', TABLE)
        Tool.Set_Output('OUT_TABLE', OUT_TABLE)
        Tool.Set_Output('OUT_SHAPES', OUT_SHAPES)
        Tool.Set_Option('FIELDS', FIELDS)
        return Tool.Execute(Verbose)
    return False

def run_tool_table_tools_11(TABLE=None, OUT_TABLE=None, OUT_SHAPES=None, FIELDS=None, Verbose=2):
    '''
    Delete Fields
    ----------
    [table_tools.11]\n
    Deletes selected fields from a table or shapefile.\n
    Arguments
    ----------
    - TABLE [`input table`] : Table. Input table or shapefile
    - OUT_TABLE [`output table`] : Output table with field(s) deleted
    - OUT_SHAPES [`output shapes`] : Output shapes with field(s) deleted
    - FIELDS [`table fields`] : Fields

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('table_tools', '11', 'Delete Fields')
    if Tool.is_Okay():
        Tool.Set_Input ('TABLE', TABLE)
        Tool.Set_Output('OUT_TABLE', OUT_TABLE)
        Tool.Set_Output('OUT_SHAPES', OUT_SHAPES)
        Tool.Set_Option('FIELDS', FIELDS)
        return Tool.Execute(Verbose)
    return False

def Copy_Selection(TABLE=None, OUT_TABLE=None, OUT_SHAPES=None, Verbose=2):
    '''
    Copy Selection
    ----------
    [table_tools.15]\n
    Copies selected records to a new table.\n
    Arguments
    ----------
    - TABLE [`input table`] : Table
    - OUT_TABLE [`output table`] : Copied Selection
    - OUT_SHAPES [`output shapes`] : Copied Selection

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('table_tools', '15', 'Copy Selection')
    if Tool.is_Okay():
        Tool.Set_Input ('TABLE', TABLE)
        Tool.Set_Output('OUT_TABLE', OUT_TABLE)
        Tool.Set_Output('OUT_SHAPES', OUT_SHAPES)
        return Tool.Execute(Verbose)
    return False

def run_tool_table_tools_15(TABLE=None, OUT_TABLE=None, OUT_SHAPES=None, Verbose=2):
    '''
    Copy Selection
    ----------
    [table_tools.15]\n
    Copies selected records to a new table.\n
    Arguments
    ----------
    - TABLE [`input table`] : Table
    - OUT_TABLE [`output table`] : Copied Selection
    - OUT_SHAPES [`output shapes`] : Copied Selection

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('table_tools', '15', 'Copy Selection')
    if Tool.is_Okay():
        Tool.Set_Input ('TABLE', TABLE)
        Tool.Set_Output('OUT_TABLE', OUT_TABLE)
        Tool.Set_Output('OUT_SHAPES', OUT_SHAPES)
        return Tool.Execute(Verbose)
    return False

def Delete_Selection(INPUT=None, Verbose=2):
    '''
    Delete Selection
    ----------
    [table_tools.16]\n
    Deletes selected records from table.\n
    Arguments
    ----------
    - INPUT [`input table`] : Input

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('table_tools', '16', 'Delete Selection')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        return Tool.Execute(Verbose)
    return False

def run_tool_table_tools_16(INPUT=None, Verbose=2):
    '''
    Delete Selection
    ----------
    [table_tools.16]\n
    Deletes selected records from table.\n
    Arguments
    ----------
    - INPUT [`input table`] : Input

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('table_tools', '16', 'Delete Selection')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        return Tool.Execute(Verbose)
    return False

def Invert_Selection(INPUT=None, Verbose=2):
    '''
    Invert Selection
    ----------
    [table_tools.17]\n
    Deselects selected and selects unselected records of given table.\n
    Arguments
    ----------
    - INPUT [`input table`] : Input

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('table_tools', '17', 'Invert Selection')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        return Tool.Execute(Verbose)
    return False

def run_tool_table_tools_17(INPUT=None, Verbose=2):
    '''
    Invert Selection
    ----------
    [table_tools.17]\n
    Deselects selected and selects unselected records of given table.\n
    Arguments
    ----------
    - INPUT [`input table`] : Input

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('table_tools', '17', 'Invert Selection')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        return Tool.Execute(Verbose)
    return False

def Select_by_Numerical_Expression(TABLE=None, COPY=None, FIELD=None, EXPRESSION=None, INVERSE=None, USE_NODATA=None, METHOD=None, POSTJOB=None, Verbose=2):
    '''
    Select by Numerical Expression
    ----------
    [table_tools.18]\n
    Selects records for which the expression evaluates to non-zero. The expression syntax is the same as the one for the table calculator. If an attribute field is selected, the expression evaluates only this attribute, which can be addressed with the letter 'x' in the expression formula. If no attribute is selected, attributes are addressed by the character 'f' (for 'field') followed by the field number (i.e.: f1, f2, ..., fn) or by the field in quota (e.g.: "Field Name").\n
    Examples:\n
    (-) f1 > f2\n
    (-) eq("Population" * 0.001, "Area")\n
    Arguments
    ----------
    - TABLE [`input table`] : Table
    - COPY [`output shapes`] : Copy
    - FIELD [`table field`] : Attribute. attribute to be searched; if not set all attributes will be searched
    - EXPRESSION [`text`] : Expression. Default: f1 > 0
    - INVERSE [`boolean`] : Inverse. Default: 0
    - USE_NODATA [`boolean`] : Use No-Data. Default: 0
    - METHOD [`choice`] : Method. Available Choices: [0] new selection [1] add to current selection [2] select from current selection [3] remove from current selection Default: 0
    - POSTJOB [`choice`] : Post Job. Available Choices: [0] none [1] copy [2] move [3] delete Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('table_tools', '18', 'Select by Numerical Expression')
    if Tool.is_Okay():
        Tool.Set_Input ('TABLE', TABLE)
        Tool.Set_Output('COPY', COPY)
        Tool.Set_Option('FIELD', FIELD)
        Tool.Set_Option('EXPRESSION', EXPRESSION)
        Tool.Set_Option('INVERSE', INVERSE)
        Tool.Set_Option('USE_NODATA', USE_NODATA)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('POSTJOB', POSTJOB)
        return Tool.Execute(Verbose)
    return False

def run_tool_table_tools_18(TABLE=None, COPY=None, FIELD=None, EXPRESSION=None, INVERSE=None, USE_NODATA=None, METHOD=None, POSTJOB=None, Verbose=2):
    '''
    Select by Numerical Expression
    ----------
    [table_tools.18]\n
    Selects records for which the expression evaluates to non-zero. The expression syntax is the same as the one for the table calculator. If an attribute field is selected, the expression evaluates only this attribute, which can be addressed with the letter 'x' in the expression formula. If no attribute is selected, attributes are addressed by the character 'f' (for 'field') followed by the field number (i.e.: f1, f2, ..., fn) or by the field in quota (e.g.: "Field Name").\n
    Examples:\n
    (-) f1 > f2\n
    (-) eq("Population" * 0.001, "Area")\n
    Arguments
    ----------
    - TABLE [`input table`] : Table
    - COPY [`output shapes`] : Copy
    - FIELD [`table field`] : Attribute. attribute to be searched; if not set all attributes will be searched
    - EXPRESSION [`text`] : Expression. Default: f1 > 0
    - INVERSE [`boolean`] : Inverse. Default: 0
    - USE_NODATA [`boolean`] : Use No-Data. Default: 0
    - METHOD [`choice`] : Method. Available Choices: [0] new selection [1] add to current selection [2] select from current selection [3] remove from current selection Default: 0
    - POSTJOB [`choice`] : Post Job. Available Choices: [0] none [1] copy [2] move [3] delete Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('table_tools', '18', 'Select by Numerical Expression')
    if Tool.is_Okay():
        Tool.Set_Input ('TABLE', TABLE)
        Tool.Set_Output('COPY', COPY)
        Tool.Set_Option('FIELD', FIELD)
        Tool.Set_Option('EXPRESSION', EXPRESSION)
        Tool.Set_Option('INVERSE', INVERSE)
        Tool.Set_Option('USE_NODATA', USE_NODATA)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('POSTJOB', POSTJOB)
        return Tool.Execute(Verbose)
    return False

def Select_by_String_Expression(TABLE=None, COPY=None, FIELD=None, EXPRESSION=None, CASE=None, COMPARE=None, INVERSE=None, METHOD=None, POSTJOB=None, Verbose=2):
    '''
    Select by String Expression
    ----------
    [table_tools.19]\n
    Searches for an character string expression in the attributes table and selects records where the expression is found.\n
    Arguments
    ----------
    - TABLE [`input table`] : Table
    - COPY [`output table`] : Copy
    - FIELD [`table field`] : Attribute. attribute to be searched; if not set all attributes will be searched
    - EXPRESSION [`text`] : Expression
    - CASE [`boolean`] : Case Sensitive. Default: 1
    - COMPARE [`choice`] : Select if.... Available Choices: [0] attribute is identical with search expression [1] attribute contains search expression [2] attribute is contained in search expression Default: 1
    - INVERSE [`boolean`] : Inverse. Default: 0
    - METHOD [`choice`] : Method. Available Choices: [0] new selection [1] add to current selection [2] select from current selection [3] remove from current selection Default: 0
    - POSTJOB [`choice`] : Post Job. Available Choices: [0] none [1] copy [2] move [3] delete Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('table_tools', '19', 'Select by String Expression')
    if Tool.is_Okay():
        Tool.Set_Input ('TABLE', TABLE)
        Tool.Set_Output('COPY', COPY)
        Tool.Set_Option('FIELD', FIELD)
        Tool.Set_Option('EXPRESSION', EXPRESSION)
        Tool.Set_Option('CASE', CASE)
        Tool.Set_Option('COMPARE', COMPARE)
        Tool.Set_Option('INVERSE', INVERSE)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('POSTJOB', POSTJOB)
        return Tool.Execute(Verbose)
    return False

def run_tool_table_tools_19(TABLE=None, COPY=None, FIELD=None, EXPRESSION=None, CASE=None, COMPARE=None, INVERSE=None, METHOD=None, POSTJOB=None, Verbose=2):
    '''
    Select by String Expression
    ----------
    [table_tools.19]\n
    Searches for an character string expression in the attributes table and selects records where the expression is found.\n
    Arguments
    ----------
    - TABLE [`input table`] : Table
    - COPY [`output table`] : Copy
    - FIELD [`table field`] : Attribute. attribute to be searched; if not set all attributes will be searched
    - EXPRESSION [`text`] : Expression
    - CASE [`boolean`] : Case Sensitive. Default: 1
    - COMPARE [`choice`] : Select if.... Available Choices: [0] attribute is identical with search expression [1] attribute contains search expression [2] attribute is contained in search expression Default: 1
    - INVERSE [`boolean`] : Inverse. Default: 0
    - METHOD [`choice`] : Method. Available Choices: [0] new selection [1] add to current selection [2] select from current selection [3] remove from current selection Default: 0
    - POSTJOB [`choice`] : Post Job. Available Choices: [0] none [1] copy [2] move [3] delete Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('table_tools', '19', 'Select by String Expression')
    if Tool.is_Okay():
        Tool.Set_Input ('TABLE', TABLE)
        Tool.Set_Output('COPY', COPY)
        Tool.Set_Option('FIELD', FIELD)
        Tool.Set_Option('EXPRESSION', EXPRESSION)
        Tool.Set_Option('CASE', CASE)
        Tool.Set_Option('COMPARE', COMPARE)
        Tool.Set_Option('INVERSE', INVERSE)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('POSTJOB', POSTJOB)
        return Tool.Execute(Verbose)
    return False

def Add_Indicator_Fields_for_Categories(TABLE=None, OUT_TABLE=None, OUT_SHAPES=None, FIELD=None, Verbose=2):
    '''
    Add Indicator Fields for Categories
    ----------
    [table_tools.20]\n
    Adds for each unique value found in the category field an indicator field that will show a value of one (1) for all records with this category value and zero (0) for all others. This might be used e.g. for subsequent indicator kriging.\n
    Arguments
    ----------
    - TABLE [`input table`] : Table. Input table or shapefile
    - OUT_TABLE [`output table`] : Output table with field(s) deleted
    - OUT_SHAPES [`output shapes`] : Output shapes with field(s) deleted
    - FIELD [`table field`] : Categories

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('table_tools', '20', 'Add Indicator Fields for Categories')
    if Tool.is_Okay():
        Tool.Set_Input ('TABLE', TABLE)
        Tool.Set_Output('OUT_TABLE', OUT_TABLE)
        Tool.Set_Output('OUT_SHAPES', OUT_SHAPES)
        Tool.Set_Option('FIELD', FIELD)
        return Tool.Execute(Verbose)
    return False

def run_tool_table_tools_20(TABLE=None, OUT_TABLE=None, OUT_SHAPES=None, FIELD=None, Verbose=2):
    '''
    Add Indicator Fields for Categories
    ----------
    [table_tools.20]\n
    Adds for each unique value found in the category field an indicator field that will show a value of one (1) for all records with this category value and zero (0) for all others. This might be used e.g. for subsequent indicator kriging.\n
    Arguments
    ----------
    - TABLE [`input table`] : Table. Input table or shapefile
    - OUT_TABLE [`output table`] : Output table with field(s) deleted
    - OUT_SHAPES [`output shapes`] : Output shapes with field(s) deleted
    - FIELD [`table field`] : Categories

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('table_tools', '20', 'Add Indicator Fields for Categories')
    if Tool.is_Okay():
        Tool.Set_Input ('TABLE', TABLE)
        Tool.Set_Output('OUT_TABLE', OUT_TABLE)
        Tool.Set_Output('OUT_SHAPES', OUT_SHAPES)
        Tool.Set_Option('FIELD', FIELD)
        return Tool.Execute(Verbose)
    return False

def Copy_Table(TABLE=None, COPY=None, Verbose=2):
    '''
    Copy Table
    ----------
    [table_tools.22]\n
    Creates a copy of a table.\n
    Arguments
    ----------
    - TABLE [`input table`] : Table
    - COPY [`output table`] : Copy

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('table_tools', '22', 'Copy Table')
    if Tool.is_Okay():
        Tool.Set_Input ('TABLE', TABLE)
        Tool.Set_Output('COPY', COPY)
        return Tool.Execute(Verbose)
    return False

def run_tool_table_tools_22(TABLE=None, COPY=None, Verbose=2):
    '''
    Copy Table
    ----------
    [table_tools.22]\n
    Creates a copy of a table.\n
    Arguments
    ----------
    - TABLE [`input table`] : Table
    - COPY [`output table`] : Copy

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('table_tools', '22', 'Copy Table')
    if Tool.is_Okay():
        Tool.Set_Input ('TABLE', TABLE)
        Tool.Set_Output('COPY', COPY)
        return Tool.Execute(Verbose)
    return False

def Change_Field_Name(TABLE=None, OUTPUT=None, FIELD=None, NAME=None, Verbose=2):
    '''
    Change Field Name
    ----------
    [table_tools.23]\n
    With this tool you can change the name of a table's attribute field.\n
    Arguments
    ----------
    - TABLE [`input table`] : Table
    - OUTPUT [`output table`] : Output
    - FIELD [`table field`] : Field
    - NAME [`text`] : Name

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('table_tools', '23', 'Change Field Name')
    if Tool.is_Okay():
        Tool.Set_Input ('TABLE', TABLE)
        Tool.Set_Output('OUTPUT', OUTPUT)
        Tool.Set_Option('FIELD', FIELD)
        Tool.Set_Option('NAME', NAME)
        return Tool.Execute(Verbose)
    return False

def run_tool_table_tools_23(TABLE=None, OUTPUT=None, FIELD=None, NAME=None, Verbose=2):
    '''
    Change Field Name
    ----------
    [table_tools.23]\n
    With this tool you can change the name of a table's attribute field.\n
    Arguments
    ----------
    - TABLE [`input table`] : Table
    - OUTPUT [`output table`] : Output
    - FIELD [`table field`] : Field
    - NAME [`text`] : Name

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('table_tools', '23', 'Change Field Name')
    if Tool.is_Okay():
        Tool.Set_Input ('TABLE', TABLE)
        Tool.Set_Output('OUTPUT', OUTPUT)
        Tool.Set_Option('FIELD', FIELD)
        Tool.Set_Option('NAME', NAME)
        return Tool.Execute(Verbose)
    return False

def Formatted_Text(TABLE=None, RESULT_TABLE=None, RESULT_SHAPES=None, FIELD=None, NAME=None, FORMAT=None, SELECTION=None, USE_NODATA=None, Verbose=2):
    '''
    Formatted Text
    ----------
    [table_tools.24]\n
    With this tool you can create new text field contents from the contents of other fields. To address other field's contents you have some format options as listed below.\n
    Fields are addressed either by their zero based column number or by their name.\n
    If the use 'no-data flag' is unchecked and a no-data value appears in a record's input fields, the result will be an empty text string.\n
    Field contents can be combined using the '+' operator. Free text arguments have to be added in quota.\n
    A simple example:\n
    '"No. " + index(1) + ": the value of '" + upper(0) + "' is " + number(1, 2)'\n
    ============\n
    [index(offset = 0)]	record's index\n
    [string(field)]	field's content as it is\n
    [lower(field)]	field's content as lower case text\n
    [upper(field)]	field's content as upper case text\n
    [integer(field)]	field's content as integer number\n
    [real(field, precision)]	field's content as real number with optional precision argument\n
    ============\n
    Arguments
    ----------
    - TABLE [`input table`] : Table
    - RESULT_TABLE [`output table`] : Result
    - RESULT_SHAPES [`output shapes`] : Result
    - FIELD [`table field`] : Field
    - NAME [`text`] : Field Name. Default: New Field
    - FORMAT [`text`] : Format. Default: "Index: " + index(1)
    - SELECTION [`boolean`] : Selection. Default: 1
    - USE_NODATA [`boolean`] : Use No-Data. Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('table_tools', '24', 'Formatted Text')
    if Tool.is_Okay():
        Tool.Set_Input ('TABLE', TABLE)
        Tool.Set_Output('RESULT_TABLE', RESULT_TABLE)
        Tool.Set_Output('RESULT_SHAPES', RESULT_SHAPES)
        Tool.Set_Option('FIELD', FIELD)
        Tool.Set_Option('NAME', NAME)
        Tool.Set_Option('FORMAT', FORMAT)
        Tool.Set_Option('SELECTION', SELECTION)
        Tool.Set_Option('USE_NODATA', USE_NODATA)
        return Tool.Execute(Verbose)
    return False

def run_tool_table_tools_24(TABLE=None, RESULT_TABLE=None, RESULT_SHAPES=None, FIELD=None, NAME=None, FORMAT=None, SELECTION=None, USE_NODATA=None, Verbose=2):
    '''
    Formatted Text
    ----------
    [table_tools.24]\n
    With this tool you can create new text field contents from the contents of other fields. To address other field's contents you have some format options as listed below.\n
    Fields are addressed either by their zero based column number or by their name.\n
    If the use 'no-data flag' is unchecked and a no-data value appears in a record's input fields, the result will be an empty text string.\n
    Field contents can be combined using the '+' operator. Free text arguments have to be added in quota.\n
    A simple example:\n
    '"No. " + index(1) + ": the value of '" + upper(0) + "' is " + number(1, 2)'\n
    ============\n
    [index(offset = 0)]	record's index\n
    [string(field)]	field's content as it is\n
    [lower(field)]	field's content as lower case text\n
    [upper(field)]	field's content as upper case text\n
    [integer(field)]	field's content as integer number\n
    [real(field, precision)]	field's content as real number with optional precision argument\n
    ============\n
    Arguments
    ----------
    - TABLE [`input table`] : Table
    - RESULT_TABLE [`output table`] : Result
    - RESULT_SHAPES [`output shapes`] : Result
    - FIELD [`table field`] : Field
    - NAME [`text`] : Field Name. Default: New Field
    - FORMAT [`text`] : Format. Default: "Index: " + index(1)
    - SELECTION [`boolean`] : Selection. Default: 1
    - USE_NODATA [`boolean`] : Use No-Data. Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('table_tools', '24', 'Formatted Text')
    if Tool.is_Okay():
        Tool.Set_Input ('TABLE', TABLE)
        Tool.Set_Output('RESULT_TABLE', RESULT_TABLE)
        Tool.Set_Output('RESULT_SHAPES', RESULT_SHAPES)
        Tool.Set_Option('FIELD', FIELD)
        Tool.Set_Option('NAME', NAME)
        Tool.Set_Option('FORMAT', FORMAT)
        Tool.Set_Option('SELECTION', SELECTION)
        Tool.Set_Option('USE_NODATA', USE_NODATA)
        return Tool.Execute(Verbose)
    return False

def Supervised_Classification_Table_Fields(TABLE=None, TRAIN_SAMPLES=None, RESULT_TABLE=None, RESULT_SHAPES=None, FEATURES=None, NORMALISE=None, TRAIN_WITH=None, TRAIN_FIELD=None, FILE_LOAD=None, FILE_SAVE=None, METHOD=None, THRESHOLD_DIST=None, THRESHOLD_ANGLE=None, THRESHOLD_PROB=None, RELATIVE_PROB=None, WTA_0=None, WTA_1=None, WTA_2=None, WTA_3=None, WTA_4=None, WTA_5=None, Verbose=2):
    '''
    Supervised Classification (Table Fields)
    ----------
    [table_tools.26]\n
    Standard classifiers for supervised classification based on attributes.\n
    Classifiers can be trained in three different ways:\n
    (-) Known classes field: choose an attribute field that provides class identifiers for those records, for which the target class is known, and no-data (or empty string) for all other records.\n
    (-) Training samples: a table with sample records providing the class identifier in the first field followed by sample data corresponding to the selected feature attributes.\n
    (-) Load statistics from file: loads feature statistics from a file that has been previously stored after training with one of the other two options.\n
    Arguments
    ----------
    - TABLE [`input table`] : Table
    - TRAIN_SAMPLES [`input table`] : Training Samples. Provide a class identifier in the first field followed by sample data corresponding to the selected feature attributes.
    - RESULT_TABLE [`output table`] : Classification
    - RESULT_SHAPES [`output shapes`] : Classification
    - FEATURES [`table fields`] : Features
    - NORMALISE [`boolean`] : Normalise. Default: 0
    - TRAIN_WITH [`choice`] : Training. Available Choices: [0] known classes field [1] training samples [2] load statistics from file Default: 0
    - TRAIN_FIELD [`table field`] : Known Classes Field
    - FILE_LOAD [`file path`] : Load Statistics from File...
    - FILE_SAVE [`file path`] : Save Statistics to File...
    - METHOD [`choice`] : Method. Available Choices: [0] Binary Encoding [1] Parallelepiped [2] Minimum Distance [3] Mahalanobis Distance [4] Maximum Likelihood [5] Spectral Angle Mapping [6] Winner Takes All Default: 2
    - THRESHOLD_DIST [`floating point number`] : Distance Threshold. Minimum: 0.000000 Default: 0.000000 Let pixel stay unclassified, if minimum euclidean or mahalanobis distance is greater than threshold.
    - THRESHOLD_ANGLE [`floating point number`] : Spectral Angle Threshold (Degree). Minimum: 0.000000 Maximum: 90.000000 Default: 0.000000 Let pixel stay unclassified, if spectral angle distance is greater than threshold.
    - THRESHOLD_PROB [`floating point number`] : Probability Threshold. Minimum: 0.000000 Maximum: 100.000000 Default: 0.000000 Let pixel stay unclassified, if maximum likelihood probability value is less than threshold.
    - RELATIVE_PROB [`choice`] : Probability Reference. Available Choices: [0] absolute [1] relative Default: 1
    - WTA_0 [`boolean`] : Binary Encoding. Default: 0
    - WTA_1 [`boolean`] : Parallelepiped. Default: 0
    - WTA_2 [`boolean`] : Minimum Distance. Default: 0
    - WTA_3 [`boolean`] : Mahalanobis Distance. Default: 0
    - WTA_4 [`boolean`] : Maximum Likelihood. Default: 0
    - WTA_5 [`boolean`] : Spectral Angle Mapping. Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('table_tools', '26', 'Supervised Classification (Table Fields)')
    if Tool.is_Okay():
        Tool.Set_Input ('TABLE', TABLE)
        Tool.Set_Input ('TRAIN_SAMPLES', TRAIN_SAMPLES)
        Tool.Set_Output('RESULT_TABLE', RESULT_TABLE)
        Tool.Set_Output('RESULT_SHAPES', RESULT_SHAPES)
        Tool.Set_Option('FEATURES', FEATURES)
        Tool.Set_Option('NORMALISE', NORMALISE)
        Tool.Set_Option('TRAIN_WITH', TRAIN_WITH)
        Tool.Set_Option('TRAIN_FIELD', TRAIN_FIELD)
        Tool.Set_Option('FILE_LOAD', FILE_LOAD)
        Tool.Set_Option('FILE_SAVE', FILE_SAVE)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('THRESHOLD_DIST', THRESHOLD_DIST)
        Tool.Set_Option('THRESHOLD_ANGLE', THRESHOLD_ANGLE)
        Tool.Set_Option('THRESHOLD_PROB', THRESHOLD_PROB)
        Tool.Set_Option('RELATIVE_PROB', RELATIVE_PROB)
        Tool.Set_Option('WTA_0', WTA_0)
        Tool.Set_Option('WTA_1', WTA_1)
        Tool.Set_Option('WTA_2', WTA_2)
        Tool.Set_Option('WTA_3', WTA_3)
        Tool.Set_Option('WTA_4', WTA_4)
        Tool.Set_Option('WTA_5', WTA_5)
        return Tool.Execute(Verbose)
    return False

def run_tool_table_tools_26(TABLE=None, TRAIN_SAMPLES=None, RESULT_TABLE=None, RESULT_SHAPES=None, FEATURES=None, NORMALISE=None, TRAIN_WITH=None, TRAIN_FIELD=None, FILE_LOAD=None, FILE_SAVE=None, METHOD=None, THRESHOLD_DIST=None, THRESHOLD_ANGLE=None, THRESHOLD_PROB=None, RELATIVE_PROB=None, WTA_0=None, WTA_1=None, WTA_2=None, WTA_3=None, WTA_4=None, WTA_5=None, Verbose=2):
    '''
    Supervised Classification (Table Fields)
    ----------
    [table_tools.26]\n
    Standard classifiers for supervised classification based on attributes.\n
    Classifiers can be trained in three different ways:\n
    (-) Known classes field: choose an attribute field that provides class identifiers for those records, for which the target class is known, and no-data (or empty string) for all other records.\n
    (-) Training samples: a table with sample records providing the class identifier in the first field followed by sample data corresponding to the selected feature attributes.\n
    (-) Load statistics from file: loads feature statistics from a file that has been previously stored after training with one of the other two options.\n
    Arguments
    ----------
    - TABLE [`input table`] : Table
    - TRAIN_SAMPLES [`input table`] : Training Samples. Provide a class identifier in the first field followed by sample data corresponding to the selected feature attributes.
    - RESULT_TABLE [`output table`] : Classification
    - RESULT_SHAPES [`output shapes`] : Classification
    - FEATURES [`table fields`] : Features
    - NORMALISE [`boolean`] : Normalise. Default: 0
    - TRAIN_WITH [`choice`] : Training. Available Choices: [0] known classes field [1] training samples [2] load statistics from file Default: 0
    - TRAIN_FIELD [`table field`] : Known Classes Field
    - FILE_LOAD [`file path`] : Load Statistics from File...
    - FILE_SAVE [`file path`] : Save Statistics to File...
    - METHOD [`choice`] : Method. Available Choices: [0] Binary Encoding [1] Parallelepiped [2] Minimum Distance [3] Mahalanobis Distance [4] Maximum Likelihood [5] Spectral Angle Mapping [6] Winner Takes All Default: 2
    - THRESHOLD_DIST [`floating point number`] : Distance Threshold. Minimum: 0.000000 Default: 0.000000 Let pixel stay unclassified, if minimum euclidean or mahalanobis distance is greater than threshold.
    - THRESHOLD_ANGLE [`floating point number`] : Spectral Angle Threshold (Degree). Minimum: 0.000000 Maximum: 90.000000 Default: 0.000000 Let pixel stay unclassified, if spectral angle distance is greater than threshold.
    - THRESHOLD_PROB [`floating point number`] : Probability Threshold. Minimum: 0.000000 Maximum: 100.000000 Default: 0.000000 Let pixel stay unclassified, if maximum likelihood probability value is less than threshold.
    - RELATIVE_PROB [`choice`] : Probability Reference. Available Choices: [0] absolute [1] relative Default: 1
    - WTA_0 [`boolean`] : Binary Encoding. Default: 0
    - WTA_1 [`boolean`] : Parallelepiped. Default: 0
    - WTA_2 [`boolean`] : Minimum Distance. Default: 0
    - WTA_3 [`boolean`] : Mahalanobis Distance. Default: 0
    - WTA_4 [`boolean`] : Maximum Likelihood. Default: 0
    - WTA_5 [`boolean`] : Spectral Angle Mapping. Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('table_tools', '26', 'Supervised Classification (Table Fields)')
    if Tool.is_Okay():
        Tool.Set_Input ('TABLE', TABLE)
        Tool.Set_Input ('TRAIN_SAMPLES', TRAIN_SAMPLES)
        Tool.Set_Output('RESULT_TABLE', RESULT_TABLE)
        Tool.Set_Output('RESULT_SHAPES', RESULT_SHAPES)
        Tool.Set_Option('FEATURES', FEATURES)
        Tool.Set_Option('NORMALISE', NORMALISE)
        Tool.Set_Option('TRAIN_WITH', TRAIN_WITH)
        Tool.Set_Option('TRAIN_FIELD', TRAIN_FIELD)
        Tool.Set_Option('FILE_LOAD', FILE_LOAD)
        Tool.Set_Option('FILE_SAVE', FILE_SAVE)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('THRESHOLD_DIST', THRESHOLD_DIST)
        Tool.Set_Option('THRESHOLD_ANGLE', THRESHOLD_ANGLE)
        Tool.Set_Option('THRESHOLD_PROB', THRESHOLD_PROB)
        Tool.Set_Option('RELATIVE_PROB', RELATIVE_PROB)
        Tool.Set_Option('WTA_0', WTA_0)
        Tool.Set_Option('WTA_1', WTA_1)
        Tool.Set_Option('WTA_2', WTA_2)
        Tool.Set_Option('WTA_3', WTA_3)
        Tool.Set_Option('WTA_4', WTA_4)
        Tool.Set_Option('WTA_5', WTA_5)
        return Tool.Execute(Verbose)
    return False

def Cluster_Analysis_Table_Fields(INPUT=None, STATISTICS=None, RESULT_TABLE=None, RESULT_SHAPES=None, FIELDS=None, NORMALISE=None, CLUSTER=None, METHOD=None, NCLUSTER=None, Verbose=2):
    '''
    Cluster Analysis (Table Fields)
    ----------
    [table_tools.28]\n
    K-Means cluster analysis using selected features from attributes table.\n
    Arguments
    ----------
    - INPUT [`input table`] : Table
    - STATISTICS [`output table`] : Statistics
    - RESULT_TABLE [`output table`] : Result
    - RESULT_SHAPES [`output shapes`] : Result
    - FIELDS [`table fields`] : Features
    - NORMALISE [`boolean`] : Normalize. Default: 0
    - CLUSTER [`table field`] : Cluster. Target field for cluster numbers. If not set a new field will be added
    - METHOD [`choice`] : Method. Available Choices: [0] Iterative Minimum Distance (Forgy 1965) [1] Hill-Climbing (Rubin 1967) [2] Combined Minimum Distance / Hillclimbing Default: 1
    - NCLUSTER [`integer number`] : Number of Clusters. Minimum: 2 Default: 10

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('table_tools', '28', 'Cluster Analysis (Table Fields)')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Output('STATISTICS', STATISTICS)
        Tool.Set_Output('RESULT_TABLE', RESULT_TABLE)
        Tool.Set_Output('RESULT_SHAPES', RESULT_SHAPES)
        Tool.Set_Option('FIELDS', FIELDS)
        Tool.Set_Option('NORMALISE', NORMALISE)
        Tool.Set_Option('CLUSTER', CLUSTER)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('NCLUSTER', NCLUSTER)
        return Tool.Execute(Verbose)
    return False

def run_tool_table_tools_28(INPUT=None, STATISTICS=None, RESULT_TABLE=None, RESULT_SHAPES=None, FIELDS=None, NORMALISE=None, CLUSTER=None, METHOD=None, NCLUSTER=None, Verbose=2):
    '''
    Cluster Analysis (Table Fields)
    ----------
    [table_tools.28]\n
    K-Means cluster analysis using selected features from attributes table.\n
    Arguments
    ----------
    - INPUT [`input table`] : Table
    - STATISTICS [`output table`] : Statistics
    - RESULT_TABLE [`output table`] : Result
    - RESULT_SHAPES [`output shapes`] : Result
    - FIELDS [`table fields`] : Features
    - NORMALISE [`boolean`] : Normalize. Default: 0
    - CLUSTER [`table field`] : Cluster. Target field for cluster numbers. If not set a new field will be added
    - METHOD [`choice`] : Method. Available Choices: [0] Iterative Minimum Distance (Forgy 1965) [1] Hill-Climbing (Rubin 1967) [2] Combined Minimum Distance / Hillclimbing Default: 1
    - NCLUSTER [`integer number`] : Number of Clusters. Minimum: 2 Default: 10

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('table_tools', '28', 'Cluster Analysis (Table Fields)')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Output('STATISTICS', STATISTICS)
        Tool.Set_Output('RESULT_TABLE', RESULT_TABLE)
        Tool.Set_Output('RESULT_SHAPES', RESULT_SHAPES)
        Tool.Set_Option('FIELDS', FIELDS)
        Tool.Set_Option('NORMALISE', NORMALISE)
        Tool.Set_Option('CLUSTER', CLUSTER)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('NCLUSTER', NCLUSTER)
        return Tool.Execute(Verbose)
    return False

