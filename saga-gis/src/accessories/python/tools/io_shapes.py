#! /usr/bin/env python

'''
Python Interface to SAGA Tools Library
----------
- Category : Import/Export
- Name     : Shapes
- ID       : io_shapes

Description
----------
Tools for the import and export of vector data.
'''

from PySAGA.helper import Tool_Wrapper

def Export_GStat_Shapes(SHAPES=None, FILENAME=None, Verbose=2):
    '''
    Export GStat Shapes
    ----------
    [io_shapes.0]\n
    GStat shapes format export.\n
    Arguments
    ----------
    - SHAPES [`input shapes`] : Shapes
    - FILENAME [`file path`] : File

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('io_shapes', '0', 'Export GStat Shapes')
    if Tool.is_Okay():
        Tool.Set_Input ('SHAPES', SHAPES)
        Tool.Set_Option('FILENAME', FILENAME)
        return Tool.Execute(Verbose)
    return False

def run_tool_io_shapes_0(SHAPES=None, FILENAME=None, Verbose=2):
    '''
    Export GStat Shapes
    ----------
    [io_shapes.0]\n
    GStat shapes format export.\n
    Arguments
    ----------
    - SHAPES [`input shapes`] : Shapes
    - FILENAME [`file path`] : File

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('io_shapes', '0', 'Export GStat Shapes')
    if Tool.is_Okay():
        Tool.Set_Input ('SHAPES', SHAPES)
        Tool.Set_Option('FILENAME', FILENAME)
        return Tool.Execute(Verbose)
    return False

def Import_GStat_Shapes(SHAPES=None, FILENAME=None, Verbose=2):
    '''
    Import GStat Shapes
    ----------
    [io_shapes.1]\n
    GStat shapes format import.\n
    Arguments
    ----------
    - SHAPES [`output shapes`] : Shapes
    - FILENAME [`file path`] : File

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('io_shapes', '1', 'Import GStat Shapes')
    if Tool.is_Okay():
        Tool.Set_Output('SHAPES', SHAPES)
        Tool.Set_Option('FILENAME', FILENAME)
        return Tool.Execute(Verbose)
    return False

def run_tool_io_shapes_1(SHAPES=None, FILENAME=None, Verbose=2):
    '''
    Import GStat Shapes
    ----------
    [io_shapes.1]\n
    GStat shapes format import.\n
    Arguments
    ----------
    - SHAPES [`output shapes`] : Shapes
    - FILENAME [`file path`] : File

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('io_shapes', '1', 'Import GStat Shapes')
    if Tool.is_Okay():
        Tool.Set_Output('SHAPES', SHAPES)
        Tool.Set_Option('FILENAME', FILENAME)
        return Tool.Execute(Verbose)
    return False

def Export_Shapes_to_XYZ(POINTS=None, FIELD=None, HEADER=None, SEPARATE=None, FILENAME=None, Verbose=2):
    '''
    Export Shapes to XYZ
    ----------
    [io_shapes.2]\n
    XYZ export filter for shapes.\n
    Arguments
    ----------
    - POINTS [`input shapes`] : Shapes
    - FIELD [`table field`] : Attribute
    - HEADER [`boolean`] : Save Table Header. Default: 1
    - SEPARATE [`choice`] : Separate Line/Polygon Points. Available Choices: [0] none [1] * [2] number of points Default: 0
    - FILENAME [`file path`] : File

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('io_shapes', '2', 'Export Shapes to XYZ')
    if Tool.is_Okay():
        Tool.Set_Input ('POINTS', POINTS)
        Tool.Set_Option('FIELD', FIELD)
        Tool.Set_Option('HEADER', HEADER)
        Tool.Set_Option('SEPARATE', SEPARATE)
        Tool.Set_Option('FILENAME', FILENAME)
        return Tool.Execute(Verbose)
    return False

def run_tool_io_shapes_2(POINTS=None, FIELD=None, HEADER=None, SEPARATE=None, FILENAME=None, Verbose=2):
    '''
    Export Shapes to XYZ
    ----------
    [io_shapes.2]\n
    XYZ export filter for shapes.\n
    Arguments
    ----------
    - POINTS [`input shapes`] : Shapes
    - FIELD [`table field`] : Attribute
    - HEADER [`boolean`] : Save Table Header. Default: 1
    - SEPARATE [`choice`] : Separate Line/Polygon Points. Available Choices: [0] none [1] * [2] number of points Default: 0
    - FILENAME [`file path`] : File

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('io_shapes', '2', 'Export Shapes to XYZ')
    if Tool.is_Okay():
        Tool.Set_Input ('POINTS', POINTS)
        Tool.Set_Option('FIELD', FIELD)
        Tool.Set_Option('HEADER', HEADER)
        Tool.Set_Option('SEPARATE', SEPARATE)
        Tool.Set_Option('FILENAME', FILENAME)
        return Tool.Execute(Verbose)
    return False

def Import_Shapes_from_XYZ(POINTS=None, HEADLINE=None, FILENAME=None, CRS_STRING=None, Verbose=2):
    '''
    Import Shapes from XYZ
    ----------
    [io_shapes.3]\n
    Imports points from a table with only list of x, y, z coordinates provided as simple text. If your table has a more complex structure, you should import it as table and then use the 'points from table' conversion tool.\n
    Arguments
    ----------
    - POINTS [`output shapes`] : Points
    - HEADLINE [`boolean`] : File contains headline. Default: 1
    - FILENAME [`file path`] : File
    - CRS_STRING [`text`] : Coordinate System Definition. Supported formats comprise PROJ and WKT strings, object codes (e.g. "EPSG:4326").

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('io_shapes', '3', 'Import Shapes from XYZ')
    if Tool.is_Okay():
        Tool.Set_Output('POINTS', POINTS)
        Tool.Set_Option('HEADLINE', HEADLINE)
        Tool.Set_Option('FILENAME', FILENAME)
        Tool.Set_Option('CRS_STRING', CRS_STRING)
        return Tool.Execute(Verbose)
    return False

def run_tool_io_shapes_3(POINTS=None, HEADLINE=None, FILENAME=None, CRS_STRING=None, Verbose=2):
    '''
    Import Shapes from XYZ
    ----------
    [io_shapes.3]\n
    Imports points from a table with only list of x, y, z coordinates provided as simple text. If your table has a more complex structure, you should import it as table and then use the 'points from table' conversion tool.\n
    Arguments
    ----------
    - POINTS [`output shapes`] : Points
    - HEADLINE [`boolean`] : File contains headline. Default: 1
    - FILENAME [`file path`] : File
    - CRS_STRING [`text`] : Coordinate System Definition. Supported formats comprise PROJ and WKT strings, object codes (e.g. "EPSG:4326").

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('io_shapes', '3', 'Import Shapes from XYZ')
    if Tool.is_Okay():
        Tool.Set_Output('POINTS', POINTS)
        Tool.Set_Option('HEADLINE', HEADLINE)
        Tool.Set_Option('FILENAME', FILENAME)
        Tool.Set_Option('CRS_STRING', CRS_STRING)
        return Tool.Execute(Verbose)
    return False

def Export_Shapes_to_Generate(SHAPES=None, FIELD=None, FILE=None, Verbose=2):
    '''
    Export Shapes to Generate
    ----------
    [io_shapes.4]\n
    Export generate shapes format.\n
    Arguments
    ----------
    - SHAPES [`input shapes`] : Shapes
    - FIELD [`table field`] : Attribute
    - FILE [`file path`] : File

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('io_shapes', '4', 'Export Shapes to Generate')
    if Tool.is_Okay():
        Tool.Set_Input ('SHAPES', SHAPES)
        Tool.Set_Option('FIELD', FIELD)
        Tool.Set_Option('FILE', FILE)
        return Tool.Execute(Verbose)
    return False

def run_tool_io_shapes_4(SHAPES=None, FIELD=None, FILE=None, Verbose=2):
    '''
    Export Shapes to Generate
    ----------
    [io_shapes.4]\n
    Export generate shapes format.\n
    Arguments
    ----------
    - SHAPES [`input shapes`] : Shapes
    - FIELD [`table field`] : Attribute
    - FILE [`file path`] : File

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('io_shapes', '4', 'Export Shapes to Generate')
    if Tool.is_Okay():
        Tool.Set_Input ('SHAPES', SHAPES)
        Tool.Set_Option('FIELD', FIELD)
        Tool.Set_Option('FILE', FILE)
        return Tool.Execute(Verbose)
    return False

def Export_Surfer_Blanking_File(SHAPES=None, NAME=None, DESC=None, ZVAL=None, FILE=None, Verbose=2):
    '''
    Export Surfer Blanking File
    ----------
    [io_shapes.5]\n
    Export shapes to Golden Software's Surfer Blanking File format.\n
    Arguments
    ----------
    - SHAPES [`input shapes`] : Shapes
    - NAME [`table field`] : Name
    - DESC [`table field`] : Description
    - ZVAL [`table field`] : z values
    - FILE [`file path`] : File

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('io_shapes', '5', 'Export Surfer Blanking File')
    if Tool.is_Okay():
        Tool.Set_Input ('SHAPES', SHAPES)
        Tool.Set_Option('NAME', NAME)
        Tool.Set_Option('DESC', DESC)
        Tool.Set_Option('ZVAL', ZVAL)
        Tool.Set_Option('FILE', FILE)
        return Tool.Execute(Verbose)
    return False

def run_tool_io_shapes_5(SHAPES=None, NAME=None, DESC=None, ZVAL=None, FILE=None, Verbose=2):
    '''
    Export Surfer Blanking File
    ----------
    [io_shapes.5]\n
    Export shapes to Golden Software's Surfer Blanking File format.\n
    Arguments
    ----------
    - SHAPES [`input shapes`] : Shapes
    - NAME [`table field`] : Name
    - DESC [`table field`] : Description
    - ZVAL [`table field`] : z values
    - FILE [`file path`] : File

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('io_shapes', '5', 'Export Surfer Blanking File')
    if Tool.is_Okay():
        Tool.Set_Input ('SHAPES', SHAPES)
        Tool.Set_Option('NAME', NAME)
        Tool.Set_Option('DESC', DESC)
        Tool.Set_Option('ZVAL', ZVAL)
        Tool.Set_Option('FILE', FILE)
        return Tool.Execute(Verbose)
    return False

def Import_Surfer_Blanking_Files(SHAPES=None, TABLE=None, FILE=None, TYPE=None, Verbose=2):
    '''
    Import Surfer Blanking Files
    ----------
    [io_shapes.6]\n
    Import polygons/polylines from Golden Software's Surfer Blanking File format.\n
    Arguments
    ----------
    - SHAPES [`output shapes`] : Shapes
    - TABLE [`output table`] : Look up table (Points)
    - FILE [`file path`] : File
    - TYPE [`choice`] : Shape Type. Available Choices: [0] points [1] lines [2] polygons Default: 1

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('io_shapes', '6', 'Import Surfer Blanking Files')
    if Tool.is_Okay():
        Tool.Set_Output('SHAPES', SHAPES)
        Tool.Set_Output('TABLE', TABLE)
        Tool.Set_Option('FILE', FILE)
        Tool.Set_Option('TYPE', TYPE)
        return Tool.Execute(Verbose)
    return False

def run_tool_io_shapes_6(SHAPES=None, TABLE=None, FILE=None, TYPE=None, Verbose=2):
    '''
    Import Surfer Blanking Files
    ----------
    [io_shapes.6]\n
    Import polygons/polylines from Golden Software's Surfer Blanking File format.\n
    Arguments
    ----------
    - SHAPES [`output shapes`] : Shapes
    - TABLE [`output table`] : Look up table (Points)
    - FILE [`file path`] : File
    - TYPE [`choice`] : Shape Type. Available Choices: [0] points [1] lines [2] polygons Default: 1

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('io_shapes', '6', 'Import Surfer Blanking Files')
    if Tool.is_Okay():
        Tool.Set_Output('SHAPES', SHAPES)
        Tool.Set_Output('TABLE', TABLE)
        Tool.Set_Option('FILE', FILE)
        Tool.Set_Option('TYPE', TYPE)
        return Tool.Execute(Verbose)
    return False

def Export_Atlas_Boundary_File(SHAPES=None, PNAME=None, SNAME=None, FILE=None, Verbose=2):
    '''
    Export Atlas Boundary File
    ----------
    [io_shapes.7]\n
    Export Atlas Boundary File\n
    Arguments
    ----------
    - SHAPES [`input shapes`] : Shapes
    - PNAME [`table field`] : Primary Name
    - SNAME [`table field`] : Secondary Name
    - FILE [`file path`] : File

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('io_shapes', '7', 'Export Atlas Boundary File')
    if Tool.is_Okay():
        Tool.Set_Input ('SHAPES', SHAPES)
        Tool.Set_Option('PNAME', PNAME)
        Tool.Set_Option('SNAME', SNAME)
        Tool.Set_Option('FILE', FILE)
        return Tool.Execute(Verbose)
    return False

def run_tool_io_shapes_7(SHAPES=None, PNAME=None, SNAME=None, FILE=None, Verbose=2):
    '''
    Export Atlas Boundary File
    ----------
    [io_shapes.7]\n
    Export Atlas Boundary File\n
    Arguments
    ----------
    - SHAPES [`input shapes`] : Shapes
    - PNAME [`table field`] : Primary Name
    - SNAME [`table field`] : Secondary Name
    - FILE [`file path`] : File

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('io_shapes', '7', 'Export Atlas Boundary File')
    if Tool.is_Okay():
        Tool.Set_Input ('SHAPES', SHAPES)
        Tool.Set_Option('PNAME', PNAME)
        Tool.Set_Option('SNAME', SNAME)
        Tool.Set_Option('FILE', FILE)
        return Tool.Execute(Verbose)
    return False

def Import_Atlas_Boundary_File(FILE=None, Verbose=2):
    '''
    Import Atlas Boundary File
    ----------
    [io_shapes.8]\n
    Import Atlas Boundary File\n
    Arguments
    ----------
    - FILE [`file path`] : File

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('io_shapes', '8', 'Import Atlas Boundary File')
    if Tool.is_Okay():
        Tool.Set_Option('FILE', FILE)
        return Tool.Execute(Verbose)
    return False

def run_tool_io_shapes_8(FILE=None, Verbose=2):
    '''
    Import Atlas Boundary File
    ----------
    [io_shapes.8]\n
    Import Atlas Boundary File\n
    Arguments
    ----------
    - FILE [`file path`] : File

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('io_shapes', '8', 'Import Atlas Boundary File')
    if Tool.is_Okay():
        Tool.Set_Option('FILE', FILE)
        return Tool.Execute(Verbose)
    return False

def Export_WASP_terrain_map_file(SHAPES=None, ELEVATION=None, FILE=None, Verbose=2):
    '''
    Export WASP terrain map file
    ----------
    [io_shapes.9]\n
    Export WAsP (Wind Atlas Analysis and Application Program) terrain map file\n
    Arguments
    ----------
    - SHAPES [`input shapes`] : Contour Lines
    - ELEVATION [`table field`] : Map File
    - FILE [`file path`] : File Name

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('io_shapes', '9', 'Export WASP terrain map file')
    if Tool.is_Okay():
        Tool.Set_Input ('SHAPES', SHAPES)
        Tool.Set_Option('ELEVATION', ELEVATION)
        Tool.Set_Option('FILE', FILE)
        return Tool.Execute(Verbose)
    return False

def run_tool_io_shapes_9(SHAPES=None, ELEVATION=None, FILE=None, Verbose=2):
    '''
    Export WASP terrain map file
    ----------
    [io_shapes.9]\n
    Export WAsP (Wind Atlas Analysis and Application Program) terrain map file\n
    Arguments
    ----------
    - SHAPES [`input shapes`] : Contour Lines
    - ELEVATION [`table field`] : Map File
    - FILE [`file path`] : File Name

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('io_shapes', '9', 'Export WASP terrain map file')
    if Tool.is_Okay():
        Tool.Set_Input ('SHAPES', SHAPES)
        Tool.Set_Option('ELEVATION', ELEVATION)
        Tool.Set_Option('FILE', FILE)
        return Tool.Execute(Verbose)
    return False

def Import_WASP_terrain_map_file(SHAPES=None, FILE=None, METHOD=None, Verbose=2):
    '''
    Import WASP terrain map file
    ----------
    [io_shapes.10]\n
    Import WAsP (Wind Atlas Analysis and Application Program) terrain map file\n
    Arguments
    ----------
    - SHAPES [`output shapes`] : Contour Lines
    - FILE [`file path`] : File Name
    - METHOD [`choice`] : Input Specification. Available Choices: [0] elevation [1] roughness [2] elevation and roughness Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('io_shapes', '10', 'Import WASP terrain map file')
    if Tool.is_Okay():
        Tool.Set_Output('SHAPES', SHAPES)
        Tool.Set_Option('FILE', FILE)
        Tool.Set_Option('METHOD', METHOD)
        return Tool.Execute(Verbose)
    return False

def run_tool_io_shapes_10(SHAPES=None, FILE=None, METHOD=None, Verbose=2):
    '''
    Import WASP terrain map file
    ----------
    [io_shapes.10]\n
    Import WAsP (Wind Atlas Analysis and Application Program) terrain map file\n
    Arguments
    ----------
    - SHAPES [`output shapes`] : Contour Lines
    - FILE [`file path`] : File Name
    - METHOD [`choice`] : Input Specification. Available Choices: [0] elevation [1] roughness [2] elevation and roughness Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('io_shapes', '10', 'Import WASP terrain map file')
    if Tool.is_Okay():
        Tool.Set_Output('SHAPES', SHAPES)
        Tool.Set_Option('FILE', FILE)
        Tool.Set_Option('METHOD', METHOD)
        return Tool.Execute(Verbose)
    return False

def Import_Stereo_Lithography_File_STL(POINTS=None, POLYGONS=None, GRID=None, TIN=None, FILE=None, METHOD=None, CENTROIDS=None, DUPLICATES=None, GRID_DIM=None, GRID_WIDTH=None, GRID_SIZE=None, ROTATE=None, ROT_X=None, ROT_Y=None, ROT_Z=None, Verbose=2):
    '''
    Import Stereo Lithography File (STL)
    ----------
    [io_shapes.11]\n
    An StL ("StereoLithography") file is a triangular representation of a 3-dimensional surface geometry. The surface is tessellated or broken down logically into a series of small triangles (facets). Each facet is described by a perpendicular direction and three points representing the vertices (corners) of the triangle (Ennex Research Corporation). The StL file format is commonly used for 3D printing.\n
    Arguments
    ----------
    - POINTS [`output point cloud`] : Points
    - POLYGONS [`output shapes`] : Polygons
    - GRID [`output data object`] : Grid
    - TIN [`output TIN`] : TIN
    - FILE [`file path`] : File
    - METHOD [`choice`] : Target. Available Choices: [0] Points [1] Polygons [2] TIN [3] Grid Default: 2
    - CENTROIDS [`boolean`] : Centroids. Default: 0
    - DUPLICATES [`boolean`] : Remove Duplicates. Default: 1
    - GRID_DIM [`choice`] : Grid Dimension. Available Choices: [0] Width [1] Cellsize Default: 0
    - GRID_WIDTH [`integer number`] : Width. Minimum: 10 Default: 2000 Number of cells.
    - GRID_SIZE [`floating point number`] : Cellsize. Minimum: 0.000000 Default: 1.000000
    - ROTATE [`boolean`] : Rotation. Default: 0
    - ROT_X [`floating point number`] : X Axis. Default: 0.000000
    - ROT_Y [`floating point number`] : Y Axis. Default: 0.000000
    - ROT_Z [`floating point number`] : Z Axis. Default: 0.000000

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('io_shapes', '11', 'Import Stereo Lithography File (STL)')
    if Tool.is_Okay():
        Tool.Set_Output('POINTS', POINTS)
        Tool.Set_Output('POLYGONS', POLYGONS)
        Tool.Set_Output('GRID', GRID)
        Tool.Set_Output('TIN', TIN)
        Tool.Set_Option('FILE', FILE)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('CENTROIDS', CENTROIDS)
        Tool.Set_Option('DUPLICATES', DUPLICATES)
        Tool.Set_Option('GRID_DIM', GRID_DIM)
        Tool.Set_Option('GRID_WIDTH', GRID_WIDTH)
        Tool.Set_Option('GRID_SIZE', GRID_SIZE)
        Tool.Set_Option('ROTATE', ROTATE)
        Tool.Set_Option('ROT_X', ROT_X)
        Tool.Set_Option('ROT_Y', ROT_Y)
        Tool.Set_Option('ROT_Z', ROT_Z)
        return Tool.Execute(Verbose)
    return False

def run_tool_io_shapes_11(POINTS=None, POLYGONS=None, GRID=None, TIN=None, FILE=None, METHOD=None, CENTROIDS=None, DUPLICATES=None, GRID_DIM=None, GRID_WIDTH=None, GRID_SIZE=None, ROTATE=None, ROT_X=None, ROT_Y=None, ROT_Z=None, Verbose=2):
    '''
    Import Stereo Lithography File (STL)
    ----------
    [io_shapes.11]\n
    An StL ("StereoLithography") file is a triangular representation of a 3-dimensional surface geometry. The surface is tessellated or broken down logically into a series of small triangles (facets). Each facet is described by a perpendicular direction and three points representing the vertices (corners) of the triangle (Ennex Research Corporation). The StL file format is commonly used for 3D printing.\n
    Arguments
    ----------
    - POINTS [`output point cloud`] : Points
    - POLYGONS [`output shapes`] : Polygons
    - GRID [`output data object`] : Grid
    - TIN [`output TIN`] : TIN
    - FILE [`file path`] : File
    - METHOD [`choice`] : Target. Available Choices: [0] Points [1] Polygons [2] TIN [3] Grid Default: 2
    - CENTROIDS [`boolean`] : Centroids. Default: 0
    - DUPLICATES [`boolean`] : Remove Duplicates. Default: 1
    - GRID_DIM [`choice`] : Grid Dimension. Available Choices: [0] Width [1] Cellsize Default: 0
    - GRID_WIDTH [`integer number`] : Width. Minimum: 10 Default: 2000 Number of cells.
    - GRID_SIZE [`floating point number`] : Cellsize. Minimum: 0.000000 Default: 1.000000
    - ROTATE [`boolean`] : Rotation. Default: 0
    - ROT_X [`floating point number`] : X Axis. Default: 0.000000
    - ROT_Y [`floating point number`] : Y Axis. Default: 0.000000
    - ROT_Z [`floating point number`] : Z Axis. Default: 0.000000

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('io_shapes', '11', 'Import Stereo Lithography File (STL)')
    if Tool.is_Okay():
        Tool.Set_Output('POINTS', POINTS)
        Tool.Set_Output('POLYGONS', POLYGONS)
        Tool.Set_Output('GRID', GRID)
        Tool.Set_Output('TIN', TIN)
        Tool.Set_Option('FILE', FILE)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('CENTROIDS', CENTROIDS)
        Tool.Set_Option('DUPLICATES', DUPLICATES)
        Tool.Set_Option('GRID_DIM', GRID_DIM)
        Tool.Set_Option('GRID_WIDTH', GRID_WIDTH)
        Tool.Set_Option('GRID_SIZE', GRID_SIZE)
        Tool.Set_Option('ROTATE', ROTATE)
        Tool.Set_Option('ROT_X', ROT_X)
        Tool.Set_Option('ROT_Y', ROT_Y)
        Tool.Set_Option('ROT_Z', ROT_Z)
        return Tool.Execute(Verbose)
    return False

def Export_TIN_to_Stereo_Lithography_File_STL(TIN=None, ZFIELD=None, FILE=None, BINARY=None, Verbose=2):
    '''
    Export TIN to Stereo Lithography File (STL)
    ----------
    [io_shapes.12]\n
    An StL ("StereoLithography") file is a triangular representation of a 3-dimensional surface geometry. The surface is tessellated or broken down logically into a series of small triangles (facets). Each facet is described by a perpendicular direction and three points representing the vertices (corners) of the triangle (Ennex Research Corporation). The StL file format is commonly used for 3D printing.\n
    Arguments
    ----------
    - TIN [`input TIN`] : TIN
    - ZFIELD [`table field`] : Attribute
    - FILE [`file path`] : File
    - BINARY [`choice`] : Output Type. Available Choices: [0] ASCII [1] binary Default: 1

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('io_shapes', '12', 'Export TIN to Stereo Lithography File (STL)')
    if Tool.is_Okay():
        Tool.Set_Input ('TIN', TIN)
        Tool.Set_Option('ZFIELD', ZFIELD)
        Tool.Set_Option('FILE', FILE)
        Tool.Set_Option('BINARY', BINARY)
        return Tool.Execute(Verbose)
    return False

def run_tool_io_shapes_12(TIN=None, ZFIELD=None, FILE=None, BINARY=None, Verbose=2):
    '''
    Export TIN to Stereo Lithography File (STL)
    ----------
    [io_shapes.12]\n
    An StL ("StereoLithography") file is a triangular representation of a 3-dimensional surface geometry. The surface is tessellated or broken down logically into a series of small triangles (facets). Each facet is described by a perpendicular direction and three points representing the vertices (corners) of the triangle (Ennex Research Corporation). The StL file format is commonly used for 3D printing.\n
    Arguments
    ----------
    - TIN [`input TIN`] : TIN
    - ZFIELD [`table field`] : Attribute
    - FILE [`file path`] : File
    - BINARY [`choice`] : Output Type. Available Choices: [0] ASCII [1] binary Default: 1

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('io_shapes', '12', 'Export TIN to Stereo Lithography File (STL)')
    if Tool.is_Okay():
        Tool.Set_Input ('TIN', TIN)
        Tool.Set_Option('ZFIELD', ZFIELD)
        Tool.Set_Option('FILE', FILE)
        Tool.Set_Option('BINARY', BINARY)
        return Tool.Execute(Verbose)
    return False

def Import_GPX(SHAPES=None, FILE=None, TIME=None, Verbose=2):
    '''
    Import GPX
    ----------
    [io_shapes.13]\n
    Imports GPS data from GPS eXchange format GPX.\n
    References:\n
    [The GPS Exchange Format](http://www.topografix.com/)\n
    Arguments
    ----------
    - SHAPES [`output shapes list`] : GPX Import
    - FILE [`file path`] : File
    - TIME [`boolean`] : Time Stamp without date. Default: 1

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('io_shapes', '13', 'Import GPX')
    if Tool.is_Okay():
        Tool.Set_Output('SHAPES', SHAPES)
        Tool.Set_Option('FILE', FILE)
        Tool.Set_Option('TIME', TIME)
        return Tool.Execute(Verbose)
    return False

def run_tool_io_shapes_13(SHAPES=None, FILE=None, TIME=None, Verbose=2):
    '''
    Import GPX
    ----------
    [io_shapes.13]\n
    Imports GPS data from GPS eXchange format GPX.\n
    References:\n
    [The GPS Exchange Format](http://www.topografix.com/)\n
    Arguments
    ----------
    - SHAPES [`output shapes list`] : GPX Import
    - FILE [`file path`] : File
    - TIME [`boolean`] : Time Stamp without date. Default: 1

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('io_shapes', '13', 'Import GPX')
    if Tool.is_Okay():
        Tool.Set_Output('SHAPES', SHAPES)
        Tool.Set_Option('FILE', FILE)
        Tool.Set_Option('TIME', TIME)
        return Tool.Execute(Verbose)
    return False

def Export_GPX(SHAPES=None, FILE=None, ELE=None, NAME=None, CMT=None, DESC=None, Verbose=2):
    '''
    Export GPX
    ----------
    [io_shapes.14]\n
    Exports vector data points to GPS eXchange format GPX.\n
    References:\n
    [The GPS Exchange Format](http://www.topografix.com/)\n
    Arguments
    ----------
    - SHAPES [`input shapes`] : Shapes
    - FILE [`file path`] : File
    - ELE [`table field`] : Elevation
    - NAME [`table field`] : Name
    - CMT [`table field`] : Comment
    - DESC [`table field`] : Description

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('io_shapes', '14', 'Export GPX')
    if Tool.is_Okay():
        Tool.Set_Input ('SHAPES', SHAPES)
        Tool.Set_Option('FILE', FILE)
        Tool.Set_Option('ELE', ELE)
        Tool.Set_Option('NAME', NAME)
        Tool.Set_Option('CMT', CMT)
        Tool.Set_Option('DESC', DESC)
        return Tool.Execute(Verbose)
    return False

def run_tool_io_shapes_14(SHAPES=None, FILE=None, ELE=None, NAME=None, CMT=None, DESC=None, Verbose=2):
    '''
    Export GPX
    ----------
    [io_shapes.14]\n
    Exports vector data points to GPS eXchange format GPX.\n
    References:\n
    [The GPS Exchange Format](http://www.topografix.com/)\n
    Arguments
    ----------
    - SHAPES [`input shapes`] : Shapes
    - FILE [`file path`] : File
    - ELE [`table field`] : Elevation
    - NAME [`table field`] : Name
    - CMT [`table field`] : Comment
    - DESC [`table field`] : Description

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('io_shapes', '14', 'Export GPX')
    if Tool.is_Okay():
        Tool.Set_Input ('SHAPES', SHAPES)
        Tool.Set_Option('FILE', FILE)
        Tool.Set_Option('ELE', ELE)
        Tool.Set_Option('NAME', NAME)
        Tool.Set_Option('CMT', CMT)
        Tool.Set_Option('DESC', DESC)
        return Tool.Execute(Verbose)
    return False

def Import_Point_Cloud_from_Shape_File(POINTS=None, FILE=None, Verbose=2):
    '''
    Import Point Cloud from Shape File
    ----------
    [io_shapes.15]\n
    Imports a point cloud from a point shapefile.\n
    Arguments
    ----------
    - POINTS [`output point cloud`] : Point Cloud
    - FILE [`file path`] : File

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('io_shapes', '15', 'Import Point Cloud from Shape File')
    if Tool.is_Okay():
        Tool.Set_Output('POINTS', POINTS)
        Tool.Set_Option('FILE', FILE)
        return Tool.Execute(Verbose)
    return False

def run_tool_io_shapes_15(POINTS=None, FILE=None, Verbose=2):
    '''
    Import Point Cloud from Shape File
    ----------
    [io_shapes.15]\n
    Imports a point cloud from a point shapefile.\n
    Arguments
    ----------
    - POINTS [`output point cloud`] : Point Cloud
    - FILE [`file path`] : File

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('io_shapes', '15', 'Import Point Cloud from Shape File')
    if Tool.is_Okay():
        Tool.Set_Output('POINTS', POINTS)
        Tool.Set_Option('FILE', FILE)
        return Tool.Execute(Verbose)
    return False

def Import_Point_Cloud_from_Text_File(POINTS=None, FILE=None, SEPARATOR=None, SKIP_HEADER=None, XFIELD=None, YFIELD=None, ZFIELD=None, FIELDS=None, FIELDNAMES=None, FIELDTYPES=None, CRS_STRING=None, Verbose=2):
    '''
    Import Point Cloud from Text File
    ----------
    [io_shapes.16]\n
    Creates a point cloud from a text file.\n
    The input file must have at least three columns holding the x, y, z coordinates of each point. You must specify the field index (i.e. the column) of these. Field index starts to count with 1. In case you like to import additional attributes, you have to provide the field indexes for those attributes with the -FIELDS option as integer numbers separated by semicolon (e.g. "-FIELDS=4;5;8").\n
    You have also to select the field separator that is used by the file and if the first line of the file should be skipped (in case it contains column headings).\n
    The columns in the input file can be in any order, and you can omit columns, but you have to provide the correct field index for those fields that you like to import.\n
    The tool usage differs slightly between SAGA GUI and SAGA CMD. With SAGA GUI you can specify names and types for additional fields in the 'Specifications' sub dialog. To do this using SAGA CMD you have to use the -FIELDNAMES and -FIELDTYPES options. The first one is for the field names, the second for the data type specification (see the GUI which number equals which data type). Again entries have to be separated by semicolons, e.g. "-FIELDNAMES=intensity;class;range -FIELDTYPES=2;2;3".\n
    Arguments
    ----------
    - POINTS [`output point cloud`] : Point Cloud
    - FILE [`file path`] : Text File
    - SEPARATOR [`choice`] : Field Separator. Available Choices: [0] tabulator [1] space [2] comma [3] semicolon Default: 0 Field Separator
    - SKIP_HEADER [`boolean`] : Skip first line. Default: 0 Skip first line as it contains column names.
    - XFIELD [`integer number`] : X is Column .... Minimum: 1 Default: 1 The column holding the X-coordinate.
    - YFIELD [`integer number`] : Y is Column .... Minimum: 1 Default: 2 The column holding the Y-coordinate.
    - ZFIELD [`integer number`] : Z is Column .... Minimum: 1 Default: 3 The column holding the Z-coordinate.
    - FIELDS [`text`] : Fields. The index (starting with 1) of the fields to import, separated by semicolon, e.g. "5;6;8"
    - FIELDNAMES [`text`] : Field Names. The name to use for each field, separated by semicolon, e.g. "intensity;class;range"
    - FIELDTYPES [`text`] : Field Types. The datatype to use for each field, separated by semicolon, e.g. "2;2;3;". The number equals the choice selection, see GUI version.
    - CRS_STRING [`text`] : Coordinate System Definition. Supported formats comprise PROJ and WKT strings, object codes (e.g. "EPSG:4326").

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('io_shapes', '16', 'Import Point Cloud from Text File')
    if Tool.is_Okay():
        Tool.Set_Output('POINTS', POINTS)
        Tool.Set_Option('FILE', FILE)
        Tool.Set_Option('SEPARATOR', SEPARATOR)
        Tool.Set_Option('SKIP_HEADER', SKIP_HEADER)
        Tool.Set_Option('XFIELD', XFIELD)
        Tool.Set_Option('YFIELD', YFIELD)
        Tool.Set_Option('ZFIELD', ZFIELD)
        Tool.Set_Option('FIELDS', FIELDS)
        Tool.Set_Option('FIELDNAMES', FIELDNAMES)
        Tool.Set_Option('FIELDTYPES', FIELDTYPES)
        Tool.Set_Option('CRS_STRING', CRS_STRING)
        return Tool.Execute(Verbose)
    return False

def run_tool_io_shapes_16(POINTS=None, FILE=None, SEPARATOR=None, SKIP_HEADER=None, XFIELD=None, YFIELD=None, ZFIELD=None, FIELDS=None, FIELDNAMES=None, FIELDTYPES=None, CRS_STRING=None, Verbose=2):
    '''
    Import Point Cloud from Text File
    ----------
    [io_shapes.16]\n
    Creates a point cloud from a text file.\n
    The input file must have at least three columns holding the x, y, z coordinates of each point. You must specify the field index (i.e. the column) of these. Field index starts to count with 1. In case you like to import additional attributes, you have to provide the field indexes for those attributes with the -FIELDS option as integer numbers separated by semicolon (e.g. "-FIELDS=4;5;8").\n
    You have also to select the field separator that is used by the file and if the first line of the file should be skipped (in case it contains column headings).\n
    The columns in the input file can be in any order, and you can omit columns, but you have to provide the correct field index for those fields that you like to import.\n
    The tool usage differs slightly between SAGA GUI and SAGA CMD. With SAGA GUI you can specify names and types for additional fields in the 'Specifications' sub dialog. To do this using SAGA CMD you have to use the -FIELDNAMES and -FIELDTYPES options. The first one is for the field names, the second for the data type specification (see the GUI which number equals which data type). Again entries have to be separated by semicolons, e.g. "-FIELDNAMES=intensity;class;range -FIELDTYPES=2;2;3".\n
    Arguments
    ----------
    - POINTS [`output point cloud`] : Point Cloud
    - FILE [`file path`] : Text File
    - SEPARATOR [`choice`] : Field Separator. Available Choices: [0] tabulator [1] space [2] comma [3] semicolon Default: 0 Field Separator
    - SKIP_HEADER [`boolean`] : Skip first line. Default: 0 Skip first line as it contains column names.
    - XFIELD [`integer number`] : X is Column .... Minimum: 1 Default: 1 The column holding the X-coordinate.
    - YFIELD [`integer number`] : Y is Column .... Minimum: 1 Default: 2 The column holding the Y-coordinate.
    - ZFIELD [`integer number`] : Z is Column .... Minimum: 1 Default: 3 The column holding the Z-coordinate.
    - FIELDS [`text`] : Fields. The index (starting with 1) of the fields to import, separated by semicolon, e.g. "5;6;8"
    - FIELDNAMES [`text`] : Field Names. The name to use for each field, separated by semicolon, e.g. "intensity;class;range"
    - FIELDTYPES [`text`] : Field Types. The datatype to use for each field, separated by semicolon, e.g. "2;2;3;". The number equals the choice selection, see GUI version.
    - CRS_STRING [`text`] : Coordinate System Definition. Supported formats comprise PROJ and WKT strings, object codes (e.g. "EPSG:4326").

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('io_shapes', '16', 'Import Point Cloud from Text File')
    if Tool.is_Okay():
        Tool.Set_Output('POINTS', POINTS)
        Tool.Set_Option('FILE', FILE)
        Tool.Set_Option('SEPARATOR', SEPARATOR)
        Tool.Set_Option('SKIP_HEADER', SKIP_HEADER)
        Tool.Set_Option('XFIELD', XFIELD)
        Tool.Set_Option('YFIELD', YFIELD)
        Tool.Set_Option('ZFIELD', ZFIELD)
        Tool.Set_Option('FIELDS', FIELDS)
        Tool.Set_Option('FIELDNAMES', FIELDNAMES)
        Tool.Set_Option('FIELDTYPES', FIELDTYPES)
        Tool.Set_Option('CRS_STRING', CRS_STRING)
        return Tool.Execute(Verbose)
    return False

def Export_Scalable_Vector_Graphics_SVG_File(LAYERS=None, LAYER=None, OUTPUT=None, FIELD=None, FILE=None, Verbose=2):
    '''
    Export Scalable Vector Graphics (SVG) File
    ----------
    [io_shapes.17]\n
    Export shapes to Scalable Vector Graphics (SVG) File.\n
    Arguments
    ----------
    - LAYERS [`input shapes list`] : Layers
    - LAYER [`input shapes`] : Layer
    - OUTPUT [`choice`] : Output. Available Choices: [0] single layer [1] multiple layers Default: 1
    - FIELD [`table field`] : Attribute
    - FILE [`file path`] : File

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('io_shapes', '17', 'Export Scalable Vector Graphics (SVG) File')
    if Tool.is_Okay():
        Tool.Set_Input ('LAYERS', LAYERS)
        Tool.Set_Input ('LAYER', LAYER)
        Tool.Set_Option('OUTPUT', OUTPUT)
        Tool.Set_Option('FIELD', FIELD)
        Tool.Set_Option('FILE', FILE)
        return Tool.Execute(Verbose)
    return False

def run_tool_io_shapes_17(LAYERS=None, LAYER=None, OUTPUT=None, FIELD=None, FILE=None, Verbose=2):
    '''
    Export Scalable Vector Graphics (SVG) File
    ----------
    [io_shapes.17]\n
    Export shapes to Scalable Vector Graphics (SVG) File.\n
    Arguments
    ----------
    - LAYERS [`input shapes list`] : Layers
    - LAYER [`input shapes`] : Layer
    - OUTPUT [`choice`] : Output. Available Choices: [0] single layer [1] multiple layers Default: 1
    - FIELD [`table field`] : Attribute
    - FILE [`file path`] : File

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('io_shapes', '17', 'Export Scalable Vector Graphics (SVG) File')
    if Tool.is_Okay():
        Tool.Set_Input ('LAYERS', LAYERS)
        Tool.Set_Input ('LAYER', LAYER)
        Tool.Set_Option('OUTPUT', OUTPUT)
        Tool.Set_Option('FIELD', FIELD)
        Tool.Set_Option('FILE', FILE)
        return Tool.Execute(Verbose)
    return False

def Export_Point_Cloud_to_Text_File(POINTS=None, FILE=None, WRITE_HEADER=None, FIELDSEP=None, FIELDS=None, PRECISIONS=None, Verbose=2):
    '''
    Export Point Cloud to Text File
    ----------
    [io_shapes.18]\n
    Exports a point cloud to a text file. Once the tool is executed, a pop-up dialog allows one to specify the fields to be exported and their decimal precision.\n
    Tool usage is different between SAGA GUI and SAGA CMD: With SAGA GUI you will get prompted to choose the fields to export and the decimal precisions to use once you execute the tool. With SAGA CMD you have to provide two strings with the -FIELDS and -PRECISIONS parameters. The first one must contain the field numbers, the latter the precisions (separated by semicolon). Field numbers start with 1, e.g. -FIELDS="1;2;3;5" -PRECISIONS="2;2;2;0".\n
    Arguments
    ----------
    - POINTS [`input point cloud`] : Point Cloud. The point cloud to export.
    - FILE [`file path`] : Text File. The file to write the point cloud to.
    - WRITE_HEADER [`boolean`] : Write Header. Default: 0 Write column names.
    - FIELDSEP [`choice`] : Field Separator. Available Choices: [0] tabulator [1] space [2] comma Default: 0 Field Separator
    - FIELDS [`text`] : Fields. The numbers (starting from 1) of the fields to export, separated by semicolon, e.g. "1;2;3;5"
    - PRECISIONS [`text`] : Precisions. The decimal precision to use for each field, separated by semicolon, e.g. "2;2;2;0"

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('io_shapes', '18', 'Export Point Cloud to Text File')
    if Tool.is_Okay():
        Tool.Set_Input ('POINTS', POINTS)
        Tool.Set_Option('FILE', FILE)
        Tool.Set_Option('WRITE_HEADER', WRITE_HEADER)
        Tool.Set_Option('FIELDSEP', FIELDSEP)
        Tool.Set_Option('FIELDS', FIELDS)
        Tool.Set_Option('PRECISIONS', PRECISIONS)
        return Tool.Execute(Verbose)
    return False

def run_tool_io_shapes_18(POINTS=None, FILE=None, WRITE_HEADER=None, FIELDSEP=None, FIELDS=None, PRECISIONS=None, Verbose=2):
    '''
    Export Point Cloud to Text File
    ----------
    [io_shapes.18]\n
    Exports a point cloud to a text file. Once the tool is executed, a pop-up dialog allows one to specify the fields to be exported and their decimal precision.\n
    Tool usage is different between SAGA GUI and SAGA CMD: With SAGA GUI you will get prompted to choose the fields to export and the decimal precisions to use once you execute the tool. With SAGA CMD you have to provide two strings with the -FIELDS and -PRECISIONS parameters. The first one must contain the field numbers, the latter the precisions (separated by semicolon). Field numbers start with 1, e.g. -FIELDS="1;2;3;5" -PRECISIONS="2;2;2;0".\n
    Arguments
    ----------
    - POINTS [`input point cloud`] : Point Cloud. The point cloud to export.
    - FILE [`file path`] : Text File. The file to write the point cloud to.
    - WRITE_HEADER [`boolean`] : Write Header. Default: 0 Write column names.
    - FIELDSEP [`choice`] : Field Separator. Available Choices: [0] tabulator [1] space [2] comma Default: 0 Field Separator
    - FIELDS [`text`] : Fields. The numbers (starting from 1) of the fields to export, separated by semicolon, e.g. "1;2;3;5"
    - PRECISIONS [`text`] : Precisions. The decimal precision to use for each field, separated by semicolon, e.g. "2;2;2;0"

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('io_shapes', '18', 'Export Point Cloud to Text File')
    if Tool.is_Okay():
        Tool.Set_Input ('POINTS', POINTS)
        Tool.Set_Option('FILE', FILE)
        Tool.Set_Option('WRITE_HEADER', WRITE_HEADER)
        Tool.Set_Option('FIELDSEP', FIELDSEP)
        Tool.Set_Option('FIELDS', FIELDS)
        Tool.Set_Option('PRECISIONS', PRECISIONS)
        return Tool.Execute(Verbose)
    return False

def Import_Simple_Features_from_Well_Known_Text(SHAPES=None, FILE=None, WKT=None, Verbose=2):
    '''
    Import Simple Features from Well Known Text
    ----------
    [io_shapes.19]\n
    Imports vector data from 'well known text' (WKT) simple features format.\n
    This import tool assumes that all features in a file are of the same type.\n
    Instead of importing from file(s), the tool also supports the conversion from a string provided with the 'WKT String' parameter.\n
    References:\n
    [Open Geospatial Consortium](http://www.opengeospatial.org/)\n
    Arguments
    ----------
    - SHAPES [`output shapes list`] : WKT Import
    - FILE [`file path`] : File
    - WKT [`long text`] : WKT String. Import WKT from string instead of file. Just paste the WKT.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('io_shapes', '19', 'Import Simple Features from Well Known Text')
    if Tool.is_Okay():
        Tool.Set_Output('SHAPES', SHAPES)
        Tool.Set_Option('FILE', FILE)
        Tool.Set_Option('WKT', WKT)
        return Tool.Execute(Verbose)
    return False

def run_tool_io_shapes_19(SHAPES=None, FILE=None, WKT=None, Verbose=2):
    '''
    Import Simple Features from Well Known Text
    ----------
    [io_shapes.19]\n
    Imports vector data from 'well known text' (WKT) simple features format.\n
    This import tool assumes that all features in a file are of the same type.\n
    Instead of importing from file(s), the tool also supports the conversion from a string provided with the 'WKT String' parameter.\n
    References:\n
    [Open Geospatial Consortium](http://www.opengeospatial.org/)\n
    Arguments
    ----------
    - SHAPES [`output shapes list`] : WKT Import
    - FILE [`file path`] : File
    - WKT [`long text`] : WKT String. Import WKT from string instead of file. Just paste the WKT.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('io_shapes', '19', 'Import Simple Features from Well Known Text')
    if Tool.is_Okay():
        Tool.Set_Output('SHAPES', SHAPES)
        Tool.Set_Option('FILE', FILE)
        Tool.Set_Option('WKT', WKT)
        return Tool.Execute(Verbose)
    return False

def Export_Simple_Features_to_Well_Known_Text(SHAPES=None, FILE=None, Verbose=2):
    '''
    Export Simple Features to Well Known Text
    ----------
    [io_shapes.20]\n
    Exports vector data to 'well known text' (WKT) simple features format.\n
    References:\n
    [Open Geospatial Consortium](http://www.opengeospatial.org/)\n
    Arguments
    ----------
    - SHAPES [`input shapes`] : Shapes
    - FILE [`file path`] : File

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('io_shapes', '20', 'Export Simple Features to Well Known Text')
    if Tool.is_Okay():
        Tool.Set_Input ('SHAPES', SHAPES)
        Tool.Set_Option('FILE', FILE)
        return Tool.Execute(Verbose)
    return False

def run_tool_io_shapes_20(SHAPES=None, FILE=None, Verbose=2):
    '''
    Export Simple Features to Well Known Text
    ----------
    [io_shapes.20]\n
    Exports vector data to 'well known text' (WKT) simple features format.\n
    References:\n
    [Open Geospatial Consortium](http://www.opengeospatial.org/)\n
    Arguments
    ----------
    - SHAPES [`input shapes`] : Shapes
    - FILE [`file path`] : File

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('io_shapes', '20', 'Export Simple Features to Well Known Text')
    if Tool.is_Okay():
        Tool.Set_Input ('SHAPES', SHAPES)
        Tool.Set_Option('FILE', FILE)
        return Tool.Execute(Verbose)
    return False

def Import_Building_Sketches_from_CityGML(BUILDINGS=None, FILES=None, PARTS=None, Verbose=2):
    '''
    Import Building Sketches from CityGML
    ----------
    [io_shapes.21]\n
    This tool facilitates the import of building sketches using a CityGML based file format, that is commonly used by German land surveying offices and geoinformation distributors.\n
    Arguments
    ----------
    - BUILDINGS [`output shapes`] : Buildings
    - FILES [`file path`] : Files
    - PARTS [`boolean`] : Check for Building Parts. Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('io_shapes', '21', 'Import Building Sketches from CityGML')
    if Tool.is_Okay():
        Tool.Set_Output('BUILDINGS', BUILDINGS)
        Tool.Set_Option('FILES', FILES)
        Tool.Set_Option('PARTS', PARTS)
        return Tool.Execute(Verbose)
    return False

def run_tool_io_shapes_21(BUILDINGS=None, FILES=None, PARTS=None, Verbose=2):
    '''
    Import Building Sketches from CityGML
    ----------
    [io_shapes.21]\n
    This tool facilitates the import of building sketches using a CityGML based file format, that is commonly used by German land surveying offices and geoinformation distributors.\n
    Arguments
    ----------
    - BUILDINGS [`output shapes`] : Buildings
    - FILES [`file path`] : Files
    - PARTS [`boolean`] : Check for Building Parts. Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('io_shapes', '21', 'Import Building Sketches from CityGML')
    if Tool.is_Okay():
        Tool.Set_Output('BUILDINGS', BUILDINGS)
        Tool.Set_Option('FILES', FILES)
        Tool.Set_Option('PARTS', PARTS)
        return Tool.Execute(Verbose)
    return False

def Export_Polygons_to_HTML_Image_Map(POLYGONS=None, LINK=None, TITLE=None, IMAGE=None, FILE=None, LINK_PREFIX=None, LINK_SUFFIX=None, Verbose=2):
    '''
    Export Polygons to HTML Image Map
    ----------
    [io_shapes.22]\n
    Tool to create an HTML ImageMap with polygon dataset and image. The tool requires a polygon dataset and an georeferenced image file. It outputs an html file for possible further editing. The image name in the mapfile html is hard coded to be map.png.\n
    [How to build the link:]\n
    (-) [Link] Select link identifier from the attribute table of the polygon data layer.\n
    (-) [Prefix] Adds a prefix to the link identifier. If just for icon with alternative text and, add ''#'').\n
    (-) [Suffix] Adds a suffix of the link identifier. Likewise ''.txt, .html, .doc, .png''.\n
    The link will be built with the pattern ''Prefix + Identifier + Suffix''.\n
    [Take care!:]\n
    (-) Image and Polygon dataset have to share the same projection.(-) Special characters in link (like &ouml;, &szlig, oder &raquo;) are not automatically replaced to html entities. Replace them after creation in editor if needed.\n
    [Example for GUI usage:]\n
    (-) Load your polygon dataset and open it in a map.(-) Create an image of this map using the map's menu command ''Save Map to Workspace'' and save it to an image file using the file name ''map.png''.(-) Run this tool and save the output to the same directory as the image.(-) Open the html file in a browser for preview.(-) Further editing of the html file is suggested.\n
    Arguments
    ----------
    - POLYGONS [`input shapes`] : Polygons
    - LINK [`table field`] : Link
    - TITLE [`table field`] : Title
    - IMAGE [`grid system`] : Image. grid system of georeferenced image
    - FILE [`file path`] : File
    - LINK_PREFIX [`text`] : Link Prefix. Default: http://www.saga-gis.org/
    - LINK_SUFFIX [`text`] : Link Suffix. Default: index.html

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('io_shapes', '22', 'Export Polygons to HTML Image Map')
    if Tool.is_Okay():
        Tool.Set_Input ('POLYGONS', POLYGONS)
        Tool.Set_Option('LINK', LINK)
        Tool.Set_Option('TITLE', TITLE)
        Tool.Set_Option('IMAGE', IMAGE)
        Tool.Set_Option('FILE', FILE)
        Tool.Set_Option('LINK_PREFIX', LINK_PREFIX)
        Tool.Set_Option('LINK_SUFFIX', LINK_SUFFIX)
        return Tool.Execute(Verbose)
    return False

def run_tool_io_shapes_22(POLYGONS=None, LINK=None, TITLE=None, IMAGE=None, FILE=None, LINK_PREFIX=None, LINK_SUFFIX=None, Verbose=2):
    '''
    Export Polygons to HTML Image Map
    ----------
    [io_shapes.22]\n
    Tool to create an HTML ImageMap with polygon dataset and image. The tool requires a polygon dataset and an georeferenced image file. It outputs an html file for possible further editing. The image name in the mapfile html is hard coded to be map.png.\n
    [How to build the link:]\n
    (-) [Link] Select link identifier from the attribute table of the polygon data layer.\n
    (-) [Prefix] Adds a prefix to the link identifier. If just for icon with alternative text and, add ''#'').\n
    (-) [Suffix] Adds a suffix of the link identifier. Likewise ''.txt, .html, .doc, .png''.\n
    The link will be built with the pattern ''Prefix + Identifier + Suffix''.\n
    [Take care!:]\n
    (-) Image and Polygon dataset have to share the same projection.(-) Special characters in link (like &ouml;, &szlig, oder &raquo;) are not automatically replaced to html entities. Replace them after creation in editor if needed.\n
    [Example for GUI usage:]\n
    (-) Load your polygon dataset and open it in a map.(-) Create an image of this map using the map's menu command ''Save Map to Workspace'' and save it to an image file using the file name ''map.png''.(-) Run this tool and save the output to the same directory as the image.(-) Open the html file in a browser for preview.(-) Further editing of the html file is suggested.\n
    Arguments
    ----------
    - POLYGONS [`input shapes`] : Polygons
    - LINK [`table field`] : Link
    - TITLE [`table field`] : Title
    - IMAGE [`grid system`] : Image. grid system of georeferenced image
    - FILE [`file path`] : File
    - LINK_PREFIX [`text`] : Link Prefix. Default: http://www.saga-gis.org/
    - LINK_SUFFIX [`text`] : Link Suffix. Default: index.html

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('io_shapes', '22', 'Export Polygons to HTML Image Map')
    if Tool.is_Okay():
        Tool.Set_Input ('POLYGONS', POLYGONS)
        Tool.Set_Option('LINK', LINK)
        Tool.Set_Option('TITLE', TITLE)
        Tool.Set_Option('IMAGE', IMAGE)
        Tool.Set_Option('FILE', FILE)
        Tool.Set_Option('LINK_PREFIX', LINK_PREFIX)
        Tool.Set_Option('LINK_SUFFIX', LINK_SUFFIX)
        return Tool.Execute(Verbose)
    return False

def Import_Point_Cloud_from_PTS_File(POINTS=None, FILENAME=None, RGB=None, CRS_STRING=None, Verbose=2):
    '''
    Import Point Cloud from PTS File
    ----------
    [io_shapes.23]\n
    Imports point cloud data from a PTS file.\n
    Arguments
    ----------
    - POINTS [`output point cloud`] : Points
    - FILENAME [`file path`] : File
    - RGB [`choice`] : Import RGB Values as.... Available Choices: [0] separate values [1] single rgb-coded integer value Default: 1
    - CRS_STRING [`text`] : Coordinate System Definition. Supported formats comprise PROJ and WKT strings, object codes (e.g. "EPSG:4326").

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('io_shapes', '23', 'Import Point Cloud from PTS File')
    if Tool.is_Okay():
        Tool.Set_Output('POINTS', POINTS)
        Tool.Set_Option('FILENAME', FILENAME)
        Tool.Set_Option('RGB', RGB)
        Tool.Set_Option('CRS_STRING', CRS_STRING)
        return Tool.Execute(Verbose)
    return False

def run_tool_io_shapes_23(POINTS=None, FILENAME=None, RGB=None, CRS_STRING=None, Verbose=2):
    '''
    Import Point Cloud from PTS File
    ----------
    [io_shapes.23]\n
    Imports point cloud data from a PTS file.\n
    Arguments
    ----------
    - POINTS [`output point cloud`] : Points
    - FILENAME [`file path`] : File
    - RGB [`choice`] : Import RGB Values as.... Available Choices: [0] separate values [1] single rgb-coded integer value Default: 1
    - CRS_STRING [`text`] : Coordinate System Definition. Supported formats comprise PROJ and WKT strings, object codes (e.g. "EPSG:4326").

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('io_shapes', '23', 'Import Point Cloud from PTS File')
    if Tool.is_Okay():
        Tool.Set_Output('POINTS', POINTS)
        Tool.Set_Option('FILENAME', FILENAME)
        Tool.Set_Option('RGB', RGB)
        Tool.Set_Option('CRS_STRING', CRS_STRING)
        return Tool.Execute(Verbose)
    return False

def Import_ASEGGDF(POINTS=None, FILE=None, Verbose=2):
    '''
    Import ASEG-GDF
    ----------
    [io_shapes.24]\n
    Import a 'General Data Format Revision 2 (GDF2)' file as defined by the 'Australian Society of Exploration Geophysicists (ASEG)' as table.\n
    Arguments
    ----------
    - POINTS [`output shapes`] : Points
    - FILE [`file path`] : File

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('io_shapes', '24', 'Import ASEG-GDF')
    if Tool.is_Okay():
        Tool.Set_Output('POINTS', POINTS)
        Tool.Set_Option('FILE', FILE)
        return Tool.Execute(Verbose)
    return False

def run_tool_io_shapes_24(POINTS=None, FILE=None, Verbose=2):
    '''
    Import ASEG-GDF
    ----------
    [io_shapes.24]\n
    Import a 'General Data Format Revision 2 (GDF2)' file as defined by the 'Australian Society of Exploration Geophysicists (ASEG)' as table.\n
    Arguments
    ----------
    - POINTS [`output shapes`] : Points
    - FILE [`file path`] : File

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('io_shapes', '24', 'Import ASEG-GDF')
    if Tool.is_Okay():
        Tool.Set_Output('POINTS', POINTS)
        Tool.Set_Option('FILE', FILE)
        return Tool.Execute(Verbose)
    return False

