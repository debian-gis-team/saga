#! /usr/bin/env python

'''
Python Interface to SAGA Tools Library
----------
- Category : Import/Export
- Name     : PDAL
- ID       : io_pdal

Description
----------
Tools that use the Point Data Abstraction Library (PDAL).

PDAL Version: 2.7.2 (git-version: Release)
'''

from PySAGA.helper import Tool_Wrapper

def Import_Point_Cloud(EXTENT_SHAPES=None, POINTS=None, FILES=None, VARS=None, VAR_TIME=None, VAR_INTENSITY=None, VAR_SCANANGLE=None, VAR_RETURN=None, VAR_RETURNS=None, VAR_CLASSIFICATION=None, VAR_USERDATA=None, VAR_EDGE=None, VAR_DIRECTION=None, VAR_SOURCEID=None, VAR_COLOR_RED=None, VAR_COLOR_GREEN=None, VAR_COLOR_BLUE=None, VAR_COLOR=None, RGB_RANGE=None, CLASSES=None, EXTENT=None, EXTENT_XMIN=None, EXTENT_XMAX=None, EXTENT_YMIN=None, EXTENT_YMAX=None, EXTENT_GRID=None, EXTENT_BUFFER=None, Verbose=2):
    '''
    Import Point Cloud
    ----------
    [io_pdal.0]\n
    The tool allows one to import point cloud data from various file formats using the "Point Data Abstraction Library" (PDAL).\n
    By default, all supported attributes will be imported. Note that the list of attributes supported by the tool is currently based on the attributes defined in the ASPRS LAS specification.\n
    PDAL 2.7.2 (git-version: Release)\n
    Supported point cloud formats:\n
    ============\n
    Name	Extension	Description\n
    readers.copc	copc	COPC Reader\n
    readers.draco		Read data from a Draco array.\n
    readers.e57		Reader for E57 files\n
    readers.fbi	fbi	Fbi Reader\n
    readers.hdf		HDF Reader\n
    readers.i3s		I3S Reader\n
    readers.icebridge		NASA HDF5-based IceBridge ATM reader.\n
    See http://nsidc.org/data/docs/daac/icebridge/ilatm1b/index.html\n
    for more information.\n
    readers.ilvis2		ILVIS2 Reader\n
    readers.las	las;laz	ASPRS LAS 1.0 - 1.4 read support\n
    readers.nitf		NITF Reader\n
    readers.obj	obj	Obj Reader\n
    readers.pcd	pcd	Read data in the Point Cloud Library (PCL) format.\n
    readers.pgpointcloud		Read data from pgpointcloud format. "query" option needs to be a\n
    SQL statement selecting the data.\n
    readers.ply	ply	Read ply files.\n
    readers.pts	pts	Pts Reader\n
    readers.ptx	ptx	Ptx Reader\n
    readers.qfit	qi	QFIT Reader\n
    readers.sbet	sbet	SBET Reader\n
    readers.slpk		SLPK Reader\n
    readers.smrmsg	smrmsg	SBET smrmsg Reader\n
    readers.stac		STAC Reader\n
    readers.terrasolid	bin	TerraSolid Reader\n
    readers.text	csv;txt	Text Reader\n
    readers.tiledb		Read data from a TileDB array.\n
    readers.tindex	tindex	TileIndex Reader\n
    ============\n
    Arguments
    ----------
    - EXTENT_SHAPES [`input shapes`] : Shapes Extent
    - POINTS [`output point cloud list`] : Points
    - FILES [`file path`] : Files
    - VARS [`boolean`] : Import All Attributes. Default: 1 Check this to import all supported attributes, or select the attributes you want to become imported individually.
    - VAR_TIME [`boolean`] : GPS-Time. Default: 0
    - VAR_INTENSITY [`boolean`] : Intensity. Default: 0
    - VAR_SCANANGLE [`boolean`] : Scan Angle. Default: 0
    - VAR_RETURN [`boolean`] : Number of the Return. Default: 0
    - VAR_RETURNS [`boolean`] : Number of Returns of Given Pulse. Default: 0
    - VAR_CLASSIFICATION [`boolean`] : Classification. Default: 0
    - VAR_USERDATA [`boolean`] : User Data. Default: 0
    - VAR_EDGE [`boolean`] : Edge of Flight Line Flag. Default: 0
    - VAR_DIRECTION [`boolean`] : Direction of Scan Flag. Default: 0
    - VAR_SOURCEID [`boolean`] : Point Source ID. Default: 0
    - VAR_COLOR_RED [`boolean`] : Red Channel Color. Default: 0
    - VAR_COLOR_GREEN [`boolean`] : Green Channel Color. Default: 0
    - VAR_COLOR_BLUE [`boolean`] : Blue Channel Color. Default: 0
    - VAR_COLOR [`boolean`] : RGB-Coded Color. Default: 0 Import R,G,B values as SAGA RGB-coded value.
    - RGB_RANGE [`choice`] : RGB Value Range. Available Choices: [0] 8 bit [1] 16 bit Default: 1 The color depth of red, green, blue (and NIR) values in the LAS file.
    - CLASSES [`text`] : Classes. If classification is available only points are loaded whose identifiers are listed (comma separated, e.g. "2, 3, 4, 5, 6"). Ignored if empty.
    - EXTENT [`choice`] : Extent. Available Choices: [0] original [1] user defined [2] grid system [3] shapes extent Default: 0
    - EXTENT_XMIN [`floating point number`] : Left. Default: 0.000000
    - EXTENT_XMAX [`floating point number`] : Right. Default: 0.000000
    - EXTENT_YMIN [`floating point number`] : Bottom. Default: 0.000000
    - EXTENT_YMAX [`floating point number`] : Top. Default: 0.000000
    - EXTENT_GRID [`grid system`] : Grid System
    - EXTENT_BUFFER [`floating point number`] : Buffer. Minimum: 0.000000 Default: 0.000000

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('io_pdal', '0', 'Import Point Cloud')
    if Tool.is_Okay():
        Tool.Set_Input ('EXTENT_SHAPES', EXTENT_SHAPES)
        Tool.Set_Output('POINTS', POINTS)
        Tool.Set_Option('FILES', FILES)
        Tool.Set_Option('VARS', VARS)
        Tool.Set_Option('VAR_TIME', VAR_TIME)
        Tool.Set_Option('VAR_INTENSITY', VAR_INTENSITY)
        Tool.Set_Option('VAR_SCANANGLE', VAR_SCANANGLE)
        Tool.Set_Option('VAR_RETURN', VAR_RETURN)
        Tool.Set_Option('VAR_RETURNS', VAR_RETURNS)
        Tool.Set_Option('VAR_CLASSIFICATION', VAR_CLASSIFICATION)
        Tool.Set_Option('VAR_USERDATA', VAR_USERDATA)
        Tool.Set_Option('VAR_EDGE', VAR_EDGE)
        Tool.Set_Option('VAR_DIRECTION', VAR_DIRECTION)
        Tool.Set_Option('VAR_SOURCEID', VAR_SOURCEID)
        Tool.Set_Option('VAR_COLOR_RED', VAR_COLOR_RED)
        Tool.Set_Option('VAR_COLOR_GREEN', VAR_COLOR_GREEN)
        Tool.Set_Option('VAR_COLOR_BLUE', VAR_COLOR_BLUE)
        Tool.Set_Option('VAR_COLOR', VAR_COLOR)
        Tool.Set_Option('RGB_RANGE', RGB_RANGE)
        Tool.Set_Option('CLASSES', CLASSES)
        Tool.Set_Option('EXTENT', EXTENT)
        Tool.Set_Option('EXTENT_XMIN', EXTENT_XMIN)
        Tool.Set_Option('EXTENT_XMAX', EXTENT_XMAX)
        Tool.Set_Option('EXTENT_YMIN', EXTENT_YMIN)
        Tool.Set_Option('EXTENT_YMAX', EXTENT_YMAX)
        Tool.Set_Option('EXTENT_GRID', EXTENT_GRID)
        Tool.Set_Option('EXTENT_BUFFER', EXTENT_BUFFER)
        return Tool.Execute(Verbose)
    return False

def run_tool_io_pdal_0(EXTENT_SHAPES=None, POINTS=None, FILES=None, VARS=None, VAR_TIME=None, VAR_INTENSITY=None, VAR_SCANANGLE=None, VAR_RETURN=None, VAR_RETURNS=None, VAR_CLASSIFICATION=None, VAR_USERDATA=None, VAR_EDGE=None, VAR_DIRECTION=None, VAR_SOURCEID=None, VAR_COLOR_RED=None, VAR_COLOR_GREEN=None, VAR_COLOR_BLUE=None, VAR_COLOR=None, RGB_RANGE=None, CLASSES=None, EXTENT=None, EXTENT_XMIN=None, EXTENT_XMAX=None, EXTENT_YMIN=None, EXTENT_YMAX=None, EXTENT_GRID=None, EXTENT_BUFFER=None, Verbose=2):
    '''
    Import Point Cloud
    ----------
    [io_pdal.0]\n
    The tool allows one to import point cloud data from various file formats using the "Point Data Abstraction Library" (PDAL).\n
    By default, all supported attributes will be imported. Note that the list of attributes supported by the tool is currently based on the attributes defined in the ASPRS LAS specification.\n
    PDAL 2.7.2 (git-version: Release)\n
    Supported point cloud formats:\n
    ============\n
    Name	Extension	Description\n
    readers.copc	copc	COPC Reader\n
    readers.draco		Read data from a Draco array.\n
    readers.e57		Reader for E57 files\n
    readers.fbi	fbi	Fbi Reader\n
    readers.hdf		HDF Reader\n
    readers.i3s		I3S Reader\n
    readers.icebridge		NASA HDF5-based IceBridge ATM reader.\n
    See http://nsidc.org/data/docs/daac/icebridge/ilatm1b/index.html\n
    for more information.\n
    readers.ilvis2		ILVIS2 Reader\n
    readers.las	las;laz	ASPRS LAS 1.0 - 1.4 read support\n
    readers.nitf		NITF Reader\n
    readers.obj	obj	Obj Reader\n
    readers.pcd	pcd	Read data in the Point Cloud Library (PCL) format.\n
    readers.pgpointcloud		Read data from pgpointcloud format. "query" option needs to be a\n
    SQL statement selecting the data.\n
    readers.ply	ply	Read ply files.\n
    readers.pts	pts	Pts Reader\n
    readers.ptx	ptx	Ptx Reader\n
    readers.qfit	qi	QFIT Reader\n
    readers.sbet	sbet	SBET Reader\n
    readers.slpk		SLPK Reader\n
    readers.smrmsg	smrmsg	SBET smrmsg Reader\n
    readers.stac		STAC Reader\n
    readers.terrasolid	bin	TerraSolid Reader\n
    readers.text	csv;txt	Text Reader\n
    readers.tiledb		Read data from a TileDB array.\n
    readers.tindex	tindex	TileIndex Reader\n
    ============\n
    Arguments
    ----------
    - EXTENT_SHAPES [`input shapes`] : Shapes Extent
    - POINTS [`output point cloud list`] : Points
    - FILES [`file path`] : Files
    - VARS [`boolean`] : Import All Attributes. Default: 1 Check this to import all supported attributes, or select the attributes you want to become imported individually.
    - VAR_TIME [`boolean`] : GPS-Time. Default: 0
    - VAR_INTENSITY [`boolean`] : Intensity. Default: 0
    - VAR_SCANANGLE [`boolean`] : Scan Angle. Default: 0
    - VAR_RETURN [`boolean`] : Number of the Return. Default: 0
    - VAR_RETURNS [`boolean`] : Number of Returns of Given Pulse. Default: 0
    - VAR_CLASSIFICATION [`boolean`] : Classification. Default: 0
    - VAR_USERDATA [`boolean`] : User Data. Default: 0
    - VAR_EDGE [`boolean`] : Edge of Flight Line Flag. Default: 0
    - VAR_DIRECTION [`boolean`] : Direction of Scan Flag. Default: 0
    - VAR_SOURCEID [`boolean`] : Point Source ID. Default: 0
    - VAR_COLOR_RED [`boolean`] : Red Channel Color. Default: 0
    - VAR_COLOR_GREEN [`boolean`] : Green Channel Color. Default: 0
    - VAR_COLOR_BLUE [`boolean`] : Blue Channel Color. Default: 0
    - VAR_COLOR [`boolean`] : RGB-Coded Color. Default: 0 Import R,G,B values as SAGA RGB-coded value.
    - RGB_RANGE [`choice`] : RGB Value Range. Available Choices: [0] 8 bit [1] 16 bit Default: 1 The color depth of red, green, blue (and NIR) values in the LAS file.
    - CLASSES [`text`] : Classes. If classification is available only points are loaded whose identifiers are listed (comma separated, e.g. "2, 3, 4, 5, 6"). Ignored if empty.
    - EXTENT [`choice`] : Extent. Available Choices: [0] original [1] user defined [2] grid system [3] shapes extent Default: 0
    - EXTENT_XMIN [`floating point number`] : Left. Default: 0.000000
    - EXTENT_XMAX [`floating point number`] : Right. Default: 0.000000
    - EXTENT_YMIN [`floating point number`] : Bottom. Default: 0.000000
    - EXTENT_YMAX [`floating point number`] : Top. Default: 0.000000
    - EXTENT_GRID [`grid system`] : Grid System
    - EXTENT_BUFFER [`floating point number`] : Buffer. Minimum: 0.000000 Default: 0.000000

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('io_pdal', '0', 'Import Point Cloud')
    if Tool.is_Okay():
        Tool.Set_Input ('EXTENT_SHAPES', EXTENT_SHAPES)
        Tool.Set_Output('POINTS', POINTS)
        Tool.Set_Option('FILES', FILES)
        Tool.Set_Option('VARS', VARS)
        Tool.Set_Option('VAR_TIME', VAR_TIME)
        Tool.Set_Option('VAR_INTENSITY', VAR_INTENSITY)
        Tool.Set_Option('VAR_SCANANGLE', VAR_SCANANGLE)
        Tool.Set_Option('VAR_RETURN', VAR_RETURN)
        Tool.Set_Option('VAR_RETURNS', VAR_RETURNS)
        Tool.Set_Option('VAR_CLASSIFICATION', VAR_CLASSIFICATION)
        Tool.Set_Option('VAR_USERDATA', VAR_USERDATA)
        Tool.Set_Option('VAR_EDGE', VAR_EDGE)
        Tool.Set_Option('VAR_DIRECTION', VAR_DIRECTION)
        Tool.Set_Option('VAR_SOURCEID', VAR_SOURCEID)
        Tool.Set_Option('VAR_COLOR_RED', VAR_COLOR_RED)
        Tool.Set_Option('VAR_COLOR_GREEN', VAR_COLOR_GREEN)
        Tool.Set_Option('VAR_COLOR_BLUE', VAR_COLOR_BLUE)
        Tool.Set_Option('VAR_COLOR', VAR_COLOR)
        Tool.Set_Option('RGB_RANGE', RGB_RANGE)
        Tool.Set_Option('CLASSES', CLASSES)
        Tool.Set_Option('EXTENT', EXTENT)
        Tool.Set_Option('EXTENT_XMIN', EXTENT_XMIN)
        Tool.Set_Option('EXTENT_XMAX', EXTENT_XMAX)
        Tool.Set_Option('EXTENT_YMIN', EXTENT_YMIN)
        Tool.Set_Option('EXTENT_YMAX', EXTENT_YMAX)
        Tool.Set_Option('EXTENT_GRID', EXTENT_GRID)
        Tool.Set_Option('EXTENT_BUFFER', EXTENT_BUFFER)
        return Tool.Execute(Verbose)
    return False

def Export_LASLAZ_File(POINTS=None, T=None, r=None, n=None, i=None, c=None, SCH=None, C=None, B=None, G=None, R=None, NIR=None, a=None, d=None, e=None, u=None, p=None, FILE=None, FILE_FORMAT=None, FORMAT=None, RGB_RANGE=None, OFF_X=None, OFF_Y=None, OFF_Z=None, SCALE_X=None, SCALE_Y=None, SCALE_Z=None, Verbose=2):
    '''
    Export LAS/LAZ File
    ----------
    [io_pdal.1]\n
    The tool allows one to export a point cloud as ASPRS LAS (or compressed LAZ) file using the "Point Data Abstraction Library" (PDAL).\n
    The file extension of the output file determines whether the file is written compressed (*.laz) or uncompressed (*.las).\n
    The number and type of attributes that can be exported depends on the chosen LAS file version and point data record format. Please have a look at the ASPRS LAS specification on how these formats are defined.\n
    PDAL-2.7.2 (git-version: Release)\n
    Arguments
    ----------
    - POINTS [`input point cloud`] : Point Cloud. The point cloud to export.
    - T [`table field`] : GPS-Time
    - r [`table field`] : Number of the Return
    - n [`table field`] : Number of Returns of Given Pulse
    - i [`table field`] : Intensity
    - c [`table field`] : Classification
    - SCH [`table field`] : Scanner Channel
    - C [`table field`] : RGB Color Value
    - B [`table field`] : Blue Channel Color
    - G [`table field`] : Green Channel Color
    - R [`table field`] : Red Channel Color
    - NIR [`table field`] : Near Infrared
    - a [`table field`] : Scan Angle
    - d [`table field`] : Direction of Scan Flag
    - e [`table field`] : Edge of Flight Line Flag
    - u [`table field`] : User Data
    - p [`table field`] : Point Source ID
    - FILE [`file path`] : Output File. The LAS/LAZ output file.
    - FILE_FORMAT [`choice`] : File Format. Available Choices: [0] LAS 1.2 [1] LAS 1.4 Default: 1 Choose the file format to write. The format determines which attributes can be written and in which data depth.
    - FORMAT [`choice`] : Point Data Record Format. Available Choices: [0] 0 [1] 1 [2] 2 [3] 3 [4] 6 [5] 7 [6] 8 Default: 3 Choose the point data record format to write. The format determines which attributes can be written.
    - RGB_RANGE [`choice`] : Color Depth. Available Choices: [0] 16 bit [1] 8 bit Default: 1 Color depth of the Red, Green, Blue, NIR values in the input point cloud. 8 bit values will be scaled to 16 bit.
    - OFF_X [`floating point number`] : Offset X. Default: 0.000000
    - OFF_Y [`floating point number`] : Offset Y. Default: 0.000000
    - OFF_Z [`floating point number`] : Offset Z. Default: 0.000000
    - SCALE_X [`floating point number`] : Scale X. Default: 0.001000
    - SCALE_Y [`floating point number`] : Scale Y. Default: 0.001000
    - SCALE_Z [`floating point number`] : Scale Z. Default: 0.001000

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('io_pdal', '1', 'Export LAS/LAZ File')
    if Tool.is_Okay():
        Tool.Set_Input ('POINTS', POINTS)
        Tool.Set_Option('T', T)
        Tool.Set_Option('r', r)
        Tool.Set_Option('n', n)
        Tool.Set_Option('i', i)
        Tool.Set_Option('c', c)
        Tool.Set_Option('sCH', SCH)
        Tool.Set_Option('C', C)
        Tool.Set_Option('B', B)
        Tool.Set_Option('G', G)
        Tool.Set_Option('R', R)
        Tool.Set_Option('NIR', NIR)
        Tool.Set_Option('a', a)
        Tool.Set_Option('d', d)
        Tool.Set_Option('e', e)
        Tool.Set_Option('u', u)
        Tool.Set_Option('p', p)
        Tool.Set_Option('FILE', FILE)
        Tool.Set_Option('FILE_FORMAT', FILE_FORMAT)
        Tool.Set_Option('FORMAT', FORMAT)
        Tool.Set_Option('RGB_RANGE', RGB_RANGE)
        Tool.Set_Option('OFF_X', OFF_X)
        Tool.Set_Option('OFF_Y', OFF_Y)
        Tool.Set_Option('OFF_Z', OFF_Z)
        Tool.Set_Option('SCALE_X', SCALE_X)
        Tool.Set_Option('SCALE_Y', SCALE_Y)
        Tool.Set_Option('SCALE_Z', SCALE_Z)
        return Tool.Execute(Verbose)
    return False

def run_tool_io_pdal_1(POINTS=None, T=None, r=None, n=None, i=None, c=None, SCH=None, C=None, B=None, G=None, R=None, NIR=None, a=None, d=None, e=None, u=None, p=None, FILE=None, FILE_FORMAT=None, FORMAT=None, RGB_RANGE=None, OFF_X=None, OFF_Y=None, OFF_Z=None, SCALE_X=None, SCALE_Y=None, SCALE_Z=None, Verbose=2):
    '''
    Export LAS/LAZ File
    ----------
    [io_pdal.1]\n
    The tool allows one to export a point cloud as ASPRS LAS (or compressed LAZ) file using the "Point Data Abstraction Library" (PDAL).\n
    The file extension of the output file determines whether the file is written compressed (*.laz) or uncompressed (*.las).\n
    The number and type of attributes that can be exported depends on the chosen LAS file version and point data record format. Please have a look at the ASPRS LAS specification on how these formats are defined.\n
    PDAL-2.7.2 (git-version: Release)\n
    Arguments
    ----------
    - POINTS [`input point cloud`] : Point Cloud. The point cloud to export.
    - T [`table field`] : GPS-Time
    - r [`table field`] : Number of the Return
    - n [`table field`] : Number of Returns of Given Pulse
    - i [`table field`] : Intensity
    - c [`table field`] : Classification
    - SCH [`table field`] : Scanner Channel
    - C [`table field`] : RGB Color Value
    - B [`table field`] : Blue Channel Color
    - G [`table field`] : Green Channel Color
    - R [`table field`] : Red Channel Color
    - NIR [`table field`] : Near Infrared
    - a [`table field`] : Scan Angle
    - d [`table field`] : Direction of Scan Flag
    - e [`table field`] : Edge of Flight Line Flag
    - u [`table field`] : User Data
    - p [`table field`] : Point Source ID
    - FILE [`file path`] : Output File. The LAS/LAZ output file.
    - FILE_FORMAT [`choice`] : File Format. Available Choices: [0] LAS 1.2 [1] LAS 1.4 Default: 1 Choose the file format to write. The format determines which attributes can be written and in which data depth.
    - FORMAT [`choice`] : Point Data Record Format. Available Choices: [0] 0 [1] 1 [2] 2 [3] 3 [4] 6 [5] 7 [6] 8 Default: 3 Choose the point data record format to write. The format determines which attributes can be written.
    - RGB_RANGE [`choice`] : Color Depth. Available Choices: [0] 16 bit [1] 8 bit Default: 1 Color depth of the Red, Green, Blue, NIR values in the input point cloud. 8 bit values will be scaled to 16 bit.
    - OFF_X [`floating point number`] : Offset X. Default: 0.000000
    - OFF_Y [`floating point number`] : Offset Y. Default: 0.000000
    - OFF_Z [`floating point number`] : Offset Z. Default: 0.000000
    - SCALE_X [`floating point number`] : Scale X. Default: 0.001000
    - SCALE_Y [`floating point number`] : Scale Y. Default: 0.001000
    - SCALE_Z [`floating point number`] : Scale Z. Default: 0.001000

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('io_pdal', '1', 'Export LAS/LAZ File')
    if Tool.is_Okay():
        Tool.Set_Input ('POINTS', POINTS)
        Tool.Set_Option('T', T)
        Tool.Set_Option('r', r)
        Tool.Set_Option('n', n)
        Tool.Set_Option('i', i)
        Tool.Set_Option('c', c)
        Tool.Set_Option('sCH', SCH)
        Tool.Set_Option('C', C)
        Tool.Set_Option('B', B)
        Tool.Set_Option('G', G)
        Tool.Set_Option('R', R)
        Tool.Set_Option('NIR', NIR)
        Tool.Set_Option('a', a)
        Tool.Set_Option('d', d)
        Tool.Set_Option('e', e)
        Tool.Set_Option('u', u)
        Tool.Set_Option('p', p)
        Tool.Set_Option('FILE', FILE)
        Tool.Set_Option('FILE_FORMAT', FILE_FORMAT)
        Tool.Set_Option('FORMAT', FORMAT)
        Tool.Set_Option('RGB_RANGE', RGB_RANGE)
        Tool.Set_Option('OFF_X', OFF_X)
        Tool.Set_Option('OFF_Y', OFF_Y)
        Tool.Set_Option('OFF_Z', OFF_Z)
        Tool.Set_Option('SCALE_X', SCALE_X)
        Tool.Set_Option('SCALE_Y', SCALE_Y)
        Tool.Set_Option('SCALE_Z', SCALE_Z)
        return Tool.Execute(Verbose)
    return False

def Import_Grid_from_Point_Cloud(TARGET_TEMPLATE=None, GRID=None, COUNT=None, FILES=None, STREAM=None, CLASSES=None, AGGREGATION=None, TARGET_DEFINITION=None, TARGET_USER_SIZE=None, TARGET_USER_XMIN=None, TARGET_USER_XMAX=None, TARGET_USER_YMIN=None, TARGET_USER_YMAX=None, TARGET_USER_COLS=None, TARGET_USER_ROWS=None, TARGET_USER_FLAT=None, TARGET_USER_FITS=None, TARGET_SYSTEM=None, Verbose=2):
    '''
    Import Grid from Point Cloud
    ----------
    [io_pdal.2]\n
    This tool directly creates a grid from point cloud elevation data and supports several file formats using the "Point Data Abstraction Library" (PDAL).\n
    PDAL 2.7.2 (git-version: Release)\n
    Supported point cloud formats:\n
    ============\n
    Name	Extension	Description\n
    readers.copc	copc	COPC Reader\n
    readers.draco		Read data from a Draco array.\n
    readers.e57		Reader for E57 files\n
    readers.fbi	fbi	Fbi Reader\n
    readers.hdf		HDF Reader\n
    readers.i3s		I3S Reader\n
    readers.icebridge		NASA HDF5-based IceBridge ATM reader.\n
    See http://nsidc.org/data/docs/daac/icebridge/ilatm1b/index.html\n
    for more information.\n
    readers.ilvis2		ILVIS2 Reader\n
    readers.las	las;laz	ASPRS LAS 1.0 - 1.4 read support\n
    readers.nitf		NITF Reader\n
    readers.obj	obj	Obj Reader\n
    readers.pcd	pcd	Read data in the Point Cloud Library (PCL) format.\n
    readers.pgpointcloud		Read data from pgpointcloud format. "query" option needs to be a\n
    SQL statement selecting the data.\n
    readers.ply	ply	Read ply files.\n
    readers.pts	pts	Pts Reader\n
    readers.ptx	ptx	Ptx Reader\n
    readers.qfit	qi	QFIT Reader\n
    readers.sbet	sbet	SBET Reader\n
    readers.slpk		SLPK Reader\n
    readers.smrmsg	smrmsg	SBET smrmsg Reader\n
    readers.stac		STAC Reader\n
    readers.terrasolid	bin	TerraSolid Reader\n
    readers.text	csv;txt	Text Reader\n
    readers.tiledb		Read data from a TileDB array.\n
    readers.tindex	tindex	TileIndex Reader\n
    ============\n
    Arguments
    ----------
    - TARGET_TEMPLATE [`optional input grid`] : Target System. use this grid's system for output grids
    - GRID [`output grid`] : Grid
    - COUNT [`output grid`] : Number of Values
    - FILES [`file path`] : Files
    - STREAM [`boolean`] : Open Streamable. Default: 1 Use streamable file access if supported by driver. Reduces memory requirements.
    - CLASSES [`text`] : Classes. If classification is available only points are loaded whose identifiers are listed (comma separated, e.g. "2, 3, 4, 5, 6"). Ignored if empty.
    - AGGREGATION [`choice`] : Value Aggregation. Available Choices: [0] first [1] last [2] minimum [3] maximum [4] mean Default: 1 Output generated when more than one point falls into one target cell.
    - TARGET_DEFINITION [`choice`] : Target Grid System. Available Choices: [0] user defined [1] grid or grid system Default: 0
    - TARGET_USER_SIZE [`floating point number`] : Cellsize. Minimum: 0.000000 Default: 1.000000
    - TARGET_USER_XMIN [`floating point number`] : West. Default: 0.000000
    - TARGET_USER_XMAX [`floating point number`] : East. Default: 100.000000
    - TARGET_USER_YMIN [`floating point number`] : South. Default: 0.000000
    - TARGET_USER_YMAX [`floating point number`] : North. Default: 100.000000
    - TARGET_USER_COLS [`integer number`] : Columns. Minimum: 1 Default: 101 Number of cells in East-West direction.
    - TARGET_USER_ROWS [`integer number`] : Rows. Minimum: 1 Default: 101 Number of cells in North-South direction.
    - TARGET_USER_FLAT [`boolean`] : Rounding. Default: 1 Round bounding coordinates to multiples of cell size. Ignored if cell size has decimal places.
    - TARGET_USER_FITS [`choice`] : Fit. Available Choices: [0] nodes [1] cells Default: 0
    - TARGET_SYSTEM [`grid system`] : Grid System

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('io_pdal', '2', 'Import Grid from Point Cloud')
    if Tool.is_Okay():
        Tool.Set_Input ('TARGET_TEMPLATE', TARGET_TEMPLATE)
        Tool.Set_Output('GRID', GRID)
        Tool.Set_Output('COUNT', COUNT)
        Tool.Set_Option('FILES', FILES)
        Tool.Set_Option('STREAM', STREAM)
        Tool.Set_Option('CLASSES', CLASSES)
        Tool.Set_Option('AGGREGATION', AGGREGATION)
        Tool.Set_Option('TARGET_DEFINITION', TARGET_DEFINITION)
        Tool.Set_Option('TARGET_USER_SIZE', TARGET_USER_SIZE)
        Tool.Set_Option('TARGET_USER_XMIN', TARGET_USER_XMIN)
        Tool.Set_Option('TARGET_USER_XMAX', TARGET_USER_XMAX)
        Tool.Set_Option('TARGET_USER_YMIN', TARGET_USER_YMIN)
        Tool.Set_Option('TARGET_USER_YMAX', TARGET_USER_YMAX)
        Tool.Set_Option('TARGET_USER_COLS', TARGET_USER_COLS)
        Tool.Set_Option('TARGET_USER_ROWS', TARGET_USER_ROWS)
        Tool.Set_Option('TARGET_USER_FLAT', TARGET_USER_FLAT)
        Tool.Set_Option('TARGET_USER_FITS', TARGET_USER_FITS)
        Tool.Set_Option('TARGET_SYSTEM', TARGET_SYSTEM)
        return Tool.Execute(Verbose)
    return False

def run_tool_io_pdal_2(TARGET_TEMPLATE=None, GRID=None, COUNT=None, FILES=None, STREAM=None, CLASSES=None, AGGREGATION=None, TARGET_DEFINITION=None, TARGET_USER_SIZE=None, TARGET_USER_XMIN=None, TARGET_USER_XMAX=None, TARGET_USER_YMIN=None, TARGET_USER_YMAX=None, TARGET_USER_COLS=None, TARGET_USER_ROWS=None, TARGET_USER_FLAT=None, TARGET_USER_FITS=None, TARGET_SYSTEM=None, Verbose=2):
    '''
    Import Grid from Point Cloud
    ----------
    [io_pdal.2]\n
    This tool directly creates a grid from point cloud elevation data and supports several file formats using the "Point Data Abstraction Library" (PDAL).\n
    PDAL 2.7.2 (git-version: Release)\n
    Supported point cloud formats:\n
    ============\n
    Name	Extension	Description\n
    readers.copc	copc	COPC Reader\n
    readers.draco		Read data from a Draco array.\n
    readers.e57		Reader for E57 files\n
    readers.fbi	fbi	Fbi Reader\n
    readers.hdf		HDF Reader\n
    readers.i3s		I3S Reader\n
    readers.icebridge		NASA HDF5-based IceBridge ATM reader.\n
    See http://nsidc.org/data/docs/daac/icebridge/ilatm1b/index.html\n
    for more information.\n
    readers.ilvis2		ILVIS2 Reader\n
    readers.las	las;laz	ASPRS LAS 1.0 - 1.4 read support\n
    readers.nitf		NITF Reader\n
    readers.obj	obj	Obj Reader\n
    readers.pcd	pcd	Read data in the Point Cloud Library (PCL) format.\n
    readers.pgpointcloud		Read data from pgpointcloud format. "query" option needs to be a\n
    SQL statement selecting the data.\n
    readers.ply	ply	Read ply files.\n
    readers.pts	pts	Pts Reader\n
    readers.ptx	ptx	Ptx Reader\n
    readers.qfit	qi	QFIT Reader\n
    readers.sbet	sbet	SBET Reader\n
    readers.slpk		SLPK Reader\n
    readers.smrmsg	smrmsg	SBET smrmsg Reader\n
    readers.stac		STAC Reader\n
    readers.terrasolid	bin	TerraSolid Reader\n
    readers.text	csv;txt	Text Reader\n
    readers.tiledb		Read data from a TileDB array.\n
    readers.tindex	tindex	TileIndex Reader\n
    ============\n
    Arguments
    ----------
    - TARGET_TEMPLATE [`optional input grid`] : Target System. use this grid's system for output grids
    - GRID [`output grid`] : Grid
    - COUNT [`output grid`] : Number of Values
    - FILES [`file path`] : Files
    - STREAM [`boolean`] : Open Streamable. Default: 1 Use streamable file access if supported by driver. Reduces memory requirements.
    - CLASSES [`text`] : Classes. If classification is available only points are loaded whose identifiers are listed (comma separated, e.g. "2, 3, 4, 5, 6"). Ignored if empty.
    - AGGREGATION [`choice`] : Value Aggregation. Available Choices: [0] first [1] last [2] minimum [3] maximum [4] mean Default: 1 Output generated when more than one point falls into one target cell.
    - TARGET_DEFINITION [`choice`] : Target Grid System. Available Choices: [0] user defined [1] grid or grid system Default: 0
    - TARGET_USER_SIZE [`floating point number`] : Cellsize. Minimum: 0.000000 Default: 1.000000
    - TARGET_USER_XMIN [`floating point number`] : West. Default: 0.000000
    - TARGET_USER_XMAX [`floating point number`] : East. Default: 100.000000
    - TARGET_USER_YMIN [`floating point number`] : South. Default: 0.000000
    - TARGET_USER_YMAX [`floating point number`] : North. Default: 100.000000
    - TARGET_USER_COLS [`integer number`] : Columns. Minimum: 1 Default: 101 Number of cells in East-West direction.
    - TARGET_USER_ROWS [`integer number`] : Rows. Minimum: 1 Default: 101 Number of cells in North-South direction.
    - TARGET_USER_FLAT [`boolean`] : Rounding. Default: 1 Round bounding coordinates to multiples of cell size. Ignored if cell size has decimal places.
    - TARGET_USER_FITS [`choice`] : Fit. Available Choices: [0] nodes [1] cells Default: 0
    - TARGET_SYSTEM [`grid system`] : Grid System

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('io_pdal', '2', 'Import Grid from Point Cloud')
    if Tool.is_Okay():
        Tool.Set_Input ('TARGET_TEMPLATE', TARGET_TEMPLATE)
        Tool.Set_Output('GRID', GRID)
        Tool.Set_Output('COUNT', COUNT)
        Tool.Set_Option('FILES', FILES)
        Tool.Set_Option('STREAM', STREAM)
        Tool.Set_Option('CLASSES', CLASSES)
        Tool.Set_Option('AGGREGATION', AGGREGATION)
        Tool.Set_Option('TARGET_DEFINITION', TARGET_DEFINITION)
        Tool.Set_Option('TARGET_USER_SIZE', TARGET_USER_SIZE)
        Tool.Set_Option('TARGET_USER_XMIN', TARGET_USER_XMIN)
        Tool.Set_Option('TARGET_USER_XMAX', TARGET_USER_XMAX)
        Tool.Set_Option('TARGET_USER_YMIN', TARGET_USER_YMIN)
        Tool.Set_Option('TARGET_USER_YMAX', TARGET_USER_YMAX)
        Tool.Set_Option('TARGET_USER_COLS', TARGET_USER_COLS)
        Tool.Set_Option('TARGET_USER_ROWS', TARGET_USER_ROWS)
        Tool.Set_Option('TARGET_USER_FLAT', TARGET_USER_FLAT)
        Tool.Set_Option('TARGET_USER_FITS', TARGET_USER_FITS)
        Tool.Set_Option('TARGET_SYSTEM', TARGET_SYSTEM)
        return Tool.Execute(Verbose)
    return False

