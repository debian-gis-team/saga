#! /usr/bin/env python

'''
Python Interface to SAGA Tools Library
----------
- Category : Grid
- Name     : Spline Interpolation
- ID       : grid_spline

Description
----------
Several spline interpolation/approximation methods for the gridding of scattered data. In most cases the 'Multilevel B-spline Interpolation' might be the optimal choice. 
'''

from PySAGA.helper import Tool_Wrapper

def Thin_Plate_Spline(SHAPES=None, TARGET_TEMPLATE=None, TARGET_OUT_GRID=None, FIELD=None, TARGET_DEFINITION=None, TARGET_USER_SIZE=None, TARGET_USER_XMIN=None, TARGET_USER_XMAX=None, TARGET_USER_YMIN=None, TARGET_USER_YMAX=None, TARGET_USER_COLS=None, TARGET_USER_ROWS=None, TARGET_USER_FLAT=None, TARGET_USER_FITS=None, TARGET_SYSTEM=None, REGULARISATION=None, SEARCH_RANGE=None, SEARCH_RADIUS=None, SEARCH_POINTS_ALL=None, SEARCH_POINTS_MIN=None, SEARCH_POINTS_MAX=None, Verbose=2):
    '''
    Thin Plate Spline
    ----------
    [grid_spline.1]\n
    Creates a 'Thin Plate Spline' function for each grid point based on all of the scattered data points that are within a given distance. The number of points can be limited to a maximum number of closest points.\n
    Arguments
    ----------
    - SHAPES [`input shapes`] : Points
    - TARGET_TEMPLATE [`optional input grid`] : Target System. use this grid's system for output grids
    - TARGET_OUT_GRID [`output grid`] : Target Grid
    - FIELD [`table field`] : Attribute
    - TARGET_DEFINITION [`choice`] : Target Grid System. Available Choices: [0] user defined [1] grid or grid system Default: 0
    - TARGET_USER_SIZE [`floating point number`] : Cellsize. Minimum: 0.000000 Default: 1.000000
    - TARGET_USER_XMIN [`floating point number`] : West. Default: 0.000000
    - TARGET_USER_XMAX [`floating point number`] : East. Default: 100.000000
    - TARGET_USER_YMIN [`floating point number`] : South. Default: 0.000000
    - TARGET_USER_YMAX [`floating point number`] : North. Default: 100.000000
    - TARGET_USER_COLS [`integer number`] : Columns. Minimum: 1 Default: 101 Number of cells in East-West direction.
    - TARGET_USER_ROWS [`integer number`] : Rows. Minimum: 1 Default: 101 Number of cells in North-South direction.
    - TARGET_USER_FLAT [`boolean`] : Rounding. Default: 1 Round bounding coordinates to multiples of cell size. Ignored if cell size has decimal places.
    - TARGET_USER_FITS [`choice`] : Fit. Available Choices: [0] nodes [1] cells Default: 0
    - TARGET_SYSTEM [`grid system`] : Grid System
    - REGULARISATION [`floating point number`] : Regularisation. Minimum: 0.000000 Default: 0.000100
    - SEARCH_RANGE [`choice`] : Search Range. Available Choices: [0] local [1] global Default: 1
    - SEARCH_RADIUS [`floating point number`] : Maximum Search Distance. Minimum: 0.000000 Default: 1000.000000 local maximum search distance given in map units
    - SEARCH_POINTS_ALL [`choice`] : Number of Points. Available Choices: [0] maximum number of nearest points [1] all points within search distance Default: 1
    - SEARCH_POINTS_MIN [`integer number`] : Minimum. Minimum: 1 Default: 16 minimum number of points to use
    - SEARCH_POINTS_MAX [`integer number`] : Maximum. Minimum: 1 Default: 20 maximum number of nearest points

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_spline', '1', 'Thin Plate Spline')
    if Tool.is_Okay():
        Tool.Set_Input ('SHAPES', SHAPES)
        Tool.Set_Input ('TARGET_TEMPLATE', TARGET_TEMPLATE)
        Tool.Set_Output('TARGET_OUT_GRID', TARGET_OUT_GRID)
        Tool.Set_Option('FIELD', FIELD)
        Tool.Set_Option('TARGET_DEFINITION', TARGET_DEFINITION)
        Tool.Set_Option('TARGET_USER_SIZE', TARGET_USER_SIZE)
        Tool.Set_Option('TARGET_USER_XMIN', TARGET_USER_XMIN)
        Tool.Set_Option('TARGET_USER_XMAX', TARGET_USER_XMAX)
        Tool.Set_Option('TARGET_USER_YMIN', TARGET_USER_YMIN)
        Tool.Set_Option('TARGET_USER_YMAX', TARGET_USER_YMAX)
        Tool.Set_Option('TARGET_USER_COLS', TARGET_USER_COLS)
        Tool.Set_Option('TARGET_USER_ROWS', TARGET_USER_ROWS)
        Tool.Set_Option('TARGET_USER_FLAT', TARGET_USER_FLAT)
        Tool.Set_Option('TARGET_USER_FITS', TARGET_USER_FITS)
        Tool.Set_Option('TARGET_SYSTEM', TARGET_SYSTEM)
        Tool.Set_Option('REGULARISATION', REGULARISATION)
        Tool.Set_Option('SEARCH_RANGE', SEARCH_RANGE)
        Tool.Set_Option('SEARCH_RADIUS', SEARCH_RADIUS)
        Tool.Set_Option('SEARCH_POINTS_ALL', SEARCH_POINTS_ALL)
        Tool.Set_Option('SEARCH_POINTS_MIN', SEARCH_POINTS_MIN)
        Tool.Set_Option('SEARCH_POINTS_MAX', SEARCH_POINTS_MAX)
        return Tool.Execute(Verbose)
    return False

def run_tool_grid_spline_1(SHAPES=None, TARGET_TEMPLATE=None, TARGET_OUT_GRID=None, FIELD=None, TARGET_DEFINITION=None, TARGET_USER_SIZE=None, TARGET_USER_XMIN=None, TARGET_USER_XMAX=None, TARGET_USER_YMIN=None, TARGET_USER_YMAX=None, TARGET_USER_COLS=None, TARGET_USER_ROWS=None, TARGET_USER_FLAT=None, TARGET_USER_FITS=None, TARGET_SYSTEM=None, REGULARISATION=None, SEARCH_RANGE=None, SEARCH_RADIUS=None, SEARCH_POINTS_ALL=None, SEARCH_POINTS_MIN=None, SEARCH_POINTS_MAX=None, Verbose=2):
    '''
    Thin Plate Spline
    ----------
    [grid_spline.1]\n
    Creates a 'Thin Plate Spline' function for each grid point based on all of the scattered data points that are within a given distance. The number of points can be limited to a maximum number of closest points.\n
    Arguments
    ----------
    - SHAPES [`input shapes`] : Points
    - TARGET_TEMPLATE [`optional input grid`] : Target System. use this grid's system for output grids
    - TARGET_OUT_GRID [`output grid`] : Target Grid
    - FIELD [`table field`] : Attribute
    - TARGET_DEFINITION [`choice`] : Target Grid System. Available Choices: [0] user defined [1] grid or grid system Default: 0
    - TARGET_USER_SIZE [`floating point number`] : Cellsize. Minimum: 0.000000 Default: 1.000000
    - TARGET_USER_XMIN [`floating point number`] : West. Default: 0.000000
    - TARGET_USER_XMAX [`floating point number`] : East. Default: 100.000000
    - TARGET_USER_YMIN [`floating point number`] : South. Default: 0.000000
    - TARGET_USER_YMAX [`floating point number`] : North. Default: 100.000000
    - TARGET_USER_COLS [`integer number`] : Columns. Minimum: 1 Default: 101 Number of cells in East-West direction.
    - TARGET_USER_ROWS [`integer number`] : Rows. Minimum: 1 Default: 101 Number of cells in North-South direction.
    - TARGET_USER_FLAT [`boolean`] : Rounding. Default: 1 Round bounding coordinates to multiples of cell size. Ignored if cell size has decimal places.
    - TARGET_USER_FITS [`choice`] : Fit. Available Choices: [0] nodes [1] cells Default: 0
    - TARGET_SYSTEM [`grid system`] : Grid System
    - REGULARISATION [`floating point number`] : Regularisation. Minimum: 0.000000 Default: 0.000100
    - SEARCH_RANGE [`choice`] : Search Range. Available Choices: [0] local [1] global Default: 1
    - SEARCH_RADIUS [`floating point number`] : Maximum Search Distance. Minimum: 0.000000 Default: 1000.000000 local maximum search distance given in map units
    - SEARCH_POINTS_ALL [`choice`] : Number of Points. Available Choices: [0] maximum number of nearest points [1] all points within search distance Default: 1
    - SEARCH_POINTS_MIN [`integer number`] : Minimum. Minimum: 1 Default: 16 minimum number of points to use
    - SEARCH_POINTS_MAX [`integer number`] : Maximum. Minimum: 1 Default: 20 maximum number of nearest points

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_spline', '1', 'Thin Plate Spline')
    if Tool.is_Okay():
        Tool.Set_Input ('SHAPES', SHAPES)
        Tool.Set_Input ('TARGET_TEMPLATE', TARGET_TEMPLATE)
        Tool.Set_Output('TARGET_OUT_GRID', TARGET_OUT_GRID)
        Tool.Set_Option('FIELD', FIELD)
        Tool.Set_Option('TARGET_DEFINITION', TARGET_DEFINITION)
        Tool.Set_Option('TARGET_USER_SIZE', TARGET_USER_SIZE)
        Tool.Set_Option('TARGET_USER_XMIN', TARGET_USER_XMIN)
        Tool.Set_Option('TARGET_USER_XMAX', TARGET_USER_XMAX)
        Tool.Set_Option('TARGET_USER_YMIN', TARGET_USER_YMIN)
        Tool.Set_Option('TARGET_USER_YMAX', TARGET_USER_YMAX)
        Tool.Set_Option('TARGET_USER_COLS', TARGET_USER_COLS)
        Tool.Set_Option('TARGET_USER_ROWS', TARGET_USER_ROWS)
        Tool.Set_Option('TARGET_USER_FLAT', TARGET_USER_FLAT)
        Tool.Set_Option('TARGET_USER_FITS', TARGET_USER_FITS)
        Tool.Set_Option('TARGET_SYSTEM', TARGET_SYSTEM)
        Tool.Set_Option('REGULARISATION', REGULARISATION)
        Tool.Set_Option('SEARCH_RANGE', SEARCH_RANGE)
        Tool.Set_Option('SEARCH_RADIUS', SEARCH_RADIUS)
        Tool.Set_Option('SEARCH_POINTS_ALL', SEARCH_POINTS_ALL)
        Tool.Set_Option('SEARCH_POINTS_MIN', SEARCH_POINTS_MIN)
        Tool.Set_Option('SEARCH_POINTS_MAX', SEARCH_POINTS_MAX)
        return Tool.Execute(Verbose)
    return False

def Thin_Plate_Spline_TIN(SHAPES=None, TARGET_TEMPLATE=None, TARGET_OUT_GRID=None, FIELD=None, TARGET_DEFINITION=None, TARGET_USER_SIZE=None, TARGET_USER_XMIN=None, TARGET_USER_XMAX=None, TARGET_USER_YMIN=None, TARGET_USER_YMAX=None, TARGET_USER_COLS=None, TARGET_USER_ROWS=None, TARGET_USER_FLAT=None, TARGET_USER_FITS=None, TARGET_SYSTEM=None, REGULARISATION=None, LEVEL=None, FRAME=None, Verbose=2):
    '''
    Thin Plate Spline (TIN)
    ----------
    [grid_spline.2]\n
    Creates a 'Thin Plate Spline' function for each triangle of a TIN and uses it for subsequent gridding. The TIN is internally created from the scattered data points input. The 'Neighbourhood' option determines the number of points used for the spline generation. 'Immediate neighbourhood' includes the points of the triangle as well as the immediate neighbour points. 'Level 1' adds the neighbours of the immediate neighbourhood and 'level 2' adds the neighbours of 'level 1' neighbours too. A higher neighbourhood degree reduces sharp breaks but also increases the computation time.\n
    Arguments
    ----------
    - SHAPES [`input shapes`] : Points
    - TARGET_TEMPLATE [`optional input grid`] : Target System. use this grid's system for output grids
    - TARGET_OUT_GRID [`output grid`] : Target Grid
    - FIELD [`table field`] : Attribute
    - TARGET_DEFINITION [`choice`] : Target Grid System. Available Choices: [0] user defined [1] grid or grid system Default: 0
    - TARGET_USER_SIZE [`floating point number`] : Cellsize. Minimum: 0.000000 Default: 1.000000
    - TARGET_USER_XMIN [`floating point number`] : West. Default: 0.000000
    - TARGET_USER_XMAX [`floating point number`] : East. Default: 100.000000
    - TARGET_USER_YMIN [`floating point number`] : South. Default: 0.000000
    - TARGET_USER_YMAX [`floating point number`] : North. Default: 100.000000
    - TARGET_USER_COLS [`integer number`] : Columns. Minimum: 1 Default: 101 Number of cells in East-West direction.
    - TARGET_USER_ROWS [`integer number`] : Rows. Minimum: 1 Default: 101 Number of cells in North-South direction.
    - TARGET_USER_FLAT [`boolean`] : Rounding. Default: 1 Round bounding coordinates to multiples of cell size. Ignored if cell size has decimal places.
    - TARGET_USER_FITS [`choice`] : Fit. Available Choices: [0] nodes [1] cells Default: 0
    - TARGET_SYSTEM [`grid system`] : Grid System
    - REGULARISATION [`floating point number`] : Regularisation. Minimum: 0.000000 Default: 0.000100
    - LEVEL [`choice`] : Neighbourhood. Available Choices: [0] immediate [1] level 1 [2] level 2 Default: 1
    - FRAME [`boolean`] : Add Frame. Default: 1

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_spline', '2', 'Thin Plate Spline (TIN)')
    if Tool.is_Okay():
        Tool.Set_Input ('SHAPES', SHAPES)
        Tool.Set_Input ('TARGET_TEMPLATE', TARGET_TEMPLATE)
        Tool.Set_Output('TARGET_OUT_GRID', TARGET_OUT_GRID)
        Tool.Set_Option('FIELD', FIELD)
        Tool.Set_Option('TARGET_DEFINITION', TARGET_DEFINITION)
        Tool.Set_Option('TARGET_USER_SIZE', TARGET_USER_SIZE)
        Tool.Set_Option('TARGET_USER_XMIN', TARGET_USER_XMIN)
        Tool.Set_Option('TARGET_USER_XMAX', TARGET_USER_XMAX)
        Tool.Set_Option('TARGET_USER_YMIN', TARGET_USER_YMIN)
        Tool.Set_Option('TARGET_USER_YMAX', TARGET_USER_YMAX)
        Tool.Set_Option('TARGET_USER_COLS', TARGET_USER_COLS)
        Tool.Set_Option('TARGET_USER_ROWS', TARGET_USER_ROWS)
        Tool.Set_Option('TARGET_USER_FLAT', TARGET_USER_FLAT)
        Tool.Set_Option('TARGET_USER_FITS', TARGET_USER_FITS)
        Tool.Set_Option('TARGET_SYSTEM', TARGET_SYSTEM)
        Tool.Set_Option('REGULARISATION', REGULARISATION)
        Tool.Set_Option('LEVEL', LEVEL)
        Tool.Set_Option('FRAME', FRAME)
        return Tool.Execute(Verbose)
    return False

def run_tool_grid_spline_2(SHAPES=None, TARGET_TEMPLATE=None, TARGET_OUT_GRID=None, FIELD=None, TARGET_DEFINITION=None, TARGET_USER_SIZE=None, TARGET_USER_XMIN=None, TARGET_USER_XMAX=None, TARGET_USER_YMIN=None, TARGET_USER_YMAX=None, TARGET_USER_COLS=None, TARGET_USER_ROWS=None, TARGET_USER_FLAT=None, TARGET_USER_FITS=None, TARGET_SYSTEM=None, REGULARISATION=None, LEVEL=None, FRAME=None, Verbose=2):
    '''
    Thin Plate Spline (TIN)
    ----------
    [grid_spline.2]\n
    Creates a 'Thin Plate Spline' function for each triangle of a TIN and uses it for subsequent gridding. The TIN is internally created from the scattered data points input. The 'Neighbourhood' option determines the number of points used for the spline generation. 'Immediate neighbourhood' includes the points of the triangle as well as the immediate neighbour points. 'Level 1' adds the neighbours of the immediate neighbourhood and 'level 2' adds the neighbours of 'level 1' neighbours too. A higher neighbourhood degree reduces sharp breaks but also increases the computation time.\n
    Arguments
    ----------
    - SHAPES [`input shapes`] : Points
    - TARGET_TEMPLATE [`optional input grid`] : Target System. use this grid's system for output grids
    - TARGET_OUT_GRID [`output grid`] : Target Grid
    - FIELD [`table field`] : Attribute
    - TARGET_DEFINITION [`choice`] : Target Grid System. Available Choices: [0] user defined [1] grid or grid system Default: 0
    - TARGET_USER_SIZE [`floating point number`] : Cellsize. Minimum: 0.000000 Default: 1.000000
    - TARGET_USER_XMIN [`floating point number`] : West. Default: 0.000000
    - TARGET_USER_XMAX [`floating point number`] : East. Default: 100.000000
    - TARGET_USER_YMIN [`floating point number`] : South. Default: 0.000000
    - TARGET_USER_YMAX [`floating point number`] : North. Default: 100.000000
    - TARGET_USER_COLS [`integer number`] : Columns. Minimum: 1 Default: 101 Number of cells in East-West direction.
    - TARGET_USER_ROWS [`integer number`] : Rows. Minimum: 1 Default: 101 Number of cells in North-South direction.
    - TARGET_USER_FLAT [`boolean`] : Rounding. Default: 1 Round bounding coordinates to multiples of cell size. Ignored if cell size has decimal places.
    - TARGET_USER_FITS [`choice`] : Fit. Available Choices: [0] nodes [1] cells Default: 0
    - TARGET_SYSTEM [`grid system`] : Grid System
    - REGULARISATION [`floating point number`] : Regularisation. Minimum: 0.000000 Default: 0.000100
    - LEVEL [`choice`] : Neighbourhood. Available Choices: [0] immediate [1] level 1 [2] level 2 Default: 1
    - FRAME [`boolean`] : Add Frame. Default: 1

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_spline', '2', 'Thin Plate Spline (TIN)')
    if Tool.is_Okay():
        Tool.Set_Input ('SHAPES', SHAPES)
        Tool.Set_Input ('TARGET_TEMPLATE', TARGET_TEMPLATE)
        Tool.Set_Output('TARGET_OUT_GRID', TARGET_OUT_GRID)
        Tool.Set_Option('FIELD', FIELD)
        Tool.Set_Option('TARGET_DEFINITION', TARGET_DEFINITION)
        Tool.Set_Option('TARGET_USER_SIZE', TARGET_USER_SIZE)
        Tool.Set_Option('TARGET_USER_XMIN', TARGET_USER_XMIN)
        Tool.Set_Option('TARGET_USER_XMAX', TARGET_USER_XMAX)
        Tool.Set_Option('TARGET_USER_YMIN', TARGET_USER_YMIN)
        Tool.Set_Option('TARGET_USER_YMAX', TARGET_USER_YMAX)
        Tool.Set_Option('TARGET_USER_COLS', TARGET_USER_COLS)
        Tool.Set_Option('TARGET_USER_ROWS', TARGET_USER_ROWS)
        Tool.Set_Option('TARGET_USER_FLAT', TARGET_USER_FLAT)
        Tool.Set_Option('TARGET_USER_FITS', TARGET_USER_FITS)
        Tool.Set_Option('TARGET_SYSTEM', TARGET_SYSTEM)
        Tool.Set_Option('REGULARISATION', REGULARISATION)
        Tool.Set_Option('LEVEL', LEVEL)
        Tool.Set_Option('FRAME', FRAME)
        return Tool.Execute(Verbose)
    return False

def BSpline_Approximation(SHAPES=None, TARGET_TEMPLATE=None, TARGET_OUT_GRID=None, FIELD=None, TARGET_DEFINITION=None, TARGET_USER_SIZE=None, TARGET_USER_XMIN=None, TARGET_USER_XMAX=None, TARGET_USER_YMIN=None, TARGET_USER_YMAX=None, TARGET_USER_COLS=None, TARGET_USER_ROWS=None, TARGET_USER_FLAT=None, TARGET_USER_FITS=None, TARGET_SYSTEM=None, LEVEL=None, Verbose=2):
    '''
    B-Spline Approximation
    ----------
    [grid_spline.3]\n
    Calculates B-spline functions for chosen level of detail. This tool serves as the basis for the 'Multilevel B-spline Interpolation' and is not suited as is for spatial data interpolation from scattered data.\n
    Arguments
    ----------
    - SHAPES [`input shapes`] : Points
    - TARGET_TEMPLATE [`optional input grid`] : Target System. use this grid's system for output grids
    - TARGET_OUT_GRID [`output grid`] : Target Grid
    - FIELD [`table field`] : Attribute
    - TARGET_DEFINITION [`choice`] : Target Grid System. Available Choices: [0] user defined [1] grid or grid system Default: 0
    - TARGET_USER_SIZE [`floating point number`] : Cellsize. Minimum: 0.000000 Default: 1.000000
    - TARGET_USER_XMIN [`floating point number`] : West. Default: 0.000000
    - TARGET_USER_XMAX [`floating point number`] : East. Default: 100.000000
    - TARGET_USER_YMIN [`floating point number`] : South. Default: 0.000000
    - TARGET_USER_YMAX [`floating point number`] : North. Default: 100.000000
    - TARGET_USER_COLS [`integer number`] : Columns. Minimum: 1 Default: 101 Number of cells in East-West direction.
    - TARGET_USER_ROWS [`integer number`] : Rows. Minimum: 1 Default: 101 Number of cells in North-South direction.
    - TARGET_USER_FLAT [`boolean`] : Rounding. Default: 1 Round bounding coordinates to multiples of cell size. Ignored if cell size has decimal places.
    - TARGET_USER_FITS [`choice`] : Fit. Available Choices: [0] nodes [1] cells Default: 0
    - TARGET_SYSTEM [`grid system`] : Grid System
    - LEVEL [`floating point number`] : Range. Minimum: 0.001000 Default: 1.000000 B-spline range expressed as number of cells.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_spline', '3', 'B-Spline Approximation')
    if Tool.is_Okay():
        Tool.Set_Input ('SHAPES', SHAPES)
        Tool.Set_Input ('TARGET_TEMPLATE', TARGET_TEMPLATE)
        Tool.Set_Output('TARGET_OUT_GRID', TARGET_OUT_GRID)
        Tool.Set_Option('FIELD', FIELD)
        Tool.Set_Option('TARGET_DEFINITION', TARGET_DEFINITION)
        Tool.Set_Option('TARGET_USER_SIZE', TARGET_USER_SIZE)
        Tool.Set_Option('TARGET_USER_XMIN', TARGET_USER_XMIN)
        Tool.Set_Option('TARGET_USER_XMAX', TARGET_USER_XMAX)
        Tool.Set_Option('TARGET_USER_YMIN', TARGET_USER_YMIN)
        Tool.Set_Option('TARGET_USER_YMAX', TARGET_USER_YMAX)
        Tool.Set_Option('TARGET_USER_COLS', TARGET_USER_COLS)
        Tool.Set_Option('TARGET_USER_ROWS', TARGET_USER_ROWS)
        Tool.Set_Option('TARGET_USER_FLAT', TARGET_USER_FLAT)
        Tool.Set_Option('TARGET_USER_FITS', TARGET_USER_FITS)
        Tool.Set_Option('TARGET_SYSTEM', TARGET_SYSTEM)
        Tool.Set_Option('LEVEL', LEVEL)
        return Tool.Execute(Verbose)
    return False

def run_tool_grid_spline_3(SHAPES=None, TARGET_TEMPLATE=None, TARGET_OUT_GRID=None, FIELD=None, TARGET_DEFINITION=None, TARGET_USER_SIZE=None, TARGET_USER_XMIN=None, TARGET_USER_XMAX=None, TARGET_USER_YMIN=None, TARGET_USER_YMAX=None, TARGET_USER_COLS=None, TARGET_USER_ROWS=None, TARGET_USER_FLAT=None, TARGET_USER_FITS=None, TARGET_SYSTEM=None, LEVEL=None, Verbose=2):
    '''
    B-Spline Approximation
    ----------
    [grid_spline.3]\n
    Calculates B-spline functions for chosen level of detail. This tool serves as the basis for the 'Multilevel B-spline Interpolation' and is not suited as is for spatial data interpolation from scattered data.\n
    Arguments
    ----------
    - SHAPES [`input shapes`] : Points
    - TARGET_TEMPLATE [`optional input grid`] : Target System. use this grid's system for output grids
    - TARGET_OUT_GRID [`output grid`] : Target Grid
    - FIELD [`table field`] : Attribute
    - TARGET_DEFINITION [`choice`] : Target Grid System. Available Choices: [0] user defined [1] grid or grid system Default: 0
    - TARGET_USER_SIZE [`floating point number`] : Cellsize. Minimum: 0.000000 Default: 1.000000
    - TARGET_USER_XMIN [`floating point number`] : West. Default: 0.000000
    - TARGET_USER_XMAX [`floating point number`] : East. Default: 100.000000
    - TARGET_USER_YMIN [`floating point number`] : South. Default: 0.000000
    - TARGET_USER_YMAX [`floating point number`] : North. Default: 100.000000
    - TARGET_USER_COLS [`integer number`] : Columns. Minimum: 1 Default: 101 Number of cells in East-West direction.
    - TARGET_USER_ROWS [`integer number`] : Rows. Minimum: 1 Default: 101 Number of cells in North-South direction.
    - TARGET_USER_FLAT [`boolean`] : Rounding. Default: 1 Round bounding coordinates to multiples of cell size. Ignored if cell size has decimal places.
    - TARGET_USER_FITS [`choice`] : Fit. Available Choices: [0] nodes [1] cells Default: 0
    - TARGET_SYSTEM [`grid system`] : Grid System
    - LEVEL [`floating point number`] : Range. Minimum: 0.001000 Default: 1.000000 B-spline range expressed as number of cells.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_spline', '3', 'B-Spline Approximation')
    if Tool.is_Okay():
        Tool.Set_Input ('SHAPES', SHAPES)
        Tool.Set_Input ('TARGET_TEMPLATE', TARGET_TEMPLATE)
        Tool.Set_Output('TARGET_OUT_GRID', TARGET_OUT_GRID)
        Tool.Set_Option('FIELD', FIELD)
        Tool.Set_Option('TARGET_DEFINITION', TARGET_DEFINITION)
        Tool.Set_Option('TARGET_USER_SIZE', TARGET_USER_SIZE)
        Tool.Set_Option('TARGET_USER_XMIN', TARGET_USER_XMIN)
        Tool.Set_Option('TARGET_USER_XMAX', TARGET_USER_XMAX)
        Tool.Set_Option('TARGET_USER_YMIN', TARGET_USER_YMIN)
        Tool.Set_Option('TARGET_USER_YMAX', TARGET_USER_YMAX)
        Tool.Set_Option('TARGET_USER_COLS', TARGET_USER_COLS)
        Tool.Set_Option('TARGET_USER_ROWS', TARGET_USER_ROWS)
        Tool.Set_Option('TARGET_USER_FLAT', TARGET_USER_FLAT)
        Tool.Set_Option('TARGET_USER_FITS', TARGET_USER_FITS)
        Tool.Set_Option('TARGET_SYSTEM', TARGET_SYSTEM)
        Tool.Set_Option('LEVEL', LEVEL)
        return Tool.Execute(Verbose)
    return False

def Multilevel_BSpline(SHAPES=None, TARGET_TEMPLATE=None, TARGET_OUT_GRID=None, FIELD=None, TARGET_DEFINITION=None, TARGET_USER_SIZE=None, TARGET_USER_XMIN=None, TARGET_USER_XMAX=None, TARGET_USER_YMIN=None, TARGET_USER_YMAX=None, TARGET_USER_COLS=None, TARGET_USER_ROWS=None, TARGET_USER_FLAT=None, TARGET_USER_FITS=None, TARGET_SYSTEM=None, METHOD=None, EPSILON=None, LEVEL_MAX=None, Verbose=2):
    '''
    Multilevel B-Spline
    ----------
    [grid_spline.4]\n
    Multilevel B-spline algorithm for spatial interpolation of scattered data as proposed by Lee, Wolberg and Shin (1997).\n
    The algorithm makes use of a coarse-to-fine hierarchy of control lattices to generate a sequence of bicubic B-spline functions, whose sum approaches the desired interpolation function. Performance gains are realized by using B-spline refinement to reduce the sum of these functions into one equivalent B-spline function.\n
    The 'Maximum Level' determines the maximum size of the final B-spline matrix and increases exponential with each level. Where level=10 requires about 1mb level=12 needs about 16mb and level=14 about 256mb(!) of additional memory.\n
    Arguments
    ----------
    - SHAPES [`input shapes`] : Points
    - TARGET_TEMPLATE [`optional input grid`] : Target System. use this grid's system for output grids
    - TARGET_OUT_GRID [`output grid`] : Target Grid
    - FIELD [`table field`] : Attribute
    - TARGET_DEFINITION [`choice`] : Target Grid System. Available Choices: [0] user defined [1] grid or grid system Default: 0
    - TARGET_USER_SIZE [`floating point number`] : Cellsize. Minimum: 0.000000 Default: 1.000000
    - TARGET_USER_XMIN [`floating point number`] : West. Default: 0.000000
    - TARGET_USER_XMAX [`floating point number`] : East. Default: 100.000000
    - TARGET_USER_YMIN [`floating point number`] : South. Default: 0.000000
    - TARGET_USER_YMAX [`floating point number`] : North. Default: 100.000000
    - TARGET_USER_COLS [`integer number`] : Columns. Minimum: 1 Default: 101 Number of cells in East-West direction.
    - TARGET_USER_ROWS [`integer number`] : Rows. Minimum: 1 Default: 101 Number of cells in North-South direction.
    - TARGET_USER_FLAT [`boolean`] : Rounding. Default: 1 Round bounding coordinates to multiples of cell size. Ignored if cell size has decimal places.
    - TARGET_USER_FITS [`choice`] : Fit. Available Choices: [0] nodes [1] cells Default: 0
    - TARGET_SYSTEM [`grid system`] : Grid System
    - METHOD [`choice`] : Refinement. Available Choices: [0] no [1] yes Default: 0
    - EPSILON [`floating point number`] : Threshold Error. Minimum: 0.000000 Default: 0.000100
    - LEVEL_MAX [`integer number`] : Maximum Level. Minimum: 1 Maximum: 14 Default: 11

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_spline', '4', 'Multilevel B-Spline')
    if Tool.is_Okay():
        Tool.Set_Input ('SHAPES', SHAPES)
        Tool.Set_Input ('TARGET_TEMPLATE', TARGET_TEMPLATE)
        Tool.Set_Output('TARGET_OUT_GRID', TARGET_OUT_GRID)
        Tool.Set_Option('FIELD', FIELD)
        Tool.Set_Option('TARGET_DEFINITION', TARGET_DEFINITION)
        Tool.Set_Option('TARGET_USER_SIZE', TARGET_USER_SIZE)
        Tool.Set_Option('TARGET_USER_XMIN', TARGET_USER_XMIN)
        Tool.Set_Option('TARGET_USER_XMAX', TARGET_USER_XMAX)
        Tool.Set_Option('TARGET_USER_YMIN', TARGET_USER_YMIN)
        Tool.Set_Option('TARGET_USER_YMAX', TARGET_USER_YMAX)
        Tool.Set_Option('TARGET_USER_COLS', TARGET_USER_COLS)
        Tool.Set_Option('TARGET_USER_ROWS', TARGET_USER_ROWS)
        Tool.Set_Option('TARGET_USER_FLAT', TARGET_USER_FLAT)
        Tool.Set_Option('TARGET_USER_FITS', TARGET_USER_FITS)
        Tool.Set_Option('TARGET_SYSTEM', TARGET_SYSTEM)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('EPSILON', EPSILON)
        Tool.Set_Option('LEVEL_MAX', LEVEL_MAX)
        return Tool.Execute(Verbose)
    return False

def run_tool_grid_spline_4(SHAPES=None, TARGET_TEMPLATE=None, TARGET_OUT_GRID=None, FIELD=None, TARGET_DEFINITION=None, TARGET_USER_SIZE=None, TARGET_USER_XMIN=None, TARGET_USER_XMAX=None, TARGET_USER_YMIN=None, TARGET_USER_YMAX=None, TARGET_USER_COLS=None, TARGET_USER_ROWS=None, TARGET_USER_FLAT=None, TARGET_USER_FITS=None, TARGET_SYSTEM=None, METHOD=None, EPSILON=None, LEVEL_MAX=None, Verbose=2):
    '''
    Multilevel B-Spline
    ----------
    [grid_spline.4]\n
    Multilevel B-spline algorithm for spatial interpolation of scattered data as proposed by Lee, Wolberg and Shin (1997).\n
    The algorithm makes use of a coarse-to-fine hierarchy of control lattices to generate a sequence of bicubic B-spline functions, whose sum approaches the desired interpolation function. Performance gains are realized by using B-spline refinement to reduce the sum of these functions into one equivalent B-spline function.\n
    The 'Maximum Level' determines the maximum size of the final B-spline matrix and increases exponential with each level. Where level=10 requires about 1mb level=12 needs about 16mb and level=14 about 256mb(!) of additional memory.\n
    Arguments
    ----------
    - SHAPES [`input shapes`] : Points
    - TARGET_TEMPLATE [`optional input grid`] : Target System. use this grid's system for output grids
    - TARGET_OUT_GRID [`output grid`] : Target Grid
    - FIELD [`table field`] : Attribute
    - TARGET_DEFINITION [`choice`] : Target Grid System. Available Choices: [0] user defined [1] grid or grid system Default: 0
    - TARGET_USER_SIZE [`floating point number`] : Cellsize. Minimum: 0.000000 Default: 1.000000
    - TARGET_USER_XMIN [`floating point number`] : West. Default: 0.000000
    - TARGET_USER_XMAX [`floating point number`] : East. Default: 100.000000
    - TARGET_USER_YMIN [`floating point number`] : South. Default: 0.000000
    - TARGET_USER_YMAX [`floating point number`] : North. Default: 100.000000
    - TARGET_USER_COLS [`integer number`] : Columns. Minimum: 1 Default: 101 Number of cells in East-West direction.
    - TARGET_USER_ROWS [`integer number`] : Rows. Minimum: 1 Default: 101 Number of cells in North-South direction.
    - TARGET_USER_FLAT [`boolean`] : Rounding. Default: 1 Round bounding coordinates to multiples of cell size. Ignored if cell size has decimal places.
    - TARGET_USER_FITS [`choice`] : Fit. Available Choices: [0] nodes [1] cells Default: 0
    - TARGET_SYSTEM [`grid system`] : Grid System
    - METHOD [`choice`] : Refinement. Available Choices: [0] no [1] yes Default: 0
    - EPSILON [`floating point number`] : Threshold Error. Minimum: 0.000000 Default: 0.000100
    - LEVEL_MAX [`integer number`] : Maximum Level. Minimum: 1 Maximum: 14 Default: 11

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_spline', '4', 'Multilevel B-Spline')
    if Tool.is_Okay():
        Tool.Set_Input ('SHAPES', SHAPES)
        Tool.Set_Input ('TARGET_TEMPLATE', TARGET_TEMPLATE)
        Tool.Set_Output('TARGET_OUT_GRID', TARGET_OUT_GRID)
        Tool.Set_Option('FIELD', FIELD)
        Tool.Set_Option('TARGET_DEFINITION', TARGET_DEFINITION)
        Tool.Set_Option('TARGET_USER_SIZE', TARGET_USER_SIZE)
        Tool.Set_Option('TARGET_USER_XMIN', TARGET_USER_XMIN)
        Tool.Set_Option('TARGET_USER_XMAX', TARGET_USER_XMAX)
        Tool.Set_Option('TARGET_USER_YMIN', TARGET_USER_YMIN)
        Tool.Set_Option('TARGET_USER_YMAX', TARGET_USER_YMAX)
        Tool.Set_Option('TARGET_USER_COLS', TARGET_USER_COLS)
        Tool.Set_Option('TARGET_USER_ROWS', TARGET_USER_ROWS)
        Tool.Set_Option('TARGET_USER_FLAT', TARGET_USER_FLAT)
        Tool.Set_Option('TARGET_USER_FITS', TARGET_USER_FITS)
        Tool.Set_Option('TARGET_SYSTEM', TARGET_SYSTEM)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('EPSILON', EPSILON)
        Tool.Set_Option('LEVEL_MAX', LEVEL_MAX)
        return Tool.Execute(Verbose)
    return False

def Multilevel_BSpline_from_Grid_Points(GRID=None, TARGET_TEMPLATE=None, TARGET_OUT_GRID=None, GRID_GRIDSYSTEM=None, TARGET_DEFINITION=None, TARGET_USER_SIZE=None, TARGET_USER_XMIN=None, TARGET_USER_XMAX=None, TARGET_USER_YMIN=None, TARGET_USER_YMAX=None, TARGET_USER_COLS=None, TARGET_USER_ROWS=None, TARGET_USER_FLAT=None, TARGET_USER_FITS=None, TARGET_SYSTEM=None, DATATYPE=None, METHOD=None, EPSILON=None, LEVEL_MAX=None, Verbose=2):
    '''
    Multilevel B-Spline from Grid Points
    ----------
    [grid_spline.5]\n
    Multilevel B-spline algorithm for spatial interpolation of scattered data as proposed by Lee, Wolberg and Shin (1997). The algorithm makes use of a coarse-to-fine hierarchy of control lattices to generate a sequence of bicubic B-spline functions, whose sum approaches the desired interpolation function. Large performance gains are realized by using B-spline refinement to reduce the sum of these functions into one equivalent B-spline function.\n
    The 'Maximum Level' determines the maximum size of the final B-spline matrix and increases exponential with each level. Where level=10 requires about 1mb level=12 needs about 16mb and level=14 about 256mb(!) of additional memory.\n
    Arguments
    ----------
    - GRID [`input grid`] : Grid
    - TARGET_TEMPLATE [`optional input grid`] : Target System. use this grid's system for output grids
    - TARGET_OUT_GRID [`output grid`] : Target Grid
    - GRID_GRIDSYSTEM [`grid system`] : Grid system
    - TARGET_DEFINITION [`choice`] : Target Grid System. Available Choices: [0] user defined [1] grid or grid system Default: 0
    - TARGET_USER_SIZE [`floating point number`] : Cellsize. Minimum: 0.000000 Default: 1.000000
    - TARGET_USER_XMIN [`floating point number`] : West. Default: 0.000000
    - TARGET_USER_XMAX [`floating point number`] : East. Default: 100.000000
    - TARGET_USER_YMIN [`floating point number`] : South. Default: 0.000000
    - TARGET_USER_YMAX [`floating point number`] : North. Default: 100.000000
    - TARGET_USER_COLS [`integer number`] : Columns. Minimum: 1 Default: 101 Number of cells in East-West direction.
    - TARGET_USER_ROWS [`integer number`] : Rows. Minimum: 1 Default: 101 Number of cells in North-South direction.
    - TARGET_USER_FLAT [`boolean`] : Rounding. Default: 1 Round bounding coordinates to multiples of cell size. Ignored if cell size has decimal places.
    - TARGET_USER_FITS [`choice`] : Fit. Available Choices: [0] nodes [1] cells Default: 0
    - TARGET_SYSTEM [`grid system`] : Grid System
    - DATATYPE [`choice`] : Data Type. Available Choices: [0] same as input grid [1] 4 byte floating point number Default: 0
    - METHOD [`choice`] : Refinement. Available Choices: [0] no [1] yes Default: 0
    - EPSILON [`floating point number`] : Threshold Error. Minimum: 0.000000 Default: 0.000100
    - LEVEL_MAX [`integer number`] : Maximum Level. Minimum: 1 Maximum: 14 Default: 11

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_spline', '5', 'Multilevel B-Spline from Grid Points')
    if Tool.is_Okay():
        Tool.Set_Input ('GRID', GRID)
        Tool.Set_Input ('TARGET_TEMPLATE', TARGET_TEMPLATE)
        Tool.Set_Output('TARGET_OUT_GRID', TARGET_OUT_GRID)
        Tool.Set_Option('GRID_GRIDSYSTEM', GRID_GRIDSYSTEM)
        Tool.Set_Option('TARGET_DEFINITION', TARGET_DEFINITION)
        Tool.Set_Option('TARGET_USER_SIZE', TARGET_USER_SIZE)
        Tool.Set_Option('TARGET_USER_XMIN', TARGET_USER_XMIN)
        Tool.Set_Option('TARGET_USER_XMAX', TARGET_USER_XMAX)
        Tool.Set_Option('TARGET_USER_YMIN', TARGET_USER_YMIN)
        Tool.Set_Option('TARGET_USER_YMAX', TARGET_USER_YMAX)
        Tool.Set_Option('TARGET_USER_COLS', TARGET_USER_COLS)
        Tool.Set_Option('TARGET_USER_ROWS', TARGET_USER_ROWS)
        Tool.Set_Option('TARGET_USER_FLAT', TARGET_USER_FLAT)
        Tool.Set_Option('TARGET_USER_FITS', TARGET_USER_FITS)
        Tool.Set_Option('TARGET_SYSTEM', TARGET_SYSTEM)
        Tool.Set_Option('DATATYPE', DATATYPE)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('EPSILON', EPSILON)
        Tool.Set_Option('LEVEL_MAX', LEVEL_MAX)
        return Tool.Execute(Verbose)
    return False

def run_tool_grid_spline_5(GRID=None, TARGET_TEMPLATE=None, TARGET_OUT_GRID=None, GRID_GRIDSYSTEM=None, TARGET_DEFINITION=None, TARGET_USER_SIZE=None, TARGET_USER_XMIN=None, TARGET_USER_XMAX=None, TARGET_USER_YMIN=None, TARGET_USER_YMAX=None, TARGET_USER_COLS=None, TARGET_USER_ROWS=None, TARGET_USER_FLAT=None, TARGET_USER_FITS=None, TARGET_SYSTEM=None, DATATYPE=None, METHOD=None, EPSILON=None, LEVEL_MAX=None, Verbose=2):
    '''
    Multilevel B-Spline from Grid Points
    ----------
    [grid_spline.5]\n
    Multilevel B-spline algorithm for spatial interpolation of scattered data as proposed by Lee, Wolberg and Shin (1997). The algorithm makes use of a coarse-to-fine hierarchy of control lattices to generate a sequence of bicubic B-spline functions, whose sum approaches the desired interpolation function. Large performance gains are realized by using B-spline refinement to reduce the sum of these functions into one equivalent B-spline function.\n
    The 'Maximum Level' determines the maximum size of the final B-spline matrix and increases exponential with each level. Where level=10 requires about 1mb level=12 needs about 16mb and level=14 about 256mb(!) of additional memory.\n
    Arguments
    ----------
    - GRID [`input grid`] : Grid
    - TARGET_TEMPLATE [`optional input grid`] : Target System. use this grid's system for output grids
    - TARGET_OUT_GRID [`output grid`] : Target Grid
    - GRID_GRIDSYSTEM [`grid system`] : Grid system
    - TARGET_DEFINITION [`choice`] : Target Grid System. Available Choices: [0] user defined [1] grid or grid system Default: 0
    - TARGET_USER_SIZE [`floating point number`] : Cellsize. Minimum: 0.000000 Default: 1.000000
    - TARGET_USER_XMIN [`floating point number`] : West. Default: 0.000000
    - TARGET_USER_XMAX [`floating point number`] : East. Default: 100.000000
    - TARGET_USER_YMIN [`floating point number`] : South. Default: 0.000000
    - TARGET_USER_YMAX [`floating point number`] : North. Default: 100.000000
    - TARGET_USER_COLS [`integer number`] : Columns. Minimum: 1 Default: 101 Number of cells in East-West direction.
    - TARGET_USER_ROWS [`integer number`] : Rows. Minimum: 1 Default: 101 Number of cells in North-South direction.
    - TARGET_USER_FLAT [`boolean`] : Rounding. Default: 1 Round bounding coordinates to multiples of cell size. Ignored if cell size has decimal places.
    - TARGET_USER_FITS [`choice`] : Fit. Available Choices: [0] nodes [1] cells Default: 0
    - TARGET_SYSTEM [`grid system`] : Grid System
    - DATATYPE [`choice`] : Data Type. Available Choices: [0] same as input grid [1] 4 byte floating point number Default: 0
    - METHOD [`choice`] : Refinement. Available Choices: [0] no [1] yes Default: 0
    - EPSILON [`floating point number`] : Threshold Error. Minimum: 0.000000 Default: 0.000100
    - LEVEL_MAX [`integer number`] : Maximum Level. Minimum: 1 Maximum: 14 Default: 11

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_spline', '5', 'Multilevel B-Spline from Grid Points')
    if Tool.is_Okay():
        Tool.Set_Input ('GRID', GRID)
        Tool.Set_Input ('TARGET_TEMPLATE', TARGET_TEMPLATE)
        Tool.Set_Output('TARGET_OUT_GRID', TARGET_OUT_GRID)
        Tool.Set_Option('GRID_GRIDSYSTEM', GRID_GRIDSYSTEM)
        Tool.Set_Option('TARGET_DEFINITION', TARGET_DEFINITION)
        Tool.Set_Option('TARGET_USER_SIZE', TARGET_USER_SIZE)
        Tool.Set_Option('TARGET_USER_XMIN', TARGET_USER_XMIN)
        Tool.Set_Option('TARGET_USER_XMAX', TARGET_USER_XMAX)
        Tool.Set_Option('TARGET_USER_YMIN', TARGET_USER_YMIN)
        Tool.Set_Option('TARGET_USER_YMAX', TARGET_USER_YMAX)
        Tool.Set_Option('TARGET_USER_COLS', TARGET_USER_COLS)
        Tool.Set_Option('TARGET_USER_ROWS', TARGET_USER_ROWS)
        Tool.Set_Option('TARGET_USER_FLAT', TARGET_USER_FLAT)
        Tool.Set_Option('TARGET_USER_FITS', TARGET_USER_FITS)
        Tool.Set_Option('TARGET_SYSTEM', TARGET_SYSTEM)
        Tool.Set_Option('DATATYPE', DATATYPE)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('EPSILON', EPSILON)
        Tool.Set_Option('LEVEL_MAX', LEVEL_MAX)
        return Tool.Execute(Verbose)
    return False

def Cubic_Spline_Approximation(SHAPES=None, TARGET_TEMPLATE=None, TARGET_OUT_GRID=None, FIELD=None, TARGET_DEFINITION=None, TARGET_USER_SIZE=None, TARGET_USER_XMIN=None, TARGET_USER_XMAX=None, TARGET_USER_YMIN=None, TARGET_USER_YMAX=None, TARGET_USER_COLS=None, TARGET_USER_ROWS=None, TARGET_USER_FLAT=None, TARGET_USER_FITS=None, TARGET_SYSTEM=None, NPMIN=None, NPMAX=None, NPPC=None, K=None, Verbose=2):
    '''
    Cubic Spline Approximation
    ----------
    [grid_spline.6]\n
    This tool approximates irregular scalar 2D data in specified points using C1-continuous bivariate cubic spline.\n
    Minimal Number of Points:                minimal number of points locally involved                in spline calculation (normally = 3)\n
    Maximal Number of Points:npmax:          maximal number of points locally involved                in spline calculation (required > 10,                recommended 20 < npmax < 60)\n
    Tolerance:                relative tolerance multiple in fitting                spline coefficients: the higher this                value, the higher degree of the locally                fitted spline (recommended 80 < k < 200)\n
    Points per square:                average number of points per square                (increase if the point distribution is strongly non-uniform                to get larger cells)\n
    Author:         Pavel Sakov,                CSIRO Marine Research\n
    Purpose:        2D data approximation with bivariate C1 cubic spline.                A set of library functions + standalone utility.\n
    Arguments
    ----------
    - SHAPES [`input shapes`] : Points
    - TARGET_TEMPLATE [`optional input grid`] : Target System. use this grid's system for output grids
    - TARGET_OUT_GRID [`output grid`] : Target Grid
    - FIELD [`table field`] : Attribute
    - TARGET_DEFINITION [`choice`] : Target Grid System. Available Choices: [0] user defined [1] grid or grid system Default: 0
    - TARGET_USER_SIZE [`floating point number`] : Cellsize. Minimum: 0.000000 Default: 1.000000
    - TARGET_USER_XMIN [`floating point number`] : West. Default: 0.000000
    - TARGET_USER_XMAX [`floating point number`] : East. Default: 100.000000
    - TARGET_USER_YMIN [`floating point number`] : South. Default: 0.000000
    - TARGET_USER_YMAX [`floating point number`] : North. Default: 100.000000
    - TARGET_USER_COLS [`integer number`] : Columns. Minimum: 1 Default: 101 Number of cells in East-West direction.
    - TARGET_USER_ROWS [`integer number`] : Rows. Minimum: 1 Default: 101 Number of cells in North-South direction.
    - TARGET_USER_FLAT [`boolean`] : Rounding. Default: 1 Round bounding coordinates to multiples of cell size. Ignored if cell size has decimal places.
    - TARGET_USER_FITS [`choice`] : Fit. Available Choices: [0] nodes [1] cells Default: 0
    - TARGET_SYSTEM [`grid system`] : Grid System
    - NPMIN [`integer number`] : Minimal Number of Points. Minimum: 0 Default: 3
    - NPMAX [`integer number`] : Maximal Number of Points. Minimum: 11 Maximum: 59 Default: 20
    - NPPC [`floating point number`] : Points per Square. Minimum: 1.000000 Default: 5.000000
    - K [`integer number`] : Tolerance. Minimum: 0 Default: 140 Spline sensitivity, reduce to get smoother results, recommended: 80 < Tolerance < 200

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_spline', '6', 'Cubic Spline Approximation')
    if Tool.is_Okay():
        Tool.Set_Input ('SHAPES', SHAPES)
        Tool.Set_Input ('TARGET_TEMPLATE', TARGET_TEMPLATE)
        Tool.Set_Output('TARGET_OUT_GRID', TARGET_OUT_GRID)
        Tool.Set_Option('FIELD', FIELD)
        Tool.Set_Option('TARGET_DEFINITION', TARGET_DEFINITION)
        Tool.Set_Option('TARGET_USER_SIZE', TARGET_USER_SIZE)
        Tool.Set_Option('TARGET_USER_XMIN', TARGET_USER_XMIN)
        Tool.Set_Option('TARGET_USER_XMAX', TARGET_USER_XMAX)
        Tool.Set_Option('TARGET_USER_YMIN', TARGET_USER_YMIN)
        Tool.Set_Option('TARGET_USER_YMAX', TARGET_USER_YMAX)
        Tool.Set_Option('TARGET_USER_COLS', TARGET_USER_COLS)
        Tool.Set_Option('TARGET_USER_ROWS', TARGET_USER_ROWS)
        Tool.Set_Option('TARGET_USER_FLAT', TARGET_USER_FLAT)
        Tool.Set_Option('TARGET_USER_FITS', TARGET_USER_FITS)
        Tool.Set_Option('TARGET_SYSTEM', TARGET_SYSTEM)
        Tool.Set_Option('NPMIN', NPMIN)
        Tool.Set_Option('NPMAX', NPMAX)
        Tool.Set_Option('NPPC', NPPC)
        Tool.Set_Option('K', K)
        return Tool.Execute(Verbose)
    return False

def run_tool_grid_spline_6(SHAPES=None, TARGET_TEMPLATE=None, TARGET_OUT_GRID=None, FIELD=None, TARGET_DEFINITION=None, TARGET_USER_SIZE=None, TARGET_USER_XMIN=None, TARGET_USER_XMAX=None, TARGET_USER_YMIN=None, TARGET_USER_YMAX=None, TARGET_USER_COLS=None, TARGET_USER_ROWS=None, TARGET_USER_FLAT=None, TARGET_USER_FITS=None, TARGET_SYSTEM=None, NPMIN=None, NPMAX=None, NPPC=None, K=None, Verbose=2):
    '''
    Cubic Spline Approximation
    ----------
    [grid_spline.6]\n
    This tool approximates irregular scalar 2D data in specified points using C1-continuous bivariate cubic spline.\n
    Minimal Number of Points:                minimal number of points locally involved                in spline calculation (normally = 3)\n
    Maximal Number of Points:npmax:          maximal number of points locally involved                in spline calculation (required > 10,                recommended 20 < npmax < 60)\n
    Tolerance:                relative tolerance multiple in fitting                spline coefficients: the higher this                value, the higher degree of the locally                fitted spline (recommended 80 < k < 200)\n
    Points per square:                average number of points per square                (increase if the point distribution is strongly non-uniform                to get larger cells)\n
    Author:         Pavel Sakov,                CSIRO Marine Research\n
    Purpose:        2D data approximation with bivariate C1 cubic spline.                A set of library functions + standalone utility.\n
    Arguments
    ----------
    - SHAPES [`input shapes`] : Points
    - TARGET_TEMPLATE [`optional input grid`] : Target System. use this grid's system for output grids
    - TARGET_OUT_GRID [`output grid`] : Target Grid
    - FIELD [`table field`] : Attribute
    - TARGET_DEFINITION [`choice`] : Target Grid System. Available Choices: [0] user defined [1] grid or grid system Default: 0
    - TARGET_USER_SIZE [`floating point number`] : Cellsize. Minimum: 0.000000 Default: 1.000000
    - TARGET_USER_XMIN [`floating point number`] : West. Default: 0.000000
    - TARGET_USER_XMAX [`floating point number`] : East. Default: 100.000000
    - TARGET_USER_YMIN [`floating point number`] : South. Default: 0.000000
    - TARGET_USER_YMAX [`floating point number`] : North. Default: 100.000000
    - TARGET_USER_COLS [`integer number`] : Columns. Minimum: 1 Default: 101 Number of cells in East-West direction.
    - TARGET_USER_ROWS [`integer number`] : Rows. Minimum: 1 Default: 101 Number of cells in North-South direction.
    - TARGET_USER_FLAT [`boolean`] : Rounding. Default: 1 Round bounding coordinates to multiples of cell size. Ignored if cell size has decimal places.
    - TARGET_USER_FITS [`choice`] : Fit. Available Choices: [0] nodes [1] cells Default: 0
    - TARGET_SYSTEM [`grid system`] : Grid System
    - NPMIN [`integer number`] : Minimal Number of Points. Minimum: 0 Default: 3
    - NPMAX [`integer number`] : Maximal Number of Points. Minimum: 11 Maximum: 59 Default: 20
    - NPPC [`floating point number`] : Points per Square. Minimum: 1.000000 Default: 5.000000
    - K [`integer number`] : Tolerance. Minimum: 0 Default: 140 Spline sensitivity, reduce to get smoother results, recommended: 80 < Tolerance < 200

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_spline', '6', 'Cubic Spline Approximation')
    if Tool.is_Okay():
        Tool.Set_Input ('SHAPES', SHAPES)
        Tool.Set_Input ('TARGET_TEMPLATE', TARGET_TEMPLATE)
        Tool.Set_Output('TARGET_OUT_GRID', TARGET_OUT_GRID)
        Tool.Set_Option('FIELD', FIELD)
        Tool.Set_Option('TARGET_DEFINITION', TARGET_DEFINITION)
        Tool.Set_Option('TARGET_USER_SIZE', TARGET_USER_SIZE)
        Tool.Set_Option('TARGET_USER_XMIN', TARGET_USER_XMIN)
        Tool.Set_Option('TARGET_USER_XMAX', TARGET_USER_XMAX)
        Tool.Set_Option('TARGET_USER_YMIN', TARGET_USER_YMIN)
        Tool.Set_Option('TARGET_USER_YMAX', TARGET_USER_YMAX)
        Tool.Set_Option('TARGET_USER_COLS', TARGET_USER_COLS)
        Tool.Set_Option('TARGET_USER_ROWS', TARGET_USER_ROWS)
        Tool.Set_Option('TARGET_USER_FLAT', TARGET_USER_FLAT)
        Tool.Set_Option('TARGET_USER_FITS', TARGET_USER_FITS)
        Tool.Set_Option('TARGET_SYSTEM', TARGET_SYSTEM)
        Tool.Set_Option('NPMIN', NPMIN)
        Tool.Set_Option('NPMAX', NPMAX)
        Tool.Set_Option('NPPC', NPPC)
        Tool.Set_Option('K', K)
        return Tool.Execute(Verbose)
    return False

def Multilevel_BSpline_for_Categories(POINTS=None, TARGET_TEMPLATE=None, CATEGORIES=None, PROBABILITY=None, FIELD=None, TARGET_DEFINITION=None, TARGET_USER_SIZE=None, TARGET_USER_XMIN=None, TARGET_USER_XMAX=None, TARGET_USER_YMIN=None, TARGET_USER_YMAX=None, TARGET_USER_COLS=None, TARGET_USER_ROWS=None, TARGET_USER_FLAT=None, TARGET_USER_FITS=None, TARGET_SYSTEM=None, Verbose=2):
    '''
    Multilevel B-Spline for Categories
    ----------
    [grid_spline.7]\n
    The 'Multilevel B-Spline for Categories' tool is comparable to indicator Kriging except that uses the Multilevel B-spline algorithm for interpolation.\n
    Arguments
    ----------
    - POINTS [`input shapes`] : Points
    - TARGET_TEMPLATE [`optional input grid`] : Target System. use this grid's system for output grids
    - CATEGORIES [`output grid`] : Categories
    - PROBABILITY [`output grid`] : Probability
    - FIELD [`table field`] : Attribute
    - TARGET_DEFINITION [`choice`] : Target Grid System. Available Choices: [0] user defined [1] grid or grid system Default: 0
    - TARGET_USER_SIZE [`floating point number`] : Cellsize. Minimum: 0.000000 Default: 1.000000
    - TARGET_USER_XMIN [`floating point number`] : West. Default: 0.000000
    - TARGET_USER_XMAX [`floating point number`] : East. Default: 100.000000
    - TARGET_USER_YMIN [`floating point number`] : South. Default: 0.000000
    - TARGET_USER_YMAX [`floating point number`] : North. Default: 100.000000
    - TARGET_USER_COLS [`integer number`] : Columns. Minimum: 1 Default: 101 Number of cells in East-West direction.
    - TARGET_USER_ROWS [`integer number`] : Rows. Minimum: 1 Default: 101 Number of cells in North-South direction.
    - TARGET_USER_FLAT [`boolean`] : Rounding. Default: 1 Round bounding coordinates to multiples of cell size. Ignored if cell size has decimal places.
    - TARGET_USER_FITS [`choice`] : Fit. Available Choices: [0] nodes [1] cells Default: 0
    - TARGET_SYSTEM [`grid system`] : Grid System

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_spline', '7', 'Multilevel B-Spline for Categories')
    if Tool.is_Okay():
        Tool.Set_Input ('POINTS', POINTS)
        Tool.Set_Input ('TARGET_TEMPLATE', TARGET_TEMPLATE)
        Tool.Set_Output('CATEGORIES', CATEGORIES)
        Tool.Set_Output('PROBABILITY', PROBABILITY)
        Tool.Set_Option('FIELD', FIELD)
        Tool.Set_Option('TARGET_DEFINITION', TARGET_DEFINITION)
        Tool.Set_Option('TARGET_USER_SIZE', TARGET_USER_SIZE)
        Tool.Set_Option('TARGET_USER_XMIN', TARGET_USER_XMIN)
        Tool.Set_Option('TARGET_USER_XMAX', TARGET_USER_XMAX)
        Tool.Set_Option('TARGET_USER_YMIN', TARGET_USER_YMIN)
        Tool.Set_Option('TARGET_USER_YMAX', TARGET_USER_YMAX)
        Tool.Set_Option('TARGET_USER_COLS', TARGET_USER_COLS)
        Tool.Set_Option('TARGET_USER_ROWS', TARGET_USER_ROWS)
        Tool.Set_Option('TARGET_USER_FLAT', TARGET_USER_FLAT)
        Tool.Set_Option('TARGET_USER_FITS', TARGET_USER_FITS)
        Tool.Set_Option('TARGET_SYSTEM', TARGET_SYSTEM)
        return Tool.Execute(Verbose)
    return False

def run_tool_grid_spline_7(POINTS=None, TARGET_TEMPLATE=None, CATEGORIES=None, PROBABILITY=None, FIELD=None, TARGET_DEFINITION=None, TARGET_USER_SIZE=None, TARGET_USER_XMIN=None, TARGET_USER_XMAX=None, TARGET_USER_YMIN=None, TARGET_USER_YMAX=None, TARGET_USER_COLS=None, TARGET_USER_ROWS=None, TARGET_USER_FLAT=None, TARGET_USER_FITS=None, TARGET_SYSTEM=None, Verbose=2):
    '''
    Multilevel B-Spline for Categories
    ----------
    [grid_spline.7]\n
    The 'Multilevel B-Spline for Categories' tool is comparable to indicator Kriging except that uses the Multilevel B-spline algorithm for interpolation.\n
    Arguments
    ----------
    - POINTS [`input shapes`] : Points
    - TARGET_TEMPLATE [`optional input grid`] : Target System. use this grid's system for output grids
    - CATEGORIES [`output grid`] : Categories
    - PROBABILITY [`output grid`] : Probability
    - FIELD [`table field`] : Attribute
    - TARGET_DEFINITION [`choice`] : Target Grid System. Available Choices: [0] user defined [1] grid or grid system Default: 0
    - TARGET_USER_SIZE [`floating point number`] : Cellsize. Minimum: 0.000000 Default: 1.000000
    - TARGET_USER_XMIN [`floating point number`] : West. Default: 0.000000
    - TARGET_USER_XMAX [`floating point number`] : East. Default: 100.000000
    - TARGET_USER_YMIN [`floating point number`] : South. Default: 0.000000
    - TARGET_USER_YMAX [`floating point number`] : North. Default: 100.000000
    - TARGET_USER_COLS [`integer number`] : Columns. Minimum: 1 Default: 101 Number of cells in East-West direction.
    - TARGET_USER_ROWS [`integer number`] : Rows. Minimum: 1 Default: 101 Number of cells in North-South direction.
    - TARGET_USER_FLAT [`boolean`] : Rounding. Default: 1 Round bounding coordinates to multiples of cell size. Ignored if cell size has decimal places.
    - TARGET_USER_FITS [`choice`] : Fit. Available Choices: [0] nodes [1] cells Default: 0
    - TARGET_SYSTEM [`grid system`] : Grid System

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_spline', '7', 'Multilevel B-Spline for Categories')
    if Tool.is_Okay():
        Tool.Set_Input ('POINTS', POINTS)
        Tool.Set_Input ('TARGET_TEMPLATE', TARGET_TEMPLATE)
        Tool.Set_Output('CATEGORIES', CATEGORIES)
        Tool.Set_Output('PROBABILITY', PROBABILITY)
        Tool.Set_Option('FIELD', FIELD)
        Tool.Set_Option('TARGET_DEFINITION', TARGET_DEFINITION)
        Tool.Set_Option('TARGET_USER_SIZE', TARGET_USER_SIZE)
        Tool.Set_Option('TARGET_USER_XMIN', TARGET_USER_XMIN)
        Tool.Set_Option('TARGET_USER_XMAX', TARGET_USER_XMAX)
        Tool.Set_Option('TARGET_USER_YMIN', TARGET_USER_YMIN)
        Tool.Set_Option('TARGET_USER_YMAX', TARGET_USER_YMAX)
        Tool.Set_Option('TARGET_USER_COLS', TARGET_USER_COLS)
        Tool.Set_Option('TARGET_USER_ROWS', TARGET_USER_ROWS)
        Tool.Set_Option('TARGET_USER_FLAT', TARGET_USER_FLAT)
        Tool.Set_Option('TARGET_USER_FITS', TARGET_USER_FITS)
        Tool.Set_Option('TARGET_SYSTEM', TARGET_SYSTEM)
        return Tool.Execute(Verbose)
    return False

def Multilevel_BSpline_3D(POINTS=None, TARGET_TEMPLATE=None, GRIDS=None, Z_FIELD=None, Z_SCALE=None, V_FIELD=None, TARGET_DEFINITION=None, TARGET_USER_SIZE=None, TARGET_USER_XMIN=None, TARGET_USER_XMAX=None, TARGET_USER_YMIN=None, TARGET_USER_YMAX=None, TARGET_USER_COLS=None, TARGET_USER_ROWS=None, TARGET_USER_FLAT=None, TARGET_USER_FITS=None, TARGET_SYSTEM=None, TARGET_USER_ZSIZE=None, TARGET_USER_ZMIN=None, TARGET_USER_ZMAX=None, TARGET_USER_ZNUM=None, EPSILON=None, LEVEL_MAX=None, Verbose=2):
    '''
    Multilevel B-Spline (3D)
    ----------
    [grid_spline.8]\n
    Multilevel B-spline algorithm for spatial interpolation of scattered data as proposed by Lee, Wolberg and Shin (1997) modified for 3D data.\n
    The algorithm makes use of a coarse-to-fine hierarchy of control lattices to generate a sequence of bicubic B-spline functions, whose sum approaches the desired interpolation function. Performance gains are realized by using B-spline refinement to reduce the sum of these functions into one equivalent B-spline function.\n
    The 'Maximum Level' determines the maximum size of the final B-spline matrix and increases exponential with each level. Where level=10 requires about 1mb level=12 needs about 16mb and level=14 about 256mb(!) of additional memory.\n
    Arguments
    ----------
    - POINTS [`input shapes`] : Points
    - TARGET_TEMPLATE [`optional input grid`] : Target System. use this grid's system for output grids
    - GRIDS [`output grid collection`] : Grid Collection
    - Z_FIELD [`table field`] : Z
    - Z_SCALE [`floating point number`] : Z Factor. Minimum: 0.000000 Default: 1.000000
    - V_FIELD [`table field`] : Value
    - TARGET_DEFINITION [`choice`] : Target Grid System. Available Choices: [0] user defined [1] grid or grid system Default: 0
    - TARGET_USER_SIZE [`floating point number`] : Cellsize. Minimum: 0.000000 Default: 1.000000
    - TARGET_USER_XMIN [`floating point number`] : West. Default: 0.000000
    - TARGET_USER_XMAX [`floating point number`] : East. Default: 100.000000
    - TARGET_USER_YMIN [`floating point number`] : South. Default: 0.000000
    - TARGET_USER_YMAX [`floating point number`] : North. Default: 100.000000
    - TARGET_USER_COLS [`integer number`] : Columns. Minimum: 1 Default: 101 Number of cells in East-West direction.
    - TARGET_USER_ROWS [`integer number`] : Rows. Minimum: 1 Default: 101 Number of cells in North-South direction.
    - TARGET_USER_FLAT [`boolean`] : Rounding. Default: 1 Round bounding coordinates to multiples of cell size. Ignored if cell size has decimal places.
    - TARGET_USER_FITS [`choice`] : Fit. Available Choices: [0] nodes [1] cells Default: 0
    - TARGET_SYSTEM [`grid system`] : Grid System
    - TARGET_USER_ZSIZE [`floating point number`] : Cellsize. Minimum: 0.000000 Default: 1.000000
    - TARGET_USER_ZMIN [`floating point number`] : Bottom. Default: 0.000000
    - TARGET_USER_ZMAX [`floating point number`] : Top. Default: 100.000000
    - TARGET_USER_ZNUM [`integer number`] : Levels. Minimum: 1 Default: 101
    - EPSILON [`floating point number`] : Threshold Error. Minimum: 0.000000 Default: 0.000100
    - LEVEL_MAX [`integer number`] : Maximum Level. Minimum: 1 Maximum: 14 Default: 11

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_spline', '8', 'Multilevel B-Spline (3D)')
    if Tool.is_Okay():
        Tool.Set_Input ('POINTS', POINTS)
        Tool.Set_Input ('TARGET_TEMPLATE', TARGET_TEMPLATE)
        Tool.Set_Output('GRIDS', GRIDS)
        Tool.Set_Option('Z_FIELD', Z_FIELD)
        Tool.Set_Option('Z_SCALE', Z_SCALE)
        Tool.Set_Option('V_FIELD', V_FIELD)
        Tool.Set_Option('TARGET_DEFINITION', TARGET_DEFINITION)
        Tool.Set_Option('TARGET_USER_SIZE', TARGET_USER_SIZE)
        Tool.Set_Option('TARGET_USER_XMIN', TARGET_USER_XMIN)
        Tool.Set_Option('TARGET_USER_XMAX', TARGET_USER_XMAX)
        Tool.Set_Option('TARGET_USER_YMIN', TARGET_USER_YMIN)
        Tool.Set_Option('TARGET_USER_YMAX', TARGET_USER_YMAX)
        Tool.Set_Option('TARGET_USER_COLS', TARGET_USER_COLS)
        Tool.Set_Option('TARGET_USER_ROWS', TARGET_USER_ROWS)
        Tool.Set_Option('TARGET_USER_FLAT', TARGET_USER_FLAT)
        Tool.Set_Option('TARGET_USER_FITS', TARGET_USER_FITS)
        Tool.Set_Option('TARGET_SYSTEM', TARGET_SYSTEM)
        Tool.Set_Option('TARGET_USER_ZSIZE', TARGET_USER_ZSIZE)
        Tool.Set_Option('TARGET_USER_ZMIN', TARGET_USER_ZMIN)
        Tool.Set_Option('TARGET_USER_ZMAX', TARGET_USER_ZMAX)
        Tool.Set_Option('TARGET_USER_ZNUM', TARGET_USER_ZNUM)
        Tool.Set_Option('EPSILON', EPSILON)
        Tool.Set_Option('LEVEL_MAX', LEVEL_MAX)
        return Tool.Execute(Verbose)
    return False

def run_tool_grid_spline_8(POINTS=None, TARGET_TEMPLATE=None, GRIDS=None, Z_FIELD=None, Z_SCALE=None, V_FIELD=None, TARGET_DEFINITION=None, TARGET_USER_SIZE=None, TARGET_USER_XMIN=None, TARGET_USER_XMAX=None, TARGET_USER_YMIN=None, TARGET_USER_YMAX=None, TARGET_USER_COLS=None, TARGET_USER_ROWS=None, TARGET_USER_FLAT=None, TARGET_USER_FITS=None, TARGET_SYSTEM=None, TARGET_USER_ZSIZE=None, TARGET_USER_ZMIN=None, TARGET_USER_ZMAX=None, TARGET_USER_ZNUM=None, EPSILON=None, LEVEL_MAX=None, Verbose=2):
    '''
    Multilevel B-Spline (3D)
    ----------
    [grid_spline.8]\n
    Multilevel B-spline algorithm for spatial interpolation of scattered data as proposed by Lee, Wolberg and Shin (1997) modified for 3D data.\n
    The algorithm makes use of a coarse-to-fine hierarchy of control lattices to generate a sequence of bicubic B-spline functions, whose sum approaches the desired interpolation function. Performance gains are realized by using B-spline refinement to reduce the sum of these functions into one equivalent B-spline function.\n
    The 'Maximum Level' determines the maximum size of the final B-spline matrix and increases exponential with each level. Where level=10 requires about 1mb level=12 needs about 16mb and level=14 about 256mb(!) of additional memory.\n
    Arguments
    ----------
    - POINTS [`input shapes`] : Points
    - TARGET_TEMPLATE [`optional input grid`] : Target System. use this grid's system for output grids
    - GRIDS [`output grid collection`] : Grid Collection
    - Z_FIELD [`table field`] : Z
    - Z_SCALE [`floating point number`] : Z Factor. Minimum: 0.000000 Default: 1.000000
    - V_FIELD [`table field`] : Value
    - TARGET_DEFINITION [`choice`] : Target Grid System. Available Choices: [0] user defined [1] grid or grid system Default: 0
    - TARGET_USER_SIZE [`floating point number`] : Cellsize. Minimum: 0.000000 Default: 1.000000
    - TARGET_USER_XMIN [`floating point number`] : West. Default: 0.000000
    - TARGET_USER_XMAX [`floating point number`] : East. Default: 100.000000
    - TARGET_USER_YMIN [`floating point number`] : South. Default: 0.000000
    - TARGET_USER_YMAX [`floating point number`] : North. Default: 100.000000
    - TARGET_USER_COLS [`integer number`] : Columns. Minimum: 1 Default: 101 Number of cells in East-West direction.
    - TARGET_USER_ROWS [`integer number`] : Rows. Minimum: 1 Default: 101 Number of cells in North-South direction.
    - TARGET_USER_FLAT [`boolean`] : Rounding. Default: 1 Round bounding coordinates to multiples of cell size. Ignored if cell size has decimal places.
    - TARGET_USER_FITS [`choice`] : Fit. Available Choices: [0] nodes [1] cells Default: 0
    - TARGET_SYSTEM [`grid system`] : Grid System
    - TARGET_USER_ZSIZE [`floating point number`] : Cellsize. Minimum: 0.000000 Default: 1.000000
    - TARGET_USER_ZMIN [`floating point number`] : Bottom. Default: 0.000000
    - TARGET_USER_ZMAX [`floating point number`] : Top. Default: 100.000000
    - TARGET_USER_ZNUM [`integer number`] : Levels. Minimum: 1 Default: 101
    - EPSILON [`floating point number`] : Threshold Error. Minimum: 0.000000 Default: 0.000100
    - LEVEL_MAX [`integer number`] : Maximum Level. Minimum: 1 Maximum: 14 Default: 11

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_spline', '8', 'Multilevel B-Spline (3D)')
    if Tool.is_Okay():
        Tool.Set_Input ('POINTS', POINTS)
        Tool.Set_Input ('TARGET_TEMPLATE', TARGET_TEMPLATE)
        Tool.Set_Output('GRIDS', GRIDS)
        Tool.Set_Option('Z_FIELD', Z_FIELD)
        Tool.Set_Option('Z_SCALE', Z_SCALE)
        Tool.Set_Option('V_FIELD', V_FIELD)
        Tool.Set_Option('TARGET_DEFINITION', TARGET_DEFINITION)
        Tool.Set_Option('TARGET_USER_SIZE', TARGET_USER_SIZE)
        Tool.Set_Option('TARGET_USER_XMIN', TARGET_USER_XMIN)
        Tool.Set_Option('TARGET_USER_XMAX', TARGET_USER_XMAX)
        Tool.Set_Option('TARGET_USER_YMIN', TARGET_USER_YMIN)
        Tool.Set_Option('TARGET_USER_YMAX', TARGET_USER_YMAX)
        Tool.Set_Option('TARGET_USER_COLS', TARGET_USER_COLS)
        Tool.Set_Option('TARGET_USER_ROWS', TARGET_USER_ROWS)
        Tool.Set_Option('TARGET_USER_FLAT', TARGET_USER_FLAT)
        Tool.Set_Option('TARGET_USER_FITS', TARGET_USER_FITS)
        Tool.Set_Option('TARGET_SYSTEM', TARGET_SYSTEM)
        Tool.Set_Option('TARGET_USER_ZSIZE', TARGET_USER_ZSIZE)
        Tool.Set_Option('TARGET_USER_ZMIN', TARGET_USER_ZMIN)
        Tool.Set_Option('TARGET_USER_ZMAX', TARGET_USER_ZMAX)
        Tool.Set_Option('TARGET_USER_ZNUM', TARGET_USER_ZNUM)
        Tool.Set_Option('EPSILON', EPSILON)
        Tool.Set_Option('LEVEL_MAX', LEVEL_MAX)
        return Tool.Execute(Verbose)
    return False

