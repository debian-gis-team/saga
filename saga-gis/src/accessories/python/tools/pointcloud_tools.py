#! /usr/bin/env python

'''
Python Interface to SAGA Tools Library
----------
- Category : Shapes
- Name     : Point Clouds
- ID       : pointcloud_tools

Description
----------
Tools for point clouds.
'''

from PySAGA.helper import Tool_Wrapper

def Point_Cloud_Cutter(POINTS=None, EXTENT=None, POLYGONS=None, CUT=None, AREA=None, INVERSE=None, XMIN=None, XMAX=None, YMIN=None, YMAX=None, GRID=None, Verbose=2):
    '''
    Point Cloud Cutter
    ----------
    [pointcloud_tools.0]\n
    This tool allows one to extract subsets from one or several point cloud datasets. The area-of-interest is defined either by bounding box coordinates, the extent of a grid system or a shapes layer, or by polygons of a shapes layer.\n
    In case a polygon shapes layer is used and one or more polygons are selected, only the selected polygons are processed.\n
    Arguments
    ----------
    - POINTS [`input point cloud list`] : Points. One or several input point cloud datasets to cut.
    - EXTENT [`input shapes`] : Shapes Extent
    - POLYGONS [`input shapes`] : Polygons
    - CUT [`output point cloud list`] : Cut. The cut output point cloud dataset(s).
    - AREA [`choice`] : Choose Cut from .... Available Choices: [0] User Defined Extent [1] Grid System Extent [2] Shapes Extent [3] Polygons Default: 0
    - INVERSE [`boolean`] : Inverse. Default: 0 Invert selection.
    - XMIN [`floating point number`] : Left. Default: 0.000000
    - XMAX [`floating point number`] : Right. Default: 0.000000
    - YMIN [`floating point number`] : Bottom. Default: 0.000000
    - YMAX [`floating point number`] : Top. Default: 0.000000
    - GRID [`grid system`] : Grid System

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('pointcloud_tools', '0', 'Point Cloud Cutter')
    if Tool.is_Okay():
        Tool.Set_Input ('POINTS', POINTS)
        Tool.Set_Input ('EXTENT', EXTENT)
        Tool.Set_Input ('POLYGONS', POLYGONS)
        Tool.Set_Output('CUT', CUT)
        Tool.Set_Option('AREA', AREA)
        Tool.Set_Option('INVERSE', INVERSE)
        Tool.Set_Option('XMIN', XMIN)
        Tool.Set_Option('XMAX', XMAX)
        Tool.Set_Option('YMIN', YMIN)
        Tool.Set_Option('YMAX', YMAX)
        Tool.Set_Option('GRID', GRID)
        return Tool.Execute(Verbose)
    return False

def run_tool_pointcloud_tools_0(POINTS=None, EXTENT=None, POLYGONS=None, CUT=None, AREA=None, INVERSE=None, XMIN=None, XMAX=None, YMIN=None, YMAX=None, GRID=None, Verbose=2):
    '''
    Point Cloud Cutter
    ----------
    [pointcloud_tools.0]\n
    This tool allows one to extract subsets from one or several point cloud datasets. The area-of-interest is defined either by bounding box coordinates, the extent of a grid system or a shapes layer, or by polygons of a shapes layer.\n
    In case a polygon shapes layer is used and one or more polygons are selected, only the selected polygons are processed.\n
    Arguments
    ----------
    - POINTS [`input point cloud list`] : Points. One or several input point cloud datasets to cut.
    - EXTENT [`input shapes`] : Shapes Extent
    - POLYGONS [`input shapes`] : Polygons
    - CUT [`output point cloud list`] : Cut. The cut output point cloud dataset(s).
    - AREA [`choice`] : Choose Cut from .... Available Choices: [0] User Defined Extent [1] Grid System Extent [2] Shapes Extent [3] Polygons Default: 0
    - INVERSE [`boolean`] : Inverse. Default: 0 Invert selection.
    - XMIN [`floating point number`] : Left. Default: 0.000000
    - XMAX [`floating point number`] : Right. Default: 0.000000
    - YMIN [`floating point number`] : Bottom. Default: 0.000000
    - YMAX [`floating point number`] : Top. Default: 0.000000
    - GRID [`grid system`] : Grid System

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('pointcloud_tools', '0', 'Point Cloud Cutter')
    if Tool.is_Okay():
        Tool.Set_Input ('POINTS', POINTS)
        Tool.Set_Input ('EXTENT', EXTENT)
        Tool.Set_Input ('POLYGONS', POLYGONS)
        Tool.Set_Output('CUT', CUT)
        Tool.Set_Option('AREA', AREA)
        Tool.Set_Option('INVERSE', INVERSE)
        Tool.Set_Option('XMIN', XMIN)
        Tool.Set_Option('XMAX', XMAX)
        Tool.Set_Option('YMIN', YMIN)
        Tool.Set_Option('YMAX', YMAX)
        Tool.Set_Option('GRID', GRID)
        return Tool.Execute(Verbose)
    return False

def Point_Cloud_from_Grid_Points(GRID=None, GRIDS=None, POINTS=None, Verbose=2):
    '''
    Point Cloud from Grid Points
    ----------
    [pointcloud_tools.2]\n
    Point Cloud from Grid Points\n
    Arguments
    ----------
    - GRID [`input grid`] : Z Value
    - GRIDS [`optional input grid list`] : Additional Values
    - POINTS [`output point cloud`] : Points

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('pointcloud_tools', '2', 'Point Cloud from Grid Points')
    if Tool.is_Okay():
        Tool.Set_Input ('GRID', GRID)
        Tool.Set_Input ('GRIDS', GRIDS)
        Tool.Set_Output('POINTS', POINTS)
        return Tool.Execute(Verbose)
    return False

def run_tool_pointcloud_tools_2(GRID=None, GRIDS=None, POINTS=None, Verbose=2):
    '''
    Point Cloud from Grid Points
    ----------
    [pointcloud_tools.2]\n
    Point Cloud from Grid Points\n
    Arguments
    ----------
    - GRID [`input grid`] : Z Value
    - GRIDS [`optional input grid list`] : Additional Values
    - POINTS [`output point cloud`] : Points

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('pointcloud_tools', '2', 'Point Cloud from Grid Points')
    if Tool.is_Okay():
        Tool.Set_Input ('GRID', GRID)
        Tool.Set_Input ('GRIDS', GRIDS)
        Tool.Set_Output('POINTS', POINTS)
        return Tool.Execute(Verbose)
    return False

def Point_Cloud_from_Shapes(SHAPES=None, POINTS=None, ZFIELD=None, OUTPUT=None, Verbose=2):
    '''
    Point Cloud from Shapes
    ----------
    [pointcloud_tools.3]\n
    Point Cloud from Shapes\n
    Arguments
    ----------
    - SHAPES [`input shapes`] : Shapes
    - POINTS [`output point cloud`] : Points
    - ZFIELD [`table field`] : Z Value
    - OUTPUT [`choice`] : Output. Available Choices: [0] only z [1] all attributes Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('pointcloud_tools', '3', 'Point Cloud from Shapes')
    if Tool.is_Okay():
        Tool.Set_Input ('SHAPES', SHAPES)
        Tool.Set_Output('POINTS', POINTS)
        Tool.Set_Option('ZFIELD', ZFIELD)
        Tool.Set_Option('OUTPUT', OUTPUT)
        return Tool.Execute(Verbose)
    return False

def run_tool_pointcloud_tools_3(SHAPES=None, POINTS=None, ZFIELD=None, OUTPUT=None, Verbose=2):
    '''
    Point Cloud from Shapes
    ----------
    [pointcloud_tools.3]\n
    Point Cloud from Shapes\n
    Arguments
    ----------
    - SHAPES [`input shapes`] : Shapes
    - POINTS [`output point cloud`] : Points
    - ZFIELD [`table field`] : Z Value
    - OUTPUT [`choice`] : Output. Available Choices: [0] only z [1] all attributes Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('pointcloud_tools', '3', 'Point Cloud from Shapes')
    if Tool.is_Okay():
        Tool.Set_Input ('SHAPES', SHAPES)
        Tool.Set_Output('POINTS', POINTS)
        Tool.Set_Option('ZFIELD', ZFIELD)
        Tool.Set_Option('OUTPUT', OUTPUT)
        return Tool.Execute(Verbose)
    return False

def Point_Cloud_to_Grid(POINTS=None, GRID=None, COUNT=None, GRIDS=None, OUTPUT=None, AGGREGATION=None, CELLSIZE=None, Verbose=2):
    '''
    Point Cloud to Grid
    ----------
    [pointcloud_tools.4]\n
    Point Cloud to Grid\n
    Arguments
    ----------
    - POINTS [`input point cloud`] : Points
    - GRID [`output data object`] : Z Value
    - COUNT [`output data object`] : Number of Points in Cell
    - GRIDS [`output grid list`] : Attributes
    - OUTPUT [`choice`] : Output. Available Choices: [0] only z [1] all attributes Default: 0
    - AGGREGATION [`choice`] : Aggregation. Available Choices: [0] first value [1] last value [2] mean value [3] lowest z [4] highest z Default: 0
    - CELLSIZE [`floating point number`] : Cellsize. Minimum: 0.000000 Default: 1.000000

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('pointcloud_tools', '4', 'Point Cloud to Grid')
    if Tool.is_Okay():
        Tool.Set_Input ('POINTS', POINTS)
        Tool.Set_Output('GRID', GRID)
        Tool.Set_Output('COUNT', COUNT)
        Tool.Set_Output('GRIDS', GRIDS)
        Tool.Set_Option('OUTPUT', OUTPUT)
        Tool.Set_Option('AGGREGATION', AGGREGATION)
        Tool.Set_Option('CELLSIZE', CELLSIZE)
        return Tool.Execute(Verbose)
    return False

def run_tool_pointcloud_tools_4(POINTS=None, GRID=None, COUNT=None, GRIDS=None, OUTPUT=None, AGGREGATION=None, CELLSIZE=None, Verbose=2):
    '''
    Point Cloud to Grid
    ----------
    [pointcloud_tools.4]\n
    Point Cloud to Grid\n
    Arguments
    ----------
    - POINTS [`input point cloud`] : Points
    - GRID [`output data object`] : Z Value
    - COUNT [`output data object`] : Number of Points in Cell
    - GRIDS [`output grid list`] : Attributes
    - OUTPUT [`choice`] : Output. Available Choices: [0] only z [1] all attributes Default: 0
    - AGGREGATION [`choice`] : Aggregation. Available Choices: [0] first value [1] last value [2] mean value [3] lowest z [4] highest z Default: 0
    - CELLSIZE [`floating point number`] : Cellsize. Minimum: 0.000000 Default: 1.000000

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('pointcloud_tools', '4', 'Point Cloud to Grid')
    if Tool.is_Okay():
        Tool.Set_Input ('POINTS', POINTS)
        Tool.Set_Output('GRID', GRID)
        Tool.Set_Output('COUNT', COUNT)
        Tool.Set_Output('GRIDS', GRIDS)
        Tool.Set_Option('OUTPUT', OUTPUT)
        Tool.Set_Option('AGGREGATION', AGGREGATION)
        Tool.Set_Option('CELLSIZE', CELLSIZE)
        return Tool.Execute(Verbose)
    return False

def Point_Cloud_to_Shapes(POINTS=None, SHAPES=None, Verbose=2):
    '''
    Point Cloud to Shapes
    ----------
    [pointcloud_tools.5]\n
    Point Cloud to Shapes\n
    Arguments
    ----------
    - POINTS [`input point cloud`] : Points
    - SHAPES [`output shapes`] : Shapes

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('pointcloud_tools', '5', 'Point Cloud to Shapes')
    if Tool.is_Okay():
        Tool.Set_Input ('POINTS', POINTS)
        Tool.Set_Output('SHAPES', SHAPES)
        return Tool.Execute(Verbose)
    return False

def run_tool_pointcloud_tools_5(POINTS=None, SHAPES=None, Verbose=2):
    '''
    Point Cloud to Shapes
    ----------
    [pointcloud_tools.5]\n
    Point Cloud to Shapes\n
    Arguments
    ----------
    - POINTS [`input point cloud`] : Points
    - SHAPES [`output shapes`] : Shapes

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('pointcloud_tools', '5', 'Point Cloud to Shapes')
    if Tool.is_Okay():
        Tool.Set_Input ('POINTS', POINTS)
        Tool.Set_Output('SHAPES', SHAPES)
        return Tool.Execute(Verbose)
    return False

def Point_Cloud_Reclassifier__Subset_Extractor(INPUT=None, RETAB_2=None, RESULT=None, ATTRIB=None, MODE=None, CREATE_ATTRIB=None, METHOD=None, OLD=None, NEW=None, SOPERATOR=None, MIN=None, MAX=None, RNEW=None, ROPERATOR=None, RETAB=None, TOPERATOR=None, F_MIN=None, F_MAX=None, F_CODE=None, NODATAOPT=None, NODATA=None, OTHEROPT=None, OTHERS=None, Verbose=2):
    '''
    Point Cloud Reclassifier / Subset Extractor
    ----------
    [pointcloud_tools.6]\n
    The tool can be used to either reclassify a Point Cloud attribute, to extract, or drop a subset of a Point Cloud based on the values of an attribute.\n
    The tool provides three different methods for selection of points to become reclassified/extracted/deleted:\n
    (-) single value\n
    (-) value range\n
    (-) value ranges specified in a lookup table\n
    Each of these three options provides it's own parameters. The 'new value' parameters are irrelevant in case a subset is extracted.\n
    In addition to these settings, two special cases ('NoData values' and 'other values' not included in the parameter setup) are supported:\n
    In mode (a) and (b) the 'NoData option' is evaluated before the method settings, in mode (c) the option is evaluated only if the NoData value isn't included in the lookup table.\n
    The 'other values' option is always evaluated after checking the method settings.\n
    Have in mind that subset deletion will be performed on the input point cloud!\n
    Arguments
    ----------
    - INPUT [`input point cloud`] : Point Cloud. Point Cloud to reclassify/extract
    - RETAB_2 [`optional input table`] : Lookup Table. Lookup table used in method "user supplied table"
    - RESULT [`output point cloud`] : Result. Reclassified or extracted Point Cloud.
    - ATTRIB [`table field`] : Attribute. Attribute to process.
    - MODE [`choice`] : Mode of Operation. Available Choices: [0] Reclassify [1] Extract Subset [2] Drop Subset Default: 0 Choose whether to reclassify a Point Cloud or to extract a subset from a Point Cloud.
    - CREATE_ATTRIB [`boolean`] : Create new Attribute. Default: 0 Check this to create a new attribute with the reclassification result. If unchecked, the existing attribute is updated.
    - METHOD [`choice`] : Selection Method. Available Choices: [0] single value [1] value range [2] simple table [3] user supplied table Default: 0 Select the desired method: 1. a single value or a range defined by a single value is reclassified, 2. a range of values is reclassified, 3. the lookup table is used to reclassify the grid.
    - OLD [`floating point number`] : Value. Default: 0.000000 Value to reclassify, extract or drop.
    - NEW [`floating point number`] : New Value. Default: 1.000000 New value.
    - SOPERATOR [`choice`] : Operator. Available Choices: [0] = [1] < [2] <= [3] >= [4] > [5] <> Default: 0 Select the desired operator (<;.;=; >;.); it is possible to define a range above or below the old value.
    - MIN [`floating point number`] : Minimum Value. Default: 0.000000 Minimum value of the range to be reclassified.
    - MAX [`floating point number`] : Maximum Value. Default: 10.000000 Maximum value of the range to be reclassified.
    - RNEW [`floating point number`] : New Value. Default: 5.000000 new value
    - ROPERATOR [`choice`] : Operator. Available Choices: [0] minimum <= value <= maximum [1] minimum < value < maximum [2] value < minimum or value > maximum [3] value <= minimum or value >= maximum Default: 0 Select operator: eg. min < value < max.
    - RETAB [`static table`] : Lookup Table. 3 Fields: - 1. [8 byte floating point number] minimum - 2. [8 byte floating point number] maximum - 3. [8 byte floating point number] new  Lookup table used in method "table"
    - TOPERATOR [`choice`] : Operator. Available Choices: [0] minimum <= value < maximum [1] minimum <= value <= maximum [2] minimum < value <= maximum [3] minimum < value < maximum Default: 0 Select the desired operator (min < value < max; min . value < max; min .value . max; min < value . max).
    - F_MIN [`table field`] : Minimum Value
    - F_MAX [`table field`] : Maximum Value
    - F_CODE [`table field`] : New Value
    - NODATAOPT [`boolean`] : No-Data Values. Default: 0 Use this option to reclassify No-Data values independently of the method settings.
    - NODATA [`floating point number`] : New Value. Default: 0.000000 new value
    - OTHEROPT [`boolean`] : Other Values. Default: 0 Use this option to reclassify all other values that are not specified in the options above.
    - OTHERS [`floating point number`] : New Value. Default: 0.000000 new value

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('pointcloud_tools', '6', 'Point Cloud Reclassifier / Subset Extractor')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Input ('RETAB_2', RETAB_2)
        Tool.Set_Output('RESULT', RESULT)
        Tool.Set_Option('ATTRIB', ATTRIB)
        Tool.Set_Option('MODE', MODE)
        Tool.Set_Option('CREATE_ATTRIB', CREATE_ATTRIB)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('OLD', OLD)
        Tool.Set_Option('NEW', NEW)
        Tool.Set_Option('SOPERATOR', SOPERATOR)
        Tool.Set_Option('MIN', MIN)
        Tool.Set_Option('MAX', MAX)
        Tool.Set_Option('RNEW', RNEW)
        Tool.Set_Option('ROPERATOR', ROPERATOR)
        Tool.Set_Option('RETAB', RETAB)
        Tool.Set_Option('TOPERATOR', TOPERATOR)
        Tool.Set_Option('F_MIN', F_MIN)
        Tool.Set_Option('F_MAX', F_MAX)
        Tool.Set_Option('F_CODE', F_CODE)
        Tool.Set_Option('NODATAOPT', NODATAOPT)
        Tool.Set_Option('NODATA', NODATA)
        Tool.Set_Option('OTHEROPT', OTHEROPT)
        Tool.Set_Option('OTHERS', OTHERS)
        return Tool.Execute(Verbose)
    return False

def run_tool_pointcloud_tools_6(INPUT=None, RETAB_2=None, RESULT=None, ATTRIB=None, MODE=None, CREATE_ATTRIB=None, METHOD=None, OLD=None, NEW=None, SOPERATOR=None, MIN=None, MAX=None, RNEW=None, ROPERATOR=None, RETAB=None, TOPERATOR=None, F_MIN=None, F_MAX=None, F_CODE=None, NODATAOPT=None, NODATA=None, OTHEROPT=None, OTHERS=None, Verbose=2):
    '''
    Point Cloud Reclassifier / Subset Extractor
    ----------
    [pointcloud_tools.6]\n
    The tool can be used to either reclassify a Point Cloud attribute, to extract, or drop a subset of a Point Cloud based on the values of an attribute.\n
    The tool provides three different methods for selection of points to become reclassified/extracted/deleted:\n
    (-) single value\n
    (-) value range\n
    (-) value ranges specified in a lookup table\n
    Each of these three options provides it's own parameters. The 'new value' parameters are irrelevant in case a subset is extracted.\n
    In addition to these settings, two special cases ('NoData values' and 'other values' not included in the parameter setup) are supported:\n
    In mode (a) and (b) the 'NoData option' is evaluated before the method settings, in mode (c) the option is evaluated only if the NoData value isn't included in the lookup table.\n
    The 'other values' option is always evaluated after checking the method settings.\n
    Have in mind that subset deletion will be performed on the input point cloud!\n
    Arguments
    ----------
    - INPUT [`input point cloud`] : Point Cloud. Point Cloud to reclassify/extract
    - RETAB_2 [`optional input table`] : Lookup Table. Lookup table used in method "user supplied table"
    - RESULT [`output point cloud`] : Result. Reclassified or extracted Point Cloud.
    - ATTRIB [`table field`] : Attribute. Attribute to process.
    - MODE [`choice`] : Mode of Operation. Available Choices: [0] Reclassify [1] Extract Subset [2] Drop Subset Default: 0 Choose whether to reclassify a Point Cloud or to extract a subset from a Point Cloud.
    - CREATE_ATTRIB [`boolean`] : Create new Attribute. Default: 0 Check this to create a new attribute with the reclassification result. If unchecked, the existing attribute is updated.
    - METHOD [`choice`] : Selection Method. Available Choices: [0] single value [1] value range [2] simple table [3] user supplied table Default: 0 Select the desired method: 1. a single value or a range defined by a single value is reclassified, 2. a range of values is reclassified, 3. the lookup table is used to reclassify the grid.
    - OLD [`floating point number`] : Value. Default: 0.000000 Value to reclassify, extract or drop.
    - NEW [`floating point number`] : New Value. Default: 1.000000 New value.
    - SOPERATOR [`choice`] : Operator. Available Choices: [0] = [1] < [2] <= [3] >= [4] > [5] <> Default: 0 Select the desired operator (<;.;=; >;.); it is possible to define a range above or below the old value.
    - MIN [`floating point number`] : Minimum Value. Default: 0.000000 Minimum value of the range to be reclassified.
    - MAX [`floating point number`] : Maximum Value. Default: 10.000000 Maximum value of the range to be reclassified.
    - RNEW [`floating point number`] : New Value. Default: 5.000000 new value
    - ROPERATOR [`choice`] : Operator. Available Choices: [0] minimum <= value <= maximum [1] minimum < value < maximum [2] value < minimum or value > maximum [3] value <= minimum or value >= maximum Default: 0 Select operator: eg. min < value < max.
    - RETAB [`static table`] : Lookup Table. 3 Fields: - 1. [8 byte floating point number] minimum - 2. [8 byte floating point number] maximum - 3. [8 byte floating point number] new  Lookup table used in method "table"
    - TOPERATOR [`choice`] : Operator. Available Choices: [0] minimum <= value < maximum [1] minimum <= value <= maximum [2] minimum < value <= maximum [3] minimum < value < maximum Default: 0 Select the desired operator (min < value < max; min . value < max; min .value . max; min < value . max).
    - F_MIN [`table field`] : Minimum Value
    - F_MAX [`table field`] : Maximum Value
    - F_CODE [`table field`] : New Value
    - NODATAOPT [`boolean`] : No-Data Values. Default: 0 Use this option to reclassify No-Data values independently of the method settings.
    - NODATA [`floating point number`] : New Value. Default: 0.000000 new value
    - OTHEROPT [`boolean`] : Other Values. Default: 0 Use this option to reclassify all other values that are not specified in the options above.
    - OTHERS [`floating point number`] : New Value. Default: 0.000000 new value

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('pointcloud_tools', '6', 'Point Cloud Reclassifier / Subset Extractor')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Input ('RETAB_2', RETAB_2)
        Tool.Set_Output('RESULT', RESULT)
        Tool.Set_Option('ATTRIB', ATTRIB)
        Tool.Set_Option('MODE', MODE)
        Tool.Set_Option('CREATE_ATTRIB', CREATE_ATTRIB)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('OLD', OLD)
        Tool.Set_Option('NEW', NEW)
        Tool.Set_Option('SOPERATOR', SOPERATOR)
        Tool.Set_Option('MIN', MIN)
        Tool.Set_Option('MAX', MAX)
        Tool.Set_Option('RNEW', RNEW)
        Tool.Set_Option('ROPERATOR', ROPERATOR)
        Tool.Set_Option('RETAB', RETAB)
        Tool.Set_Option('TOPERATOR', TOPERATOR)
        Tool.Set_Option('F_MIN', F_MIN)
        Tool.Set_Option('F_MAX', F_MAX)
        Tool.Set_Option('F_CODE', F_CODE)
        Tool.Set_Option('NODATAOPT', NODATAOPT)
        Tool.Set_Option('NODATA', NODATA)
        Tool.Set_Option('OTHEROPT', OTHEROPT)
        Tool.Set_Option('OTHERS', OTHERS)
        return Tool.Execute(Verbose)
    return False

def Drop_Point_Cloud_Attributes(INPUT=None, OUTPUT=None, FIELDS=None, Verbose=2):
    '''
    Drop Point Cloud Attributes
    ----------
    [pointcloud_tools.7]\n
    The tool can be used to drop attributes from a point cloud. In case the output dataset is not set, the attribute(s) will be dropped from the input dataset, i.e. the input dataset will be overwritten.\n
    Arguments
    ----------
    - INPUT [`input point cloud`] : Input. Point cloud to drop attribute(s) from.
    - OUTPUT [`output point cloud`] : Output. Point cloud with attribute(s) dropped.
    - FIELDS [`table fields`] : Attributes. The attribute field(s) to drop.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('pointcloud_tools', '7', 'Drop Point Cloud Attributes')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Output('OUTPUT', OUTPUT)
        Tool.Set_Option('FIELDS', FIELDS)
        return Tool.Execute(Verbose)
    return False

def run_tool_pointcloud_tools_7(INPUT=None, OUTPUT=None, FIELDS=None, Verbose=2):
    '''
    Drop Point Cloud Attributes
    ----------
    [pointcloud_tools.7]\n
    The tool can be used to drop attributes from a point cloud. In case the output dataset is not set, the attribute(s) will be dropped from the input dataset, i.e. the input dataset will be overwritten.\n
    Arguments
    ----------
    - INPUT [`input point cloud`] : Input. Point cloud to drop attribute(s) from.
    - OUTPUT [`output point cloud`] : Output. Point cloud with attribute(s) dropped.
    - FIELDS [`table fields`] : Attributes. The attribute field(s) to drop.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('pointcloud_tools', '7', 'Drop Point Cloud Attributes')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Output('OUTPUT', OUTPUT)
        Tool.Set_Option('FIELDS', FIELDS)
        return Tool.Execute(Verbose)
    return False

def Transform_Point_Cloud(IN=None, OUT=None, DX=None, DY=None, DZ=None, ANGLEX=None, ANGLEY=None, ANGLEZ=None, SCALEX=None, SCALEY=None, SCALEZ=None, ANCHORX=None, ANCHORY=None, ANCHORZ=None, Verbose=2):
    '''
    Transform Point Cloud
    ----------
    [pointcloud_tools.8]\n
    The tool allows one to move, rotate and/or scale a point cloud.\n
    Arguments
    ----------
    - IN [`input point cloud`] : Input. The input point cloud.
    - OUT [`output point cloud`] : Output. The transformed output point cloud.
    - DX [`floating point number`] : dX. Default: 0.000000 The shift along the x-axis [map units].
    - DY [`floating point number`] : dY. Default: 0.000000 The shift along the y-axis [map units].
    - DZ [`floating point number`] : dZ. Default: 0.000000 The shift along the z-axis [map units].
    - ANGLEX [`floating point number`] : Angle X. Default: 0.000000 Angle in degrees, clockwise around x-axis.
    - ANGLEY [`floating point number`] : Angle Y. Default: 0.000000 Angle in degrees, clockwise around y-axis.
    - ANGLEZ [`floating point number`] : Angle Z. Default: 0.000000 Angle in degrees, clockwise around z-axis.
    - SCALEX [`floating point number`] : Scale Factor X. Default: 1.000000 The scale factor in x-direction.
    - SCALEY [`floating point number`] : Scale Factor Y. Default: 1.000000 The scale factor in y-direction.
    - SCALEZ [`floating point number`] : Scale Factor Z. Default: 1.000000 The scale factor in z-direction.
    - ANCHORX [`floating point number`] : X. Default: 0.000000 The x-coordinate of the anchor point.
    - ANCHORY [`floating point number`] : Y. Default: 0.000000 The y-coordinate of the anchor point.
    - ANCHORZ [`floating point number`] : Z. Default: 0.000000 The z-coordinate of the anchor point.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('pointcloud_tools', '8', 'Transform Point Cloud')
    if Tool.is_Okay():
        Tool.Set_Input ('IN', IN)
        Tool.Set_Output('OUT', OUT)
        Tool.Set_Option('DX', DX)
        Tool.Set_Option('DY', DY)
        Tool.Set_Option('DZ', DZ)
        Tool.Set_Option('ANGLEX', ANGLEX)
        Tool.Set_Option('ANGLEY', ANGLEY)
        Tool.Set_Option('ANGLEZ', ANGLEZ)
        Tool.Set_Option('SCALEX', SCALEX)
        Tool.Set_Option('SCALEY', SCALEY)
        Tool.Set_Option('SCALEZ', SCALEZ)
        Tool.Set_Option('ANCHORX', ANCHORX)
        Tool.Set_Option('ANCHORY', ANCHORY)
        Tool.Set_Option('ANCHORZ', ANCHORZ)
        return Tool.Execute(Verbose)
    return False

def run_tool_pointcloud_tools_8(IN=None, OUT=None, DX=None, DY=None, DZ=None, ANGLEX=None, ANGLEY=None, ANGLEZ=None, SCALEX=None, SCALEY=None, SCALEZ=None, ANCHORX=None, ANCHORY=None, ANCHORZ=None, Verbose=2):
    '''
    Transform Point Cloud
    ----------
    [pointcloud_tools.8]\n
    The tool allows one to move, rotate and/or scale a point cloud.\n
    Arguments
    ----------
    - IN [`input point cloud`] : Input. The input point cloud.
    - OUT [`output point cloud`] : Output. The transformed output point cloud.
    - DX [`floating point number`] : dX. Default: 0.000000 The shift along the x-axis [map units].
    - DY [`floating point number`] : dY. Default: 0.000000 The shift along the y-axis [map units].
    - DZ [`floating point number`] : dZ. Default: 0.000000 The shift along the z-axis [map units].
    - ANGLEX [`floating point number`] : Angle X. Default: 0.000000 Angle in degrees, clockwise around x-axis.
    - ANGLEY [`floating point number`] : Angle Y. Default: 0.000000 Angle in degrees, clockwise around y-axis.
    - ANGLEZ [`floating point number`] : Angle Z. Default: 0.000000 Angle in degrees, clockwise around z-axis.
    - SCALEX [`floating point number`] : Scale Factor X. Default: 1.000000 The scale factor in x-direction.
    - SCALEY [`floating point number`] : Scale Factor Y. Default: 1.000000 The scale factor in y-direction.
    - SCALEZ [`floating point number`] : Scale Factor Z. Default: 1.000000 The scale factor in z-direction.
    - ANCHORX [`floating point number`] : X. Default: 0.000000 The x-coordinate of the anchor point.
    - ANCHORY [`floating point number`] : Y. Default: 0.000000 The y-coordinate of the anchor point.
    - ANCHORZ [`floating point number`] : Z. Default: 0.000000 The z-coordinate of the anchor point.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('pointcloud_tools', '8', 'Transform Point Cloud')
    if Tool.is_Okay():
        Tool.Set_Input ('IN', IN)
        Tool.Set_Output('OUT', OUT)
        Tool.Set_Option('DX', DX)
        Tool.Set_Option('DY', DY)
        Tool.Set_Option('DZ', DZ)
        Tool.Set_Option('ANGLEX', ANGLEX)
        Tool.Set_Option('ANGLEY', ANGLEY)
        Tool.Set_Option('ANGLEZ', ANGLEZ)
        Tool.Set_Option('SCALEX', SCALEX)
        Tool.Set_Option('SCALEY', SCALEY)
        Tool.Set_Option('SCALEZ', SCALEZ)
        Tool.Set_Option('ANCHORX', ANCHORX)
        Tool.Set_Option('ANCHORY', ANCHORY)
        Tool.Set_Option('ANCHORZ', ANCHORZ)
        return Tool.Execute(Verbose)
    return False

def Point_Cloud_Thinning_Simple(INPUT=None, RESULT=None, PERCENT=None, Verbose=2):
    '''
    Point Cloud Thinning (Simple)
    ----------
    [pointcloud_tools.9]\n
    This simple thinning tool reduces the number of points in a point cloud by sequential point removal. It is therefore most suited for points stored in chronological order.\n
    Arguments
    ----------
    - INPUT [`input point cloud`] : Points
    - RESULT [`output point cloud`] : Thinned Points
    - PERCENT [`floating point number`] : Percent. Minimum: 0.000000 Maximum: 100.000000 Default: 50.000000 Reduce the number of points to this percentage.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('pointcloud_tools', '9', 'Point Cloud Thinning (Simple)')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Output('RESULT', RESULT)
        Tool.Set_Option('PERCENT', PERCENT)
        return Tool.Execute(Verbose)
    return False

def run_tool_pointcloud_tools_9(INPUT=None, RESULT=None, PERCENT=None, Verbose=2):
    '''
    Point Cloud Thinning (Simple)
    ----------
    [pointcloud_tools.9]\n
    This simple thinning tool reduces the number of points in a point cloud by sequential point removal. It is therefore most suited for points stored in chronological order.\n
    Arguments
    ----------
    - INPUT [`input point cloud`] : Points
    - RESULT [`output point cloud`] : Thinned Points
    - PERCENT [`floating point number`] : Percent. Minimum: 0.000000 Maximum: 100.000000 Default: 50.000000 Reduce the number of points to this percentage.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('pointcloud_tools', '9', 'Point Cloud Thinning (Simple)')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Output('RESULT', RESULT)
        Tool.Set_Option('PERCENT', PERCENT)
        return Tool.Execute(Verbose)
    return False

def Point_Cloud_Attribute_Calculator(PC_IN=None, PC_OUT=None, FORMULA=None, NAME=None, FNAME=None, TYPE=None, USE_NODATA=None, Verbose=2):
    '''
    Point Cloud Attribute Calculator
    ----------
    [pointcloud_tools.10]\n
    The tool allows one to calculate a new attribute value per point, based on existing attributes of that point and a mathematical expression. Attribute fields are addressed by the character 'f' (for 'field') followed by the field number (i.e.: f1, f2, ..., fn) or by the field name in square brackets (e.g.: [Field Name]).\n
    Examples:\n
    sin(f1) * f2 + f3\n
    [intensity] / 1000\n
    The following operators are available for the formula definition:\n
    ============\n
    [+]	Addition\n
    [-]	Subtraction\n
    [*]	Multiplication\n
    [/]	Division\n
    [abs(x)]	Absolute Value\n
    [mod(x, y)]	Returns the floating point remainder of x/y\n
    [int(x)]	Returns the integer part of floating point value x\n
    [sqr(x)]	Square\n
    [sqrt(x)]	Square Root\n
    [exp(x)]	Exponential\n
    [pow(x, y)]	Returns x raised to the power of y\n
    [x ^ y]	Returns x raised to the power of y\n
    [ln(x)]	Natural Logarithm\n
    [log(x)]	Base 10 Logarithm\n
    [pi()]	Returns the value of Pi\n
    [sin(x)]	Sine, expects radians\n
    [cos(x)]	Cosine, expects radians\n
    [tan(x)]	Tangent, expects radians\n
    [asin(x)]	Arcsine, returns radians\n
    [acos(x)]	Arccosine, returns radians\n
    [atan(x)]	Arctangent, returns radians\n
    [atan2(x, y)]	Arctangent of x/y, returns radians\n
    [min(x, y)]	Returns the minimum of values x and y\n
    [max(x, y)]	Returns the maximum of values x and y\n
    [gt(x, y)]	Returns true (1), if x is greater than y, else false (0)\n
    [x > y]	Returns true (1), if x is greater than y, else false (0)\n
    [lt(x, y)]	Returns true (1), if x is less than y, else false (0)\n
    [x &lt; y]	Returns true (1), if x is less than y, else false (0)\n
    [eq(x, y)]	Returns true (1), if x equals y, else false (0)\n
    [x = y]	Returns true (1), if x equals y, else false (0)\n
    [and(x, y)]	Returns true (1), if both x and y are true (i.e. not 0)\n
    [or(x, y)]	Returns true (1), if at least one of both x and y is true (i.e. not 0)\n
    [ifelse(c, x, y)]	Returns x, if condition c is true (i.e. not 0), else y\n
    [rand_u(x, y)]	Random number, uniform distribution with minimum x and maximum y\n
    [rand_g(x, y)]	Random number, Gaussian distribution with mean x and standard deviation y\n
    ============\n
    Arguments
    ----------
    - PC_IN [`input point cloud`] : Point Cloud. Input point cloud.
    - PC_OUT [`output point cloud`] : Result. Output point cloud.
    - FORMULA [`text`] : Formula. Default: f1+f2
    - NAME [`text`] : Output Field Name. Default: Calculation
    - FNAME [`boolean`] : Take Formula. Default: 0
    - TYPE [`data type`] : Data Type. Available Choices: [0] bit [1] unsigned 1 byte integer [2] signed 1 byte integer [3] unsigned 2 byte integer [4] signed 2 byte integer [5] unsigned 4 byte integer [6] signed 4 byte integer [7] unsigned 8 byte integer [8] signed 8 byte integer [9] 4 byte floating point number [10] 8 byte floating point number Default: 9 Choose the data type of the output attribute.
    - USE_NODATA [`boolean`] : Use NoData. Default: 0 Include NoData values in the calculation. A typical application is the use of an ifelse() statement on NoData values.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('pointcloud_tools', '10', 'Point Cloud Attribute Calculator')
    if Tool.is_Okay():
        Tool.Set_Input ('PC_IN', PC_IN)
        Tool.Set_Output('PC_OUT', PC_OUT)
        Tool.Set_Option('FORMULA', FORMULA)
        Tool.Set_Option('NAME', NAME)
        Tool.Set_Option('FNAME', FNAME)
        Tool.Set_Option('TYPE', TYPE)
        Tool.Set_Option('USE_NODATA', USE_NODATA)
        return Tool.Execute(Verbose)
    return False

def run_tool_pointcloud_tools_10(PC_IN=None, PC_OUT=None, FORMULA=None, NAME=None, FNAME=None, TYPE=None, USE_NODATA=None, Verbose=2):
    '''
    Point Cloud Attribute Calculator
    ----------
    [pointcloud_tools.10]\n
    The tool allows one to calculate a new attribute value per point, based on existing attributes of that point and a mathematical expression. Attribute fields are addressed by the character 'f' (for 'field') followed by the field number (i.e.: f1, f2, ..., fn) or by the field name in square brackets (e.g.: [Field Name]).\n
    Examples:\n
    sin(f1) * f2 + f3\n
    [intensity] / 1000\n
    The following operators are available for the formula definition:\n
    ============\n
    [+]	Addition\n
    [-]	Subtraction\n
    [*]	Multiplication\n
    [/]	Division\n
    [abs(x)]	Absolute Value\n
    [mod(x, y)]	Returns the floating point remainder of x/y\n
    [int(x)]	Returns the integer part of floating point value x\n
    [sqr(x)]	Square\n
    [sqrt(x)]	Square Root\n
    [exp(x)]	Exponential\n
    [pow(x, y)]	Returns x raised to the power of y\n
    [x ^ y]	Returns x raised to the power of y\n
    [ln(x)]	Natural Logarithm\n
    [log(x)]	Base 10 Logarithm\n
    [pi()]	Returns the value of Pi\n
    [sin(x)]	Sine, expects radians\n
    [cos(x)]	Cosine, expects radians\n
    [tan(x)]	Tangent, expects radians\n
    [asin(x)]	Arcsine, returns radians\n
    [acos(x)]	Arccosine, returns radians\n
    [atan(x)]	Arctangent, returns radians\n
    [atan2(x, y)]	Arctangent of x/y, returns radians\n
    [min(x, y)]	Returns the minimum of values x and y\n
    [max(x, y)]	Returns the maximum of values x and y\n
    [gt(x, y)]	Returns true (1), if x is greater than y, else false (0)\n
    [x > y]	Returns true (1), if x is greater than y, else false (0)\n
    [lt(x, y)]	Returns true (1), if x is less than y, else false (0)\n
    [x &lt; y]	Returns true (1), if x is less than y, else false (0)\n
    [eq(x, y)]	Returns true (1), if x equals y, else false (0)\n
    [x = y]	Returns true (1), if x equals y, else false (0)\n
    [and(x, y)]	Returns true (1), if both x and y are true (i.e. not 0)\n
    [or(x, y)]	Returns true (1), if at least one of both x and y is true (i.e. not 0)\n
    [ifelse(c, x, y)]	Returns x, if condition c is true (i.e. not 0), else y\n
    [rand_u(x, y)]	Random number, uniform distribution with minimum x and maximum y\n
    [rand_g(x, y)]	Random number, Gaussian distribution with mean x and standard deviation y\n
    ============\n
    Arguments
    ----------
    - PC_IN [`input point cloud`] : Point Cloud. Input point cloud.
    - PC_OUT [`output point cloud`] : Result. Output point cloud.
    - FORMULA [`text`] : Formula. Default: f1+f2
    - NAME [`text`] : Output Field Name. Default: Calculation
    - FNAME [`boolean`] : Take Formula. Default: 0
    - TYPE [`data type`] : Data Type. Available Choices: [0] bit [1] unsigned 1 byte integer [2] signed 1 byte integer [3] unsigned 2 byte integer [4] signed 2 byte integer [5] unsigned 4 byte integer [6] signed 4 byte integer [7] unsigned 8 byte integer [8] signed 8 byte integer [9] 4 byte floating point number [10] 8 byte floating point number Default: 9 Choose the data type of the output attribute.
    - USE_NODATA [`boolean`] : Use NoData. Default: 0 Include NoData values in the calculation. A typical application is the use of an ifelse() statement on NoData values.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('pointcloud_tools', '10', 'Point Cloud Attribute Calculator')
    if Tool.is_Okay():
        Tool.Set_Input ('PC_IN', PC_IN)
        Tool.Set_Output('PC_OUT', PC_OUT)
        Tool.Set_Option('FORMULA', FORMULA)
        Tool.Set_Option('NAME', NAME)
        Tool.Set_Option('FNAME', FNAME)
        Tool.Set_Option('TYPE', TYPE)
        Tool.Set_Option('USE_NODATA', USE_NODATA)
        return Tool.Execute(Verbose)
    return False

def Cluster_Analysis_for_Point_Clouds(PC_IN=None, PC_OUT=None, STATISTICS=None, FIELDS=None, METHOD=None, NCLUSTER=None, NORMALISE=None, Verbose=2):
    '''
    Cluster Analysis for Point Clouds
    ----------
    [pointcloud_tools.11]\n
    Cluster analysis for point clouds.\n
    This tool is a port of the 'Cluster Analysis for Grids' tool from the 'Imagery - Classification' tool library.\n
    Arguments
    ----------
    - PC_IN [`input point cloud`] : Point Cloud. Input
    - PC_OUT [`output point cloud`] : Result. Output
    - STATISTICS [`output table`] : Statistics
    - FIELDS [`table fields`] : Attributes. The attribute fields to cluster
    - METHOD [`choice`] : Method. Available Choices: [0] Iterative Minimum Distance (Forgy 1965) [1] Hill-Climbing (Rubin 1967) [2] Combined Minimum Distance / Hillclimbing Default: 1
    - NCLUSTER [`integer number`] : Clusters. Minimum: 2 Default: 10 Number of clusters
    - NORMALISE [`boolean`] : Normalise. Default: 1 Automatically normalise attributes by standard deviation before clustering.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('pointcloud_tools', '11', 'Cluster Analysis for Point Clouds')
    if Tool.is_Okay():
        Tool.Set_Input ('PC_IN', PC_IN)
        Tool.Set_Output('PC_OUT', PC_OUT)
        Tool.Set_Output('STATISTICS', STATISTICS)
        Tool.Set_Option('FIELDS', FIELDS)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('NCLUSTER', NCLUSTER)
        Tool.Set_Option('NORMALISE', NORMALISE)
        return Tool.Execute(Verbose)
    return False

def run_tool_pointcloud_tools_11(PC_IN=None, PC_OUT=None, STATISTICS=None, FIELDS=None, METHOD=None, NCLUSTER=None, NORMALISE=None, Verbose=2):
    '''
    Cluster Analysis for Point Clouds
    ----------
    [pointcloud_tools.11]\n
    Cluster analysis for point clouds.\n
    This tool is a port of the 'Cluster Analysis for Grids' tool from the 'Imagery - Classification' tool library.\n
    Arguments
    ----------
    - PC_IN [`input point cloud`] : Point Cloud. Input
    - PC_OUT [`output point cloud`] : Result. Output
    - STATISTICS [`output table`] : Statistics
    - FIELDS [`table fields`] : Attributes. The attribute fields to cluster
    - METHOD [`choice`] : Method. Available Choices: [0] Iterative Minimum Distance (Forgy 1965) [1] Hill-Climbing (Rubin 1967) [2] Combined Minimum Distance / Hillclimbing Default: 1
    - NCLUSTER [`integer number`] : Clusters. Minimum: 2 Default: 10 Number of clusters
    - NORMALISE [`boolean`] : Normalise. Default: 1 Automatically normalise attributes by standard deviation before clustering.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('pointcloud_tools', '11', 'Cluster Analysis for Point Clouds')
    if Tool.is_Okay():
        Tool.Set_Input ('PC_IN', PC_IN)
        Tool.Set_Output('PC_OUT', PC_OUT)
        Tool.Set_Output('STATISTICS', STATISTICS)
        Tool.Set_Option('FIELDS', FIELDS)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('NCLUSTER', NCLUSTER)
        Tool.Set_Option('NORMALISE', NORMALISE)
        return Tool.Execute(Verbose)
    return False

def Merge_Point_Clouds(PC_LAYERS=None, PC_OUT=None, DEL_LAYERS=None, ADD_IDENTIFIER=None, START_VALUE=None, Verbose=2):
    '''
    Merge Point Clouds
    ----------
    [pointcloud_tools.12]\n
    This tool can be used to merge point clouds. The attribute fields of the merged point cloud resemble those of the first point cloud in the input list. In order to merge the attributes of the additional point cloud layers, these must be consistent (field name and type) with the first point cloud in the input list. Missing attribute values are set to no-data.\n
    Arguments
    ----------
    - PC_LAYERS [`input point cloud list`] : Point Clouds
    - PC_OUT [`output point cloud`] : Merged Point Cloud
    - DEL_LAYERS [`boolean`] : Delete Input. Default: 1 Removes input layers from memory while merging.
    - ADD_IDENTIFIER [`boolean`] : Add Input Identifier. Default: 0 Adds a field with an identifier for the input point cloud a point originates from.
    - START_VALUE [`integer number`] : Start Value. Default: 1 The start value to be used for the identifier.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('pointcloud_tools', '12', 'Merge Point Clouds')
    if Tool.is_Okay():
        Tool.Set_Input ('PC_LAYERS', PC_LAYERS)
        Tool.Set_Output('PC_OUT', PC_OUT)
        Tool.Set_Option('DEL_LAYERS', DEL_LAYERS)
        Tool.Set_Option('ADD_IDENTIFIER', ADD_IDENTIFIER)
        Tool.Set_Option('START_VALUE', START_VALUE)
        return Tool.Execute(Verbose)
    return False

def run_tool_pointcloud_tools_12(PC_LAYERS=None, PC_OUT=None, DEL_LAYERS=None, ADD_IDENTIFIER=None, START_VALUE=None, Verbose=2):
    '''
    Merge Point Clouds
    ----------
    [pointcloud_tools.12]\n
    This tool can be used to merge point clouds. The attribute fields of the merged point cloud resemble those of the first point cloud in the input list. In order to merge the attributes of the additional point cloud layers, these must be consistent (field name and type) with the first point cloud in the input list. Missing attribute values are set to no-data.\n
    Arguments
    ----------
    - PC_LAYERS [`input point cloud list`] : Point Clouds
    - PC_OUT [`output point cloud`] : Merged Point Cloud
    - DEL_LAYERS [`boolean`] : Delete Input. Default: 1 Removes input layers from memory while merging.
    - ADD_IDENTIFIER [`boolean`] : Add Input Identifier. Default: 0 Adds a field with an identifier for the input point cloud a point originates from.
    - START_VALUE [`integer number`] : Start Value. Default: 1 The start value to be used for the identifier.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('pointcloud_tools', '12', 'Merge Point Clouds')
    if Tool.is_Okay():
        Tool.Set_Input ('PC_LAYERS', PC_LAYERS)
        Tool.Set_Output('PC_OUT', PC_OUT)
        Tool.Set_Option('DEL_LAYERS', DEL_LAYERS)
        Tool.Set_Option('ADD_IDENTIFIER', ADD_IDENTIFIER)
        Tool.Set_Option('START_VALUE', START_VALUE)
        return Tool.Execute(Verbose)
    return False

def Point_Cloud_from_Table(TAB_IN=None, PC_OUT=None, FIELD_X=None, FIELD_Y=None, FIELD_Z=None, FIELDS=None, Verbose=2):
    '''
    Point Cloud from Table
    ----------
    [pointcloud_tools.13]\n
    This tool allows one to create a point cloud from a table.\n
    Arguments
    ----------
    - TAB_IN [`input table`] : Table. The input table.
    - PC_OUT [`output point cloud`] : Point Cloud. The output point cloud.
    - FIELD_X [`table field`] : X. The attribute field with the x-coordinate.
    - FIELD_Y [`table field`] : Y. The attribute field with the y-coordinate.
    - FIELD_Z [`table field`] : Z. The attribute field with the z-coordinate.
    - FIELDS [`table fields`] : Attributes. The attribute fields to convert, optional.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('pointcloud_tools', '13', 'Point Cloud from Table')
    if Tool.is_Okay():
        Tool.Set_Input ('TAB_IN', TAB_IN)
        Tool.Set_Output('PC_OUT', PC_OUT)
        Tool.Set_Option('FIELD_X', FIELD_X)
        Tool.Set_Option('FIELD_Y', FIELD_Y)
        Tool.Set_Option('FIELD_Z', FIELD_Z)
        Tool.Set_Option('FIELDS', FIELDS)
        return Tool.Execute(Verbose)
    return False

def run_tool_pointcloud_tools_13(TAB_IN=None, PC_OUT=None, FIELD_X=None, FIELD_Y=None, FIELD_Z=None, FIELDS=None, Verbose=2):
    '''
    Point Cloud from Table
    ----------
    [pointcloud_tools.13]\n
    This tool allows one to create a point cloud from a table.\n
    Arguments
    ----------
    - TAB_IN [`input table`] : Table. The input table.
    - PC_OUT [`output point cloud`] : Point Cloud. The output point cloud.
    - FIELD_X [`table field`] : X. The attribute field with the x-coordinate.
    - FIELD_Y [`table field`] : Y. The attribute field with the y-coordinate.
    - FIELD_Z [`table field`] : Z. The attribute field with the z-coordinate.
    - FIELDS [`table fields`] : Attributes. The attribute fields to convert, optional.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('pointcloud_tools', '13', 'Point Cloud from Table')
    if Tool.is_Okay():
        Tool.Set_Input ('TAB_IN', TAB_IN)
        Tool.Set_Output('PC_OUT', PC_OUT)
        Tool.Set_Option('FIELD_X', FIELD_X)
        Tool.Set_Option('FIELD_Y', FIELD_Y)
        Tool.Set_Option('FIELD_Z', FIELD_Z)
        Tool.Set_Option('FIELDS', FIELDS)
        return Tool.Execute(Verbose)
    return False

def Select_Point_Cloud_from_List(PC_LIST=None, PC=None, INDEX=None, Verbose=2):
    '''
    Select Point Cloud from List
    ----------
    [pointcloud_tools.14]\n
    Main use of this tool is to support tool chain development, allowing to pick a single point cloud from a point cloud list.\n
    Arguments
    ----------
    - PC_LIST [`input point cloud list`] : Point Cloud List. The input point cloud list.
    - PC [`output point cloud`] : Point Cloud. The point cloud picked from the point cloud list.
    - INDEX [`integer number`] : Index. Minimum: 0 Default: 0 The list index of the point cloud to pick. Indices start at zero.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('pointcloud_tools', '14', 'Select Point Cloud from List')
    if Tool.is_Okay():
        Tool.Set_Input ('PC_LIST', PC_LIST)
        Tool.Set_Output('PC', PC)
        Tool.Set_Option('INDEX', INDEX)
        return Tool.Execute(Verbose)
    return False

def run_tool_pointcloud_tools_14(PC_LIST=None, PC=None, INDEX=None, Verbose=2):
    '''
    Select Point Cloud from List
    ----------
    [pointcloud_tools.14]\n
    Main use of this tool is to support tool chain development, allowing to pick a single point cloud from a point cloud list.\n
    Arguments
    ----------
    - PC_LIST [`input point cloud list`] : Point Cloud List. The input point cloud list.
    - PC [`output point cloud`] : Point Cloud. The point cloud picked from the point cloud list.
    - INDEX [`integer number`] : Index. Minimum: 0 Default: 0 The list index of the point cloud to pick. Indices start at zero.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('pointcloud_tools', '14', 'Select Point Cloud from List')
    if Tool.is_Okay():
        Tool.Set_Input ('PC_LIST', PC_LIST)
        Tool.Set_Output('PC', PC)
        Tool.Set_Option('INDEX', INDEX)
        return Tool.Execute(Verbose)
    return False

def Ground_Classification(PC_IN=None, PC_OUT=None, RADIUS=None, TERRAINSLOPE=None, FILTERMOD=None, STDDEV=None, Verbose=2):
    '''
    Ground Classification
    ----------
    [pointcloud_tools.15]\n
    The tool allows one to filter a point cloud into ground (bare earth) and non-ground points. The ground points can be used later to create a digital elevation model from the data, for example.\n
    The tool uses concepts as described by Vosselman (2000) and is based on the assumption that a large height difference between two nearby points is unlikely to be caused by a steep slope in the terrain. The probability that the higher point might be non-ground increases when the distance between the two points decreases. Therefore the filter defines a maximum height difference (dz_max) between two points as a function of the distance (d) between the points (dz_max(d) = d). A point is classified as terrain if there is no other point within the kernel radius to which the height difference is larger than the allowed maximum height difference at the distance between these two points.\n
    The approximate terrain slope (s) parameter is used to modify the filter function to match the overall slope in the study area (dz_max(d) = d * s).\n
    A 5% confidence interval (ci = 1.65 * sqrt(2 * stddev)) may be used to modify the filter function even further by either relaxing (dz_max(d) = d * s + ci) or amplifying (dz_max(d) = d * s - ci) the filter criterium.\n
    Arguments
    ----------
    - PC_IN [`input point cloud`] : Point Cloud. The input point cloud to classify.
    - PC_OUT [`output point cloud`] : Point Cloud Classified. The classified point cloud.
    - RADIUS [`floating point number`] : Filter Radius. Minimum: 0.001000 Default: 2.500000 The radius of the filter kernel [map units]. Must be large enough to reach ground points next to non-ground objects.
    - TERRAINSLOPE [`floating point number`] : Terrain Slope [%]. Minimum: 0.000000 Default: 30.000000 The approximate terrain slope [%]. Used to relax the filter criterium in steeper terrain.
    - FILTERMOD [`choice`] : Filter Modification. Available Choices: [0] none [1] relax filter [2] amplify filter Default: 0 Choose whether to apply the filter kernel without modification or to use a confidence interval to relax or amplify the height criterium.
    - STDDEV [`floating point number`] : Standard Deviation. Minimum: 0.000000 Default: 0.100000 The standard deviation used to calculate a 5% confidence interval applied to the height threshold [map units].

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('pointcloud_tools', '15', 'Ground Classification')
    if Tool.is_Okay():
        Tool.Set_Input ('PC_IN', PC_IN)
        Tool.Set_Output('PC_OUT', PC_OUT)
        Tool.Set_Option('RADIUS', RADIUS)
        Tool.Set_Option('TERRAINSLOPE', TERRAINSLOPE)
        Tool.Set_Option('FILTERMOD', FILTERMOD)
        Tool.Set_Option('STDDEV', STDDEV)
        return Tool.Execute(Verbose)
    return False

def run_tool_pointcloud_tools_15(PC_IN=None, PC_OUT=None, RADIUS=None, TERRAINSLOPE=None, FILTERMOD=None, STDDEV=None, Verbose=2):
    '''
    Ground Classification
    ----------
    [pointcloud_tools.15]\n
    The tool allows one to filter a point cloud into ground (bare earth) and non-ground points. The ground points can be used later to create a digital elevation model from the data, for example.\n
    The tool uses concepts as described by Vosselman (2000) and is based on the assumption that a large height difference between two nearby points is unlikely to be caused by a steep slope in the terrain. The probability that the higher point might be non-ground increases when the distance between the two points decreases. Therefore the filter defines a maximum height difference (dz_max) between two points as a function of the distance (d) between the points (dz_max(d) = d). A point is classified as terrain if there is no other point within the kernel radius to which the height difference is larger than the allowed maximum height difference at the distance between these two points.\n
    The approximate terrain slope (s) parameter is used to modify the filter function to match the overall slope in the study area (dz_max(d) = d * s).\n
    A 5% confidence interval (ci = 1.65 * sqrt(2 * stddev)) may be used to modify the filter function even further by either relaxing (dz_max(d) = d * s + ci) or amplifying (dz_max(d) = d * s - ci) the filter criterium.\n
    Arguments
    ----------
    - PC_IN [`input point cloud`] : Point Cloud. The input point cloud to classify.
    - PC_OUT [`output point cloud`] : Point Cloud Classified. The classified point cloud.
    - RADIUS [`floating point number`] : Filter Radius. Minimum: 0.001000 Default: 2.500000 The radius of the filter kernel [map units]. Must be large enough to reach ground points next to non-ground objects.
    - TERRAINSLOPE [`floating point number`] : Terrain Slope [%]. Minimum: 0.000000 Default: 30.000000 The approximate terrain slope [%]. Used to relax the filter criterium in steeper terrain.
    - FILTERMOD [`choice`] : Filter Modification. Available Choices: [0] none [1] relax filter [2] amplify filter Default: 0 Choose whether to apply the filter kernel without modification or to use a confidence interval to relax or amplify the height criterium.
    - STDDEV [`floating point number`] : Standard Deviation. Minimum: 0.000000 Default: 0.100000 The standard deviation used to calculate a 5% confidence interval applied to the height threshold [map units].

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('pointcloud_tools', '15', 'Ground Classification')
    if Tool.is_Okay():
        Tool.Set_Input ('PC_IN', PC_IN)
        Tool.Set_Output('PC_OUT', PC_OUT)
        Tool.Set_Option('RADIUS', RADIUS)
        Tool.Set_Option('TERRAINSLOPE', TERRAINSLOPE)
        Tool.Set_Option('FILTERMOD', FILTERMOD)
        Tool.Set_Option('STDDEV', STDDEV)
        return Tool.Execute(Verbose)
    return False

def Isolated_Points_Filter(PC_IN=None, PC_OUT=None, RADIUS=None, MAX_POINTS=None, METHOD=None, Verbose=2):
    '''
    Isolated Points Filter
    ----------
    [pointcloud_tools.16]\n
    The tool allows one to detect isolated points within a point cloud. These points can be either tagged as "isolated" or be removed entirely from the dataset.\n
    A point is assumed to be isolated as soon as the number of points in the search radius is below the specified threshold.\n
    If isolated points become tagged, a new attribute field "ISOLATED" is added that provides the number of neighbours found for isolated points (including the point itself) or zero for all other points.\n
    Arguments
    ----------
    - PC_IN [`input point cloud`] : Points. The input point cloud to analyze.
    - PC_OUT [`output point cloud`] : Filtered Points. The filtered point cloud.
    - RADIUS [`floating point number`] : Filter Radius. Minimum: 0.001000 Default: 1.500000 The search radius of the filter [map units].
    - MAX_POINTS [`integer number`] : Maximum Number of Points. Minimum: 1 Maximum: 255 Default: 1 The maximum number of points within the search radius to consider a point as isolated. Includes the search point.
    - METHOD [`choice`] : Method. Available Choices: [0] remove points [1] tag points Default: 0 Choose the filter method.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('pointcloud_tools', '16', 'Isolated Points Filter')
    if Tool.is_Okay():
        Tool.Set_Input ('PC_IN', PC_IN)
        Tool.Set_Output('PC_OUT', PC_OUT)
        Tool.Set_Option('RADIUS', RADIUS)
        Tool.Set_Option('MAX_POINTS', MAX_POINTS)
        Tool.Set_Option('METHOD', METHOD)
        return Tool.Execute(Verbose)
    return False

def run_tool_pointcloud_tools_16(PC_IN=None, PC_OUT=None, RADIUS=None, MAX_POINTS=None, METHOD=None, Verbose=2):
    '''
    Isolated Points Filter
    ----------
    [pointcloud_tools.16]\n
    The tool allows one to detect isolated points within a point cloud. These points can be either tagged as "isolated" or be removed entirely from the dataset.\n
    A point is assumed to be isolated as soon as the number of points in the search radius is below the specified threshold.\n
    If isolated points become tagged, a new attribute field "ISOLATED" is added that provides the number of neighbours found for isolated points (including the point itself) or zero for all other points.\n
    Arguments
    ----------
    - PC_IN [`input point cloud`] : Points. The input point cloud to analyze.
    - PC_OUT [`output point cloud`] : Filtered Points. The filtered point cloud.
    - RADIUS [`floating point number`] : Filter Radius. Minimum: 0.001000 Default: 1.500000 The search radius of the filter [map units].
    - MAX_POINTS [`integer number`] : Maximum Number of Points. Minimum: 1 Maximum: 255 Default: 1 The maximum number of points within the search radius to consider a point as isolated. Includes the search point.
    - METHOD [`choice`] : Method. Available Choices: [0] remove points [1] tag points Default: 0 Choose the filter method.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('pointcloud_tools', '16', 'Isolated Points Filter')
    if Tool.is_Okay():
        Tool.Set_Input ('PC_IN', PC_IN)
        Tool.Set_Output('PC_OUT', PC_OUT)
        Tool.Set_Option('RADIUS', RADIUS)
        Tool.Set_Option('MAX_POINTS', MAX_POINTS)
        Tool.Set_Option('METHOD', METHOD)
        return Tool.Execute(Verbose)
    return False

