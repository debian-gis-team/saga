#! /usr/bin/env python

'''
Python Interface to SAGA Tools Library
----------
- Category : Grid
- Name     : Filter
- ID       : grid_filter

Description
----------
Various filter and convolution tools for raster data.
'''

from PySAGA.helper import Tool_Wrapper

def Simple_Filter(INPUT=None, RESULT=None, METHOD=None, KERNEL_TYPE=None, KERNEL_RADIUS=None, Verbose=2):
    '''
    Simple Filter
    ----------
    [grid_filter.0]\n
    Simple standard filters for grids.\n
    Arguments
    ----------
    - INPUT [`input grid`] : Grid
    - RESULT [`output grid`] : Filtered Grid
    - METHOD [`choice`] : Filter. Available Choices: [0] Smooth [1] Sharpen [2] Edge Default: 0 Choose the filter method.
    - KERNEL_TYPE [`choice`] : Kernel Type. Available Choices: [0] Square [1] Circle Default: 1 The kernel's shape.
    - KERNEL_RADIUS [`integer number`] : Radius. Minimum: 0 Default: 2 cells

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_filter', '0', 'Simple Filter')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Output('RESULT', RESULT)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('KERNEL_TYPE', KERNEL_TYPE)
        Tool.Set_Option('KERNEL_RADIUS', KERNEL_RADIUS)
        return Tool.Execute(Verbose)
    return False

def run_tool_grid_filter_0(INPUT=None, RESULT=None, METHOD=None, KERNEL_TYPE=None, KERNEL_RADIUS=None, Verbose=2):
    '''
    Simple Filter
    ----------
    [grid_filter.0]\n
    Simple standard filters for grids.\n
    Arguments
    ----------
    - INPUT [`input grid`] : Grid
    - RESULT [`output grid`] : Filtered Grid
    - METHOD [`choice`] : Filter. Available Choices: [0] Smooth [1] Sharpen [2] Edge Default: 0 Choose the filter method.
    - KERNEL_TYPE [`choice`] : Kernel Type. Available Choices: [0] Square [1] Circle Default: 1 The kernel's shape.
    - KERNEL_RADIUS [`integer number`] : Radius. Minimum: 0 Default: 2 cells

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_filter', '0', 'Simple Filter')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Output('RESULT', RESULT)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('KERNEL_TYPE', KERNEL_TYPE)
        Tool.Set_Option('KERNEL_RADIUS', KERNEL_RADIUS)
        return Tool.Execute(Verbose)
    return False

def Gaussian_Filter(INPUT=None, RESULT=None, KERNEL_RADIUS=None, SIGMA=None, Verbose=2):
    '''
    Gaussian Filter
    ----------
    [grid_filter.1]\n
    The Gaussian filter is a low-pass filter operator that is used to 'blur' or 'soften' data and to remove detail and noise.\n
    The degree of smoothing is determined by the kernel size specified as radius and the weighting of each raster cell within the kernel. The weighting scheme uses the Gaussian bell curve function and can be adjusted to the kernel size with the 'Standard Deviation' option.\n
    Arguments
    ----------
    - INPUT [`input grid`] : Grid
    - RESULT [`output grid`] : Filtered Grid
    - KERNEL_RADIUS [`integer number`] : Kernel Radius. Minimum: 1 Default: 2
    - SIGMA [`floating point number`] : Standard Deviation. Minimum: 1.000000 Default: 50.000000 The standard deviation as percentage of the kernel radius.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_filter', '1', 'Gaussian Filter')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Output('RESULT', RESULT)
        Tool.Set_Option('KERNEL_RADIUS', KERNEL_RADIUS)
        Tool.Set_Option('SIGMA', SIGMA)
        return Tool.Execute(Verbose)
    return False

def run_tool_grid_filter_1(INPUT=None, RESULT=None, KERNEL_RADIUS=None, SIGMA=None, Verbose=2):
    '''
    Gaussian Filter
    ----------
    [grid_filter.1]\n
    The Gaussian filter is a low-pass filter operator that is used to 'blur' or 'soften' data and to remove detail and noise.\n
    The degree of smoothing is determined by the kernel size specified as radius and the weighting of each raster cell within the kernel. The weighting scheme uses the Gaussian bell curve function and can be adjusted to the kernel size with the 'Standard Deviation' option.\n
    Arguments
    ----------
    - INPUT [`input grid`] : Grid
    - RESULT [`output grid`] : Filtered Grid
    - KERNEL_RADIUS [`integer number`] : Kernel Radius. Minimum: 1 Default: 2
    - SIGMA [`floating point number`] : Standard Deviation. Minimum: 1.000000 Default: 50.000000 The standard deviation as percentage of the kernel radius.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_filter', '1', 'Gaussian Filter')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Output('RESULT', RESULT)
        Tool.Set_Option('KERNEL_RADIUS', KERNEL_RADIUS)
        Tool.Set_Option('SIGMA', SIGMA)
        return Tool.Execute(Verbose)
    return False

def Laplacian_Filter(INPUT=None, RESULT=None, METHOD=None, KERNEL_RADIUS=None, SIGMA=None, Verbose=2):
    '''
    Laplacian Filter
    ----------
    [grid_filter.2]\n
    The Laplacian filter is a high-pass filter operator that is commonly used for edge detection. Also referred to as Laplacian of Gaussian (LoG) or Marr-Hildreth-Operator.\n
    The kernel can be defined by search radius and weighting function adjustment or be chosen from predefined standards.\n
    Standard kernel 1 (3x3):\n
    ============\n
    0	-1	 0\n
    -1	 4	-1\n
    0	-1	 0\n
    ============\n
    Standard kernel 2 (3x3):\n
    ============\n
    -1	-1	-1\n
    -1	 8	-1\n
    -1	-1	-1\n
    ============\n
    Standard kernel 3 (3x3):\n
    ============\n
    -1	-2	-1\n
    -2	12	-2\n
    -1	-2	-1\n
    ============\n
    Arguments
    ----------
    - INPUT [`input grid`] : Grid
    - RESULT [`output grid`] : Filtered Grid
    - METHOD [`choice`] : Method. Available Choices: [0] standard kernel 1 [1] standard kernel 2 [2] Standard kernel 3 [3] user defined kernel Default: 3
    - KERNEL_RADIUS [`integer number`] : Kernel Radius. Minimum: 1 Default: 2
    - SIGMA [`floating point number`] : Standard Deviation. Minimum: 1.000000 Default: 50.000000 The standard deviation as percentage of the kernel radius.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_filter', '2', 'Laplacian Filter')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Output('RESULT', RESULT)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('KERNEL_RADIUS', KERNEL_RADIUS)
        Tool.Set_Option('SIGMA', SIGMA)
        return Tool.Execute(Verbose)
    return False

def run_tool_grid_filter_2(INPUT=None, RESULT=None, METHOD=None, KERNEL_RADIUS=None, SIGMA=None, Verbose=2):
    '''
    Laplacian Filter
    ----------
    [grid_filter.2]\n
    The Laplacian filter is a high-pass filter operator that is commonly used for edge detection. Also referred to as Laplacian of Gaussian (LoG) or Marr-Hildreth-Operator.\n
    The kernel can be defined by search radius and weighting function adjustment or be chosen from predefined standards.\n
    Standard kernel 1 (3x3):\n
    ============\n
    0	-1	 0\n
    -1	 4	-1\n
    0	-1	 0\n
    ============\n
    Standard kernel 2 (3x3):\n
    ============\n
    -1	-1	-1\n
    -1	 8	-1\n
    -1	-1	-1\n
    ============\n
    Standard kernel 3 (3x3):\n
    ============\n
    -1	-2	-1\n
    -2	12	-2\n
    -1	-2	-1\n
    ============\n
    Arguments
    ----------
    - INPUT [`input grid`] : Grid
    - RESULT [`output grid`] : Filtered Grid
    - METHOD [`choice`] : Method. Available Choices: [0] standard kernel 1 [1] standard kernel 2 [2] Standard kernel 3 [3] user defined kernel Default: 3
    - KERNEL_RADIUS [`integer number`] : Kernel Radius. Minimum: 1 Default: 2
    - SIGMA [`floating point number`] : Standard Deviation. Minimum: 1.000000 Default: 50.000000 The standard deviation as percentage of the kernel radius.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_filter', '2', 'Laplacian Filter')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Output('RESULT', RESULT)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('KERNEL_RADIUS', KERNEL_RADIUS)
        Tool.Set_Option('SIGMA', SIGMA)
        return Tool.Execute(Verbose)
    return False

def Multi_Direction_Lee_Filter(INPUT=None, RESULT=None, STDDEV=None, DIR=None, NOISE_ABS=None, NOISE_REL=None, WEIGHTED=None, METHOD=None, Verbose=2):
    '''
    Multi Direction Lee Filter
    ----------
    [grid_filter.3]\n
    The tool searches for the minimum variance within 16 directions and applies a Lee Filter in the direction of minimum variance. The filter is edge-preserving and can be used to remove speckle noise from SAR images or to smooth DTMs. Applied to DTMs, this filter will preserve slope breaks and narrow valleys.\n
    For more details, please refer to the references.\n
    Arguments
    ----------
    - INPUT [`input grid`] : Grid
    - RESULT [`output grid`] : Filtered Grid
    - STDDEV [`output grid`] : Minimum Standard Deviation
    - DIR [`output grid`] : Direction of Minimum Standard Deviation
    - NOISE_ABS [`floating point number`] : Estimated Noise (absolute). Minimum: 0.000000 Default: 1.000000 Estimated noise in units of input data
    - NOISE_REL [`floating point number`] : Estimated Noise (relative). Minimum: 0.000000 Default: 1.000000 Estimated noise relative to mean standard deviation
    - WEIGHTED [`boolean`] : Weighted. Default: 1
    - METHOD [`choice`] : Method. Available Choices: [0] noise variance given as absolute value [1] noise variance given relative to mean standard deviation [2] original calculation (Ringeler) Default: 1

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_filter', '3', 'Multi Direction Lee Filter')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Output('RESULT', RESULT)
        Tool.Set_Output('STDDEV', STDDEV)
        Tool.Set_Output('DIR', DIR)
        Tool.Set_Option('NOISE_ABS', NOISE_ABS)
        Tool.Set_Option('NOISE_REL', NOISE_REL)
        Tool.Set_Option('WEIGHTED', WEIGHTED)
        Tool.Set_Option('METHOD', METHOD)
        return Tool.Execute(Verbose)
    return False

def run_tool_grid_filter_3(INPUT=None, RESULT=None, STDDEV=None, DIR=None, NOISE_ABS=None, NOISE_REL=None, WEIGHTED=None, METHOD=None, Verbose=2):
    '''
    Multi Direction Lee Filter
    ----------
    [grid_filter.3]\n
    The tool searches for the minimum variance within 16 directions and applies a Lee Filter in the direction of minimum variance. The filter is edge-preserving and can be used to remove speckle noise from SAR images or to smooth DTMs. Applied to DTMs, this filter will preserve slope breaks and narrow valleys.\n
    For more details, please refer to the references.\n
    Arguments
    ----------
    - INPUT [`input grid`] : Grid
    - RESULT [`output grid`] : Filtered Grid
    - STDDEV [`output grid`] : Minimum Standard Deviation
    - DIR [`output grid`] : Direction of Minimum Standard Deviation
    - NOISE_ABS [`floating point number`] : Estimated Noise (absolute). Minimum: 0.000000 Default: 1.000000 Estimated noise in units of input data
    - NOISE_REL [`floating point number`] : Estimated Noise (relative). Minimum: 0.000000 Default: 1.000000 Estimated noise relative to mean standard deviation
    - WEIGHTED [`boolean`] : Weighted. Default: 1
    - METHOD [`choice`] : Method. Available Choices: [0] noise variance given as absolute value [1] noise variance given relative to mean standard deviation [2] original calculation (Ringeler) Default: 1

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_filter', '3', 'Multi Direction Lee Filter')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Output('RESULT', RESULT)
        Tool.Set_Output('STDDEV', STDDEV)
        Tool.Set_Output('DIR', DIR)
        Tool.Set_Option('NOISE_ABS', NOISE_ABS)
        Tool.Set_Option('NOISE_REL', NOISE_REL)
        Tool.Set_Option('WEIGHTED', WEIGHTED)
        Tool.Set_Option('METHOD', METHOD)
        return Tool.Execute(Verbose)
    return False

def User_Defined_Filter(INPUT=None, FILTER=None, RESULT=None, FILTER_3X3=None, ABSOLUTE=None, Verbose=2):
    '''
    User Defined Filter
    ----------
    [grid_filter.4]\n
    User defined filter matrix. The filter can be chosen from loaded tables. If not specified a fixed table with 3 rows (and 3 columns) will be used.\n
    Arguments
    ----------
    - INPUT [`input grid`] : Grid
    - FILTER [`optional input table`] : Kernel
    - RESULT [`output grid`] : Filtered Grid
    - FILTER_3X3 [`static table`] : 3x3 Kernel. 3 Fields: - 1. [8 byte floating point number] 1 - 2. [8 byte floating point number] 2 - 3. [8 byte floating point number] 3 
    - ABSOLUTE [`boolean`] : Absolute Weighting. Default: 1 If not checked to be absolute resulting sum will become divided by the sum of filter kernel's weights.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_filter', '4', 'User Defined Filter')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Input ('FILTER', FILTER)
        Tool.Set_Output('RESULT', RESULT)
        Tool.Set_Option('FILTER_3X3', FILTER_3X3)
        Tool.Set_Option('ABSOLUTE', ABSOLUTE)
        return Tool.Execute(Verbose)
    return False

def run_tool_grid_filter_4(INPUT=None, FILTER=None, RESULT=None, FILTER_3X3=None, ABSOLUTE=None, Verbose=2):
    '''
    User Defined Filter
    ----------
    [grid_filter.4]\n
    User defined filter matrix. The filter can be chosen from loaded tables. If not specified a fixed table with 3 rows (and 3 columns) will be used.\n
    Arguments
    ----------
    - INPUT [`input grid`] : Grid
    - FILTER [`optional input table`] : Kernel
    - RESULT [`output grid`] : Filtered Grid
    - FILTER_3X3 [`static table`] : 3x3 Kernel. 3 Fields: - 1. [8 byte floating point number] 1 - 2. [8 byte floating point number] 2 - 3. [8 byte floating point number] 3 
    - ABSOLUTE [`boolean`] : Absolute Weighting. Default: 1 If not checked to be absolute resulting sum will become divided by the sum of filter kernel's weights.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_filter', '4', 'User Defined Filter')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Input ('FILTER', FILTER)
        Tool.Set_Output('RESULT', RESULT)
        Tool.Set_Option('FILTER_3X3', FILTER_3X3)
        Tool.Set_Option('ABSOLUTE', ABSOLUTE)
        return Tool.Execute(Verbose)
    return False

def Filter_Clumps(GRID=None, OUTPUT=None, THRESHOLD=None, Verbose=2):
    '''
    Filter Clumps
    ----------
    [grid_filter.5]\n
    (c) 2004 by Victor Olaya. Filter Clumps\n
    Arguments
    ----------
    - GRID [`input grid`] : Input Grid
    - OUTPUT [`output grid`] : Filtered Grid
    - THRESHOLD [`integer number`] : Min. Size. Default: 10 Min. Size (Cells)

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_filter', '5', 'Filter Clumps')
    if Tool.is_Okay():
        Tool.Set_Input ('GRID', GRID)
        Tool.Set_Output('OUTPUT', OUTPUT)
        Tool.Set_Option('THRESHOLD', THRESHOLD)
        return Tool.Execute(Verbose)
    return False

def run_tool_grid_filter_5(GRID=None, OUTPUT=None, THRESHOLD=None, Verbose=2):
    '''
    Filter Clumps
    ----------
    [grid_filter.5]\n
    (c) 2004 by Victor Olaya. Filter Clumps\n
    Arguments
    ----------
    - GRID [`input grid`] : Input Grid
    - OUTPUT [`output grid`] : Filtered Grid
    - THRESHOLD [`integer number`] : Min. Size. Default: 10 Min. Size (Cells)

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_filter', '5', 'Filter Clumps')
    if Tool.is_Okay():
        Tool.Set_Input ('GRID', GRID)
        Tool.Set_Output('OUTPUT', OUTPUT)
        Tool.Set_Option('THRESHOLD', THRESHOLD)
        return Tool.Execute(Verbose)
    return False

def MajorityMinority_Filter(INPUT=None, RESULT=None, TYPE=None, THRESHOLD=None, KERNEL_TYPE=None, KERNEL_RADIUS=None, Verbose=2):
    '''
    Majority/Minority Filter
    ----------
    [grid_filter.6]\n
    Majority filter for grids.\n
    Arguments
    ----------
    - INPUT [`input grid`] : Grid
    - RESULT [`output grid`] : Filtered Grid
    - TYPE [`choice`] : Type. Available Choices: [0] Majority [1] Minority Default: 0
    - THRESHOLD [`floating point number`] : Threshold. Minimum: 0.000000 Maximum: 100.000000 Default: 0.000000 The majority/minority threshold [percent].
    - KERNEL_TYPE [`choice`] : Kernel Type. Available Choices: [0] Square [1] Circle Default: 1 The kernel's shape.
    - KERNEL_RADIUS [`integer number`] : Radius. Minimum: 0 Default: 2 cells

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_filter', '6', 'Majority/Minority Filter')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Output('RESULT', RESULT)
        Tool.Set_Option('TYPE', TYPE)
        Tool.Set_Option('THRESHOLD', THRESHOLD)
        Tool.Set_Option('KERNEL_TYPE', KERNEL_TYPE)
        Tool.Set_Option('KERNEL_RADIUS', KERNEL_RADIUS)
        return Tool.Execute(Verbose)
    return False

def run_tool_grid_filter_6(INPUT=None, RESULT=None, TYPE=None, THRESHOLD=None, KERNEL_TYPE=None, KERNEL_RADIUS=None, Verbose=2):
    '''
    Majority/Minority Filter
    ----------
    [grid_filter.6]\n
    Majority filter for grids.\n
    Arguments
    ----------
    - INPUT [`input grid`] : Grid
    - RESULT [`output grid`] : Filtered Grid
    - TYPE [`choice`] : Type. Available Choices: [0] Majority [1] Minority Default: 0
    - THRESHOLD [`floating point number`] : Threshold. Minimum: 0.000000 Maximum: 100.000000 Default: 0.000000 The majority/minority threshold [percent].
    - KERNEL_TYPE [`choice`] : Kernel Type. Available Choices: [0] Square [1] Circle Default: 1 The kernel's shape.
    - KERNEL_RADIUS [`integer number`] : Radius. Minimum: 0 Default: 2 cells

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_filter', '6', 'Majority/Minority Filter')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Output('RESULT', RESULT)
        Tool.Set_Option('TYPE', TYPE)
        Tool.Set_Option('THRESHOLD', THRESHOLD)
        Tool.Set_Option('KERNEL_TYPE', KERNEL_TYPE)
        Tool.Set_Option('KERNEL_RADIUS', KERNEL_RADIUS)
        return Tool.Execute(Verbose)
    return False

def DTM_Filter_slopebased(INPUT=None, GROUND=None, NONGROUND=None, RADIUS=None, TERRAINSLOPE=None, FILTERMOD=None, STDDEV=None, Verbose=2):
    '''
    DTM Filter (slope-based)
    ----------
    [grid_filter.7]\n
    The tool can be used to filter a digital elevation model in order to classify its cells into bare earth and object cells (ground and non-ground cells).\n
    The tool uses concepts as described by Vosselman (2000) and is based on the assumption that a large height difference between two nearby cells is unlikely to be caused by a steep slope in the terrain. The probability that the higher cell might be non-ground increases when the distance between the two cells decreases. Therefore the filter defines a maximum height difference (dz_max) between two cells as a function of the distance (d) between the cells (dz_max(d) = d). A cell is classified as terrain if there is no cell within the kernel radius to which the height difference is larger than the allowed maximum height difference at the distance between these two cells.\n
    The approximate terrain slope (s) parameter is used to modify the filter function to match the overall slope in the study area (dz_max(d) = d * s).\n
    A 5% confidence interval (ci = 1.65 * sqrt(2 * stddev)) may be used to modify the filter function even further by either relaxing (dz_max(d) = d * s + ci) or amplifying (dz_max(d) = d * s - ci) the filter criterium.\n
    Arguments
    ----------
    - INPUT [`input grid`] : DEM. The grid to filter.
    - GROUND [`output grid`] : Bare Earth. The filtered DEM containing only cells classified as ground.
    - NONGROUND [`output grid`] : Removed Objects. The non-ground objects removed by the filter.
    - RADIUS [`integer number`] : Kernel Radius. Minimum: 1 Default: 5 The radius of the filter kernel [grid cells]. Must be large enough to reach ground cells next to non-ground objects.
    - TERRAINSLOPE [`floating point number`] : Terrain Slope [%]. Minimum: 0.000000 Default: 30.000000 The approximate terrain slope [%]. Used to relax the filter criterium in steeper terrain.
    - FILTERMOD [`choice`] : Filter Modification. Available Choices: [0] none [1] relax filter [2] amplify filter Default: 0 Choose whether to apply the filter kernel without modification or to use a confidence interval to relax or amplify the height criterium.
    - STDDEV [`floating point number`] : Standard Deviation. Minimum: 0.000000 Default: 0.100000 The standard deviation used to calculate a 5% confidence interval applied to the height threshold [map units].

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_filter', '7', 'DTM Filter (slope-based)')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Output('GROUND', GROUND)
        Tool.Set_Output('NONGROUND', NONGROUND)
        Tool.Set_Option('RADIUS', RADIUS)
        Tool.Set_Option('TERRAINSLOPE', TERRAINSLOPE)
        Tool.Set_Option('FILTERMOD', FILTERMOD)
        Tool.Set_Option('STDDEV', STDDEV)
        return Tool.Execute(Verbose)
    return False

def run_tool_grid_filter_7(INPUT=None, GROUND=None, NONGROUND=None, RADIUS=None, TERRAINSLOPE=None, FILTERMOD=None, STDDEV=None, Verbose=2):
    '''
    DTM Filter (slope-based)
    ----------
    [grid_filter.7]\n
    The tool can be used to filter a digital elevation model in order to classify its cells into bare earth and object cells (ground and non-ground cells).\n
    The tool uses concepts as described by Vosselman (2000) and is based on the assumption that a large height difference between two nearby cells is unlikely to be caused by a steep slope in the terrain. The probability that the higher cell might be non-ground increases when the distance between the two cells decreases. Therefore the filter defines a maximum height difference (dz_max) between two cells as a function of the distance (d) between the cells (dz_max(d) = d). A cell is classified as terrain if there is no cell within the kernel radius to which the height difference is larger than the allowed maximum height difference at the distance between these two cells.\n
    The approximate terrain slope (s) parameter is used to modify the filter function to match the overall slope in the study area (dz_max(d) = d * s).\n
    A 5% confidence interval (ci = 1.65 * sqrt(2 * stddev)) may be used to modify the filter function even further by either relaxing (dz_max(d) = d * s + ci) or amplifying (dz_max(d) = d * s - ci) the filter criterium.\n
    Arguments
    ----------
    - INPUT [`input grid`] : DEM. The grid to filter.
    - GROUND [`output grid`] : Bare Earth. The filtered DEM containing only cells classified as ground.
    - NONGROUND [`output grid`] : Removed Objects. The non-ground objects removed by the filter.
    - RADIUS [`integer number`] : Kernel Radius. Minimum: 1 Default: 5 The radius of the filter kernel [grid cells]. Must be large enough to reach ground cells next to non-ground objects.
    - TERRAINSLOPE [`floating point number`] : Terrain Slope [%]. Minimum: 0.000000 Default: 30.000000 The approximate terrain slope [%]. Used to relax the filter criterium in steeper terrain.
    - FILTERMOD [`choice`] : Filter Modification. Available Choices: [0] none [1] relax filter [2] amplify filter Default: 0 Choose whether to apply the filter kernel without modification or to use a confidence interval to relax or amplify the height criterium.
    - STDDEV [`floating point number`] : Standard Deviation. Minimum: 0.000000 Default: 0.100000 The standard deviation used to calculate a 5% confidence interval applied to the height threshold [map units].

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_filter', '7', 'DTM Filter (slope-based)')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Output('GROUND', GROUND)
        Tool.Set_Output('NONGROUND', NONGROUND)
        Tool.Set_Option('RADIUS', RADIUS)
        Tool.Set_Option('TERRAINSLOPE', TERRAINSLOPE)
        Tool.Set_Option('FILTERMOD', FILTERMOD)
        Tool.Set_Option('STDDEV', STDDEV)
        return Tool.Execute(Verbose)
    return False

def Morphological_Filter(INPUT=None, RESULT=None, METHOD=None, KERNEL_TYPE=None, KERNEL_RADIUS=None, Verbose=2):
    '''
    Morphological Filter
    ----------
    [grid_filter.8]\n
    Morphological filter for grids. Dilation returns the maximum and erosion the minimum value found in a cell's neighbourhood as defined by the kernel. Opening applies first an erosion followed by a dilation and closing is a dilation followed by an erosion.\n
    Arguments
    ----------
    - INPUT [`input grid`] : Grid
    - RESULT [`output grid`] : Filtered Grid
    - METHOD [`choice`] : Method. Available Choices: [0] Dilation [1] Erosion [2] Opening [3] Closing Default: 0 Choose the operation to perform.
    - KERNEL_TYPE [`choice`] : Kernel Type. Available Choices: [0] Square [1] Circle Default: 1 The kernel's shape.
    - KERNEL_RADIUS [`integer number`] : Radius. Minimum: 0 Default: 2 cells

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_filter', '8', 'Morphological Filter')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Output('RESULT', RESULT)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('KERNEL_TYPE', KERNEL_TYPE)
        Tool.Set_Option('KERNEL_RADIUS', KERNEL_RADIUS)
        return Tool.Execute(Verbose)
    return False

def run_tool_grid_filter_8(INPUT=None, RESULT=None, METHOD=None, KERNEL_TYPE=None, KERNEL_RADIUS=None, Verbose=2):
    '''
    Morphological Filter
    ----------
    [grid_filter.8]\n
    Morphological filter for grids. Dilation returns the maximum and erosion the minimum value found in a cell's neighbourhood as defined by the kernel. Opening applies first an erosion followed by a dilation and closing is a dilation followed by an erosion.\n
    Arguments
    ----------
    - INPUT [`input grid`] : Grid
    - RESULT [`output grid`] : Filtered Grid
    - METHOD [`choice`] : Method. Available Choices: [0] Dilation [1] Erosion [2] Opening [3] Closing Default: 0 Choose the operation to perform.
    - KERNEL_TYPE [`choice`] : Kernel Type. Available Choices: [0] Square [1] Circle Default: 1 The kernel's shape.
    - KERNEL_RADIUS [`integer number`] : Radius. Minimum: 0 Default: 2 cells

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_filter', '8', 'Morphological Filter')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Output('RESULT', RESULT)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('KERNEL_TYPE', KERNEL_TYPE)
        Tool.Set_Option('KERNEL_RADIUS', KERNEL_RADIUS)
        return Tool.Execute(Verbose)
    return False

def Rank_Filter(INPUT=None, RESULT=None, RANK=None, KERNEL_TYPE=None, KERNEL_RADIUS=None, Verbose=2):
    '''
    Rank Filter
    ----------
    [grid_filter.9]\n
    Rank filter for grids. Set rank to fifty percent to apply a median filter.\n
    Arguments
    ----------
    - INPUT [`input grid`] : Grid
    - RESULT [`output grid`] : Filtered Grid
    - RANK [`floating point number`] : Rank. Minimum: 0.000000 Maximum: 100.000000 Default: 50.000000 The rank [percent].
    - KERNEL_TYPE [`choice`] : Kernel Type. Available Choices: [0] Square [1] Circle Default: 1 The kernel's shape.
    - KERNEL_RADIUS [`integer number`] : Radius. Minimum: 0 Default: 2 cells

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_filter', '9', 'Rank Filter')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Output('RESULT', RESULT)
        Tool.Set_Option('RANK', RANK)
        Tool.Set_Option('KERNEL_TYPE', KERNEL_TYPE)
        Tool.Set_Option('KERNEL_RADIUS', KERNEL_RADIUS)
        return Tool.Execute(Verbose)
    return False

def run_tool_grid_filter_9(INPUT=None, RESULT=None, RANK=None, KERNEL_TYPE=None, KERNEL_RADIUS=None, Verbose=2):
    '''
    Rank Filter
    ----------
    [grid_filter.9]\n
    Rank filter for grids. Set rank to fifty percent to apply a median filter.\n
    Arguments
    ----------
    - INPUT [`input grid`] : Grid
    - RESULT [`output grid`] : Filtered Grid
    - RANK [`floating point number`] : Rank. Minimum: 0.000000 Maximum: 100.000000 Default: 50.000000 The rank [percent].
    - KERNEL_TYPE [`choice`] : Kernel Type. Available Choices: [0] Square [1] Circle Default: 1 The kernel's shape.
    - KERNEL_RADIUS [`integer number`] : Radius. Minimum: 0 Default: 2 cells

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_filter', '9', 'Rank Filter')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Output('RESULT', RESULT)
        Tool.Set_Option('RANK', RANK)
        Tool.Set_Option('KERNEL_TYPE', KERNEL_TYPE)
        Tool.Set_Option('KERNEL_RADIUS', KERNEL_RADIUS)
        return Tool.Execute(Verbose)
    return False

def Mesh_Denoise(INPUT=None, OUTPUT=None, SIGMA=None, ITER=None, VITER=None, NB_CV=None, ZONLY=None, Verbose=2):
    '''
    Mesh Denoise
    ----------
    [grid_filter.10]\n
    Mesh denoising for grids, using the algorithm of Sun et al. (2007).\n
    References:\n
    Cardiff University: Filtering and Processing of Irregular Meshes with Uncertainties. [online](http://www.cs.cf.ac.uk/meshfiltering/).\n
    Stevenson, J.A., Sun, X., Mitchell, N.C. (2010): Despeckling SRTM and other topographic data with a denoising algorithm, Geomorphology, Vol.114, No.3, pp.238-252.\n
    Sun, X., Rosin, P.L., Martin, R.R., Langbein, F.C. (2007): Fast and effective feature-preserving mesh denoising. IEEE Transactions on Visualization and Computer Graphics, Vol.13, No.5, pp.925-938.\n
    Arguments
    ----------
    - INPUT [`input grid`] : Grid
    - OUTPUT [`output grid`] : Denoised Grid
    - SIGMA [`floating point number`] : Threshold. Minimum: 0.000000 Maximum: 1.000000 Default: 0.900000
    - ITER [`integer number`] : Number of Iterations for Normal Updating. Minimum: 1 Default: 5
    - VITER [`integer number`] : Number of Iterations for Vertex Updating. Minimum: 1 Default: 50
    - NB_CV [`choice`] : Common Edge Type of Face Neighbourhood. Available Choices: [0] Common Vertex [1] Common Edge Default: 0
    - ZONLY [`boolean`] : Only Z-Direction Position is Updated. Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_filter', '10', 'Mesh Denoise')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Output('OUTPUT', OUTPUT)
        Tool.Set_Option('SIGMA', SIGMA)
        Tool.Set_Option('ITER', ITER)
        Tool.Set_Option('VITER', VITER)
        Tool.Set_Option('NB_CV', NB_CV)
        Tool.Set_Option('ZONLY', ZONLY)
        return Tool.Execute(Verbose)
    return False

def run_tool_grid_filter_10(INPUT=None, OUTPUT=None, SIGMA=None, ITER=None, VITER=None, NB_CV=None, ZONLY=None, Verbose=2):
    '''
    Mesh Denoise
    ----------
    [grid_filter.10]\n
    Mesh denoising for grids, using the algorithm of Sun et al. (2007).\n
    References:\n
    Cardiff University: Filtering and Processing of Irregular Meshes with Uncertainties. [online](http://www.cs.cf.ac.uk/meshfiltering/).\n
    Stevenson, J.A., Sun, X., Mitchell, N.C. (2010): Despeckling SRTM and other topographic data with a denoising algorithm, Geomorphology, Vol.114, No.3, pp.238-252.\n
    Sun, X., Rosin, P.L., Martin, R.R., Langbein, F.C. (2007): Fast and effective feature-preserving mesh denoising. IEEE Transactions on Visualization and Computer Graphics, Vol.13, No.5, pp.925-938.\n
    Arguments
    ----------
    - INPUT [`input grid`] : Grid
    - OUTPUT [`output grid`] : Denoised Grid
    - SIGMA [`floating point number`] : Threshold. Minimum: 0.000000 Maximum: 1.000000 Default: 0.900000
    - ITER [`integer number`] : Number of Iterations for Normal Updating. Minimum: 1 Default: 5
    - VITER [`integer number`] : Number of Iterations for Vertex Updating. Minimum: 1 Default: 50
    - NB_CV [`choice`] : Common Edge Type of Face Neighbourhood. Available Choices: [0] Common Vertex [1] Common Edge Default: 0
    - ZONLY [`boolean`] : Only Z-Direction Position is Updated. Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_filter', '10', 'Mesh Denoise')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Output('OUTPUT', OUTPUT)
        Tool.Set_Option('SIGMA', SIGMA)
        Tool.Set_Option('ITER', ITER)
        Tool.Set_Option('VITER', VITER)
        Tool.Set_Option('NB_CV', NB_CV)
        Tool.Set_Option('ZONLY', ZONLY)
        return Tool.Execute(Verbose)
    return False

def Resampling_Filter(GRID=None, LOPASS=None, HIPASS=None, SCALE=None, Verbose=2):
    '''
    Resampling Filter
    ----------
    [grid_filter.11]\n
    Resampling filter for grids. Resamples in a first step the given grid to desired resampling cell size, expressed as multiple of the original cell size (scale factor). This is an up-scaling through which cell values are aggregated as cell area weighted means. Second step is the down-scaling to original cell size using spline interpolation. Specially for larger search distances this is a comparably fast alternative for simple low and high pass filter operations.\n
    Arguments
    ----------
    - GRID [`input grid`] : Grid
    - LOPASS [`output grid`] : Low Pass Filter
    - HIPASS [`output grid`] : High Pass Filter
    - SCALE [`floating point number`] : Scale Factor. Minimum: 1.000000 Default: 10.000000

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_filter', '11', 'Resampling Filter')
    if Tool.is_Okay():
        Tool.Set_Input ('GRID', GRID)
        Tool.Set_Output('LOPASS', LOPASS)
        Tool.Set_Output('HIPASS', HIPASS)
        Tool.Set_Option('SCALE', SCALE)
        return Tool.Execute(Verbose)
    return False

def run_tool_grid_filter_11(GRID=None, LOPASS=None, HIPASS=None, SCALE=None, Verbose=2):
    '''
    Resampling Filter
    ----------
    [grid_filter.11]\n
    Resampling filter for grids. Resamples in a first step the given grid to desired resampling cell size, expressed as multiple of the original cell size (scale factor). This is an up-scaling through which cell values are aggregated as cell area weighted means. Second step is the down-scaling to original cell size using spline interpolation. Specially for larger search distances this is a comparably fast alternative for simple low and high pass filter operations.\n
    Arguments
    ----------
    - GRID [`input grid`] : Grid
    - LOPASS [`output grid`] : Low Pass Filter
    - HIPASS [`output grid`] : High Pass Filter
    - SCALE [`floating point number`] : Scale Factor. Minimum: 1.000000 Default: 10.000000

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_filter', '11', 'Resampling Filter')
    if Tool.is_Okay():
        Tool.Set_Input ('GRID', GRID)
        Tool.Set_Output('LOPASS', LOPASS)
        Tool.Set_Output('HIPASS', HIPASS)
        Tool.Set_Option('SCALE', SCALE)
        return Tool.Execute(Verbose)
    return False

def Geodesic_Morphological_Reconstruction(INPUT_GRID=None, DIFFERENCE_GRID=None, OBJECT_GRID=None, SHIFT_VALUE=None, BORDER_YES_NO=None, THRESHOLD=None, Verbose=2):
    '''
    Geodesic Morphological Reconstruction
    ----------
    [grid_filter.12]\n
    Geodesic morphological reconstruction according to Vincent (1993). Here we use the algorithm on p. 194: Computing of Regional Maxima and Breadth-first Scanning.\n
    A marker is derived from the input grid by subtracting a constant shift value. Optionally, the shift value can be set to zero at the border of the grid ("Preserve 1px border Yes/No" parameter). The final result is a grid showing the difference between the input image and the morphological reconstruction of the marker under the input image. If the Option "Create a binary mask" is selected, the difference grid is thresholded with provided threshold value to create a binary image of maxima regions.\n
    Arguments
    ----------
    - INPUT_GRID [`input grid`] : Input Grid. Input for the morphological reconstruction
    - DIFFERENCE_GRID [`output grid`] : Morphological Reconstruction. Reconstruction, difference to input grid.
    - OBJECT_GRID [`output grid`] : Objects. Binary object mask
    - SHIFT_VALUE [`floating point number`] : Shift Value. Default: 5.000000
    - BORDER_YES_NO [`boolean`] : Preserve 1px border. Default: 1
    - THRESHOLD [`floating point number`] : Threshold. Minimum: 0.000000 Default: 1.000000 Threshold for object identification.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_filter', '12', 'Geodesic Morphological Reconstruction')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT_GRID', INPUT_GRID)
        Tool.Set_Output('DIFFERENCE_GRID', DIFFERENCE_GRID)
        Tool.Set_Output('OBJECT_GRID', OBJECT_GRID)
        Tool.Set_Option('SHIFT_VALUE', SHIFT_VALUE)
        Tool.Set_Option('BORDER_YES_NO', BORDER_YES_NO)
        Tool.Set_Option('THRESHOLD', THRESHOLD)
        return Tool.Execute(Verbose)
    return False

def run_tool_grid_filter_12(INPUT_GRID=None, DIFFERENCE_GRID=None, OBJECT_GRID=None, SHIFT_VALUE=None, BORDER_YES_NO=None, THRESHOLD=None, Verbose=2):
    '''
    Geodesic Morphological Reconstruction
    ----------
    [grid_filter.12]\n
    Geodesic morphological reconstruction according to Vincent (1993). Here we use the algorithm on p. 194: Computing of Regional Maxima and Breadth-first Scanning.\n
    A marker is derived from the input grid by subtracting a constant shift value. Optionally, the shift value can be set to zero at the border of the grid ("Preserve 1px border Yes/No" parameter). The final result is a grid showing the difference between the input image and the morphological reconstruction of the marker under the input image. If the Option "Create a binary mask" is selected, the difference grid is thresholded with provided threshold value to create a binary image of maxima regions.\n
    Arguments
    ----------
    - INPUT_GRID [`input grid`] : Input Grid. Input for the morphological reconstruction
    - DIFFERENCE_GRID [`output grid`] : Morphological Reconstruction. Reconstruction, difference to input grid.
    - OBJECT_GRID [`output grid`] : Objects. Binary object mask
    - SHIFT_VALUE [`floating point number`] : Shift Value. Default: 5.000000
    - BORDER_YES_NO [`boolean`] : Preserve 1px border. Default: 1
    - THRESHOLD [`floating point number`] : Threshold. Minimum: 0.000000 Default: 1.000000 Threshold for object identification.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_filter', '12', 'Geodesic Morphological Reconstruction')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT_GRID', INPUT_GRID)
        Tool.Set_Output('DIFFERENCE_GRID', DIFFERENCE_GRID)
        Tool.Set_Output('OBJECT_GRID', OBJECT_GRID)
        Tool.Set_Option('SHIFT_VALUE', SHIFT_VALUE)
        Tool.Set_Option('BORDER_YES_NO', BORDER_YES_NO)
        Tool.Set_Option('THRESHOLD', THRESHOLD)
        return Tool.Execute(Verbose)
    return False

def Binary_ErosionReconstruction(INPUT_GRID=None, OUTPUT_GRID=None, RADIUS=None, Verbose=2):
    '''
    Binary Erosion-Reconstruction
    ----------
    [grid_filter.13]\n
    Common binary opening does not guarantee, that foreground regions which outlast the erosion step are reconstructed to their original shape in the dilation step. Depending on the application, that might be considered as a deficiency. Therefore this tool provides a combination of erosion with the binary Geodesic Morphological Reconstruction, see Vincent (1993). Here we use the algorithm on p. 194: Breadth-first Scanning.\n
    The marker is defined as the eroded input grid, whereas the mask is just the input grid itself. The output grid is the reconstruction of the marker under the mask.\n
    Arguments
    ----------
    - INPUT_GRID [`input grid`] : Input Grid. Grid to be filtered
    - OUTPUT_GRID [`output grid`] : Output Grid. Reconstruction result
    - RADIUS [`integer number`] : Filter Size (Radius). Default: 3 Filter size (radius in grid cells)

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_filter', '13', 'Binary Erosion-Reconstruction')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT_GRID', INPUT_GRID)
        Tool.Set_Output('OUTPUT_GRID', OUTPUT_GRID)
        Tool.Set_Option('RADIUS', RADIUS)
        return Tool.Execute(Verbose)
    return False

def run_tool_grid_filter_13(INPUT_GRID=None, OUTPUT_GRID=None, RADIUS=None, Verbose=2):
    '''
    Binary Erosion-Reconstruction
    ----------
    [grid_filter.13]\n
    Common binary opening does not guarantee, that foreground regions which outlast the erosion step are reconstructed to their original shape in the dilation step. Depending on the application, that might be considered as a deficiency. Therefore this tool provides a combination of erosion with the binary Geodesic Morphological Reconstruction, see Vincent (1993). Here we use the algorithm on p. 194: Breadth-first Scanning.\n
    The marker is defined as the eroded input grid, whereas the mask is just the input grid itself. The output grid is the reconstruction of the marker under the mask.\n
    Arguments
    ----------
    - INPUT_GRID [`input grid`] : Input Grid. Grid to be filtered
    - OUTPUT_GRID [`output grid`] : Output Grid. Reconstruction result
    - RADIUS [`integer number`] : Filter Size (Radius). Default: 3 Filter size (radius in grid cells)

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_filter', '13', 'Binary Erosion-Reconstruction')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT_GRID', INPUT_GRID)
        Tool.Set_Output('OUTPUT_GRID', OUTPUT_GRID)
        Tool.Set_Option('RADIUS', RADIUS)
        return Tool.Execute(Verbose)
    return False

def Connectivity_Analysis(INPUT_GRID=None, FILTERED_MASK=None, SYMBOLIC_IMAGE=None, OUTLINES=None, FILTER=None, SIZE=None, BORDER_PIXEL_CENTERS=None, REMOVE_MARGINAL_REGIONS=None, Verbose=2):
    '''
    Connectivity Analysis
    ----------
    [grid_filter.14]\n
    Connectivity analysis of a binary input image according to\n
    Burger, W., Burge, M.: Digitale Bildverarbeitung. Springer Verlag 2006, p.208.\n
    Output consists in a symbolic image of the connected foreground regions and a shape of the borders of the foreground regions (outer and inner borders). The shape may contain alternatively the centers or the corners of the border pixels. Optionally, the regions which have contact with the image borders can be removed together with their border shapes.\n
    In addition, an optional morphological filter (erosion-binary reconstruction) can be applied to the input image first.\n
    Arguments
    ----------
    - INPUT_GRID [`input grid`] : Input Binary Grid. Binary input image for the connectivity analysis
    - FILTERED_MASK [`output grid`] : Filtered Image. Morphologically filtered binary mask
    - SYMBOLIC_IMAGE [`output grid`] : Symbolic Image. The final symbolic image
    - OUTLINES [`output shapes`] : Outlines. Polygon outlines of object regions
    - FILTER [`boolean`] : Apply Filter?. Default: 1 Apply a filter (erosion - binary reconstruction) to the input image 
    - SIZE [`integer number`] : Filter Size (Radius). Default: 3 Filter size (radius in grid cells)
    - BORDER_PIXEL_CENTERS [`boolean`] : Pixel Centers?. Default: 0 Should the output shapes contain the centers of the border pixels instead of the corners?
    - REMOVE_MARGINAL_REGIONS [`boolean`] : Remove Border Regions?. Default: 0 Remove regions which have contact with (are adjacent to) the image borders?

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_filter', '14', 'Connectivity Analysis')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT_GRID', INPUT_GRID)
        Tool.Set_Output('FILTERED_MASK', FILTERED_MASK)
        Tool.Set_Output('SYMBOLIC_IMAGE', SYMBOLIC_IMAGE)
        Tool.Set_Output('OUTLINES', OUTLINES)
        Tool.Set_Option('FILTER', FILTER)
        Tool.Set_Option('SIZE', SIZE)
        Tool.Set_Option('BORDER_PIXEL_CENTERS', BORDER_PIXEL_CENTERS)
        Tool.Set_Option('REMOVE_MARGINAL_REGIONS', REMOVE_MARGINAL_REGIONS)
        return Tool.Execute(Verbose)
    return False

def run_tool_grid_filter_14(INPUT_GRID=None, FILTERED_MASK=None, SYMBOLIC_IMAGE=None, OUTLINES=None, FILTER=None, SIZE=None, BORDER_PIXEL_CENTERS=None, REMOVE_MARGINAL_REGIONS=None, Verbose=2):
    '''
    Connectivity Analysis
    ----------
    [grid_filter.14]\n
    Connectivity analysis of a binary input image according to\n
    Burger, W., Burge, M.: Digitale Bildverarbeitung. Springer Verlag 2006, p.208.\n
    Output consists in a symbolic image of the connected foreground regions and a shape of the borders of the foreground regions (outer and inner borders). The shape may contain alternatively the centers or the corners of the border pixels. Optionally, the regions which have contact with the image borders can be removed together with their border shapes.\n
    In addition, an optional morphological filter (erosion-binary reconstruction) can be applied to the input image first.\n
    Arguments
    ----------
    - INPUT_GRID [`input grid`] : Input Binary Grid. Binary input image for the connectivity analysis
    - FILTERED_MASK [`output grid`] : Filtered Image. Morphologically filtered binary mask
    - SYMBOLIC_IMAGE [`output grid`] : Symbolic Image. The final symbolic image
    - OUTLINES [`output shapes`] : Outlines. Polygon outlines of object regions
    - FILTER [`boolean`] : Apply Filter?. Default: 1 Apply a filter (erosion - binary reconstruction) to the input image 
    - SIZE [`integer number`] : Filter Size (Radius). Default: 3 Filter size (radius in grid cells)
    - BORDER_PIXEL_CENTERS [`boolean`] : Pixel Centers?. Default: 0 Should the output shapes contain the centers of the border pixels instead of the corners?
    - REMOVE_MARGINAL_REGIONS [`boolean`] : Remove Border Regions?. Default: 0 Remove regions which have contact with (are adjacent to) the image borders?

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_filter', '14', 'Connectivity Analysis')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT_GRID', INPUT_GRID)
        Tool.Set_Output('FILTERED_MASK', FILTERED_MASK)
        Tool.Set_Output('SYMBOLIC_IMAGE', SYMBOLIC_IMAGE)
        Tool.Set_Output('OUTLINES', OUTLINES)
        Tool.Set_Option('FILTER', FILTER)
        Tool.Set_Option('SIZE', SIZE)
        Tool.Set_Option('BORDER_PIXEL_CENTERS', BORDER_PIXEL_CENTERS)
        Tool.Set_Option('REMOVE_MARGINAL_REGIONS', REMOVE_MARGINAL_REGIONS)
        return Tool.Execute(Verbose)
    return False

def Sieve_Classes(INPUT=None, OUTPUT=None, MODE=None, THRESHOLD=None, ALL=None, CLASS=None, Verbose=2):
    '''
    Sieve Classes
    ----------
    [grid_filter.15]\n
    The 'Sieve Classes' tool counts the number of adjacent cells sharing the same value (the class identifier). Areas that are formed by less cells than specified by the threshold will be removed (sieved), i.e. they are set to no-data.\n
    Arguments
    ----------
    - INPUT [`input grid`] : Classes
    - OUTPUT [`output grid`] : Sieved Classes
    - MODE [`choice`] : Neighbourhood. Available Choices: [0] Neumann [1] Moore Default: 0 Neumann: the four horizontally and vertically neighboured cells; Moore: all eight adjacent cells
    - THRESHOLD [`integer number`] : Minimum Threshold. Minimum: 2 Default: 4 Minimum number of cells in a group of adjacent cells.
    - ALL [`choice`] : Class Selection. Available Choices: [0] single class [1] all classes Default: 1
    - CLASS [`floating point number`] : Class Identifier. Default: 1.000000

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_filter', '15', 'Sieve Classes')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Output('OUTPUT', OUTPUT)
        Tool.Set_Option('MODE', MODE)
        Tool.Set_Option('THRESHOLD', THRESHOLD)
        Tool.Set_Option('ALL', ALL)
        Tool.Set_Option('CLASS', CLASS)
        return Tool.Execute(Verbose)
    return False

def run_tool_grid_filter_15(INPUT=None, OUTPUT=None, MODE=None, THRESHOLD=None, ALL=None, CLASS=None, Verbose=2):
    '''
    Sieve Classes
    ----------
    [grid_filter.15]\n
    The 'Sieve Classes' tool counts the number of adjacent cells sharing the same value (the class identifier). Areas that are formed by less cells than specified by the threshold will be removed (sieved), i.e. they are set to no-data.\n
    Arguments
    ----------
    - INPUT [`input grid`] : Classes
    - OUTPUT [`output grid`] : Sieved Classes
    - MODE [`choice`] : Neighbourhood. Available Choices: [0] Neumann [1] Moore Default: 0 Neumann: the four horizontally and vertically neighboured cells; Moore: all eight adjacent cells
    - THRESHOLD [`integer number`] : Minimum Threshold. Minimum: 2 Default: 4 Minimum number of cells in a group of adjacent cells.
    - ALL [`choice`] : Class Selection. Available Choices: [0] single class [1] all classes Default: 1
    - CLASS [`floating point number`] : Class Identifier. Default: 1.000000

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_filter', '15', 'Sieve Classes')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Output('OUTPUT', OUTPUT)
        Tool.Set_Option('MODE', MODE)
        Tool.Set_Option('THRESHOLD', THRESHOLD)
        Tool.Set_Option('ALL', ALL)
        Tool.Set_Option('CLASS', CLASS)
        return Tool.Execute(Verbose)
    return False

def Wombling_Edge_Detection(FEATURE=None, EDGE_POINTS=None, EDGE_LINES=None, GRADIENTS=None, TMAGNITUDE=None, TDIRECTION=None, TNEIGHBOUR=None, ALIGNMENT=None, NEIGHBOUR=None, GRADIENTS_OUT=None, Verbose=2):
    '''
    Wombling (Edge Detection)
    ----------
    [grid_filter.16]\n
    Continuous Wombling for edge detection. Uses magnitude of gradient to detect edges between adjacent cells. Edge segments connect such edges, when the difference of their gradient directions is below given threshold.\n
    Arguments
    ----------
    - FEATURE [`input grid`] : Feature
    - EDGE_POINTS [`output shapes`] : Edge Points
    - EDGE_LINES [`output shapes`] : Edge Segments
    - GRADIENTS [`output grid list`] : Gradients
    - TMAGNITUDE [`floating point number`] : Minimum Magnitude. Minimum: 0.000000 Maximum: 100.000000 Default: 90.000000 Minimum magnitude as percentile.
    - TDIRECTION [`floating point number`] : Maximum Angle. Minimum: 0.000000 Maximum: 180.000000 Default: 30.000000 Maximum angular difference as degree between adjacent segment points.
    - TNEIGHBOUR [`integer number`] : Minimum Neighbours. Minimum: 0 Default: 1 Minimum number of neighbouring potential edge cells with similar direction.
    - ALIGNMENT [`choice`] : Alignment. Available Choices: [0] between cells [1] on cell Default: 1
    - NEIGHBOUR [`choice`] : Edge Connectivity. Available Choices: [0] Rooke's case [1] Queen's case Default: 1
    - GRADIENTS_OUT [`boolean`] : Output of Gradients. Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_filter', '16', 'Wombling (Edge Detection)')
    if Tool.is_Okay():
        Tool.Set_Input ('FEATURE', FEATURE)
        Tool.Set_Output('EDGE_POINTS', EDGE_POINTS)
        Tool.Set_Output('EDGE_LINES', EDGE_LINES)
        Tool.Set_Output('GRADIENTS', GRADIENTS)
        Tool.Set_Option('TMAGNITUDE', TMAGNITUDE)
        Tool.Set_Option('TDIRECTION', TDIRECTION)
        Tool.Set_Option('TNEIGHBOUR', TNEIGHBOUR)
        Tool.Set_Option('ALIGNMENT', ALIGNMENT)
        Tool.Set_Option('NEIGHBOUR', NEIGHBOUR)
        Tool.Set_Option('GRADIENTS_OUT', GRADIENTS_OUT)
        return Tool.Execute(Verbose)
    return False

def run_tool_grid_filter_16(FEATURE=None, EDGE_POINTS=None, EDGE_LINES=None, GRADIENTS=None, TMAGNITUDE=None, TDIRECTION=None, TNEIGHBOUR=None, ALIGNMENT=None, NEIGHBOUR=None, GRADIENTS_OUT=None, Verbose=2):
    '''
    Wombling (Edge Detection)
    ----------
    [grid_filter.16]\n
    Continuous Wombling for edge detection. Uses magnitude of gradient to detect edges between adjacent cells. Edge segments connect such edges, when the difference of their gradient directions is below given threshold.\n
    Arguments
    ----------
    - FEATURE [`input grid`] : Feature
    - EDGE_POINTS [`output shapes`] : Edge Points
    - EDGE_LINES [`output shapes`] : Edge Segments
    - GRADIENTS [`output grid list`] : Gradients
    - TMAGNITUDE [`floating point number`] : Minimum Magnitude. Minimum: 0.000000 Maximum: 100.000000 Default: 90.000000 Minimum magnitude as percentile.
    - TDIRECTION [`floating point number`] : Maximum Angle. Minimum: 0.000000 Maximum: 180.000000 Default: 30.000000 Maximum angular difference as degree between adjacent segment points.
    - TNEIGHBOUR [`integer number`] : Minimum Neighbours. Minimum: 0 Default: 1 Minimum number of neighbouring potential edge cells with similar direction.
    - ALIGNMENT [`choice`] : Alignment. Available Choices: [0] between cells [1] on cell Default: 1
    - NEIGHBOUR [`choice`] : Edge Connectivity. Available Choices: [0] Rooke's case [1] Queen's case Default: 1
    - GRADIENTS_OUT [`boolean`] : Output of Gradients. Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_filter', '16', 'Wombling (Edge Detection)')
    if Tool.is_Okay():
        Tool.Set_Input ('FEATURE', FEATURE)
        Tool.Set_Output('EDGE_POINTS', EDGE_POINTS)
        Tool.Set_Output('EDGE_LINES', EDGE_LINES)
        Tool.Set_Output('GRADIENTS', GRADIENTS)
        Tool.Set_Option('TMAGNITUDE', TMAGNITUDE)
        Tool.Set_Option('TDIRECTION', TDIRECTION)
        Tool.Set_Option('TNEIGHBOUR', TNEIGHBOUR)
        Tool.Set_Option('ALIGNMENT', ALIGNMENT)
        Tool.Set_Option('NEIGHBOUR', NEIGHBOUR)
        Tool.Set_Option('GRADIENTS_OUT', GRADIENTS_OUT)
        return Tool.Execute(Verbose)
    return False

def Wombling_for_Multiple_Features_Edge_Detection(FEATURES=None, EDGE_CELLS=None, OUTPUT=None, TMAGNITUDE=None, TDIRECTION=None, TNEIGHBOUR=None, ALIGNMENT=None, NEIGHBOUR=None, OUTPUT_ADD=None, ZERO_AS_NODATA=None, Verbose=2):
    '''
    Wombling for Multiple Features (Edge Detection)
    ----------
    [grid_filter.17]\n
    Continuous Wombling for edge detection. Uses magnitude of gradient to detect edges between adjacent cells. Edge segments connect such edges, when the difference of their gradient directions is below given threshold.\n
    Arguments
    ----------
    - FEATURES [`input grid list`] : Features
    - EDGE_CELLS [`output grid list`] : Edges
    - OUTPUT [`output grid list`] : Output
    - TMAGNITUDE [`floating point number`] : Minimum Magnitude. Minimum: 0.000000 Maximum: 100.000000 Default: 90.000000 Minimum magnitude as percentile.
    - TDIRECTION [`floating point number`] : Maximum Angle. Minimum: 0.000000 Maximum: 180.000000 Default: 30.000000 Maximum angular difference as degree between adjacent segment points.
    - TNEIGHBOUR [`integer number`] : Minimum Neighbours. Minimum: 0 Default: 1 Minimum number of neighbouring potential edge cells with similar direction.
    - ALIGNMENT [`choice`] : Alignment. Available Choices: [0] between cells [1] on cell Default: 1
    - NEIGHBOUR [`choice`] : Edge Connectivity. Available Choices: [0] Rooke's case [1] Queen's case Default: 1
    - OUTPUT_ADD [`choice`] : Additional Output. Available Choices: [0] none [1] gradients [2] edge cells Default: 0
    - ZERO_AS_NODATA [`boolean`] : Zero as No-Data. Default: 1

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_filter', '17', 'Wombling for Multiple Features (Edge Detection)')
    if Tool.is_Okay():
        Tool.Set_Input ('FEATURES', FEATURES)
        Tool.Set_Output('EDGE_CELLS', EDGE_CELLS)
        Tool.Set_Output('OUTPUT', OUTPUT)
        Tool.Set_Option('TMAGNITUDE', TMAGNITUDE)
        Tool.Set_Option('TDIRECTION', TDIRECTION)
        Tool.Set_Option('TNEIGHBOUR', TNEIGHBOUR)
        Tool.Set_Option('ALIGNMENT', ALIGNMENT)
        Tool.Set_Option('NEIGHBOUR', NEIGHBOUR)
        Tool.Set_Option('OUTPUT_ADD', OUTPUT_ADD)
        Tool.Set_Option('ZERO_AS_NODATA', ZERO_AS_NODATA)
        return Tool.Execute(Verbose)
    return False

def run_tool_grid_filter_17(FEATURES=None, EDGE_CELLS=None, OUTPUT=None, TMAGNITUDE=None, TDIRECTION=None, TNEIGHBOUR=None, ALIGNMENT=None, NEIGHBOUR=None, OUTPUT_ADD=None, ZERO_AS_NODATA=None, Verbose=2):
    '''
    Wombling for Multiple Features (Edge Detection)
    ----------
    [grid_filter.17]\n
    Continuous Wombling for edge detection. Uses magnitude of gradient to detect edges between adjacent cells. Edge segments connect such edges, when the difference of their gradient directions is below given threshold.\n
    Arguments
    ----------
    - FEATURES [`input grid list`] : Features
    - EDGE_CELLS [`output grid list`] : Edges
    - OUTPUT [`output grid list`] : Output
    - TMAGNITUDE [`floating point number`] : Minimum Magnitude. Minimum: 0.000000 Maximum: 100.000000 Default: 90.000000 Minimum magnitude as percentile.
    - TDIRECTION [`floating point number`] : Maximum Angle. Minimum: 0.000000 Maximum: 180.000000 Default: 30.000000 Maximum angular difference as degree between adjacent segment points.
    - TNEIGHBOUR [`integer number`] : Minimum Neighbours. Minimum: 0 Default: 1 Minimum number of neighbouring potential edge cells with similar direction.
    - ALIGNMENT [`choice`] : Alignment. Available Choices: [0] between cells [1] on cell Default: 1
    - NEIGHBOUR [`choice`] : Edge Connectivity. Available Choices: [0] Rooke's case [1] Queen's case Default: 1
    - OUTPUT_ADD [`choice`] : Additional Output. Available Choices: [0] none [1] gradients [2] edge cells Default: 0
    - ZERO_AS_NODATA [`boolean`] : Zero as No-Data. Default: 1

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_filter', '17', 'Wombling for Multiple Features (Edge Detection)')
    if Tool.is_Okay():
        Tool.Set_Input ('FEATURES', FEATURES)
        Tool.Set_Output('EDGE_CELLS', EDGE_CELLS)
        Tool.Set_Output('OUTPUT', OUTPUT)
        Tool.Set_Option('TMAGNITUDE', TMAGNITUDE)
        Tool.Set_Option('TDIRECTION', TDIRECTION)
        Tool.Set_Option('TNEIGHBOUR', TNEIGHBOUR)
        Tool.Set_Option('ALIGNMENT', ALIGNMENT)
        Tool.Set_Option('NEIGHBOUR', NEIGHBOUR)
        Tool.Set_Option('OUTPUT_ADD', OUTPUT_ADD)
        Tool.Set_Option('ZERO_AS_NODATA', ZERO_AS_NODATA)
        return Tool.Execute(Verbose)
    return False

def Simple_Filter_Restricted_to_Polygons(INPUT=None, SHAPES=None, RESULT=None, METHOD=None, SKIP_OUTSIDE=None, KERNEL_TYPE=None, KERNEL_RADIUS=None, Verbose=2):
    '''
    Simple Filter (Restricted to Polygons)
    ----------
    [grid_filter.18]\n
    Simple standard filters for grids, evaluation within polygons.\n
    Arguments
    ----------
    - INPUT [`input grid`] : Grid
    - SHAPES [`input shapes`] : Polygons. The filter will only operate on cells that belong to the same polygon or to none if skip outside option is off.
    - RESULT [`output grid`] : Filtered Grid
    - METHOD [`choice`] : Filter. Available Choices: [0] Smooth [1] Sharpen [2] Edge Default: 0 Choose the filter method.
    - SKIP_OUTSIDE [`boolean`] : Skip Outside Cells. Default: 0 Process only cells that are covered by a polygon.
    - KERNEL_TYPE [`choice`] : Kernel Type. Available Choices: [0] Square [1] Circle Default: 1 The kernel's shape.
    - KERNEL_RADIUS [`integer number`] : Radius. Minimum: 0 Default: 2 cells

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_filter', '18', 'Simple Filter (Restricted to Polygons)')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Input ('SHAPES', SHAPES)
        Tool.Set_Output('RESULT', RESULT)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('SKIP_OUTSIDE', SKIP_OUTSIDE)
        Tool.Set_Option('KERNEL_TYPE', KERNEL_TYPE)
        Tool.Set_Option('KERNEL_RADIUS', KERNEL_RADIUS)
        return Tool.Execute(Verbose)
    return False

def run_tool_grid_filter_18(INPUT=None, SHAPES=None, RESULT=None, METHOD=None, SKIP_OUTSIDE=None, KERNEL_TYPE=None, KERNEL_RADIUS=None, Verbose=2):
    '''
    Simple Filter (Restricted to Polygons)
    ----------
    [grid_filter.18]\n
    Simple standard filters for grids, evaluation within polygons.\n
    Arguments
    ----------
    - INPUT [`input grid`] : Grid
    - SHAPES [`input shapes`] : Polygons. The filter will only operate on cells that belong to the same polygon or to none if skip outside option is off.
    - RESULT [`output grid`] : Filtered Grid
    - METHOD [`choice`] : Filter. Available Choices: [0] Smooth [1] Sharpen [2] Edge Default: 0 Choose the filter method.
    - SKIP_OUTSIDE [`boolean`] : Skip Outside Cells. Default: 0 Process only cells that are covered by a polygon.
    - KERNEL_TYPE [`choice`] : Kernel Type. Available Choices: [0] Square [1] Circle Default: 1 The kernel's shape.
    - KERNEL_RADIUS [`integer number`] : Radius. Minimum: 0 Default: 2 cells

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_filter', '18', 'Simple Filter (Restricted to Polygons)')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Input ('SHAPES', SHAPES)
        Tool.Set_Output('RESULT', RESULT)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('SKIP_OUTSIDE', SKIP_OUTSIDE)
        Tool.Set_Option('KERNEL_TYPE', KERNEL_TYPE)
        Tool.Set_Option('KERNEL_RADIUS', KERNEL_RADIUS)
        return Tool.Execute(Verbose)
    return False

def Sharpening_Filter(INPUT=None, RESULT=None, METHOD=None, KERNEL_RADIUS=None, SIGMA=None, Verbose=2):
    '''
    Sharpening Filter
    ----------
    [grid_filter.19]\n
    This Sharpening filter uses a Laplacian filter to detect the high frequencies in the supplied grid and adds it to the original values.\n
    Arguments
    ----------
    - INPUT [`input grid`] : Grid
    - RESULT [`output grid`] : Filtered Grid
    - METHOD [`choice`] : Method. Available Choices: [0] standard kernel 1 [1] standard kernel 2 [2] Standard kernel 3 [3] user defined kernel Default: 3
    - KERNEL_RADIUS [`integer number`] : Kernel Radius. Minimum: 1 Default: 2
    - SIGMA [`floating point number`] : Standard Deviation. Minimum: 1.000000 Default: 50.000000 The standard deviation as percentage of the kernel radius.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_filter', '19', 'Sharpening Filter')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Output('RESULT', RESULT)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('KERNEL_RADIUS', KERNEL_RADIUS)
        Tool.Set_Option('SIGMA', SIGMA)
        return Tool.Execute(Verbose)
    return False

def run_tool_grid_filter_19(INPUT=None, RESULT=None, METHOD=None, KERNEL_RADIUS=None, SIGMA=None, Verbose=2):
    '''
    Sharpening Filter
    ----------
    [grid_filter.19]\n
    This Sharpening filter uses a Laplacian filter to detect the high frequencies in the supplied grid and adds it to the original values.\n
    Arguments
    ----------
    - INPUT [`input grid`] : Grid
    - RESULT [`output grid`] : Filtered Grid
    - METHOD [`choice`] : Method. Available Choices: [0] standard kernel 1 [1] standard kernel 2 [2] Standard kernel 3 [3] user defined kernel Default: 3
    - KERNEL_RADIUS [`integer number`] : Kernel Radius. Minimum: 1 Default: 2
    - SIGMA [`floating point number`] : Standard Deviation. Minimum: 1.000000 Default: 50.000000 The standard deviation as percentage of the kernel radius.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_filter', '19', 'Sharpening Filter')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Output('RESULT', RESULT)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('KERNEL_RADIUS', KERNEL_RADIUS)
        Tool.Set_Option('SIGMA', SIGMA)
        return Tool.Execute(Verbose)
    return False

def Notch_Filter_for_Grids(INPUT_GRID=None, LOWPASS_UPPER=None, HIPASS_LOWER=None, FILTERED_GRID=None, GRID_SYSTEM=None, HIGH_PASS=None, LOW_PASS=None, Verbose=2):
    '''
    Notch Filter for Grids
    ----------
    [grid_filter.notch_filter]\n
    This tool chain uses the Grid > Filter > Resampling Filter tool to simulate a wide notch filter applied between the two resampling cell sizes chosen.\n
    Arguments
    ----------
    - INPUT_GRID [`input grid`] : Input Grid
    - LOWPASS_UPPER [`output grid`] : Lowpass Upper
    - HIPASS_LOWER [`output grid`] : Highpass Lower
    - FILTERED_GRID [`output grid`] : Notch
    - GRID_SYSTEM [`grid system`] : Grid System
    - HIGH_PASS [`floating point number`] : Upper search distance . Default: 75.000000
    - LOW_PASS [`floating point number`] : Lower search distance. Default: 25.000000

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_filter', 'notch_filter', 'Notch Filter for Grids')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT_GRID', INPUT_GRID)
        Tool.Set_Output('LOWPASS_UPPER', LOWPASS_UPPER)
        Tool.Set_Output('HIPASS_LOWER', HIPASS_LOWER)
        Tool.Set_Output('FILTERED_GRID', FILTERED_GRID)
        Tool.Set_Option('GRID_SYSTEM', GRID_SYSTEM)
        Tool.Set_Option('HIGH_PASS', HIGH_PASS)
        Tool.Set_Option('LOW_PASS', LOW_PASS)
        return Tool.Execute(Verbose)
    return False

def run_tool_grid_filter_notch_filter(INPUT_GRID=None, LOWPASS_UPPER=None, HIPASS_LOWER=None, FILTERED_GRID=None, GRID_SYSTEM=None, HIGH_PASS=None, LOW_PASS=None, Verbose=2):
    '''
    Notch Filter for Grids
    ----------
    [grid_filter.notch_filter]\n
    This tool chain uses the Grid > Filter > Resampling Filter tool to simulate a wide notch filter applied between the two resampling cell sizes chosen.\n
    Arguments
    ----------
    - INPUT_GRID [`input grid`] : Input Grid
    - LOWPASS_UPPER [`output grid`] : Lowpass Upper
    - HIPASS_LOWER [`output grid`] : Highpass Lower
    - FILTERED_GRID [`output grid`] : Notch
    - GRID_SYSTEM [`grid system`] : Grid System
    - HIGH_PASS [`floating point number`] : Upper search distance . Default: 75.000000
    - LOW_PASS [`floating point number`] : Lower search distance. Default: 25.000000

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_filter', 'notch_filter', 'Notch Filter for Grids')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT_GRID', INPUT_GRID)
        Tool.Set_Output('LOWPASS_UPPER', LOWPASS_UPPER)
        Tool.Set_Output('HIPASS_LOWER', HIPASS_LOWER)
        Tool.Set_Output('FILTERED_GRID', FILTERED_GRID)
        Tool.Set_Option('GRID_SYSTEM', GRID_SYSTEM)
        Tool.Set_Option('HIGH_PASS', HIGH_PASS)
        Tool.Set_Option('LOW_PASS', LOW_PASS)
        return Tool.Execute(Verbose)
    return False

def Sieve_and_Clump(CLASSES=None, FILTERED=None, GRID_SYSTEM=None, MODE=None, SIEVE=None, EXPAND=None, Verbose=2):
    '''
    Sieve and Clump
    ----------
    [grid_filter.sieve_and_clump]\n
    Combines the tools 'Sieve Classes' to remove small patches and 'Shrink and Expand' to close the gaps created by sieving classes.\n
    Arguments
    ----------
    - CLASSES [`input grid`] : Classes
    - FILTERED [`output grid`] : Sieve and Clump
    - GRID_SYSTEM [`grid system`] : Grid System
    - MODE [`choice`] : Filter. Available Choices: [0] Neumann [1] Moore Default: 0
    - SIEVE [`integer number`] : Sieving Threshold. Default: 2
    - EXPAND [`integer number`] : Maximum Expansion Distance. Default: 4

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_filter', 'sieve_and_clump', 'Sieve and Clump')
    if Tool.is_Okay():
        Tool.Set_Input ('CLASSES', CLASSES)
        Tool.Set_Output('FILTERED', FILTERED)
        Tool.Set_Option('GRID_SYSTEM', GRID_SYSTEM)
        Tool.Set_Option('MODE', MODE)
        Tool.Set_Option('SIEVE', SIEVE)
        Tool.Set_Option('EXPAND', EXPAND)
        return Tool.Execute(Verbose)
    return False

def run_tool_grid_filter_sieve_and_clump(CLASSES=None, FILTERED=None, GRID_SYSTEM=None, MODE=None, SIEVE=None, EXPAND=None, Verbose=2):
    '''
    Sieve and Clump
    ----------
    [grid_filter.sieve_and_clump]\n
    Combines the tools 'Sieve Classes' to remove small patches and 'Shrink and Expand' to close the gaps created by sieving classes.\n
    Arguments
    ----------
    - CLASSES [`input grid`] : Classes
    - FILTERED [`output grid`] : Sieve and Clump
    - GRID_SYSTEM [`grid system`] : Grid System
    - MODE [`choice`] : Filter. Available Choices: [0] Neumann [1] Moore Default: 0
    - SIEVE [`integer number`] : Sieving Threshold. Default: 2
    - EXPAND [`integer number`] : Maximum Expansion Distance. Default: 4

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_filter', 'sieve_and_clump', 'Sieve and Clump')
    if Tool.is_Okay():
        Tool.Set_Input ('CLASSES', CLASSES)
        Tool.Set_Output('FILTERED', FILTERED)
        Tool.Set_Option('GRID_SYSTEM', GRID_SYSTEM)
        Tool.Set_Option('MODE', MODE)
        Tool.Set_Option('SIEVE', SIEVE)
        Tool.Set_Option('EXPAND', EXPAND)
        return Tool.Execute(Verbose)
    return False

def Simple_Filter_for_Multiple_Grids(GRIDS=None, FILTERED=None, METHOD=None, KERNEL_TYPE=None, KERNEL_RADIUS=None, Verbose=2):
    '''
    Simple Filter for Multiple Grids
    ----------
    [grid_filter.simple_filter_bulk]\n
    Apply a simple filter simultaneously to multiple grids.\n
    Demonstrates the 'For-Each tool' chain option which easily allows\n
    to apply a single input grid tool to multiple input grids.\n
    \n
    Arguments
    ----------
    - GRIDS [`input grid list`] : Grids
    - FILTERED [`output grid list`] : Filtered Grids
    - METHOD [`choice`] : Filter. Available Choices: [0] Smooth [1] Sharpen [2] Edge Default: 0
    - KERNEL_TYPE [`choice`] : Kernel Type. Available Choices: [0] Square [1] Circle Default: 0
    - KERNEL_RADIUS [`integer number`] : Kernel Radius. Default: 2

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_filter', 'simple_filter_bulk', 'Simple Filter for Multiple Grids')
    if Tool.is_Okay():
        Tool.Set_Input ('GRIDS', GRIDS)
        Tool.Set_Output('FILTERED', FILTERED)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('KERNEL_TYPE', KERNEL_TYPE)
        Tool.Set_Option('KERNEL_RADIUS', KERNEL_RADIUS)
        return Tool.Execute(Verbose)
    return False

def run_tool_grid_filter_simple_filter_bulk(GRIDS=None, FILTERED=None, METHOD=None, KERNEL_TYPE=None, KERNEL_RADIUS=None, Verbose=2):
    '''
    Simple Filter for Multiple Grids
    ----------
    [grid_filter.simple_filter_bulk]\n
    Apply a simple filter simultaneously to multiple grids.\n
    Demonstrates the 'For-Each tool' chain option which easily allows\n
    to apply a single input grid tool to multiple input grids.\n
    \n
    Arguments
    ----------
    - GRIDS [`input grid list`] : Grids
    - FILTERED [`output grid list`] : Filtered Grids
    - METHOD [`choice`] : Filter. Available Choices: [0] Smooth [1] Sharpen [2] Edge Default: 0
    - KERNEL_TYPE [`choice`] : Kernel Type. Available Choices: [0] Square [1] Circle Default: 0
    - KERNEL_RADIUS [`integer number`] : Kernel Radius. Default: 2

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_filter', 'simple_filter_bulk', 'Simple Filter for Multiple Grids')
    if Tool.is_Okay():
        Tool.Set_Input ('GRIDS', GRIDS)
        Tool.Set_Output('FILTERED', FILTERED)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('KERNEL_TYPE', KERNEL_TYPE)
        Tool.Set_Option('KERNEL_RADIUS', KERNEL_RADIUS)
        return Tool.Execute(Verbose)
    return False

