#! /usr/bin/env python

'''
Python Interface to SAGA Tools Library
----------
- Category : TIN
- Name     : Tools
- ID       : tin_tools

Description
----------
Tools for Triangulated Irregular Network (TIN) processing.
'''

from PySAGA.helper import Tool_Wrapper

def Grid_to_TIN(GRID=None, VALUES=None, TIN=None, Verbose=2):
    '''
    Grid to TIN
    ----------
    [tin_tools.0]\n
    Creates a TIN from grid points. No data values will be ignored.\n
    Arguments
    ----------
    - GRID [`input grid`] : Grid
    - VALUES [`optional input grid list`] : Values
    - TIN [`output TIN`] : TIN

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('tin_tools', '0', 'Grid to TIN')
    if Tool.is_Okay():
        Tool.Set_Input ('GRID', GRID)
        Tool.Set_Input ('VALUES', VALUES)
        Tool.Set_Output('TIN', TIN)
        return Tool.Execute(Verbose)
    return False

def run_tool_tin_tools_0(GRID=None, VALUES=None, TIN=None, Verbose=2):
    '''
    Grid to TIN
    ----------
    [tin_tools.0]\n
    Creates a TIN from grid points. No data values will be ignored.\n
    Arguments
    ----------
    - GRID [`input grid`] : Grid
    - VALUES [`optional input grid list`] : Values
    - TIN [`output TIN`] : TIN

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('tin_tools', '0', 'Grid to TIN')
    if Tool.is_Okay():
        Tool.Set_Input ('GRID', GRID)
        Tool.Set_Input ('VALUES', VALUES)
        Tool.Set_Output('TIN', TIN)
        return Tool.Execute(Verbose)
    return False

def Grid_to_TIN_Surface_Specific_Points(GRID=None, VALUES=None, TIN=None, METHOD=None, HIGH=None, FLOW=None, PEUCKER=None, Verbose=2):
    '''
    Grid to TIN (Surface Specific Points)
    ----------
    [tin_tools.1]\n
    Creates a TIN by identifying (surface) specific points of a grid.\n
    Arguments
    ----------
    - GRID [`input grid`] : Grid
    - VALUES [`optional input grid list`] : Values
    - TIN [`output TIN`] : TIN
    - METHOD [`choice`] : Method. Available Choices: [0] Mark Highest Neighbour [1] Opposite Neighbours [2] Flow Direction [3] Flow Direction (up and down) [4] Peucker & Douglas Default: 1 The method used to identify surface specific points.
    - HIGH [`integer number`] : Mark Highest Neighbour. Minimum: 1 Maximum: 4 Default: 4
    - FLOW [`value range`] : Flow Direction
    - PEUCKER [`floating point number`] : Peucker & Douglas. Default: 2.000000

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('tin_tools', '1', 'Grid to TIN (Surface Specific Points)')
    if Tool.is_Okay():
        Tool.Set_Input ('GRID', GRID)
        Tool.Set_Input ('VALUES', VALUES)
        Tool.Set_Output('TIN', TIN)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('HIGH', HIGH)
        Tool.Set_Option('FLOW', FLOW)
        Tool.Set_Option('PEUCKER', PEUCKER)
        return Tool.Execute(Verbose)
    return False

def run_tool_tin_tools_1(GRID=None, VALUES=None, TIN=None, METHOD=None, HIGH=None, FLOW=None, PEUCKER=None, Verbose=2):
    '''
    Grid to TIN (Surface Specific Points)
    ----------
    [tin_tools.1]\n
    Creates a TIN by identifying (surface) specific points of a grid.\n
    Arguments
    ----------
    - GRID [`input grid`] : Grid
    - VALUES [`optional input grid list`] : Values
    - TIN [`output TIN`] : TIN
    - METHOD [`choice`] : Method. Available Choices: [0] Mark Highest Neighbour [1] Opposite Neighbours [2] Flow Direction [3] Flow Direction (up and down) [4] Peucker & Douglas Default: 1 The method used to identify surface specific points.
    - HIGH [`integer number`] : Mark Highest Neighbour. Minimum: 1 Maximum: 4 Default: 4
    - FLOW [`value range`] : Flow Direction
    - PEUCKER [`floating point number`] : Peucker & Douglas. Default: 2.000000

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('tin_tools', '1', 'Grid to TIN (Surface Specific Points)')
    if Tool.is_Okay():
        Tool.Set_Input ('GRID', GRID)
        Tool.Set_Input ('VALUES', VALUES)
        Tool.Set_Output('TIN', TIN)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('HIGH', HIGH)
        Tool.Set_Option('FLOW', FLOW)
        Tool.Set_Option('PEUCKER', PEUCKER)
        return Tool.Execute(Verbose)
    return False

def Shapes_to_TIN(SHAPES=None, TIN=None, Verbose=2):
    '''
    Shapes to TIN
    ----------
    [tin_tools.2]\n
    Convert a shapes layer to a TIN\n
    Arguments
    ----------
    - SHAPES [`input shapes`] : Shapes
    - TIN [`output TIN`] : TIN

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('tin_tools', '2', 'Shapes to TIN')
    if Tool.is_Okay():
        Tool.Set_Input ('SHAPES', SHAPES)
        Tool.Set_Output('TIN', TIN)
        return Tool.Execute(Verbose)
    return False

def run_tool_tin_tools_2(SHAPES=None, TIN=None, Verbose=2):
    '''
    Shapes to TIN
    ----------
    [tin_tools.2]\n
    Convert a shapes layer to a TIN\n
    Arguments
    ----------
    - SHAPES [`input shapes`] : Shapes
    - TIN [`output TIN`] : TIN

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('tin_tools', '2', 'Shapes to TIN')
    if Tool.is_Okay():
        Tool.Set_Input ('SHAPES', SHAPES)
        Tool.Set_Output('TIN', TIN)
        return Tool.Execute(Verbose)
    return False

def TIN_to_Shapes(TIN=None, POINTS=None, CENTER=None, EDGES=None, TRIANGLES=None, POLYGONS=None, Verbose=2):
    '''
    TIN to Shapes
    ----------
    [tin_tools.3]\n
    Converts a TIN data set to shapes layers.\n
    Arguments
    ----------
    - TIN [`input TIN`] : TIN
    - POINTS [`output shapes`] : Points
    - CENTER [`output shapes`] : Center of Triangles
    - EDGES [`output shapes`] : Edges
    - TRIANGLES [`output shapes`] : Triangles
    - POLYGONS [`output shapes`] : Polygons

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('tin_tools', '3', 'TIN to Shapes')
    if Tool.is_Okay():
        Tool.Set_Input ('TIN', TIN)
        Tool.Set_Output('POINTS', POINTS)
        Tool.Set_Output('CENTER', CENTER)
        Tool.Set_Output('EDGES', EDGES)
        Tool.Set_Output('TRIANGLES', TRIANGLES)
        Tool.Set_Output('POLYGONS', POLYGONS)
        return Tool.Execute(Verbose)
    return False

def run_tool_tin_tools_3(TIN=None, POINTS=None, CENTER=None, EDGES=None, TRIANGLES=None, POLYGONS=None, Verbose=2):
    '''
    TIN to Shapes
    ----------
    [tin_tools.3]\n
    Converts a TIN data set to shapes layers.\n
    Arguments
    ----------
    - TIN [`input TIN`] : TIN
    - POINTS [`output shapes`] : Points
    - CENTER [`output shapes`] : Center of Triangles
    - EDGES [`output shapes`] : Edges
    - TRIANGLES [`output shapes`] : Triangles
    - POLYGONS [`output shapes`] : Polygons

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('tin_tools', '3', 'TIN to Shapes')
    if Tool.is_Okay():
        Tool.Set_Input ('TIN', TIN)
        Tool.Set_Output('POINTS', POINTS)
        Tool.Set_Output('CENTER', CENTER)
        Tool.Set_Output('EDGES', EDGES)
        Tool.Set_Output('TRIANGLES', TRIANGLES)
        Tool.Set_Output('POLYGONS', POLYGONS)
        return Tool.Execute(Verbose)
    return False

def Gradient(TIN=None, GRADIENT=None, ZFIELD=None, DEGREE=None, Verbose=2):
    '''
    Gradient
    ----------
    [tin_tools.4]\n
    Calculates the gradient based on the values of each triangle's points.\n
    Arguments
    ----------
    - TIN [`input TIN`] : TIN
    - GRADIENT [`output shapes`] : TIN_Gradient
    - ZFIELD [`table field`] : Z Values
    - DEGREE [`choice`] : Output Unit. Available Choices: [0] Radians [1] Degree Default: 1

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('tin_tools', '4', 'Gradient')
    if Tool.is_Okay():
        Tool.Set_Input ('TIN', TIN)
        Tool.Set_Output('GRADIENT', GRADIENT)
        Tool.Set_Option('ZFIELD', ZFIELD)
        Tool.Set_Option('DEGREE', DEGREE)
        return Tool.Execute(Verbose)
    return False

def run_tool_tin_tools_4(TIN=None, GRADIENT=None, ZFIELD=None, DEGREE=None, Verbose=2):
    '''
    Gradient
    ----------
    [tin_tools.4]\n
    Calculates the gradient based on the values of each triangle's points.\n
    Arguments
    ----------
    - TIN [`input TIN`] : TIN
    - GRADIENT [`output shapes`] : TIN_Gradient
    - ZFIELD [`table field`] : Z Values
    - DEGREE [`choice`] : Output Unit. Available Choices: [0] Radians [1] Degree Default: 1

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('tin_tools', '4', 'Gradient')
    if Tool.is_Okay():
        Tool.Set_Input ('TIN', TIN)
        Tool.Set_Output('GRADIENT', GRADIENT)
        Tool.Set_Option('ZFIELD', ZFIELD)
        Tool.Set_Option('DEGREE', DEGREE)
        return Tool.Execute(Verbose)
    return False

def Flow_Accumulation_Trace(DEM=None, FLOW=None, ZFIELD=None, Verbose=2):
    '''
    Flow Accumulation (Trace)
    ----------
    [tin_tools.5]\n
    Calculates the catchment area based on the selected elevation values.\n
    Arguments
    ----------
    - DEM [`input TIN`] : TIN
    - FLOW [`output TIN`] : Flow Accumulation
    - ZFIELD [`table field`] : Z Values

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('tin_tools', '5', 'Flow Accumulation (Trace)')
    if Tool.is_Okay():
        Tool.Set_Input ('DEM', DEM)
        Tool.Set_Output('FLOW', FLOW)
        Tool.Set_Option('ZFIELD', ZFIELD)
        return Tool.Execute(Verbose)
    return False

def run_tool_tin_tools_5(DEM=None, FLOW=None, ZFIELD=None, Verbose=2):
    '''
    Flow Accumulation (Trace)
    ----------
    [tin_tools.5]\n
    Calculates the catchment area based on the selected elevation values.\n
    Arguments
    ----------
    - DEM [`input TIN`] : TIN
    - FLOW [`output TIN`] : Flow Accumulation
    - ZFIELD [`table field`] : Z Values

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('tin_tools', '5', 'Flow Accumulation (Trace)')
    if Tool.is_Okay():
        Tool.Set_Input ('DEM', DEM)
        Tool.Set_Output('FLOW', FLOW)
        Tool.Set_Option('ZFIELD', ZFIELD)
        return Tool.Execute(Verbose)
    return False

def Flow_Accumulation_Parallel(DEM=None, FLOW=None, ZFIELD=None, METHOD=None, Verbose=2):
    '''
    Flow Accumulation (Parallel)
    ----------
    [tin_tools.6]\n
    Calculates the catchment area based on the selected elevation values.\n
    Arguments
    ----------
    - DEM [`input TIN`] : TIN
    - FLOW [`output TIN`] : Flow Accumulation
    - ZFIELD [`table field`] : Z Values
    - METHOD [`choice`] : Method. Available Choices: [0] Single Flow Direction [1] Multiple Flow Direction Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('tin_tools', '6', 'Flow Accumulation (Parallel)')
    if Tool.is_Okay():
        Tool.Set_Input ('DEM', DEM)
        Tool.Set_Output('FLOW', FLOW)
        Tool.Set_Option('ZFIELD', ZFIELD)
        Tool.Set_Option('METHOD', METHOD)
        return Tool.Execute(Verbose)
    return False

def run_tool_tin_tools_6(DEM=None, FLOW=None, ZFIELD=None, METHOD=None, Verbose=2):
    '''
    Flow Accumulation (Parallel)
    ----------
    [tin_tools.6]\n
    Calculates the catchment area based on the selected elevation values.\n
    Arguments
    ----------
    - DEM [`input TIN`] : TIN
    - FLOW [`output TIN`] : Flow Accumulation
    - ZFIELD [`table field`] : Z Values
    - METHOD [`choice`] : Method. Available Choices: [0] Single Flow Direction [1] Multiple Flow Direction Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('tin_tools', '6', 'Flow Accumulation (Parallel)')
    if Tool.is_Okay():
        Tool.Set_Input ('DEM', DEM)
        Tool.Set_Output('FLOW', FLOW)
        Tool.Set_Option('ZFIELD', ZFIELD)
        Tool.Set_Option('METHOD', METHOD)
        return Tool.Execute(Verbose)
    return False

