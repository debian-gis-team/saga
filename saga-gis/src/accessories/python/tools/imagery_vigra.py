#! /usr/bin/env python

'''
Python Interface to SAGA Tools Library
----------
- Category : Imagery
- Name     : ViGrA [deprecated]
- ID       : imagery_vigra

Description
----------
ViGrA - "Vision with Generic Algorithms"
Version: 1.11.1
ViGrA is a novel computer vision library that puts its main emphasize on customizable algorithms and data structures. By using template techniques similar to those in the C++ Standard Template Library (STL), you can easily adapt any ViGrA component to the needs of your application, without thereby giving up execution speed.
Find out more at the [ViGrA - Homepage](https://ukoethe.github.io/vigra/).
[[Because the ViGrA project seems to be abandoned since quite a while SAGA's ViGrA tool set has been marked as <u>deprecated</u> and might become removed from future SAGA versions!]
'''

from PySAGA.helper import Tool_Wrapper

def Smoothing_ViGrA(INPUT=None, OUTPUT=None, TYPE=None, SCALE=None, EDGE=None, Verbose=2):
    '''
    Smoothing (ViGrA)
    ----------
    [imagery_vigra.0]\n
    Based on the code example "smooth.cxx" by Ullrich Koethe.\n
    Arguments
    ----------
    - INPUT [`input grid`] : Input
    - OUTPUT [`output grid`] : Output
    - TYPE [`choice`] : Type of smoothing. Available Choices: [0] exponential [1] nonlinear [2] gaussian Default: 0
    - SCALE [`floating point number`] : Size of smoothing filter. Minimum: 0.000000 Default: 2.000000 Smoothing kernel size specified as multiple of a cell.
    - EDGE [`floating point number`] : Edge threshold for nonlinear smoothing. Minimum: 0.000000 Default: 1.000000

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('imagery_vigra', '0', 'Smoothing (ViGrA)')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Output('OUTPUT', OUTPUT)
        Tool.Set_Option('TYPE', TYPE)
        Tool.Set_Option('SCALE', SCALE)
        Tool.Set_Option('EDGE', EDGE)
        return Tool.Execute(Verbose)
    return False

def run_tool_imagery_vigra_0(INPUT=None, OUTPUT=None, TYPE=None, SCALE=None, EDGE=None, Verbose=2):
    '''
    Smoothing (ViGrA)
    ----------
    [imagery_vigra.0]\n
    Based on the code example "smooth.cxx" by Ullrich Koethe.\n
    Arguments
    ----------
    - INPUT [`input grid`] : Input
    - OUTPUT [`output grid`] : Output
    - TYPE [`choice`] : Type of smoothing. Available Choices: [0] exponential [1] nonlinear [2] gaussian Default: 0
    - SCALE [`floating point number`] : Size of smoothing filter. Minimum: 0.000000 Default: 2.000000 Smoothing kernel size specified as multiple of a cell.
    - EDGE [`floating point number`] : Edge threshold for nonlinear smoothing. Minimum: 0.000000 Default: 1.000000

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('imagery_vigra', '0', 'Smoothing (ViGrA)')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Output('OUTPUT', OUTPUT)
        Tool.Set_Option('TYPE', TYPE)
        Tool.Set_Option('SCALE', SCALE)
        Tool.Set_Option('EDGE', EDGE)
        return Tool.Execute(Verbose)
    return False

def Edge_Detection_ViGrA(INPUT=None, OUTPUT=None, TYPE=None, SCALE=None, THRESHOLD=None, Verbose=2):
    '''
    Edge Detection (ViGrA)
    ----------
    [imagery_vigra.1]\n
    Edge detection.\n
    Arguments
    ----------
    - INPUT [`input grid`] : Input
    - OUTPUT [`output grid`] : Edges
    - TYPE [`choice`] : Detector type. Available Choices: [0] Canny [1] Shen-Castan Default: 0
    - SCALE [`floating point number`] : Operator scale. Minimum: 0.000000 Default: 1.000000
    - THRESHOLD [`floating point number`] : Gradient threshold. Minimum: 0.000000 Default: 1.000000

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('imagery_vigra', '1', 'Edge Detection (ViGrA)')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Output('OUTPUT', OUTPUT)
        Tool.Set_Option('TYPE', TYPE)
        Tool.Set_Option('SCALE', SCALE)
        Tool.Set_Option('THRESHOLD', THRESHOLD)
        return Tool.Execute(Verbose)
    return False

def run_tool_imagery_vigra_1(INPUT=None, OUTPUT=None, TYPE=None, SCALE=None, THRESHOLD=None, Verbose=2):
    '''
    Edge Detection (ViGrA)
    ----------
    [imagery_vigra.1]\n
    Edge detection.\n
    Arguments
    ----------
    - INPUT [`input grid`] : Input
    - OUTPUT [`output grid`] : Edges
    - TYPE [`choice`] : Detector type. Available Choices: [0] Canny [1] Shen-Castan Default: 0
    - SCALE [`floating point number`] : Operator scale. Minimum: 0.000000 Default: 1.000000
    - THRESHOLD [`floating point number`] : Gradient threshold. Minimum: 0.000000 Default: 1.000000

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('imagery_vigra', '1', 'Edge Detection (ViGrA)')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Output('OUTPUT', OUTPUT)
        Tool.Set_Option('TYPE', TYPE)
        Tool.Set_Option('SCALE', SCALE)
        Tool.Set_Option('THRESHOLD', THRESHOLD)
        return Tool.Execute(Verbose)
    return False

def Morphological_Filter_ViGrA(INPUT=None, OUTPUT=None, TYPE=None, RADIUS=None, RANK=None, RESCALE=None, Verbose=2):
    '''
    Morphological Filter (ViGrA)
    ----------
    [imagery_vigra.2]\n
    Morphological filter.\n
    Arguments
    ----------
    - INPUT [`input grid`] : Input
    - OUTPUT [`output grid`] : Output
    - TYPE [`choice`] : Operation. Available Choices: [0] Dilation [1] Erosion [2] Median [3] User defined rank Default: 0
    - RADIUS [`integer number`] : Radius (cells). Minimum: 0 Default: 1
    - RANK [`floating point number`] : User defined rank. Minimum: 0.000000 Maximum: 1.000000 Default: 0.500000
    - RESCALE [`boolean`] : Rescale Values (0-255). Default: 1

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('imagery_vigra', '2', 'Morphological Filter (ViGrA)')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Output('OUTPUT', OUTPUT)
        Tool.Set_Option('TYPE', TYPE)
        Tool.Set_Option('RADIUS', RADIUS)
        Tool.Set_Option('RANK', RANK)
        Tool.Set_Option('RESCALE', RESCALE)
        return Tool.Execute(Verbose)
    return False

def run_tool_imagery_vigra_2(INPUT=None, OUTPUT=None, TYPE=None, RADIUS=None, RANK=None, RESCALE=None, Verbose=2):
    '''
    Morphological Filter (ViGrA)
    ----------
    [imagery_vigra.2]\n
    Morphological filter.\n
    Arguments
    ----------
    - INPUT [`input grid`] : Input
    - OUTPUT [`output grid`] : Output
    - TYPE [`choice`] : Operation. Available Choices: [0] Dilation [1] Erosion [2] Median [3] User defined rank Default: 0
    - RADIUS [`integer number`] : Radius (cells). Minimum: 0 Default: 1
    - RANK [`floating point number`] : User defined rank. Minimum: 0.000000 Maximum: 1.000000 Default: 0.500000
    - RESCALE [`boolean`] : Rescale Values (0-255). Default: 1

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('imagery_vigra', '2', 'Morphological Filter (ViGrA)')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Output('OUTPUT', OUTPUT)
        Tool.Set_Option('TYPE', TYPE)
        Tool.Set_Option('RADIUS', RADIUS)
        Tool.Set_Option('RANK', RANK)
        Tool.Set_Option('RESCALE', RESCALE)
        return Tool.Execute(Verbose)
    return False

def Distance_ViGrA(INPUT=None, OUTPUT=None, NORM=None, Verbose=2):
    '''
    Distance (ViGrA)
    ----------
    [imagery_vigra.3]\n
    Distance to feature cells on a raster. Feature cells are all cells not representing a no-data value.\n
    Arguments
    ----------
    - INPUT [`input grid`] : Features. Features are all pixels not representing a no-data value.
    - OUTPUT [`output grid`] : Distance
    - NORM [`choice`] : Type of distance calculation. Available Choices: [0] Chessboard [1] Manhattan [2] Euclidean Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('imagery_vigra', '3', 'Distance (ViGrA)')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Output('OUTPUT', OUTPUT)
        Tool.Set_Option('NORM', NORM)
        return Tool.Execute(Verbose)
    return False

def run_tool_imagery_vigra_3(INPUT=None, OUTPUT=None, NORM=None, Verbose=2):
    '''
    Distance (ViGrA)
    ----------
    [imagery_vigra.3]\n
    Distance to feature cells on a raster. Feature cells are all cells not representing a no-data value.\n
    Arguments
    ----------
    - INPUT [`input grid`] : Features. Features are all pixels not representing a no-data value.
    - OUTPUT [`output grid`] : Distance
    - NORM [`choice`] : Type of distance calculation. Available Choices: [0] Chessboard [1] Manhattan [2] Euclidean Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('imagery_vigra', '3', 'Distance (ViGrA)')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Output('OUTPUT', OUTPUT)
        Tool.Set_Option('NORM', NORM)
        return Tool.Execute(Verbose)
    return False

def Watershed_Segmentation_ViGrA(INPUT=None, OUTPUT=None, SCALE=None, RGB=None, EDGES=None, Verbose=2):
    '''
    Watershed Segmentation (ViGrA)
    ----------
    [imagery_vigra.4]\n
    Note that the watershed algorithm usually results in an oversegmentation (i.e., too many regions), but its boundary localization is quite good.\n
    Based on the code example "watershed.cxx" by Ullrich Koethe.\n
    Arguments
    ----------
    - INPUT [`input grid`] : Input
    - OUTPUT [`output grid`] : Segmentation
    - SCALE [`floating point number`] : Width of gradient filter. Minimum: 0.000000 Default: 1.000000
    - RGB [`boolean`] : RGB coded data. Default: 0
    - EDGES [`boolean`] : Edges. Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('imagery_vigra', '4', 'Watershed Segmentation (ViGrA)')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Output('OUTPUT', OUTPUT)
        Tool.Set_Option('SCALE', SCALE)
        Tool.Set_Option('RGB', RGB)
        Tool.Set_Option('EDGES', EDGES)
        return Tool.Execute(Verbose)
    return False

def run_tool_imagery_vigra_4(INPUT=None, OUTPUT=None, SCALE=None, RGB=None, EDGES=None, Verbose=2):
    '''
    Watershed Segmentation (ViGrA)
    ----------
    [imagery_vigra.4]\n
    Note that the watershed algorithm usually results in an oversegmentation (i.e., too many regions), but its boundary localization is quite good.\n
    Based on the code example "watershed.cxx" by Ullrich Koethe.\n
    Arguments
    ----------
    - INPUT [`input grid`] : Input
    - OUTPUT [`output grid`] : Segmentation
    - SCALE [`floating point number`] : Width of gradient filter. Minimum: 0.000000 Default: 1.000000
    - RGB [`boolean`] : RGB coded data. Default: 0
    - EDGES [`boolean`] : Edges. Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('imagery_vigra', '4', 'Watershed Segmentation (ViGrA)')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Output('OUTPUT', OUTPUT)
        Tool.Set_Option('SCALE', SCALE)
        Tool.Set_Option('RGB', RGB)
        Tool.Set_Option('EDGES', EDGES)
        return Tool.Execute(Verbose)
    return False

def Fourier_Transform_ViGrA(INPUT=None, REAL=None, IMAG=None, CENTER=None, Verbose=2):
    '''
    Fourier Transform (ViGrA)
    ----------
    [imagery_vigra.5]\n
    Fourier Transform.\n
    Arguments
    ----------
    - INPUT [`input grid`] : Input
    - REAL [`output grid`] : Real
    - IMAG [`output grid`] : Imaginary
    - CENTER [`boolean`] : Centered. Default: 1

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('imagery_vigra', '5', 'Fourier Transform (ViGrA)')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Output('REAL', REAL)
        Tool.Set_Output('IMAG', IMAG)
        Tool.Set_Option('CENTER', CENTER)
        return Tool.Execute(Verbose)
    return False

def run_tool_imagery_vigra_5(INPUT=None, REAL=None, IMAG=None, CENTER=None, Verbose=2):
    '''
    Fourier Transform (ViGrA)
    ----------
    [imagery_vigra.5]\n
    Fourier Transform.\n
    Arguments
    ----------
    - INPUT [`input grid`] : Input
    - REAL [`output grid`] : Real
    - IMAG [`output grid`] : Imaginary
    - CENTER [`boolean`] : Centered. Default: 1

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('imagery_vigra', '5', 'Fourier Transform (ViGrA)')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Output('REAL', REAL)
        Tool.Set_Output('IMAG', IMAG)
        Tool.Set_Option('CENTER', CENTER)
        return Tool.Execute(Verbose)
    return False

def Fourier_Transform_Inverse_ViGrA(REAL=None, IMAG=None, OUTPUT=None, CENTER=None, Verbose=2):
    '''
    Fourier Transform Inverse (ViGrA)
    ----------
    [imagery_vigra.6]\n
    Inverse Fourier Transform.\n
    Arguments
    ----------
    - REAL [`input grid`] : Real
    - IMAG [`input grid`] : Imaginary
    - OUTPUT [`output grid`] : Output
    - CENTER [`boolean`] : Centered. Default: 1

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('imagery_vigra', '6', 'Fourier Transform Inverse (ViGrA)')
    if Tool.is_Okay():
        Tool.Set_Input ('REAL', REAL)
        Tool.Set_Input ('IMAG', IMAG)
        Tool.Set_Output('OUTPUT', OUTPUT)
        Tool.Set_Option('CENTER', CENTER)
        return Tool.Execute(Verbose)
    return False

def run_tool_imagery_vigra_6(REAL=None, IMAG=None, OUTPUT=None, CENTER=None, Verbose=2):
    '''
    Fourier Transform Inverse (ViGrA)
    ----------
    [imagery_vigra.6]\n
    Inverse Fourier Transform.\n
    Arguments
    ----------
    - REAL [`input grid`] : Real
    - IMAG [`input grid`] : Imaginary
    - OUTPUT [`output grid`] : Output
    - CENTER [`boolean`] : Centered. Default: 1

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('imagery_vigra', '6', 'Fourier Transform Inverse (ViGrA)')
    if Tool.is_Okay():
        Tool.Set_Input ('REAL', REAL)
        Tool.Set_Input ('IMAG', IMAG)
        Tool.Set_Output('OUTPUT', OUTPUT)
        Tool.Set_Option('CENTER', CENTER)
        return Tool.Execute(Verbose)
    return False

def Fourier_Filter_ViGrA(INPUT=None, OUTPUT=None, SCALE=None, POWER=None, RANGE=None, FILTER=None, Verbose=2):
    '''
    Fourier Filter (ViGrA)
    ----------
    [imagery_vigra.8]\n
    Fourier Filter.\n
    Arguments
    ----------
    - INPUT [`input grid`] : Input
    - OUTPUT [`output grid`] : Output
    - SCALE [`floating point number`] : Size of smoothing filter. Minimum: 0.000000 Default: 2.000000
    - POWER [`floating point number`] : Power. Default: 0.500000
    - RANGE [`value range`] : Range
    - FILTER [`choice`] : Filter. Available Choices: [0] gaussian [1] power of distance [2] include range [3] exclude range Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('imagery_vigra', '8', 'Fourier Filter (ViGrA)')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Output('OUTPUT', OUTPUT)
        Tool.Set_Option('SCALE', SCALE)
        Tool.Set_Option('POWER', POWER)
        Tool.Set_Option('RANGE', RANGE)
        Tool.Set_Option('FILTER', FILTER)
        return Tool.Execute(Verbose)
    return False

def run_tool_imagery_vigra_8(INPUT=None, OUTPUT=None, SCALE=None, POWER=None, RANGE=None, FILTER=None, Verbose=2):
    '''
    Fourier Filter (ViGrA)
    ----------
    [imagery_vigra.8]\n
    Fourier Filter.\n
    Arguments
    ----------
    - INPUT [`input grid`] : Input
    - OUTPUT [`output grid`] : Output
    - SCALE [`floating point number`] : Size of smoothing filter. Minimum: 0.000000 Default: 2.000000
    - POWER [`floating point number`] : Power. Default: 0.500000
    - RANGE [`value range`] : Range
    - FILTER [`choice`] : Filter. Available Choices: [0] gaussian [1] power of distance [2] include range [3] exclude range Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('imagery_vigra', '8', 'Fourier Filter (ViGrA)')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Output('OUTPUT', OUTPUT)
        Tool.Set_Option('SCALE', SCALE)
        Tool.Set_Option('POWER', POWER)
        Tool.Set_Option('RANGE', RANGE)
        Tool.Set_Option('FILTER', FILTER)
        return Tool.Execute(Verbose)
    return False

def Random_Forest_Classification_ViGrA(FEATURES=None, TRAINING=None, CLASSES=None, PROBABILITY=None, PROBABILITIES=None, IMPORTANCES=None, BPROBABILITIES=None, FIELD=None, LABEL_AS_ID=None, DO_MRMR=None, MRMR_NFEATURES=None, MRMR_DISCRETIZE=None, MRMR_THRESHOLD=None, MRMR_METHOD=None, RF_TREE_COUNT=None, RF_TREE_SAMPLES=None, RF_REPLACE=None, RF_SPLIT_MIN_SIZE=None, RF_NODE_FEATURES=None, RF_STRATIFICATION=None, Verbose=2):
    '''
    Random Forest Classification (ViGrA)
    ----------
    [imagery_vigra.9]\n
    Random Forest Classification.\n
    Arguments
    ----------
    - FEATURES [`input grid list`] : Features
    - TRAINING [`input shapes`] : Training Areas
    - CLASSES [`output grid`] : Random Forest Classification
    - PROBABILITY [`output grid`] : Prediction Probability
    - PROBABILITIES [`output grid list`] : Feature Probabilities
    - IMPORTANCES [`output table`] : Feature Importances
    - BPROBABILITIES [`boolean`] : Feature Probabilities. Default: 0
    - FIELD [`table field`] : Label Field
    - LABEL_AS_ID [`boolean`] : Use Label as Identifier. Default: 0 Use training area labels as identifier in classification result, assumes all label values are integer numbers!
    - DO_MRMR [`boolean`] : Minimum Redundancy Feature Selection. Default: 0 Use only features selected by the minimum Redundancy Maximum Relevance (mRMR) algorithm
    - MRMR_NFEATURES [`integer number`] : Number of Features. Minimum: 1 Default: 50
    - MRMR_DISCRETIZE [`boolean`] : Discretization. Default: 1 uncheck this means no discretizaton (i.e. data is already integer)
    - MRMR_THRESHOLD [`floating point number`] : Discretization Threshold. Minimum: 0.000000 Default: 1.000000 a double number of the discretization threshold; set to 0 to make binarization
    - MRMR_METHOD [`choice`] : Selection Method. Available Choices: [0] Mutual Information Difference (MID) [1] Mutual Information Quotient (MIQ) Default: 0
    - RF_TREE_COUNT [`integer number`] : Tree Count. Minimum: 1 Default: 32 How many trees to create?
    - RF_TREE_SAMPLES [`floating point number`] : Samples per Tree. Minimum: 0.000000 Maximum: 1.000000 Default: 1.000000 Specifies the fraction of the total number of samples used per tree for learning.
    - RF_REPLACE [`boolean`] : Sample with Replacement. Default: 1 Sample from training population with or without replacement?
    - RF_SPLIT_MIN_SIZE [`integer number`] : Minimum Node Split Size. Minimum: 1 Default: 1 Number of examples required for a node to be split. Choose 1 for complete growing.
    - RF_NODE_FEATURES [`choice`] : Features per Node. Available Choices: [0] logarithmic [1] square root [2] all Default: 1
    - RF_STRATIFICATION [`choice`] : Stratification. Available Choices: [0] none [1] equal [2] proportional Default: 0 Specifies stratification strategy. Either none, equal amount of class samples, or proportional to fraction of class samples.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('imagery_vigra', '9', 'Random Forest Classification (ViGrA)')
    if Tool.is_Okay():
        Tool.Set_Input ('FEATURES', FEATURES)
        Tool.Set_Input ('TRAINING', TRAINING)
        Tool.Set_Output('CLASSES', CLASSES)
        Tool.Set_Output('PROBABILITY', PROBABILITY)
        Tool.Set_Output('PROBABILITIES', PROBABILITIES)
        Tool.Set_Output('IMPORTANCES', IMPORTANCES)
        Tool.Set_Option('BPROBABILITIES', BPROBABILITIES)
        Tool.Set_Option('FIELD', FIELD)
        Tool.Set_Option('LABEL_AS_ID', LABEL_AS_ID)
        Tool.Set_Option('DO_MRMR', DO_MRMR)
        Tool.Set_Option('mRMR_NFEATURES', MRMR_NFEATURES)
        Tool.Set_Option('mRMR_DISCRETIZE', MRMR_DISCRETIZE)
        Tool.Set_Option('mRMR_THRESHOLD', MRMR_THRESHOLD)
        Tool.Set_Option('mRMR_METHOD', MRMR_METHOD)
        Tool.Set_Option('RF_TREE_COUNT', RF_TREE_COUNT)
        Tool.Set_Option('RF_TREE_SAMPLES', RF_TREE_SAMPLES)
        Tool.Set_Option('RF_REPLACE', RF_REPLACE)
        Tool.Set_Option('RF_SPLIT_MIN_SIZE', RF_SPLIT_MIN_SIZE)
        Tool.Set_Option('RF_NODE_FEATURES', RF_NODE_FEATURES)
        Tool.Set_Option('RF_STRATIFICATION', RF_STRATIFICATION)
        return Tool.Execute(Verbose)
    return False

def run_tool_imagery_vigra_9(FEATURES=None, TRAINING=None, CLASSES=None, PROBABILITY=None, PROBABILITIES=None, IMPORTANCES=None, BPROBABILITIES=None, FIELD=None, LABEL_AS_ID=None, DO_MRMR=None, MRMR_NFEATURES=None, MRMR_DISCRETIZE=None, MRMR_THRESHOLD=None, MRMR_METHOD=None, RF_TREE_COUNT=None, RF_TREE_SAMPLES=None, RF_REPLACE=None, RF_SPLIT_MIN_SIZE=None, RF_NODE_FEATURES=None, RF_STRATIFICATION=None, Verbose=2):
    '''
    Random Forest Classification (ViGrA)
    ----------
    [imagery_vigra.9]\n
    Random Forest Classification.\n
    Arguments
    ----------
    - FEATURES [`input grid list`] : Features
    - TRAINING [`input shapes`] : Training Areas
    - CLASSES [`output grid`] : Random Forest Classification
    - PROBABILITY [`output grid`] : Prediction Probability
    - PROBABILITIES [`output grid list`] : Feature Probabilities
    - IMPORTANCES [`output table`] : Feature Importances
    - BPROBABILITIES [`boolean`] : Feature Probabilities. Default: 0
    - FIELD [`table field`] : Label Field
    - LABEL_AS_ID [`boolean`] : Use Label as Identifier. Default: 0 Use training area labels as identifier in classification result, assumes all label values are integer numbers!
    - DO_MRMR [`boolean`] : Minimum Redundancy Feature Selection. Default: 0 Use only features selected by the minimum Redundancy Maximum Relevance (mRMR) algorithm
    - MRMR_NFEATURES [`integer number`] : Number of Features. Minimum: 1 Default: 50
    - MRMR_DISCRETIZE [`boolean`] : Discretization. Default: 1 uncheck this means no discretizaton (i.e. data is already integer)
    - MRMR_THRESHOLD [`floating point number`] : Discretization Threshold. Minimum: 0.000000 Default: 1.000000 a double number of the discretization threshold; set to 0 to make binarization
    - MRMR_METHOD [`choice`] : Selection Method. Available Choices: [0] Mutual Information Difference (MID) [1] Mutual Information Quotient (MIQ) Default: 0
    - RF_TREE_COUNT [`integer number`] : Tree Count. Minimum: 1 Default: 32 How many trees to create?
    - RF_TREE_SAMPLES [`floating point number`] : Samples per Tree. Minimum: 0.000000 Maximum: 1.000000 Default: 1.000000 Specifies the fraction of the total number of samples used per tree for learning.
    - RF_REPLACE [`boolean`] : Sample with Replacement. Default: 1 Sample from training population with or without replacement?
    - RF_SPLIT_MIN_SIZE [`integer number`] : Minimum Node Split Size. Minimum: 1 Default: 1 Number of examples required for a node to be split. Choose 1 for complete growing.
    - RF_NODE_FEATURES [`choice`] : Features per Node. Available Choices: [0] logarithmic [1] square root [2] all Default: 1
    - RF_STRATIFICATION [`choice`] : Stratification. Available Choices: [0] none [1] equal [2] proportional Default: 0 Specifies stratification strategy. Either none, equal amount of class samples, or proportional to fraction of class samples.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('imagery_vigra', '9', 'Random Forest Classification (ViGrA)')
    if Tool.is_Okay():
        Tool.Set_Input ('FEATURES', FEATURES)
        Tool.Set_Input ('TRAINING', TRAINING)
        Tool.Set_Output('CLASSES', CLASSES)
        Tool.Set_Output('PROBABILITY', PROBABILITY)
        Tool.Set_Output('PROBABILITIES', PROBABILITIES)
        Tool.Set_Output('IMPORTANCES', IMPORTANCES)
        Tool.Set_Option('BPROBABILITIES', BPROBABILITIES)
        Tool.Set_Option('FIELD', FIELD)
        Tool.Set_Option('LABEL_AS_ID', LABEL_AS_ID)
        Tool.Set_Option('DO_MRMR', DO_MRMR)
        Tool.Set_Option('mRMR_NFEATURES', MRMR_NFEATURES)
        Tool.Set_Option('mRMR_DISCRETIZE', MRMR_DISCRETIZE)
        Tool.Set_Option('mRMR_THRESHOLD', MRMR_THRESHOLD)
        Tool.Set_Option('mRMR_METHOD', MRMR_METHOD)
        Tool.Set_Option('RF_TREE_COUNT', RF_TREE_COUNT)
        Tool.Set_Option('RF_TREE_SAMPLES', RF_TREE_SAMPLES)
        Tool.Set_Option('RF_REPLACE', RF_REPLACE)
        Tool.Set_Option('RF_SPLIT_MIN_SIZE', RF_SPLIT_MIN_SIZE)
        Tool.Set_Option('RF_NODE_FEATURES', RF_NODE_FEATURES)
        Tool.Set_Option('RF_STRATIFICATION', RF_STRATIFICATION)
        return Tool.Execute(Verbose)
    return False

def Random_Forest_Presence_Prediction_ViGrA(FEATURES=None, PRESENCE=None, PREDICTION=None, PROBABILITY=None, BACKGROUND=None, DO_MRMR=None, MRMR_NFEATURES=None, MRMR_DISCRETIZE=None, MRMR_THRESHOLD=None, MRMR_METHOD=None, RF_TREE_COUNT=None, RF_TREE_SAMPLES=None, RF_REPLACE=None, RF_SPLIT_MIN_SIZE=None, RF_NODE_FEATURES=None, RF_STRATIFICATION=None, Verbose=2):
    '''
    Random Forest Presence Prediction (ViGrA)
    ----------
    [imagery_vigra.10]\n
    Random Forest Presence Prediction\n
    Arguments
    ----------
    - FEATURES [`input grid list`] : Features
    - PRESENCE [`input shapes`] : Presence Data
    - PREDICTION [`output grid`] : Presence Prediction
    - PROBABILITY [`output grid`] : Presence Probability
    - BACKGROUND [`floating point number`] : Background Sample Density [Percent]. Minimum: 0.000000 Maximum: 100.000000 Default: 1.000000
    - DO_MRMR [`boolean`] : Minimum Redundancy Feature Selection. Default: 0 Use only features selected by the minimum Redundancy Maximum Relevance (mRMR) algorithm
    - MRMR_NFEATURES [`integer number`] : Number of Features. Minimum: 1 Default: 50
    - MRMR_DISCRETIZE [`boolean`] : Discretization. Default: 1 uncheck this means no discretizaton (i.e. data is already integer)
    - MRMR_THRESHOLD [`floating point number`] : Discretization Threshold. Minimum: 0.000000 Default: 1.000000 a double number of the discretization threshold; set to 0 to make binarization
    - MRMR_METHOD [`choice`] : Selection Method. Available Choices: [0] Mutual Information Difference (MID) [1] Mutual Information Quotient (MIQ) Default: 0
    - RF_TREE_COUNT [`integer number`] : Tree Count. Minimum: 1 Default: 32 How many trees to create?
    - RF_TREE_SAMPLES [`floating point number`] : Samples per Tree. Minimum: 0.000000 Maximum: 1.000000 Default: 1.000000 Specifies the fraction of the total number of samples used per tree for learning.
    - RF_REPLACE [`boolean`] : Sample with Replacement. Default: 1 Sample from training population with or without replacement?
    - RF_SPLIT_MIN_SIZE [`integer number`] : Minimum Node Split Size. Minimum: 1 Default: 1 Number of examples required for a node to be split. Choose 1 for complete growing.
    - RF_NODE_FEATURES [`choice`] : Features per Node. Available Choices: [0] logarithmic [1] square root [2] all Default: 1
    - RF_STRATIFICATION [`choice`] : Stratification. Available Choices: [0] none [1] equal [2] proportional Default: 0 Specifies stratification strategy. Either none, equal amount of class samples, or proportional to fraction of class samples.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('imagery_vigra', '10', 'Random Forest Presence Prediction (ViGrA)')
    if Tool.is_Okay():
        Tool.Set_Input ('FEATURES', FEATURES)
        Tool.Set_Input ('PRESENCE', PRESENCE)
        Tool.Set_Output('PREDICTION', PREDICTION)
        Tool.Set_Output('PROBABILITY', PROBABILITY)
        Tool.Set_Option('BACKGROUND', BACKGROUND)
        Tool.Set_Option('DO_MRMR', DO_MRMR)
        Tool.Set_Option('mRMR_NFEATURES', MRMR_NFEATURES)
        Tool.Set_Option('mRMR_DISCRETIZE', MRMR_DISCRETIZE)
        Tool.Set_Option('mRMR_THRESHOLD', MRMR_THRESHOLD)
        Tool.Set_Option('mRMR_METHOD', MRMR_METHOD)
        Tool.Set_Option('RF_TREE_COUNT', RF_TREE_COUNT)
        Tool.Set_Option('RF_TREE_SAMPLES', RF_TREE_SAMPLES)
        Tool.Set_Option('RF_REPLACE', RF_REPLACE)
        Tool.Set_Option('RF_SPLIT_MIN_SIZE', RF_SPLIT_MIN_SIZE)
        Tool.Set_Option('RF_NODE_FEATURES', RF_NODE_FEATURES)
        Tool.Set_Option('RF_STRATIFICATION', RF_STRATIFICATION)
        return Tool.Execute(Verbose)
    return False

def run_tool_imagery_vigra_10(FEATURES=None, PRESENCE=None, PREDICTION=None, PROBABILITY=None, BACKGROUND=None, DO_MRMR=None, MRMR_NFEATURES=None, MRMR_DISCRETIZE=None, MRMR_THRESHOLD=None, MRMR_METHOD=None, RF_TREE_COUNT=None, RF_TREE_SAMPLES=None, RF_REPLACE=None, RF_SPLIT_MIN_SIZE=None, RF_NODE_FEATURES=None, RF_STRATIFICATION=None, Verbose=2):
    '''
    Random Forest Presence Prediction (ViGrA)
    ----------
    [imagery_vigra.10]\n
    Random Forest Presence Prediction\n
    Arguments
    ----------
    - FEATURES [`input grid list`] : Features
    - PRESENCE [`input shapes`] : Presence Data
    - PREDICTION [`output grid`] : Presence Prediction
    - PROBABILITY [`output grid`] : Presence Probability
    - BACKGROUND [`floating point number`] : Background Sample Density [Percent]. Minimum: 0.000000 Maximum: 100.000000 Default: 1.000000
    - DO_MRMR [`boolean`] : Minimum Redundancy Feature Selection. Default: 0 Use only features selected by the minimum Redundancy Maximum Relevance (mRMR) algorithm
    - MRMR_NFEATURES [`integer number`] : Number of Features. Minimum: 1 Default: 50
    - MRMR_DISCRETIZE [`boolean`] : Discretization. Default: 1 uncheck this means no discretizaton (i.e. data is already integer)
    - MRMR_THRESHOLD [`floating point number`] : Discretization Threshold. Minimum: 0.000000 Default: 1.000000 a double number of the discretization threshold; set to 0 to make binarization
    - MRMR_METHOD [`choice`] : Selection Method. Available Choices: [0] Mutual Information Difference (MID) [1] Mutual Information Quotient (MIQ) Default: 0
    - RF_TREE_COUNT [`integer number`] : Tree Count. Minimum: 1 Default: 32 How many trees to create?
    - RF_TREE_SAMPLES [`floating point number`] : Samples per Tree. Minimum: 0.000000 Maximum: 1.000000 Default: 1.000000 Specifies the fraction of the total number of samples used per tree for learning.
    - RF_REPLACE [`boolean`] : Sample with Replacement. Default: 1 Sample from training population with or without replacement?
    - RF_SPLIT_MIN_SIZE [`integer number`] : Minimum Node Split Size. Minimum: 1 Default: 1 Number of examples required for a node to be split. Choose 1 for complete growing.
    - RF_NODE_FEATURES [`choice`] : Features per Node. Available Choices: [0] logarithmic [1] square root [2] all Default: 1
    - RF_STRATIFICATION [`choice`] : Stratification. Available Choices: [0] none [1] equal [2] proportional Default: 0 Specifies stratification strategy. Either none, equal amount of class samples, or proportional to fraction of class samples.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('imagery_vigra', '10', 'Random Forest Presence Prediction (ViGrA)')
    if Tool.is_Okay():
        Tool.Set_Input ('FEATURES', FEATURES)
        Tool.Set_Input ('PRESENCE', PRESENCE)
        Tool.Set_Output('PREDICTION', PREDICTION)
        Tool.Set_Output('PROBABILITY', PROBABILITY)
        Tool.Set_Option('BACKGROUND', BACKGROUND)
        Tool.Set_Option('DO_MRMR', DO_MRMR)
        Tool.Set_Option('mRMR_NFEATURES', MRMR_NFEATURES)
        Tool.Set_Option('mRMR_DISCRETIZE', MRMR_DISCRETIZE)
        Tool.Set_Option('mRMR_THRESHOLD', MRMR_THRESHOLD)
        Tool.Set_Option('mRMR_METHOD', MRMR_METHOD)
        Tool.Set_Option('RF_TREE_COUNT', RF_TREE_COUNT)
        Tool.Set_Option('RF_TREE_SAMPLES', RF_TREE_SAMPLES)
        Tool.Set_Option('RF_REPLACE', RF_REPLACE)
        Tool.Set_Option('RF_SPLIT_MIN_SIZE', RF_SPLIT_MIN_SIZE)
        Tool.Set_Option('RF_NODE_FEATURES', RF_NODE_FEATURES)
        Tool.Set_Option('RF_STRATIFICATION', RF_STRATIFICATION)
        return Tool.Execute(Verbose)
    return False

def Random_Forest_Table_Classification_ViGrA(TABLE=None, IMPORTANCES=None, FEATURES=None, PREDICTION=None, TRAINING=None, LABEL_AS_ID=None, RF_TREE_COUNT=None, RF_TREE_SAMPLES=None, RF_REPLACE=None, RF_SPLIT_MIN_SIZE=None, RF_NODE_FEATURES=None, RF_STRATIFICATION=None, Verbose=2):
    '''
    Random Forest Table Classification (ViGrA)
    ----------
    [imagery_vigra.11]\n
    Random Forest Table Classification.\n
    Arguments
    ----------
    - TABLE [`input table`] : Table. Table with features, must include class-ID
    - IMPORTANCES [`output table`] : Feature Importances
    - FEATURES [`table fields`] : Features. Select features (table fields) for classification
    - PREDICTION [`table field`] : Prediction. This is field that will have the prediction results. If not set it will be added to the table.
    - TRAINING [`table field`] : Training. this is the table field that defines the training classes
    - LABEL_AS_ID [`boolean`] : Use Label as Identifier. Default: 0 Use training area labels as identifier in classification result, assumes all label values are integer numbers!
    - RF_TREE_COUNT [`integer number`] : Tree Count. Minimum: 1 Default: 32 How many trees to create?
    - RF_TREE_SAMPLES [`floating point number`] : Samples per Tree. Minimum: 0.000000 Maximum: 1.000000 Default: 1.000000 Specifies the fraction of the total number of samples used per tree for learning.
    - RF_REPLACE [`boolean`] : Sample with Replacement. Default: 1 Sample from training population with or without replacement?
    - RF_SPLIT_MIN_SIZE [`integer number`] : Minimum Node Split Size. Minimum: 1 Default: 1 Number of examples required for a node to be split. Choose 1 for complete growing.
    - RF_NODE_FEATURES [`choice`] : Features per Node. Available Choices: [0] logarithmic [1] square root [2] all Default: 1
    - RF_STRATIFICATION [`choice`] : Stratification. Available Choices: [0] none [1] equal [2] proportional Default: 0 Specifies stratification strategy. Either none, equal amount of class samples, or proportional to fraction of class samples.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('imagery_vigra', '11', 'Random Forest Table Classification (ViGrA)')
    if Tool.is_Okay():
        Tool.Set_Input ('TABLE', TABLE)
        Tool.Set_Output('IMPORTANCES', IMPORTANCES)
        Tool.Set_Option('FEATURES', FEATURES)
        Tool.Set_Option('PREDICTION', PREDICTION)
        Tool.Set_Option('TRAINING', TRAINING)
        Tool.Set_Option('LABEL_AS_ID', LABEL_AS_ID)
        Tool.Set_Option('RF_TREE_COUNT', RF_TREE_COUNT)
        Tool.Set_Option('RF_TREE_SAMPLES', RF_TREE_SAMPLES)
        Tool.Set_Option('RF_REPLACE', RF_REPLACE)
        Tool.Set_Option('RF_SPLIT_MIN_SIZE', RF_SPLIT_MIN_SIZE)
        Tool.Set_Option('RF_NODE_FEATURES', RF_NODE_FEATURES)
        Tool.Set_Option('RF_STRATIFICATION', RF_STRATIFICATION)
        return Tool.Execute(Verbose)
    return False

def run_tool_imagery_vigra_11(TABLE=None, IMPORTANCES=None, FEATURES=None, PREDICTION=None, TRAINING=None, LABEL_AS_ID=None, RF_TREE_COUNT=None, RF_TREE_SAMPLES=None, RF_REPLACE=None, RF_SPLIT_MIN_SIZE=None, RF_NODE_FEATURES=None, RF_STRATIFICATION=None, Verbose=2):
    '''
    Random Forest Table Classification (ViGrA)
    ----------
    [imagery_vigra.11]\n
    Random Forest Table Classification.\n
    Arguments
    ----------
    - TABLE [`input table`] : Table. Table with features, must include class-ID
    - IMPORTANCES [`output table`] : Feature Importances
    - FEATURES [`table fields`] : Features. Select features (table fields) for classification
    - PREDICTION [`table field`] : Prediction. This is field that will have the prediction results. If not set it will be added to the table.
    - TRAINING [`table field`] : Training. this is the table field that defines the training classes
    - LABEL_AS_ID [`boolean`] : Use Label as Identifier. Default: 0 Use training area labels as identifier in classification result, assumes all label values are integer numbers!
    - RF_TREE_COUNT [`integer number`] : Tree Count. Minimum: 1 Default: 32 How many trees to create?
    - RF_TREE_SAMPLES [`floating point number`] : Samples per Tree. Minimum: 0.000000 Maximum: 1.000000 Default: 1.000000 Specifies the fraction of the total number of samples used per tree for learning.
    - RF_REPLACE [`boolean`] : Sample with Replacement. Default: 1 Sample from training population with or without replacement?
    - RF_SPLIT_MIN_SIZE [`integer number`] : Minimum Node Split Size. Minimum: 1 Default: 1 Number of examples required for a node to be split. Choose 1 for complete growing.
    - RF_NODE_FEATURES [`choice`] : Features per Node. Available Choices: [0] logarithmic [1] square root [2] all Default: 1
    - RF_STRATIFICATION [`choice`] : Stratification. Available Choices: [0] none [1] equal [2] proportional Default: 0 Specifies stratification strategy. Either none, equal amount of class samples, or proportional to fraction of class samples.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('imagery_vigra', '11', 'Random Forest Table Classification (ViGrA)')
    if Tool.is_Okay():
        Tool.Set_Input ('TABLE', TABLE)
        Tool.Set_Output('IMPORTANCES', IMPORTANCES)
        Tool.Set_Option('FEATURES', FEATURES)
        Tool.Set_Option('PREDICTION', PREDICTION)
        Tool.Set_Option('TRAINING', TRAINING)
        Tool.Set_Option('LABEL_AS_ID', LABEL_AS_ID)
        Tool.Set_Option('RF_TREE_COUNT', RF_TREE_COUNT)
        Tool.Set_Option('RF_TREE_SAMPLES', RF_TREE_SAMPLES)
        Tool.Set_Option('RF_REPLACE', RF_REPLACE)
        Tool.Set_Option('RF_SPLIT_MIN_SIZE', RF_SPLIT_MIN_SIZE)
        Tool.Set_Option('RF_NODE_FEATURES', RF_NODE_FEATURES)
        Tool.Set_Option('RF_STRATIFICATION', RF_STRATIFICATION)
        return Tool.Execute(Verbose)
    return False

