#! /usr/bin/env python

'''
Python Interface to SAGA Tools Library
----------
- Category : Import/Export
- Name     : Virtual
- ID       : io_virtual

Description
----------
Tools for the handling of virtual datasets.
'''

from PySAGA.helper import Tool_Wrapper

def Create_Virtual_Point_Cloud_Dataset(FILES=None, INPUT_FILE_LIST=None, FILENAME=None, METHOD_PATHS=None, USE_HEADER=None, Verbose=2):
    '''
    Create Virtual Point Cloud Dataset
    ----------
    [io_virtual.0]\n
    The tool allows one to create a virtual point cloud dataset from a set of SAGA point cloud files. For a large number of files, it is advised to use an input file list, i.e. a text file with the full path to an input point cloud on each line. If possible, you should make use of the point cloud headers files to construct the virtual dataset. This avoids that each dataset has to be loaded and thus reduces execution time enormously.\n
    A virtual point cloud dataset is a simple XML format with the file extension .spcvf, which describes a mosaic of individual point cloud files. Such a virtual point cloud dataset can be used for seamless data access with the 'Get Subset from Virtual Point Cloud' tool.\n
    All point cloud input datasets must share the same attribute table structure, NoData value and projection.\n
    Arguments
    ----------
    - FILES [`file path`] : Input Files. The input point cloud files to use
    - INPUT_FILE_LIST [`file path`] : Input File List. A text file with the full path to an input point cloud on each line
    - FILENAME [`file path`] : Filename. The full path and name of the .spcvf file
    - METHOD_PATHS [`choice`] : File Paths. Available Choices: [0] absolute [1] relative Default: 1 Choose how to handle file paths. With relative paths, you can package the *.spcvf and your point cloud tiles easily.
    - USE_HEADER [`boolean`] : Use Header File. Default: 0 Check this parameter to use (only) the point cloud header file to construct the virtual dataset.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('io_virtual', '0', 'Create Virtual Point Cloud Dataset')
    if Tool.is_Okay():
        Tool.Set_Option('FILES', FILES)
        Tool.Set_Option('INPUT_FILE_LIST', INPUT_FILE_LIST)
        Tool.Set_Option('FILENAME', FILENAME)
        Tool.Set_Option('METHOD_PATHS', METHOD_PATHS)
        Tool.Set_Option('USE_HEADER', USE_HEADER)
        return Tool.Execute(Verbose)
    return False

def run_tool_io_virtual_0(FILES=None, INPUT_FILE_LIST=None, FILENAME=None, METHOD_PATHS=None, USE_HEADER=None, Verbose=2):
    '''
    Create Virtual Point Cloud Dataset
    ----------
    [io_virtual.0]\n
    The tool allows one to create a virtual point cloud dataset from a set of SAGA point cloud files. For a large number of files, it is advised to use an input file list, i.e. a text file with the full path to an input point cloud on each line. If possible, you should make use of the point cloud headers files to construct the virtual dataset. This avoids that each dataset has to be loaded and thus reduces execution time enormously.\n
    A virtual point cloud dataset is a simple XML format with the file extension .spcvf, which describes a mosaic of individual point cloud files. Such a virtual point cloud dataset can be used for seamless data access with the 'Get Subset from Virtual Point Cloud' tool.\n
    All point cloud input datasets must share the same attribute table structure, NoData value and projection.\n
    Arguments
    ----------
    - FILES [`file path`] : Input Files. The input point cloud files to use
    - INPUT_FILE_LIST [`file path`] : Input File List. A text file with the full path to an input point cloud on each line
    - FILENAME [`file path`] : Filename. The full path and name of the .spcvf file
    - METHOD_PATHS [`choice`] : File Paths. Available Choices: [0] absolute [1] relative Default: 1 Choose how to handle file paths. With relative paths, you can package the *.spcvf and your point cloud tiles easily.
    - USE_HEADER [`boolean`] : Use Header File. Default: 0 Check this parameter to use (only) the point cloud header file to construct the virtual dataset.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('io_virtual', '0', 'Create Virtual Point Cloud Dataset')
    if Tool.is_Okay():
        Tool.Set_Option('FILES', FILES)
        Tool.Set_Option('INPUT_FILE_LIST', INPUT_FILE_LIST)
        Tool.Set_Option('FILENAME', FILENAME)
        Tool.Set_Option('METHOD_PATHS', METHOD_PATHS)
        Tool.Set_Option('USE_HEADER', USE_HEADER)
        return Tool.Execute(Verbose)
    return False

def Get_Subset_from_Virtual_Point_Cloud(AOI_SHP=None, AOI_GRID=None, PC_OUT=None, FILENAME=None, FILEPATH=None, COPY_ATTR=None, ATTRIBUTE_LIST=None, CONSTRAIN_QUERY=None, ATTR_FIELD=None, VALUE_RANGE=None, FIELD_TILENAME=None, AOI_GRID_GRIDSYSTEM=None, AOI_XRANGE=None, AOI_YRANGE=None, AOI_ADD_OVERLAP=None, OVERLAP=None, FILENAME_TILE_INFO=None, ONE_PC_PER_POLYGON=None, Verbose=2):
    '''
    Get Subset from Virtual Point Cloud
    ----------
    [io_virtual.1]\n
    The tool allows one to retrieve a point cloud from a virtual point cloud dataset by applying the provided area-of-interest (AOI). The extent of the AOI can be provided either as polygon shapefile, grid or by coordinates. Optionally, an overlap can be added to the AOI and a spcvf tile info file can be outputted. The latter can be used to remove the overlap later.\n
    In case an overlap is used and the AOI is provided as polygon shapfile, only the bounding boxes of the polygons are used.\n
    With polygon shapefiles additional functionality is available:\n
    * in case one or more polygons are selected, only the selected polygons are used.\n
    * in case the shapefile contains several polygons and the 'One Point Cloud per Polygon' parameter is checked, a point cloud dataset is outputted for each polygon. In case the 'Tilename' attribute is provided, the output files are named by this attribute. Otherwise the output file names are build from the lower left coordinate of each tile.\n
    The derived datasets can be outputted either as point cloud list or written to an output directory. For the latter, you must provide a valid file path with the 'Optional Output Filepath' parameter.\n
    Optionally, the query can be constrained by providing an attribute field and a value range that must be met.\n
    A virtual point cloud dataset is a simple XML format with the file extension .spcvf, which can be created with the 'Create Virtual Point Cloud Dataset' tool.\n
    Arguments
    ----------
    - AOI_SHP [`optional input shapes`] : Shape. Shapefile describing the AOI.
    - AOI_GRID [`optional input grid`] : Grid. Grid describing the AOI.
    - PC_OUT [`output point cloud list`] : Point Cloud. The output point cloud(s)
    - FILENAME [`file path`] : Filename. The full path and name of the .spcvf file
    - FILEPATH [`file path`] : Optional Output Filepath. The full path to which the output(s) should be written. Leave empty to output the datasets as point cloud list.
    - COPY_ATTR [`boolean`] : Copy existing Attributes. Default: 1 Copy attributes from input to output point cloud.
    - ATTRIBUTE_LIST [`text`] : Copy Attributes. Default: 1;2;3 Field numbers (starting from 1) of the attributes to copy, separated by semicolon; fields one to three (x;y;z) are mandatory.
    - CONSTRAIN_QUERY [`boolean`] : Constrain Query. Default: 0 Check this parameter to constrain the query by an attribute range.
    - ATTR_FIELD [`integer number`] : Attribute Field. Minimum: 1 Default: 1 The attribute field to use as constraint. Field numbers start with 1.
    - VALUE_RANGE [`value range`] : Value Range. Minimum and maximum of attribute range [].
    - FIELD_TILENAME [`table field`] : Tilename. Attribute used for naming the output file(s)
    - AOI_GRID_GRIDSYSTEM [`grid system`] : Grid system
    - AOI_XRANGE [`value range`] : X-Extent. Minimum and maximum x-coordinate of AOI.
    - AOI_YRANGE [`value range`] : Y-Extent. Minimum and maximum y-coordinate of AOI.
    - AOI_ADD_OVERLAP [`boolean`] : Add Overlap. Default: 0 Add overlap to AOI
    - OVERLAP [`floating point number`] : Overlap. Minimum: 0.000000 Default: 50.000000 Overlap [map units]
    - FILENAME_TILE_INFO [`file path`] : Optional Tile Info Filename. The full path and name of an optional spcvf tile info file. Such a file contains information about the bounding boxes without overlap and can be used to remove the overlap from the tiles later. Leave empty to not output such a file.
    - ONE_PC_PER_POLYGON [`boolean`] : One Point Cloud per Polygon. Default: 0 Write one point cloud dataset for each polygon

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('io_virtual', '1', 'Get Subset from Virtual Point Cloud')
    if Tool.is_Okay():
        Tool.Set_Input ('AOI_SHP', AOI_SHP)
        Tool.Set_Input ('AOI_GRID', AOI_GRID)
        Tool.Set_Output('PC_OUT', PC_OUT)
        Tool.Set_Option('FILENAME', FILENAME)
        Tool.Set_Option('FILEPATH', FILEPATH)
        Tool.Set_Option('COPY_ATTR', COPY_ATTR)
        Tool.Set_Option('ATTRIBUTE_LIST', ATTRIBUTE_LIST)
        Tool.Set_Option('CONSTRAIN_QUERY', CONSTRAIN_QUERY)
        Tool.Set_Option('ATTR_FIELD', ATTR_FIELD)
        Tool.Set_Option('VALUE_RANGE', VALUE_RANGE)
        Tool.Set_Option('FIELD_TILENAME', FIELD_TILENAME)
        Tool.Set_Option('AOI_GRID_GRIDSYSTEM', AOI_GRID_GRIDSYSTEM)
        Tool.Set_Option('AOI_XRANGE', AOI_XRANGE)
        Tool.Set_Option('AOI_YRANGE', AOI_YRANGE)
        Tool.Set_Option('AOI_ADD_OVERLAP', AOI_ADD_OVERLAP)
        Tool.Set_Option('OVERLAP', OVERLAP)
        Tool.Set_Option('FILENAME_TILE_INFO', FILENAME_TILE_INFO)
        Tool.Set_Option('ONE_PC_PER_POLYGON', ONE_PC_PER_POLYGON)
        return Tool.Execute(Verbose)
    return False

def run_tool_io_virtual_1(AOI_SHP=None, AOI_GRID=None, PC_OUT=None, FILENAME=None, FILEPATH=None, COPY_ATTR=None, ATTRIBUTE_LIST=None, CONSTRAIN_QUERY=None, ATTR_FIELD=None, VALUE_RANGE=None, FIELD_TILENAME=None, AOI_GRID_GRIDSYSTEM=None, AOI_XRANGE=None, AOI_YRANGE=None, AOI_ADD_OVERLAP=None, OVERLAP=None, FILENAME_TILE_INFO=None, ONE_PC_PER_POLYGON=None, Verbose=2):
    '''
    Get Subset from Virtual Point Cloud
    ----------
    [io_virtual.1]\n
    The tool allows one to retrieve a point cloud from a virtual point cloud dataset by applying the provided area-of-interest (AOI). The extent of the AOI can be provided either as polygon shapefile, grid or by coordinates. Optionally, an overlap can be added to the AOI and a spcvf tile info file can be outputted. The latter can be used to remove the overlap later.\n
    In case an overlap is used and the AOI is provided as polygon shapfile, only the bounding boxes of the polygons are used.\n
    With polygon shapefiles additional functionality is available:\n
    * in case one or more polygons are selected, only the selected polygons are used.\n
    * in case the shapefile contains several polygons and the 'One Point Cloud per Polygon' parameter is checked, a point cloud dataset is outputted for each polygon. In case the 'Tilename' attribute is provided, the output files are named by this attribute. Otherwise the output file names are build from the lower left coordinate of each tile.\n
    The derived datasets can be outputted either as point cloud list or written to an output directory. For the latter, you must provide a valid file path with the 'Optional Output Filepath' parameter.\n
    Optionally, the query can be constrained by providing an attribute field and a value range that must be met.\n
    A virtual point cloud dataset is a simple XML format with the file extension .spcvf, which can be created with the 'Create Virtual Point Cloud Dataset' tool.\n
    Arguments
    ----------
    - AOI_SHP [`optional input shapes`] : Shape. Shapefile describing the AOI.
    - AOI_GRID [`optional input grid`] : Grid. Grid describing the AOI.
    - PC_OUT [`output point cloud list`] : Point Cloud. The output point cloud(s)
    - FILENAME [`file path`] : Filename. The full path and name of the .spcvf file
    - FILEPATH [`file path`] : Optional Output Filepath. The full path to which the output(s) should be written. Leave empty to output the datasets as point cloud list.
    - COPY_ATTR [`boolean`] : Copy existing Attributes. Default: 1 Copy attributes from input to output point cloud.
    - ATTRIBUTE_LIST [`text`] : Copy Attributes. Default: 1;2;3 Field numbers (starting from 1) of the attributes to copy, separated by semicolon; fields one to three (x;y;z) are mandatory.
    - CONSTRAIN_QUERY [`boolean`] : Constrain Query. Default: 0 Check this parameter to constrain the query by an attribute range.
    - ATTR_FIELD [`integer number`] : Attribute Field. Minimum: 1 Default: 1 The attribute field to use as constraint. Field numbers start with 1.
    - VALUE_RANGE [`value range`] : Value Range. Minimum and maximum of attribute range [].
    - FIELD_TILENAME [`table field`] : Tilename. Attribute used for naming the output file(s)
    - AOI_GRID_GRIDSYSTEM [`grid system`] : Grid system
    - AOI_XRANGE [`value range`] : X-Extent. Minimum and maximum x-coordinate of AOI.
    - AOI_YRANGE [`value range`] : Y-Extent. Minimum and maximum y-coordinate of AOI.
    - AOI_ADD_OVERLAP [`boolean`] : Add Overlap. Default: 0 Add overlap to AOI
    - OVERLAP [`floating point number`] : Overlap. Minimum: 0.000000 Default: 50.000000 Overlap [map units]
    - FILENAME_TILE_INFO [`file path`] : Optional Tile Info Filename. The full path and name of an optional spcvf tile info file. Such a file contains information about the bounding boxes without overlap and can be used to remove the overlap from the tiles later. Leave empty to not output such a file.
    - ONE_PC_PER_POLYGON [`boolean`] : One Point Cloud per Polygon. Default: 0 Write one point cloud dataset for each polygon

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('io_virtual', '1', 'Get Subset from Virtual Point Cloud')
    if Tool.is_Okay():
        Tool.Set_Input ('AOI_SHP', AOI_SHP)
        Tool.Set_Input ('AOI_GRID', AOI_GRID)
        Tool.Set_Output('PC_OUT', PC_OUT)
        Tool.Set_Option('FILENAME', FILENAME)
        Tool.Set_Option('FILEPATH', FILEPATH)
        Tool.Set_Option('COPY_ATTR', COPY_ATTR)
        Tool.Set_Option('ATTRIBUTE_LIST', ATTRIBUTE_LIST)
        Tool.Set_Option('CONSTRAIN_QUERY', CONSTRAIN_QUERY)
        Tool.Set_Option('ATTR_FIELD', ATTR_FIELD)
        Tool.Set_Option('VALUE_RANGE', VALUE_RANGE)
        Tool.Set_Option('FIELD_TILENAME', FIELD_TILENAME)
        Tool.Set_Option('AOI_GRID_GRIDSYSTEM', AOI_GRID_GRIDSYSTEM)
        Tool.Set_Option('AOI_XRANGE', AOI_XRANGE)
        Tool.Set_Option('AOI_YRANGE', AOI_YRANGE)
        Tool.Set_Option('AOI_ADD_OVERLAP', AOI_ADD_OVERLAP)
        Tool.Set_Option('OVERLAP', OVERLAP)
        Tool.Set_Option('FILENAME_TILE_INFO', FILENAME_TILE_INFO)
        Tool.Set_Option('ONE_PC_PER_POLYGON', ONE_PC_PER_POLYGON)
        return Tool.Execute(Verbose)
    return False

def Create_Tileshape_from_Virtual_Point_Cloud(TILE_SHP=None, FILENAME=None, Verbose=2):
    '''
    Create Tileshape from Virtual Point Cloud
    ----------
    [io_virtual.2]\n
    The tool allows one to create a polygon shapefile with the bounding boxes of a virtual point cloud dataset. Additionally, the header information of the chosen virtual point cloud dataset is reported (since SPCVFDataset version 1.1).\n
    A virtual point cloud dataset is a simple XML format with the file extension .spcvf, which can be created with the 'Create Virtual Point Cloud Dataset' tool.\n
    Arguments
    ----------
    - TILE_SHP [`output shapes`] : Tileshape. Polygon shapefile describing the bounding boxes of spcvf tiles.
    - FILENAME [`file path`] : Filename. The full path and name of the .spcvf file

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('io_virtual', '2', 'Create Tileshape from Virtual Point Cloud')
    if Tool.is_Okay():
        Tool.Set_Output('TILE_SHP', TILE_SHP)
        Tool.Set_Option('FILENAME', FILENAME)
        return Tool.Execute(Verbose)
    return False

def run_tool_io_virtual_2(TILE_SHP=None, FILENAME=None, Verbose=2):
    '''
    Create Tileshape from Virtual Point Cloud
    ----------
    [io_virtual.2]\n
    The tool allows one to create a polygon shapefile with the bounding boxes of a virtual point cloud dataset. Additionally, the header information of the chosen virtual point cloud dataset is reported (since SPCVFDataset version 1.1).\n
    A virtual point cloud dataset is a simple XML format with the file extension .spcvf, which can be created with the 'Create Virtual Point Cloud Dataset' tool.\n
    Arguments
    ----------
    - TILE_SHP [`output shapes`] : Tileshape. Polygon shapefile describing the bounding boxes of spcvf tiles.
    - FILENAME [`file path`] : Filename. The full path and name of the .spcvf file

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('io_virtual', '2', 'Create Tileshape from Virtual Point Cloud')
    if Tool.is_Okay():
        Tool.Set_Output('TILE_SHP', TILE_SHP)
        Tool.Set_Option('FILENAME', FILENAME)
        return Tool.Execute(Verbose)
    return False

def Get_Grid_from_Virtual_Point_Cloud(AOI_SHP=None, AOI_GRID=None, GRID_OUT=None, FILENAME=None, FILEPATH=None, ATTR_FIELD_GRID=None, CELL_SIZE=None, GRID_SYSTEM_FIT=None, METHOD=None, CONSTRAIN_QUERY=None, ATTR_FIELD=None, VALUE_RANGE=None, FIELD_TILENAME=None, AOI_GRID_GRIDSYSTEM=None, AOI_XRANGE=None, AOI_YRANGE=None, AOI_ADD_OVERLAP=None, OVERLAP=None, Verbose=2):
    '''
    Get Grid from Virtual Point Cloud
    ----------
    [io_virtual.4]\n
    The tool allows one to retrieve a grid from a virtual point cloud dataset by applying the provided area-of-interest (AOI). The extent of the AOI can be provided either as polygon shapefile, grid or by coordinates. Optionally, an overlap can be added to the AOI. In case an overlap is used and the AOI is provided as polygon shapfile, only the bounding boxes of the polygons are used.\n
    With polygon shapefiles additional functionality is available:\n
    * in case one or more polygons are selected, only the selected polygons are used.\n
    * in case the shapefile contains several polygons a grid dataset is outputted for each polygon. In case the 'Tilename' attribute is provided, the output files are named by this attribute. Otherwise the output file names are build from the lower left coordinate of each tile.\n
    The derived datasets can be outputted either as grid list or written to an output directory. For the latter, you must provide a valid file path with the 'Optional Output Filepath' parameter.\n
    Optionally, the query can be constrained by providing an attribute field and a value range that must be met.\n
    A virtual point cloud dataset is a simple XML format with the file extension .spcvf, which can be created with the 'Create Virtual Point Cloud Dataset' tool.\n
    Arguments
    ----------
    - AOI_SHP [`optional input shapes`] : Shape. Shapefile describing the AOI.
    - AOI_GRID [`optional input grid`] : Grid. Grid describing the AOI.
    - GRID_OUT [`output grid list`] : Grid. The output grid(s)
    - FILENAME [`file path`] : Filename. The full path and name of the .spcvf file
    - FILEPATH [`file path`] : Optional Output Filepath. The full path to which the output(s) should be written. Leave empty to output the datasets as grid list.
    - ATTR_FIELD_GRID [`integer number`] : Attribute Field to Grid. Minimum: 3 Default: 1 The attribute field to grid. Field numbers start with 1, so elevation is attribute field 3.
    - CELL_SIZE [`floating point number`] : Cellsize. Minimum: 0.001000 Default: 1.000000 Cellsize of the output grid [map units]
    - GRID_SYSTEM_FIT [`choice`] : Grid System Fit. Available Choices: [0] nodes [1] cells Default: 1 Choose how to align the output grid system to the AOI
    - METHOD [`choice`] : Aggregation. Available Choices: [0] lowest [1] highest Default: 1 Choose how to aggregate the values
    - CONSTRAIN_QUERY [`boolean`] : Constrain Query. Default: 0 Check this parameter to constrain the query by an attribute range.
    - ATTR_FIELD [`integer number`] : Attribute Field. Minimum: 1 Default: 1 The attribute field to use as constraint. Field numbers start with 1.
    - VALUE_RANGE [`value range`] : Value Range. Minimum and maximum of attribute range [].
    - FIELD_TILENAME [`table field`] : Tilename. Attribute used for naming the output file(s)
    - AOI_GRID_GRIDSYSTEM [`grid system`] : Grid system
    - AOI_XRANGE [`value range`] : X-Extent. Minimum and maximum x-coordinate of AOI.
    - AOI_YRANGE [`value range`] : Y-Extent. Minimum and maximum y-coordinate of AOI.
    - AOI_ADD_OVERLAP [`boolean`] : Add Overlap. Default: 0 Add overlap to AOI
    - OVERLAP [`floating point number`] : Overlap. Minimum: 0.000000 Default: 50.000000 Overlap [map units]

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('io_virtual', '4', 'Get Grid from Virtual Point Cloud')
    if Tool.is_Okay():
        Tool.Set_Input ('AOI_SHP', AOI_SHP)
        Tool.Set_Input ('AOI_GRID', AOI_GRID)
        Tool.Set_Output('GRID_OUT', GRID_OUT)
        Tool.Set_Option('FILENAME', FILENAME)
        Tool.Set_Option('FILEPATH', FILEPATH)
        Tool.Set_Option('ATTR_FIELD_GRID', ATTR_FIELD_GRID)
        Tool.Set_Option('CELL_SIZE', CELL_SIZE)
        Tool.Set_Option('GRID_SYSTEM_FIT', GRID_SYSTEM_FIT)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('CONSTRAIN_QUERY', CONSTRAIN_QUERY)
        Tool.Set_Option('ATTR_FIELD', ATTR_FIELD)
        Tool.Set_Option('VALUE_RANGE', VALUE_RANGE)
        Tool.Set_Option('FIELD_TILENAME', FIELD_TILENAME)
        Tool.Set_Option('AOI_GRID_GRIDSYSTEM', AOI_GRID_GRIDSYSTEM)
        Tool.Set_Option('AOI_XRANGE', AOI_XRANGE)
        Tool.Set_Option('AOI_YRANGE', AOI_YRANGE)
        Tool.Set_Option('AOI_ADD_OVERLAP', AOI_ADD_OVERLAP)
        Tool.Set_Option('OVERLAP', OVERLAP)
        return Tool.Execute(Verbose)
    return False

def run_tool_io_virtual_4(AOI_SHP=None, AOI_GRID=None, GRID_OUT=None, FILENAME=None, FILEPATH=None, ATTR_FIELD_GRID=None, CELL_SIZE=None, GRID_SYSTEM_FIT=None, METHOD=None, CONSTRAIN_QUERY=None, ATTR_FIELD=None, VALUE_RANGE=None, FIELD_TILENAME=None, AOI_GRID_GRIDSYSTEM=None, AOI_XRANGE=None, AOI_YRANGE=None, AOI_ADD_OVERLAP=None, OVERLAP=None, Verbose=2):
    '''
    Get Grid from Virtual Point Cloud
    ----------
    [io_virtual.4]\n
    The tool allows one to retrieve a grid from a virtual point cloud dataset by applying the provided area-of-interest (AOI). The extent of the AOI can be provided either as polygon shapefile, grid or by coordinates. Optionally, an overlap can be added to the AOI. In case an overlap is used and the AOI is provided as polygon shapfile, only the bounding boxes of the polygons are used.\n
    With polygon shapefiles additional functionality is available:\n
    * in case one or more polygons are selected, only the selected polygons are used.\n
    * in case the shapefile contains several polygons a grid dataset is outputted for each polygon. In case the 'Tilename' attribute is provided, the output files are named by this attribute. Otherwise the output file names are build from the lower left coordinate of each tile.\n
    The derived datasets can be outputted either as grid list or written to an output directory. For the latter, you must provide a valid file path with the 'Optional Output Filepath' parameter.\n
    Optionally, the query can be constrained by providing an attribute field and a value range that must be met.\n
    A virtual point cloud dataset is a simple XML format with the file extension .spcvf, which can be created with the 'Create Virtual Point Cloud Dataset' tool.\n
    Arguments
    ----------
    - AOI_SHP [`optional input shapes`] : Shape. Shapefile describing the AOI.
    - AOI_GRID [`optional input grid`] : Grid. Grid describing the AOI.
    - GRID_OUT [`output grid list`] : Grid. The output grid(s)
    - FILENAME [`file path`] : Filename. The full path and name of the .spcvf file
    - FILEPATH [`file path`] : Optional Output Filepath. The full path to which the output(s) should be written. Leave empty to output the datasets as grid list.
    - ATTR_FIELD_GRID [`integer number`] : Attribute Field to Grid. Minimum: 3 Default: 1 The attribute field to grid. Field numbers start with 1, so elevation is attribute field 3.
    - CELL_SIZE [`floating point number`] : Cellsize. Minimum: 0.001000 Default: 1.000000 Cellsize of the output grid [map units]
    - GRID_SYSTEM_FIT [`choice`] : Grid System Fit. Available Choices: [0] nodes [1] cells Default: 1 Choose how to align the output grid system to the AOI
    - METHOD [`choice`] : Aggregation. Available Choices: [0] lowest [1] highest Default: 1 Choose how to aggregate the values
    - CONSTRAIN_QUERY [`boolean`] : Constrain Query. Default: 0 Check this parameter to constrain the query by an attribute range.
    - ATTR_FIELD [`integer number`] : Attribute Field. Minimum: 1 Default: 1 The attribute field to use as constraint. Field numbers start with 1.
    - VALUE_RANGE [`value range`] : Value Range. Minimum and maximum of attribute range [].
    - FIELD_TILENAME [`table field`] : Tilename. Attribute used for naming the output file(s)
    - AOI_GRID_GRIDSYSTEM [`grid system`] : Grid system
    - AOI_XRANGE [`value range`] : X-Extent. Minimum and maximum x-coordinate of AOI.
    - AOI_YRANGE [`value range`] : Y-Extent. Minimum and maximum y-coordinate of AOI.
    - AOI_ADD_OVERLAP [`boolean`] : Add Overlap. Default: 0 Add overlap to AOI
    - OVERLAP [`floating point number`] : Overlap. Minimum: 0.000000 Default: 50.000000 Overlap [map units]

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('io_virtual', '4', 'Get Grid from Virtual Point Cloud')
    if Tool.is_Okay():
        Tool.Set_Input ('AOI_SHP', AOI_SHP)
        Tool.Set_Input ('AOI_GRID', AOI_GRID)
        Tool.Set_Output('GRID_OUT', GRID_OUT)
        Tool.Set_Option('FILENAME', FILENAME)
        Tool.Set_Option('FILEPATH', FILEPATH)
        Tool.Set_Option('ATTR_FIELD_GRID', ATTR_FIELD_GRID)
        Tool.Set_Option('CELL_SIZE', CELL_SIZE)
        Tool.Set_Option('GRID_SYSTEM_FIT', GRID_SYSTEM_FIT)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('CONSTRAIN_QUERY', CONSTRAIN_QUERY)
        Tool.Set_Option('ATTR_FIELD', ATTR_FIELD)
        Tool.Set_Option('VALUE_RANGE', VALUE_RANGE)
        Tool.Set_Option('FIELD_TILENAME', FIELD_TILENAME)
        Tool.Set_Option('AOI_GRID_GRIDSYSTEM', AOI_GRID_GRIDSYSTEM)
        Tool.Set_Option('AOI_XRANGE', AOI_XRANGE)
        Tool.Set_Option('AOI_YRANGE', AOI_YRANGE)
        Tool.Set_Option('AOI_ADD_OVERLAP', AOI_ADD_OVERLAP)
        Tool.Set_Option('OVERLAP', OVERLAP)
        return Tool.Execute(Verbose)
    return False

def Remove_Overlap_from_Virtual_Point_Cloud_Tiles(FILENAME=None, FILEPATH=None, Verbose=2):
    '''
    Remove Overlap from Virtual Point Cloud Tiles
    ----------
    [io_virtual.6]\n
    The tool allows one to remove the overlap from point cloud tiles created from a virtual point cloud dataset. The tiles must have been created with an overlap and a spcvf tile info file must have been outputted too. The latter describes the original bounding boxes of the tiles (i.e. without overlap) and is used by this tool to remove the overlap.\n
    A virtual point cloud dataset is a simple XML format with the file extension .spcvf, which can be created with the 'Create Virtual Point Cloud Dataset' tool. Point cloud tiles with an overlap are usually created from such an virtual point cloud dataset with the 'Get Subset from Virtual Point Cloud' tool.\n
    Arguments
    ----------
    - FILENAME [`file path`] : Tile Info File. The full path and name of the spcvf tile info file describing the point cloud tiles without overlap
    - FILEPATH [`file path`] : Output Filepath. The full path to which the point cloud tiles without overlap should be written.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('io_virtual', '6', 'Remove Overlap from Virtual Point Cloud Tiles')
    if Tool.is_Okay():
        Tool.Set_Option('FILENAME', FILENAME)
        Tool.Set_Option('FILEPATH', FILEPATH)
        return Tool.Execute(Verbose)
    return False

def run_tool_io_virtual_6(FILENAME=None, FILEPATH=None, Verbose=2):
    '''
    Remove Overlap from Virtual Point Cloud Tiles
    ----------
    [io_virtual.6]\n
    The tool allows one to remove the overlap from point cloud tiles created from a virtual point cloud dataset. The tiles must have been created with an overlap and a spcvf tile info file must have been outputted too. The latter describes the original bounding boxes of the tiles (i.e. without overlap) and is used by this tool to remove the overlap.\n
    A virtual point cloud dataset is a simple XML format with the file extension .spcvf, which can be created with the 'Create Virtual Point Cloud Dataset' tool. Point cloud tiles with an overlap are usually created from such an virtual point cloud dataset with the 'Get Subset from Virtual Point Cloud' tool.\n
    Arguments
    ----------
    - FILENAME [`file path`] : Tile Info File. The full path and name of the spcvf tile info file describing the point cloud tiles without overlap
    - FILEPATH [`file path`] : Output Filepath. The full path to which the point cloud tiles without overlap should be written.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('io_virtual', '6', 'Remove Overlap from Virtual Point Cloud Tiles')
    if Tool.is_Okay():
        Tool.Set_Option('FILENAME', FILENAME)
        Tool.Set_Option('FILEPATH', FILEPATH)
        return Tool.Execute(Verbose)
    return False

