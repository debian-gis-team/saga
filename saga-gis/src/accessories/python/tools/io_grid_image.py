#! /usr/bin/env python

'''
Python Interface to SAGA Tools Library
----------
- Category : Import/Export
- Name     : Images
- ID       : io_grid_image

Description
----------
Image Import/Export.
'''

from PySAGA.helper import Tool_Wrapper

def Export_to_Image_File(GRID=None, SHADE=None, FILE=None, FILE_WORLD=None, FILE_KML=None, NO_DATA=None, NO_DATA_COL=None, COLOURING=None, COL_COUNT=None, COL_REVERT=None, COL_PALETTE=None, COL_DEPTH=None, GRADUATED=None, STDDEV=None, LINEAR=None, STRETCH=None, SCALE_MODE=None, SCALE_LOG=None, LUT=None, SHADE_TRANS=None, SHADE_COLOURING=None, SHADE_BRIGHT=None, SHADE_STDDEV=None, Verbose=2):
    '''
    Export to Image File
    ----------
    [io_grid_image.0]\n
    With this tool you can save a RGB to an image file. Optionally, a shade RGB can be overlaid using the specified transparency and brightness adjustment.\n
    If input rasters come with geographic coordinates an additionaloption for KML file creation (Google Earth) is selectable.\n
    Arguments
    ----------
    - GRID [`input grid`] : GRID
    - SHADE [`optional input grid`] : Shade
    - FILE [`file path`] : Image File
    - FILE_WORLD [`boolean`] : Create World File. Default: 1 Store georeference along image to an additional file.
    - FILE_KML [`boolean`] : Create KML File. Default: 0 Expects that the input RGB uses geographic coordinates.
    - NO_DATA [`boolean`] : Set Transparency for No-Data. Default: 1
    - NO_DATA_COL [`color`] : No-Data Color. Default: 16777215
    - COLOURING [`choice`] : Coloring. Available Choices: [0] histogram stretch to standard deviation [1] histogram stretch to percentage range [2] histogram stretch to value range [3] lookup table [4] rgb coded values Default: 0
    - COL_COUNT [`integer number`] : Number of Colors. Default: 100
    - COL_REVERT [`boolean`] : Revert Palette. Default: 0
    - COL_PALETTE [`colors`] : Colors Palette
    - COL_DEPTH [`integer number`] : Color Depth. Minimum: 2 Maximum: 256 Default: 236 Number of color entries used when storing GIF.
    - GRADUATED [`boolean`] : Graduated Colors. Default: 1
    - STDDEV [`floating point number`] : Standard Deviation. Minimum: 0.000000 Default: 2.000000
    - LINEAR [`value range`] : Percentage Range
    - STRETCH [`value range`] : Value Range
    - SCALE_MODE [`choice`] : Scaling. Available Choices: [0] linear intervals [1] increasing geometrical intervals [2] decreasing geometrical intervals Default: 0 Scaling applied to coloring choices (i) RGB's standard deviation, (ii) RGB's value range, (iii) specified value range
    - SCALE_LOG [`floating point number`] : Geometrical Interval Factor. Minimum: 0.001000 Default: 10.000000
    - LUT [`static table`] : Lookup Table. 5 Fields: - 1. [color] Color - 2. [string] Name - 3. [string] Description - 4. [8 byte floating point number] Minimum - 5. [8 byte floating point number] Maximum 
    - SHADE_TRANS [`floating point number`] : Transparency. Minimum: 0.000000 Maximum: 100.000000 Default: 75.000000 The transparency of the shade [%]
    - SHADE_COLOURING [`choice`] : Histogram Stretch. Available Choices: [0] Linear [1] Standard Deviation Default: 0
    - SHADE_BRIGHT [`value range`] : Linear. Minimum and maximum [%], the range for histogram stretch.
    - SHADE_STDDEV [`floating point number`] : Standard Deviation. Minimum: 0.000000 Default: 2.000000

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('io_grid_image', '0', 'Export to Image File')
    if Tool.is_Okay():
        Tool.Set_Input ('GRID', GRID)
        Tool.Set_Input ('SHADE', SHADE)
        Tool.Set_Option('FILE', FILE)
        Tool.Set_Option('FILE_WORLD', FILE_WORLD)
        Tool.Set_Option('FILE_KML', FILE_KML)
        Tool.Set_Option('NO_DATA', NO_DATA)
        Tool.Set_Option('NO_DATA_COL', NO_DATA_COL)
        Tool.Set_Option('COLOURING', COLOURING)
        Tool.Set_Option('COL_COUNT', COL_COUNT)
        Tool.Set_Option('COL_REVERT', COL_REVERT)
        Tool.Set_Option('COL_PALETTE', COL_PALETTE)
        Tool.Set_Option('COL_DEPTH', COL_DEPTH)
        Tool.Set_Option('GRADUATED', GRADUATED)
        Tool.Set_Option('STDDEV', STDDEV)
        Tool.Set_Option('LINEAR', LINEAR)
        Tool.Set_Option('STRETCH', STRETCH)
        Tool.Set_Option('SCALE_MODE', SCALE_MODE)
        Tool.Set_Option('SCALE_LOG', SCALE_LOG)
        Tool.Set_Option('LUT', LUT)
        Tool.Set_Option('SHADE_TRANS', SHADE_TRANS)
        Tool.Set_Option('SHADE_COLOURING', SHADE_COLOURING)
        Tool.Set_Option('SHADE_BRIGHT', SHADE_BRIGHT)
        Tool.Set_Option('SHADE_STDDEV', SHADE_STDDEV)
        return Tool.Execute(Verbose)
    return False

def run_tool_io_grid_image_0(GRID=None, SHADE=None, FILE=None, FILE_WORLD=None, FILE_KML=None, NO_DATA=None, NO_DATA_COL=None, COLOURING=None, COL_COUNT=None, COL_REVERT=None, COL_PALETTE=None, COL_DEPTH=None, GRADUATED=None, STDDEV=None, LINEAR=None, STRETCH=None, SCALE_MODE=None, SCALE_LOG=None, LUT=None, SHADE_TRANS=None, SHADE_COLOURING=None, SHADE_BRIGHT=None, SHADE_STDDEV=None, Verbose=2):
    '''
    Export to Image File
    ----------
    [io_grid_image.0]\n
    With this tool you can save a RGB to an image file. Optionally, a shade RGB can be overlaid using the specified transparency and brightness adjustment.\n
    If input rasters come with geographic coordinates an additionaloption for KML file creation (Google Earth) is selectable.\n
    Arguments
    ----------
    - GRID [`input grid`] : GRID
    - SHADE [`optional input grid`] : Shade
    - FILE [`file path`] : Image File
    - FILE_WORLD [`boolean`] : Create World File. Default: 1 Store georeference along image to an additional file.
    - FILE_KML [`boolean`] : Create KML File. Default: 0 Expects that the input RGB uses geographic coordinates.
    - NO_DATA [`boolean`] : Set Transparency for No-Data. Default: 1
    - NO_DATA_COL [`color`] : No-Data Color. Default: 16777215
    - COLOURING [`choice`] : Coloring. Available Choices: [0] histogram stretch to standard deviation [1] histogram stretch to percentage range [2] histogram stretch to value range [3] lookup table [4] rgb coded values Default: 0
    - COL_COUNT [`integer number`] : Number of Colors. Default: 100
    - COL_REVERT [`boolean`] : Revert Palette. Default: 0
    - COL_PALETTE [`colors`] : Colors Palette
    - COL_DEPTH [`integer number`] : Color Depth. Minimum: 2 Maximum: 256 Default: 236 Number of color entries used when storing GIF.
    - GRADUATED [`boolean`] : Graduated Colors. Default: 1
    - STDDEV [`floating point number`] : Standard Deviation. Minimum: 0.000000 Default: 2.000000
    - LINEAR [`value range`] : Percentage Range
    - STRETCH [`value range`] : Value Range
    - SCALE_MODE [`choice`] : Scaling. Available Choices: [0] linear intervals [1] increasing geometrical intervals [2] decreasing geometrical intervals Default: 0 Scaling applied to coloring choices (i) RGB's standard deviation, (ii) RGB's value range, (iii) specified value range
    - SCALE_LOG [`floating point number`] : Geometrical Interval Factor. Minimum: 0.001000 Default: 10.000000
    - LUT [`static table`] : Lookup Table. 5 Fields: - 1. [color] Color - 2. [string] Name - 3. [string] Description - 4. [8 byte floating point number] Minimum - 5. [8 byte floating point number] Maximum 
    - SHADE_TRANS [`floating point number`] : Transparency. Minimum: 0.000000 Maximum: 100.000000 Default: 75.000000 The transparency of the shade [%]
    - SHADE_COLOURING [`choice`] : Histogram Stretch. Available Choices: [0] Linear [1] Standard Deviation Default: 0
    - SHADE_BRIGHT [`value range`] : Linear. Minimum and maximum [%], the range for histogram stretch.
    - SHADE_STDDEV [`floating point number`] : Standard Deviation. Minimum: 0.000000 Default: 2.000000

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('io_grid_image', '0', 'Export to Image File')
    if Tool.is_Okay():
        Tool.Set_Input ('GRID', GRID)
        Tool.Set_Input ('SHADE', SHADE)
        Tool.Set_Option('FILE', FILE)
        Tool.Set_Option('FILE_WORLD', FILE_WORLD)
        Tool.Set_Option('FILE_KML', FILE_KML)
        Tool.Set_Option('NO_DATA', NO_DATA)
        Tool.Set_Option('NO_DATA_COL', NO_DATA_COL)
        Tool.Set_Option('COLOURING', COLOURING)
        Tool.Set_Option('COL_COUNT', COL_COUNT)
        Tool.Set_Option('COL_REVERT', COL_REVERT)
        Tool.Set_Option('COL_PALETTE', COL_PALETTE)
        Tool.Set_Option('COL_DEPTH', COL_DEPTH)
        Tool.Set_Option('GRADUATED', GRADUATED)
        Tool.Set_Option('STDDEV', STDDEV)
        Tool.Set_Option('LINEAR', LINEAR)
        Tool.Set_Option('STRETCH', STRETCH)
        Tool.Set_Option('SCALE_MODE', SCALE_MODE)
        Tool.Set_Option('SCALE_LOG', SCALE_LOG)
        Tool.Set_Option('LUT', LUT)
        Tool.Set_Option('SHADE_TRANS', SHADE_TRANS)
        Tool.Set_Option('SHADE_COLOURING', SHADE_COLOURING)
        Tool.Set_Option('SHADE_BRIGHT', SHADE_BRIGHT)
        Tool.Set_Option('SHADE_STDDEV', SHADE_STDDEV)
        return Tool.Execute(Verbose)
    return False

def Import_Image_File(OUT_GRID=None, OUT_RED=None, OUT_GREEN=None, OUT_BLUE=None, FILE=None, METHOD=None, Verbose=2):
    '''
    Import Image File
    ----------
    [io_grid_image.1]\n
    Loads an image.\n
    Arguments
    ----------
    - OUT_GRID [`output data object`] : Image
    - OUT_RED [`output data object`] : Image (Red Channel)
    - OUT_GREEN [`output data object`] : Image (Green Channel)
    - OUT_BLUE [`output data object`] : Image (Blue Channel)
    - FILE [`file path`] : Image File
    - METHOD [`choice`] : Options. Available Choices: [0] Standard [1] Split Channels [2] Enforce True Color Default: 2

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('io_grid_image', '1', 'Import Image File')
    if Tool.is_Okay():
        Tool.Set_Output('OUT_GRID', OUT_GRID)
        Tool.Set_Output('OUT_RED', OUT_RED)
        Tool.Set_Output('OUT_GREEN', OUT_GREEN)
        Tool.Set_Output('OUT_BLUE', OUT_BLUE)
        Tool.Set_Option('FILE', FILE)
        Tool.Set_Option('METHOD', METHOD)
        return Tool.Execute(Verbose)
    return False

def run_tool_io_grid_image_1(OUT_GRID=None, OUT_RED=None, OUT_GREEN=None, OUT_BLUE=None, FILE=None, METHOD=None, Verbose=2):
    '''
    Import Image File
    ----------
    [io_grid_image.1]\n
    Loads an image.\n
    Arguments
    ----------
    - OUT_GRID [`output data object`] : Image
    - OUT_RED [`output data object`] : Image (Red Channel)
    - OUT_GREEN [`output data object`] : Image (Green Channel)
    - OUT_BLUE [`output data object`] : Image (Blue Channel)
    - FILE [`file path`] : Image File
    - METHOD [`choice`] : Options. Available Choices: [0] Standard [1] Split Channels [2] Enforce True Color Default: 2

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('io_grid_image', '1', 'Import Image File')
    if Tool.is_Okay():
        Tool.Set_Output('OUT_GRID', OUT_GRID)
        Tool.Set_Output('OUT_RED', OUT_RED)
        Tool.Set_Output('OUT_GREEN', OUT_GREEN)
        Tool.Set_Output('OUT_BLUE', OUT_BLUE)
        Tool.Set_Option('FILE', FILE)
        Tool.Set_Option('METHOD', METHOD)
        return Tool.Execute(Verbose)
    return False

def Export_Grid_to_KML(GRID=None, SHADE=None, FILE=None, FORMAT=None, COLOURING=None, COL_PALETTE=None, COL_COUNT=None, COL_REVERT=None, STDDEV=None, STRETCH=None, LUT=None, RESAMPLING=None, SHADE_BRIGHT=None, Verbose=2):
    '''
    Export Grid to KML
    ----------
    [io_grid_image.2]\n
    Uses 'Export Image' tool to create the image file. Automatically projects raster to geographic coordinate system, if its projection is known and not geographic.\n
    Arguments
    ----------
    - GRID [`input grid`] : Grid
    - SHADE [`optional input grid`] : Shade
    - FILE [`file path`] : File
    - FORMAT [`choice`] : Image Format. Available Choices: [0] Portable Network Graphics [1] JPEG - JFIF Compliant [2] Tagged Image File Format [3] Windows or OS/2 Bitmap [4] Zsoft Paintbrush Default: 1
    - COLOURING [`choice`] : Colors. Available Choices: [0] stretch to grid's standard deviation [1] stretch to grid's value range [2] stretch to specified value range [3] lookup table [4] rgb coded values Default: 0
    - COL_PALETTE [`choice`] : Color Ramp. Available Choices: [0] DEFAULT [1] DEFAULT_BRIGHT [2] BLACK_WHITE [3] BLACK_RED [4] BLACK_GREEN [5] BLACK_BLUE [6] WHITE_RED [7] WHITE_GREEN [8] WHITE_BLUE [9] YELLOW_RED [10] YELLOW_GREEN [11] YELLOW_BLUE [12] RED_GREEN [13] RED_BLUE [14] GREEN_BLUE [15] RED_GREY_BLUE [16] RED_GREY_GREEN [17] GREEN_GREY_BLUE [18] RED_GREEN_BLUE [19] RED_BLUE_GREEN [20] GREEN_RED_BLUE [21] RAINBOW [22] NEON [23] TOPOGRAPHY [24] ASPECT_1 [25] ASPECT_2 [26] ASPECT_3 Default: 0
    - COL_COUNT [`integer number`] : Number of Colors. Default: 100
    - COL_REVERT [`boolean`] : Invert Ramp. Default: 0
    - STDDEV [`floating point number`] : Standard Deviation. Minimum: 0.000000 Default: 2.000000
    - STRETCH [`value range`] : Stretch to Value Range
    - LUT [`static table`] : Lookup Table. 5 Fields: - 1. [color] Color - 2. [string] Name - 3. [string] Description - 4. [8 byte floating point number] Minimum - 5. [8 byte floating point number] Maximum 
    - RESAMPLING [`boolean`] : Interpolation. Default: 1 Resampling method used when projection is needed
    - SHADE_BRIGHT [`value range`] : Shade Brightness. Allows one to scale shade brightness [percent]

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('io_grid_image', '2', 'Export Grid to KML')
    if Tool.is_Okay():
        Tool.Set_Input ('GRID', GRID)
        Tool.Set_Input ('SHADE', SHADE)
        Tool.Set_Option('FILE', FILE)
        Tool.Set_Option('FORMAT', FORMAT)
        Tool.Set_Option('COLOURING', COLOURING)
        Tool.Set_Option('COL_PALETTE', COL_PALETTE)
        Tool.Set_Option('COL_COUNT', COL_COUNT)
        Tool.Set_Option('COL_REVERT', COL_REVERT)
        Tool.Set_Option('STDDEV', STDDEV)
        Tool.Set_Option('STRETCH', STRETCH)
        Tool.Set_Option('LUT', LUT)
        Tool.Set_Option('RESAMPLING', RESAMPLING)
        Tool.Set_Option('SHADE_BRIGHT', SHADE_BRIGHT)
        return Tool.Execute(Verbose)
    return False

def run_tool_io_grid_image_2(GRID=None, SHADE=None, FILE=None, FORMAT=None, COLOURING=None, COL_PALETTE=None, COL_COUNT=None, COL_REVERT=None, STDDEV=None, STRETCH=None, LUT=None, RESAMPLING=None, SHADE_BRIGHT=None, Verbose=2):
    '''
    Export Grid to KML
    ----------
    [io_grid_image.2]\n
    Uses 'Export Image' tool to create the image file. Automatically projects raster to geographic coordinate system, if its projection is known and not geographic.\n
    Arguments
    ----------
    - GRID [`input grid`] : Grid
    - SHADE [`optional input grid`] : Shade
    - FILE [`file path`] : File
    - FORMAT [`choice`] : Image Format. Available Choices: [0] Portable Network Graphics [1] JPEG - JFIF Compliant [2] Tagged Image File Format [3] Windows or OS/2 Bitmap [4] Zsoft Paintbrush Default: 1
    - COLOURING [`choice`] : Colors. Available Choices: [0] stretch to grid's standard deviation [1] stretch to grid's value range [2] stretch to specified value range [3] lookup table [4] rgb coded values Default: 0
    - COL_PALETTE [`choice`] : Color Ramp. Available Choices: [0] DEFAULT [1] DEFAULT_BRIGHT [2] BLACK_WHITE [3] BLACK_RED [4] BLACK_GREEN [5] BLACK_BLUE [6] WHITE_RED [7] WHITE_GREEN [8] WHITE_BLUE [9] YELLOW_RED [10] YELLOW_GREEN [11] YELLOW_BLUE [12] RED_GREEN [13] RED_BLUE [14] GREEN_BLUE [15] RED_GREY_BLUE [16] RED_GREY_GREEN [17] GREEN_GREY_BLUE [18] RED_GREEN_BLUE [19] RED_BLUE_GREEN [20] GREEN_RED_BLUE [21] RAINBOW [22] NEON [23] TOPOGRAPHY [24] ASPECT_1 [25] ASPECT_2 [26] ASPECT_3 Default: 0
    - COL_COUNT [`integer number`] : Number of Colors. Default: 100
    - COL_REVERT [`boolean`] : Invert Ramp. Default: 0
    - STDDEV [`floating point number`] : Standard Deviation. Minimum: 0.000000 Default: 2.000000
    - STRETCH [`value range`] : Stretch to Value Range
    - LUT [`static table`] : Lookup Table. 5 Fields: - 1. [color] Color - 2. [string] Name - 3. [string] Description - 4. [8 byte floating point number] Minimum - 5. [8 byte floating point number] Maximum 
    - RESAMPLING [`boolean`] : Interpolation. Default: 1 Resampling method used when projection is needed
    - SHADE_BRIGHT [`value range`] : Shade Brightness. Allows one to scale shade brightness [percent]

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('io_grid_image', '2', 'Export Grid to KML')
    if Tool.is_Okay():
        Tool.Set_Input ('GRID', GRID)
        Tool.Set_Input ('SHADE', SHADE)
        Tool.Set_Option('FILE', FILE)
        Tool.Set_Option('FORMAT', FORMAT)
        Tool.Set_Option('COLOURING', COLOURING)
        Tool.Set_Option('COL_PALETTE', COL_PALETTE)
        Tool.Set_Option('COL_COUNT', COL_COUNT)
        Tool.Set_Option('COL_REVERT', COL_REVERT)
        Tool.Set_Option('STDDEV', STDDEV)
        Tool.Set_Option('STRETCH', STRETCH)
        Tool.Set_Option('LUT', LUT)
        Tool.Set_Option('RESAMPLING', RESAMPLING)
        Tool.Set_Option('SHADE_BRIGHT', SHADE_BRIGHT)
        return Tool.Execute(Verbose)
    return False

def Import_Grids_from_KML(GRIDS=None, FILE=None, Verbose=2):
    '''
    Import Grids from KML
    ----------
    [io_grid_image.3]\n
    Uses 'Import Image' tool to load the ground overlay image files associated with the kml.\n
    Arguments
    ----------
    - GRIDS [`output grid list`] : Grids
    - FILE [`file path`] : KML/KMZ File

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('io_grid_image', '3', 'Import Grids from KML')
    if Tool.is_Okay():
        Tool.Set_Output('GRIDS', GRIDS)
        Tool.Set_Option('FILE', FILE)
        return Tool.Execute(Verbose)
    return False

def run_tool_io_grid_image_3(GRIDS=None, FILE=None, Verbose=2):
    '''
    Import Grids from KML
    ----------
    [io_grid_image.3]\n
    Uses 'Import Image' tool to load the ground overlay image files associated with the kml.\n
    Arguments
    ----------
    - GRIDS [`output grid list`] : Grids
    - FILE [`file path`] : KML/KMZ File

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('io_grid_image', '3', 'Import Grids from KML')
    if Tool.is_Okay():
        Tool.Set_Output('GRIDS', GRIDS)
        Tool.Set_Option('FILE', FILE)
        return Tool.Execute(Verbose)
    return False

def Export_Animated_GIF_File(GRIDS=None, SHADE=None, FILE=None, DELAY=None, COLORS=None, FILE_WORLD=None, FILE_KML=None, NO_DATA=None, NO_DATA_COL=None, COLOURING=None, COL_COUNT=None, COL_REVERT=None, COL_PALETTE=None, GRADUATED=None, STDDEV=None, LINEAR=None, STRETCH=None, SCALE_MODE=None, SCALE_LOG=None, LUT=None, SHADE_TRANS=None, SHADE_COLOURING=None, SHADE_BRIGHT=None, SHADE_STDDEV=None, Verbose=2):
    '''
    Export Animated GIF File
    ----------
    [io_grid_image.4]\n
    Create an animated GIF file from a list of rasters.\n
    If input rasters come with geographic coordinates an additionaloption for KML file creation (Google Earth) is selectable.\n
    Arguments
    ----------
    - GRIDS [`input grid list`] : Grids
    - SHADE [`optional input grid`] : Shade
    - FILE [`file path`] : Image File
    - DELAY [`integer number`] : Delay. Minimum: 0 Default: 100 Delay, in milliseconds, to wait between each frame.
    - COLORS [`integer number`] : Color Depth. Minimum: 2 Maximum: 256 Default: 236 Number of color entries to be used.
    - FILE_WORLD [`boolean`] : Create World File. Default: 0 Store georeference along image to an additional file.
    - FILE_KML [`boolean`] : Create KML File. Default: 0 Expects that the input grid uses geographic coordinates.
    - NO_DATA [`boolean`] : Set Transparency for No-Data. Default: 1
    - NO_DATA_COL [`color`] : No-Data Color. Default: 16777215
    - COLOURING [`choice`] : Coloring. Available Choices: [0] histogram stretch to standard deviation [1] histogram stretch to percentage range [2] histogram stretch to value range [3] lookup table [4] rgb coded values Default: 0
    - COL_COUNT [`integer number`] : Number of Colors. Default: 100
    - COL_REVERT [`boolean`] : Revert Palette. Default: 0
    - COL_PALETTE [`colors`] : Colors Palette
    - GRADUATED [`boolean`] : Graduated Colors. Default: 1
    - STDDEV [`floating point number`] : Standard Deviation. Minimum: 0.000000 Default: 2.000000
    - LINEAR [`value range`] : Percentage Range
    - STRETCH [`value range`] : Value Range
    - SCALE_MODE [`choice`] : Scaling. Available Choices: [0] linear intervals [1] increasing geometrical intervals [2] decreasing geometrical intervals Default: 0 Scaling applied to coloring choices (i) grid's standard deviation, (ii) grid's value range, (iii) specified value range
    - SCALE_LOG [`floating point number`] : Geometrical Interval Factor. Minimum: 0.001000 Default: 10.000000
    - LUT [`static table`] : Lookup Table. 5 Fields: - 1. [color] Color - 2. [string] Name - 3. [string] Description - 4. [8 byte floating point number] Minimum - 5. [8 byte floating point number] Maximum 
    - SHADE_TRANS [`floating point number`] : Transparency. Minimum: 0.000000 Maximum: 100.000000 Default: 75.000000 The transparency of the shade [%]
    - SHADE_COLOURING [`choice`] : Histogram Stretch. Available Choices: [0] Linear [1] Standard Deviation Default: 0
    - SHADE_BRIGHT [`value range`] : Linear. Minimum and maximum [%], the range for histogram stretch.
    - SHADE_STDDEV [`floating point number`] : Standard Deviation. Minimum: 0.000000 Default: 2.000000

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('io_grid_image', '4', 'Export Animated GIF File')
    if Tool.is_Okay():
        Tool.Set_Input ('GRIDS', GRIDS)
        Tool.Set_Input ('SHADE', SHADE)
        Tool.Set_Option('FILE', FILE)
        Tool.Set_Option('DELAY', DELAY)
        Tool.Set_Option('COLORS', COLORS)
        Tool.Set_Option('FILE_WORLD', FILE_WORLD)
        Tool.Set_Option('FILE_KML', FILE_KML)
        Tool.Set_Option('NO_DATA', NO_DATA)
        Tool.Set_Option('NO_DATA_COL', NO_DATA_COL)
        Tool.Set_Option('COLOURING', COLOURING)
        Tool.Set_Option('COL_COUNT', COL_COUNT)
        Tool.Set_Option('COL_REVERT', COL_REVERT)
        Tool.Set_Option('COL_PALETTE', COL_PALETTE)
        Tool.Set_Option('GRADUATED', GRADUATED)
        Tool.Set_Option('STDDEV', STDDEV)
        Tool.Set_Option('LINEAR', LINEAR)
        Tool.Set_Option('STRETCH', STRETCH)
        Tool.Set_Option('SCALE_MODE', SCALE_MODE)
        Tool.Set_Option('SCALE_LOG', SCALE_LOG)
        Tool.Set_Option('LUT', LUT)
        Tool.Set_Option('SHADE_TRANS', SHADE_TRANS)
        Tool.Set_Option('SHADE_COLOURING', SHADE_COLOURING)
        Tool.Set_Option('SHADE_BRIGHT', SHADE_BRIGHT)
        Tool.Set_Option('SHADE_STDDEV', SHADE_STDDEV)
        return Tool.Execute(Verbose)
    return False

def run_tool_io_grid_image_4(GRIDS=None, SHADE=None, FILE=None, DELAY=None, COLORS=None, FILE_WORLD=None, FILE_KML=None, NO_DATA=None, NO_DATA_COL=None, COLOURING=None, COL_COUNT=None, COL_REVERT=None, COL_PALETTE=None, GRADUATED=None, STDDEV=None, LINEAR=None, STRETCH=None, SCALE_MODE=None, SCALE_LOG=None, LUT=None, SHADE_TRANS=None, SHADE_COLOURING=None, SHADE_BRIGHT=None, SHADE_STDDEV=None, Verbose=2):
    '''
    Export Animated GIF File
    ----------
    [io_grid_image.4]\n
    Create an animated GIF file from a list of rasters.\n
    If input rasters come with geographic coordinates an additionaloption for KML file creation (Google Earth) is selectable.\n
    Arguments
    ----------
    - GRIDS [`input grid list`] : Grids
    - SHADE [`optional input grid`] : Shade
    - FILE [`file path`] : Image File
    - DELAY [`integer number`] : Delay. Minimum: 0 Default: 100 Delay, in milliseconds, to wait between each frame.
    - COLORS [`integer number`] : Color Depth. Minimum: 2 Maximum: 256 Default: 236 Number of color entries to be used.
    - FILE_WORLD [`boolean`] : Create World File. Default: 0 Store georeference along image to an additional file.
    - FILE_KML [`boolean`] : Create KML File. Default: 0 Expects that the input grid uses geographic coordinates.
    - NO_DATA [`boolean`] : Set Transparency for No-Data. Default: 1
    - NO_DATA_COL [`color`] : No-Data Color. Default: 16777215
    - COLOURING [`choice`] : Coloring. Available Choices: [0] histogram stretch to standard deviation [1] histogram stretch to percentage range [2] histogram stretch to value range [3] lookup table [4] rgb coded values Default: 0
    - COL_COUNT [`integer number`] : Number of Colors. Default: 100
    - COL_REVERT [`boolean`] : Revert Palette. Default: 0
    - COL_PALETTE [`colors`] : Colors Palette
    - GRADUATED [`boolean`] : Graduated Colors. Default: 1
    - STDDEV [`floating point number`] : Standard Deviation. Minimum: 0.000000 Default: 2.000000
    - LINEAR [`value range`] : Percentage Range
    - STRETCH [`value range`] : Value Range
    - SCALE_MODE [`choice`] : Scaling. Available Choices: [0] linear intervals [1] increasing geometrical intervals [2] decreasing geometrical intervals Default: 0 Scaling applied to coloring choices (i) grid's standard deviation, (ii) grid's value range, (iii) specified value range
    - SCALE_LOG [`floating point number`] : Geometrical Interval Factor. Minimum: 0.001000 Default: 10.000000
    - LUT [`static table`] : Lookup Table. 5 Fields: - 1. [color] Color - 2. [string] Name - 3. [string] Description - 4. [8 byte floating point number] Minimum - 5. [8 byte floating point number] Maximum 
    - SHADE_TRANS [`floating point number`] : Transparency. Minimum: 0.000000 Maximum: 100.000000 Default: 75.000000 The transparency of the shade [%]
    - SHADE_COLOURING [`choice`] : Histogram Stretch. Available Choices: [0] Linear [1] Standard Deviation Default: 0
    - SHADE_BRIGHT [`value range`] : Linear. Minimum and maximum [%], the range for histogram stretch.
    - SHADE_STDDEV [`floating point number`] : Standard Deviation. Minimum: 0.000000 Default: 2.000000

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('io_grid_image', '4', 'Export Animated GIF File')
    if Tool.is_Okay():
        Tool.Set_Input ('GRIDS', GRIDS)
        Tool.Set_Input ('SHADE', SHADE)
        Tool.Set_Option('FILE', FILE)
        Tool.Set_Option('DELAY', DELAY)
        Tool.Set_Option('COLORS', COLORS)
        Tool.Set_Option('FILE_WORLD', FILE_WORLD)
        Tool.Set_Option('FILE_KML', FILE_KML)
        Tool.Set_Option('NO_DATA', NO_DATA)
        Tool.Set_Option('NO_DATA_COL', NO_DATA_COL)
        Tool.Set_Option('COLOURING', COLOURING)
        Tool.Set_Option('COL_COUNT', COL_COUNT)
        Tool.Set_Option('COL_REVERT', COL_REVERT)
        Tool.Set_Option('COL_PALETTE', COL_PALETTE)
        Tool.Set_Option('GRADUATED', GRADUATED)
        Tool.Set_Option('STDDEV', STDDEV)
        Tool.Set_Option('LINEAR', LINEAR)
        Tool.Set_Option('STRETCH', STRETCH)
        Tool.Set_Option('SCALE_MODE', SCALE_MODE)
        Tool.Set_Option('SCALE_LOG', SCALE_LOG)
        Tool.Set_Option('LUT', LUT)
        Tool.Set_Option('SHADE_TRANS', SHADE_TRANS)
        Tool.Set_Option('SHADE_COLOURING', SHADE_COLOURING)
        Tool.Set_Option('SHADE_BRIGHT', SHADE_BRIGHT)
        Tool.Set_Option('SHADE_STDDEV', SHADE_STDDEV)
        return Tool.Execute(Verbose)
    return False

