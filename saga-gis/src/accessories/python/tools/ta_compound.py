#! /usr/bin/env python

'''
Python Interface to SAGA Tools Library
----------
- Category : Terrain Analysis
- Name     : Compound Analyses
- ID       : ta_compound

Description
----------
This library provides one-step tools for compound terrain analyses.
'''

from PySAGA.helper import Tool_Wrapper

def Compound_Basic_Terrain_Analysis(ELEVATION=None, SHADE=None, SLOPE=None, ASPECT=None, HCURV=None, VCURV=None, CONVERGENCE=None, SINKS=None, FLOW=None, WETNESS=None, LSFACTOR=None, CHANNELS=None, BASINS=None, CHNL_BASE=None, CHNL_DIST=None, VALL_DEPTH=None, RSP=None, THRESHOLD=None, Verbose=2):
    '''
    Compound Basic Terrain Analysis
    ----------
    [ta_compound.0]\n
    A selection of popular parameters and objects to be derived from a Digital Terrain Model using standard settings. This one-step tool makes use of tools from the libraries:\n
    (-) ta_morphometry\n
    (-) ta_lighting\n
    (-) ta_preproc\n
    (-) ta_channels\n
    (-) ta_hydrology\n
    Arguments
    ----------
    - ELEVATION [`input grid`] : Elevation
    - SHADE [`output grid`] : Analytical Hillshading
    - SLOPE [`output grid`] : Slope
    - ASPECT [`output grid`] : Aspect
    - HCURV [`output grid`] : Plan Curvature
    - VCURV [`output grid`] : Profile Curvature
    - CONVERGENCE [`output grid`] : Convergence Index
    - SINKS [`output grid`] : Closed Depressions
    - FLOW [`output grid`] : Total Catchment Area
    - WETNESS [`output grid`] : Topographic Wetness Index
    - LSFACTOR [`output grid`] : LS-Factor
    - CHANNELS [`output shapes`] : Channel Network
    - BASINS [`output shapes`] : Drainage Basins
    - CHNL_BASE [`output grid`] : Channel Network Base Level
    - CHNL_DIST [`output grid`] : Channel Network Distance
    - VALL_DEPTH [`output grid`] : Valley Depth
    - RSP [`output grid`] : Relative Slope Position
    - THRESHOLD [`integer number`] : Channel Density. Minimum: 1 Default: 5 Strahler order to begin a channel.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_compound', '0', 'Compound Basic Terrain Analysis')
    if Tool.is_Okay():
        Tool.Set_Input ('ELEVATION', ELEVATION)
        Tool.Set_Output('SHADE', SHADE)
        Tool.Set_Output('SLOPE', SLOPE)
        Tool.Set_Output('ASPECT', ASPECT)
        Tool.Set_Output('HCURV', HCURV)
        Tool.Set_Output('VCURV', VCURV)
        Tool.Set_Output('CONVERGENCE', CONVERGENCE)
        Tool.Set_Output('SINKS', SINKS)
        Tool.Set_Output('FLOW', FLOW)
        Tool.Set_Output('WETNESS', WETNESS)
        Tool.Set_Output('LSFACTOR', LSFACTOR)
        Tool.Set_Output('CHANNELS', CHANNELS)
        Tool.Set_Output('BASINS', BASINS)
        Tool.Set_Output('CHNL_BASE', CHNL_BASE)
        Tool.Set_Output('CHNL_DIST', CHNL_DIST)
        Tool.Set_Output('VALL_DEPTH', VALL_DEPTH)
        Tool.Set_Output('RSP', RSP)
        Tool.Set_Option('THRESHOLD', THRESHOLD)
        return Tool.Execute(Verbose)
    return False

def run_tool_ta_compound_0(ELEVATION=None, SHADE=None, SLOPE=None, ASPECT=None, HCURV=None, VCURV=None, CONVERGENCE=None, SINKS=None, FLOW=None, WETNESS=None, LSFACTOR=None, CHANNELS=None, BASINS=None, CHNL_BASE=None, CHNL_DIST=None, VALL_DEPTH=None, RSP=None, THRESHOLD=None, Verbose=2):
    '''
    Compound Basic Terrain Analysis
    ----------
    [ta_compound.0]\n
    A selection of popular parameters and objects to be derived from a Digital Terrain Model using standard settings. This one-step tool makes use of tools from the libraries:\n
    (-) ta_morphometry\n
    (-) ta_lighting\n
    (-) ta_preproc\n
    (-) ta_channels\n
    (-) ta_hydrology\n
    Arguments
    ----------
    - ELEVATION [`input grid`] : Elevation
    - SHADE [`output grid`] : Analytical Hillshading
    - SLOPE [`output grid`] : Slope
    - ASPECT [`output grid`] : Aspect
    - HCURV [`output grid`] : Plan Curvature
    - VCURV [`output grid`] : Profile Curvature
    - CONVERGENCE [`output grid`] : Convergence Index
    - SINKS [`output grid`] : Closed Depressions
    - FLOW [`output grid`] : Total Catchment Area
    - WETNESS [`output grid`] : Topographic Wetness Index
    - LSFACTOR [`output grid`] : LS-Factor
    - CHANNELS [`output shapes`] : Channel Network
    - BASINS [`output shapes`] : Drainage Basins
    - CHNL_BASE [`output grid`] : Channel Network Base Level
    - CHNL_DIST [`output grid`] : Channel Network Distance
    - VALL_DEPTH [`output grid`] : Valley Depth
    - RSP [`output grid`] : Relative Slope Position
    - THRESHOLD [`integer number`] : Channel Density. Minimum: 1 Default: 5 Strahler order to begin a channel.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_compound', '0', 'Compound Basic Terrain Analysis')
    if Tool.is_Okay():
        Tool.Set_Input ('ELEVATION', ELEVATION)
        Tool.Set_Output('SHADE', SHADE)
        Tool.Set_Output('SLOPE', SLOPE)
        Tool.Set_Output('ASPECT', ASPECT)
        Tool.Set_Output('HCURV', HCURV)
        Tool.Set_Output('VCURV', VCURV)
        Tool.Set_Output('CONVERGENCE', CONVERGENCE)
        Tool.Set_Output('SINKS', SINKS)
        Tool.Set_Output('FLOW', FLOW)
        Tool.Set_Output('WETNESS', WETNESS)
        Tool.Set_Output('LSFACTOR', LSFACTOR)
        Tool.Set_Output('CHANNELS', CHANNELS)
        Tool.Set_Output('BASINS', BASINS)
        Tool.Set_Output('CHNL_BASE', CHNL_BASE)
        Tool.Set_Output('CHNL_DIST', CHNL_DIST)
        Tool.Set_Output('VALL_DEPTH', VALL_DEPTH)
        Tool.Set_Output('RSP', RSP)
        Tool.Set_Option('THRESHOLD', THRESHOLD)
        return Tool.Execute(Verbose)
    return False

def Compound_Morphometric_Terrain_Analysis(ELEVATION=None, SURFACE_AREA=None, SLOPE=None, ASPECT=None, NORTHNESS=None, EASTNESS=None, CONVERGENCE=None, CURVE_PLAN=None, CURVE_CROSS=None, CURVE_PROFILE=None, CURVE_LONGITUDE=None, CURVE_DOWNSLOPE=None, CURVE_UPSLOPE=None, MASS_BALANCE=None, CELL_BALANCE=None, TPI=None, TERRAIN_CONVEXITY=None, TERRAIN_TEXTURE=None, TRI=None, VRM=None, PROTECTION=None, OPENNESS_POS=None, OPENNESS_NEG=None, SCALE=None, Verbose=2):
    '''
    Compound Morphometric Terrain Analysis
    ----------
    [ta_compound.1]\n
    A one-step tool for the creation of selected terrain parameters related to morphometry.For details look at the description of the tools used for processing:\n
    (-) [ta_morphometry] Slope, Aspect, Curvature\n
    (-) [ta_morphometry] Convergence Index (Search Radius)\n
    (-) [ta_morphometry] Upslope and Downslope Curvature\n
    (-) [ta_morphometry] Mass Balance Index\n
    (-) [ta_hydrology] Cell Balance\n
    (-) [ta_morphometry] Topographic Position Index\n
    (-) [ta_morphometry] Terrain Surface Convexity\n
    (-) [ta_morphometry] Terrain Surface Texture\n
    (-) [ta_morphometry] Terrain Ruggedness Index\n
    (-) [ta_morphometry] Vector Ruggedness Measure\n
    (-) [ta_morphometry] Morphometric Protection Index\n
    (-) [ta_lighting] Topographic Openness\n
    Arguments
    ----------
    - ELEVATION [`input grid`] : Elevation
    - SURFACE_AREA [`output grid`] : Real Surface Area
    - SLOPE [`output grid`] : Slope
    - ASPECT [`output grid`] : Aspect
    - NORTHNESS [`output grid`] : Northness
    - EASTNESS [`output grid`] : Eastness
    - CONVERGENCE [`output grid`] : Convergence Index
    - CURVE_PLAN [`output grid`] : Plan Curvature
    - CURVE_CROSS [`output grid`] : Cross-Sectional Curvature
    - CURVE_PROFILE [`output grid`] : Profile Curvature
    - CURVE_LONGITUDE [`output grid`] : Longitudinal Curvature
    - CURVE_DOWNSLOPE [`output grid`] : Downslope Curvature
    - CURVE_UPSLOPE [`output grid`] : Upslope Curvature
    - MASS_BALANCE [`output grid`] : Mass Balance Index
    - CELL_BALANCE [`output grid`] : Cell Balance
    - TPI [`output grid`] : Topographic Position Index
    - TERRAIN_CONVEXITY [`output grid`] : Terrain Surface Convexity
    - TERRAIN_TEXTURE [`output grid`] : Terrain Surface Texture
    - TRI [`output grid`] : Terrain Ruggedness Index
    - VRM [`output grid`] : Vector Ruggedness Measure
    - PROTECTION [`output grid`] : Morphometric Protection Index
    - OPENNESS_POS [`output grid`] : Positive Topographic Openness
    - OPENNESS_NEG [`output grid`] : Negative Topographic Openness
    - SCALE [`integer number`] : Scale. Minimum: 1 Default: 4 Targeted scale (cells). Applies to "Convergence Index".

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_compound', '1', 'Compound Morphometric Terrain Analysis')
    if Tool.is_Okay():
        Tool.Set_Input ('ELEVATION', ELEVATION)
        Tool.Set_Output('SURFACE_AREA', SURFACE_AREA)
        Tool.Set_Output('SLOPE', SLOPE)
        Tool.Set_Output('ASPECT', ASPECT)
        Tool.Set_Output('NORTHNESS', NORTHNESS)
        Tool.Set_Output('EASTNESS', EASTNESS)
        Tool.Set_Output('CONVERGENCE', CONVERGENCE)
        Tool.Set_Output('CURVE_PLAN', CURVE_PLAN)
        Tool.Set_Output('CURVE_CROSS', CURVE_CROSS)
        Tool.Set_Output('CURVE_PROFILE', CURVE_PROFILE)
        Tool.Set_Output('CURVE_LONGITUDE', CURVE_LONGITUDE)
        Tool.Set_Output('CURVE_DOWNSLOPE', CURVE_DOWNSLOPE)
        Tool.Set_Output('CURVE_UPSLOPE', CURVE_UPSLOPE)
        Tool.Set_Output('MASS_BALANCE', MASS_BALANCE)
        Tool.Set_Output('CELL_BALANCE', CELL_BALANCE)
        Tool.Set_Output('TPI', TPI)
        Tool.Set_Output('TERRAIN_CONVEXITY', TERRAIN_CONVEXITY)
        Tool.Set_Output('TERRAIN_TEXTURE', TERRAIN_TEXTURE)
        Tool.Set_Output('TRI', TRI)
        Tool.Set_Output('VRM', VRM)
        Tool.Set_Output('PROTECTION', PROTECTION)
        Tool.Set_Output('OPENNESS_POS', OPENNESS_POS)
        Tool.Set_Output('OPENNESS_NEG', OPENNESS_NEG)
        Tool.Set_Option('SCALE', SCALE)
        return Tool.Execute(Verbose)
    return False

def run_tool_ta_compound_1(ELEVATION=None, SURFACE_AREA=None, SLOPE=None, ASPECT=None, NORTHNESS=None, EASTNESS=None, CONVERGENCE=None, CURVE_PLAN=None, CURVE_CROSS=None, CURVE_PROFILE=None, CURVE_LONGITUDE=None, CURVE_DOWNSLOPE=None, CURVE_UPSLOPE=None, MASS_BALANCE=None, CELL_BALANCE=None, TPI=None, TERRAIN_CONVEXITY=None, TERRAIN_TEXTURE=None, TRI=None, VRM=None, PROTECTION=None, OPENNESS_POS=None, OPENNESS_NEG=None, SCALE=None, Verbose=2):
    '''
    Compound Morphometric Terrain Analysis
    ----------
    [ta_compound.1]\n
    A one-step tool for the creation of selected terrain parameters related to morphometry.For details look at the description of the tools used for processing:\n
    (-) [ta_morphometry] Slope, Aspect, Curvature\n
    (-) [ta_morphometry] Convergence Index (Search Radius)\n
    (-) [ta_morphometry] Upslope and Downslope Curvature\n
    (-) [ta_morphometry] Mass Balance Index\n
    (-) [ta_hydrology] Cell Balance\n
    (-) [ta_morphometry] Topographic Position Index\n
    (-) [ta_morphometry] Terrain Surface Convexity\n
    (-) [ta_morphometry] Terrain Surface Texture\n
    (-) [ta_morphometry] Terrain Ruggedness Index\n
    (-) [ta_morphometry] Vector Ruggedness Measure\n
    (-) [ta_morphometry] Morphometric Protection Index\n
    (-) [ta_lighting] Topographic Openness\n
    Arguments
    ----------
    - ELEVATION [`input grid`] : Elevation
    - SURFACE_AREA [`output grid`] : Real Surface Area
    - SLOPE [`output grid`] : Slope
    - ASPECT [`output grid`] : Aspect
    - NORTHNESS [`output grid`] : Northness
    - EASTNESS [`output grid`] : Eastness
    - CONVERGENCE [`output grid`] : Convergence Index
    - CURVE_PLAN [`output grid`] : Plan Curvature
    - CURVE_CROSS [`output grid`] : Cross-Sectional Curvature
    - CURVE_PROFILE [`output grid`] : Profile Curvature
    - CURVE_LONGITUDE [`output grid`] : Longitudinal Curvature
    - CURVE_DOWNSLOPE [`output grid`] : Downslope Curvature
    - CURVE_UPSLOPE [`output grid`] : Upslope Curvature
    - MASS_BALANCE [`output grid`] : Mass Balance Index
    - CELL_BALANCE [`output grid`] : Cell Balance
    - TPI [`output grid`] : Topographic Position Index
    - TERRAIN_CONVEXITY [`output grid`] : Terrain Surface Convexity
    - TERRAIN_TEXTURE [`output grid`] : Terrain Surface Texture
    - TRI [`output grid`] : Terrain Ruggedness Index
    - VRM [`output grid`] : Vector Ruggedness Measure
    - PROTECTION [`output grid`] : Morphometric Protection Index
    - OPENNESS_POS [`output grid`] : Positive Topographic Openness
    - OPENNESS_NEG [`output grid`] : Negative Topographic Openness
    - SCALE [`integer number`] : Scale. Minimum: 1 Default: 4 Targeted scale (cells). Applies to "Convergence Index".

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_compound', '1', 'Compound Morphometric Terrain Analysis')
    if Tool.is_Okay():
        Tool.Set_Input ('ELEVATION', ELEVATION)
        Tool.Set_Output('SURFACE_AREA', SURFACE_AREA)
        Tool.Set_Output('SLOPE', SLOPE)
        Tool.Set_Output('ASPECT', ASPECT)
        Tool.Set_Output('NORTHNESS', NORTHNESS)
        Tool.Set_Output('EASTNESS', EASTNESS)
        Tool.Set_Output('CONVERGENCE', CONVERGENCE)
        Tool.Set_Output('CURVE_PLAN', CURVE_PLAN)
        Tool.Set_Output('CURVE_CROSS', CURVE_CROSS)
        Tool.Set_Output('CURVE_PROFILE', CURVE_PROFILE)
        Tool.Set_Output('CURVE_LONGITUDE', CURVE_LONGITUDE)
        Tool.Set_Output('CURVE_DOWNSLOPE', CURVE_DOWNSLOPE)
        Tool.Set_Output('CURVE_UPSLOPE', CURVE_UPSLOPE)
        Tool.Set_Output('MASS_BALANCE', MASS_BALANCE)
        Tool.Set_Output('CELL_BALANCE', CELL_BALANCE)
        Tool.Set_Output('TPI', TPI)
        Tool.Set_Output('TERRAIN_CONVEXITY', TERRAIN_CONVEXITY)
        Tool.Set_Output('TERRAIN_TEXTURE', TERRAIN_TEXTURE)
        Tool.Set_Output('TRI', TRI)
        Tool.Set_Output('VRM', VRM)
        Tool.Set_Output('PROTECTION', PROTECTION)
        Tool.Set_Output('OPENNESS_POS', OPENNESS_POS)
        Tool.Set_Output('OPENNESS_NEG', OPENNESS_NEG)
        Tool.Set_Option('SCALE', SCALE)
        return Tool.Execute(Verbose)
    return False

def Compound_Hydrologic_Terrain_Analysis(ELEVATION=None, SINKS=None, TCA=None, SCA=None, TWI=None, TWI_SAGA=None, LS=None, SPI=None, CIT=None, METHOD_PREPROC=None, METHOD_FLOWACC=None, METHOD_LS=None, Verbose=2):
    '''
    Compound Hydrologic Terrain Analysis
    ----------
    [ta_compound.2]\n
    A one-step tool for the creation of selected terrain parameters related to hydrology.For details look at the description of the tools used for processing:\n
    (-) [ta_preproc] Sink Removal\n
    (-) [ta_preproc] Fill Sinks (Wang & Liu)\n
    (-) [ta_preproc] Breach Depressions\n
    (-) [ta_hydrology] Flow Accumulation (Top-Down)\n
    (-) [ta_hydrology] Flow Accumulation (Flow Tracing)\n
    (-) [ta_hydrology] Topographic Wetness Index\n
    (-) [ta_hydrology] SAGA Wetness Index\n
    (-) [ta_hydrology] LS Factor\n
    (-) [ta_hydrology] CIT\n
    Arguments
    ----------
    - ELEVATION [`input grid`] : Elevation
    - SINKS [`output grid`] : Closed Depressions
    - TCA [`output grid`] : Total Catchment Area
    - SCA [`output grid`] : Specific Catchment Area
    - TWI [`output grid`] : Topographic Wetness Index
    - TWI_SAGA [`output grid`] : SAGA Topographic Wetness Index
    - LS [`output grid`] : LS Factor
    - SPI [`output grid`] : Stream Power Index
    - CIT [`output grid`] : Channel Initiation Threshold
    - METHOD_PREPROC [`choice`] : Preprocessing. Available Choices: [0] Sink Removal [1] Fill Sinks (Wang & Liu) [2] Breach Depressions [3] none Default: 0
    - METHOD_FLOWACC [`choice`] : Flow Accumulation. Available Choices: [0] Deterministic 8 [1] Rho 8 [2] Deterministic Infinity [3] Multiple Flow Direction [4] Multiple Triangular Flow Direction [5] Multiple Maximum Downslope Gradient Based Flow Direction [6] Kinematic Routing Algorithm [7] DEMON Default: 3 Algorithm used to calculate total catchment area.
    - METHOD_LS [`choice`] : LS Factor. Available Choices: [0] Moore et al. 1991 [1] Desmet & Govers 1996 [2] Boehner & Selige 2006 Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_compound', '2', 'Compound Hydrologic Terrain Analysis')
    if Tool.is_Okay():
        Tool.Set_Input ('ELEVATION', ELEVATION)
        Tool.Set_Output('SINKS', SINKS)
        Tool.Set_Output('TCA', TCA)
        Tool.Set_Output('SCA', SCA)
        Tool.Set_Output('TWI', TWI)
        Tool.Set_Output('TWI_SAGA', TWI_SAGA)
        Tool.Set_Output('LS', LS)
        Tool.Set_Output('SPI', SPI)
        Tool.Set_Output('CIT', CIT)
        Tool.Set_Option('METHOD_PREPROC', METHOD_PREPROC)
        Tool.Set_Option('METHOD_FLOWACC', METHOD_FLOWACC)
        Tool.Set_Option('METHOD_LS', METHOD_LS)
        return Tool.Execute(Verbose)
    return False

def run_tool_ta_compound_2(ELEVATION=None, SINKS=None, TCA=None, SCA=None, TWI=None, TWI_SAGA=None, LS=None, SPI=None, CIT=None, METHOD_PREPROC=None, METHOD_FLOWACC=None, METHOD_LS=None, Verbose=2):
    '''
    Compound Hydrologic Terrain Analysis
    ----------
    [ta_compound.2]\n
    A one-step tool for the creation of selected terrain parameters related to hydrology.For details look at the description of the tools used for processing:\n
    (-) [ta_preproc] Sink Removal\n
    (-) [ta_preproc] Fill Sinks (Wang & Liu)\n
    (-) [ta_preproc] Breach Depressions\n
    (-) [ta_hydrology] Flow Accumulation (Top-Down)\n
    (-) [ta_hydrology] Flow Accumulation (Flow Tracing)\n
    (-) [ta_hydrology] Topographic Wetness Index\n
    (-) [ta_hydrology] SAGA Wetness Index\n
    (-) [ta_hydrology] LS Factor\n
    (-) [ta_hydrology] CIT\n
    Arguments
    ----------
    - ELEVATION [`input grid`] : Elevation
    - SINKS [`output grid`] : Closed Depressions
    - TCA [`output grid`] : Total Catchment Area
    - SCA [`output grid`] : Specific Catchment Area
    - TWI [`output grid`] : Topographic Wetness Index
    - TWI_SAGA [`output grid`] : SAGA Topographic Wetness Index
    - LS [`output grid`] : LS Factor
    - SPI [`output grid`] : Stream Power Index
    - CIT [`output grid`] : Channel Initiation Threshold
    - METHOD_PREPROC [`choice`] : Preprocessing. Available Choices: [0] Sink Removal [1] Fill Sinks (Wang & Liu) [2] Breach Depressions [3] none Default: 0
    - METHOD_FLOWACC [`choice`] : Flow Accumulation. Available Choices: [0] Deterministic 8 [1] Rho 8 [2] Deterministic Infinity [3] Multiple Flow Direction [4] Multiple Triangular Flow Direction [5] Multiple Maximum Downslope Gradient Based Flow Direction [6] Kinematic Routing Algorithm [7] DEMON Default: 3 Algorithm used to calculate total catchment area.
    - METHOD_LS [`choice`] : LS Factor. Available Choices: [0] Moore et al. 1991 [1] Desmet & Govers 1996 [2] Boehner & Selige 2006 Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_compound', '2', 'Compound Hydrologic Terrain Analysis')
    if Tool.is_Okay():
        Tool.Set_Input ('ELEVATION', ELEVATION)
        Tool.Set_Output('SINKS', SINKS)
        Tool.Set_Output('TCA', TCA)
        Tool.Set_Output('SCA', SCA)
        Tool.Set_Output('TWI', TWI)
        Tool.Set_Output('TWI_SAGA', TWI_SAGA)
        Tool.Set_Output('LS', LS)
        Tool.Set_Output('SPI', SPI)
        Tool.Set_Output('CIT', CIT)
        Tool.Set_Option('METHOD_PREPROC', METHOD_PREPROC)
        Tool.Set_Option('METHOD_FLOWACC', METHOD_FLOWACC)
        Tool.Set_Option('METHOD_LS', METHOD_LS)
        return Tool.Execute(Verbose)
    return False

def Compound_Channel_Network_Analysis(ELEVATION=None, CHANNELS=None, BASINS=None, SUBBASINS=None, HEADS=None, MOUTHS=None, FLOW_DISTANCE=None, FLOW_DISTHORZ=None, FLOW_DISTVERT=None, CHNL_BASE=None, CHNL_DIST=None, METHOD_PREPROC=None, THRESHOLD=None, Verbose=2):
    '''
    Compound Channel Network Analysis
    ----------
    [ta_compound.3]\n
    A one-step tool for the creation of selected terrain parameters related to channels.For details look at the description of the tools used for processing:\n
    (-) [ta_preproc] Sink Removal\n
    (-) [ta_preproc] Fill Sinks (Wang & Liu)\n
    (-) [ta_preproc] Breach Depressions\n
    (-) [ta_channels] Channel Network and Drainage Basins\n
    (-) [ta_channels] Watershed Basins (Extended)\n
    (-) [ta_channels] Overland Flow Distance to Channel Network\n
    (-) [ta_channels] Vertical Distance to Channel Network\n
    Arguments
    ----------
    - ELEVATION [`input grid`] : Elevation
    - CHANNELS [`output shapes`] : Channel Network
    - BASINS [`output shapes`] : Drainage Basins
    - SUBBASINS [`output shapes`] : Drainage Sub-Basins
    - HEADS [`output shapes`] : Channel Heads
    - MOUTHS [`output shapes`] : Channel Mouths
    - FLOW_DISTANCE [`output grid`] : Overland Flow Distance
    - FLOW_DISTHORZ [`output grid`] : Horizontal Overland Flow Distance
    - FLOW_DISTVERT [`output grid`] : Vertical Overland Flow Distance
    - CHNL_BASE [`output grid`] : Channel Network Base Level
    - CHNL_DIST [`output grid`] : Channel Network Distance
    - METHOD_PREPROC [`choice`] : Preprocessing. Available Choices: [0] Sink Removal [1] Fill Sinks (Wang & Liu) [2] Breach Depressions [3] none Default: 0
    - THRESHOLD [`integer number`] : Channel Density. Minimum: 1 Default: 5 Strahler order to begin a channel.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_compound', '3', 'Compound Channel Network Analysis')
    if Tool.is_Okay():
        Tool.Set_Input ('ELEVATION', ELEVATION)
        Tool.Set_Output('CHANNELS', CHANNELS)
        Tool.Set_Output('BASINS', BASINS)
        Tool.Set_Output('SUBBASINS', SUBBASINS)
        Tool.Set_Output('HEADS', HEADS)
        Tool.Set_Output('MOUTHS', MOUTHS)
        Tool.Set_Output('FLOW_DISTANCE', FLOW_DISTANCE)
        Tool.Set_Output('FLOW_DISTHORZ', FLOW_DISTHORZ)
        Tool.Set_Output('FLOW_DISTVERT', FLOW_DISTVERT)
        Tool.Set_Output('CHNL_BASE', CHNL_BASE)
        Tool.Set_Output('CHNL_DIST', CHNL_DIST)
        Tool.Set_Option('METHOD_PREPROC', METHOD_PREPROC)
        Tool.Set_Option('THRESHOLD', THRESHOLD)
        return Tool.Execute(Verbose)
    return False

def run_tool_ta_compound_3(ELEVATION=None, CHANNELS=None, BASINS=None, SUBBASINS=None, HEADS=None, MOUTHS=None, FLOW_DISTANCE=None, FLOW_DISTHORZ=None, FLOW_DISTVERT=None, CHNL_BASE=None, CHNL_DIST=None, METHOD_PREPROC=None, THRESHOLD=None, Verbose=2):
    '''
    Compound Channel Network Analysis
    ----------
    [ta_compound.3]\n
    A one-step tool for the creation of selected terrain parameters related to channels.For details look at the description of the tools used for processing:\n
    (-) [ta_preproc] Sink Removal\n
    (-) [ta_preproc] Fill Sinks (Wang & Liu)\n
    (-) [ta_preproc] Breach Depressions\n
    (-) [ta_channels] Channel Network and Drainage Basins\n
    (-) [ta_channels] Watershed Basins (Extended)\n
    (-) [ta_channels] Overland Flow Distance to Channel Network\n
    (-) [ta_channels] Vertical Distance to Channel Network\n
    Arguments
    ----------
    - ELEVATION [`input grid`] : Elevation
    - CHANNELS [`output shapes`] : Channel Network
    - BASINS [`output shapes`] : Drainage Basins
    - SUBBASINS [`output shapes`] : Drainage Sub-Basins
    - HEADS [`output shapes`] : Channel Heads
    - MOUTHS [`output shapes`] : Channel Mouths
    - FLOW_DISTANCE [`output grid`] : Overland Flow Distance
    - FLOW_DISTHORZ [`output grid`] : Horizontal Overland Flow Distance
    - FLOW_DISTVERT [`output grid`] : Vertical Overland Flow Distance
    - CHNL_BASE [`output grid`] : Channel Network Base Level
    - CHNL_DIST [`output grid`] : Channel Network Distance
    - METHOD_PREPROC [`choice`] : Preprocessing. Available Choices: [0] Sink Removal [1] Fill Sinks (Wang & Liu) [2] Breach Depressions [3] none Default: 0
    - THRESHOLD [`integer number`] : Channel Density. Minimum: 1 Default: 5 Strahler order to begin a channel.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_compound', '3', 'Compound Channel Network Analysis')
    if Tool.is_Okay():
        Tool.Set_Input ('ELEVATION', ELEVATION)
        Tool.Set_Output('CHANNELS', CHANNELS)
        Tool.Set_Output('BASINS', BASINS)
        Tool.Set_Output('SUBBASINS', SUBBASINS)
        Tool.Set_Output('HEADS', HEADS)
        Tool.Set_Output('MOUTHS', MOUTHS)
        Tool.Set_Output('FLOW_DISTANCE', FLOW_DISTANCE)
        Tool.Set_Output('FLOW_DISTHORZ', FLOW_DISTHORZ)
        Tool.Set_Output('FLOW_DISTVERT', FLOW_DISTVERT)
        Tool.Set_Output('CHNL_BASE', CHNL_BASE)
        Tool.Set_Output('CHNL_DIST', CHNL_DIST)
        Tool.Set_Option('METHOD_PREPROC', METHOD_PREPROC)
        Tool.Set_Option('THRESHOLD', THRESHOLD)
        return Tool.Execute(Verbose)
    return False

def Compound_Terrain_Classification(ELEVATION=None, SPECPOINTS=None, CURVATURE=None, FUZZY=None, MORPHOMETRIC=None, TPI=None, GEOMORPHONS=None, TERRAINSURF=None, SCALE=None, Verbose=2):
    '''
    Compound Terrain Classification
    ----------
    [ta_compound.4]\n
    A one-step tool for the creation of selected terrain classifications.For details look at the description of the tools used for processing:\n
    (-) [ta_morphometry] Surface Specific Points\n
    (-) [ta_morphometry] Curvature Classification\n
    (-) [ta_morphometry] Fuzzy Landform Element Classification\n
    (-) [ta_morphometry] Morphometric Features\n
    (-) [ta_morphometry] TPI Based Landform Classification\n
    (-) [ta_morphometry] Terrain Surface Classification (Iwahashi and Pike)\n
    (-) [ta_lighting] Geomorphons\n
    Arguments
    ----------
    - ELEVATION [`input grid`] : Elevation
    - SPECPOINTS [`output grid`] : Surface Specific Points
    - CURVATURE [`output grid`] : Curvature Classification
    - FUZZY [`output grid`] : Fuzzy Landform Classification
    - MORPHOMETRIC [`output grid`] : Morphometric Features
    - TPI [`output grid`] : Topographic Position Classification
    - GEOMORPHONS [`output grid`] : Geomorphons
    - TERRAINSURF [`output grid`] : Terrain Surface Classification
    - SCALE [`integer number`] : Scale. Minimum: 2 Default: 10 Targeted scale (cells). Does not affect "Surface Specific Points" and  "Fuzzy Landform Classification".

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_compound', '4', 'Compound Terrain Classification')
    if Tool.is_Okay():
        Tool.Set_Input ('ELEVATION', ELEVATION)
        Tool.Set_Output('SPECPOINTS', SPECPOINTS)
        Tool.Set_Output('CURVATURE', CURVATURE)
        Tool.Set_Output('FUZZY', FUZZY)
        Tool.Set_Output('MORPHOMETRIC', MORPHOMETRIC)
        Tool.Set_Output('TPI', TPI)
        Tool.Set_Output('GEOMORPHONS', GEOMORPHONS)
        Tool.Set_Output('TERRAINSURF', TERRAINSURF)
        Tool.Set_Option('SCALE', SCALE)
        return Tool.Execute(Verbose)
    return False

def run_tool_ta_compound_4(ELEVATION=None, SPECPOINTS=None, CURVATURE=None, FUZZY=None, MORPHOMETRIC=None, TPI=None, GEOMORPHONS=None, TERRAINSURF=None, SCALE=None, Verbose=2):
    '''
    Compound Terrain Classification
    ----------
    [ta_compound.4]\n
    A one-step tool for the creation of selected terrain classifications.For details look at the description of the tools used for processing:\n
    (-) [ta_morphometry] Surface Specific Points\n
    (-) [ta_morphometry] Curvature Classification\n
    (-) [ta_morphometry] Fuzzy Landform Element Classification\n
    (-) [ta_morphometry] Morphometric Features\n
    (-) [ta_morphometry] TPI Based Landform Classification\n
    (-) [ta_morphometry] Terrain Surface Classification (Iwahashi and Pike)\n
    (-) [ta_lighting] Geomorphons\n
    Arguments
    ----------
    - ELEVATION [`input grid`] : Elevation
    - SPECPOINTS [`output grid`] : Surface Specific Points
    - CURVATURE [`output grid`] : Curvature Classification
    - FUZZY [`output grid`] : Fuzzy Landform Classification
    - MORPHOMETRIC [`output grid`] : Morphometric Features
    - TPI [`output grid`] : Topographic Position Classification
    - GEOMORPHONS [`output grid`] : Geomorphons
    - TERRAINSURF [`output grid`] : Terrain Surface Classification
    - SCALE [`integer number`] : Scale. Minimum: 2 Default: 10 Targeted scale (cells). Does not affect "Surface Specific Points" and  "Fuzzy Landform Classification".

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_compound', '4', 'Compound Terrain Classification')
    if Tool.is_Okay():
        Tool.Set_Input ('ELEVATION', ELEVATION)
        Tool.Set_Output('SPECPOINTS', SPECPOINTS)
        Tool.Set_Output('CURVATURE', CURVATURE)
        Tool.Set_Output('FUZZY', FUZZY)
        Tool.Set_Output('MORPHOMETRIC', MORPHOMETRIC)
        Tool.Set_Output('TPI', TPI)
        Tool.Set_Output('GEOMORPHONS', GEOMORPHONS)
        Tool.Set_Output('TERRAINSURF', TERRAINSURF)
        Tool.Set_Option('SCALE', SCALE)
        return Tool.Execute(Verbose)
    return False

