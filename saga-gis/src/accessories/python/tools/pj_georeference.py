#! /usr/bin/env python

'''
Python Interface to SAGA Tools Library
----------
- Category : Projection
- Name     : Georeferencing
- ID       : pj_georeference

Description
----------
Tools for the georeferencing of spatial data.
'''

from PySAGA.helper import Tool_Wrapper

def Rectify_Grid(REF_SOURCE=None, GRID=None, REF_TARGET=None, TARGET_TEMPLATE=None, TARGET_GRID=None, XFIELD=None, YFIELD=None, METHOD=None, ORDER=None, RESAMPLING=None, BYTEWISE=None, DATA_TYPE=None, CRS_STRING=None, TARGET_DEFINITION=None, TARGET_USER_SIZE=None, TARGET_USER_XMIN=None, TARGET_USER_XMAX=None, TARGET_USER_YMIN=None, TARGET_USER_YMAX=None, TARGET_USER_COLS=None, TARGET_USER_ROWS=None, TARGET_USER_FLAT=None, TARGET_USER_FITS=None, TARGET_SYSTEM=None, Verbose=2):
    '''
    Rectify Grid
    ----------
    [pj_georeference.1]\n
    Georeferencing and rectification for grids. Either choose the attribute fields (x/y) with the projected coordinates for the reference points (origin) or supply an additional points layer with correspondent points in the target projection.\n
    Arguments
    ----------
    - REF_SOURCE [`input shapes`] : Reference Points (Origin)
    - GRID [`input grid`] : Grid
    - REF_TARGET [`optional input shapes`] : Reference Points (Projection)
    - TARGET_TEMPLATE [`optional input grid`] : Target System. use this grid's system for output grids
    - TARGET_GRID [`output grid`] : Target
    - XFIELD [`table field`] : x Position
    - YFIELD [`table field`] : y Position
    - METHOD [`choice`] : Method. Available Choices: [0] Automatic [1] Triangulation [2] Spline [3] Affine [4] 1st Order Polynomial [5] 2nd Order Polynomial [6] 3rd Order Polynomial [7] Polynomial, Order Default: 0
    - ORDER [`integer number`] : Polynomial Order. Minimum: 1 Default: 3
    - RESAMPLING [`choice`] : Resampling. Available Choices: [0] Nearest Neighbour [1] Bilinear Interpolation [2] Bicubic Spline Interpolation [3] B-Spline Interpolation Default: 3
    - BYTEWISE [`boolean`] : Bytewise Interpolation. Default: 0
    - DATA_TYPE [`data type`] : Data Type. Available Choices: [0] unsigned 1 byte integer [1] signed 1 byte integer [2] unsigned 2 byte integer [3] signed 2 byte integer [4] unsigned 4 byte integer [5] signed 4 byte integer [6] unsigned 8 byte integer [7] signed 8 byte integer [8] 4 byte floating point number [9] 8 byte floating point number [10] Preserve Default: 10
    - CRS_STRING [`text`] : Coordinate System Definition. Supported formats comprise PROJ and WKT strings, object codes (e.g. "EPSG:4326").
    - TARGET_DEFINITION [`choice`] : Target Grid System. Available Choices: [0] user defined [1] grid or grid system Default: 0
    - TARGET_USER_SIZE [`floating point number`] : Cellsize. Minimum: 0.000000 Default: 1.000000
    - TARGET_USER_XMIN [`floating point number`] : West. Default: 0.000000
    - TARGET_USER_XMAX [`floating point number`] : East. Default: 100.000000
    - TARGET_USER_YMIN [`floating point number`] : South. Default: 0.000000
    - TARGET_USER_YMAX [`floating point number`] : North. Default: 100.000000
    - TARGET_USER_COLS [`integer number`] : Columns. Minimum: 1 Default: 101 Number of cells in East-West direction.
    - TARGET_USER_ROWS [`integer number`] : Rows. Minimum: 1 Default: 101 Number of cells in North-South direction.
    - TARGET_USER_FLAT [`boolean`] : Rounding. Default: 1 Round bounding coordinates to multiples of cell size. Ignored if cell size has decimal places.
    - TARGET_USER_FITS [`choice`] : Fit. Available Choices: [0] nodes [1] cells Default: 0
    - TARGET_SYSTEM [`grid system`] : Grid System

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('pj_georeference', '1', 'Rectify Grid')
    if Tool.is_Okay():
        Tool.Set_Input ('REF_SOURCE', REF_SOURCE)
        Tool.Set_Input ('GRID', GRID)
        Tool.Set_Input ('REF_TARGET', REF_TARGET)
        Tool.Set_Input ('TARGET_TEMPLATE', TARGET_TEMPLATE)
        Tool.Set_Output('TARGET_GRID', TARGET_GRID)
        Tool.Set_Option('XFIELD', XFIELD)
        Tool.Set_Option('YFIELD', YFIELD)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('ORDER', ORDER)
        Tool.Set_Option('RESAMPLING', RESAMPLING)
        Tool.Set_Option('BYTEWISE', BYTEWISE)
        Tool.Set_Option('DATA_TYPE', DATA_TYPE)
        Tool.Set_Option('CRS_STRING', CRS_STRING)
        Tool.Set_Option('TARGET_DEFINITION', TARGET_DEFINITION)
        Tool.Set_Option('TARGET_USER_SIZE', TARGET_USER_SIZE)
        Tool.Set_Option('TARGET_USER_XMIN', TARGET_USER_XMIN)
        Tool.Set_Option('TARGET_USER_XMAX', TARGET_USER_XMAX)
        Tool.Set_Option('TARGET_USER_YMIN', TARGET_USER_YMIN)
        Tool.Set_Option('TARGET_USER_YMAX', TARGET_USER_YMAX)
        Tool.Set_Option('TARGET_USER_COLS', TARGET_USER_COLS)
        Tool.Set_Option('TARGET_USER_ROWS', TARGET_USER_ROWS)
        Tool.Set_Option('TARGET_USER_FLAT', TARGET_USER_FLAT)
        Tool.Set_Option('TARGET_USER_FITS', TARGET_USER_FITS)
        Tool.Set_Option('TARGET_SYSTEM', TARGET_SYSTEM)
        return Tool.Execute(Verbose)
    return False

def run_tool_pj_georeference_1(REF_SOURCE=None, GRID=None, REF_TARGET=None, TARGET_TEMPLATE=None, TARGET_GRID=None, XFIELD=None, YFIELD=None, METHOD=None, ORDER=None, RESAMPLING=None, BYTEWISE=None, DATA_TYPE=None, CRS_STRING=None, TARGET_DEFINITION=None, TARGET_USER_SIZE=None, TARGET_USER_XMIN=None, TARGET_USER_XMAX=None, TARGET_USER_YMIN=None, TARGET_USER_YMAX=None, TARGET_USER_COLS=None, TARGET_USER_ROWS=None, TARGET_USER_FLAT=None, TARGET_USER_FITS=None, TARGET_SYSTEM=None, Verbose=2):
    '''
    Rectify Grid
    ----------
    [pj_georeference.1]\n
    Georeferencing and rectification for grids. Either choose the attribute fields (x/y) with the projected coordinates for the reference points (origin) or supply an additional points layer with correspondent points in the target projection.\n
    Arguments
    ----------
    - REF_SOURCE [`input shapes`] : Reference Points (Origin)
    - GRID [`input grid`] : Grid
    - REF_TARGET [`optional input shapes`] : Reference Points (Projection)
    - TARGET_TEMPLATE [`optional input grid`] : Target System. use this grid's system for output grids
    - TARGET_GRID [`output grid`] : Target
    - XFIELD [`table field`] : x Position
    - YFIELD [`table field`] : y Position
    - METHOD [`choice`] : Method. Available Choices: [0] Automatic [1] Triangulation [2] Spline [3] Affine [4] 1st Order Polynomial [5] 2nd Order Polynomial [6] 3rd Order Polynomial [7] Polynomial, Order Default: 0
    - ORDER [`integer number`] : Polynomial Order. Minimum: 1 Default: 3
    - RESAMPLING [`choice`] : Resampling. Available Choices: [0] Nearest Neighbour [1] Bilinear Interpolation [2] Bicubic Spline Interpolation [3] B-Spline Interpolation Default: 3
    - BYTEWISE [`boolean`] : Bytewise Interpolation. Default: 0
    - DATA_TYPE [`data type`] : Data Type. Available Choices: [0] unsigned 1 byte integer [1] signed 1 byte integer [2] unsigned 2 byte integer [3] signed 2 byte integer [4] unsigned 4 byte integer [5] signed 4 byte integer [6] unsigned 8 byte integer [7] signed 8 byte integer [8] 4 byte floating point number [9] 8 byte floating point number [10] Preserve Default: 10
    - CRS_STRING [`text`] : Coordinate System Definition. Supported formats comprise PROJ and WKT strings, object codes (e.g. "EPSG:4326").
    - TARGET_DEFINITION [`choice`] : Target Grid System. Available Choices: [0] user defined [1] grid or grid system Default: 0
    - TARGET_USER_SIZE [`floating point number`] : Cellsize. Minimum: 0.000000 Default: 1.000000
    - TARGET_USER_XMIN [`floating point number`] : West. Default: 0.000000
    - TARGET_USER_XMAX [`floating point number`] : East. Default: 100.000000
    - TARGET_USER_YMIN [`floating point number`] : South. Default: 0.000000
    - TARGET_USER_YMAX [`floating point number`] : North. Default: 100.000000
    - TARGET_USER_COLS [`integer number`] : Columns. Minimum: 1 Default: 101 Number of cells in East-West direction.
    - TARGET_USER_ROWS [`integer number`] : Rows. Minimum: 1 Default: 101 Number of cells in North-South direction.
    - TARGET_USER_FLAT [`boolean`] : Rounding. Default: 1 Round bounding coordinates to multiples of cell size. Ignored if cell size has decimal places.
    - TARGET_USER_FITS [`choice`] : Fit. Available Choices: [0] nodes [1] cells Default: 0
    - TARGET_SYSTEM [`grid system`] : Grid System

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('pj_georeference', '1', 'Rectify Grid')
    if Tool.is_Okay():
        Tool.Set_Input ('REF_SOURCE', REF_SOURCE)
        Tool.Set_Input ('GRID', GRID)
        Tool.Set_Input ('REF_TARGET', REF_TARGET)
        Tool.Set_Input ('TARGET_TEMPLATE', TARGET_TEMPLATE)
        Tool.Set_Output('TARGET_GRID', TARGET_GRID)
        Tool.Set_Option('XFIELD', XFIELD)
        Tool.Set_Option('YFIELD', YFIELD)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('ORDER', ORDER)
        Tool.Set_Option('RESAMPLING', RESAMPLING)
        Tool.Set_Option('BYTEWISE', BYTEWISE)
        Tool.Set_Option('DATA_TYPE', DATA_TYPE)
        Tool.Set_Option('CRS_STRING', CRS_STRING)
        Tool.Set_Option('TARGET_DEFINITION', TARGET_DEFINITION)
        Tool.Set_Option('TARGET_USER_SIZE', TARGET_USER_SIZE)
        Tool.Set_Option('TARGET_USER_XMIN', TARGET_USER_XMIN)
        Tool.Set_Option('TARGET_USER_XMAX', TARGET_USER_XMAX)
        Tool.Set_Option('TARGET_USER_YMIN', TARGET_USER_YMIN)
        Tool.Set_Option('TARGET_USER_YMAX', TARGET_USER_YMAX)
        Tool.Set_Option('TARGET_USER_COLS', TARGET_USER_COLS)
        Tool.Set_Option('TARGET_USER_ROWS', TARGET_USER_ROWS)
        Tool.Set_Option('TARGET_USER_FLAT', TARGET_USER_FLAT)
        Tool.Set_Option('TARGET_USER_FITS', TARGET_USER_FITS)
        Tool.Set_Option('TARGET_SYSTEM', TARGET_SYSTEM)
        return Tool.Execute(Verbose)
    return False

def Warping_Shapes(REF_SOURCE=None, INPUT=None, REF_TARGET=None, OUTPUT=None, XFIELD=None, YFIELD=None, METHOD=None, ORDER=None, Verbose=2):
    '''
    Warping Shapes
    ----------
    [pj_georeference.2]\n
    Georeferencing of shapes layers. Either choose the attribute fields (x/y) with the projected coordinates for the reference points (origin) or supply a additional points layer with correspondent points in the target projection.\n
    Arguments
    ----------
    - REF_SOURCE [`input shapes`] : Reference Points (Origin)
    - INPUT [`input shapes`] : Input
    - REF_TARGET [`optional input shapes`] : Reference Points (Projection)
    - OUTPUT [`output shapes`] : Output
    - XFIELD [`table field`] : x Position
    - YFIELD [`table field`] : y Position
    - METHOD [`choice`] : Method. Available Choices: [0] Automatic [1] Triangulation [2] Spline [3] Affine [4] 1st Order Polynomial [5] 2nd Order Polynomial [6] 3rd Order Polynomial [7] Polynomial, Order Default: 0
    - ORDER [`integer number`] : Polynomial Order. Minimum: 1 Default: 3

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('pj_georeference', '2', 'Warping Shapes')
    if Tool.is_Okay():
        Tool.Set_Input ('REF_SOURCE', REF_SOURCE)
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Input ('REF_TARGET', REF_TARGET)
        Tool.Set_Output('OUTPUT', OUTPUT)
        Tool.Set_Option('XFIELD', XFIELD)
        Tool.Set_Option('YFIELD', YFIELD)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('ORDER', ORDER)
        return Tool.Execute(Verbose)
    return False

def run_tool_pj_georeference_2(REF_SOURCE=None, INPUT=None, REF_TARGET=None, OUTPUT=None, XFIELD=None, YFIELD=None, METHOD=None, ORDER=None, Verbose=2):
    '''
    Warping Shapes
    ----------
    [pj_georeference.2]\n
    Georeferencing of shapes layers. Either choose the attribute fields (x/y) with the projected coordinates for the reference points (origin) or supply a additional points layer with correspondent points in the target projection.\n
    Arguments
    ----------
    - REF_SOURCE [`input shapes`] : Reference Points (Origin)
    - INPUT [`input shapes`] : Input
    - REF_TARGET [`optional input shapes`] : Reference Points (Projection)
    - OUTPUT [`output shapes`] : Output
    - XFIELD [`table field`] : x Position
    - YFIELD [`table field`] : y Position
    - METHOD [`choice`] : Method. Available Choices: [0] Automatic [1] Triangulation [2] Spline [3] Affine [4] 1st Order Polynomial [5] 2nd Order Polynomial [6] 3rd Order Polynomial [7] Polynomial, Order Default: 0
    - ORDER [`integer number`] : Polynomial Order. Minimum: 1 Default: 3

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('pj_georeference', '2', 'Warping Shapes')
    if Tool.is_Okay():
        Tool.Set_Input ('REF_SOURCE', REF_SOURCE)
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Input ('REF_TARGET', REF_TARGET)
        Tool.Set_Output('OUTPUT', OUTPUT)
        Tool.Set_Option('XFIELD', XFIELD)
        Tool.Set_Option('YFIELD', YFIELD)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('ORDER', ORDER)
        return Tool.Execute(Verbose)
    return False

def Direct_Georeferencing_of_Airborne_Photographs(INPUT=None, DEM=None, TARGET_TEMPLATE=None, OUTPUT=None, EXTENT=None, DEM_GRIDSYSTEM=None, DEM_DEFAULT=None, CFL=None, PXSIZE=None, X=None, Y=None, Z=None, OMEGA=None, PHI=None, KAPPA=None, KAPPA_OFF=None, ORIENTATION=None, ROW_ORDER=None, RESAMPLING=None, DATA_TYPE=None, TARGET_DEFINITION=None, TARGET_USER_SIZE=None, TARGET_USER_XMIN=None, TARGET_USER_XMAX=None, TARGET_USER_YMIN=None, TARGET_USER_YMAX=None, TARGET_USER_COLS=None, TARGET_USER_ROWS=None, TARGET_USER_FLAT=None, TARGET_USER_FITS=None, TARGET_SYSTEM=None, Verbose=2):
    '''
    Direct Georeferencing of Airborne Photographs
    ----------
    [pj_georeference.4]\n
    Direct georeferencing of aerial photographs uses extrinsic (position, altitude) and intrinsic (focal length, physical pixel size) camera parameters. Orthorectification routine supports additional data from a Digital Elevation Model (DEM).\n
    Arguments
    ----------
    - INPUT [`input grid list`] : Unreferenced Grids
    - DEM [`optional input grid`] : Elevation
    - TARGET_TEMPLATE [`optional input grid`] : Target System. use this grid's system for output grids
    - OUTPUT [`output grid list`] : Referenced Grids
    - EXTENT [`output shapes`] : Extent
    - DEM_GRIDSYSTEM [`grid system`] : Grid system
    - DEM_DEFAULT [`floating point number`] : Default. Default: 0.000000 default value if no grid has been selected
    - CFL [`floating point number`] : Focal Length [mm]. Minimum: 0.000000 Default: 80.000000
    - PXSIZE [`floating point number`] : CCD Physical Pixel Size [micron]. Minimum: 0.000000 Default: 5.200000
    - X [`floating point number`] : X. Default: 0.000000
    - Y [`floating point number`] : Y. Default: 0.000000
    - Z [`floating point number`] : Z. Default: 1000.000000
    - OMEGA [`floating point number`] : Omega. Default: 0.000000 X axis rotation angle [degree] (roll)
    - PHI [`floating point number`] : Phi. Default: 0.000000 Y axis rotation angle [degree] (pitch)
    - KAPPA [`floating point number`] : Kappa. Default: 0.000000 Z axis rotation angle [degree] (heading)
    - KAPPA_OFF [`floating point number`] : Offset. Default: 90.000000 origin adjustment angle [degree] for kappa (Z axis, heading)
    - ORIENTATION [`choice`] : Orientation. Available Choices: [0] BLUH [1] PATB Default: 0
    - ROW_ORDER [`choice`] : Row Order. Available Choices: [0] top down [1] bottom up Default: 0
    - RESAMPLING [`choice`] : Resampling. Available Choices: [0] Nearest Neighbour [1] Bilinear Interpolation [2] Bicubic Spline Interpolation [3] B-Spline Interpolation Default: 3
    - DATA_TYPE [`data type`] : Data Type. Available Choices: [0] unsigned 1 byte integer [1] signed 1 byte integer [2] unsigned 2 byte integer [3] signed 2 byte integer [4] unsigned 4 byte integer [5] signed 4 byte integer [6] unsigned 8 byte integer [7] signed 8 byte integer [8] 4 byte floating point number [9] 8 byte floating point number [10] same as original Default: 10
    - TARGET_DEFINITION [`choice`] : Target Grid System. Available Choices: [0] user defined [1] grid or grid system Default: 0
    - TARGET_USER_SIZE [`floating point number`] : Cellsize. Minimum: 0.000000 Default: 1.000000
    - TARGET_USER_XMIN [`floating point number`] : West. Default: 0.000000
    - TARGET_USER_XMAX [`floating point number`] : East. Default: 100.000000
    - TARGET_USER_YMIN [`floating point number`] : South. Default: 0.000000
    - TARGET_USER_YMAX [`floating point number`] : North. Default: 100.000000
    - TARGET_USER_COLS [`integer number`] : Columns. Minimum: 1 Default: 101 Number of cells in East-West direction.
    - TARGET_USER_ROWS [`integer number`] : Rows. Minimum: 1 Default: 101 Number of cells in North-South direction.
    - TARGET_USER_FLAT [`boolean`] : Rounding. Default: 1 Round bounding coordinates to multiples of cell size. Ignored if cell size has decimal places.
    - TARGET_USER_FITS [`choice`] : Fit. Available Choices: [0] nodes [1] cells Default: 0
    - TARGET_SYSTEM [`grid system`] : Grid System

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('pj_georeference', '4', 'Direct Georeferencing of Airborne Photographs')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Input ('DEM', DEM)
        Tool.Set_Input ('TARGET_TEMPLATE', TARGET_TEMPLATE)
        Tool.Set_Output('OUTPUT', OUTPUT)
        Tool.Set_Output('EXTENT', EXTENT)
        Tool.Set_Option('DEM_GRIDSYSTEM', DEM_GRIDSYSTEM)
        Tool.Set_Option('DEM_DEFAULT', DEM_DEFAULT)
        Tool.Set_Option('CFL', CFL)
        Tool.Set_Option('PXSIZE', PXSIZE)
        Tool.Set_Option('X', X)
        Tool.Set_Option('Y', Y)
        Tool.Set_Option('Z', Z)
        Tool.Set_Option('OMEGA', OMEGA)
        Tool.Set_Option('PHI', PHI)
        Tool.Set_Option('KAPPA', KAPPA)
        Tool.Set_Option('KAPPA_OFF', KAPPA_OFF)
        Tool.Set_Option('ORIENTATION', ORIENTATION)
        Tool.Set_Option('ROW_ORDER', ROW_ORDER)
        Tool.Set_Option('RESAMPLING', RESAMPLING)
        Tool.Set_Option('DATA_TYPE', DATA_TYPE)
        Tool.Set_Option('TARGET_DEFINITION', TARGET_DEFINITION)
        Tool.Set_Option('TARGET_USER_SIZE', TARGET_USER_SIZE)
        Tool.Set_Option('TARGET_USER_XMIN', TARGET_USER_XMIN)
        Tool.Set_Option('TARGET_USER_XMAX', TARGET_USER_XMAX)
        Tool.Set_Option('TARGET_USER_YMIN', TARGET_USER_YMIN)
        Tool.Set_Option('TARGET_USER_YMAX', TARGET_USER_YMAX)
        Tool.Set_Option('TARGET_USER_COLS', TARGET_USER_COLS)
        Tool.Set_Option('TARGET_USER_ROWS', TARGET_USER_ROWS)
        Tool.Set_Option('TARGET_USER_FLAT', TARGET_USER_FLAT)
        Tool.Set_Option('TARGET_USER_FITS', TARGET_USER_FITS)
        Tool.Set_Option('TARGET_SYSTEM', TARGET_SYSTEM)
        return Tool.Execute(Verbose)
    return False

def run_tool_pj_georeference_4(INPUT=None, DEM=None, TARGET_TEMPLATE=None, OUTPUT=None, EXTENT=None, DEM_GRIDSYSTEM=None, DEM_DEFAULT=None, CFL=None, PXSIZE=None, X=None, Y=None, Z=None, OMEGA=None, PHI=None, KAPPA=None, KAPPA_OFF=None, ORIENTATION=None, ROW_ORDER=None, RESAMPLING=None, DATA_TYPE=None, TARGET_DEFINITION=None, TARGET_USER_SIZE=None, TARGET_USER_XMIN=None, TARGET_USER_XMAX=None, TARGET_USER_YMIN=None, TARGET_USER_YMAX=None, TARGET_USER_COLS=None, TARGET_USER_ROWS=None, TARGET_USER_FLAT=None, TARGET_USER_FITS=None, TARGET_SYSTEM=None, Verbose=2):
    '''
    Direct Georeferencing of Airborne Photographs
    ----------
    [pj_georeference.4]\n
    Direct georeferencing of aerial photographs uses extrinsic (position, altitude) and intrinsic (focal length, physical pixel size) camera parameters. Orthorectification routine supports additional data from a Digital Elevation Model (DEM).\n
    Arguments
    ----------
    - INPUT [`input grid list`] : Unreferenced Grids
    - DEM [`optional input grid`] : Elevation
    - TARGET_TEMPLATE [`optional input grid`] : Target System. use this grid's system for output grids
    - OUTPUT [`output grid list`] : Referenced Grids
    - EXTENT [`output shapes`] : Extent
    - DEM_GRIDSYSTEM [`grid system`] : Grid system
    - DEM_DEFAULT [`floating point number`] : Default. Default: 0.000000 default value if no grid has been selected
    - CFL [`floating point number`] : Focal Length [mm]. Minimum: 0.000000 Default: 80.000000
    - PXSIZE [`floating point number`] : CCD Physical Pixel Size [micron]. Minimum: 0.000000 Default: 5.200000
    - X [`floating point number`] : X. Default: 0.000000
    - Y [`floating point number`] : Y. Default: 0.000000
    - Z [`floating point number`] : Z. Default: 1000.000000
    - OMEGA [`floating point number`] : Omega. Default: 0.000000 X axis rotation angle [degree] (roll)
    - PHI [`floating point number`] : Phi. Default: 0.000000 Y axis rotation angle [degree] (pitch)
    - KAPPA [`floating point number`] : Kappa. Default: 0.000000 Z axis rotation angle [degree] (heading)
    - KAPPA_OFF [`floating point number`] : Offset. Default: 90.000000 origin adjustment angle [degree] for kappa (Z axis, heading)
    - ORIENTATION [`choice`] : Orientation. Available Choices: [0] BLUH [1] PATB Default: 0
    - ROW_ORDER [`choice`] : Row Order. Available Choices: [0] top down [1] bottom up Default: 0
    - RESAMPLING [`choice`] : Resampling. Available Choices: [0] Nearest Neighbour [1] Bilinear Interpolation [2] Bicubic Spline Interpolation [3] B-Spline Interpolation Default: 3
    - DATA_TYPE [`data type`] : Data Type. Available Choices: [0] unsigned 1 byte integer [1] signed 1 byte integer [2] unsigned 2 byte integer [3] signed 2 byte integer [4] unsigned 4 byte integer [5] signed 4 byte integer [6] unsigned 8 byte integer [7] signed 8 byte integer [8] 4 byte floating point number [9] 8 byte floating point number [10] same as original Default: 10
    - TARGET_DEFINITION [`choice`] : Target Grid System. Available Choices: [0] user defined [1] grid or grid system Default: 0
    - TARGET_USER_SIZE [`floating point number`] : Cellsize. Minimum: 0.000000 Default: 1.000000
    - TARGET_USER_XMIN [`floating point number`] : West. Default: 0.000000
    - TARGET_USER_XMAX [`floating point number`] : East. Default: 100.000000
    - TARGET_USER_YMIN [`floating point number`] : South. Default: 0.000000
    - TARGET_USER_YMAX [`floating point number`] : North. Default: 100.000000
    - TARGET_USER_COLS [`integer number`] : Columns. Minimum: 1 Default: 101 Number of cells in East-West direction.
    - TARGET_USER_ROWS [`integer number`] : Rows. Minimum: 1 Default: 101 Number of cells in North-South direction.
    - TARGET_USER_FLAT [`boolean`] : Rounding. Default: 1 Round bounding coordinates to multiples of cell size. Ignored if cell size has decimal places.
    - TARGET_USER_FITS [`choice`] : Fit. Available Choices: [0] nodes [1] cells Default: 0
    - TARGET_SYSTEM [`grid system`] : Grid System

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('pj_georeference', '4', 'Direct Georeferencing of Airborne Photographs')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Input ('DEM', DEM)
        Tool.Set_Input ('TARGET_TEMPLATE', TARGET_TEMPLATE)
        Tool.Set_Output('OUTPUT', OUTPUT)
        Tool.Set_Output('EXTENT', EXTENT)
        Tool.Set_Option('DEM_GRIDSYSTEM', DEM_GRIDSYSTEM)
        Tool.Set_Option('DEM_DEFAULT', DEM_DEFAULT)
        Tool.Set_Option('CFL', CFL)
        Tool.Set_Option('PXSIZE', PXSIZE)
        Tool.Set_Option('X', X)
        Tool.Set_Option('Y', Y)
        Tool.Set_Option('Z', Z)
        Tool.Set_Option('OMEGA', OMEGA)
        Tool.Set_Option('PHI', PHI)
        Tool.Set_Option('KAPPA', KAPPA)
        Tool.Set_Option('KAPPA_OFF', KAPPA_OFF)
        Tool.Set_Option('ORIENTATION', ORIENTATION)
        Tool.Set_Option('ROW_ORDER', ROW_ORDER)
        Tool.Set_Option('RESAMPLING', RESAMPLING)
        Tool.Set_Option('DATA_TYPE', DATA_TYPE)
        Tool.Set_Option('TARGET_DEFINITION', TARGET_DEFINITION)
        Tool.Set_Option('TARGET_USER_SIZE', TARGET_USER_SIZE)
        Tool.Set_Option('TARGET_USER_XMIN', TARGET_USER_XMIN)
        Tool.Set_Option('TARGET_USER_XMAX', TARGET_USER_XMAX)
        Tool.Set_Option('TARGET_USER_YMIN', TARGET_USER_YMIN)
        Tool.Set_Option('TARGET_USER_YMAX', TARGET_USER_YMAX)
        Tool.Set_Option('TARGET_USER_COLS', TARGET_USER_COLS)
        Tool.Set_Option('TARGET_USER_ROWS', TARGET_USER_ROWS)
        Tool.Set_Option('TARGET_USER_FLAT', TARGET_USER_FLAT)
        Tool.Set_Option('TARGET_USER_FITS', TARGET_USER_FITS)
        Tool.Set_Option('TARGET_SYSTEM', TARGET_SYSTEM)
        return Tool.Execute(Verbose)
    return False

def Define_Georeference_for_Grids(GRIDS=None, REFERENCED=None, DEFINITION=None, SYSTEM=None, SIZE=None, XMIN=None, XMAX=None, YMIN=None, YMAX=None, CELL_REF=None, Verbose=2):
    '''
    Define Georeference for Grids
    ----------
    [pj_georeference.5]\n
    This tool simply allows definition of grid's cellsize and position. It does not perform any kind of warping but might be helpful, if the grid has lost this information or is already aligned with the coordinate system.\n
    Arguments
    ----------
    - GRIDS [`input grid list`] : Grids
    - REFERENCED [`output grid list`] : Referenced Grids
    - DEFINITION [`choice`] : Definition. Available Choices: [0] cellsize and lower left cell coordinates [1] cellsize and upper left cell coordinates [2] lower left cell coordinates and left to right range [3] lower left cell coordinates and lower to upper range Default: 0
    - SYSTEM [`grid system`] : Grid System
    - SIZE [`floating point number`] : Cellsize. Minimum: 0.000000 Default: 1.000000
    - XMIN [`floating point number`] : Left. Default: 0.000000
    - XMAX [`floating point number`] : Right. Default: 0.000000
    - YMIN [`floating point number`] : Lower. Default: 0.000000
    - YMAX [`floating point number`] : Upper. Default: 0.000000
    - CELL_REF [`choice`] : Cell Reference. Available Choices: [0] center [1] corner Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('pj_georeference', '5', 'Define Georeference for Grids')
    if Tool.is_Okay():
        Tool.Set_Input ('GRIDS', GRIDS)
        Tool.Set_Output('REFERENCED', REFERENCED)
        Tool.Set_Option('DEFINITION', DEFINITION)
        Tool.Set_Option('SYSTEM', SYSTEM)
        Tool.Set_Option('SIZE', SIZE)
        Tool.Set_Option('XMIN', XMIN)
        Tool.Set_Option('XMAX', XMAX)
        Tool.Set_Option('YMIN', YMIN)
        Tool.Set_Option('YMAX', YMAX)
        Tool.Set_Option('CELL_REF', CELL_REF)
        return Tool.Execute(Verbose)
    return False

def run_tool_pj_georeference_5(GRIDS=None, REFERENCED=None, DEFINITION=None, SYSTEM=None, SIZE=None, XMIN=None, XMAX=None, YMIN=None, YMAX=None, CELL_REF=None, Verbose=2):
    '''
    Define Georeference for Grids
    ----------
    [pj_georeference.5]\n
    This tool simply allows definition of grid's cellsize and position. It does not perform any kind of warping but might be helpful, if the grid has lost this information or is already aligned with the coordinate system.\n
    Arguments
    ----------
    - GRIDS [`input grid list`] : Grids
    - REFERENCED [`output grid list`] : Referenced Grids
    - DEFINITION [`choice`] : Definition. Available Choices: [0] cellsize and lower left cell coordinates [1] cellsize and upper left cell coordinates [2] lower left cell coordinates and left to right range [3] lower left cell coordinates and lower to upper range Default: 0
    - SYSTEM [`grid system`] : Grid System
    - SIZE [`floating point number`] : Cellsize. Minimum: 0.000000 Default: 1.000000
    - XMIN [`floating point number`] : Left. Default: 0.000000
    - XMAX [`floating point number`] : Right. Default: 0.000000
    - YMIN [`floating point number`] : Lower. Default: 0.000000
    - YMAX [`floating point number`] : Upper. Default: 0.000000
    - CELL_REF [`choice`] : Cell Reference. Available Choices: [0] center [1] corner Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('pj_georeference', '5', 'Define Georeference for Grids')
    if Tool.is_Okay():
        Tool.Set_Input ('GRIDS', GRIDS)
        Tool.Set_Output('REFERENCED', REFERENCED)
        Tool.Set_Option('DEFINITION', DEFINITION)
        Tool.Set_Option('SYSTEM', SYSTEM)
        Tool.Set_Option('SIZE', SIZE)
        Tool.Set_Option('XMIN', XMIN)
        Tool.Set_Option('XMAX', XMAX)
        Tool.Set_Option('YMIN', YMIN)
        Tool.Set_Option('YMAX', YMAX)
        Tool.Set_Option('CELL_REF', CELL_REF)
        return Tool.Execute(Verbose)
    return False

def World_File_from_Flight_and_Camera_Settings(EXTENT=None, FILE=None, NX=None, NY=None, CFL=None, PXSIZE=None, X=None, Y=None, Z=None, OMEGA=None, PHI=None, KAPPA=None, KAPPA_OFF=None, ORIENTATION=None, Verbose=2):
    '''
    World File from Flight and Camera Settings
    ----------
    [pj_georeference.6]\n
    Creates a world file (RST = rotation, scaling, translation) for georeferencing images by direct georeferencing. Direct georeferencing uses extrinsic (position, attitude) and intrinsic (focal length, physical pixel size) camera parameters.\n
    Arguments
    ----------
    - EXTENT [`output shapes`] : Extent
    - FILE [`file path`] : World File
    - NX [`integer number`] : Number of Columns. Minimum: 1 Default: 100
    - NY [`integer number`] : Number of Rows. Minimum: 1 Default: 100
    - CFL [`floating point number`] : Focal Length [mm]. Minimum: 0.000000 Default: 80.000000
    - PXSIZE [`floating point number`] : CCD Physical Pixel Size [micron]. Minimum: 0.000000 Default: 5.200000
    - X [`floating point number`] : X. Default: 0.000000
    - Y [`floating point number`] : Y. Default: 0.000000
    - Z [`floating point number`] : Z. Default: 1000.000000
    - OMEGA [`floating point number`] : Omega. Default: 0.000000 X axis rotation angle [degree] (roll)
    - PHI [`floating point number`] : Phi. Default: 0.000000 Y axis rotation angle [degree] (pitch)
    - KAPPA [`floating point number`] : Kappa. Default: 0.000000 Z axis rotation angle [degree] (heading)
    - KAPPA_OFF [`floating point number`] : Offset. Default: 90.000000 origin adjustment angle [degree] for kappa (Z axis, heading)
    - ORIENTATION [`choice`] : Orientation. Available Choices: [0] BLUH [1] PATB Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('pj_georeference', '6', 'World File from Flight and Camera Settings')
    if Tool.is_Okay():
        Tool.Set_Output('EXTENT', EXTENT)
        Tool.Set_Option('FILE', FILE)
        Tool.Set_Option('NX', NX)
        Tool.Set_Option('NY', NY)
        Tool.Set_Option('CFL', CFL)
        Tool.Set_Option('PXSIZE', PXSIZE)
        Tool.Set_Option('X', X)
        Tool.Set_Option('Y', Y)
        Tool.Set_Option('Z', Z)
        Tool.Set_Option('OMEGA', OMEGA)
        Tool.Set_Option('PHI', PHI)
        Tool.Set_Option('KAPPA', KAPPA)
        Tool.Set_Option('KAPPA_OFF', KAPPA_OFF)
        Tool.Set_Option('ORIENTATION', ORIENTATION)
        return Tool.Execute(Verbose)
    return False

def run_tool_pj_georeference_6(EXTENT=None, FILE=None, NX=None, NY=None, CFL=None, PXSIZE=None, X=None, Y=None, Z=None, OMEGA=None, PHI=None, KAPPA=None, KAPPA_OFF=None, ORIENTATION=None, Verbose=2):
    '''
    World File from Flight and Camera Settings
    ----------
    [pj_georeference.6]\n
    Creates a world file (RST = rotation, scaling, translation) for georeferencing images by direct georeferencing. Direct georeferencing uses extrinsic (position, attitude) and intrinsic (focal length, physical pixel size) camera parameters.\n
    Arguments
    ----------
    - EXTENT [`output shapes`] : Extent
    - FILE [`file path`] : World File
    - NX [`integer number`] : Number of Columns. Minimum: 1 Default: 100
    - NY [`integer number`] : Number of Rows. Minimum: 1 Default: 100
    - CFL [`floating point number`] : Focal Length [mm]. Minimum: 0.000000 Default: 80.000000
    - PXSIZE [`floating point number`] : CCD Physical Pixel Size [micron]. Minimum: 0.000000 Default: 5.200000
    - X [`floating point number`] : X. Default: 0.000000
    - Y [`floating point number`] : Y. Default: 0.000000
    - Z [`floating point number`] : Z. Default: 1000.000000
    - OMEGA [`floating point number`] : Omega. Default: 0.000000 X axis rotation angle [degree] (roll)
    - PHI [`floating point number`] : Phi. Default: 0.000000 Y axis rotation angle [degree] (pitch)
    - KAPPA [`floating point number`] : Kappa. Default: 0.000000 Z axis rotation angle [degree] (heading)
    - KAPPA_OFF [`floating point number`] : Offset. Default: 90.000000 origin adjustment angle [degree] for kappa (Z axis, heading)
    - ORIENTATION [`choice`] : Orientation. Available Choices: [0] BLUH [1] PATB Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('pj_georeference', '6', 'World File from Flight and Camera Settings')
    if Tool.is_Okay():
        Tool.Set_Output('EXTENT', EXTENT)
        Tool.Set_Option('FILE', FILE)
        Tool.Set_Option('NX', NX)
        Tool.Set_Option('NY', NY)
        Tool.Set_Option('CFL', CFL)
        Tool.Set_Option('PXSIZE', PXSIZE)
        Tool.Set_Option('X', X)
        Tool.Set_Option('Y', Y)
        Tool.Set_Option('Z', Z)
        Tool.Set_Option('OMEGA', OMEGA)
        Tool.Set_Option('PHI', PHI)
        Tool.Set_Option('KAPPA', KAPPA)
        Tool.Set_Option('KAPPA_OFF', KAPPA_OFF)
        Tool.Set_Option('ORIENTATION', ORIENTATION)
        return Tool.Execute(Verbose)
    return False

def Georeference_with_Coordinate_Grids(GRID_X=None, GRID_Y=None, GRIDS=None, TARGET_TEMPLATE=None, OUTPUT=None, RESAMPLING=None, BYTEWISE=None, KEEP_TYPE=None, TARGET_DEFINITION=None, TARGET_USER_SIZE=None, TARGET_USER_XMIN=None, TARGET_USER_XMAX=None, TARGET_USER_YMIN=None, TARGET_USER_YMAX=None, TARGET_USER_COLS=None, TARGET_USER_ROWS=None, TARGET_USER_FLAT=None, TARGET_USER_FITS=None, TARGET_SYSTEM=None, Verbose=2):
    '''
    Georeference with Coordinate Grids
    ----------
    [pj_georeference.7]\n
    Georeferencing grids of grids two coordinate grids (x/y) that provide for each grid cell the targeted coordinate.\n
    Arguments
    ----------
    - GRID_X [`input grid`] : X Coordinates
    - GRID_Y [`input grid`] : Y Coordinates
    - GRIDS [`input grid list`] : Grids
    - TARGET_TEMPLATE [`optional input grid`] : Target System. use this grid's system for output grids
    - OUTPUT [`output grid list`] : Grids
    - RESAMPLING [`choice`] : Resampling. Available Choices: [0] Nearest Neighbour [1] Bilinear Interpolation [2] Bicubic Spline Interpolation [3] B-Spline Interpolation Default: 3
    - BYTEWISE [`boolean`] : Bytewise Interpolation. Default: 0
    - KEEP_TYPE [`boolean`] : Preserve Data Type. Default: 0
    - TARGET_DEFINITION [`choice`] : Target Grid System. Available Choices: [0] user defined [1] grid or grid system Default: 0
    - TARGET_USER_SIZE [`floating point number`] : Cellsize. Minimum: 0.000000 Default: 1.000000
    - TARGET_USER_XMIN [`floating point number`] : West. Default: 0.000000
    - TARGET_USER_XMAX [`floating point number`] : East. Default: 100.000000
    - TARGET_USER_YMIN [`floating point number`] : South. Default: 0.000000
    - TARGET_USER_YMAX [`floating point number`] : North. Default: 100.000000
    - TARGET_USER_COLS [`integer number`] : Columns. Minimum: 1 Default: 101 Number of cells in East-West direction.
    - TARGET_USER_ROWS [`integer number`] : Rows. Minimum: 1 Default: 101 Number of cells in North-South direction.
    - TARGET_USER_FLAT [`boolean`] : Rounding. Default: 1 Round bounding coordinates to multiples of cell size. Ignored if cell size has decimal places.
    - TARGET_USER_FITS [`choice`] : Fit. Available Choices: [0] nodes [1] cells Default: 0
    - TARGET_SYSTEM [`grid system`] : Grid System

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('pj_georeference', '7', 'Georeference with Coordinate Grids')
    if Tool.is_Okay():
        Tool.Set_Input ('GRID_X', GRID_X)
        Tool.Set_Input ('GRID_Y', GRID_Y)
        Tool.Set_Input ('GRIDS', GRIDS)
        Tool.Set_Input ('TARGET_TEMPLATE', TARGET_TEMPLATE)
        Tool.Set_Output('OUTPUT', OUTPUT)
        Tool.Set_Option('RESAMPLING', RESAMPLING)
        Tool.Set_Option('BYTEWISE', BYTEWISE)
        Tool.Set_Option('KEEP_TYPE', KEEP_TYPE)
        Tool.Set_Option('TARGET_DEFINITION', TARGET_DEFINITION)
        Tool.Set_Option('TARGET_USER_SIZE', TARGET_USER_SIZE)
        Tool.Set_Option('TARGET_USER_XMIN', TARGET_USER_XMIN)
        Tool.Set_Option('TARGET_USER_XMAX', TARGET_USER_XMAX)
        Tool.Set_Option('TARGET_USER_YMIN', TARGET_USER_YMIN)
        Tool.Set_Option('TARGET_USER_YMAX', TARGET_USER_YMAX)
        Tool.Set_Option('TARGET_USER_COLS', TARGET_USER_COLS)
        Tool.Set_Option('TARGET_USER_ROWS', TARGET_USER_ROWS)
        Tool.Set_Option('TARGET_USER_FLAT', TARGET_USER_FLAT)
        Tool.Set_Option('TARGET_USER_FITS', TARGET_USER_FITS)
        Tool.Set_Option('TARGET_SYSTEM', TARGET_SYSTEM)
        return Tool.Execute(Verbose)
    return False

def run_tool_pj_georeference_7(GRID_X=None, GRID_Y=None, GRIDS=None, TARGET_TEMPLATE=None, OUTPUT=None, RESAMPLING=None, BYTEWISE=None, KEEP_TYPE=None, TARGET_DEFINITION=None, TARGET_USER_SIZE=None, TARGET_USER_XMIN=None, TARGET_USER_XMAX=None, TARGET_USER_YMIN=None, TARGET_USER_YMAX=None, TARGET_USER_COLS=None, TARGET_USER_ROWS=None, TARGET_USER_FLAT=None, TARGET_USER_FITS=None, TARGET_SYSTEM=None, Verbose=2):
    '''
    Georeference with Coordinate Grids
    ----------
    [pj_georeference.7]\n
    Georeferencing grids of grids two coordinate grids (x/y) that provide for each grid cell the targeted coordinate.\n
    Arguments
    ----------
    - GRID_X [`input grid`] : X Coordinates
    - GRID_Y [`input grid`] : Y Coordinates
    - GRIDS [`input grid list`] : Grids
    - TARGET_TEMPLATE [`optional input grid`] : Target System. use this grid's system for output grids
    - OUTPUT [`output grid list`] : Grids
    - RESAMPLING [`choice`] : Resampling. Available Choices: [0] Nearest Neighbour [1] Bilinear Interpolation [2] Bicubic Spline Interpolation [3] B-Spline Interpolation Default: 3
    - BYTEWISE [`boolean`] : Bytewise Interpolation. Default: 0
    - KEEP_TYPE [`boolean`] : Preserve Data Type. Default: 0
    - TARGET_DEFINITION [`choice`] : Target Grid System. Available Choices: [0] user defined [1] grid or grid system Default: 0
    - TARGET_USER_SIZE [`floating point number`] : Cellsize. Minimum: 0.000000 Default: 1.000000
    - TARGET_USER_XMIN [`floating point number`] : West. Default: 0.000000
    - TARGET_USER_XMAX [`floating point number`] : East. Default: 100.000000
    - TARGET_USER_YMIN [`floating point number`] : South. Default: 0.000000
    - TARGET_USER_YMAX [`floating point number`] : North. Default: 100.000000
    - TARGET_USER_COLS [`integer number`] : Columns. Minimum: 1 Default: 101 Number of cells in East-West direction.
    - TARGET_USER_ROWS [`integer number`] : Rows. Minimum: 1 Default: 101 Number of cells in North-South direction.
    - TARGET_USER_FLAT [`boolean`] : Rounding. Default: 1 Round bounding coordinates to multiples of cell size. Ignored if cell size has decimal places.
    - TARGET_USER_FITS [`choice`] : Fit. Available Choices: [0] nodes [1] cells Default: 0
    - TARGET_SYSTEM [`grid system`] : Grid System

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('pj_georeference', '7', 'Georeference with Coordinate Grids')
    if Tool.is_Okay():
        Tool.Set_Input ('GRID_X', GRID_X)
        Tool.Set_Input ('GRID_Y', GRID_Y)
        Tool.Set_Input ('GRIDS', GRIDS)
        Tool.Set_Input ('TARGET_TEMPLATE', TARGET_TEMPLATE)
        Tool.Set_Output('OUTPUT', OUTPUT)
        Tool.Set_Option('RESAMPLING', RESAMPLING)
        Tool.Set_Option('BYTEWISE', BYTEWISE)
        Tool.Set_Option('KEEP_TYPE', KEEP_TYPE)
        Tool.Set_Option('TARGET_DEFINITION', TARGET_DEFINITION)
        Tool.Set_Option('TARGET_USER_SIZE', TARGET_USER_SIZE)
        Tool.Set_Option('TARGET_USER_XMIN', TARGET_USER_XMIN)
        Tool.Set_Option('TARGET_USER_XMAX', TARGET_USER_XMAX)
        Tool.Set_Option('TARGET_USER_YMIN', TARGET_USER_YMIN)
        Tool.Set_Option('TARGET_USER_YMAX', TARGET_USER_YMAX)
        Tool.Set_Option('TARGET_USER_COLS', TARGET_USER_COLS)
        Tool.Set_Option('TARGET_USER_ROWS', TARGET_USER_ROWS)
        Tool.Set_Option('TARGET_USER_FLAT', TARGET_USER_FLAT)
        Tool.Set_Option('TARGET_USER_FITS', TARGET_USER_FITS)
        Tool.Set_Option('TARGET_SYSTEM', TARGET_SYSTEM)
        return Tool.Execute(Verbose)
    return False

def Rectify_Grid_List(REF_SOURCE=None, GRIDS=None, REF_TARGET=None, TARGET_TEMPLATE=None, TARGET_GRIDS=None, XFIELD=None, YFIELD=None, METHOD=None, ORDER=None, RESAMPLING=None, BYTEWISE=None, DATA_TYPE=None, CRS_STRING=None, TARGET_DEFINITION=None, TARGET_USER_SIZE=None, TARGET_USER_XMIN=None, TARGET_USER_XMAX=None, TARGET_USER_YMIN=None, TARGET_USER_YMAX=None, TARGET_USER_COLS=None, TARGET_USER_ROWS=None, TARGET_USER_FLAT=None, TARGET_USER_FITS=None, TARGET_SYSTEM=None, Verbose=2):
    '''
    Rectify Grid List
    ----------
    [pj_georeference.10]\n
    Georeferencing and rectification for grids. Either choose the attribute fields (x/y) with the projected coordinates for the reference points (origin) or supply an additional points layer with correspondent points in the target projection.\n
    Arguments
    ----------
    - REF_SOURCE [`input shapes`] : Reference Points (Origin)
    - GRIDS [`input grid list`] : Grids
    - REF_TARGET [`optional input shapes`] : Reference Points (Projection)
    - TARGET_TEMPLATE [`optional input grid`] : Target System. use this grid's system for output grids
    - TARGET_GRIDS [`output grid list`] : Target
    - XFIELD [`table field`] : x Position
    - YFIELD [`table field`] : y Position
    - METHOD [`choice`] : Method. Available Choices: [0] Automatic [1] Triangulation [2] Spline [3] Affine [4] 1st Order Polynomial [5] 2nd Order Polynomial [6] 3rd Order Polynomial [7] Polynomial, Order Default: 0
    - ORDER [`integer number`] : Polynomial Order. Minimum: 1 Default: 3
    - RESAMPLING [`choice`] : Resampling. Available Choices: [0] Nearest Neighbour [1] Bilinear Interpolation [2] Bicubic Spline Interpolation [3] B-Spline Interpolation Default: 3
    - BYTEWISE [`boolean`] : Bytewise Interpolation. Default: 0
    - DATA_TYPE [`data type`] : Data Type. Available Choices: [0] unsigned 1 byte integer [1] signed 1 byte integer [2] unsigned 2 byte integer [3] signed 2 byte integer [4] unsigned 4 byte integer [5] signed 4 byte integer [6] unsigned 8 byte integer [7] signed 8 byte integer [8] 4 byte floating point number [9] 8 byte floating point number [10] Preserve Default: 10
    - CRS_STRING [`text`] : Coordinate System Definition. Supported formats comprise PROJ and WKT strings, object codes (e.g. "EPSG:4326").
    - TARGET_DEFINITION [`choice`] : Target Grid System. Available Choices: [0] user defined [1] grid or grid system Default: 0
    - TARGET_USER_SIZE [`floating point number`] : Cellsize. Minimum: 0.000000 Default: 1.000000
    - TARGET_USER_XMIN [`floating point number`] : West. Default: 0.000000
    - TARGET_USER_XMAX [`floating point number`] : East. Default: 100.000000
    - TARGET_USER_YMIN [`floating point number`] : South. Default: 0.000000
    - TARGET_USER_YMAX [`floating point number`] : North. Default: 100.000000
    - TARGET_USER_COLS [`integer number`] : Columns. Minimum: 1 Default: 101 Number of cells in East-West direction.
    - TARGET_USER_ROWS [`integer number`] : Rows. Minimum: 1 Default: 101 Number of cells in North-South direction.
    - TARGET_USER_FLAT [`boolean`] : Rounding. Default: 1 Round bounding coordinates to multiples of cell size. Ignored if cell size has decimal places.
    - TARGET_USER_FITS [`choice`] : Fit. Available Choices: [0] nodes [1] cells Default: 0
    - TARGET_SYSTEM [`grid system`] : Grid System

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('pj_georeference', '10', 'Rectify Grid List')
    if Tool.is_Okay():
        Tool.Set_Input ('REF_SOURCE', REF_SOURCE)
        Tool.Set_Input ('GRIDS', GRIDS)
        Tool.Set_Input ('REF_TARGET', REF_TARGET)
        Tool.Set_Input ('TARGET_TEMPLATE', TARGET_TEMPLATE)
        Tool.Set_Output('TARGET_GRIDS', TARGET_GRIDS)
        Tool.Set_Option('XFIELD', XFIELD)
        Tool.Set_Option('YFIELD', YFIELD)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('ORDER', ORDER)
        Tool.Set_Option('RESAMPLING', RESAMPLING)
        Tool.Set_Option('BYTEWISE', BYTEWISE)
        Tool.Set_Option('DATA_TYPE', DATA_TYPE)
        Tool.Set_Option('CRS_STRING', CRS_STRING)
        Tool.Set_Option('TARGET_DEFINITION', TARGET_DEFINITION)
        Tool.Set_Option('TARGET_USER_SIZE', TARGET_USER_SIZE)
        Tool.Set_Option('TARGET_USER_XMIN', TARGET_USER_XMIN)
        Tool.Set_Option('TARGET_USER_XMAX', TARGET_USER_XMAX)
        Tool.Set_Option('TARGET_USER_YMIN', TARGET_USER_YMIN)
        Tool.Set_Option('TARGET_USER_YMAX', TARGET_USER_YMAX)
        Tool.Set_Option('TARGET_USER_COLS', TARGET_USER_COLS)
        Tool.Set_Option('TARGET_USER_ROWS', TARGET_USER_ROWS)
        Tool.Set_Option('TARGET_USER_FLAT', TARGET_USER_FLAT)
        Tool.Set_Option('TARGET_USER_FITS', TARGET_USER_FITS)
        Tool.Set_Option('TARGET_SYSTEM', TARGET_SYSTEM)
        return Tool.Execute(Verbose)
    return False

def run_tool_pj_georeference_10(REF_SOURCE=None, GRIDS=None, REF_TARGET=None, TARGET_TEMPLATE=None, TARGET_GRIDS=None, XFIELD=None, YFIELD=None, METHOD=None, ORDER=None, RESAMPLING=None, BYTEWISE=None, DATA_TYPE=None, CRS_STRING=None, TARGET_DEFINITION=None, TARGET_USER_SIZE=None, TARGET_USER_XMIN=None, TARGET_USER_XMAX=None, TARGET_USER_YMIN=None, TARGET_USER_YMAX=None, TARGET_USER_COLS=None, TARGET_USER_ROWS=None, TARGET_USER_FLAT=None, TARGET_USER_FITS=None, TARGET_SYSTEM=None, Verbose=2):
    '''
    Rectify Grid List
    ----------
    [pj_georeference.10]\n
    Georeferencing and rectification for grids. Either choose the attribute fields (x/y) with the projected coordinates for the reference points (origin) or supply an additional points layer with correspondent points in the target projection.\n
    Arguments
    ----------
    - REF_SOURCE [`input shapes`] : Reference Points (Origin)
    - GRIDS [`input grid list`] : Grids
    - REF_TARGET [`optional input shapes`] : Reference Points (Projection)
    - TARGET_TEMPLATE [`optional input grid`] : Target System. use this grid's system for output grids
    - TARGET_GRIDS [`output grid list`] : Target
    - XFIELD [`table field`] : x Position
    - YFIELD [`table field`] : y Position
    - METHOD [`choice`] : Method. Available Choices: [0] Automatic [1] Triangulation [2] Spline [3] Affine [4] 1st Order Polynomial [5] 2nd Order Polynomial [6] 3rd Order Polynomial [7] Polynomial, Order Default: 0
    - ORDER [`integer number`] : Polynomial Order. Minimum: 1 Default: 3
    - RESAMPLING [`choice`] : Resampling. Available Choices: [0] Nearest Neighbour [1] Bilinear Interpolation [2] Bicubic Spline Interpolation [3] B-Spline Interpolation Default: 3
    - BYTEWISE [`boolean`] : Bytewise Interpolation. Default: 0
    - DATA_TYPE [`data type`] : Data Type. Available Choices: [0] unsigned 1 byte integer [1] signed 1 byte integer [2] unsigned 2 byte integer [3] signed 2 byte integer [4] unsigned 4 byte integer [5] signed 4 byte integer [6] unsigned 8 byte integer [7] signed 8 byte integer [8] 4 byte floating point number [9] 8 byte floating point number [10] Preserve Default: 10
    - CRS_STRING [`text`] : Coordinate System Definition. Supported formats comprise PROJ and WKT strings, object codes (e.g. "EPSG:4326").
    - TARGET_DEFINITION [`choice`] : Target Grid System. Available Choices: [0] user defined [1] grid or grid system Default: 0
    - TARGET_USER_SIZE [`floating point number`] : Cellsize. Minimum: 0.000000 Default: 1.000000
    - TARGET_USER_XMIN [`floating point number`] : West. Default: 0.000000
    - TARGET_USER_XMAX [`floating point number`] : East. Default: 100.000000
    - TARGET_USER_YMIN [`floating point number`] : South. Default: 0.000000
    - TARGET_USER_YMAX [`floating point number`] : North. Default: 100.000000
    - TARGET_USER_COLS [`integer number`] : Columns. Minimum: 1 Default: 101 Number of cells in East-West direction.
    - TARGET_USER_ROWS [`integer number`] : Rows. Minimum: 1 Default: 101 Number of cells in North-South direction.
    - TARGET_USER_FLAT [`boolean`] : Rounding. Default: 1 Round bounding coordinates to multiples of cell size. Ignored if cell size has decimal places.
    - TARGET_USER_FITS [`choice`] : Fit. Available Choices: [0] nodes [1] cells Default: 0
    - TARGET_SYSTEM [`grid system`] : Grid System

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('pj_georeference', '10', 'Rectify Grid List')
    if Tool.is_Okay():
        Tool.Set_Input ('REF_SOURCE', REF_SOURCE)
        Tool.Set_Input ('GRIDS', GRIDS)
        Tool.Set_Input ('REF_TARGET', REF_TARGET)
        Tool.Set_Input ('TARGET_TEMPLATE', TARGET_TEMPLATE)
        Tool.Set_Output('TARGET_GRIDS', TARGET_GRIDS)
        Tool.Set_Option('XFIELD', XFIELD)
        Tool.Set_Option('YFIELD', YFIELD)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('ORDER', ORDER)
        Tool.Set_Option('RESAMPLING', RESAMPLING)
        Tool.Set_Option('BYTEWISE', BYTEWISE)
        Tool.Set_Option('DATA_TYPE', DATA_TYPE)
        Tool.Set_Option('CRS_STRING', CRS_STRING)
        Tool.Set_Option('TARGET_DEFINITION', TARGET_DEFINITION)
        Tool.Set_Option('TARGET_USER_SIZE', TARGET_USER_SIZE)
        Tool.Set_Option('TARGET_USER_XMIN', TARGET_USER_XMIN)
        Tool.Set_Option('TARGET_USER_XMAX', TARGET_USER_XMAX)
        Tool.Set_Option('TARGET_USER_YMIN', TARGET_USER_YMIN)
        Tool.Set_Option('TARGET_USER_YMAX', TARGET_USER_YMAX)
        Tool.Set_Option('TARGET_USER_COLS', TARGET_USER_COLS)
        Tool.Set_Option('TARGET_USER_ROWS', TARGET_USER_ROWS)
        Tool.Set_Option('TARGET_USER_FLAT', TARGET_USER_FLAT)
        Tool.Set_Option('TARGET_USER_FITS', TARGET_USER_FITS)
        Tool.Set_Option('TARGET_SYSTEM', TARGET_SYSTEM)
        return Tool.Execute(Verbose)
    return False

