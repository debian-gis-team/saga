#! /usr/bin/env python

'''
Python Interface to SAGA Tools Library
----------
- Category : Shapes
- Name     : Lines
- ID       : shapes_lines

Description
----------
Tools for lines.
'''

from PySAGA.helper import Tool_Wrapper

def Convert_Polygons_to_Lines(POLYGONS=None, LINES=None, Verbose=2):
    '''
    Convert Polygons to Lines
    ----------
    [shapes_lines.0]\n
    Convert polygons to lines.\n
    Arguments
    ----------
    - POLYGONS [`input shapes`] : Polygons
    - LINES [`output shapes`] : Lines

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_lines', '0', 'Convert Polygons to Lines')
    if Tool.is_Okay():
        Tool.Set_Input ('POLYGONS', POLYGONS)
        Tool.Set_Output('LINES', LINES)
        return Tool.Execute(Verbose)
    return False

def run_tool_shapes_lines_0(POLYGONS=None, LINES=None, Verbose=2):
    '''
    Convert Polygons to Lines
    ----------
    [shapes_lines.0]\n
    Convert polygons to lines.\n
    Arguments
    ----------
    - POLYGONS [`input shapes`] : Polygons
    - LINES [`output shapes`] : Lines

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_lines', '0', 'Convert Polygons to Lines')
    if Tool.is_Okay():
        Tool.Set_Input ('POLYGONS', POLYGONS)
        Tool.Set_Output('LINES', LINES)
        return Tool.Execute(Verbose)
    return False

def Convert_Points_to_Lines(POINTS=None, LINES=None, ORDER=None, SEPARATE=None, ELEVATION=None, MEASURE=None, MAXDIST=None, Verbose=2):
    '''
    Convert Points to Line(s)
    ----------
    [shapes_lines.1]\n
    The tool allows one to convert points into lines. Several attributes can be specified to control the construction of the lines:\n
    (-) an attribute that specifies the order of the points along each line\n
    (-) an attribute with a line identifier that is used to separate lines\n
    (-) an attribute with height information that is used as vertex Z value (output shapes layer will be of type XYZ)\n
    (-) an attribute with measure information that is used as vertex M value (output shapes layer will be of type XYZM)\n
    If no attributes are specified, a single line is constructed from the points in the saved sequence.\n
    Arguments
    ----------
    - POINTS [`input shapes`] : Points
    - LINES [`output shapes`] : Lines
    - ORDER [`table field`] : Order by...
    - SEPARATE [`table field`] : Separate by...
    - ELEVATION [`table field`] : Elevation
    - MEASURE [`table field`] : Measure
    - MAXDIST [`floating point number`] : Maximum Distance. Minimum: 0.000000 Default: 0.000000 Maximum distance allowed to connect two consecutive points. Ignored if set to zero (default).

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_lines', '1', 'Convert Points to Line(s)')
    if Tool.is_Okay():
        Tool.Set_Input ('POINTS', POINTS)
        Tool.Set_Output('LINES', LINES)
        Tool.Set_Option('ORDER', ORDER)
        Tool.Set_Option('SEPARATE', SEPARATE)
        Tool.Set_Option('ELEVATION', ELEVATION)
        Tool.Set_Option('MEASURE', MEASURE)
        Tool.Set_Option('MAXDIST', MAXDIST)
        return Tool.Execute(Verbose)
    return False

def run_tool_shapes_lines_1(POINTS=None, LINES=None, ORDER=None, SEPARATE=None, ELEVATION=None, MEASURE=None, MAXDIST=None, Verbose=2):
    '''
    Convert Points to Line(s)
    ----------
    [shapes_lines.1]\n
    The tool allows one to convert points into lines. Several attributes can be specified to control the construction of the lines:\n
    (-) an attribute that specifies the order of the points along each line\n
    (-) an attribute with a line identifier that is used to separate lines\n
    (-) an attribute with height information that is used as vertex Z value (output shapes layer will be of type XYZ)\n
    (-) an attribute with measure information that is used as vertex M value (output shapes layer will be of type XYZM)\n
    If no attributes are specified, a single line is constructed from the points in the saved sequence.\n
    Arguments
    ----------
    - POINTS [`input shapes`] : Points
    - LINES [`output shapes`] : Lines
    - ORDER [`table field`] : Order by...
    - SEPARATE [`table field`] : Separate by...
    - ELEVATION [`table field`] : Elevation
    - MEASURE [`table field`] : Measure
    - MAXDIST [`floating point number`] : Maximum Distance. Minimum: 0.000000 Default: 0.000000 Maximum distance allowed to connect two consecutive points. Ignored if set to zero (default).

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_lines', '1', 'Convert Points to Line(s)')
    if Tool.is_Okay():
        Tool.Set_Input ('POINTS', POINTS)
        Tool.Set_Output('LINES', LINES)
        Tool.Set_Option('ORDER', ORDER)
        Tool.Set_Option('SEPARATE', SEPARATE)
        Tool.Set_Option('ELEVATION', ELEVATION)
        Tool.Set_Option('MEASURE', MEASURE)
        Tool.Set_Option('MAXDIST', MAXDIST)
        return Tool.Execute(Verbose)
    return False

def Line_Properties(LINES=None, OUTPUT=None, BPARTS=None, BPOINTS=None, BLENGTH=None, Verbose=2):
    '''
    Line Properties
    ----------
    [shapes_lines.2]\n
    Line properties: length, number of vertices.\n
    Arguments
    ----------
    - LINES [`input shapes`] : Lines
    - OUTPUT [`output shapes`] : Lines with Property Attributes. If not set property attributes will be added to the original layer.
    - BPARTS [`boolean`] : Number of Parts. Default: 0
    - BPOINTS [`boolean`] : Number of Vertices. Default: 0
    - BLENGTH [`boolean`] : Length. Default: 1

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_lines', '2', 'Line Properties')
    if Tool.is_Okay():
        Tool.Set_Input ('LINES', LINES)
        Tool.Set_Output('OUTPUT', OUTPUT)
        Tool.Set_Option('BPARTS', BPARTS)
        Tool.Set_Option('BPOINTS', BPOINTS)
        Tool.Set_Option('BLENGTH', BLENGTH)
        return Tool.Execute(Verbose)
    return False

def run_tool_shapes_lines_2(LINES=None, OUTPUT=None, BPARTS=None, BPOINTS=None, BLENGTH=None, Verbose=2):
    '''
    Line Properties
    ----------
    [shapes_lines.2]\n
    Line properties: length, number of vertices.\n
    Arguments
    ----------
    - LINES [`input shapes`] : Lines
    - OUTPUT [`output shapes`] : Lines with Property Attributes. If not set property attributes will be added to the original layer.
    - BPARTS [`boolean`] : Number of Parts. Default: 0
    - BPOINTS [`boolean`] : Number of Vertices. Default: 0
    - BLENGTH [`boolean`] : Length. Default: 1

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_lines', '2', 'Line Properties')
    if Tool.is_Okay():
        Tool.Set_Input ('LINES', LINES)
        Tool.Set_Output('OUTPUT', OUTPUT)
        Tool.Set_Option('BPARTS', BPARTS)
        Tool.Set_Option('BPOINTS', BPOINTS)
        Tool.Set_Option('BLENGTH', BLENGTH)
        return Tool.Execute(Verbose)
    return False

def LinePolygon_Intersection(LINES=None, POLYGONS=None, INTERSECT=None, DIFFERENCE=None, OUTPUT=None, ATTRIBUTES=None, Verbose=2):
    '''
    Line-Polygon Intersection
    ----------
    [shapes_lines.3]\n
    Line-polygon intersection. Splits lines with polygon arcs.\n
    Arguments
    ----------
    - LINES [`input shapes`] : Lines
    - POLYGONS [`input shapes`] : Polygons
    - INTERSECT [`output shapes`] : Intersection
    - DIFFERENCE [`output shapes`] : Difference
    - OUTPUT [`choice`] : Output. Available Choices: [0] intersection [1] difference [2] intersection and difference Default: 2
    - ATTRIBUTES [`choice`] : Attributes. Available Choices: [0] polygon [1] line [2] line and polygon Default: 1 attributes inherited to intersection result

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_lines', '3', 'Line-Polygon Intersection')
    if Tool.is_Okay():
        Tool.Set_Input ('LINES', LINES)
        Tool.Set_Input ('POLYGONS', POLYGONS)
        Tool.Set_Output('INTERSECT', INTERSECT)
        Tool.Set_Output('DIFFERENCE', DIFFERENCE)
        Tool.Set_Option('OUTPUT', OUTPUT)
        Tool.Set_Option('ATTRIBUTES', ATTRIBUTES)
        return Tool.Execute(Verbose)
    return False

def run_tool_shapes_lines_3(LINES=None, POLYGONS=None, INTERSECT=None, DIFFERENCE=None, OUTPUT=None, ATTRIBUTES=None, Verbose=2):
    '''
    Line-Polygon Intersection
    ----------
    [shapes_lines.3]\n
    Line-polygon intersection. Splits lines with polygon arcs.\n
    Arguments
    ----------
    - LINES [`input shapes`] : Lines
    - POLYGONS [`input shapes`] : Polygons
    - INTERSECT [`output shapes`] : Intersection
    - DIFFERENCE [`output shapes`] : Difference
    - OUTPUT [`choice`] : Output. Available Choices: [0] intersection [1] difference [2] intersection and difference Default: 2
    - ATTRIBUTES [`choice`] : Attributes. Available Choices: [0] polygon [1] line [2] line and polygon Default: 1 attributes inherited to intersection result

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_lines', '3', 'Line-Polygon Intersection')
    if Tool.is_Okay():
        Tool.Set_Input ('LINES', LINES)
        Tool.Set_Input ('POLYGONS', POLYGONS)
        Tool.Set_Output('INTERSECT', INTERSECT)
        Tool.Set_Output('DIFFERENCE', DIFFERENCE)
        Tool.Set_Option('OUTPUT', OUTPUT)
        Tool.Set_Option('ATTRIBUTES', ATTRIBUTES)
        return Tool.Execute(Verbose)
    return False

def Line_Simplification(LINES=None, OUTPUT=None, TOLERANCE=None, Verbose=2):
    '''
    Line Simplification
    ----------
    [shapes_lines.4]\n
    Line simplification implementing the Ramer-Douglas-Peucker algorithm.\n
    References:\n
    - Ramer, U. (1972): An iterative procedure for the polygonal approximation of plane curves. Computer Graphics and Image Processing, 1(3), 244-256\n
    - Douglas, D., Peucker, T. (1973): Algorithms for the reduction of the number of points required to represent a digitized line or its caricature. The Canadian Cartographer 10(2), 112-122\n
    - Polyline Reduction source code at [mappinghacks.com](http://mappinghacks.com/code/PolyLineReduction/)\n
    Arguments
    ----------
    - LINES [`input shapes`] : Lines. Line or polygon shapefile to simplify.
    - OUTPUT [`output shapes`] : Simplified Lines. If not set points will be removed from the input data set.
    - TOLERANCE [`floating point number`] : Tolerance. Minimum: 0.000000 Default: 1.000000 Maximum deviation allowed between original and simplified curve [map units].

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_lines', '4', 'Line Simplification')
    if Tool.is_Okay():
        Tool.Set_Input ('LINES', LINES)
        Tool.Set_Output('OUTPUT', OUTPUT)
        Tool.Set_Option('TOLERANCE', TOLERANCE)
        return Tool.Execute(Verbose)
    return False

def run_tool_shapes_lines_4(LINES=None, OUTPUT=None, TOLERANCE=None, Verbose=2):
    '''
    Line Simplification
    ----------
    [shapes_lines.4]\n
    Line simplification implementing the Ramer-Douglas-Peucker algorithm.\n
    References:\n
    - Ramer, U. (1972): An iterative procedure for the polygonal approximation of plane curves. Computer Graphics and Image Processing, 1(3), 244-256\n
    - Douglas, D., Peucker, T. (1973): Algorithms for the reduction of the number of points required to represent a digitized line or its caricature. The Canadian Cartographer 10(2), 112-122\n
    - Polyline Reduction source code at [mappinghacks.com](http://mappinghacks.com/code/PolyLineReduction/)\n
    Arguments
    ----------
    - LINES [`input shapes`] : Lines. Line or polygon shapefile to simplify.
    - OUTPUT [`output shapes`] : Simplified Lines. If not set points will be removed from the input data set.
    - TOLERANCE [`floating point number`] : Tolerance. Minimum: 0.000000 Default: 1.000000 Maximum deviation allowed between original and simplified curve [map units].

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_lines', '4', 'Line Simplification')
    if Tool.is_Okay():
        Tool.Set_Input ('LINES', LINES)
        Tool.Set_Output('OUTPUT', OUTPUT)
        Tool.Set_Option('TOLERANCE', TOLERANCE)
        return Tool.Execute(Verbose)
    return False

def Line_Dissolve(LINES=None, DISSOLVED=None, FIELDS=None, STATISTICS=None, STAT_SUM=None, STAT_AVG=None, STAT_MIN=None, STAT_MAX=None, STAT_RNG=None, STAT_DEV=None, STAT_VAR=None, STAT_LST=None, STAT_NUM=None, STAT_NAMING=None, Verbose=2):
    '''
    Line Dissolve
    ----------
    [shapes_lines.5]\n
    Dissolves line shapes, which share the same attribute value(s).\n
    Arguments
    ----------
    - LINES [`input shapes`] : Lines
    - DISSOLVED [`output shapes`] : Dissolved Lines
    - FIELDS [`table fields`] : Dissolve Field(s)
    - STATISTICS [`table fields`] : Statistics Field(s)
    - STAT_SUM [`boolean`] : Sum. Default: 0
    - STAT_AVG [`boolean`] : Mean. Default: 1
    - STAT_MIN [`boolean`] : Minimum. Default: 0
    - STAT_MAX [`boolean`] : Maximum. Default: 0
    - STAT_RNG [`boolean`] : Range. Default: 0
    - STAT_DEV [`boolean`] : Deviation. Default: 0
    - STAT_VAR [`boolean`] : Variance. Default: 0
    - STAT_LST [`boolean`] : Listing. Default: 0
    - STAT_NUM [`boolean`] : Count. Default: 0
    - STAT_NAMING [`choice`] : Field Naming. Available Choices: [0] variable type + original name [1] original name + variable type [2] original name [3] variable type Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_lines', '5', 'Line Dissolve')
    if Tool.is_Okay():
        Tool.Set_Input ('LINES', LINES)
        Tool.Set_Output('DISSOLVED', DISSOLVED)
        Tool.Set_Option('FIELDS', FIELDS)
        Tool.Set_Option('STATISTICS', STATISTICS)
        Tool.Set_Option('STAT_SUM', STAT_SUM)
        Tool.Set_Option('STAT_AVG', STAT_AVG)
        Tool.Set_Option('STAT_MIN', STAT_MIN)
        Tool.Set_Option('STAT_MAX', STAT_MAX)
        Tool.Set_Option('STAT_RNG', STAT_RNG)
        Tool.Set_Option('STAT_DEV', STAT_DEV)
        Tool.Set_Option('STAT_VAR', STAT_VAR)
        Tool.Set_Option('STAT_LST', STAT_LST)
        Tool.Set_Option('STAT_NUM', STAT_NUM)
        Tool.Set_Option('STAT_NAMING', STAT_NAMING)
        return Tool.Execute(Verbose)
    return False

def run_tool_shapes_lines_5(LINES=None, DISSOLVED=None, FIELDS=None, STATISTICS=None, STAT_SUM=None, STAT_AVG=None, STAT_MIN=None, STAT_MAX=None, STAT_RNG=None, STAT_DEV=None, STAT_VAR=None, STAT_LST=None, STAT_NUM=None, STAT_NAMING=None, Verbose=2):
    '''
    Line Dissolve
    ----------
    [shapes_lines.5]\n
    Dissolves line shapes, which share the same attribute value(s).\n
    Arguments
    ----------
    - LINES [`input shapes`] : Lines
    - DISSOLVED [`output shapes`] : Dissolved Lines
    - FIELDS [`table fields`] : Dissolve Field(s)
    - STATISTICS [`table fields`] : Statistics Field(s)
    - STAT_SUM [`boolean`] : Sum. Default: 0
    - STAT_AVG [`boolean`] : Mean. Default: 1
    - STAT_MIN [`boolean`] : Minimum. Default: 0
    - STAT_MAX [`boolean`] : Maximum. Default: 0
    - STAT_RNG [`boolean`] : Range. Default: 0
    - STAT_DEV [`boolean`] : Deviation. Default: 0
    - STAT_VAR [`boolean`] : Variance. Default: 0
    - STAT_LST [`boolean`] : Listing. Default: 0
    - STAT_NUM [`boolean`] : Count. Default: 0
    - STAT_NAMING [`choice`] : Field Naming. Available Choices: [0] variable type + original name [1] original name + variable type [2] original name [3] variable type Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_lines', '5', 'Line Dissolve')
    if Tool.is_Okay():
        Tool.Set_Input ('LINES', LINES)
        Tool.Set_Output('DISSOLVED', DISSOLVED)
        Tool.Set_Option('FIELDS', FIELDS)
        Tool.Set_Option('STATISTICS', STATISTICS)
        Tool.Set_Option('STAT_SUM', STAT_SUM)
        Tool.Set_Option('STAT_AVG', STAT_AVG)
        Tool.Set_Option('STAT_MIN', STAT_MIN)
        Tool.Set_Option('STAT_MAX', STAT_MAX)
        Tool.Set_Option('STAT_RNG', STAT_RNG)
        Tool.Set_Option('STAT_DEV', STAT_DEV)
        Tool.Set_Option('STAT_VAR', STAT_VAR)
        Tool.Set_Option('STAT_LST', STAT_LST)
        Tool.Set_Option('STAT_NUM', STAT_NUM)
        Tool.Set_Option('STAT_NAMING', STAT_NAMING)
        return Tool.Execute(Verbose)
    return False

def Split_Lines_with_Lines(LINES=None, SPLIT=None, INTERSECT=None, OUTPUT=None, Verbose=2):
    '''
    Split Lines with Lines
    ----------
    [shapes_lines.6]\n
    The tool allows one to split lines with lines.\n
    Arguments
    ----------
    - LINES [`input shapes`] : Lines. The input lines to split.
    - SPLIT [`input shapes`] : Split Features. The lines with which the input lines are to be divided.
    - INTERSECT [`output shapes`] : Intersection. The split lines.
    - OUTPUT [`choice`] : Output. Available Choices: [0] polylines [1] separate lines Default: 1 Choose the output line type, either polylines or separate lines.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_lines', '6', 'Split Lines with Lines')
    if Tool.is_Okay():
        Tool.Set_Input ('LINES', LINES)
        Tool.Set_Input ('SPLIT', SPLIT)
        Tool.Set_Output('INTERSECT', INTERSECT)
        Tool.Set_Option('OUTPUT', OUTPUT)
        return Tool.Execute(Verbose)
    return False

def run_tool_shapes_lines_6(LINES=None, SPLIT=None, INTERSECT=None, OUTPUT=None, Verbose=2):
    '''
    Split Lines with Lines
    ----------
    [shapes_lines.6]\n
    The tool allows one to split lines with lines.\n
    Arguments
    ----------
    - LINES [`input shapes`] : Lines. The input lines to split.
    - SPLIT [`input shapes`] : Split Features. The lines with which the input lines are to be divided.
    - INTERSECT [`output shapes`] : Intersection. The split lines.
    - OUTPUT [`choice`] : Output. Available Choices: [0] polylines [1] separate lines Default: 1 Choose the output line type, either polylines or separate lines.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_lines', '6', 'Split Lines with Lines')
    if Tool.is_Okay():
        Tool.Set_Input ('LINES', LINES)
        Tool.Set_Input ('SPLIT', SPLIT)
        Tool.Set_Output('INTERSECT', INTERSECT)
        Tool.Set_Option('OUTPUT', OUTPUT)
        return Tool.Execute(Verbose)
    return False

def Line_Smoothing(LINES_IN=None, LINES_OUT=None, METHOD=None, SENSITIVITY=None, ITERATIONS=None, PRESERVATION=None, SIGMA=None, Verbose=2):
    '''
    Line Smoothing
    ----------
    [shapes_lines.7]\n
    The tool provides methods for line smoothing including iterative averaging (SIA) and Gaussian filtering.\n
    Iterative averaging (SIA) is described by Mansouryar & Hedayati (2012). A higher smoothing sensitivity results in a stronger smoothing in less iterations and vice versa. The 'improved SIA model' simply applies a preservation factor in the first iteration and then runs the 'basic SIA model' for the following iterations.\n
    Gaussian filtering with shrinkage correction is described by Lowe (1989).\n
    In case the density of line vertices is too high, the 'Line Simplification' tool can be applied first. If the density of line vertices is too low, additional vertices can be inserted by applying the 'Convert Lines to Points' and the 'Convert Points to Line(s)' tools prior to smoothing.\n
    References:\n
    Lowe, D. (1989): Organization of Smooth Image Curves at Multiple Scales. International Journal of Computer Vision, 3: 119-130. ([pdf](http://www.cs.ubc.ca/~lowe/papers/iccv88.pdf))\n
    Mansouryar, M. & Hedayati, A. (2012): Smoothing Via Iterative Averaging (SIA) - A Basic Technique for Line Smoothing. International Journal of Computer and Electrical Engineering Vol. 4, No. 3: 307-311. ([pdf](http://www.ijcee.org/papers/501-P063.pdf))\n
    Arguments
    ----------
    - LINES_IN [`input shapes`] : Lines. The input line shapefile to smooth.
    - LINES_OUT [`output shapes`] : Smoothed Lines. The smoothed output line shapefile.
    - METHOD [`choice`] : Method. Available Choices: [0] basic SIA model [1] improved SIA model [2] Gaussian Filtering Default: 2 Choose the method to apply.
    - SENSITIVITY [`integer number`] : Sensitivity. Minimum: 1 Default: 3 Half the size of the moving window [vertex count], controls smoothing sensitivity.
    - ITERATIONS [`integer number`] : Iterations. Minimum: 1 Default: 10 The number of smoothing iterations [-].
    - PRESERVATION [`floating point number`] : Preservation. Minimum: 1.000000 Default: 10.000000 The smoothing preservation factor [-].
    - SIGMA [`floating point number`] : Sigma. Minimum: 0.500000 Default: 2.000000 Standard deviation of the Gaussian filter [-].

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_lines', '7', 'Line Smoothing')
    if Tool.is_Okay():
        Tool.Set_Input ('LINES_IN', LINES_IN)
        Tool.Set_Output('LINES_OUT', LINES_OUT)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('SENSITIVITY', SENSITIVITY)
        Tool.Set_Option('ITERATIONS', ITERATIONS)
        Tool.Set_Option('PRESERVATION', PRESERVATION)
        Tool.Set_Option('SIGMA', SIGMA)
        return Tool.Execute(Verbose)
    return False

def run_tool_shapes_lines_7(LINES_IN=None, LINES_OUT=None, METHOD=None, SENSITIVITY=None, ITERATIONS=None, PRESERVATION=None, SIGMA=None, Verbose=2):
    '''
    Line Smoothing
    ----------
    [shapes_lines.7]\n
    The tool provides methods for line smoothing including iterative averaging (SIA) and Gaussian filtering.\n
    Iterative averaging (SIA) is described by Mansouryar & Hedayati (2012). A higher smoothing sensitivity results in a stronger smoothing in less iterations and vice versa. The 'improved SIA model' simply applies a preservation factor in the first iteration and then runs the 'basic SIA model' for the following iterations.\n
    Gaussian filtering with shrinkage correction is described by Lowe (1989).\n
    In case the density of line vertices is too high, the 'Line Simplification' tool can be applied first. If the density of line vertices is too low, additional vertices can be inserted by applying the 'Convert Lines to Points' and the 'Convert Points to Line(s)' tools prior to smoothing.\n
    References:\n
    Lowe, D. (1989): Organization of Smooth Image Curves at Multiple Scales. International Journal of Computer Vision, 3: 119-130. ([pdf](http://www.cs.ubc.ca/~lowe/papers/iccv88.pdf))\n
    Mansouryar, M. & Hedayati, A. (2012): Smoothing Via Iterative Averaging (SIA) - A Basic Technique for Line Smoothing. International Journal of Computer and Electrical Engineering Vol. 4, No. 3: 307-311. ([pdf](http://www.ijcee.org/papers/501-P063.pdf))\n
    Arguments
    ----------
    - LINES_IN [`input shapes`] : Lines. The input line shapefile to smooth.
    - LINES_OUT [`output shapes`] : Smoothed Lines. The smoothed output line shapefile.
    - METHOD [`choice`] : Method. Available Choices: [0] basic SIA model [1] improved SIA model [2] Gaussian Filtering Default: 2 Choose the method to apply.
    - SENSITIVITY [`integer number`] : Sensitivity. Minimum: 1 Default: 3 Half the size of the moving window [vertex count], controls smoothing sensitivity.
    - ITERATIONS [`integer number`] : Iterations. Minimum: 1 Default: 10 The number of smoothing iterations [-].
    - PRESERVATION [`floating point number`] : Preservation. Minimum: 1.000000 Default: 10.000000 The smoothing preservation factor [-].
    - SIGMA [`floating point number`] : Sigma. Minimum: 0.500000 Default: 2.000000 Standard deviation of the Gaussian filter [-].

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_lines', '7', 'Line Smoothing')
    if Tool.is_Okay():
        Tool.Set_Input ('LINES_IN', LINES_IN)
        Tool.Set_Output('LINES_OUT', LINES_OUT)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('SENSITIVITY', SENSITIVITY)
        Tool.Set_Option('ITERATIONS', ITERATIONS)
        Tool.Set_Option('PRESERVATION', PRESERVATION)
        Tool.Set_Option('SIGMA', SIGMA)
        return Tool.Execute(Verbose)
    return False

def Split_Lines_at_Points(LINES=None, SPLIT=None, INTERSECT=None, OUTPUT=None, EPSILON=None, MIN_VERTEX_DIST=None, Verbose=2):
    '''
    Split Lines at Points
    ----------
    [shapes_lines.8]\n
    The tool allows one to split lines at certain points. The points must be provided as point shapes.\n
    The order in which the input lines are stored is retained, i.e. split parts are inserted at their original location in the dataset. By using a minimum vertex distance, a splitting close to existing line vertex locations can be avoided.\n
    Arguments
    ----------
    - LINES [`input shapes`] : Lines. The input lines to split.
    - SPLIT [`input shapes`] : Split Features. The points at which the input lines are to be split.
    - INTERSECT [`output shapes`] : Intersection. The split lines.
    - OUTPUT [`choice`] : Output. Available Choices: [0] polylines [1] separate lines Default: 1 Choose the output line type, either polylines or separate lines.
    - EPSILON [`floating point number`] : Epsilon. Default: 0.000000 The tolerance used to find the point-line intersections [map units].
    - MIN_VERTEX_DIST [`floating point number`] : Minimum Vertex Distance. Default: 0.000000 The minimum distance of a point to a line vertex in order to split the line at the point's location [map units].

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_lines', '8', 'Split Lines at Points')
    if Tool.is_Okay():
        Tool.Set_Input ('LINES', LINES)
        Tool.Set_Input ('SPLIT', SPLIT)
        Tool.Set_Output('INTERSECT', INTERSECT)
        Tool.Set_Option('OUTPUT', OUTPUT)
        Tool.Set_Option('EPSILON', EPSILON)
        Tool.Set_Option('MIN_VERTEX_DIST', MIN_VERTEX_DIST)
        return Tool.Execute(Verbose)
    return False

def run_tool_shapes_lines_8(LINES=None, SPLIT=None, INTERSECT=None, OUTPUT=None, EPSILON=None, MIN_VERTEX_DIST=None, Verbose=2):
    '''
    Split Lines at Points
    ----------
    [shapes_lines.8]\n
    The tool allows one to split lines at certain points. The points must be provided as point shapes.\n
    The order in which the input lines are stored is retained, i.e. split parts are inserted at their original location in the dataset. By using a minimum vertex distance, a splitting close to existing line vertex locations can be avoided.\n
    Arguments
    ----------
    - LINES [`input shapes`] : Lines. The input lines to split.
    - SPLIT [`input shapes`] : Split Features. The points at which the input lines are to be split.
    - INTERSECT [`output shapes`] : Intersection. The split lines.
    - OUTPUT [`choice`] : Output. Available Choices: [0] polylines [1] separate lines Default: 1 Choose the output line type, either polylines or separate lines.
    - EPSILON [`floating point number`] : Epsilon. Default: 0.000000 The tolerance used to find the point-line intersections [map units].
    - MIN_VERTEX_DIST [`floating point number`] : Minimum Vertex Distance. Default: 0.000000 The minimum distance of a point to a line vertex in order to split the line at the point's location [map units].

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_lines', '8', 'Split Lines at Points')
    if Tool.is_Okay():
        Tool.Set_Input ('LINES', LINES)
        Tool.Set_Input ('SPLIT', SPLIT)
        Tool.Set_Output('INTERSECT', INTERSECT)
        Tool.Set_Option('OUTPUT', OUTPUT)
        Tool.Set_Option('EPSILON', EPSILON)
        Tool.Set_Option('MIN_VERTEX_DIST', MIN_VERTEX_DIST)
        return Tool.Execute(Verbose)
    return False

def Line_Crossings(LINES_A=None, LINES_B=None, CROSSINGS=None, ATTRIBUTES=None, Verbose=2):
    '''
    Line Crossings
    ----------
    [shapes_lines.9]\n
    Line Crossings\n
    Arguments
    ----------
    - LINES_A [`input shapes`] : 1st Lines Layer
    - LINES_B [`input shapes`] : 2nd Lines Layer
    - CROSSINGS [`output shapes`] : Crossings
    - ATTRIBUTES [`choice`] : Parent Attributes. Available Choices: [0] index [1] attributes [2] index and attributes Default: 2 attributes inherited by parent lines layers

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_lines', '9', 'Line Crossings')
    if Tool.is_Okay():
        Tool.Set_Input ('LINES_A', LINES_A)
        Tool.Set_Input ('LINES_B', LINES_B)
        Tool.Set_Output('CROSSINGS', CROSSINGS)
        Tool.Set_Option('ATTRIBUTES', ATTRIBUTES)
        return Tool.Execute(Verbose)
    return False

def run_tool_shapes_lines_9(LINES_A=None, LINES_B=None, CROSSINGS=None, ATTRIBUTES=None, Verbose=2):
    '''
    Line Crossings
    ----------
    [shapes_lines.9]\n
    Line Crossings\n
    Arguments
    ----------
    - LINES_A [`input shapes`] : 1st Lines Layer
    - LINES_B [`input shapes`] : 2nd Lines Layer
    - CROSSINGS [`output shapes`] : Crossings
    - ATTRIBUTES [`choice`] : Parent Attributes. Available Choices: [0] index [1] attributes [2] index and attributes Default: 2 attributes inherited by parent lines layers

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_lines', '9', 'Line Crossings')
    if Tool.is_Okay():
        Tool.Set_Input ('LINES_A', LINES_A)
        Tool.Set_Input ('LINES_B', LINES_B)
        Tool.Set_Output('CROSSINGS', CROSSINGS)
        Tool.Set_Option('ATTRIBUTES', ATTRIBUTES)
        return Tool.Execute(Verbose)
    return False

def Extract_Closed_Lines(LINES_IN=None, LINES_OUT=None, TOLERANCE=None, MAX_LENGTH=None, Verbose=2):
    '''
    Extract Closed Lines
    ----------
    [shapes_lines.10]\n
    The tool allows one to extract closed lines from the input shapefile. Closed lines are detected by examining the distance between the first and last line vertex.\n
    The 'tolerance' parameter describes the maximum distance allowed between the first and last line vertex. The 'maximum length' parameter can be used to exclude long lines.\n
    Arguments
    ----------
    - LINES_IN [`input shapes`] : Lines. The input line shapefile.
    - LINES_OUT [`output shapes`] : Closed Lines. The output line shapefile with the extracted lines.
    - TOLERANCE [`floating point number`] : Tolerance. Minimum: 0.000000 Default: 0.001000 The maximum distance between the first and last line vertex [map units].
    - MAX_LENGTH [`floating point number`] : Maximum Length. Minimum: 0.000000 Default: 10000.000000 The maximum length of extracted lines [map units].

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_lines', '10', 'Extract Closed Lines')
    if Tool.is_Okay():
        Tool.Set_Input ('LINES_IN', LINES_IN)
        Tool.Set_Output('LINES_OUT', LINES_OUT)
        Tool.Set_Option('TOLERANCE', TOLERANCE)
        Tool.Set_Option('MAX_LENGTH', MAX_LENGTH)
        return Tool.Execute(Verbose)
    return False

def run_tool_shapes_lines_10(LINES_IN=None, LINES_OUT=None, TOLERANCE=None, MAX_LENGTH=None, Verbose=2):
    '''
    Extract Closed Lines
    ----------
    [shapes_lines.10]\n
    The tool allows one to extract closed lines from the input shapefile. Closed lines are detected by examining the distance between the first and last line vertex.\n
    The 'tolerance' parameter describes the maximum distance allowed between the first and last line vertex. The 'maximum length' parameter can be used to exclude long lines.\n
    Arguments
    ----------
    - LINES_IN [`input shapes`] : Lines. The input line shapefile.
    - LINES_OUT [`output shapes`] : Closed Lines. The output line shapefile with the extracted lines.
    - TOLERANCE [`floating point number`] : Tolerance. Minimum: 0.000000 Default: 0.001000 The maximum distance between the first and last line vertex [map units].
    - MAX_LENGTH [`floating point number`] : Maximum Length. Minimum: 0.000000 Default: 10000.000000 The maximum length of extracted lines [map units].

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_lines', '10', 'Extract Closed Lines')
    if Tool.is_Okay():
        Tool.Set_Input ('LINES_IN', LINES_IN)
        Tool.Set_Output('LINES_OUT', LINES_OUT)
        Tool.Set_Option('TOLERANCE', TOLERANCE)
        Tool.Set_Option('MAX_LENGTH', MAX_LENGTH)
        return Tool.Execute(Verbose)
    return False

def Split_Lines(INPUT=None, OUTPUT=None, INS_POINTS=None, DISTRIBUTION=None, LENGTH=None, CAPS_LENGTH=None, NUMBER=None, CAPS_NUMBER=None, Verbose=2):
    '''
    Split Lines
    ----------
    [shapes_lines.11]\n
    The tool allows one to split lines into multiple lines. The lines can be split based on a user-defined line length or the given number of divisions of each input line. Optionally, a point shapes layer with the points at which the lines were split can be created. This can be used, e.g., to create the midpoint of each polyline by dividing each line into two halves.\n
    Arguments
    ----------
    - INPUT [`input shapes`] : Input Lines. Input line shapefile.
    - OUTPUT [`output shapes`] : Output Lines. Output line shapefile.
    - INS_POINTS [`output shapes`] : Inserted Points. The points at which the lines were split.
    - DISTRIBUTION [`choice`] : Distribution. Available Choices: [0] by length [1] by number Default: 0 Choose the method how to split the lines, either by a given line length or by the given number of divisions.
    - LENGTH [`floating point number`] : Length. Minimum: 0.000000 Default: 5.000000 Output line length used to split the lines [map units].
    - CAPS_LENGTH [`choice`] : Caps. Available Choices: [0] start full length [1] start remaining length [2] even ends Default: 0
    - NUMBER [`integer number`] : Number Of Splits. Minimum: 0 Default: 5 The number of divisions per line [-].
    - CAPS_NUMBER [`choice`] : Caps. Available Choices: [0] full segment [1] half segment Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_lines', '11', 'Split Lines')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Output('OUTPUT', OUTPUT)
        Tool.Set_Output('INS_POINTS', INS_POINTS)
        Tool.Set_Option('DISTRIBUTION', DISTRIBUTION)
        Tool.Set_Option('LENGTH', LENGTH)
        Tool.Set_Option('CAPS_LENGTH', CAPS_LENGTH)
        Tool.Set_Option('NUMBER', NUMBER)
        Tool.Set_Option('CAPS_NUMBER', CAPS_NUMBER)
        return Tool.Execute(Verbose)
    return False

def run_tool_shapes_lines_11(INPUT=None, OUTPUT=None, INS_POINTS=None, DISTRIBUTION=None, LENGTH=None, CAPS_LENGTH=None, NUMBER=None, CAPS_NUMBER=None, Verbose=2):
    '''
    Split Lines
    ----------
    [shapes_lines.11]\n
    The tool allows one to split lines into multiple lines. The lines can be split based on a user-defined line length or the given number of divisions of each input line. Optionally, a point shapes layer with the points at which the lines were split can be created. This can be used, e.g., to create the midpoint of each polyline by dividing each line into two halves.\n
    Arguments
    ----------
    - INPUT [`input shapes`] : Input Lines. Input line shapefile.
    - OUTPUT [`output shapes`] : Output Lines. Output line shapefile.
    - INS_POINTS [`output shapes`] : Inserted Points. The points at which the lines were split.
    - DISTRIBUTION [`choice`] : Distribution. Available Choices: [0] by length [1] by number Default: 0 Choose the method how to split the lines, either by a given line length or by the given number of divisions.
    - LENGTH [`floating point number`] : Length. Minimum: 0.000000 Default: 5.000000 Output line length used to split the lines [map units].
    - CAPS_LENGTH [`choice`] : Caps. Available Choices: [0] start full length [1] start remaining length [2] even ends Default: 0
    - NUMBER [`integer number`] : Number Of Splits. Minimum: 0 Default: 5 The number of divisions per line [-].
    - CAPS_NUMBER [`choice`] : Caps. Available Choices: [0] full segment [1] half segment Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_lines', '11', 'Split Lines')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Output('OUTPUT', OUTPUT)
        Tool.Set_Output('INS_POINTS', INS_POINTS)
        Tool.Set_Option('DISTRIBUTION', DISTRIBUTION)
        Tool.Set_Option('LENGTH', LENGTH)
        Tool.Set_Option('CAPS_LENGTH', CAPS_LENGTH)
        Tool.Set_Option('NUMBER', NUMBER)
        Tool.Set_Option('CAPS_NUMBER', CAPS_NUMBER)
        return Tool.Execute(Verbose)
    return False

def Line_Parts_to_Separate_Lines(LINES=None, PARTS=None, Verbose=2):
    '''
    Line Parts to Separate Lines
    ----------
    [shapes_lines.12]\n
    The tool allows one to split multi-part lines into separate lines. Invalid line parts with less than two points are skipped.\n
    Arguments
    ----------
    - LINES [`input shapes`] : Lines. The multi-part lines.
    - PARTS [`output shapes`] : Line Parts. The separated lines.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_lines', '12', 'Line Parts to Separate Lines')
    if Tool.is_Okay():
        Tool.Set_Input ('LINES', LINES)
        Tool.Set_Output('PARTS', PARTS)
        return Tool.Execute(Verbose)
    return False

def run_tool_shapes_lines_12(LINES=None, PARTS=None, Verbose=2):
    '''
    Line Parts to Separate Lines
    ----------
    [shapes_lines.12]\n
    The tool allows one to split multi-part lines into separate lines. Invalid line parts with less than two points are skipped.\n
    Arguments
    ----------
    - LINES [`input shapes`] : Lines. The multi-part lines.
    - PARTS [`output shapes`] : Line Parts. The separated lines.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_lines', '12', 'Line Parts to Separate Lines')
    if Tool.is_Okay():
        Tool.Set_Input ('LINES', LINES)
        Tool.Set_Output('PARTS', PARTS)
        return Tool.Execute(Verbose)
    return False

def Flip_Line_Direction(LINES=None, FLIPPED=None, Verbose=2):
    '''
    Flip Line Direction
    ----------
    [shapes_lines.13]\n
    The tool allows one to reverse the from-to direction of line features. Flipping can be useful, when the line orientation represents flow direction, for example.\n
    Arguments
    ----------
    - LINES [`input shapes`] : Lines. The input line shapefile.
    - FLIPPED [`output shapes`] : Flipped Lines. The output line shapefile with the flipped lines.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_lines', '13', 'Flip Line Direction')
    if Tool.is_Okay():
        Tool.Set_Input ('LINES', LINES)
        Tool.Set_Output('FLIPPED', FLIPPED)
        return Tool.Execute(Verbose)
    return False

def run_tool_shapes_lines_13(LINES=None, FLIPPED=None, Verbose=2):
    '''
    Flip Line Direction
    ----------
    [shapes_lines.13]\n
    The tool allows one to reverse the from-to direction of line features. Flipping can be useful, when the line orientation represents flow direction, for example.\n
    Arguments
    ----------
    - LINES [`input shapes`] : Lines. The input line shapefile.
    - FLIPPED [`output shapes`] : Flipped Lines. The output line shapefile with the flipped lines.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_lines', '13', 'Flip Line Direction')
    if Tool.is_Okay():
        Tool.Set_Input ('LINES', LINES)
        Tool.Set_Output('FLIPPED', FLIPPED)
        return Tool.Execute(Verbose)
    return False

def Merge_Line_Parts_to_Lines(PARTS=None, LINES=None, EPSILON=None, Verbose=2):
    '''
    Merge Line Parts to Lines
    ----------
    [shapes_lines.14]\n
    The tool allows one to merge the parts of multipart lines into lines. Invalid line parts with less than two points are skipped.\n
    Arguments
    ----------
    - PARTS [`input shapes`] : Multipart Lines. The multipart lines.
    - LINES [`output shapes`] : Lines. The merged lines.
    - EPSILON [`floating point number`] : Epsilon. Default: 0.000000 The tolerance used to detect connected parts [map units].

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_lines', '14', 'Merge Line Parts to Lines')
    if Tool.is_Okay():
        Tool.Set_Input ('PARTS', PARTS)
        Tool.Set_Output('LINES', LINES)
        Tool.Set_Option('EPSILON', EPSILON)
        return Tool.Execute(Verbose)
    return False

def run_tool_shapes_lines_14(PARTS=None, LINES=None, EPSILON=None, Verbose=2):
    '''
    Merge Line Parts to Lines
    ----------
    [shapes_lines.14]\n
    The tool allows one to merge the parts of multipart lines into lines. Invalid line parts with less than two points are skipped.\n
    Arguments
    ----------
    - PARTS [`input shapes`] : Multipart Lines. The multipart lines.
    - LINES [`output shapes`] : Lines. The merged lines.
    - EPSILON [`floating point number`] : Epsilon. Default: 0.000000 The tolerance used to detect connected parts [map units].

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_lines', '14', 'Merge Line Parts to Lines')
    if Tool.is_Okay():
        Tool.Set_Input ('PARTS', PARTS)
        Tool.Set_Output('LINES', LINES)
        Tool.Set_Option('EPSILON', EPSILON)
        return Tool.Execute(Verbose)
    return False

def Line_Density(LINES=None, TARGET_TEMPLATE=None, TARGET_OUT_GRID=None, POPULATION=None, RADIUS=None, UNIT=None, SHAPE=None, OUTPUT=None, SCALING=None, NO_ZERO=None, TARGET_DEFINITION=None, TARGET_USER_SIZE=None, TARGET_USER_XMIN=None, TARGET_USER_XMAX=None, TARGET_USER_YMIN=None, TARGET_USER_YMAX=None, TARGET_USER_COLS=None, TARGET_USER_ROWS=None, TARGET_USER_FLAT=None, TARGET_USER_FITS=None, TARGET_SYSTEM=None, Verbose=2):
    '''
    Line Density
    ----------
    [shapes_lines.15]\n
    Line density.\n
    Arguments
    ----------
    - LINES [`input shapes`] : Lines
    - TARGET_TEMPLATE [`optional input grid`] : Target System. use this grid's system for output grids
    - TARGET_OUT_GRID [`output grid`] : Target Grid
    - POPULATION [`table field`] : Population
    - RADIUS [`floating point number`] : Radius. Minimum: 0.000000 Default: 1.000000
    - UNIT [`choice`] : Unit. Available Choices: [0] map units [1] cell size Default: 0
    - SHAPE [`choice`] : Shape. Available Choices: [0] circle [1] square Default: 0
    - OUTPUT [`choice`] : Output. Available Choices: [0] absolute [1] relative Default: 1
    - SCALING [`floating point number`] : Scaling. Default: 1.000000
    - NO_ZERO [`boolean`] : Zero as No-Data. Default: 1
    - TARGET_DEFINITION [`choice`] : Target Grid System. Available Choices: [0] user defined [1] grid or grid system Default: 0
    - TARGET_USER_SIZE [`floating point number`] : Cellsize. Minimum: 0.000000 Default: 1.000000
    - TARGET_USER_XMIN [`floating point number`] : West. Default: 0.000000
    - TARGET_USER_XMAX [`floating point number`] : East. Default: 100.000000
    - TARGET_USER_YMIN [`floating point number`] : South. Default: 0.000000
    - TARGET_USER_YMAX [`floating point number`] : North. Default: 100.000000
    - TARGET_USER_COLS [`integer number`] : Columns. Minimum: 1 Default: 101 Number of cells in East-West direction.
    - TARGET_USER_ROWS [`integer number`] : Rows. Minimum: 1 Default: 101 Number of cells in North-South direction.
    - TARGET_USER_FLAT [`boolean`] : Rounding. Default: 1 Round bounding coordinates to multiples of cell size. Ignored if cell size has decimal places.
    - TARGET_USER_FITS [`choice`] : Fit. Available Choices: [0] nodes [1] cells Default: 0
    - TARGET_SYSTEM [`grid system`] : Grid System

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_lines', '15', 'Line Density')
    if Tool.is_Okay():
        Tool.Set_Input ('LINES', LINES)
        Tool.Set_Input ('TARGET_TEMPLATE', TARGET_TEMPLATE)
        Tool.Set_Output('TARGET_OUT_GRID', TARGET_OUT_GRID)
        Tool.Set_Option('POPULATION', POPULATION)
        Tool.Set_Option('RADIUS', RADIUS)
        Tool.Set_Option('UNIT', UNIT)
        Tool.Set_Option('SHAPE', SHAPE)
        Tool.Set_Option('OUTPUT', OUTPUT)
        Tool.Set_Option('SCALING', SCALING)
        Tool.Set_Option('NO_ZERO', NO_ZERO)
        Tool.Set_Option('TARGET_DEFINITION', TARGET_DEFINITION)
        Tool.Set_Option('TARGET_USER_SIZE', TARGET_USER_SIZE)
        Tool.Set_Option('TARGET_USER_XMIN', TARGET_USER_XMIN)
        Tool.Set_Option('TARGET_USER_XMAX', TARGET_USER_XMAX)
        Tool.Set_Option('TARGET_USER_YMIN', TARGET_USER_YMIN)
        Tool.Set_Option('TARGET_USER_YMAX', TARGET_USER_YMAX)
        Tool.Set_Option('TARGET_USER_COLS', TARGET_USER_COLS)
        Tool.Set_Option('TARGET_USER_ROWS', TARGET_USER_ROWS)
        Tool.Set_Option('TARGET_USER_FLAT', TARGET_USER_FLAT)
        Tool.Set_Option('TARGET_USER_FITS', TARGET_USER_FITS)
        Tool.Set_Option('TARGET_SYSTEM', TARGET_SYSTEM)
        return Tool.Execute(Verbose)
    return False

def run_tool_shapes_lines_15(LINES=None, TARGET_TEMPLATE=None, TARGET_OUT_GRID=None, POPULATION=None, RADIUS=None, UNIT=None, SHAPE=None, OUTPUT=None, SCALING=None, NO_ZERO=None, TARGET_DEFINITION=None, TARGET_USER_SIZE=None, TARGET_USER_XMIN=None, TARGET_USER_XMAX=None, TARGET_USER_YMIN=None, TARGET_USER_YMAX=None, TARGET_USER_COLS=None, TARGET_USER_ROWS=None, TARGET_USER_FLAT=None, TARGET_USER_FITS=None, TARGET_SYSTEM=None, Verbose=2):
    '''
    Line Density
    ----------
    [shapes_lines.15]\n
    Line density.\n
    Arguments
    ----------
    - LINES [`input shapes`] : Lines
    - TARGET_TEMPLATE [`optional input grid`] : Target System. use this grid's system for output grids
    - TARGET_OUT_GRID [`output grid`] : Target Grid
    - POPULATION [`table field`] : Population
    - RADIUS [`floating point number`] : Radius. Minimum: 0.000000 Default: 1.000000
    - UNIT [`choice`] : Unit. Available Choices: [0] map units [1] cell size Default: 0
    - SHAPE [`choice`] : Shape. Available Choices: [0] circle [1] square Default: 0
    - OUTPUT [`choice`] : Output. Available Choices: [0] absolute [1] relative Default: 1
    - SCALING [`floating point number`] : Scaling. Default: 1.000000
    - NO_ZERO [`boolean`] : Zero as No-Data. Default: 1
    - TARGET_DEFINITION [`choice`] : Target Grid System. Available Choices: [0] user defined [1] grid or grid system Default: 0
    - TARGET_USER_SIZE [`floating point number`] : Cellsize. Minimum: 0.000000 Default: 1.000000
    - TARGET_USER_XMIN [`floating point number`] : West. Default: 0.000000
    - TARGET_USER_XMAX [`floating point number`] : East. Default: 100.000000
    - TARGET_USER_YMIN [`floating point number`] : South. Default: 0.000000
    - TARGET_USER_YMAX [`floating point number`] : North. Default: 100.000000
    - TARGET_USER_COLS [`integer number`] : Columns. Minimum: 1 Default: 101 Number of cells in East-West direction.
    - TARGET_USER_ROWS [`integer number`] : Rows. Minimum: 1 Default: 101 Number of cells in North-South direction.
    - TARGET_USER_FLAT [`boolean`] : Rounding. Default: 1 Round bounding coordinates to multiples of cell size. Ignored if cell size has decimal places.
    - TARGET_USER_FITS [`choice`] : Fit. Available Choices: [0] nodes [1] cells Default: 0
    - TARGET_SYSTEM [`grid system`] : Grid System

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_lines', '15', 'Line Density')
    if Tool.is_Okay():
        Tool.Set_Input ('LINES', LINES)
        Tool.Set_Input ('TARGET_TEMPLATE', TARGET_TEMPLATE)
        Tool.Set_Output('TARGET_OUT_GRID', TARGET_OUT_GRID)
        Tool.Set_Option('POPULATION', POPULATION)
        Tool.Set_Option('RADIUS', RADIUS)
        Tool.Set_Option('UNIT', UNIT)
        Tool.Set_Option('SHAPE', SHAPE)
        Tool.Set_Option('OUTPUT', OUTPUT)
        Tool.Set_Option('SCALING', SCALING)
        Tool.Set_Option('NO_ZERO', NO_ZERO)
        Tool.Set_Option('TARGET_DEFINITION', TARGET_DEFINITION)
        Tool.Set_Option('TARGET_USER_SIZE', TARGET_USER_SIZE)
        Tool.Set_Option('TARGET_USER_XMIN', TARGET_USER_XMIN)
        Tool.Set_Option('TARGET_USER_XMAX', TARGET_USER_XMAX)
        Tool.Set_Option('TARGET_USER_YMIN', TARGET_USER_YMIN)
        Tool.Set_Option('TARGET_USER_YMAX', TARGET_USER_YMAX)
        Tool.Set_Option('TARGET_USER_COLS', TARGET_USER_COLS)
        Tool.Set_Option('TARGET_USER_ROWS', TARGET_USER_ROWS)
        Tool.Set_Option('TARGET_USER_FLAT', TARGET_USER_FLAT)
        Tool.Set_Option('TARGET_USER_FITS', TARGET_USER_FITS)
        Tool.Set_Option('TARGET_SYSTEM', TARGET_SYSTEM)
        return Tool.Execute(Verbose)
    return False

def Topologize_Polylines(INPUTLINES=None, SYSTEM=None, OUTPUTLINES=None, OUTPUTPOINTS=None, TOLERANCE=None, SYSTEM_GRIDSYSTEM=None, SIMPLIFY=None, Verbose=2):
    '''
    Topologize Polylines
    ----------
    [shapes_lines.16]\n
    The topology module will convert a shapefile containing lines to a topological network of lines: the direction of every line (i.e., the point order) is used to create a network from and to nodes.\n
    (-) It will convert start and endpoints of every line to nodes\n
    (-) a new line shape is created with three attributes\n
    (-) the start_id and end_id will refer to the node_id, the length is the original length\n
    (-) the shape is either the original shape, or a simplified straight line between the nodes if “SIMPLIFY” is selected\n
    (-) if the original shapefile contains multiparts (e.g., e in the example above) each part will be exported as a separate line\n
    Arguments
    ----------
    - INPUTLINES [`input shapes`] : Input Lines
    - SYSTEM [`optional input grid`] : Target grid (for grid system)
    - OUTPUTLINES [`output shapes`] : Output Lines
    - OUTPUTPOINTS [`output shapes`] : Output Nodes
    - TOLERANCE [`floating point number`] : Tolerance Distance. Default: 0.010000
    - SYSTEM_GRIDSYSTEM [`grid system`] : Grid system
    - SIMPLIFY [`boolean`] : Simplify. Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_lines', '16', 'Topologize Polylines')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUTLINES', INPUTLINES)
        Tool.Set_Input ('SYSTEM', SYSTEM)
        Tool.Set_Output('OUTPUTLINES', OUTPUTLINES)
        Tool.Set_Output('OUTPUTPOINTS', OUTPUTPOINTS)
        Tool.Set_Option('TOLERANCE', TOLERANCE)
        Tool.Set_Option('SYSTEM_GRIDSYSTEM', SYSTEM_GRIDSYSTEM)
        Tool.Set_Option('SIMPLIFY', SIMPLIFY)
        return Tool.Execute(Verbose)
    return False

def run_tool_shapes_lines_16(INPUTLINES=None, SYSTEM=None, OUTPUTLINES=None, OUTPUTPOINTS=None, TOLERANCE=None, SYSTEM_GRIDSYSTEM=None, SIMPLIFY=None, Verbose=2):
    '''
    Topologize Polylines
    ----------
    [shapes_lines.16]\n
    The topology module will convert a shapefile containing lines to a topological network of lines: the direction of every line (i.e., the point order) is used to create a network from and to nodes.\n
    (-) It will convert start and endpoints of every line to nodes\n
    (-) a new line shape is created with three attributes\n
    (-) the start_id and end_id will refer to the node_id, the length is the original length\n
    (-) the shape is either the original shape, or a simplified straight line between the nodes if “SIMPLIFY” is selected\n
    (-) if the original shapefile contains multiparts (e.g., e in the example above) each part will be exported as a separate line\n
    Arguments
    ----------
    - INPUTLINES [`input shapes`] : Input Lines
    - SYSTEM [`optional input grid`] : Target grid (for grid system)
    - OUTPUTLINES [`output shapes`] : Output Lines
    - OUTPUTPOINTS [`output shapes`] : Output Nodes
    - TOLERANCE [`floating point number`] : Tolerance Distance. Default: 0.010000
    - SYSTEM_GRIDSYSTEM [`grid system`] : Grid system
    - SIMPLIFY [`boolean`] : Simplify. Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_lines', '16', 'Topologize Polylines')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUTLINES', INPUTLINES)
        Tool.Set_Input ('SYSTEM', SYSTEM)
        Tool.Set_Output('OUTPUTLINES', OUTPUTLINES)
        Tool.Set_Output('OUTPUTPOINTS', OUTPUTPOINTS)
        Tool.Set_Option('TOLERANCE', TOLERANCE)
        Tool.Set_Option('SYSTEM_GRIDSYSTEM', SYSTEM_GRIDSYSTEM)
        Tool.Set_Option('SIMPLIFY', SIMPLIFY)
        return Tool.Execute(Verbose)
    return False

def Upstream_Edges(INPUTLINES=None, ADJECANT_EDGES=None, UPSTREAM_EDGES=None, Verbose=2):
    '''
    Upstream Edges
    ----------
    [shapes_lines.17]\n
    Create a table with adjecant/upstream edges in a topology.\n
    Arguments
    ----------
    - INPUTLINES [`input table`] : Input Topology. Line result of the topology tool.
    - ADJECANT_EDGES [`output table`] : adjecant edges
    - UPSTREAM_EDGES [`output table`] : upstream edges

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_lines', '17', 'Upstream Edges')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUTLINES', INPUTLINES)
        Tool.Set_Output('ADJECANT_EDGES', ADJECANT_EDGES)
        Tool.Set_Output('UPSTREAM_EDGES', UPSTREAM_EDGES)
        return Tool.Execute(Verbose)
    return False

def run_tool_shapes_lines_17(INPUTLINES=None, ADJECANT_EDGES=None, UPSTREAM_EDGES=None, Verbose=2):
    '''
    Upstream Edges
    ----------
    [shapes_lines.17]\n
    Create a table with adjecant/upstream edges in a topology.\n
    Arguments
    ----------
    - INPUTLINES [`input table`] : Input Topology. Line result of the topology tool.
    - ADJECANT_EDGES [`output table`] : adjecant edges
    - UPSTREAM_EDGES [`output table`] : upstream edges

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_lines', '17', 'Upstream Edges')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUTLINES', INPUTLINES)
        Tool.Set_Output('ADJECANT_EDGES', ADJECANT_EDGES)
        Tool.Set_Output('UPSTREAM_EDGES', UPSTREAM_EDGES)
        return Tool.Execute(Verbose)
    return False

