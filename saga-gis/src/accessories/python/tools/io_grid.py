#! /usr/bin/env python

'''
Python Interface to SAGA Tools Library
----------
- Category : Import/Export
- Name     : Grids
- ID       : io_grid

Description
----------
Tools for the import and export of gridded data.
'''

from PySAGA.helper import Tool_Wrapper

def Export_ESRI_ArcInfo_Grid(GRID=None, FILE=None, FORMAT=None, GEOREF=None, PREC=None, DECSEP=None, Verbose=2):
    '''
    Export ESRI Arc/Info Grid
    ----------
    [io_grid.0]\n
    Export grid to ESRI's Arc/Info grid format.\n
    Arguments
    ----------
    - GRID [`input grid`] : Grid
    - FILE [`file path`] : File
    - FORMAT [`choice`] : Format. Available Choices: [0] binary [1] ASCII Default: 1
    - GEOREF [`choice`] : Geo-Reference. Available Choices: [0] corner [1] center Default: 0 The grids geo-reference must be related either to the center or the corner of its lower left grid cell.
    - PREC [`integer number`] : ASCII Precision. Minimum: -1 Default: 4 Number of decimals when writing floating point values in ASCII format.
    - DECSEP [`choice`] : ASCII Decimal Separator. Available Choices: [0] point (.) [1] comma (,) Default: 0 Applies also to the binary format header file.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('io_grid', '0', 'Export ESRI Arc/Info Grid')
    if Tool.is_Okay():
        Tool.Set_Input ('GRID', GRID)
        Tool.Set_Option('FILE', FILE)
        Tool.Set_Option('FORMAT', FORMAT)
        Tool.Set_Option('GEOREF', GEOREF)
        Tool.Set_Option('PREC', PREC)
        Tool.Set_Option('DECSEP', DECSEP)
        return Tool.Execute(Verbose)
    return False

def run_tool_io_grid_0(GRID=None, FILE=None, FORMAT=None, GEOREF=None, PREC=None, DECSEP=None, Verbose=2):
    '''
    Export ESRI Arc/Info Grid
    ----------
    [io_grid.0]\n
    Export grid to ESRI's Arc/Info grid format.\n
    Arguments
    ----------
    - GRID [`input grid`] : Grid
    - FILE [`file path`] : File
    - FORMAT [`choice`] : Format. Available Choices: [0] binary [1] ASCII Default: 1
    - GEOREF [`choice`] : Geo-Reference. Available Choices: [0] corner [1] center Default: 0 The grids geo-reference must be related either to the center or the corner of its lower left grid cell.
    - PREC [`integer number`] : ASCII Precision. Minimum: -1 Default: 4 Number of decimals when writing floating point values in ASCII format.
    - DECSEP [`choice`] : ASCII Decimal Separator. Available Choices: [0] point (.) [1] comma (,) Default: 0 Applies also to the binary format header file.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('io_grid', '0', 'Export ESRI Arc/Info Grid')
    if Tool.is_Okay():
        Tool.Set_Input ('GRID', GRID)
        Tool.Set_Option('FILE', FILE)
        Tool.Set_Option('FORMAT', FORMAT)
        Tool.Set_Option('GEOREF', GEOREF)
        Tool.Set_Option('PREC', PREC)
        Tool.Set_Option('DECSEP', DECSEP)
        return Tool.Execute(Verbose)
    return False

def Import_ESRI_ArcInfo_Grid(GRID=None, FILE=None, GRID_TYPE=None, NODATA=None, NODATA_VAL=None, CRS_STRING=None, Verbose=2):
    '''
    Import ESRI Arc/Info Grid
    ----------
    [io_grid.1]\n
    Import grid from ESRI's Arc/Info grid format.\n
    Arguments
    ----------
    - GRID [`output data object`] : Grid
    - FILE [`file path`] : File
    - GRID_TYPE [`choice`] : Target Grid Type. Available Choices: [0] Integer (2 byte) [1] Integer (4 byte) [2] Floating Point (4 byte) [3] Floating Point (8 byte) Default: 2
    - NODATA [`choice`] : No-Data Value. Available Choices: [0] Input File's NoData Value [1] User Defined NoData Value Default: 0 Choose whether the input file's NoData value or a user specified NoData value is written
    - NODATA_VAL [`floating point number`] : User Defined No-Data Value. Default: -99999.000000
    - CRS_STRING [`text`] : Coordinate System Definition. Supported formats comprise PROJ and WKT strings, object codes (e.g. "EPSG:4326").

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('io_grid', '1', 'Import ESRI Arc/Info Grid')
    if Tool.is_Okay():
        Tool.Set_Output('GRID', GRID)
        Tool.Set_Option('FILE', FILE)
        Tool.Set_Option('GRID_TYPE', GRID_TYPE)
        Tool.Set_Option('NODATA', NODATA)
        Tool.Set_Option('NODATA_VAL', NODATA_VAL)
        Tool.Set_Option('CRS_STRING', CRS_STRING)
        return Tool.Execute(Verbose)
    return False

def run_tool_io_grid_1(GRID=None, FILE=None, GRID_TYPE=None, NODATA=None, NODATA_VAL=None, CRS_STRING=None, Verbose=2):
    '''
    Import ESRI Arc/Info Grid
    ----------
    [io_grid.1]\n
    Import grid from ESRI's Arc/Info grid format.\n
    Arguments
    ----------
    - GRID [`output data object`] : Grid
    - FILE [`file path`] : File
    - GRID_TYPE [`choice`] : Target Grid Type. Available Choices: [0] Integer (2 byte) [1] Integer (4 byte) [2] Floating Point (4 byte) [3] Floating Point (8 byte) Default: 2
    - NODATA [`choice`] : No-Data Value. Available Choices: [0] Input File's NoData Value [1] User Defined NoData Value Default: 0 Choose whether the input file's NoData value or a user specified NoData value is written
    - NODATA_VAL [`floating point number`] : User Defined No-Data Value. Default: -99999.000000
    - CRS_STRING [`text`] : Coordinate System Definition. Supported formats comprise PROJ and WKT strings, object codes (e.g. "EPSG:4326").

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('io_grid', '1', 'Import ESRI Arc/Info Grid')
    if Tool.is_Okay():
        Tool.Set_Output('GRID', GRID)
        Tool.Set_Option('FILE', FILE)
        Tool.Set_Option('GRID_TYPE', GRID_TYPE)
        Tool.Set_Option('NODATA', NODATA)
        Tool.Set_Option('NODATA_VAL', NODATA_VAL)
        Tool.Set_Option('CRS_STRING', CRS_STRING)
        return Tool.Execute(Verbose)
    return False

def Export_Surfer_Grid(GRID=None, FILE=None, FORMAT=None, NODATA=None, Verbose=2):
    '''
    Export Surfer Grid
    ----------
    [io_grid.2]\n
    Export grid to Golden Software's Surfer grid format.\n
    Arguments
    ----------
    - GRID [`input grid`] : Grid
    - FILE [`file path`] : File
    - FORMAT [`choice`] : Format. Available Choices: [0] binary [1] ASCII Default: 0
    - NODATA [`boolean`] : Use Surfer's No-Data Value. Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('io_grid', '2', 'Export Surfer Grid')
    if Tool.is_Okay():
        Tool.Set_Input ('GRID', GRID)
        Tool.Set_Option('FILE', FILE)
        Tool.Set_Option('FORMAT', FORMAT)
        Tool.Set_Option('NODATA', NODATA)
        return Tool.Execute(Verbose)
    return False

def run_tool_io_grid_2(GRID=None, FILE=None, FORMAT=None, NODATA=None, Verbose=2):
    '''
    Export Surfer Grid
    ----------
    [io_grid.2]\n
    Export grid to Golden Software's Surfer grid format.\n
    Arguments
    ----------
    - GRID [`input grid`] : Grid
    - FILE [`file path`] : File
    - FORMAT [`choice`] : Format. Available Choices: [0] binary [1] ASCII Default: 0
    - NODATA [`boolean`] : Use Surfer's No-Data Value. Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('io_grid', '2', 'Export Surfer Grid')
    if Tool.is_Okay():
        Tool.Set_Input ('GRID', GRID)
        Tool.Set_Option('FILE', FILE)
        Tool.Set_Option('FORMAT', FORMAT)
        Tool.Set_Option('NODATA', NODATA)
        return Tool.Execute(Verbose)
    return False

def Import_Surfer_Grid(GRID=None, FILE=None, NODATA=None, NODATA_VAL=None, CRS_STRING=None, Verbose=2):
    '''
    Import Surfer Grid
    ----------
    [io_grid.3]\n
    Import grid from Golden Software's Surfer grid format.\n
    Arguments
    ----------
    - GRID [`output data object`] : Grid
    - FILE [`file path`] : File
    - NODATA [`choice`] : No Data Value. Available Choices: [0] Surfer's No Data Value [1] User Defined Default: 0
    - NODATA_VAL [`floating point number`] : User Defined No Data Value. Default: -99999.000000
    - CRS_STRING [`text`] : Coordinate System Definition. Supported formats comprise PROJ and WKT strings, object codes (e.g. "EPSG:4326").

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('io_grid', '3', 'Import Surfer Grid')
    if Tool.is_Okay():
        Tool.Set_Output('GRID', GRID)
        Tool.Set_Option('FILE', FILE)
        Tool.Set_Option('NODATA', NODATA)
        Tool.Set_Option('NODATA_VAL', NODATA_VAL)
        Tool.Set_Option('CRS_STRING', CRS_STRING)
        return Tool.Execute(Verbose)
    return False

def run_tool_io_grid_3(GRID=None, FILE=None, NODATA=None, NODATA_VAL=None, CRS_STRING=None, Verbose=2):
    '''
    Import Surfer Grid
    ----------
    [io_grid.3]\n
    Import grid from Golden Software's Surfer grid format.\n
    Arguments
    ----------
    - GRID [`output data object`] : Grid
    - FILE [`file path`] : File
    - NODATA [`choice`] : No Data Value. Available Choices: [0] Surfer's No Data Value [1] User Defined Default: 0
    - NODATA_VAL [`floating point number`] : User Defined No Data Value. Default: -99999.000000
    - CRS_STRING [`text`] : Coordinate System Definition. Supported formats comprise PROJ and WKT strings, object codes (e.g. "EPSG:4326").

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('io_grid', '3', 'Import Surfer Grid')
    if Tool.is_Okay():
        Tool.Set_Output('GRID', GRID)
        Tool.Set_Option('FILE', FILE)
        Tool.Set_Option('NODATA', NODATA)
        Tool.Set_Option('NODATA_VAL', NODATA_VAL)
        Tool.Set_Option('CRS_STRING', CRS_STRING)
        return Tool.Execute(Verbose)
    return False

def Import_Binary_Raw_Data(GRID=None, FILE=None, NX=None, NY=None, CELLSIZE=None, POS_VECTOR=None, POS_X=None, POS_X_SIDE=None, POS_Y=None, POS_Y_SIDE=None, DATA_TYPE=None, BYTEORDER=None, UNIT=None, ZFACTOR=None, NODATA=None, DATA_OFFSET=None, LINE_OFFSET=None, LINE_ENDSET=None, ORDER=None, TOPDOWN=None, LEFTRIGHT=None, CRS_STRING=None, Verbose=2):
    '''
    Import Binary Raw Data
    ----------
    [io_grid.4]\n
    Imports grid from binary raw data.\n
    Arguments
    ----------
    - GRID [`output data object`] : Grid
    - FILE [`file path`] : File
    - NX [`integer number`] : Number of Columns. Minimum: 1 Default: 1
    - NY [`integer number`] : Number of Rows. Minimum: 1 Default: 1
    - CELLSIZE [`floating point number`] : Cell Size. Minimum: 0.000000 Default: 1.000000
    - POS_VECTOR [`choice`] : Position Vector. Available Choices: [0] cell's center [1] cell's corner Default: 0
    - POS_X [`floating point number`] : X. Default: 0.000000
    - POS_X_SIDE [`choice`] : Side. Available Choices: [0] left [1] right Default: 0
    - POS_Y [`floating point number`] : Y. Default: 0.000000
    - POS_Y_SIDE [`choice`] : Side. Available Choices: [0] top [1] bottom Default: 0
    - DATA_TYPE [`data type`] : Data Type. Available Choices: [0] unsigned 1 byte integer [1] signed 1 byte integer [2] unsigned 2 byte integer [3] signed 2 byte integer [4] unsigned 4 byte integer [5] signed 4 byte integer [6] unsigned 8 byte integer [7] signed 8 byte integer [8] 4 byte floating point number [9] 8 byte floating point number Default: 8
    - BYTEORDER [`choice`] : Byte Order. Available Choices: [0] Little Endian (Intel) [1] Big Endian (Motorola) Default: 0
    - UNIT [`text`] : Unit
    - ZFACTOR [`floating point number`] : Z-Scale. Default: 1.000000
    - NODATA [`floating point number`] : No Data Value. Default: -99999.000000
    - DATA_OFFSET [`integer number`] : Data Offset (Bytes). Default: 0
    - LINE_OFFSET [`integer number`] : Record Offset (Bytes). Default: 0
    - LINE_ENDSET [`integer number`] : Record Endset (Bytes). Default: 0
    - ORDER [`choice`] : Value Order. Available Choices: [0] columns by rows [1] rows by columns Default: 0
    - TOPDOWN [`boolean`] : Invert Row Order. Default: 1
    - LEFTRIGHT [`boolean`] : Invert Column Order. Default: 0
    - CRS_STRING [`text`] : Coordinate System Definition. Supported formats comprise PROJ and WKT strings, object codes (e.g. "EPSG:4326").

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('io_grid', '4', 'Import Binary Raw Data')
    if Tool.is_Okay():
        Tool.Set_Output('GRID', GRID)
        Tool.Set_Option('FILE', FILE)
        Tool.Set_Option('NX', NX)
        Tool.Set_Option('NY', NY)
        Tool.Set_Option('CELLSIZE', CELLSIZE)
        Tool.Set_Option('POS_VECTOR', POS_VECTOR)
        Tool.Set_Option('POS_X', POS_X)
        Tool.Set_Option('POS_X_SIDE', POS_X_SIDE)
        Tool.Set_Option('POS_Y', POS_Y)
        Tool.Set_Option('POS_Y_SIDE', POS_Y_SIDE)
        Tool.Set_Option('DATA_TYPE', DATA_TYPE)
        Tool.Set_Option('BYTEORDER', BYTEORDER)
        Tool.Set_Option('UNIT', UNIT)
        Tool.Set_Option('ZFACTOR', ZFACTOR)
        Tool.Set_Option('NODATA', NODATA)
        Tool.Set_Option('DATA_OFFSET', DATA_OFFSET)
        Tool.Set_Option('LINE_OFFSET', LINE_OFFSET)
        Tool.Set_Option('LINE_ENDSET', LINE_ENDSET)
        Tool.Set_Option('ORDER', ORDER)
        Tool.Set_Option('TOPDOWN', TOPDOWN)
        Tool.Set_Option('LEFTRIGHT', LEFTRIGHT)
        Tool.Set_Option('CRS_STRING', CRS_STRING)
        return Tool.Execute(Verbose)
    return False

def run_tool_io_grid_4(GRID=None, FILE=None, NX=None, NY=None, CELLSIZE=None, POS_VECTOR=None, POS_X=None, POS_X_SIDE=None, POS_Y=None, POS_Y_SIDE=None, DATA_TYPE=None, BYTEORDER=None, UNIT=None, ZFACTOR=None, NODATA=None, DATA_OFFSET=None, LINE_OFFSET=None, LINE_ENDSET=None, ORDER=None, TOPDOWN=None, LEFTRIGHT=None, CRS_STRING=None, Verbose=2):
    '''
    Import Binary Raw Data
    ----------
    [io_grid.4]\n
    Imports grid from binary raw data.\n
    Arguments
    ----------
    - GRID [`output data object`] : Grid
    - FILE [`file path`] : File
    - NX [`integer number`] : Number of Columns. Minimum: 1 Default: 1
    - NY [`integer number`] : Number of Rows. Minimum: 1 Default: 1
    - CELLSIZE [`floating point number`] : Cell Size. Minimum: 0.000000 Default: 1.000000
    - POS_VECTOR [`choice`] : Position Vector. Available Choices: [0] cell's center [1] cell's corner Default: 0
    - POS_X [`floating point number`] : X. Default: 0.000000
    - POS_X_SIDE [`choice`] : Side. Available Choices: [0] left [1] right Default: 0
    - POS_Y [`floating point number`] : Y. Default: 0.000000
    - POS_Y_SIDE [`choice`] : Side. Available Choices: [0] top [1] bottom Default: 0
    - DATA_TYPE [`data type`] : Data Type. Available Choices: [0] unsigned 1 byte integer [1] signed 1 byte integer [2] unsigned 2 byte integer [3] signed 2 byte integer [4] unsigned 4 byte integer [5] signed 4 byte integer [6] unsigned 8 byte integer [7] signed 8 byte integer [8] 4 byte floating point number [9] 8 byte floating point number Default: 8
    - BYTEORDER [`choice`] : Byte Order. Available Choices: [0] Little Endian (Intel) [1] Big Endian (Motorola) Default: 0
    - UNIT [`text`] : Unit
    - ZFACTOR [`floating point number`] : Z-Scale. Default: 1.000000
    - NODATA [`floating point number`] : No Data Value. Default: -99999.000000
    - DATA_OFFSET [`integer number`] : Data Offset (Bytes). Default: 0
    - LINE_OFFSET [`integer number`] : Record Offset (Bytes). Default: 0
    - LINE_ENDSET [`integer number`] : Record Endset (Bytes). Default: 0
    - ORDER [`choice`] : Value Order. Available Choices: [0] columns by rows [1] rows by columns Default: 0
    - TOPDOWN [`boolean`] : Invert Row Order. Default: 1
    - LEFTRIGHT [`boolean`] : Invert Column Order. Default: 0
    - CRS_STRING [`text`] : Coordinate System Definition. Supported formats comprise PROJ and WKT strings, object codes (e.g. "EPSG:4326").

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('io_grid', '4', 'Import Binary Raw Data')
    if Tool.is_Okay():
        Tool.Set_Output('GRID', GRID)
        Tool.Set_Option('FILE', FILE)
        Tool.Set_Option('NX', NX)
        Tool.Set_Option('NY', NY)
        Tool.Set_Option('CELLSIZE', CELLSIZE)
        Tool.Set_Option('POS_VECTOR', POS_VECTOR)
        Tool.Set_Option('POS_X', POS_X)
        Tool.Set_Option('POS_X_SIDE', POS_X_SIDE)
        Tool.Set_Option('POS_Y', POS_Y)
        Tool.Set_Option('POS_Y_SIDE', POS_Y_SIDE)
        Tool.Set_Option('DATA_TYPE', DATA_TYPE)
        Tool.Set_Option('BYTEORDER', BYTEORDER)
        Tool.Set_Option('UNIT', UNIT)
        Tool.Set_Option('ZFACTOR', ZFACTOR)
        Tool.Set_Option('NODATA', NODATA)
        Tool.Set_Option('DATA_OFFSET', DATA_OFFSET)
        Tool.Set_Option('LINE_OFFSET', LINE_OFFSET)
        Tool.Set_Option('LINE_ENDSET', LINE_ENDSET)
        Tool.Set_Option('ORDER', ORDER)
        Tool.Set_Option('TOPDOWN', TOPDOWN)
        Tool.Set_Option('LEFTRIGHT', LEFTRIGHT)
        Tool.Set_Option('CRS_STRING', CRS_STRING)
        return Tool.Execute(Verbose)
    return False

def Export_Grid_to_XYZ(GRIDS=None, FILENAME=None, HEADER=None, NODATA=None, PREC=None, SEPARATOR=None, SEP_OTHER=None, Verbose=2):
    '''
    Export Grid to XYZ
    ----------
    [io_grid.5]\n
    The tool allows one to export a grid or several grids to a table (text format), which stores the x/y-coordinates and the cell values of the input grid(s).\n
    By default, No-Data cells are not written to the output. This can be changed with the "Write No-Data" parameter. If No-Data cells are skipped, these are detected for the first input grid which operates like a mask.\n
    Arguments
    ----------
    - GRIDS [`input grid list`] : Grids. The grid(s) to export.
    - FILENAME [`file path`] : File. The output file.
    - HEADER [`boolean`] : Write Header. Default: 1 Write a header to the .xyz file.
    - NODATA [`boolean`] : Write No-Data. Default: 0 Write No-Data cells to the .xyz file.
    - PREC [`integer number`] : Floating Point Precision. Minimum: -1 Default: 2 Number of decimals of exported floating point values. A value of -1 writes the significant decimals.
    - SEPARATOR [`choice`] : Field Separator. Available Choices: [0] tabulator [1] ; [2] , [3] space [4] other Default: 0
    - SEP_OTHER [`text`] : other. Default: *

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('io_grid', '5', 'Export Grid to XYZ')
    if Tool.is_Okay():
        Tool.Set_Input ('GRIDS', GRIDS)
        Tool.Set_Option('FILENAME', FILENAME)
        Tool.Set_Option('HEADER', HEADER)
        Tool.Set_Option('NODATA', NODATA)
        Tool.Set_Option('PREC', PREC)
        Tool.Set_Option('SEPARATOR', SEPARATOR)
        Tool.Set_Option('SEP_OTHER', SEP_OTHER)
        return Tool.Execute(Verbose)
    return False

def run_tool_io_grid_5(GRIDS=None, FILENAME=None, HEADER=None, NODATA=None, PREC=None, SEPARATOR=None, SEP_OTHER=None, Verbose=2):
    '''
    Export Grid to XYZ
    ----------
    [io_grid.5]\n
    The tool allows one to export a grid or several grids to a table (text format), which stores the x/y-coordinates and the cell values of the input grid(s).\n
    By default, No-Data cells are not written to the output. This can be changed with the "Write No-Data" parameter. If No-Data cells are skipped, these are detected for the first input grid which operates like a mask.\n
    Arguments
    ----------
    - GRIDS [`input grid list`] : Grids. The grid(s) to export.
    - FILENAME [`file path`] : File. The output file.
    - HEADER [`boolean`] : Write Header. Default: 1 Write a header to the .xyz file.
    - NODATA [`boolean`] : Write No-Data. Default: 0 Write No-Data cells to the .xyz file.
    - PREC [`integer number`] : Floating Point Precision. Minimum: -1 Default: 2 Number of decimals of exported floating point values. A value of -1 writes the significant decimals.
    - SEPARATOR [`choice`] : Field Separator. Available Choices: [0] tabulator [1] ; [2] , [3] space [4] other Default: 0
    - SEP_OTHER [`text`] : other. Default: *

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('io_grid', '5', 'Export Grid to XYZ')
    if Tool.is_Okay():
        Tool.Set_Input ('GRIDS', GRIDS)
        Tool.Set_Option('FILENAME', FILENAME)
        Tool.Set_Option('HEADER', HEADER)
        Tool.Set_Option('NODATA', NODATA)
        Tool.Set_Option('PREC', PREC)
        Tool.Set_Option('SEPARATOR', SEPARATOR)
        Tool.Set_Option('SEP_OTHER', SEP_OTHER)
        return Tool.Execute(Verbose)
    return False

def Import_Grid_from_XYZ(GRID=None, COUNT=None, FILENAME=None, SKIP=None, SEPARATOR=None, USER=None, TYPE=None, CELLSIZE=None, COUNT_CREATE=None, CRS_STRING=None, Verbose=2):
    '''
    Import Grid from XYZ
    ----------
    [io_grid.6]\n
    The tool allows one to import gridded data from a table (text format), which contains for each grid cell the x/y-coordinates and the cell value. If several values end up in a single grid cell, the mean value of all input values is written to that cell.\n
    Arguments
    ----------
    - GRID [`output data object`] : Grid. The imported grid.
    - COUNT [`output data object`] : Count Grid. The number of values detected in each grid cell.
    - FILENAME [`file path`] : File. The input file.
    - SKIP [`integer number`] : Skip Leading Lines. Minimum: 0 Default: 0 The number of leading lines to skip (usually header data).
    - SEPARATOR [`choice`] : Separator. Available Choices: [0] default delimiters [1] space [2] , [3] ; [4] tabulator [5] user defined Default: 0
    - USER [`text`] : User Defined. Default: * The user defined delimiter.
    - TYPE [`data type`] : Data Storage Type. Available Choices: [0] bit [1] unsigned 1 byte integer [2] signed 1 byte integer [3] unsigned 2 byte integer [4] signed 2 byte integer [5] unsigned 4 byte integer [6] signed 4 byte integer [7] unsigned 8 byte integer [8] signed 8 byte integer [9] 4 byte floating point number [10] 8 byte floating point number Default: 9
    - CELLSIZE [`floating point number`] : Cell Size. Minimum: 0.000000 Default: 1.000000 The cell size of the output grid. Set to zero to let the tool suggest an appropriate cellsize.
    - COUNT_CREATE [`boolean`] : Count Grid. Default: 0 Create a grid with the number of values falling into each target grid cell.
    - CRS_STRING [`text`] : Coordinate System Definition. Supported formats comprise PROJ and WKT strings, object codes (e.g. "EPSG:4326").

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('io_grid', '6', 'Import Grid from XYZ')
    if Tool.is_Okay():
        Tool.Set_Output('GRID', GRID)
        Tool.Set_Output('COUNT', COUNT)
        Tool.Set_Option('FILENAME', FILENAME)
        Tool.Set_Option('SKIP', SKIP)
        Tool.Set_Option('SEPARATOR', SEPARATOR)
        Tool.Set_Option('USER', USER)
        Tool.Set_Option('TYPE', TYPE)
        Tool.Set_Option('CELLSIZE', CELLSIZE)
        Tool.Set_Option('COUNT_CREATE', COUNT_CREATE)
        Tool.Set_Option('CRS_STRING', CRS_STRING)
        return Tool.Execute(Verbose)
    return False

def run_tool_io_grid_6(GRID=None, COUNT=None, FILENAME=None, SKIP=None, SEPARATOR=None, USER=None, TYPE=None, CELLSIZE=None, COUNT_CREATE=None, CRS_STRING=None, Verbose=2):
    '''
    Import Grid from XYZ
    ----------
    [io_grid.6]\n
    The tool allows one to import gridded data from a table (text format), which contains for each grid cell the x/y-coordinates and the cell value. If several values end up in a single grid cell, the mean value of all input values is written to that cell.\n
    Arguments
    ----------
    - GRID [`output data object`] : Grid. The imported grid.
    - COUNT [`output data object`] : Count Grid. The number of values detected in each grid cell.
    - FILENAME [`file path`] : File. The input file.
    - SKIP [`integer number`] : Skip Leading Lines. Minimum: 0 Default: 0 The number of leading lines to skip (usually header data).
    - SEPARATOR [`choice`] : Separator. Available Choices: [0] default delimiters [1] space [2] , [3] ; [4] tabulator [5] user defined Default: 0
    - USER [`text`] : User Defined. Default: * The user defined delimiter.
    - TYPE [`data type`] : Data Storage Type. Available Choices: [0] bit [1] unsigned 1 byte integer [2] signed 1 byte integer [3] unsigned 2 byte integer [4] signed 2 byte integer [5] unsigned 4 byte integer [6] signed 4 byte integer [7] unsigned 8 byte integer [8] signed 8 byte integer [9] 4 byte floating point number [10] 8 byte floating point number Default: 9
    - CELLSIZE [`floating point number`] : Cell Size. Minimum: 0.000000 Default: 1.000000 The cell size of the output grid. Set to zero to let the tool suggest an appropriate cellsize.
    - COUNT_CREATE [`boolean`] : Count Grid. Default: 0 Create a grid with the number of values falling into each target grid cell.
    - CRS_STRING [`text`] : Coordinate System Definition. Supported formats comprise PROJ and WKT strings, object codes (e.g. "EPSG:4326").

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('io_grid', '6', 'Import Grid from XYZ')
    if Tool.is_Okay():
        Tool.Set_Output('GRID', GRID)
        Tool.Set_Output('COUNT', COUNT)
        Tool.Set_Option('FILENAME', FILENAME)
        Tool.Set_Option('SKIP', SKIP)
        Tool.Set_Option('SEPARATOR', SEPARATOR)
        Tool.Set_Option('USER', USER)
        Tool.Set_Option('TYPE', TYPE)
        Tool.Set_Option('CELLSIZE', CELLSIZE)
        Tool.Set_Option('COUNT_CREATE', COUNT_CREATE)
        Tool.Set_Option('CRS_STRING', CRS_STRING)
        return Tool.Execute(Verbose)
    return False

def Import_USGS_SRTM_Grid(GRIDS=None, FILE=None, RESOLUTION=None, Verbose=2):
    '''
    Import USGS SRTM Grid
    ----------
    [io_grid.7]\n
    Import grid from USGS SRTM (Shuttle Radar Topography Mission) data.\n
    You find data and further information at:\n
    [  http://dds.cr.usgs.gov/srtm/](http://dds.cr.usgs.gov/srtm/)\n
    [  http://www.jpl.nasa.gov/srtm/](http://www.jpl.nasa.gov/srtm/)\n
    Farr, T.G., M. Kobrick (2000):\n
    'Shuttle Radar Topography Mission produces a wealth of data',\n
    Amer. Geophys. Union Eos, v. 81, p. 583-585\n
    Rosen, P.A., S. Hensley, I.R. Joughin, F.K. Li, S.N. Madsen, E. Rodriguez, R.M. Goldstein (2000):\n
    'Synthetic aperture radar interferometry'\n
    Proc. IEEE, v. 88, p. 333-382\n
    Arguments
    ----------
    - GRIDS [`output grid list`] : Grids
    - FILE [`file path`] : Files
    - RESOLUTION [`choice`] : Resolution. Available Choices: [0] 1 arc-second [1] 3 arc-second Default: 1

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('io_grid', '7', 'Import USGS SRTM Grid')
    if Tool.is_Okay():
        Tool.Set_Output('GRIDS', GRIDS)
        Tool.Set_Option('FILE', FILE)
        Tool.Set_Option('RESOLUTION', RESOLUTION)
        return Tool.Execute(Verbose)
    return False

def run_tool_io_grid_7(GRIDS=None, FILE=None, RESOLUTION=None, Verbose=2):
    '''
    Import USGS SRTM Grid
    ----------
    [io_grid.7]\n
    Import grid from USGS SRTM (Shuttle Radar Topography Mission) data.\n
    You find data and further information at:\n
    [  http://dds.cr.usgs.gov/srtm/](http://dds.cr.usgs.gov/srtm/)\n
    [  http://www.jpl.nasa.gov/srtm/](http://www.jpl.nasa.gov/srtm/)\n
    Farr, T.G., M. Kobrick (2000):\n
    'Shuttle Radar Topography Mission produces a wealth of data',\n
    Amer. Geophys. Union Eos, v. 81, p. 583-585\n
    Rosen, P.A., S. Hensley, I.R. Joughin, F.K. Li, S.N. Madsen, E. Rodriguez, R.M. Goldstein (2000):\n
    'Synthetic aperture radar interferometry'\n
    Proc. IEEE, v. 88, p. 333-382\n
    Arguments
    ----------
    - GRIDS [`output grid list`] : Grids
    - FILE [`file path`] : Files
    - RESOLUTION [`choice`] : Resolution. Available Choices: [0] 1 arc-second [1] 3 arc-second Default: 1

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('io_grid', '7', 'Import USGS SRTM Grid')
    if Tool.is_Okay():
        Tool.Set_Output('GRIDS', GRIDS)
        Tool.Set_Option('FILE', FILE)
        Tool.Set_Option('RESOLUTION', RESOLUTION)
        return Tool.Execute(Verbose)
    return False

def Import_MOLA_Grid_MEGDR(GRID=None, FILE=None, TYPE=None, ORIENT=None, Verbose=2):
    '''
    Import MOLA Grid (MEGDR)
    ----------
    [io_grid.8]\n
    Import Mars Orbit Laser Altimeter (MOLA) grids of the Mars Global Surveyor (MGS) Mission (Topographic maps, Mission Experiment Gridded Data Records - MEGDRs). Find more information and obtain free data from [Mars Global Surveyor: MOLA (NASA)](https://pds-geosciences.wustl.edu/missions/mgs/mola.html)\n
    Arguments
    ----------
    - GRID [`output data object`] : Grid
    - FILE [`file path`] : File
    - TYPE [`choice`] : Grid Type. Available Choices: [0] 2 byte integer [1] 4 byte floating point Default: 1
    - ORIENT [`choice`] : Orientation. Available Choices: [0] normal [1] down under Default: 1

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('io_grid', '8', 'Import MOLA Grid (MEGDR)')
    if Tool.is_Okay():
        Tool.Set_Output('GRID', GRID)
        Tool.Set_Option('FILE', FILE)
        Tool.Set_Option('TYPE', TYPE)
        Tool.Set_Option('ORIENT', ORIENT)
        return Tool.Execute(Verbose)
    return False

def run_tool_io_grid_8(GRID=None, FILE=None, TYPE=None, ORIENT=None, Verbose=2):
    '''
    Import MOLA Grid (MEGDR)
    ----------
    [io_grid.8]\n
    Import Mars Orbit Laser Altimeter (MOLA) grids of the Mars Global Surveyor (MGS) Mission (Topographic maps, Mission Experiment Gridded Data Records - MEGDRs). Find more information and obtain free data from [Mars Global Surveyor: MOLA (NASA)](https://pds-geosciences.wustl.edu/missions/mgs/mola.html)\n
    Arguments
    ----------
    - GRID [`output data object`] : Grid
    - FILE [`file path`] : File
    - TYPE [`choice`] : Grid Type. Available Choices: [0] 2 byte integer [1] 4 byte floating point Default: 1
    - ORIENT [`choice`] : Orientation. Available Choices: [0] normal [1] down under Default: 1

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('io_grid', '8', 'Import MOLA Grid (MEGDR)')
    if Tool.is_Okay():
        Tool.Set_Output('GRID', GRID)
        Tool.Set_Option('FILE', FILE)
        Tool.Set_Option('TYPE', TYPE)
        Tool.Set_Option('ORIENT', ORIENT)
        return Tool.Execute(Verbose)
    return False

def Import_SRTM30_DEM(GRID=None, PATH=None, XMIN=None, XMAX=None, YMIN=None, YMAX=None, Verbose=2):
    '''
    Import SRTM30 DEM
    ----------
    [io_grid.9]\n
    Extracts elevation grids from SRTM30 data.\n
    '"SRTM30 is a near-global digital elevation model (DEM) comprising a combination of data from the Shuttle Radar Topography Mission, flown in February, 2000 and the the U.S. Geological Survey's GTOPO30 data set. It can be considered to be either an SRTM data set enhanced with GTOPO30, or as an upgrade to GTOPO30."' (NASA)\n
    Further information about the GTOPO30 data set:\n
    [http://edcdaac.usgs.gov/gtopo30/gtopo30.html](http://edcdaac.usgs.gov/gtopo30/gtopo30.html)\n
    SRTM30 data can be downloaded from:\n
    [ftp://e0srp01u.ecs.nasa.gov/srtm/version2/SRTM30/](ftp://e0srp01u.ecs.nasa.gov/srtm/version2/SRTM30/)\n
    A directory, that contains the uncompressed SRTM30 DEM files, can be located using the "Path" Parameter of this tool.\n
    Arguments
    ----------
    - GRID [`output data object`] : Grid
    - PATH [`file path`] : Path
    - XMIN [`integer number`] : West. Default: 60
    - XMAX [`integer number`] : East. Default: 120
    - YMIN [`integer number`] : South. Default: 20
    - YMAX [`integer number`] : North. Default: 50

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('io_grid', '9', 'Import SRTM30 DEM')
    if Tool.is_Okay():
        Tool.Set_Output('GRID', GRID)
        Tool.Set_Option('PATH', PATH)
        Tool.Set_Option('XMIN', XMIN)
        Tool.Set_Option('XMAX', XMAX)
        Tool.Set_Option('YMIN', YMIN)
        Tool.Set_Option('YMAX', YMAX)
        return Tool.Execute(Verbose)
    return False

def run_tool_io_grid_9(GRID=None, PATH=None, XMIN=None, XMAX=None, YMIN=None, YMAX=None, Verbose=2):
    '''
    Import SRTM30 DEM
    ----------
    [io_grid.9]\n
    Extracts elevation grids from SRTM30 data.\n
    '"SRTM30 is a near-global digital elevation model (DEM) comprising a combination of data from the Shuttle Radar Topography Mission, flown in February, 2000 and the the U.S. Geological Survey's GTOPO30 data set. It can be considered to be either an SRTM data set enhanced with GTOPO30, or as an upgrade to GTOPO30."' (NASA)\n
    Further information about the GTOPO30 data set:\n
    [http://edcdaac.usgs.gov/gtopo30/gtopo30.html](http://edcdaac.usgs.gov/gtopo30/gtopo30.html)\n
    SRTM30 data can be downloaded from:\n
    [ftp://e0srp01u.ecs.nasa.gov/srtm/version2/SRTM30/](ftp://e0srp01u.ecs.nasa.gov/srtm/version2/SRTM30/)\n
    A directory, that contains the uncompressed SRTM30 DEM files, can be located using the "Path" Parameter of this tool.\n
    Arguments
    ----------
    - GRID [`output data object`] : Grid
    - PATH [`file path`] : Path
    - XMIN [`integer number`] : West. Default: 60
    - XMAX [`integer number`] : East. Default: 120
    - YMIN [`integer number`] : South. Default: 20
    - YMAX [`integer number`] : North. Default: 50

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('io_grid', '9', 'Import SRTM30 DEM')
    if Tool.is_Okay():
        Tool.Set_Output('GRID', GRID)
        Tool.Set_Option('PATH', PATH)
        Tool.Set_Option('XMIN', XMIN)
        Tool.Set_Option('XMAX', XMAX)
        Tool.Set_Option('YMIN', YMIN)
        Tool.Set_Option('YMAX', YMAX)
        return Tool.Execute(Verbose)
    return False

def Export_True_Color_Bitmap(IMAGE=None, FILE=None, Verbose=2):
    '''
    Export True Color Bitmap
    ----------
    [io_grid.10]\n
    Export red-green-blue coded image grids to MS-Windows true color bitmaps. This tool writes the data directly to the file and is hence particularly suitable for very large data sets.\n
    Arguments
    ----------
    - IMAGE [`input grid`] : Image Grid
    - FILE [`file path`] : File

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('io_grid', '10', 'Export True Color Bitmap')
    if Tool.is_Okay():
        Tool.Set_Input ('IMAGE', IMAGE)
        Tool.Set_Option('FILE', FILE)
        return Tool.Execute(Verbose)
    return False

def run_tool_io_grid_10(IMAGE=None, FILE=None, Verbose=2):
    '''
    Export True Color Bitmap
    ----------
    [io_grid.10]\n
    Export red-green-blue coded image grids to MS-Windows true color bitmaps. This tool writes the data directly to the file and is hence particularly suitable for very large data sets.\n
    Arguments
    ----------
    - IMAGE [`input grid`] : Image Grid
    - FILE [`file path`] : File

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('io_grid', '10', 'Export True Color Bitmap')
    if Tool.is_Okay():
        Tool.Set_Input ('IMAGE', IMAGE)
        Tool.Set_Option('FILE', FILE)
        return Tool.Execute(Verbose)
    return False

def Import_Erdas_LANGIS(GRIDS=None, FILE=None, Verbose=2):
    '''
    Import Erdas LAN/GIS
    ----------
    [io_grid.11]\n
    Import Erdas LAN/GIS files.\n
    The format analysis is based on the GRASS tool i.in.erdas. Go to the [GRASS GIS Hompage](http://grass.itc.it/) for more information.\n
    Arguments
    ----------
    - GRIDS [`output grid list`] : Grids
    - FILE [`file path`] : File

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('io_grid', '11', 'Import Erdas LAN/GIS')
    if Tool.is_Okay():
        Tool.Set_Output('GRIDS', GRIDS)
        Tool.Set_Option('FILE', FILE)
        return Tool.Execute(Verbose)
    return False

def run_tool_io_grid_11(GRIDS=None, FILE=None, Verbose=2):
    '''
    Import Erdas LAN/GIS
    ----------
    [io_grid.11]\n
    Import Erdas LAN/GIS files.\n
    The format analysis is based on the GRASS tool i.in.erdas. Go to the [GRASS GIS Hompage](http://grass.itc.it/) for more information.\n
    Arguments
    ----------
    - GRIDS [`output grid list`] : Grids
    - FILE [`file path`] : File

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('io_grid', '11', 'Import Erdas LAN/GIS')
    if Tool.is_Okay():
        Tool.Set_Output('GRIDS', GRIDS)
        Tool.Set_Option('FILE', FILE)
        return Tool.Execute(Verbose)
    return False

def Import_Grid_from_Table(GRID=None, FILE=None, CELLSIZE=None, XMIN=None, YMIN=None, UNIT=None, ZFACTOR=None, NODATA=None, HEADLINES=None, DATA_TYPE=None, TOPDOWN=None, CRS_STRING=None, Verbose=2):
    '''
    Import Grid from Table
    ----------
    [io_grid.12]\n
    Imports a grid from a table.\n
    Arguments
    ----------
    - GRID [`output data object`] : Grid
    - FILE [`file path`] : Table
    - CELLSIZE [`floating point number`] : Cell Size. Default: 1.000000
    - XMIN [`floating point number`] : Left Border. Default: 0.000000
    - YMIN [`floating point number`] : Lower Border. Default: 0.000000
    - UNIT [`text`] : Unit Name
    - ZFACTOR [`floating point number`] : Z Multiplier. Default: 1.000000
    - NODATA [`floating point number`] : No Data Value. Default: -99999.000000
    - HEADLINES [`integer number`] : Header Lines. Minimum: 0 Default: 0
    - DATA_TYPE [`data type`] : Data Type. Available Choices: [0] bit [1] unsigned 1 byte integer [2] signed 1 byte integer [3] unsigned 2 byte integer [4] signed 2 byte integer [5] unsigned 4 byte integer [6] signed 4 byte integer [7] unsigned 8 byte integer [8] signed 8 byte integer [9] 4 byte floating point number [10] 8 byte floating point number Default: 9
    - TOPDOWN [`choice`] : Line Order. Available Choices: [0] Bottom to Top [1] Top to Bottom Default: 0
    - CRS_STRING [`text`] : Coordinate System Definition. Supported formats comprise PROJ and WKT strings, object codes (e.g. "EPSG:4326").

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('io_grid', '12', 'Import Grid from Table')
    if Tool.is_Okay():
        Tool.Set_Output('GRID', GRID)
        Tool.Set_Option('FILE', FILE)
        Tool.Set_Option('CELLSIZE', CELLSIZE)
        Tool.Set_Option('XMIN', XMIN)
        Tool.Set_Option('YMIN', YMIN)
        Tool.Set_Option('UNIT', UNIT)
        Tool.Set_Option('ZFACTOR', ZFACTOR)
        Tool.Set_Option('NODATA', NODATA)
        Tool.Set_Option('HEADLINES', HEADLINES)
        Tool.Set_Option('DATA_TYPE', DATA_TYPE)
        Tool.Set_Option('TOPDOWN', TOPDOWN)
        Tool.Set_Option('CRS_STRING', CRS_STRING)
        return Tool.Execute(Verbose)
    return False

def run_tool_io_grid_12(GRID=None, FILE=None, CELLSIZE=None, XMIN=None, YMIN=None, UNIT=None, ZFACTOR=None, NODATA=None, HEADLINES=None, DATA_TYPE=None, TOPDOWN=None, CRS_STRING=None, Verbose=2):
    '''
    Import Grid from Table
    ----------
    [io_grid.12]\n
    Imports a grid from a table.\n
    Arguments
    ----------
    - GRID [`output data object`] : Grid
    - FILE [`file path`] : Table
    - CELLSIZE [`floating point number`] : Cell Size. Default: 1.000000
    - XMIN [`floating point number`] : Left Border. Default: 0.000000
    - YMIN [`floating point number`] : Lower Border. Default: 0.000000
    - UNIT [`text`] : Unit Name
    - ZFACTOR [`floating point number`] : Z Multiplier. Default: 1.000000
    - NODATA [`floating point number`] : No Data Value. Default: -99999.000000
    - HEADLINES [`integer number`] : Header Lines. Minimum: 0 Default: 0
    - DATA_TYPE [`data type`] : Data Type. Available Choices: [0] bit [1] unsigned 1 byte integer [2] signed 1 byte integer [3] unsigned 2 byte integer [4] signed 2 byte integer [5] unsigned 4 byte integer [6] signed 4 byte integer [7] unsigned 8 byte integer [8] signed 8 byte integer [9] 4 byte floating point number [10] 8 byte floating point number Default: 9
    - TOPDOWN [`choice`] : Line Order. Available Choices: [0] Bottom to Top [1] Top to Bottom Default: 0
    - CRS_STRING [`text`] : Coordinate System Definition. Supported formats comprise PROJ and WKT strings, object codes (e.g. "EPSG:4326").

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('io_grid', '12', 'Import Grid from Table')
    if Tool.is_Okay():
        Tool.Set_Output('GRID', GRID)
        Tool.Set_Option('FILE', FILE)
        Tool.Set_Option('CELLSIZE', CELLSIZE)
        Tool.Set_Option('XMIN', XMIN)
        Tool.Set_Option('YMIN', YMIN)
        Tool.Set_Option('UNIT', UNIT)
        Tool.Set_Option('ZFACTOR', ZFACTOR)
        Tool.Set_Option('NODATA', NODATA)
        Tool.Set_Option('HEADLINES', HEADLINES)
        Tool.Set_Option('DATA_TYPE', DATA_TYPE)
        Tool.Set_Option('TOPDOWN', TOPDOWN)
        Tool.Set_Option('CRS_STRING', CRS_STRING)
        return Tool.Execute(Verbose)
    return False

def Import_WRF_Geogrid_Binary_Format(GRIDS=None, FILE=None, Verbose=2):
    '''
    Import WRF Geogrid Binary Format
    ----------
    [io_grid.13]\n
    Imports grid(s) from Weather Research and Forecasting Model (WRF) geogrid binary format.\n
    Arguments
    ----------
    - GRIDS [`output grid list`] : Grids
    - FILE [`file path`] : File

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('io_grid', '13', 'Import WRF Geogrid Binary Format')
    if Tool.is_Okay():
        Tool.Set_Output('GRIDS', GRIDS)
        Tool.Set_Option('FILE', FILE)
        return Tool.Execute(Verbose)
    return False

def run_tool_io_grid_13(GRIDS=None, FILE=None, Verbose=2):
    '''
    Import WRF Geogrid Binary Format
    ----------
    [io_grid.13]\n
    Imports grid(s) from Weather Research and Forecasting Model (WRF) geogrid binary format.\n
    Arguments
    ----------
    - GRIDS [`output grid list`] : Grids
    - FILE [`file path`] : File

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('io_grid', '13', 'Import WRF Geogrid Binary Format')
    if Tool.is_Okay():
        Tool.Set_Output('GRIDS', GRIDS)
        Tool.Set_Option('FILE', FILE)
        return Tool.Execute(Verbose)
    return False

def Export_WRF_Geogrid_Binary_Format(GRIDS=None, FILE=None, DATATYPE=None, TYPE=None, NAME_DIGITS=None, MISSING=None, SCALE=None, UNITS=None, DESCRIPTION=None, MMINLU=None, TILE_BDR=None, PROJECTION=None, SDTLON=None, TRUELAT1=None, TRUELAT2=None, ISWATER=None, ISLAKE=None, ISICE=None, ISURBAN=None, ISOILWATER=None, Verbose=2):
    '''
    Export WRF Geogrid Binary Format
    ----------
    [io_grid.14]\n
    Exports grid(s) to Weather Research and Forecasting Model (WRF) geogrid binary format.\n
    Arguments
    ----------
    - GRIDS [`input grid list`] : Grids
    - FILE [`file path`] : Directory
    - DATATYPE [`choice`] : Data Type. Available Choices: [0] 1 byte unsigned [1] 1 byte signed [2] 2 byte unsigned [3] 2 byte signed [4] 4 byte unsigned [5] 4 byte signed Default: 0
    - TYPE [`choice`] : Type. Available Choices: [0] categorical [1] continuous Default: 0
    - NAME_DIGITS [`integer number`] : Filename Digits. Minimum: 5 Maximum: 6 Default: 5
    - MISSING [`floating point number`] : Missing Value. Default: -99999.000000
    - SCALE [`floating point number`] : Scale Factor. Default: 1.000000
    - UNITS [`text`] : Units
    - DESCRIPTION [`text`] : Description
    - MMINLU [`text`] : Look Up Section. Default: USGS
    - TILE_BDR [`integer number`] : Halo Width. Minimum: 0 Default: 0
    - PROJECTION [`choice`] : Projection. Available Choices: [0] lambert [1] polar [2] mercator [3] regular_ll [4] albers_nad83 [5] polar_wgs84 Default: 3
    - SDTLON [`floating point number`] : Standard Longitude. Default: 0.000000
    - TRUELAT1 [`floating point number`] : True Latitude 1. Default: 45.000000
    - TRUELAT2 [`floating point number`] : True Latitude 2. Default: 35.000000
    - ISWATER [`integer number`] : Water. Default: 16
    - ISLAKE [`integer number`] : Lake. Default: -1
    - ISICE [`integer number`] : Ice. Default: 24
    - ISURBAN [`integer number`] : Urban. Default: 1
    - ISOILWATER [`integer number`] : Soil Water. Default: 14

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('io_grid', '14', 'Export WRF Geogrid Binary Format')
    if Tool.is_Okay():
        Tool.Set_Input ('GRIDS', GRIDS)
        Tool.Set_Option('FILE', FILE)
        Tool.Set_Option('DATATYPE', DATATYPE)
        Tool.Set_Option('TYPE', TYPE)
        Tool.Set_Option('NAME_DIGITS', NAME_DIGITS)
        Tool.Set_Option('MISSING', MISSING)
        Tool.Set_Option('SCALE', SCALE)
        Tool.Set_Option('UNITS', UNITS)
        Tool.Set_Option('DESCRIPTION', DESCRIPTION)
        Tool.Set_Option('MMINLU', MMINLU)
        Tool.Set_Option('TILE_BDR', TILE_BDR)
        Tool.Set_Option('PROJECTION', PROJECTION)
        Tool.Set_Option('SDTLON', SDTLON)
        Tool.Set_Option('TRUELAT1', TRUELAT1)
        Tool.Set_Option('TRUELAT2', TRUELAT2)
        Tool.Set_Option('ISWATER', ISWATER)
        Tool.Set_Option('ISLAKE', ISLAKE)
        Tool.Set_Option('ISICE', ISICE)
        Tool.Set_Option('ISURBAN', ISURBAN)
        Tool.Set_Option('ISOILWATER', ISOILWATER)
        return Tool.Execute(Verbose)
    return False

def run_tool_io_grid_14(GRIDS=None, FILE=None, DATATYPE=None, TYPE=None, NAME_DIGITS=None, MISSING=None, SCALE=None, UNITS=None, DESCRIPTION=None, MMINLU=None, TILE_BDR=None, PROJECTION=None, SDTLON=None, TRUELAT1=None, TRUELAT2=None, ISWATER=None, ISLAKE=None, ISICE=None, ISURBAN=None, ISOILWATER=None, Verbose=2):
    '''
    Export WRF Geogrid Binary Format
    ----------
    [io_grid.14]\n
    Exports grid(s) to Weather Research and Forecasting Model (WRF) geogrid binary format.\n
    Arguments
    ----------
    - GRIDS [`input grid list`] : Grids
    - FILE [`file path`] : Directory
    - DATATYPE [`choice`] : Data Type. Available Choices: [0] 1 byte unsigned [1] 1 byte signed [2] 2 byte unsigned [3] 2 byte signed [4] 4 byte unsigned [5] 4 byte signed Default: 0
    - TYPE [`choice`] : Type. Available Choices: [0] categorical [1] continuous Default: 0
    - NAME_DIGITS [`integer number`] : Filename Digits. Minimum: 5 Maximum: 6 Default: 5
    - MISSING [`floating point number`] : Missing Value. Default: -99999.000000
    - SCALE [`floating point number`] : Scale Factor. Default: 1.000000
    - UNITS [`text`] : Units
    - DESCRIPTION [`text`] : Description
    - MMINLU [`text`] : Look Up Section. Default: USGS
    - TILE_BDR [`integer number`] : Halo Width. Minimum: 0 Default: 0
    - PROJECTION [`choice`] : Projection. Available Choices: [0] lambert [1] polar [2] mercator [3] regular_ll [4] albers_nad83 [5] polar_wgs84 Default: 3
    - SDTLON [`floating point number`] : Standard Longitude. Default: 0.000000
    - TRUELAT1 [`floating point number`] : True Latitude 1. Default: 45.000000
    - TRUELAT2 [`floating point number`] : True Latitude 2. Default: 35.000000
    - ISWATER [`integer number`] : Water. Default: 16
    - ISLAKE [`integer number`] : Lake. Default: -1
    - ISICE [`integer number`] : Ice. Default: 24
    - ISURBAN [`integer number`] : Urban. Default: 1
    - ISOILWATER [`integer number`] : Soil Water. Default: 14

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('io_grid', '14', 'Export WRF Geogrid Binary Format')
    if Tool.is_Okay():
        Tool.Set_Input ('GRIDS', GRIDS)
        Tool.Set_Option('FILE', FILE)
        Tool.Set_Option('DATATYPE', DATATYPE)
        Tool.Set_Option('TYPE', TYPE)
        Tool.Set_Option('NAME_DIGITS', NAME_DIGITS)
        Tool.Set_Option('MISSING', MISSING)
        Tool.Set_Option('SCALE', SCALE)
        Tool.Set_Option('UNITS', UNITS)
        Tool.Set_Option('DESCRIPTION', DESCRIPTION)
        Tool.Set_Option('MMINLU', MMINLU)
        Tool.Set_Option('TILE_BDR', TILE_BDR)
        Tool.Set_Option('PROJECTION', PROJECTION)
        Tool.Set_Option('SDTLON', SDTLON)
        Tool.Set_Option('TRUELAT1', TRUELAT1)
        Tool.Set_Option('TRUELAT2', TRUELAT2)
        Tool.Set_Option('ISWATER', ISWATER)
        Tool.Set_Option('ISLAKE', ISLAKE)
        Tool.Set_Option('ISICE', ISICE)
        Tool.Set_Option('ISURBAN', ISURBAN)
        Tool.Set_Option('ISOILWATER', ISOILWATER)
        return Tool.Execute(Verbose)
    return False

def Import_Clip_and_Resample_Grids(CLIP=None, GRIDS=None, FILES=None, KEEP_TYPE=None, NODATA=None, NODATA_VAL=None, RESAMPLE=None, CELLSIZE=None, SCALE_UP=None, SCALE_DOWN=None, Verbose=2):
    '''
    Import, Clip and Resample Grids
    ----------
    [io_grid.16]\n
    Imports and optionally clips and/or resamples selected raster files.\n
    Arguments
    ----------
    - CLIP [`optional input shapes`] : Region of Interest. bounding box for clipping
    - GRIDS [`output grid list`] : Grids
    - FILES [`file path`] : Files
    - KEEP_TYPE [`boolean`] : Preserve Data Type. Default: 0
    - NODATA [`boolean`] : User Defined No-Data Value. Default: 0
    - NODATA_VAL [`floating point number`] : No-Data Value. Default: 0.000000
    - RESAMPLE [`boolean`] : Resample. Default: 0
    - CELLSIZE [`floating point number`] : Cell Size. Minimum: 0.000000 Default: 100.000000
    - SCALE_UP [`choice`] : Upscaling Method. Available Choices: [0] Nearest Neighbour [1] Bilinear Interpolation [2] Bicubic Spline Interpolation [3] B-Spline Interpolation [4] Mean Value [5] Mean Value (cell area weighted) [6] Minimum Value [7] Maximum Value [8] Majority Default: 5
    - SCALE_DOWN [`choice`] : Downscaling Method. Available Choices: [0] Nearest Neighbour [1] Bilinear Interpolation [2] Bicubic Spline Interpolation [3] B-Spline Interpolation Default: 3

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('io_grid', '16', 'Import, Clip and Resample Grids')
    if Tool.is_Okay():
        Tool.Set_Input ('CLIP', CLIP)
        Tool.Set_Output('GRIDS', GRIDS)
        Tool.Set_Option('FILES', FILES)
        Tool.Set_Option('KEEP_TYPE', KEEP_TYPE)
        Tool.Set_Option('NODATA', NODATA)
        Tool.Set_Option('NODATA_VAL', NODATA_VAL)
        Tool.Set_Option('RESAMPLE', RESAMPLE)
        Tool.Set_Option('CELLSIZE', CELLSIZE)
        Tool.Set_Option('SCALE_UP', SCALE_UP)
        Tool.Set_Option('SCALE_DOWN', SCALE_DOWN)
        return Tool.Execute(Verbose)
    return False

def run_tool_io_grid_16(CLIP=None, GRIDS=None, FILES=None, KEEP_TYPE=None, NODATA=None, NODATA_VAL=None, RESAMPLE=None, CELLSIZE=None, SCALE_UP=None, SCALE_DOWN=None, Verbose=2):
    '''
    Import, Clip and Resample Grids
    ----------
    [io_grid.16]\n
    Imports and optionally clips and/or resamples selected raster files.\n
    Arguments
    ----------
    - CLIP [`optional input shapes`] : Region of Interest. bounding box for clipping
    - GRIDS [`output grid list`] : Grids
    - FILES [`file path`] : Files
    - KEEP_TYPE [`boolean`] : Preserve Data Type. Default: 0
    - NODATA [`boolean`] : User Defined No-Data Value. Default: 0
    - NODATA_VAL [`floating point number`] : No-Data Value. Default: 0.000000
    - RESAMPLE [`boolean`] : Resample. Default: 0
    - CELLSIZE [`floating point number`] : Cell Size. Minimum: 0.000000 Default: 100.000000
    - SCALE_UP [`choice`] : Upscaling Method. Available Choices: [0] Nearest Neighbour [1] Bilinear Interpolation [2] Bicubic Spline Interpolation [3] B-Spline Interpolation [4] Mean Value [5] Mean Value (cell area weighted) [6] Minimum Value [7] Maximum Value [8] Majority Default: 5
    - SCALE_DOWN [`choice`] : Downscaling Method. Available Choices: [0] Nearest Neighbour [1] Bilinear Interpolation [2] Bicubic Spline Interpolation [3] B-Spline Interpolation Default: 3

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('io_grid', '16', 'Import, Clip and Resample Grids')
    if Tool.is_Okay():
        Tool.Set_Input ('CLIP', CLIP)
        Tool.Set_Output('GRIDS', GRIDS)
        Tool.Set_Option('FILES', FILES)
        Tool.Set_Option('KEEP_TYPE', KEEP_TYPE)
        Tool.Set_Option('NODATA', NODATA)
        Tool.Set_Option('NODATA_VAL', NODATA_VAL)
        Tool.Set_Option('RESAMPLE', RESAMPLE)
        Tool.Set_Option('CELLSIZE', CELLSIZE)
        Tool.Set_Option('SCALE_UP', SCALE_UP)
        Tool.Set_Option('SCALE_DOWN', SCALE_DOWN)
        return Tool.Execute(Verbose)
    return False

def Import_CRU_Grids(GRIDS=None, FILE=None, SHIFT=None, Verbose=2):
    '''
    Import CRU Grids
    ----------
    [io_grid.17]\n
    Import grids from 'Climatic Research Unit Global Climate Dataset' files.\n
    Arguments
    ----------
    - GRIDS [`output grid list`] : Grids
    - FILE [`file path`] : File
    - SHIFT [`boolean`] : Shift. Default: 1

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('io_grid', '17', 'Import CRU Grids')
    if Tool.is_Okay():
        Tool.Set_Output('GRIDS', GRIDS)
        Tool.Set_Option('FILE', FILE)
        Tool.Set_Option('SHIFT', SHIFT)
        return Tool.Execute(Verbose)
    return False

def run_tool_io_grid_17(GRIDS=None, FILE=None, SHIFT=None, Verbose=2):
    '''
    Import CRU Grids
    ----------
    [io_grid.17]\n
    Import grids from 'Climatic Research Unit Global Climate Dataset' files.\n
    Arguments
    ----------
    - GRIDS [`output grid list`] : Grids
    - FILE [`file path`] : File
    - SHIFT [`boolean`] : Shift. Default: 1

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('io_grid', '17', 'Import CRU Grids')
    if Tool.is_Okay():
        Tool.Set_Output('GRIDS', GRIDS)
        Tool.Set_Option('FILE', FILE)
        Tool.Set_Option('SHIFT', SHIFT)
        return Tool.Execute(Verbose)
    return False

def Import_Regular_SubsurfaceViewer_Grid(TABLE=None, POINTS=None, LAYERS=None, GRIDS=None, FILE=None, FIELD=None, BLAYERS=None, NLAYERS=None, Verbose=2):
    '''
    Import Regular SubsurfaceViewer Grid
    ----------
    [io_grid.18]\n
    Import a SubsurfaceViewer file. Works only for regular cells (x/y).\n
    (SubsurfaceViewer - a GIS for the geological subsurface)\n
    Arguments
    ----------
    - TABLE [`output table`] : Table
    - POINTS [`output shapes`] : Cell Centers
    - LAYERS [`output data object`] : Layers
    - GRIDS [`output grid collection list`] : Grids
    - FILE [`file path`] : File. The input file.
    - FIELD [`choice`] : Layer Field. Available Choices: [0] name Default: 0
    - BLAYERS [`boolean`] : Layers Grid Collection. Default: 0
    - NLAYERS [`integer number`] : Number of Grids. Minimum: 2 Default: 10

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('io_grid', '18', 'Import Regular SubsurfaceViewer Grid')
    if Tool.is_Okay():
        Tool.Set_Output('TABLE', TABLE)
        Tool.Set_Output('POINTS', POINTS)
        Tool.Set_Output('LAYERS', LAYERS)
        Tool.Set_Output('GRIDS', GRIDS)
        Tool.Set_Option('FILE', FILE)
        Tool.Set_Option('FIELD', FIELD)
        Tool.Set_Option('BLAYERS', BLAYERS)
        Tool.Set_Option('NLAYERS', NLAYERS)
        return Tool.Execute(Verbose)
    return False

def run_tool_io_grid_18(TABLE=None, POINTS=None, LAYERS=None, GRIDS=None, FILE=None, FIELD=None, BLAYERS=None, NLAYERS=None, Verbose=2):
    '''
    Import Regular SubsurfaceViewer Grid
    ----------
    [io_grid.18]\n
    Import a SubsurfaceViewer file. Works only for regular cells (x/y).\n
    (SubsurfaceViewer - a GIS for the geological subsurface)\n
    Arguments
    ----------
    - TABLE [`output table`] : Table
    - POINTS [`output shapes`] : Cell Centers
    - LAYERS [`output data object`] : Layers
    - GRIDS [`output grid collection list`] : Grids
    - FILE [`file path`] : File. The input file.
    - FIELD [`choice`] : Layer Field. Available Choices: [0] name Default: 0
    - BLAYERS [`boolean`] : Layers Grid Collection. Default: 0
    - NLAYERS [`integer number`] : Number of Grids. Minimum: 2 Default: 10

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('io_grid', '18', 'Import Regular SubsurfaceViewer Grid')
    if Tool.is_Okay():
        Tool.Set_Output('TABLE', TABLE)
        Tool.Set_Output('POINTS', POINTS)
        Tool.Set_Output('LAYERS', LAYERS)
        Tool.Set_Output('GRIDS', GRIDS)
        Tool.Set_Option('FILE', FILE)
        Tool.Set_Option('FIELD', FIELD)
        Tool.Set_Option('BLAYERS', BLAYERS)
        Tool.Set_Option('NLAYERS', NLAYERS)
        return Tool.Execute(Verbose)
    return False

