#! /usr/bin/env python

'''
Python Interface to SAGA Tools Library
----------
- Category : Terrain Analysis
- Name     : Preprocessing
- ID       : ta_preprocessor

Description
----------
Tools for the preprocessing of digital terrain models.
'''

from PySAGA.helper import Tool_Wrapper

def Flat_Detection(DEM=None, NOFLATS=None, FLATS=None, FLAT_OUTPUT=None, NEIGHBOURHOOD=None, Verbose=2):
    '''
    Flat Detection
    ----------
    [ta_preprocessor.0]\n
    Identifies areas of connected cells sharing identical values. Connected cells can be searched by Neumann or Moore neighbourhood.\n
    Arguments
    ----------
    - DEM [`input grid`] : DEM
    - NOFLATS [`output grid`] : No Flats
    - FLATS [`output grid`] : Flat Areas
    - FLAT_OUTPUT [`choice`] : Flat Area Values. Available Choices: [0] elevation [1] enumeration Default: 0
    - NEIGHBOURHOOD [`choice`] : Neighbourhood. Available Choices: [0] Neumann [1] Moore Default: 1

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_preprocessor', '0', 'Flat Detection')
    if Tool.is_Okay():
        Tool.Set_Input ('DEM', DEM)
        Tool.Set_Output('NOFLATS', NOFLATS)
        Tool.Set_Output('FLATS', FLATS)
        Tool.Set_Option('FLAT_OUTPUT', FLAT_OUTPUT)
        Tool.Set_Option('NEIGHBOURHOOD', NEIGHBOURHOOD)
        return Tool.Execute(Verbose)
    return False

def run_tool_ta_preprocessor_0(DEM=None, NOFLATS=None, FLATS=None, FLAT_OUTPUT=None, NEIGHBOURHOOD=None, Verbose=2):
    '''
    Flat Detection
    ----------
    [ta_preprocessor.0]\n
    Identifies areas of connected cells sharing identical values. Connected cells can be searched by Neumann or Moore neighbourhood.\n
    Arguments
    ----------
    - DEM [`input grid`] : DEM
    - NOFLATS [`output grid`] : No Flats
    - FLATS [`output grid`] : Flat Areas
    - FLAT_OUTPUT [`choice`] : Flat Area Values. Available Choices: [0] elevation [1] enumeration Default: 0
    - NEIGHBOURHOOD [`choice`] : Neighbourhood. Available Choices: [0] Neumann [1] Moore Default: 1

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_preprocessor', '0', 'Flat Detection')
    if Tool.is_Okay():
        Tool.Set_Input ('DEM', DEM)
        Tool.Set_Output('NOFLATS', NOFLATS)
        Tool.Set_Output('FLATS', FLATS)
        Tool.Set_Option('FLAT_OUTPUT', FLAT_OUTPUT)
        Tool.Set_Option('NEIGHBOURHOOD', NEIGHBOURHOOD)
        return Tool.Execute(Verbose)
    return False

def Sink_Drainage_Route_Detection(ELEVATION=None, SINKROUTE=None, THRESHOLD=None, THRSHEIGHT=None, Verbose=2):
    '''
    Sink Drainage Route Detection
    ----------
    [ta_preprocessor.1]\n
    Sink drainage route detection.\n
    Arguments
    ----------
    - ELEVATION [`input grid`] : Elevation
    - SINKROUTE [`output grid`] : Sink Route
    - THRESHOLD [`boolean`] : Threshold. Default: 0
    - THRSHEIGHT [`floating point number`] : Threshold Height. Minimum: 0.000000 Default: 100.000000 The maximum depth to which a sink is considered for removal.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_preprocessor', '1', 'Sink Drainage Route Detection')
    if Tool.is_Okay():
        Tool.Set_Input ('ELEVATION', ELEVATION)
        Tool.Set_Output('SINKROUTE', SINKROUTE)
        Tool.Set_Option('THRESHOLD', THRESHOLD)
        Tool.Set_Option('THRSHEIGHT', THRSHEIGHT)
        return Tool.Execute(Verbose)
    return False

def run_tool_ta_preprocessor_1(ELEVATION=None, SINKROUTE=None, THRESHOLD=None, THRSHEIGHT=None, Verbose=2):
    '''
    Sink Drainage Route Detection
    ----------
    [ta_preprocessor.1]\n
    Sink drainage route detection.\n
    Arguments
    ----------
    - ELEVATION [`input grid`] : Elevation
    - SINKROUTE [`output grid`] : Sink Route
    - THRESHOLD [`boolean`] : Threshold. Default: 0
    - THRSHEIGHT [`floating point number`] : Threshold Height. Minimum: 0.000000 Default: 100.000000 The maximum depth to which a sink is considered for removal.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_preprocessor', '1', 'Sink Drainage Route Detection')
    if Tool.is_Okay():
        Tool.Set_Input ('ELEVATION', ELEVATION)
        Tool.Set_Output('SINKROUTE', SINKROUTE)
        Tool.Set_Option('THRESHOLD', THRESHOLD)
        Tool.Set_Option('THRSHEIGHT', THRSHEIGHT)
        return Tool.Execute(Verbose)
    return False

def Sink_Removal(DEM=None, SINKROUTE=None, DEM_PREPROC=None, METHOD=None, THRESHOLD=None, THRSHEIGHT=None, EPSILON=None, Verbose=2):
    '''
    Sink Removal
    ----------
    [ta_preprocessor.2]\n
    Sink removal.\n
    Arguments
    ----------
    - DEM [`input grid`] : DEM. Digital Elevation Model that has to be processed
    - SINKROUTE [`optional input grid`] : Sink Route
    - DEM_PREPROC [`output grid`] : Preprocessed DEM. Preprocessed DEM. If this is not set changes will be stored in the original DEM grid.
    - METHOD [`choice`] : Method. Available Choices: [0] Deepen Drainage Routes [1] Fill Sinks Default: 1
    - THRESHOLD [`boolean`] : Threshold. Default: 0
    - THRSHEIGHT [`floating point number`] : Threshold Height. Minimum: 0.000000 Default: 100.000000 The maximum depth to which a sink is considered for removal.
    - EPSILON [`floating point number`] : Epsilon. Minimum: 0.000000 Default: 0.001000 Difference in elevation used to create non-flat gradient conserving filled/carved surfaces.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_preprocessor', '2', 'Sink Removal')
    if Tool.is_Okay():
        Tool.Set_Input ('DEM', DEM)
        Tool.Set_Input ('SINKROUTE', SINKROUTE)
        Tool.Set_Output('DEM_PREPROC', DEM_PREPROC)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('THRESHOLD', THRESHOLD)
        Tool.Set_Option('THRSHEIGHT', THRSHEIGHT)
        Tool.Set_Option('EPSILON', EPSILON)
        return Tool.Execute(Verbose)
    return False

def run_tool_ta_preprocessor_2(DEM=None, SINKROUTE=None, DEM_PREPROC=None, METHOD=None, THRESHOLD=None, THRSHEIGHT=None, EPSILON=None, Verbose=2):
    '''
    Sink Removal
    ----------
    [ta_preprocessor.2]\n
    Sink removal.\n
    Arguments
    ----------
    - DEM [`input grid`] : DEM. Digital Elevation Model that has to be processed
    - SINKROUTE [`optional input grid`] : Sink Route
    - DEM_PREPROC [`output grid`] : Preprocessed DEM. Preprocessed DEM. If this is not set changes will be stored in the original DEM grid.
    - METHOD [`choice`] : Method. Available Choices: [0] Deepen Drainage Routes [1] Fill Sinks Default: 1
    - THRESHOLD [`boolean`] : Threshold. Default: 0
    - THRSHEIGHT [`floating point number`] : Threshold Height. Minimum: 0.000000 Default: 100.000000 The maximum depth to which a sink is considered for removal.
    - EPSILON [`floating point number`] : Epsilon. Minimum: 0.000000 Default: 0.001000 Difference in elevation used to create non-flat gradient conserving filled/carved surfaces.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_preprocessor', '2', 'Sink Removal')
    if Tool.is_Okay():
        Tool.Set_Input ('DEM', DEM)
        Tool.Set_Input ('SINKROUTE', SINKROUTE)
        Tool.Set_Output('DEM_PREPROC', DEM_PREPROC)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('THRESHOLD', THRESHOLD)
        Tool.Set_Option('THRSHEIGHT', THRSHEIGHT)
        Tool.Set_Option('EPSILON', EPSILON)
        return Tool.Execute(Verbose)
    return False

def Fill_Sinks_PlanchonDarboux_2001(DEM=None, RESULT=None, MINSLOPE=None, Verbose=2):
    '''
    Fill Sinks (Planchon/Darboux, 2001)
    ----------
    [ta_preprocessor.3]\n
    Depression filling algorithm after Olivier Planchon & Frederic Darboux (2001).\n
    Arguments
    ----------
    - DEM [`input grid`] : DEM. digital elevation model [m]
    - RESULT [`output grid`] : Filled DEM. processed DEM
    - MINSLOPE [`floating point number`] : Minimum Slope [Degree]. Minimum: 0.000000 Default: 0.010000 minimum slope angle preserved from one cell to the next, zero results in flat areas [Degree]

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_preprocessor', '3', 'Fill Sinks (Planchon/Darboux, 2001)')
    if Tool.is_Okay():
        Tool.Set_Input ('DEM', DEM)
        Tool.Set_Output('RESULT', RESULT)
        Tool.Set_Option('MINSLOPE', MINSLOPE)
        return Tool.Execute(Verbose)
    return False

def run_tool_ta_preprocessor_3(DEM=None, RESULT=None, MINSLOPE=None, Verbose=2):
    '''
    Fill Sinks (Planchon/Darboux, 2001)
    ----------
    [ta_preprocessor.3]\n
    Depression filling algorithm after Olivier Planchon & Frederic Darboux (2001).\n
    Arguments
    ----------
    - DEM [`input grid`] : DEM. digital elevation model [m]
    - RESULT [`output grid`] : Filled DEM. processed DEM
    - MINSLOPE [`floating point number`] : Minimum Slope [Degree]. Minimum: 0.000000 Default: 0.010000 minimum slope angle preserved from one cell to the next, zero results in flat areas [Degree]

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_preprocessor', '3', 'Fill Sinks (Planchon/Darboux, 2001)')
    if Tool.is_Okay():
        Tool.Set_Input ('DEM', DEM)
        Tool.Set_Output('RESULT', RESULT)
        Tool.Set_Option('MINSLOPE', MINSLOPE)
        return Tool.Execute(Verbose)
    return False

def Fill_Sinks_Wang_and_Liu(ELEV=None, FILLED=None, FDIR=None, WSHED=None, MINSLOPE=None, Verbose=2):
    '''
    Fill Sinks (Wang & Liu)
    ----------
    [ta_preprocessor.4]\n
    This tool uses an algorithm proposed by Wang & Liu to identify and fill surface depressions in digital elevation models.\n
    The method was enhanced to allow the creation of hydrologic sound elevation models, i.e. not only to fill the depression(s) but also to preserve a downward slope along the flow path. If desired, this is accomplished by preserving a minimum slope gradient (and thus elevation difference) between cells.\n
    This is the fully featured version of the tool creating a depression less DEM, a flow path grid and a grid with watershed basins. If you encounter problems processing large data sets (e.g. LIDAR data) with this tool try the basic version (Fill Sinks XXL).\n
    References:\n
    Wang, L. & H. Liu (2006): An efficient method for identifying and filling surface depressions in digital elevation models for hydrologic analysis and modelling. International Journal of Geographical Information Science, Vol. 20, No. 2: 193-213.\n
    Arguments
    ----------
    - ELEV [`input grid`] : DEM. Digital elevation model
    - FILLED [`output grid`] : Filled DEM. Depression-free digital elevation model
    - FDIR [`output grid`] : Flow Directions. Computed flow directions, 0=N, 1=NE, 2=E, ... 7=NW
    - WSHED [`output grid`] : Watershed Basins. Delineated watershed basins
    - MINSLOPE [`floating point number`] : Minimum Slope [Degree]. Minimum: 0.000000 Default: 0.100000 Minimum slope gradient to preserve from cell to cell; with a value of zero sinks are filled up to the spill elevation (which results in flat areas). Unit [Degree]

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_preprocessor', '4', 'Fill Sinks (Wang & Liu)')
    if Tool.is_Okay():
        Tool.Set_Input ('ELEV', ELEV)
        Tool.Set_Output('FILLED', FILLED)
        Tool.Set_Output('FDIR', FDIR)
        Tool.Set_Output('WSHED', WSHED)
        Tool.Set_Option('MINSLOPE', MINSLOPE)
        return Tool.Execute(Verbose)
    return False

def run_tool_ta_preprocessor_4(ELEV=None, FILLED=None, FDIR=None, WSHED=None, MINSLOPE=None, Verbose=2):
    '''
    Fill Sinks (Wang & Liu)
    ----------
    [ta_preprocessor.4]\n
    This tool uses an algorithm proposed by Wang & Liu to identify and fill surface depressions in digital elevation models.\n
    The method was enhanced to allow the creation of hydrologic sound elevation models, i.e. not only to fill the depression(s) but also to preserve a downward slope along the flow path. If desired, this is accomplished by preserving a minimum slope gradient (and thus elevation difference) between cells.\n
    This is the fully featured version of the tool creating a depression less DEM, a flow path grid and a grid with watershed basins. If you encounter problems processing large data sets (e.g. LIDAR data) with this tool try the basic version (Fill Sinks XXL).\n
    References:\n
    Wang, L. & H. Liu (2006): An efficient method for identifying and filling surface depressions in digital elevation models for hydrologic analysis and modelling. International Journal of Geographical Information Science, Vol. 20, No. 2: 193-213.\n
    Arguments
    ----------
    - ELEV [`input grid`] : DEM. Digital elevation model
    - FILLED [`output grid`] : Filled DEM. Depression-free digital elevation model
    - FDIR [`output grid`] : Flow Directions. Computed flow directions, 0=N, 1=NE, 2=E, ... 7=NW
    - WSHED [`output grid`] : Watershed Basins. Delineated watershed basins
    - MINSLOPE [`floating point number`] : Minimum Slope [Degree]. Minimum: 0.000000 Default: 0.100000 Minimum slope gradient to preserve from cell to cell; with a value of zero sinks are filled up to the spill elevation (which results in flat areas). Unit [Degree]

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_preprocessor', '4', 'Fill Sinks (Wang & Liu)')
    if Tool.is_Okay():
        Tool.Set_Input ('ELEV', ELEV)
        Tool.Set_Output('FILLED', FILLED)
        Tool.Set_Output('FDIR', FDIR)
        Tool.Set_Output('WSHED', WSHED)
        Tool.Set_Option('MINSLOPE', MINSLOPE)
        return Tool.Execute(Verbose)
    return False

def Fill_Sinks_XXL_Wang_and_Liu(ELEV=None, FILLED=None, MINSLOPE=None, Verbose=2):
    '''
    Fill Sinks XXL (Wang & Liu)
    ----------
    [ta_preprocessor.5]\n
    This tool uses an algorithm proposed by Wang & Liu to identify and fill surface depressions in digital elevation models.\n
    The method was enhanced to allow the creation of hydrologic sound elevation models, i.e. not only to fill the depression(s) but also to preserve a downward slope along the flow path. If desired, this is accomplished by preserving a minimum slope gradient (and thus elevation difference) between cells.\n
    This version of the tool is designed to work on large data sets (e.g. LIDAR data), with smaller datasets you might like to check out the fully featured standard version of the tool.\n
    References:\n
    Wang, L. & H. Liu (2006): An efficient method for identifying and filling surface depressions in digital elevation models for hydrologic analysis and modelling. International Journal of Geographical Information Science, Vol. 20, No. 2: 193-213.\n
    Arguments
    ----------
    - ELEV [`input grid`] : DEM. Digital elevation model
    - FILLED [`output grid`] : Filled DEM. Depression-free digital elevation model
    - MINSLOPE [`floating point number`] : Minimum Slope [Degree]. Minimum: 0.000000 Default: 0.100000 Minimum slope gradient to preserve from cell to cell; with a value of zero sinks are filled up to the spill elevation (which results in flat areas). Unit [Degree]

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_preprocessor', '5', 'Fill Sinks XXL (Wang & Liu)')
    if Tool.is_Okay():
        Tool.Set_Input ('ELEV', ELEV)
        Tool.Set_Output('FILLED', FILLED)
        Tool.Set_Option('MINSLOPE', MINSLOPE)
        return Tool.Execute(Verbose)
    return False

def run_tool_ta_preprocessor_5(ELEV=None, FILLED=None, MINSLOPE=None, Verbose=2):
    '''
    Fill Sinks XXL (Wang & Liu)
    ----------
    [ta_preprocessor.5]\n
    This tool uses an algorithm proposed by Wang & Liu to identify and fill surface depressions in digital elevation models.\n
    The method was enhanced to allow the creation of hydrologic sound elevation models, i.e. not only to fill the depression(s) but also to preserve a downward slope along the flow path. If desired, this is accomplished by preserving a minimum slope gradient (and thus elevation difference) between cells.\n
    This version of the tool is designed to work on large data sets (e.g. LIDAR data), with smaller datasets you might like to check out the fully featured standard version of the tool.\n
    References:\n
    Wang, L. & H. Liu (2006): An efficient method for identifying and filling surface depressions in digital elevation models for hydrologic analysis and modelling. International Journal of Geographical Information Science, Vol. 20, No. 2: 193-213.\n
    Arguments
    ----------
    - ELEV [`input grid`] : DEM. Digital elevation model
    - FILLED [`output grid`] : Filled DEM. Depression-free digital elevation model
    - MINSLOPE [`floating point number`] : Minimum Slope [Degree]. Minimum: 0.000000 Default: 0.100000 Minimum slope gradient to preserve from cell to cell; with a value of zero sinks are filled up to the spill elevation (which results in flat areas). Unit [Degree]

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_preprocessor', '5', 'Fill Sinks XXL (Wang & Liu)')
    if Tool.is_Okay():
        Tool.Set_Input ('ELEV', ELEV)
        Tool.Set_Output('FILLED', FILLED)
        Tool.Set_Option('MINSLOPE', MINSLOPE)
        return Tool.Execute(Verbose)
    return False

def Burn_Stream_Network_into_DEM(DEM=None, STREAM=None, FLOWDIR=None, BURN=None, METHOD=None, EPSILON=None, Verbose=2):
    '''
    Burn Stream Network into DEM
    ----------
    [ta_preprocessor.6]\n
    Burns a stream network into a Digital Elevation Model (DEM). Stream cells have to be coded with valid data values, all other cells should be set to no data value. First two methods decrease . The third method ensures a steady downstream gradient. An elevation decrease is only applied, if a downstream cell is equally high or higher. You should provide a grid with flow directions for determination of downstream cells. The 'Sink Drainage Route Detection' tool offers such flow directions.\n
    Arguments
    ----------
    - DEM [`input grid`] : DEM
    - STREAM [`input grid`] : Streams
    - FLOWDIR [`input grid`] : Flow Direction
    - BURN [`output grid`] : Processed DEM
    - METHOD [`choice`] : Method. Available Choices: [0] simply decrease cell's value by epsilon [1] lower cell's value to neighbours minimum value minus epsilon [2] trace stream network downstream Default: 0
    - EPSILON [`floating point number`] : Epsilon. Minimum: 0.000000 Default: 1.000000

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_preprocessor', '6', 'Burn Stream Network into DEM')
    if Tool.is_Okay():
        Tool.Set_Input ('DEM', DEM)
        Tool.Set_Input ('STREAM', STREAM)
        Tool.Set_Input ('FLOWDIR', FLOWDIR)
        Tool.Set_Output('BURN', BURN)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('EPSILON', EPSILON)
        return Tool.Execute(Verbose)
    return False

def run_tool_ta_preprocessor_6(DEM=None, STREAM=None, FLOWDIR=None, BURN=None, METHOD=None, EPSILON=None, Verbose=2):
    '''
    Burn Stream Network into DEM
    ----------
    [ta_preprocessor.6]\n
    Burns a stream network into a Digital Elevation Model (DEM). Stream cells have to be coded with valid data values, all other cells should be set to no data value. First two methods decrease . The third method ensures a steady downstream gradient. An elevation decrease is only applied, if a downstream cell is equally high or higher. You should provide a grid with flow directions for determination of downstream cells. The 'Sink Drainage Route Detection' tool offers such flow directions.\n
    Arguments
    ----------
    - DEM [`input grid`] : DEM
    - STREAM [`input grid`] : Streams
    - FLOWDIR [`input grid`] : Flow Direction
    - BURN [`output grid`] : Processed DEM
    - METHOD [`choice`] : Method. Available Choices: [0] simply decrease cell's value by epsilon [1] lower cell's value to neighbours minimum value minus epsilon [2] trace stream network downstream Default: 0
    - EPSILON [`floating point number`] : Epsilon. Minimum: 0.000000 Default: 1.000000

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_preprocessor', '6', 'Burn Stream Network into DEM')
    if Tool.is_Okay():
        Tool.Set_Input ('DEM', DEM)
        Tool.Set_Input ('STREAM', STREAM)
        Tool.Set_Input ('FLOWDIR', FLOWDIR)
        Tool.Set_Output('BURN', BURN)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('EPSILON', EPSILON)
        return Tool.Execute(Verbose)
    return False

def Breach_Depressions(DEM=None, NOSINKS=None, MAX_LENGTH=None, MAX_ZDEC=None, MIN_ZDROP=None, Verbose=2):
    '''
    Breach Depressions
    ----------
    [ta_preprocessor.7]\n
    This tool removes all depressions in a DEM by breaching. It can be used to pre-process a digital elevation model (DEM) prior to being used for hydrological analysis. It uses a cost-distance criteria for deciding upon the breach target, i.e. the cell to which the tool will trench a connecting path, and for determining the breach path itself (from the WhiteBox GAT documentation).\n
    This is a re-implementation of the 'Breach Depressions' Java code as provided by Dr. John Lindsay's WhiteBox GAT software.\n
    Arguments
    ----------
    - DEM [`input grid`] : Elevation
    - NOSINKS [`output grid`] : Preprocessed
    - MAX_LENGTH [`integer number`] : Maximum Breach Channel Length. Minimum: 1 Default: 50 [Cells]
    - MAX_ZDEC [`floating point number`] : Maximum Elevation Decrement. Minimum: 0.000000 Default: 0.000000
    - MIN_ZDROP [`floating point number`] : Minimum Elevation Drop. Minimum: 0.000000 Default: 0.000000

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_preprocessor', '7', 'Breach Depressions')
    if Tool.is_Okay():
        Tool.Set_Input ('DEM', DEM)
        Tool.Set_Output('NOSINKS', NOSINKS)
        Tool.Set_Option('MAX_LENGTH', MAX_LENGTH)
        Tool.Set_Option('MAX_ZDEC', MAX_ZDEC)
        Tool.Set_Option('MIN_ZDROP', MIN_ZDROP)
        return Tool.Execute(Verbose)
    return False

def run_tool_ta_preprocessor_7(DEM=None, NOSINKS=None, MAX_LENGTH=None, MAX_ZDEC=None, MIN_ZDROP=None, Verbose=2):
    '''
    Breach Depressions
    ----------
    [ta_preprocessor.7]\n
    This tool removes all depressions in a DEM by breaching. It can be used to pre-process a digital elevation model (DEM) prior to being used for hydrological analysis. It uses a cost-distance criteria for deciding upon the breach target, i.e. the cell to which the tool will trench a connecting path, and for determining the breach path itself (from the WhiteBox GAT documentation).\n
    This is a re-implementation of the 'Breach Depressions' Java code as provided by Dr. John Lindsay's WhiteBox GAT software.\n
    Arguments
    ----------
    - DEM [`input grid`] : Elevation
    - NOSINKS [`output grid`] : Preprocessed
    - MAX_LENGTH [`integer number`] : Maximum Breach Channel Length. Minimum: 1 Default: 50 [Cells]
    - MAX_ZDEC [`floating point number`] : Maximum Elevation Decrement. Minimum: 0.000000 Default: 0.000000
    - MIN_ZDROP [`floating point number`] : Minimum Elevation Drop. Minimum: 0.000000 Default: 0.000000

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_preprocessor', '7', 'Breach Depressions')
    if Tool.is_Okay():
        Tool.Set_Input ('DEM', DEM)
        Tool.Set_Output('NOSINKS', NOSINKS)
        Tool.Set_Option('MAX_LENGTH', MAX_LENGTH)
        Tool.Set_Option('MAX_ZDEC', MAX_ZDEC)
        Tool.Set_Option('MIN_ZDROP', MIN_ZDROP)
        return Tool.Execute(Verbose)
    return False

def Fill_Minima(DEM=None, RESULT=None, Verbose=2):
    '''
    Fill Minima
    ----------
    [ta_preprocessor.8]\n
    Minima filling. Currently only for unsigned 1 byte integer grids.\n
    Arguments
    ----------
    - DEM [`input grid`] : DEM. digital elevation model [m]
    - RESULT [`output grid`] : Filled DEM. processed DEM

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_preprocessor', '8', 'Fill Minima')
    if Tool.is_Okay():
        Tool.Set_Input ('DEM', DEM)
        Tool.Set_Output('RESULT', RESULT)
        return Tool.Execute(Verbose)
    return False

def run_tool_ta_preprocessor_8(DEM=None, RESULT=None, Verbose=2):
    '''
    Fill Minima
    ----------
    [ta_preprocessor.8]\n
    Minima filling. Currently only for unsigned 1 byte integer grids.\n
    Arguments
    ----------
    - DEM [`input grid`] : DEM. digital elevation model [m]
    - RESULT [`output grid`] : Filled DEM. processed DEM

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_preprocessor', '8', 'Fill Minima')
    if Tool.is_Okay():
        Tool.Set_Input ('DEM', DEM)
        Tool.Set_Output('RESULT', RESULT)
        return Tool.Execute(Verbose)
    return False

