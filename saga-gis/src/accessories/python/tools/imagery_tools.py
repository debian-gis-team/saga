#! /usr/bin/env python

'''
Python Interface to SAGA Tools Library
----------
- Category : Imagery
- Name     : Tools
- ID       : imagery_tools

Description
----------
Image processing and analysis tools.
'''

from PySAGA.helper import Tool_Wrapper

def Vegetation_Index_Distance_Based(RED=None, NIR=None, PVI0=None, PVI1=None, PVI2=None, PVI3=None, TSAVI=None, ATSAVI=None, INTERCEPT=None, SLOPE=None, Verbose=2):
    '''
    Vegetation Index (Distance Based)
    ----------
    [imagery_tools.0]\n
    Distance based vegetation indices.\n
    Arguments
    ----------
    - RED [`input grid`] : Red Reflectance
    - NIR [`input grid`] : Near Infrared Reflectance
    - PVI0 [`output grid`] : Perpendicular Vegetation Index (Richardson and Wiegand, 1977)
    - PVI1 [`output grid`] : Perpendicular Vegetation Index (Perry and Lautenschlager, 1984)
    - PVI2 [`output grid`] : Perpendicular Vegetation Index (Walther and Shabaani)
    - PVI3 [`output grid`] : Perpendicular Vegetation Index (Qi, et al., 1994)
    - TSAVI [`output grid`] : Transformed Soil Adjusted Vegetation Index (Baret et al. 1989)
    - ATSAVI [`output grid`] : Transformed Soil Adjusted Vegetation Index (Baret and Guyot, 1991)
    - INTERCEPT [`floating point number`] : Intercept of Soil Line. Default: 0.000000
    - SLOPE [`floating point number`] : Slope of Soil Line. Default: 0.500000

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('imagery_tools', '0', 'Vegetation Index (Distance Based)')
    if Tool.is_Okay():
        Tool.Set_Input ('RED', RED)
        Tool.Set_Input ('NIR', NIR)
        Tool.Set_Output('PVI0', PVI0)
        Tool.Set_Output('PVI1', PVI1)
        Tool.Set_Output('PVI2', PVI2)
        Tool.Set_Output('PVI3', PVI3)
        Tool.Set_Output('TSAVI', TSAVI)
        Tool.Set_Output('ATSAVI', ATSAVI)
        Tool.Set_Option('INTERCEPT', INTERCEPT)
        Tool.Set_Option('SLOPE', SLOPE)
        return Tool.Execute(Verbose)
    return False

def run_tool_imagery_tools_0(RED=None, NIR=None, PVI0=None, PVI1=None, PVI2=None, PVI3=None, TSAVI=None, ATSAVI=None, INTERCEPT=None, SLOPE=None, Verbose=2):
    '''
    Vegetation Index (Distance Based)
    ----------
    [imagery_tools.0]\n
    Distance based vegetation indices.\n
    Arguments
    ----------
    - RED [`input grid`] : Red Reflectance
    - NIR [`input grid`] : Near Infrared Reflectance
    - PVI0 [`output grid`] : Perpendicular Vegetation Index (Richardson and Wiegand, 1977)
    - PVI1 [`output grid`] : Perpendicular Vegetation Index (Perry and Lautenschlager, 1984)
    - PVI2 [`output grid`] : Perpendicular Vegetation Index (Walther and Shabaani)
    - PVI3 [`output grid`] : Perpendicular Vegetation Index (Qi, et al., 1994)
    - TSAVI [`output grid`] : Transformed Soil Adjusted Vegetation Index (Baret et al. 1989)
    - ATSAVI [`output grid`] : Transformed Soil Adjusted Vegetation Index (Baret and Guyot, 1991)
    - INTERCEPT [`floating point number`] : Intercept of Soil Line. Default: 0.000000
    - SLOPE [`floating point number`] : Slope of Soil Line. Default: 0.500000

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('imagery_tools', '0', 'Vegetation Index (Distance Based)')
    if Tool.is_Okay():
        Tool.Set_Input ('RED', RED)
        Tool.Set_Input ('NIR', NIR)
        Tool.Set_Output('PVI0', PVI0)
        Tool.Set_Output('PVI1', PVI1)
        Tool.Set_Output('PVI2', PVI2)
        Tool.Set_Output('PVI3', PVI3)
        Tool.Set_Output('TSAVI', TSAVI)
        Tool.Set_Output('ATSAVI', ATSAVI)
        Tool.Set_Option('INTERCEPT', INTERCEPT)
        Tool.Set_Option('SLOPE', SLOPE)
        return Tool.Execute(Verbose)
    return False

def Vegetation_Index_Slope_Based(RED=None, NIR=None, DVI=None, NDVI=None, RVI=None, NRVI=None, TVI=None, CTVI=None, TTVI=None, SAVI=None, SOIL=None, Verbose=2):
    '''
    Vegetation Index (Slope Based)
    ----------
    [imagery_tools.1]\n
    Slope based vegetation indices.\n
    (-) Difference Vegetation Index\n
    DVI = NIR - R\n
    (-) Normalized Difference Vegetation Index (Rouse et al. 1974)\n
    NDVI = (NIR - R) / (NIR + R)\n
    (-) Ratio Vegetation Index (Richardson and Wiegand, 1977)\n
    RVI = R / NIR\n
    (-) Normalized Ratio Vegetation Index (Baret and Guyot, 1991)\n
    NRVI = (RVI - 1) / (RVI + 1)\n
    (-) Transformed Vegetation Index (Deering et al., 1975)\n
    TVI = [(NIR - R) / (NIR + R) + 0.5]^0.5\n
    (-) Corrected Transformed Ratio Vegetation Index (Perry and Lautenschlager, 1984)\n
    CTVI = [(NDVI + 0.5) / abs(NDVI + 0.5)] * [abs(NDVI + 0.5)]^0.5\n
    (-) Thiam's Transformed Vegetation Index (Thiam, 1997)\n
    RVI = [abs(NDVI) + 0.5]^0.5\n
    (-) Soil Adjusted Vegetation Index (Huete, 1988)\n
    SAVI = [(NIR - R) / (NIR + R + L)] * (1 + L)\n
    (NIR = near infrared, R = red, S = soil adjustment factor)\n
    Arguments
    ----------
    - RED [`input grid`] : Red Reflectance
    - NIR [`input grid`] : Near Infrared Reflectance
    - DVI [`output grid`] : Difference Vegetation Index
    - NDVI [`output grid`] : Normalized Difference Vegetation Index
    - RVI [`output grid`] : Ratio Vegetation Index
    - NRVI [`output grid`] : Normalized Ratio Vegetation Index
    - TVI [`output grid`] : Transformed Vegetation Index
    - CTVI [`output grid`] : Corrected Transformed Vegetation Index
    - TTVI [`output grid`] : Thiam's Transformed Vegetation Index
    - SAVI [`output grid`] : Soil Adjusted Vegetation Index
    - SOIL [`floating point number`] : Soil Adjustment Factor. Minimum: 0.000000 Maximum: 1.000000 Default: 0.500000 For SAVI, suggested values (after Silleos et al. 2006): 1.0 = very low, 0.5 = intermediate, 0.25 = high density vegetation.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('imagery_tools', '1', 'Vegetation Index (Slope Based)')
    if Tool.is_Okay():
        Tool.Set_Input ('RED', RED)
        Tool.Set_Input ('NIR', NIR)
        Tool.Set_Output('DVI', DVI)
        Tool.Set_Output('NDVI', NDVI)
        Tool.Set_Output('RVI', RVI)
        Tool.Set_Output('NRVI', NRVI)
        Tool.Set_Output('TVI', TVI)
        Tool.Set_Output('CTVI', CTVI)
        Tool.Set_Output('TTVI', TTVI)
        Tool.Set_Output('SAVI', SAVI)
        Tool.Set_Option('SOIL', SOIL)
        return Tool.Execute(Verbose)
    return False

def run_tool_imagery_tools_1(RED=None, NIR=None, DVI=None, NDVI=None, RVI=None, NRVI=None, TVI=None, CTVI=None, TTVI=None, SAVI=None, SOIL=None, Verbose=2):
    '''
    Vegetation Index (Slope Based)
    ----------
    [imagery_tools.1]\n
    Slope based vegetation indices.\n
    (-) Difference Vegetation Index\n
    DVI = NIR - R\n
    (-) Normalized Difference Vegetation Index (Rouse et al. 1974)\n
    NDVI = (NIR - R) / (NIR + R)\n
    (-) Ratio Vegetation Index (Richardson and Wiegand, 1977)\n
    RVI = R / NIR\n
    (-) Normalized Ratio Vegetation Index (Baret and Guyot, 1991)\n
    NRVI = (RVI - 1) / (RVI + 1)\n
    (-) Transformed Vegetation Index (Deering et al., 1975)\n
    TVI = [(NIR - R) / (NIR + R) + 0.5]^0.5\n
    (-) Corrected Transformed Ratio Vegetation Index (Perry and Lautenschlager, 1984)\n
    CTVI = [(NDVI + 0.5) / abs(NDVI + 0.5)] * [abs(NDVI + 0.5)]^0.5\n
    (-) Thiam's Transformed Vegetation Index (Thiam, 1997)\n
    RVI = [abs(NDVI) + 0.5]^0.5\n
    (-) Soil Adjusted Vegetation Index (Huete, 1988)\n
    SAVI = [(NIR - R) / (NIR + R + L)] * (1 + L)\n
    (NIR = near infrared, R = red, S = soil adjustment factor)\n
    Arguments
    ----------
    - RED [`input grid`] : Red Reflectance
    - NIR [`input grid`] : Near Infrared Reflectance
    - DVI [`output grid`] : Difference Vegetation Index
    - NDVI [`output grid`] : Normalized Difference Vegetation Index
    - RVI [`output grid`] : Ratio Vegetation Index
    - NRVI [`output grid`] : Normalized Ratio Vegetation Index
    - TVI [`output grid`] : Transformed Vegetation Index
    - CTVI [`output grid`] : Corrected Transformed Vegetation Index
    - TTVI [`output grid`] : Thiam's Transformed Vegetation Index
    - SAVI [`output grid`] : Soil Adjusted Vegetation Index
    - SOIL [`floating point number`] : Soil Adjustment Factor. Minimum: 0.000000 Maximum: 1.000000 Default: 0.500000 For SAVI, suggested values (after Silleos et al. 2006): 1.0 = very low, 0.5 = intermediate, 0.25 = high density vegetation.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('imagery_tools', '1', 'Vegetation Index (Slope Based)')
    if Tool.is_Okay():
        Tool.Set_Input ('RED', RED)
        Tool.Set_Input ('NIR', NIR)
        Tool.Set_Output('DVI', DVI)
        Tool.Set_Output('NDVI', NDVI)
        Tool.Set_Output('RVI', RVI)
        Tool.Set_Output('NRVI', NRVI)
        Tool.Set_Output('TVI', TVI)
        Tool.Set_Output('CTVI', CTVI)
        Tool.Set_Output('TTVI', TTVI)
        Tool.Set_Output('SAVI', SAVI)
        Tool.Set_Option('SOIL', SOIL)
        return Tool.Execute(Verbose)
    return False

def Enhanced_Vegetation_Index(RED=None, NIR=None, BLUE=None, EVI=None, GAIN=None, L=None, CBLUE=None, CRED=None, Verbose=2):
    '''
    Enhanced Vegetation Index
    ----------
    [imagery_tools.2]\n
    Enhanced Vegetation Index (EVI).\n
    Arguments
    ----------
    - RED [`input grid`] : Red Reflectance
    - NIR [`input grid`] : Near Infrared Reflectance
    - BLUE [`optional input grid`] : Blue Reflectance
    - EVI [`output grid`] : Enhanced Vegetation Index
    - GAIN [`floating point number`] : Gain. Minimum: 0.000000 Default: 2.500000
    - L [`floating point number`] : Canopy Background Adjustment. Minimum: 0.000000 Default: 1.000000
    - CBLUE [`floating point number`] : Aerosol Resistance Coefficient (Blue). Minimum: 0.000000 Default: 7.500000
    - CRED [`floating point number`] : Aerosol Resistance Coefficient (Red). Minimum: 0.000000 Default: 6.000000

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('imagery_tools', '2', 'Enhanced Vegetation Index')
    if Tool.is_Okay():
        Tool.Set_Input ('RED', RED)
        Tool.Set_Input ('NIR', NIR)
        Tool.Set_Input ('BLUE', BLUE)
        Tool.Set_Output('EVI', EVI)
        Tool.Set_Option('GAIN', GAIN)
        Tool.Set_Option('L', L)
        Tool.Set_Option('CBLUE', CBLUE)
        Tool.Set_Option('CRED', CRED)
        return Tool.Execute(Verbose)
    return False

def run_tool_imagery_tools_2(RED=None, NIR=None, BLUE=None, EVI=None, GAIN=None, L=None, CBLUE=None, CRED=None, Verbose=2):
    '''
    Enhanced Vegetation Index
    ----------
    [imagery_tools.2]\n
    Enhanced Vegetation Index (EVI).\n
    Arguments
    ----------
    - RED [`input grid`] : Red Reflectance
    - NIR [`input grid`] : Near Infrared Reflectance
    - BLUE [`optional input grid`] : Blue Reflectance
    - EVI [`output grid`] : Enhanced Vegetation Index
    - GAIN [`floating point number`] : Gain. Minimum: 0.000000 Default: 2.500000
    - L [`floating point number`] : Canopy Background Adjustment. Minimum: 0.000000 Default: 1.000000
    - CBLUE [`floating point number`] : Aerosol Resistance Coefficient (Blue). Minimum: 0.000000 Default: 7.500000
    - CRED [`floating point number`] : Aerosol Resistance Coefficient (Red). Minimum: 0.000000 Default: 6.000000

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('imagery_tools', '2', 'Enhanced Vegetation Index')
    if Tool.is_Okay():
        Tool.Set_Input ('RED', RED)
        Tool.Set_Input ('NIR', NIR)
        Tool.Set_Input ('BLUE', BLUE)
        Tool.Set_Output('EVI', EVI)
        Tool.Set_Option('GAIN', GAIN)
        Tool.Set_Option('L', L)
        Tool.Set_Option('CBLUE', CBLUE)
        Tool.Set_Option('CRED', CRED)
        return Tool.Execute(Verbose)
    return False

def Tasseled_Cap_Transformation(BLUE=None, GREEN=None, RED=None, NIR=None, MIR1=None, MIR2=None, BRIGHTNESS=None, GREENNESS=None, WETNESS=None, Verbose=2):
    '''
    Tasseled Cap Transformation
    ----------
    [imagery_tools.3]\n
    Tasseled Cap Transformation as proposed for Landsat Thematic Mapper.\n
    Arguments
    ----------
    - BLUE [`input grid`] : Blue (TM 1)
    - GREEN [`input grid`] : Green (TM 2)
    - RED [`input grid`] : Red (TM 3)
    - NIR [`input grid`] : Near Infrared (TM 4)
    - MIR1 [`input grid`] : Mid Infrared (TM 5)
    - MIR2 [`input grid`] : Mid Infrared (TM 7)
    - BRIGHTNESS [`output grid`] : Brightness
    - GREENNESS [`output grid`] : Greenness
    - WETNESS [`output grid`] : Wetness

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('imagery_tools', '3', 'Tasseled Cap Transformation')
    if Tool.is_Okay():
        Tool.Set_Input ('BLUE', BLUE)
        Tool.Set_Input ('GREEN', GREEN)
        Tool.Set_Input ('RED', RED)
        Tool.Set_Input ('NIR', NIR)
        Tool.Set_Input ('MIR1', MIR1)
        Tool.Set_Input ('MIR2', MIR2)
        Tool.Set_Output('BRIGHTNESS', BRIGHTNESS)
        Tool.Set_Output('GREENNESS', GREENNESS)
        Tool.Set_Output('WETNESS', WETNESS)
        return Tool.Execute(Verbose)
    return False

def run_tool_imagery_tools_3(BLUE=None, GREEN=None, RED=None, NIR=None, MIR1=None, MIR2=None, BRIGHTNESS=None, GREENNESS=None, WETNESS=None, Verbose=2):
    '''
    Tasseled Cap Transformation
    ----------
    [imagery_tools.3]\n
    Tasseled Cap Transformation as proposed for Landsat Thematic Mapper.\n
    Arguments
    ----------
    - BLUE [`input grid`] : Blue (TM 1)
    - GREEN [`input grid`] : Green (TM 2)
    - RED [`input grid`] : Red (TM 3)
    - NIR [`input grid`] : Near Infrared (TM 4)
    - MIR1 [`input grid`] : Mid Infrared (TM 5)
    - MIR2 [`input grid`] : Mid Infrared (TM 7)
    - BRIGHTNESS [`output grid`] : Brightness
    - GREENNESS [`output grid`] : Greenness
    - WETNESS [`output grid`] : Wetness

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('imagery_tools', '3', 'Tasseled Cap Transformation')
    if Tool.is_Okay():
        Tool.Set_Input ('BLUE', BLUE)
        Tool.Set_Input ('GREEN', GREEN)
        Tool.Set_Input ('RED', RED)
        Tool.Set_Input ('NIR', NIR)
        Tool.Set_Input ('MIR1', MIR1)
        Tool.Set_Input ('MIR2', MIR2)
        Tool.Set_Output('BRIGHTNESS', BRIGHTNESS)
        Tool.Set_Output('GREENNESS', GREENNESS)
        Tool.Set_Output('WETNESS', WETNESS)
        return Tool.Execute(Verbose)
    return False

def IHS_Sharpening(R=None, G=None, B=None, PAN=None, R_SHARP=None, G_SHARP=None, B_SHARP=None, SHARP=None, PAN_SYSTEM=None, OUTPUT=None, RESAMPLING=None, PAN_MATCH=None, Verbose=2):
    '''
    IHS Sharpening
    ----------
    [imagery_tools.4]\n
    Intensity, hue, saturation (IHS) sharpening.\n
    Arguments
    ----------
    - R [`input grid`] : Red
    - G [`input grid`] : Green
    - B [`input grid`] : Blue
    - PAN [`input grid`] : Panchromatic Channel
    - R_SHARP [`output grid`] : Red
    - G_SHARP [`output grid`] : Green
    - B_SHARP [`output grid`] : Blue
    - SHARP [`output grid collection`] : Sharpened
    - PAN_SYSTEM [`grid system`] : High Resolution Grid System
    - OUTPUT [`choice`] : Output. Available Choices: [0] single grids [1] grid collection Default: 1
    - RESAMPLING [`choice`] : Resampling. Available Choices: [0] nearest neighbour [1] bilinear [2] cubic convolution Default: 2
    - PAN_MATCH [`choice`] : Panchromatic Channel Matching. Available Choices: [0] normalized [1] standardized Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('imagery_tools', '4', 'IHS Sharpening')
    if Tool.is_Okay():
        Tool.Set_Input ('R', R)
        Tool.Set_Input ('G', G)
        Tool.Set_Input ('B', B)
        Tool.Set_Input ('PAN', PAN)
        Tool.Set_Output('R_SHARP', R_SHARP)
        Tool.Set_Output('G_SHARP', G_SHARP)
        Tool.Set_Output('B_SHARP', B_SHARP)
        Tool.Set_Output('SHARP', SHARP)
        Tool.Set_Option('PAN_SYSTEM', PAN_SYSTEM)
        Tool.Set_Option('OUTPUT', OUTPUT)
        Tool.Set_Option('RESAMPLING', RESAMPLING)
        Tool.Set_Option('PAN_MATCH', PAN_MATCH)
        return Tool.Execute(Verbose)
    return False

def run_tool_imagery_tools_4(R=None, G=None, B=None, PAN=None, R_SHARP=None, G_SHARP=None, B_SHARP=None, SHARP=None, PAN_SYSTEM=None, OUTPUT=None, RESAMPLING=None, PAN_MATCH=None, Verbose=2):
    '''
    IHS Sharpening
    ----------
    [imagery_tools.4]\n
    Intensity, hue, saturation (IHS) sharpening.\n
    Arguments
    ----------
    - R [`input grid`] : Red
    - G [`input grid`] : Green
    - B [`input grid`] : Blue
    - PAN [`input grid`] : Panchromatic Channel
    - R_SHARP [`output grid`] : Red
    - G_SHARP [`output grid`] : Green
    - B_SHARP [`output grid`] : Blue
    - SHARP [`output grid collection`] : Sharpened
    - PAN_SYSTEM [`grid system`] : High Resolution Grid System
    - OUTPUT [`choice`] : Output. Available Choices: [0] single grids [1] grid collection Default: 1
    - RESAMPLING [`choice`] : Resampling. Available Choices: [0] nearest neighbour [1] bilinear [2] cubic convolution Default: 2
    - PAN_MATCH [`choice`] : Panchromatic Channel Matching. Available Choices: [0] normalized [1] standardized Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('imagery_tools', '4', 'IHS Sharpening')
    if Tool.is_Okay():
        Tool.Set_Input ('R', R)
        Tool.Set_Input ('G', G)
        Tool.Set_Input ('B', B)
        Tool.Set_Input ('PAN', PAN)
        Tool.Set_Output('R_SHARP', R_SHARP)
        Tool.Set_Output('G_SHARP', G_SHARP)
        Tool.Set_Output('B_SHARP', B_SHARP)
        Tool.Set_Output('SHARP', SHARP)
        Tool.Set_Option('PAN_SYSTEM', PAN_SYSTEM)
        Tool.Set_Option('OUTPUT', OUTPUT)
        Tool.Set_Option('RESAMPLING', RESAMPLING)
        Tool.Set_Option('PAN_MATCH', PAN_MATCH)
        return Tool.Execute(Verbose)
    return False

def Colour_Normalized_Brovey_Sharpening(R=None, G=None, B=None, PAN=None, R_SHARP=None, G_SHARP=None, B_SHARP=None, SHARP=None, PAN_SYSTEM=None, OUTPUT=None, RESAMPLING=None, Verbose=2):
    '''
    Colour Normalized Brovey Sharpening
    ----------
    [imagery_tools.5]\n
    Colour normalized (Brovey) sharpening.\n
    Arguments
    ----------
    - R [`input grid`] : Red
    - G [`input grid`] : Green
    - B [`input grid`] : Blue
    - PAN [`input grid`] : Panchromatic Channel
    - R_SHARP [`output grid`] : Red
    - G_SHARP [`output grid`] : Green
    - B_SHARP [`output grid`] : Blue
    - SHARP [`output grid collection`] : Sharpened
    - PAN_SYSTEM [`grid system`] : High Resolution Grid System
    - OUTPUT [`choice`] : Output. Available Choices: [0] single grids [1] grid collection Default: 1
    - RESAMPLING [`choice`] : Resampling. Available Choices: [0] nearest neighbour [1] bilinear [2] cubic convolution Default: 2

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('imagery_tools', '5', 'Colour Normalized Brovey Sharpening')
    if Tool.is_Okay():
        Tool.Set_Input ('R', R)
        Tool.Set_Input ('G', G)
        Tool.Set_Input ('B', B)
        Tool.Set_Input ('PAN', PAN)
        Tool.Set_Output('R_SHARP', R_SHARP)
        Tool.Set_Output('G_SHARP', G_SHARP)
        Tool.Set_Output('B_SHARP', B_SHARP)
        Tool.Set_Output('SHARP', SHARP)
        Tool.Set_Option('PAN_SYSTEM', PAN_SYSTEM)
        Tool.Set_Option('OUTPUT', OUTPUT)
        Tool.Set_Option('RESAMPLING', RESAMPLING)
        return Tool.Execute(Verbose)
    return False

def run_tool_imagery_tools_5(R=None, G=None, B=None, PAN=None, R_SHARP=None, G_SHARP=None, B_SHARP=None, SHARP=None, PAN_SYSTEM=None, OUTPUT=None, RESAMPLING=None, Verbose=2):
    '''
    Colour Normalized Brovey Sharpening
    ----------
    [imagery_tools.5]\n
    Colour normalized (Brovey) sharpening.\n
    Arguments
    ----------
    - R [`input grid`] : Red
    - G [`input grid`] : Green
    - B [`input grid`] : Blue
    - PAN [`input grid`] : Panchromatic Channel
    - R_SHARP [`output grid`] : Red
    - G_SHARP [`output grid`] : Green
    - B_SHARP [`output grid`] : Blue
    - SHARP [`output grid collection`] : Sharpened
    - PAN_SYSTEM [`grid system`] : High Resolution Grid System
    - OUTPUT [`choice`] : Output. Available Choices: [0] single grids [1] grid collection Default: 1
    - RESAMPLING [`choice`] : Resampling. Available Choices: [0] nearest neighbour [1] bilinear [2] cubic convolution Default: 2

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('imagery_tools', '5', 'Colour Normalized Brovey Sharpening')
    if Tool.is_Okay():
        Tool.Set_Input ('R', R)
        Tool.Set_Input ('G', G)
        Tool.Set_Input ('B', B)
        Tool.Set_Input ('PAN', PAN)
        Tool.Set_Output('R_SHARP', R_SHARP)
        Tool.Set_Output('G_SHARP', G_SHARP)
        Tool.Set_Output('B_SHARP', B_SHARP)
        Tool.Set_Output('SHARP', SHARP)
        Tool.Set_Option('PAN_SYSTEM', PAN_SYSTEM)
        Tool.Set_Option('OUTPUT', OUTPUT)
        Tool.Set_Option('RESAMPLING', RESAMPLING)
        return Tool.Execute(Verbose)
    return False

def Colour_Normalized_Spectral_Sharpening(GRIDS=None, PAN=None, SINGLES=None, COLLECTION=None, LO_RES=None, OUTPUT=None, RESAMPLING=None, Verbose=2):
    '''
    Colour Normalized Spectral Sharpening
    ----------
    [imagery_tools.6]\n
    Colour normalized spectral sharpening.\n
    Arguments
    ----------
    - GRIDS [`input grid list`] : Original Channels
    - PAN [`input grid`] : Panchromatic Channel
    - SINGLES [`output grid list`] : Sharpened Channels
    - COLLECTION [`output grid collection`] : Sharpened Channels
    - LO_RES [`grid system`] : Low Resolution Grid System
    - OUTPUT [`choice`] : Output. Available Choices: [0] single grids [1] grid collection Default: 1
    - RESAMPLING [`choice`] : Resampling. Available Choices: [0] nearest neighbour [1] bilinear [2] cubic convolution Default: 2

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('imagery_tools', '6', 'Colour Normalized Spectral Sharpening')
    if Tool.is_Okay():
        Tool.Set_Input ('GRIDS', GRIDS)
        Tool.Set_Input ('PAN', PAN)
        Tool.Set_Output('SINGLES', SINGLES)
        Tool.Set_Output('COLLECTION', COLLECTION)
        Tool.Set_Option('LO_RES', LO_RES)
        Tool.Set_Option('OUTPUT', OUTPUT)
        Tool.Set_Option('RESAMPLING', RESAMPLING)
        return Tool.Execute(Verbose)
    return False

def run_tool_imagery_tools_6(GRIDS=None, PAN=None, SINGLES=None, COLLECTION=None, LO_RES=None, OUTPUT=None, RESAMPLING=None, Verbose=2):
    '''
    Colour Normalized Spectral Sharpening
    ----------
    [imagery_tools.6]\n
    Colour normalized spectral sharpening.\n
    Arguments
    ----------
    - GRIDS [`input grid list`] : Original Channels
    - PAN [`input grid`] : Panchromatic Channel
    - SINGLES [`output grid list`] : Sharpened Channels
    - COLLECTION [`output grid collection`] : Sharpened Channels
    - LO_RES [`grid system`] : Low Resolution Grid System
    - OUTPUT [`choice`] : Output. Available Choices: [0] single grids [1] grid collection Default: 1
    - RESAMPLING [`choice`] : Resampling. Available Choices: [0] nearest neighbour [1] bilinear [2] cubic convolution Default: 2

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('imagery_tools', '6', 'Colour Normalized Spectral Sharpening')
    if Tool.is_Okay():
        Tool.Set_Input ('GRIDS', GRIDS)
        Tool.Set_Input ('PAN', PAN)
        Tool.Set_Output('SINGLES', SINGLES)
        Tool.Set_Output('COLLECTION', COLLECTION)
        Tool.Set_Option('LO_RES', LO_RES)
        Tool.Set_Option('OUTPUT', OUTPUT)
        Tool.Set_Option('RESAMPLING', RESAMPLING)
        return Tool.Execute(Verbose)
    return False

def Principal_Component_Based_Image_Sharpening(GRIDS=None, PAN=None, SINGLES=None, COLLECTION=None, LO_RES=None, OUTPUT=None, METHOD=None, RESAMPLING=None, PAN_MATCH=None, OVERWRITE=None, Verbose=2):
    '''
    Principal Component Based Image Sharpening
    ----------
    [imagery_tools.7]\n
    Principal component based image sharpening.\n
    Arguments
    ----------
    - GRIDS [`input grid list`] : Original Channels
    - PAN [`input grid`] : Panchromatic Channel
    - SINGLES [`output grid list`] : Sharpened Channels
    - COLLECTION [`output grid collection`] : Sharpened Channels
    - LO_RES [`grid system`] : Low Resolution
    - OUTPUT [`choice`] : Output. Available Choices: [0] single grids [1] grid collection Default: 1
    - METHOD [`choice`] : Method. Available Choices: [0] correlation matrix [1] variance-covariance matrix [2] sums-of-squares-and-cross-products matrix Default: 2
    - RESAMPLING [`choice`] : Resampling. Available Choices: [0] nearest neighbour [1] bilinear [2] cubic convolution Default: 2
    - PAN_MATCH [`choice`] : Panchromatic Channel Matching. Available Choices: [0] normalized [1] standardized Default: 1
    - OVERWRITE [`boolean`] : Overwrite. Default: 1 overwrite previous output if adequate

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('imagery_tools', '7', 'Principal Component Based Image Sharpening')
    if Tool.is_Okay():
        Tool.Set_Input ('GRIDS', GRIDS)
        Tool.Set_Input ('PAN', PAN)
        Tool.Set_Output('SINGLES', SINGLES)
        Tool.Set_Output('COLLECTION', COLLECTION)
        Tool.Set_Option('LO_RES', LO_RES)
        Tool.Set_Option('OUTPUT', OUTPUT)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('RESAMPLING', RESAMPLING)
        Tool.Set_Option('PAN_MATCH', PAN_MATCH)
        Tool.Set_Option('OVERWRITE', OVERWRITE)
        return Tool.Execute(Verbose)
    return False

def run_tool_imagery_tools_7(GRIDS=None, PAN=None, SINGLES=None, COLLECTION=None, LO_RES=None, OUTPUT=None, METHOD=None, RESAMPLING=None, PAN_MATCH=None, OVERWRITE=None, Verbose=2):
    '''
    Principal Component Based Image Sharpening
    ----------
    [imagery_tools.7]\n
    Principal component based image sharpening.\n
    Arguments
    ----------
    - GRIDS [`input grid list`] : Original Channels
    - PAN [`input grid`] : Panchromatic Channel
    - SINGLES [`output grid list`] : Sharpened Channels
    - COLLECTION [`output grid collection`] : Sharpened Channels
    - LO_RES [`grid system`] : Low Resolution
    - OUTPUT [`choice`] : Output. Available Choices: [0] single grids [1] grid collection Default: 1
    - METHOD [`choice`] : Method. Available Choices: [0] correlation matrix [1] variance-covariance matrix [2] sums-of-squares-and-cross-products matrix Default: 2
    - RESAMPLING [`choice`] : Resampling. Available Choices: [0] nearest neighbour [1] bilinear [2] cubic convolution Default: 2
    - PAN_MATCH [`choice`] : Panchromatic Channel Matching. Available Choices: [0] normalized [1] standardized Default: 1
    - OVERWRITE [`boolean`] : Overwrite. Default: 1 overwrite previous output if adequate

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('imagery_tools', '7', 'Principal Component Based Image Sharpening')
    if Tool.is_Okay():
        Tool.Set_Input ('GRIDS', GRIDS)
        Tool.Set_Input ('PAN', PAN)
        Tool.Set_Output('SINGLES', SINGLES)
        Tool.Set_Output('COLLECTION', COLLECTION)
        Tool.Set_Option('LO_RES', LO_RES)
        Tool.Set_Option('OUTPUT', OUTPUT)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('RESAMPLING', RESAMPLING)
        Tool.Set_Option('PAN_MATCH', PAN_MATCH)
        Tool.Set_Option('OVERWRITE', OVERWRITE)
        return Tool.Execute(Verbose)
    return False

def Top_of_Atmosphere_Reflectance(MSS01=None, MSS02=None, MSS03=None, MSS04=None, TM01=None, TM02=None, TM03=None, TM04=None, TM05=None, TM07=None, ETM01=None, ETM02=None, ETM03=None, ETM04=None, ETM05=None, ETM07=None, OLI01=None, OLI02=None, OLI03=None, OLI04=None, OLI05=None, OLI06=None, OLI07=None, OLI09=None, TM_T06=None, ETM_T61=None, ETM_T62=None, TIRS10=None, TIRS11=None, PAN08=None, SPECTRAL=None, THERMAL=None, PANBAND=None, MSS=None, TM=None, ETM=None, OLI=None, TM_T=None, ETM_T=None, TIRS=None, PAN=None, METAFILE=None, SENSOR=None, DATE_ACQU=None, DATE_PROD=None, SUN_HGT=None, GRIDS_OUT=None, GRIDS_NAME=None, AS_RAD=None, AC_METHOD=None, AC_DO_CELLS=None, AC_RAYLEIGH=None, AC_SUN_RAD=None, ETM_GAIN_10=None, ETM_GAIN_20=None, ETM_GAIN_30=None, ETM_GAIN_40=None, ETM_GAIN_50=None, ETM_GAIN_61=None, ETM_GAIN_62=None, ETM_GAIN_70=None, ETM_GAIN_80=None, Verbose=2):
    '''
    Top of Atmosphere Reflectance
    ----------
    [imagery_tools.8]\n
    Calculation of top-of-atmosphere radiance or reflectance and temperature (TOAR) for Landsat MSS/TM/ETM+. This tool incorporates E.J. Tizado's GRASS GIS implementation (i.landsat.toar).\n
    Arguments
    ----------
    - MSS01 [`optional input grid`] : Band 1
    - MSS02 [`optional input grid`] : Band 2
    - MSS03 [`optional input grid`] : Band 3
    - MSS04 [`optional input grid`] : Band 4
    - TM01 [`optional input grid`] : Band 1
    - TM02 [`optional input grid`] : Band 2
    - TM03 [`optional input grid`] : Band 3
    - TM04 [`optional input grid`] : Band 4
    - TM05 [`optional input grid`] : Band 5
    - TM07 [`optional input grid`] : Band 7
    - ETM01 [`optional input grid`] : Band 1
    - ETM02 [`optional input grid`] : Band 2
    - ETM03 [`optional input grid`] : Band 3
    - ETM04 [`optional input grid`] : Band 4
    - ETM05 [`optional input grid`] : Band 5
    - ETM07 [`optional input grid`] : Band 7
    - OLI01 [`optional input grid`] : Band 1
    - OLI02 [`optional input grid`] : Band 2
    - OLI03 [`optional input grid`] : Band 3
    - OLI04 [`optional input grid`] : Band 4
    - OLI05 [`optional input grid`] : Band 5
    - OLI06 [`optional input grid`] : Band 6
    - OLI07 [`optional input grid`] : Band 7
    - OLI09 [`optional input grid`] : Band 9
    - TM_T06 [`optional input grid`] : Band 6
    - ETM_T61 [`optional input grid`] : Band 61
    - ETM_T62 [`optional input grid`] : Band 62
    - TIRS10 [`optional input grid`] : Band 10
    - TIRS11 [`optional input grid`] : Band 11
    - PAN08 [`optional input grid`] : Band 8
    - SPECTRAL [`output grid list`] : Spectral
    - THERMAL [`output grid list`] : Thermal
    - PANBAND [`output grid list`] : Panchromatic
    - MSS [`grid system`] : Spectral
    - TM [`grid system`] : Spectral
    - ETM [`grid system`] : Spectral
    - OLI [`grid system`] : Spectral
    - TM_T [`grid system`] : Thermal
    - ETM_T [`grid system`] : Thermal
    - TIRS [`grid system`] : Thermal
    - PAN [`grid system`] : Panchromatic
    - METAFILE [`file path`] : Metadata File. Name of Landsat metadata file (.met or MTL.txt)
    - SENSOR [`choice`] : Spacecraft Sensor. Available Choices: [0] Landsat-1 MSS [1] Landsat-2 MSS [2] Landsat-3 MSS [3] Landsat-4 MSS [4] Landsat-5 MSS [5] Landsat-4 TM [6] Landsat-5 TM [7] Landsat-7 ETM+ [8] Landsat-8 OLI/TIRS Default: 7
    - DATE_ACQU [`text`] : Image Acquisition Date. Default: 2001-01-01 Image acquisition date (yyyy-mm-dd)
    - DATE_PROD [`text`] : Image Creation Date. Default: 2001-01-01 Image creation date (yyyy-mm-dd)
    - SUN_HGT [`floating point number`] : Suns's Height. Minimum: 0.000000 Maximum: 90.000000 Default: 45.000000 Sun's height above horizon in degree
    - GRIDS_OUT [`boolean`] : Spectral Output as Grid Collection. Default: 1
    - GRIDS_NAME [`text`] : Name. Default: Landsat Calibrated
    - AS_RAD [`boolean`] : At-Sensor Radiance. Default: 0 Output at-sensor radiance for all bands
    - AC_METHOD [`choice`] : Atmospheric Correction. Available Choices: [0] uncorrected [1] corrected [2] dark object subtraction 1 [3] dark object subtraction 2 [4] dark object subtraction 2b [5] dark object subtraction 3 [6] dark object subtraction 4 Default: 0
    - AC_DO_CELLS [`integer number`] : Minimum Number of Dark Object Cells. Minimum: 0 Default: 1000 Minimum pixels to consider digital number as dark object
    - AC_RAYLEIGH [`floating point number`] : Rayleigh Scattering. Minimum: 0.000000 Default: 0.000000 Rayleigh atmosphere (diffuse sky irradiance)
    - AC_SUN_RAD [`floating point number`] : Solar Radiance. Minimum: 0.000000 Maximum: 100.000000 Default: 1.000000 Percent of solar radiance in path radiance
    - ETM_GAIN_10 [`choice`] : Band 1. Available Choices: [0] low [1] high Default: 1
    - ETM_GAIN_20 [`choice`] : Band 2. Available Choices: [0] low [1] high Default: 1
    - ETM_GAIN_30 [`choice`] : Band 3. Available Choices: [0] low [1] high Default: 1
    - ETM_GAIN_40 [`choice`] : Band 4. Available Choices: [0] low [1] high Default: 1
    - ETM_GAIN_50 [`choice`] : Band 5. Available Choices: [0] low [1] high Default: 1
    - ETM_GAIN_61 [`choice`] : Band 61. Available Choices: [0] low [1] high Default: 0
    - ETM_GAIN_62 [`choice`] : Band 62. Available Choices: [0] low [1] high Default: 1
    - ETM_GAIN_70 [`choice`] : Band 7. Available Choices: [0] low [1] high Default: 1
    - ETM_GAIN_80 [`choice`] : Band 8. Available Choices: [0] low [1] high Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('imagery_tools', '8', 'Top of Atmosphere Reflectance')
    if Tool.is_Okay():
        Tool.Set_Input ('MSS01', MSS01)
        Tool.Set_Input ('MSS02', MSS02)
        Tool.Set_Input ('MSS03', MSS03)
        Tool.Set_Input ('MSS04', MSS04)
        Tool.Set_Input ('TM01', TM01)
        Tool.Set_Input ('TM02', TM02)
        Tool.Set_Input ('TM03', TM03)
        Tool.Set_Input ('TM04', TM04)
        Tool.Set_Input ('TM05', TM05)
        Tool.Set_Input ('TM07', TM07)
        Tool.Set_Input ('ETM01', ETM01)
        Tool.Set_Input ('ETM02', ETM02)
        Tool.Set_Input ('ETM03', ETM03)
        Tool.Set_Input ('ETM04', ETM04)
        Tool.Set_Input ('ETM05', ETM05)
        Tool.Set_Input ('ETM07', ETM07)
        Tool.Set_Input ('OLI01', OLI01)
        Tool.Set_Input ('OLI02', OLI02)
        Tool.Set_Input ('OLI03', OLI03)
        Tool.Set_Input ('OLI04', OLI04)
        Tool.Set_Input ('OLI05', OLI05)
        Tool.Set_Input ('OLI06', OLI06)
        Tool.Set_Input ('OLI07', OLI07)
        Tool.Set_Input ('OLI09', OLI09)
        Tool.Set_Input ('TM_T06', TM_T06)
        Tool.Set_Input ('ETM_T61', ETM_T61)
        Tool.Set_Input ('ETM_T62', ETM_T62)
        Tool.Set_Input ('TIRS10', TIRS10)
        Tool.Set_Input ('TIRS11', TIRS11)
        Tool.Set_Input ('PAN08', PAN08)
        Tool.Set_Output('SPECTRAL', SPECTRAL)
        Tool.Set_Output('THERMAL', THERMAL)
        Tool.Set_Output('PANBAND', PANBAND)
        Tool.Set_Option('MSS', MSS)
        Tool.Set_Option('TM', TM)
        Tool.Set_Option('ETM', ETM)
        Tool.Set_Option('OLI', OLI)
        Tool.Set_Option('TM_T', TM_T)
        Tool.Set_Option('ETM_T', ETM_T)
        Tool.Set_Option('TIRS', TIRS)
        Tool.Set_Option('PAN', PAN)
        Tool.Set_Option('METAFILE', METAFILE)
        Tool.Set_Option('SENSOR', SENSOR)
        Tool.Set_Option('DATE_ACQU', DATE_ACQU)
        Tool.Set_Option('DATE_PROD', DATE_PROD)
        Tool.Set_Option('SUN_HGT', SUN_HGT)
        Tool.Set_Option('GRIDS_OUT', GRIDS_OUT)
        Tool.Set_Option('GRIDS_NAME', GRIDS_NAME)
        Tool.Set_Option('AS_RAD', AS_RAD)
        Tool.Set_Option('AC_METHOD', AC_METHOD)
        Tool.Set_Option('AC_DO_CELLS', AC_DO_CELLS)
        Tool.Set_Option('AC_RAYLEIGH', AC_RAYLEIGH)
        Tool.Set_Option('AC_SUN_RAD', AC_SUN_RAD)
        Tool.Set_Option('ETM_GAIN_10', ETM_GAIN_10)
        Tool.Set_Option('ETM_GAIN_20', ETM_GAIN_20)
        Tool.Set_Option('ETM_GAIN_30', ETM_GAIN_30)
        Tool.Set_Option('ETM_GAIN_40', ETM_GAIN_40)
        Tool.Set_Option('ETM_GAIN_50', ETM_GAIN_50)
        Tool.Set_Option('ETM_GAIN_61', ETM_GAIN_61)
        Tool.Set_Option('ETM_GAIN_62', ETM_GAIN_62)
        Tool.Set_Option('ETM_GAIN_70', ETM_GAIN_70)
        Tool.Set_Option('ETM_GAIN_80', ETM_GAIN_80)
        return Tool.Execute(Verbose)
    return False

def run_tool_imagery_tools_8(MSS01=None, MSS02=None, MSS03=None, MSS04=None, TM01=None, TM02=None, TM03=None, TM04=None, TM05=None, TM07=None, ETM01=None, ETM02=None, ETM03=None, ETM04=None, ETM05=None, ETM07=None, OLI01=None, OLI02=None, OLI03=None, OLI04=None, OLI05=None, OLI06=None, OLI07=None, OLI09=None, TM_T06=None, ETM_T61=None, ETM_T62=None, TIRS10=None, TIRS11=None, PAN08=None, SPECTRAL=None, THERMAL=None, PANBAND=None, MSS=None, TM=None, ETM=None, OLI=None, TM_T=None, ETM_T=None, TIRS=None, PAN=None, METAFILE=None, SENSOR=None, DATE_ACQU=None, DATE_PROD=None, SUN_HGT=None, GRIDS_OUT=None, GRIDS_NAME=None, AS_RAD=None, AC_METHOD=None, AC_DO_CELLS=None, AC_RAYLEIGH=None, AC_SUN_RAD=None, ETM_GAIN_10=None, ETM_GAIN_20=None, ETM_GAIN_30=None, ETM_GAIN_40=None, ETM_GAIN_50=None, ETM_GAIN_61=None, ETM_GAIN_62=None, ETM_GAIN_70=None, ETM_GAIN_80=None, Verbose=2):
    '''
    Top of Atmosphere Reflectance
    ----------
    [imagery_tools.8]\n
    Calculation of top-of-atmosphere radiance or reflectance and temperature (TOAR) for Landsat MSS/TM/ETM+. This tool incorporates E.J. Tizado's GRASS GIS implementation (i.landsat.toar).\n
    Arguments
    ----------
    - MSS01 [`optional input grid`] : Band 1
    - MSS02 [`optional input grid`] : Band 2
    - MSS03 [`optional input grid`] : Band 3
    - MSS04 [`optional input grid`] : Band 4
    - TM01 [`optional input grid`] : Band 1
    - TM02 [`optional input grid`] : Band 2
    - TM03 [`optional input grid`] : Band 3
    - TM04 [`optional input grid`] : Band 4
    - TM05 [`optional input grid`] : Band 5
    - TM07 [`optional input grid`] : Band 7
    - ETM01 [`optional input grid`] : Band 1
    - ETM02 [`optional input grid`] : Band 2
    - ETM03 [`optional input grid`] : Band 3
    - ETM04 [`optional input grid`] : Band 4
    - ETM05 [`optional input grid`] : Band 5
    - ETM07 [`optional input grid`] : Band 7
    - OLI01 [`optional input grid`] : Band 1
    - OLI02 [`optional input grid`] : Band 2
    - OLI03 [`optional input grid`] : Band 3
    - OLI04 [`optional input grid`] : Band 4
    - OLI05 [`optional input grid`] : Band 5
    - OLI06 [`optional input grid`] : Band 6
    - OLI07 [`optional input grid`] : Band 7
    - OLI09 [`optional input grid`] : Band 9
    - TM_T06 [`optional input grid`] : Band 6
    - ETM_T61 [`optional input grid`] : Band 61
    - ETM_T62 [`optional input grid`] : Band 62
    - TIRS10 [`optional input grid`] : Band 10
    - TIRS11 [`optional input grid`] : Band 11
    - PAN08 [`optional input grid`] : Band 8
    - SPECTRAL [`output grid list`] : Spectral
    - THERMAL [`output grid list`] : Thermal
    - PANBAND [`output grid list`] : Panchromatic
    - MSS [`grid system`] : Spectral
    - TM [`grid system`] : Spectral
    - ETM [`grid system`] : Spectral
    - OLI [`grid system`] : Spectral
    - TM_T [`grid system`] : Thermal
    - ETM_T [`grid system`] : Thermal
    - TIRS [`grid system`] : Thermal
    - PAN [`grid system`] : Panchromatic
    - METAFILE [`file path`] : Metadata File. Name of Landsat metadata file (.met or MTL.txt)
    - SENSOR [`choice`] : Spacecraft Sensor. Available Choices: [0] Landsat-1 MSS [1] Landsat-2 MSS [2] Landsat-3 MSS [3] Landsat-4 MSS [4] Landsat-5 MSS [5] Landsat-4 TM [6] Landsat-5 TM [7] Landsat-7 ETM+ [8] Landsat-8 OLI/TIRS Default: 7
    - DATE_ACQU [`text`] : Image Acquisition Date. Default: 2001-01-01 Image acquisition date (yyyy-mm-dd)
    - DATE_PROD [`text`] : Image Creation Date. Default: 2001-01-01 Image creation date (yyyy-mm-dd)
    - SUN_HGT [`floating point number`] : Suns's Height. Minimum: 0.000000 Maximum: 90.000000 Default: 45.000000 Sun's height above horizon in degree
    - GRIDS_OUT [`boolean`] : Spectral Output as Grid Collection. Default: 1
    - GRIDS_NAME [`text`] : Name. Default: Landsat Calibrated
    - AS_RAD [`boolean`] : At-Sensor Radiance. Default: 0 Output at-sensor radiance for all bands
    - AC_METHOD [`choice`] : Atmospheric Correction. Available Choices: [0] uncorrected [1] corrected [2] dark object subtraction 1 [3] dark object subtraction 2 [4] dark object subtraction 2b [5] dark object subtraction 3 [6] dark object subtraction 4 Default: 0
    - AC_DO_CELLS [`integer number`] : Minimum Number of Dark Object Cells. Minimum: 0 Default: 1000 Minimum pixels to consider digital number as dark object
    - AC_RAYLEIGH [`floating point number`] : Rayleigh Scattering. Minimum: 0.000000 Default: 0.000000 Rayleigh atmosphere (diffuse sky irradiance)
    - AC_SUN_RAD [`floating point number`] : Solar Radiance. Minimum: 0.000000 Maximum: 100.000000 Default: 1.000000 Percent of solar radiance in path radiance
    - ETM_GAIN_10 [`choice`] : Band 1. Available Choices: [0] low [1] high Default: 1
    - ETM_GAIN_20 [`choice`] : Band 2. Available Choices: [0] low [1] high Default: 1
    - ETM_GAIN_30 [`choice`] : Band 3. Available Choices: [0] low [1] high Default: 1
    - ETM_GAIN_40 [`choice`] : Band 4. Available Choices: [0] low [1] high Default: 1
    - ETM_GAIN_50 [`choice`] : Band 5. Available Choices: [0] low [1] high Default: 1
    - ETM_GAIN_61 [`choice`] : Band 61. Available Choices: [0] low [1] high Default: 0
    - ETM_GAIN_62 [`choice`] : Band 62. Available Choices: [0] low [1] high Default: 1
    - ETM_GAIN_70 [`choice`] : Band 7. Available Choices: [0] low [1] high Default: 1
    - ETM_GAIN_80 [`choice`] : Band 8. Available Choices: [0] low [1] high Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('imagery_tools', '8', 'Top of Atmosphere Reflectance')
    if Tool.is_Okay():
        Tool.Set_Input ('MSS01', MSS01)
        Tool.Set_Input ('MSS02', MSS02)
        Tool.Set_Input ('MSS03', MSS03)
        Tool.Set_Input ('MSS04', MSS04)
        Tool.Set_Input ('TM01', TM01)
        Tool.Set_Input ('TM02', TM02)
        Tool.Set_Input ('TM03', TM03)
        Tool.Set_Input ('TM04', TM04)
        Tool.Set_Input ('TM05', TM05)
        Tool.Set_Input ('TM07', TM07)
        Tool.Set_Input ('ETM01', ETM01)
        Tool.Set_Input ('ETM02', ETM02)
        Tool.Set_Input ('ETM03', ETM03)
        Tool.Set_Input ('ETM04', ETM04)
        Tool.Set_Input ('ETM05', ETM05)
        Tool.Set_Input ('ETM07', ETM07)
        Tool.Set_Input ('OLI01', OLI01)
        Tool.Set_Input ('OLI02', OLI02)
        Tool.Set_Input ('OLI03', OLI03)
        Tool.Set_Input ('OLI04', OLI04)
        Tool.Set_Input ('OLI05', OLI05)
        Tool.Set_Input ('OLI06', OLI06)
        Tool.Set_Input ('OLI07', OLI07)
        Tool.Set_Input ('OLI09', OLI09)
        Tool.Set_Input ('TM_T06', TM_T06)
        Tool.Set_Input ('ETM_T61', ETM_T61)
        Tool.Set_Input ('ETM_T62', ETM_T62)
        Tool.Set_Input ('TIRS10', TIRS10)
        Tool.Set_Input ('TIRS11', TIRS11)
        Tool.Set_Input ('PAN08', PAN08)
        Tool.Set_Output('SPECTRAL', SPECTRAL)
        Tool.Set_Output('THERMAL', THERMAL)
        Tool.Set_Output('PANBAND', PANBAND)
        Tool.Set_Option('MSS', MSS)
        Tool.Set_Option('TM', TM)
        Tool.Set_Option('ETM', ETM)
        Tool.Set_Option('OLI', OLI)
        Tool.Set_Option('TM_T', TM_T)
        Tool.Set_Option('ETM_T', ETM_T)
        Tool.Set_Option('TIRS', TIRS)
        Tool.Set_Option('PAN', PAN)
        Tool.Set_Option('METAFILE', METAFILE)
        Tool.Set_Option('SENSOR', SENSOR)
        Tool.Set_Option('DATE_ACQU', DATE_ACQU)
        Tool.Set_Option('DATE_PROD', DATE_PROD)
        Tool.Set_Option('SUN_HGT', SUN_HGT)
        Tool.Set_Option('GRIDS_OUT', GRIDS_OUT)
        Tool.Set_Option('GRIDS_NAME', GRIDS_NAME)
        Tool.Set_Option('AS_RAD', AS_RAD)
        Tool.Set_Option('AC_METHOD', AC_METHOD)
        Tool.Set_Option('AC_DO_CELLS', AC_DO_CELLS)
        Tool.Set_Option('AC_RAYLEIGH', AC_RAYLEIGH)
        Tool.Set_Option('AC_SUN_RAD', AC_SUN_RAD)
        Tool.Set_Option('ETM_GAIN_10', ETM_GAIN_10)
        Tool.Set_Option('ETM_GAIN_20', ETM_GAIN_20)
        Tool.Set_Option('ETM_GAIN_30', ETM_GAIN_30)
        Tool.Set_Option('ETM_GAIN_40', ETM_GAIN_40)
        Tool.Set_Option('ETM_GAIN_50', ETM_GAIN_50)
        Tool.Set_Option('ETM_GAIN_61', ETM_GAIN_61)
        Tool.Set_Option('ETM_GAIN_62', ETM_GAIN_62)
        Tool.Set_Option('ETM_GAIN_70', ETM_GAIN_70)
        Tool.Set_Option('ETM_GAIN_80', ETM_GAIN_80)
        return Tool.Execute(Verbose)
    return False

def Automated_Cloud_Cover_Assessment(BAND2=None, BAND3=None, BAND4=None, BAND5=None, BAND6=None, CLOUD=None, BAND6_GRIDSYSTEM=None, B56C=None, B45R=None, HIST_N=None, CSIG=None, PASS2=None, SHADOW=None, FILTER=None, Verbose=2):
    '''
    Automated Cloud Cover Assessment
    ----------
    [imagery_tools.9]\n
    Automated Cloud-Cover Assessment (ACCA) for Landsat TM/ETM+ imagery as proposed by Irish (2000). This tool incorporates E.J. Tizado's GRASS GIS implementation (i.landsat.acca).\n
    Arguments
    ----------
    - BAND2 [`input grid`] : Green. Reflectance
    - BAND3 [`input grid`] : Red. Reflectance
    - BAND4 [`input grid`] : NIR. Reflectance
    - BAND5 [`input grid`] : SWIR. Reflectance
    - BAND6 [`input grid`] : Thermal. Kelvin
    - CLOUD [`output grid`] : Clouds
    - BAND6_GRIDSYSTEM [`grid system`] : Grid system
    - B56C [`floating point number`] : SWIR/Thermal Threshold. Default: 225.000000 Threshold for SWIR/Thermal Composite (step 6).
    - B45R [`floating point number`] : Desert Detection Threshold. Default: 1.000000 Threshold for desert detection (step 10,  NIR/SWIR Ratio).
    - HIST_N [`integer number`] : Temperature Histogram. Minimum: 10 Default: 100 Number of classes in the cloud temperature histogram.
    - CSIG [`boolean`] : Cloud Signature. Default: 1 Always use cloud signature (step 14).
    - PASS2 [`boolean`] : Cloud Differentiation. Default: 0 Differentiate between warm (not ambiguous) and cold clouds.
    - SHADOW [`boolean`] : Shadows. Default: 1 Include a category for cloud shadows.
    - FILTER [`boolean`] : Filter. Default: 1 Apply post-processing filter to remove small holes.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('imagery_tools', '9', 'Automated Cloud Cover Assessment')
    if Tool.is_Okay():
        Tool.Set_Input ('BAND2', BAND2)
        Tool.Set_Input ('BAND3', BAND3)
        Tool.Set_Input ('BAND4', BAND4)
        Tool.Set_Input ('BAND5', BAND5)
        Tool.Set_Input ('BAND6', BAND6)
        Tool.Set_Output('CLOUD', CLOUD)
        Tool.Set_Option('BAND6_GRIDSYSTEM', BAND6_GRIDSYSTEM)
        Tool.Set_Option('B56C', B56C)
        Tool.Set_Option('B45R', B45R)
        Tool.Set_Option('HIST_N', HIST_N)
        Tool.Set_Option('CSIG', CSIG)
        Tool.Set_Option('PASS2', PASS2)
        Tool.Set_Option('SHADOW', SHADOW)
        Tool.Set_Option('FILTER', FILTER)
        return Tool.Execute(Verbose)
    return False

def run_tool_imagery_tools_9(BAND2=None, BAND3=None, BAND4=None, BAND5=None, BAND6=None, CLOUD=None, BAND6_GRIDSYSTEM=None, B56C=None, B45R=None, HIST_N=None, CSIG=None, PASS2=None, SHADOW=None, FILTER=None, Verbose=2):
    '''
    Automated Cloud Cover Assessment
    ----------
    [imagery_tools.9]\n
    Automated Cloud-Cover Assessment (ACCA) for Landsat TM/ETM+ imagery as proposed by Irish (2000). This tool incorporates E.J. Tizado's GRASS GIS implementation (i.landsat.acca).\n
    Arguments
    ----------
    - BAND2 [`input grid`] : Green. Reflectance
    - BAND3 [`input grid`] : Red. Reflectance
    - BAND4 [`input grid`] : NIR. Reflectance
    - BAND5 [`input grid`] : SWIR. Reflectance
    - BAND6 [`input grid`] : Thermal. Kelvin
    - CLOUD [`output grid`] : Clouds
    - BAND6_GRIDSYSTEM [`grid system`] : Grid system
    - B56C [`floating point number`] : SWIR/Thermal Threshold. Default: 225.000000 Threshold for SWIR/Thermal Composite (step 6).
    - B45R [`floating point number`] : Desert Detection Threshold. Default: 1.000000 Threshold for desert detection (step 10,  NIR/SWIR Ratio).
    - HIST_N [`integer number`] : Temperature Histogram. Minimum: 10 Default: 100 Number of classes in the cloud temperature histogram.
    - CSIG [`boolean`] : Cloud Signature. Default: 1 Always use cloud signature (step 14).
    - PASS2 [`boolean`] : Cloud Differentiation. Default: 0 Differentiate between warm (not ambiguous) and cold clouds.
    - SHADOW [`boolean`] : Shadows. Default: 1 Include a category for cloud shadows.
    - FILTER [`boolean`] : Filter. Default: 1 Apply post-processing filter to remove small holes.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('imagery_tools', '9', 'Automated Cloud Cover Assessment')
    if Tool.is_Okay():
        Tool.Set_Input ('BAND2', BAND2)
        Tool.Set_Input ('BAND3', BAND3)
        Tool.Set_Input ('BAND4', BAND4)
        Tool.Set_Input ('BAND5', BAND5)
        Tool.Set_Input ('BAND6', BAND6)
        Tool.Set_Output('CLOUD', CLOUD)
        Tool.Set_Option('BAND6_GRIDSYSTEM', BAND6_GRIDSYSTEM)
        Tool.Set_Option('B56C', B56C)
        Tool.Set_Option('B45R', B45R)
        Tool.Set_Option('HIST_N', HIST_N)
        Tool.Set_Option('CSIG', CSIG)
        Tool.Set_Option('PASS2', PASS2)
        Tool.Set_Option('SHADOW', SHADOW)
        Tool.Set_Option('FILTER', FILTER)
        return Tool.Execute(Verbose)
    return False

def Landsat_Import_with_Options(BANDS=None, FILES=None, PROJECTION=None, RESAMPLING=None, SHOW_RGB=None, SHOW_R=None, SHOW_G=None, SHOW_B=None, Verbose=2):
    '''
    Landsat Import with Options
    ----------
    [imagery_tools.10]\n
    This tool facilitates the import and display of Landsat scenes, which have each band given as a single GeoTIFF file.\n
    The development of this tool has been requested and sponsored by Rohan Fisher, Charles Darwin University, Australia.\n
    Arguments
    ----------
    - BANDS [`output grid list`] : Bands
    - FILES [`file path`] : Files
    - PROJECTION [`choice`] : Coordinate System. Available Choices: [0] UTM North [1] UTM South [2] Geographic Coordinates Default: 0
    - RESAMPLING [`choice`] : Resampling. Available Choices: [0] Nearest Neighbour [1] Bilinear Interpolation [2] Bicubic Spline Interpolation [3] B-Spline Interpolation Default: 3
    - SHOW_RGB [`boolean`] : Show a Composite. Default: 1
    - SHOW_R [`choice`] : Red. Available Choices: [0] no choice available Default: 0
    - SHOW_G [`choice`] : Green. Available Choices: [0] no choice available Default: 0
    - SHOW_B [`choice`] : Blue. Available Choices: [0] no choice available Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('imagery_tools', '10', 'Landsat Import with Options')
    if Tool.is_Okay():
        Tool.Set_Output('BANDS', BANDS)
        Tool.Set_Option('FILES', FILES)
        Tool.Set_Option('PROJECTION', PROJECTION)
        Tool.Set_Option('RESAMPLING', RESAMPLING)
        Tool.Set_Option('SHOW_RGB', SHOW_RGB)
        Tool.Set_Option('SHOW_R', SHOW_R)
        Tool.Set_Option('SHOW_G', SHOW_G)
        Tool.Set_Option('SHOW_B', SHOW_B)
        return Tool.Execute(Verbose)
    return False

def run_tool_imagery_tools_10(BANDS=None, FILES=None, PROJECTION=None, RESAMPLING=None, SHOW_RGB=None, SHOW_R=None, SHOW_G=None, SHOW_B=None, Verbose=2):
    '''
    Landsat Import with Options
    ----------
    [imagery_tools.10]\n
    This tool facilitates the import and display of Landsat scenes, which have each band given as a single GeoTIFF file.\n
    The development of this tool has been requested and sponsored by Rohan Fisher, Charles Darwin University, Australia.\n
    Arguments
    ----------
    - BANDS [`output grid list`] : Bands
    - FILES [`file path`] : Files
    - PROJECTION [`choice`] : Coordinate System. Available Choices: [0] UTM North [1] UTM South [2] Geographic Coordinates Default: 0
    - RESAMPLING [`choice`] : Resampling. Available Choices: [0] Nearest Neighbour [1] Bilinear Interpolation [2] Bicubic Spline Interpolation [3] B-Spline Interpolation Default: 3
    - SHOW_RGB [`boolean`] : Show a Composite. Default: 1
    - SHOW_R [`choice`] : Red. Available Choices: [0] no choice available Default: 0
    - SHOW_G [`choice`] : Green. Available Choices: [0] no choice available Default: 0
    - SHOW_B [`choice`] : Blue. Available Choices: [0] no choice available Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('imagery_tools', '10', 'Landsat Import with Options')
    if Tool.is_Okay():
        Tool.Set_Output('BANDS', BANDS)
        Tool.Set_Option('FILES', FILES)
        Tool.Set_Option('PROJECTION', PROJECTION)
        Tool.Set_Option('RESAMPLING', RESAMPLING)
        Tool.Set_Option('SHOW_RGB', SHOW_RGB)
        Tool.Set_Option('SHOW_R', SHOW_R)
        Tool.Set_Option('SHOW_G', SHOW_G)
        Tool.Set_Option('SHOW_B', SHOW_B)
        return Tool.Execute(Verbose)
    return False

def Textural_Features(GRID=None, ASM=None, CONTRAST=None, CORRELATION=None, VARIANCE=None, IDM=None, SUM_AVERAGE=None, SUM_ENTROPY=None, SUM_VARIANCE=None, ENTROPY=None, DIF_VARIANCE=None, DIF_ENTROPY=None, MOC_1=None, MOC_2=None, DIRECTION=None, RADIUS=None, DISTANCE=None, MAX_CATS=None, Verbose=2):
    '''
    Textural Features
    ----------
    [imagery_tools.11]\n
    Textural features. This tool is based on the GRASS GIS implementation by Carmine Basco (r.texture).\n
    Arguments
    ----------
    - GRID [`input grid`] : Grid
    - ASM [`output grid`] : Angular Second Moment
    - CONTRAST [`output grid`] : Contrast
    - CORRELATION [`output grid`] : Correlation
    - VARIANCE [`output grid`] : Variance
    - IDM [`output grid`] : Inverse Diff Moment
    - SUM_AVERAGE [`output grid`] : Sum Average
    - SUM_ENTROPY [`output grid`] : Sum Entropy
    - SUM_VARIANCE [`output grid`] : Sum Variance
    - ENTROPY [`output grid`] : Entropy
    - DIF_VARIANCE [`output grid`] : Difference Variance
    - DIF_ENTROPY [`output grid`] : Difference Entropy
    - MOC_1 [`output grid`] : Measure of Correlation-1
    - MOC_2 [`output grid`] : Measure of Correlation-2
    - DIRECTION [`choice`] : Direction. Available Choices: [0] all [1] N-S [2] NE-SW [3] E-W [4] SE-NW Default: 0
    - RADIUS [`integer number`] : Radius. Minimum: 1 Default: 1 kernel radius in cells
    - DISTANCE [`integer number`] : Distance. Minimum: 1 Default: 1 The distance between two samples.
    - MAX_CATS [`integer number`] : Maximum Number of Categories. Minimum: 2 Default: 256

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('imagery_tools', '11', 'Textural Features')
    if Tool.is_Okay():
        Tool.Set_Input ('GRID', GRID)
        Tool.Set_Output('ASM', ASM)
        Tool.Set_Output('CONTRAST', CONTRAST)
        Tool.Set_Output('CORRELATION', CORRELATION)
        Tool.Set_Output('VARIANCE', VARIANCE)
        Tool.Set_Output('IDM', IDM)
        Tool.Set_Output('SUM_AVERAGE', SUM_AVERAGE)
        Tool.Set_Output('SUM_ENTROPY', SUM_ENTROPY)
        Tool.Set_Output('SUM_VARIANCE', SUM_VARIANCE)
        Tool.Set_Output('ENTROPY', ENTROPY)
        Tool.Set_Output('DIF_VARIANCE', DIF_VARIANCE)
        Tool.Set_Output('DIF_ENTROPY', DIF_ENTROPY)
        Tool.Set_Output('MOC_1', MOC_1)
        Tool.Set_Output('MOC_2', MOC_2)
        Tool.Set_Option('DIRECTION', DIRECTION)
        Tool.Set_Option('RADIUS', RADIUS)
        Tool.Set_Option('DISTANCE', DISTANCE)
        Tool.Set_Option('MAX_CATS', MAX_CATS)
        return Tool.Execute(Verbose)
    return False

def run_tool_imagery_tools_11(GRID=None, ASM=None, CONTRAST=None, CORRELATION=None, VARIANCE=None, IDM=None, SUM_AVERAGE=None, SUM_ENTROPY=None, SUM_VARIANCE=None, ENTROPY=None, DIF_VARIANCE=None, DIF_ENTROPY=None, MOC_1=None, MOC_2=None, DIRECTION=None, RADIUS=None, DISTANCE=None, MAX_CATS=None, Verbose=2):
    '''
    Textural Features
    ----------
    [imagery_tools.11]\n
    Textural features. This tool is based on the GRASS GIS implementation by Carmine Basco (r.texture).\n
    Arguments
    ----------
    - GRID [`input grid`] : Grid
    - ASM [`output grid`] : Angular Second Moment
    - CONTRAST [`output grid`] : Contrast
    - CORRELATION [`output grid`] : Correlation
    - VARIANCE [`output grid`] : Variance
    - IDM [`output grid`] : Inverse Diff Moment
    - SUM_AVERAGE [`output grid`] : Sum Average
    - SUM_ENTROPY [`output grid`] : Sum Entropy
    - SUM_VARIANCE [`output grid`] : Sum Variance
    - ENTROPY [`output grid`] : Entropy
    - DIF_VARIANCE [`output grid`] : Difference Variance
    - DIF_ENTROPY [`output grid`] : Difference Entropy
    - MOC_1 [`output grid`] : Measure of Correlation-1
    - MOC_2 [`output grid`] : Measure of Correlation-2
    - DIRECTION [`choice`] : Direction. Available Choices: [0] all [1] N-S [2] NE-SW [3] E-W [4] SE-NW Default: 0
    - RADIUS [`integer number`] : Radius. Minimum: 1 Default: 1 kernel radius in cells
    - DISTANCE [`integer number`] : Distance. Minimum: 1 Default: 1 The distance between two samples.
    - MAX_CATS [`integer number`] : Maximum Number of Categories. Minimum: 2 Default: 256

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('imagery_tools', '11', 'Textural Features')
    if Tool.is_Okay():
        Tool.Set_Input ('GRID', GRID)
        Tool.Set_Output('ASM', ASM)
        Tool.Set_Output('CONTRAST', CONTRAST)
        Tool.Set_Output('CORRELATION', CORRELATION)
        Tool.Set_Output('VARIANCE', VARIANCE)
        Tool.Set_Output('IDM', IDM)
        Tool.Set_Output('SUM_AVERAGE', SUM_AVERAGE)
        Tool.Set_Output('SUM_ENTROPY', SUM_ENTROPY)
        Tool.Set_Output('SUM_VARIANCE', SUM_VARIANCE)
        Tool.Set_Output('ENTROPY', ENTROPY)
        Tool.Set_Output('DIF_VARIANCE', DIF_VARIANCE)
        Tool.Set_Output('DIF_ENTROPY', DIF_ENTROPY)
        Tool.Set_Output('MOC_1', MOC_1)
        Tool.Set_Output('MOC_2', MOC_2)
        Tool.Set_Option('DIRECTION', DIRECTION)
        Tool.Set_Option('RADIUS', RADIUS)
        Tool.Set_Option('DISTANCE', DISTANCE)
        Tool.Set_Option('MAX_CATS', MAX_CATS)
        return Tool.Execute(Verbose)
    return False

def Local_Statistical_Measures(GRID=None, CONTRAST=None, ENERGY=None, ENTROPY=None, VARIANCE=None, TYPE=None, RADIUS=None, NORMALIZE=None, NORM_MIN=None, NORM_MAX=None, Verbose=2):
    '''
    Local Statistical Measures
    ----------
    [imagery_tools.12]\n
    Local Statistical Measures\n
    Arguments
    ----------
    - GRID [`input grid`] : Grid
    - CONTRAST [`output grid`] : Contrast
    - ENERGY [`output grid`] : Energy
    - ENTROPY [`output grid`] : Entropy
    - VARIANCE [`output grid`] : Variance
    - TYPE [`choice`] : Kernel. Available Choices: [0] square [1] circle Default: 1 kernel radius in cells
    - RADIUS [`integer number`] : Radius. Minimum: 1 Default: 1 kernel radius in cells
    - NORMALIZE [`choice`] : Normalization. Available Choices: [0] no [1] scale to range Default: 1
    - NORM_MIN [`floating point number`] : Minimum. Default: 1.000000
    - NORM_MAX [`floating point number`] : Maximum. Default: 255.000000

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('imagery_tools', '12', 'Local Statistical Measures')
    if Tool.is_Okay():
        Tool.Set_Input ('GRID', GRID)
        Tool.Set_Output('CONTRAST', CONTRAST)
        Tool.Set_Output('ENERGY', ENERGY)
        Tool.Set_Output('ENTROPY', ENTROPY)
        Tool.Set_Output('VARIANCE', VARIANCE)
        Tool.Set_Option('TYPE', TYPE)
        Tool.Set_Option('RADIUS', RADIUS)
        Tool.Set_Option('NORMALIZE', NORMALIZE)
        Tool.Set_Option('NORM_MIN', NORM_MIN)
        Tool.Set_Option('NORM_MAX', NORM_MAX)
        return Tool.Execute(Verbose)
    return False

def run_tool_imagery_tools_12(GRID=None, CONTRAST=None, ENERGY=None, ENTROPY=None, VARIANCE=None, TYPE=None, RADIUS=None, NORMALIZE=None, NORM_MIN=None, NORM_MAX=None, Verbose=2):
    '''
    Local Statistical Measures
    ----------
    [imagery_tools.12]\n
    Local Statistical Measures\n
    Arguments
    ----------
    - GRID [`input grid`] : Grid
    - CONTRAST [`output grid`] : Contrast
    - ENERGY [`output grid`] : Energy
    - ENTROPY [`output grid`] : Entropy
    - VARIANCE [`output grid`] : Variance
    - TYPE [`choice`] : Kernel. Available Choices: [0] square [1] circle Default: 1 kernel radius in cells
    - RADIUS [`integer number`] : Radius. Minimum: 1 Default: 1 kernel radius in cells
    - NORMALIZE [`choice`] : Normalization. Available Choices: [0] no [1] scale to range Default: 1
    - NORM_MIN [`floating point number`] : Minimum. Default: 1.000000
    - NORM_MAX [`floating point number`] : Maximum. Default: 255.000000

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('imagery_tools', '12', 'Local Statistical Measures')
    if Tool.is_Okay():
        Tool.Set_Input ('GRID', GRID)
        Tool.Set_Output('CONTRAST', CONTRAST)
        Tool.Set_Output('ENERGY', ENERGY)
        Tool.Set_Output('ENTROPY', ENTROPY)
        Tool.Set_Output('VARIANCE', VARIANCE)
        Tool.Set_Option('TYPE', TYPE)
        Tool.Set_Option('RADIUS', RADIUS)
        Tool.Set_Option('NORMALIZE', NORMALIZE)
        Tool.Set_Option('NORM_MIN', NORM_MIN)
        Tool.Set_Option('NORM_MAX', NORM_MAX)
        return Tool.Execute(Verbose)
    return False

def Universal_Image_Quality_Index(GRID_A=None, GRID_B=None, QUALITY=None, CORRELATION=None, LUMINANCE=None, CONTRAST=None, K1=None, K2=None, L=None, KERNEL_TYPE=None, KERNEL_RADIUS=None, Verbose=2):
    '''
    Universal Image Quality Index
    ----------
    [imagery_tools.13]\n
    The Universal Image Quality Index compares two grids (greyscale images) using the three parameters luminance, contrast and structure. This is done for each pixel using a moving window as specified by the kernel radius.\n
    Arguments
    ----------
    - GRID_A [`input grid`] : First Grid
    - GRID_B [`input grid`] : Second Grid
    - QUALITY [`output grid`] : Universal Image Quality Index. The product of correlation, luminance and contrast similarity.
    - CORRELATION [`output grid`] : Correlation. The correlation coefficient.
    - LUMINANCE [`output grid`] : Luminance. The similarity of mean luminance.
    - CONTRAST [`output grid`] : Contrast. The similarity of contrast.
    - K1 [`floating point number`] : k1. Minimum: 0.000000 Maximum: 1.000000 Default: 0.010000
    - K2 [`floating point number`] : k2. Minimum: 0.000000 Maximum: 1.000000 Default: 0.030000
    - L [`integer number`] : L. Minimum: 1 Default: 255 The dynamic range for the image pixel, i.e. the number of different grey values.
    - KERNEL_TYPE [`choice`] : Kernel Type. Available Choices: [0] Square [1] Circle Default: 1 The kernel's shape.
    - KERNEL_RADIUS [`integer number`] : Radius. Minimum: 0 Default: 2 cells

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('imagery_tools', '13', 'Universal Image Quality Index')
    if Tool.is_Okay():
        Tool.Set_Input ('GRID_A', GRID_A)
        Tool.Set_Input ('GRID_B', GRID_B)
        Tool.Set_Output('QUALITY', QUALITY)
        Tool.Set_Output('CORRELATION', CORRELATION)
        Tool.Set_Output('LUMINANCE', LUMINANCE)
        Tool.Set_Output('CONTRAST', CONTRAST)
        Tool.Set_Option('K1', K1)
        Tool.Set_Option('K2', K2)
        Tool.Set_Option('L', L)
        Tool.Set_Option('KERNEL_TYPE', KERNEL_TYPE)
        Tool.Set_Option('KERNEL_RADIUS', KERNEL_RADIUS)
        return Tool.Execute(Verbose)
    return False

def run_tool_imagery_tools_13(GRID_A=None, GRID_B=None, QUALITY=None, CORRELATION=None, LUMINANCE=None, CONTRAST=None, K1=None, K2=None, L=None, KERNEL_TYPE=None, KERNEL_RADIUS=None, Verbose=2):
    '''
    Universal Image Quality Index
    ----------
    [imagery_tools.13]\n
    The Universal Image Quality Index compares two grids (greyscale images) using the three parameters luminance, contrast and structure. This is done for each pixel using a moving window as specified by the kernel radius.\n
    Arguments
    ----------
    - GRID_A [`input grid`] : First Grid
    - GRID_B [`input grid`] : Second Grid
    - QUALITY [`output grid`] : Universal Image Quality Index. The product of correlation, luminance and contrast similarity.
    - CORRELATION [`output grid`] : Correlation. The correlation coefficient.
    - LUMINANCE [`output grid`] : Luminance. The similarity of mean luminance.
    - CONTRAST [`output grid`] : Contrast. The similarity of contrast.
    - K1 [`floating point number`] : k1. Minimum: 0.000000 Maximum: 1.000000 Default: 0.010000
    - K2 [`floating point number`] : k2. Minimum: 0.000000 Maximum: 1.000000 Default: 0.030000
    - L [`integer number`] : L. Minimum: 1 Default: 255 The dynamic range for the image pixel, i.e. the number of different grey values.
    - KERNEL_TYPE [`choice`] : Kernel Type. Available Choices: [0] Square [1] Circle Default: 1 The kernel's shape.
    - KERNEL_RADIUS [`integer number`] : Radius. Minimum: 0 Default: 2 cells

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('imagery_tools', '13', 'Universal Image Quality Index')
    if Tool.is_Okay():
        Tool.Set_Input ('GRID_A', GRID_A)
        Tool.Set_Input ('GRID_B', GRID_B)
        Tool.Set_Output('QUALITY', QUALITY)
        Tool.Set_Output('CORRELATION', CORRELATION)
        Tool.Set_Output('LUMINANCE', LUMINANCE)
        Tool.Set_Output('CONTRAST', CONTRAST)
        Tool.Set_Option('K1', K1)
        Tool.Set_Option('K2', K2)
        Tool.Set_Option('L', L)
        Tool.Set_Option('KERNEL_TYPE', KERNEL_TYPE)
        Tool.Set_Option('KERNEL_RADIUS', KERNEL_RADIUS)
        return Tool.Execute(Verbose)
    return False

def Import_Landsat_Scene(EXTENT_SHAPES=None, BANDS_SPECTRAL=None, BANDS_THERMAL=None, BANDS_AUXILIARY=None, BAND_INFO=None, METAFILE=None, MULTI2GRIDS=None, SKIP_PAN=None, SKIP_AEROSOL=None, SKIP_CIRRUS=None, CALIBRATION=None, DATA_TYPE=None, TEMP_UNIT=None, PROJECTION=None, RESAMPLING=None, UTM_ZONE=None, UTM_SOUTH=None, EXTENT=None, EXTENT_XMIN=None, EXTENT_XMAX=None, EXTENT_YMIN=None, EXTENT_YMAX=None, EXTENT_GRID=None, EXTENT_BUFFER=None, Verbose=2):
    '''
    Import Landsat Scene
    ----------
    [imagery_tools.14]\n
    Import Landsat scenes including metadata from Landsat metadata files. Band data have to be stored in the same folder as the chosen metadata file in uncompressed GeoTIFF format.\n
    Arguments
    ----------
    - EXTENT_SHAPES [`input shapes`] : Shapes Extent
    - BANDS_SPECTRAL [`output grid list`] : Spectral Bands
    - BANDS_THERMAL [`output grid list`] : Thermal Bands
    - BANDS_AUXILIARY [`output grid list`] : Auxiliary Bands
    - BAND_INFO [`output table`] : Band Info
    - METAFILE [`file path`] : Metadata File
    - MULTI2GRIDS [`boolean`] : Spectral Bands as Grid Collection. Default: 1
    - SKIP_PAN [`boolean`] : Skip Panchromatic Band. Default: 1
    - SKIP_AEROSOL [`boolean`] : Skip Aerosol Band. Default: 1
    - SKIP_CIRRUS [`boolean`] : Skip Cirrus Band. Default: 1
    - CALIBRATION [`choice`] : Radiometric Calibration. Available Choices: [0] digital numbers [1] radiance [2] reflectance Default: 0
    - DATA_TYPE [`choice`] : Output Data Type. Available Choices: [0] integers with scaling [1] floating point numbers Default: 0
    - TEMP_UNIT [`choice`] : Temperature Unit. Available Choices: [0] Kelvin [1] Celsius Default: 0
    - PROJECTION [`choice`] : Coordinate System. Available Choices: [0] UTM North [1] UTM South [2] Different UTM Zone [3] Geographic Coordinates Default: 0 If using the extent option in combination with 'Different UTM Zone' or 'Geographic Coordinates' extent is expected to be defined with UTM North coordinates.
    - RESAMPLING [`choice`] : Resampling. Available Choices: [0] Nearest Neighbour [1] Bilinear Interpolation [2] Bicubic Spline Interpolation [3] B-Spline Interpolation Default: 3
    - UTM_ZONE [`integer number`] : Zone. Minimum: 1 Maximum: 60 Default: 32
    - UTM_SOUTH [`boolean`] : South. Default: 0
    - EXTENT [`choice`] : Extent. Available Choices: [0] original [1] user defined [2] grid system [3] shapes extent Default: 0
    - EXTENT_XMIN [`floating point number`] : Left. Default: 0.000000
    - EXTENT_XMAX [`floating point number`] : Right. Default: 0.000000
    - EXTENT_YMIN [`floating point number`] : Bottom. Default: 0.000000
    - EXTENT_YMAX [`floating point number`] : Top. Default: 0.000000
    - EXTENT_GRID [`grid system`] : Grid System
    - EXTENT_BUFFER [`floating point number`] : Buffer. Minimum: 0.000000 Default: 0.000000

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('imagery_tools', '14', 'Import Landsat Scene')
    if Tool.is_Okay():
        Tool.Set_Input ('EXTENT_SHAPES', EXTENT_SHAPES)
        Tool.Set_Output('BANDS_SPECTRAL', BANDS_SPECTRAL)
        Tool.Set_Output('BANDS_THERMAL', BANDS_THERMAL)
        Tool.Set_Output('BANDS_AUXILIARY', BANDS_AUXILIARY)
        Tool.Set_Output('BAND_INFO', BAND_INFO)
        Tool.Set_Option('METAFILE', METAFILE)
        Tool.Set_Option('MULTI2GRIDS', MULTI2GRIDS)
        Tool.Set_Option('SKIP_PAN', SKIP_PAN)
        Tool.Set_Option('SKIP_AEROSOL', SKIP_AEROSOL)
        Tool.Set_Option('SKIP_CIRRUS', SKIP_CIRRUS)
        Tool.Set_Option('CALIBRATION', CALIBRATION)
        Tool.Set_Option('DATA_TYPE', DATA_TYPE)
        Tool.Set_Option('TEMP_UNIT', TEMP_UNIT)
        Tool.Set_Option('PROJECTION', PROJECTION)
        Tool.Set_Option('RESAMPLING', RESAMPLING)
        Tool.Set_Option('UTM_ZONE', UTM_ZONE)
        Tool.Set_Option('UTM_SOUTH', UTM_SOUTH)
        Tool.Set_Option('EXTENT', EXTENT)
        Tool.Set_Option('EXTENT_XMIN', EXTENT_XMIN)
        Tool.Set_Option('EXTENT_XMAX', EXTENT_XMAX)
        Tool.Set_Option('EXTENT_YMIN', EXTENT_YMIN)
        Tool.Set_Option('EXTENT_YMAX', EXTENT_YMAX)
        Tool.Set_Option('EXTENT_GRID', EXTENT_GRID)
        Tool.Set_Option('EXTENT_BUFFER', EXTENT_BUFFER)
        return Tool.Execute(Verbose)
    return False

def run_tool_imagery_tools_14(EXTENT_SHAPES=None, BANDS_SPECTRAL=None, BANDS_THERMAL=None, BANDS_AUXILIARY=None, BAND_INFO=None, METAFILE=None, MULTI2GRIDS=None, SKIP_PAN=None, SKIP_AEROSOL=None, SKIP_CIRRUS=None, CALIBRATION=None, DATA_TYPE=None, TEMP_UNIT=None, PROJECTION=None, RESAMPLING=None, UTM_ZONE=None, UTM_SOUTH=None, EXTENT=None, EXTENT_XMIN=None, EXTENT_XMAX=None, EXTENT_YMIN=None, EXTENT_YMAX=None, EXTENT_GRID=None, EXTENT_BUFFER=None, Verbose=2):
    '''
    Import Landsat Scene
    ----------
    [imagery_tools.14]\n
    Import Landsat scenes including metadata from Landsat metadata files. Band data have to be stored in the same folder as the chosen metadata file in uncompressed GeoTIFF format.\n
    Arguments
    ----------
    - EXTENT_SHAPES [`input shapes`] : Shapes Extent
    - BANDS_SPECTRAL [`output grid list`] : Spectral Bands
    - BANDS_THERMAL [`output grid list`] : Thermal Bands
    - BANDS_AUXILIARY [`output grid list`] : Auxiliary Bands
    - BAND_INFO [`output table`] : Band Info
    - METAFILE [`file path`] : Metadata File
    - MULTI2GRIDS [`boolean`] : Spectral Bands as Grid Collection. Default: 1
    - SKIP_PAN [`boolean`] : Skip Panchromatic Band. Default: 1
    - SKIP_AEROSOL [`boolean`] : Skip Aerosol Band. Default: 1
    - SKIP_CIRRUS [`boolean`] : Skip Cirrus Band. Default: 1
    - CALIBRATION [`choice`] : Radiometric Calibration. Available Choices: [0] digital numbers [1] radiance [2] reflectance Default: 0
    - DATA_TYPE [`choice`] : Output Data Type. Available Choices: [0] integers with scaling [1] floating point numbers Default: 0
    - TEMP_UNIT [`choice`] : Temperature Unit. Available Choices: [0] Kelvin [1] Celsius Default: 0
    - PROJECTION [`choice`] : Coordinate System. Available Choices: [0] UTM North [1] UTM South [2] Different UTM Zone [3] Geographic Coordinates Default: 0 If using the extent option in combination with 'Different UTM Zone' or 'Geographic Coordinates' extent is expected to be defined with UTM North coordinates.
    - RESAMPLING [`choice`] : Resampling. Available Choices: [0] Nearest Neighbour [1] Bilinear Interpolation [2] Bicubic Spline Interpolation [3] B-Spline Interpolation Default: 3
    - UTM_ZONE [`integer number`] : Zone. Minimum: 1 Maximum: 60 Default: 32
    - UTM_SOUTH [`boolean`] : South. Default: 0
    - EXTENT [`choice`] : Extent. Available Choices: [0] original [1] user defined [2] grid system [3] shapes extent Default: 0
    - EXTENT_XMIN [`floating point number`] : Left. Default: 0.000000
    - EXTENT_XMAX [`floating point number`] : Right. Default: 0.000000
    - EXTENT_YMIN [`floating point number`] : Bottom. Default: 0.000000
    - EXTENT_YMAX [`floating point number`] : Top. Default: 0.000000
    - EXTENT_GRID [`grid system`] : Grid System
    - EXTENT_BUFFER [`floating point number`] : Buffer. Minimum: 0.000000 Default: 0.000000

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('imagery_tools', '14', 'Import Landsat Scene')
    if Tool.is_Okay():
        Tool.Set_Input ('EXTENT_SHAPES', EXTENT_SHAPES)
        Tool.Set_Output('BANDS_SPECTRAL', BANDS_SPECTRAL)
        Tool.Set_Output('BANDS_THERMAL', BANDS_THERMAL)
        Tool.Set_Output('BANDS_AUXILIARY', BANDS_AUXILIARY)
        Tool.Set_Output('BAND_INFO', BAND_INFO)
        Tool.Set_Option('METAFILE', METAFILE)
        Tool.Set_Option('MULTI2GRIDS', MULTI2GRIDS)
        Tool.Set_Option('SKIP_PAN', SKIP_PAN)
        Tool.Set_Option('SKIP_AEROSOL', SKIP_AEROSOL)
        Tool.Set_Option('SKIP_CIRRUS', SKIP_CIRRUS)
        Tool.Set_Option('CALIBRATION', CALIBRATION)
        Tool.Set_Option('DATA_TYPE', DATA_TYPE)
        Tool.Set_Option('TEMP_UNIT', TEMP_UNIT)
        Tool.Set_Option('PROJECTION', PROJECTION)
        Tool.Set_Option('RESAMPLING', RESAMPLING)
        Tool.Set_Option('UTM_ZONE', UTM_ZONE)
        Tool.Set_Option('UTM_SOUTH', UTM_SOUTH)
        Tool.Set_Option('EXTENT', EXTENT)
        Tool.Set_Option('EXTENT_XMIN', EXTENT_XMIN)
        Tool.Set_Option('EXTENT_XMAX', EXTENT_XMAX)
        Tool.Set_Option('EXTENT_YMIN', EXTENT_YMIN)
        Tool.Set_Option('EXTENT_YMAX', EXTENT_YMAX)
        Tool.Set_Option('EXTENT_GRID', EXTENT_GRID)
        Tool.Set_Option('EXTENT_BUFFER', EXTENT_BUFFER)
        return Tool.Execute(Verbose)
    return False

def Import_Sentinel2_Scene(EXTENT_SHAPES=None, BANDS=None, BANDS_AUX=None, METAFILE=None, MULTI2GRIDS=None, LOAD_60M=None, LOAD_AOT=None, LOAD_WVP=None, LOAD_SCL=None, REFLECTANCE=None, RESOLUTION=None, PROJECTION=None, RESAMPLING=None, UTM_ZONE=None, UTM_SOUTH=None, EXTENT=None, EXTENT_XMIN=None, EXTENT_XMAX=None, EXTENT_YMIN=None, EXTENT_YMAX=None, EXTENT_GRID=None, EXTENT_BUFFER=None, Verbose=2):
    '''
    Import Sentinel-2 Scene
    ----------
    [imagery_tools.15]\n
    Import Sentinel-2 scenes from a folder structure as provided by the original ESA download.\n
    Arguments
    ----------
    - EXTENT_SHAPES [`input shapes`] : Shapes Extent
    - BANDS [`output grid list`] : Spectral Bands
    - BANDS_AUX [`output grid list`] : Auxiliary Bands
    - METAFILE [`file path`] : Metadata File
    - MULTI2GRIDS [`boolean`] : Spectral Bands as Grid Collection. Default: 1
    - LOAD_60M [`boolean`] : Aerosol, Vapour, Cirrus. Default: 0
    - LOAD_AOT [`boolean`] : Aerosol Optical Thickness. Default: 0
    - LOAD_WVP [`boolean`] : Water Vapour. Default: 0
    - LOAD_SCL [`boolean`] : Scene Classification. Default: 0
    - REFLECTANCE [`choice`] : Reflectance Values. Available Choices: [0] digital number [1] fraction Default: 1
    - RESOLUTION [`choice`] : Resolution. Available Choices: [0] original [1] 10m [2] 20m Default: 0 Allows to resample all spectral bands to the same target resolution, either 10 or 20m.
    - PROJECTION [`choice`] : Coordinate System. Available Choices: [0] original [1] UTM North [2] UTM South [3] Different UTM Zone [4] Geographic Coordinates Default: 0 If using the extent option in combination with 'Different UTM Zone' or 'Geographic Coordinates' extent is expected to be defined with UTM North coordinates.
    - RESAMPLING [`choice`] : Resampling. Available Choices: [0] Nearest Neighbour [1] Bilinear Interpolation [2] Bicubic Spline Interpolation [3] B-Spline Interpolation Default: 3
    - UTM_ZONE [`integer number`] : Zone. Minimum: 1 Maximum: 60 Default: 32
    - UTM_SOUTH [`boolean`] : South. Default: 0
    - EXTENT [`choice`] : Extent. Available Choices: [0] original [1] user defined [2] grid system [3] shapes extent Default: 0
    - EXTENT_XMIN [`floating point number`] : Left. Default: 0.000000
    - EXTENT_XMAX [`floating point number`] : Right. Default: 0.000000
    - EXTENT_YMIN [`floating point number`] : Bottom. Default: 0.000000
    - EXTENT_YMAX [`floating point number`] : Top. Default: 0.000000
    - EXTENT_GRID [`grid system`] : Grid System
    - EXTENT_BUFFER [`floating point number`] : Buffer. Minimum: 0.000000 Default: 0.000000

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('imagery_tools', '15', 'Import Sentinel-2 Scene')
    if Tool.is_Okay():
        Tool.Set_Input ('EXTENT_SHAPES', EXTENT_SHAPES)
        Tool.Set_Output('BANDS', BANDS)
        Tool.Set_Output('BANDS_AUX', BANDS_AUX)
        Tool.Set_Option('METAFILE', METAFILE)
        Tool.Set_Option('MULTI2GRIDS', MULTI2GRIDS)
        Tool.Set_Option('LOAD_60M', LOAD_60M)
        Tool.Set_Option('LOAD_AOT', LOAD_AOT)
        Tool.Set_Option('LOAD_WVP', LOAD_WVP)
        Tool.Set_Option('LOAD_SCL', LOAD_SCL)
        Tool.Set_Option('REFLECTANCE', REFLECTANCE)
        Tool.Set_Option('RESOLUTION', RESOLUTION)
        Tool.Set_Option('PROJECTION', PROJECTION)
        Tool.Set_Option('RESAMPLING', RESAMPLING)
        Tool.Set_Option('UTM_ZONE', UTM_ZONE)
        Tool.Set_Option('UTM_SOUTH', UTM_SOUTH)
        Tool.Set_Option('EXTENT', EXTENT)
        Tool.Set_Option('EXTENT_XMIN', EXTENT_XMIN)
        Tool.Set_Option('EXTENT_XMAX', EXTENT_XMAX)
        Tool.Set_Option('EXTENT_YMIN', EXTENT_YMIN)
        Tool.Set_Option('EXTENT_YMAX', EXTENT_YMAX)
        Tool.Set_Option('EXTENT_GRID', EXTENT_GRID)
        Tool.Set_Option('EXTENT_BUFFER', EXTENT_BUFFER)
        return Tool.Execute(Verbose)
    return False

def run_tool_imagery_tools_15(EXTENT_SHAPES=None, BANDS=None, BANDS_AUX=None, METAFILE=None, MULTI2GRIDS=None, LOAD_60M=None, LOAD_AOT=None, LOAD_WVP=None, LOAD_SCL=None, REFLECTANCE=None, RESOLUTION=None, PROJECTION=None, RESAMPLING=None, UTM_ZONE=None, UTM_SOUTH=None, EXTENT=None, EXTENT_XMIN=None, EXTENT_XMAX=None, EXTENT_YMIN=None, EXTENT_YMAX=None, EXTENT_GRID=None, EXTENT_BUFFER=None, Verbose=2):
    '''
    Import Sentinel-2 Scene
    ----------
    [imagery_tools.15]\n
    Import Sentinel-2 scenes from a folder structure as provided by the original ESA download.\n
    Arguments
    ----------
    - EXTENT_SHAPES [`input shapes`] : Shapes Extent
    - BANDS [`output grid list`] : Spectral Bands
    - BANDS_AUX [`output grid list`] : Auxiliary Bands
    - METAFILE [`file path`] : Metadata File
    - MULTI2GRIDS [`boolean`] : Spectral Bands as Grid Collection. Default: 1
    - LOAD_60M [`boolean`] : Aerosol, Vapour, Cirrus. Default: 0
    - LOAD_AOT [`boolean`] : Aerosol Optical Thickness. Default: 0
    - LOAD_WVP [`boolean`] : Water Vapour. Default: 0
    - LOAD_SCL [`boolean`] : Scene Classification. Default: 0
    - REFLECTANCE [`choice`] : Reflectance Values. Available Choices: [0] digital number [1] fraction Default: 1
    - RESOLUTION [`choice`] : Resolution. Available Choices: [0] original [1] 10m [2] 20m Default: 0 Allows to resample all spectral bands to the same target resolution, either 10 or 20m.
    - PROJECTION [`choice`] : Coordinate System. Available Choices: [0] original [1] UTM North [2] UTM South [3] Different UTM Zone [4] Geographic Coordinates Default: 0 If using the extent option in combination with 'Different UTM Zone' or 'Geographic Coordinates' extent is expected to be defined with UTM North coordinates.
    - RESAMPLING [`choice`] : Resampling. Available Choices: [0] Nearest Neighbour [1] Bilinear Interpolation [2] Bicubic Spline Interpolation [3] B-Spline Interpolation Default: 3
    - UTM_ZONE [`integer number`] : Zone. Minimum: 1 Maximum: 60 Default: 32
    - UTM_SOUTH [`boolean`] : South. Default: 0
    - EXTENT [`choice`] : Extent. Available Choices: [0] original [1] user defined [2] grid system [3] shapes extent Default: 0
    - EXTENT_XMIN [`floating point number`] : Left. Default: 0.000000
    - EXTENT_XMAX [`floating point number`] : Right. Default: 0.000000
    - EXTENT_YMIN [`floating point number`] : Bottom. Default: 0.000000
    - EXTENT_YMAX [`floating point number`] : Top. Default: 0.000000
    - EXTENT_GRID [`grid system`] : Grid System
    - EXTENT_BUFFER [`floating point number`] : Buffer. Minimum: 0.000000 Default: 0.000000

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('imagery_tools', '15', 'Import Sentinel-2 Scene')
    if Tool.is_Okay():
        Tool.Set_Input ('EXTENT_SHAPES', EXTENT_SHAPES)
        Tool.Set_Output('BANDS', BANDS)
        Tool.Set_Output('BANDS_AUX', BANDS_AUX)
        Tool.Set_Option('METAFILE', METAFILE)
        Tool.Set_Option('MULTI2GRIDS', MULTI2GRIDS)
        Tool.Set_Option('LOAD_60M', LOAD_60M)
        Tool.Set_Option('LOAD_AOT', LOAD_AOT)
        Tool.Set_Option('LOAD_WVP', LOAD_WVP)
        Tool.Set_Option('LOAD_SCL', LOAD_SCL)
        Tool.Set_Option('REFLECTANCE', REFLECTANCE)
        Tool.Set_Option('RESOLUTION', RESOLUTION)
        Tool.Set_Option('PROJECTION', PROJECTION)
        Tool.Set_Option('RESAMPLING', RESAMPLING)
        Tool.Set_Option('UTM_ZONE', UTM_ZONE)
        Tool.Set_Option('UTM_SOUTH', UTM_SOUTH)
        Tool.Set_Option('EXTENT', EXTENT)
        Tool.Set_Option('EXTENT_XMIN', EXTENT_XMIN)
        Tool.Set_Option('EXTENT_XMAX', EXTENT_XMAX)
        Tool.Set_Option('EXTENT_YMIN', EXTENT_YMIN)
        Tool.Set_Option('EXTENT_YMAX', EXTENT_YMAX)
        Tool.Set_Option('EXTENT_GRID', EXTENT_GRID)
        Tool.Set_Option('EXTENT_BUFFER', EXTENT_BUFFER)
        return Tool.Execute(Verbose)
    return False

def Import_Sentinel3_OLCI_Scene(BANDS=None, DIRECTORY=None, RESOLUTION=None, COLLECTION=None, CRS_STRING=None, Verbose=2):
    '''
    Import Sentinel-3 OLCI Scene
    ----------
    [imagery_tools.16]\n
    Import Sentinel-3 OLCI (Ocean and Land Colour Instrument) scenes from a folder structure as provided by the original ESA download.\n
    Arguments
    ----------
    - BANDS [`output grid list`] : Bands
    - DIRECTORY [`file path`] : Directory
    - RESOLUTION [`floating point number`] : Target Resolution. Minimum: 0.001000 Default: 0.002778
    - COLLECTION [`boolean`] : Bands as Grid Collection. Default: 1
    - CRS_STRING [`text`] : Coordinate System Definition. Supported formats comprise PROJ and WKT strings, object codes (e.g. "EPSG:4326").

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('imagery_tools', '16', 'Import Sentinel-3 OLCI Scene')
    if Tool.is_Okay():
        Tool.Set_Output('BANDS', BANDS)
        Tool.Set_Option('DIRECTORY', DIRECTORY)
        Tool.Set_Option('RESOLUTION', RESOLUTION)
        Tool.Set_Option('COLLECTION', COLLECTION)
        Tool.Set_Option('CRS_STRING', CRS_STRING)
        return Tool.Execute(Verbose)
    return False

def run_tool_imagery_tools_16(BANDS=None, DIRECTORY=None, RESOLUTION=None, COLLECTION=None, CRS_STRING=None, Verbose=2):
    '''
    Import Sentinel-3 OLCI Scene
    ----------
    [imagery_tools.16]\n
    Import Sentinel-3 OLCI (Ocean and Land Colour Instrument) scenes from a folder structure as provided by the original ESA download.\n
    Arguments
    ----------
    - BANDS [`output grid list`] : Bands
    - DIRECTORY [`file path`] : Directory
    - RESOLUTION [`floating point number`] : Target Resolution. Minimum: 0.001000 Default: 0.002778
    - COLLECTION [`boolean`] : Bands as Grid Collection. Default: 1
    - CRS_STRING [`text`] : Coordinate System Definition. Supported formats comprise PROJ and WKT strings, object codes (e.g. "EPSG:4326").

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('imagery_tools', '16', 'Import Sentinel-3 OLCI Scene')
    if Tool.is_Okay():
        Tool.Set_Output('BANDS', BANDS)
        Tool.Set_Option('DIRECTORY', DIRECTORY)
        Tool.Set_Option('RESOLUTION', RESOLUTION)
        Tool.Set_Option('COLLECTION', COLLECTION)
        Tool.Set_Option('CRS_STRING', CRS_STRING)
        return Tool.Execute(Verbose)
    return False

def Spectral_Profile(BANDS=None, LOCATION=None, PROFILE=None, SHOW=None, RESAMPLING=None, LENGTHS=None, Verbose=2):
    '''
    Spectral Profile
    ----------
    [imagery_tools.17]\n
    Spectral Profile\n
    Arguments
    ----------
    - BANDS [`input grid list`] : Spectral Bands
    - LOCATION [`input shapes`] : Profile Location
    - PROFILE [`output table`] : Spectral Profile
    - SHOW [`boolean`] : Show Diagram. Default: 0
    - RESAMPLING [`choice`] : Resampling. Available Choices: [0] Nearest Neighbour [1] Bilinear Interpolation [2] Bicubic Spline Interpolation [3] B-Spline Interpolation Default: 3
    - LENGTHS [`text`] : Wave Lengths. Default: 0.485 0.56 0.66 0.83 1.65 2.215 11.45 Space separated wave lengths ordered corresponding to the bands in input list. If empty a simple enumeration will be used instead.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('imagery_tools', '17', 'Spectral Profile')
    if Tool.is_Okay():
        Tool.Set_Input ('BANDS', BANDS)
        Tool.Set_Input ('LOCATION', LOCATION)
        Tool.Set_Output('PROFILE', PROFILE)
        Tool.Set_Option('SHOW', SHOW)
        Tool.Set_Option('RESAMPLING', RESAMPLING)
        Tool.Set_Option('LENGTHS', LENGTHS)
        return Tool.Execute(Verbose)
    return False

def run_tool_imagery_tools_17(BANDS=None, LOCATION=None, PROFILE=None, SHOW=None, RESAMPLING=None, LENGTHS=None, Verbose=2):
    '''
    Spectral Profile
    ----------
    [imagery_tools.17]\n
    Spectral Profile\n
    Arguments
    ----------
    - BANDS [`input grid list`] : Spectral Bands
    - LOCATION [`input shapes`] : Profile Location
    - PROFILE [`output table`] : Spectral Profile
    - SHOW [`boolean`] : Show Diagram. Default: 0
    - RESAMPLING [`choice`] : Resampling. Available Choices: [0] Nearest Neighbour [1] Bilinear Interpolation [2] Bicubic Spline Interpolation [3] B-Spline Interpolation Default: 3
    - LENGTHS [`text`] : Wave Lengths. Default: 0.485 0.56 0.66 0.83 1.65 2.215 11.45 Space separated wave lengths ordered corresponding to the bands in input list. If empty a simple enumeration will be used instead.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('imagery_tools', '17', 'Spectral Profile')
    if Tool.is_Okay():
        Tool.Set_Input ('BANDS', BANDS)
        Tool.Set_Input ('LOCATION', LOCATION)
        Tool.Set_Output('PROFILE', PROFILE)
        Tool.Set_Option('SHOW', SHOW)
        Tool.Set_Option('RESAMPLING', RESAMPLING)
        Tool.Set_Option('LENGTHS', LENGTHS)
        return Tool.Execute(Verbose)
    return False

def Topographic_Correction(DEM=None, BANDS=None, CORRECTED=None, DEM_GRIDSYSTEM=None, AZIMUTH=None, HEIGHT=None, METHOD=None, MINNAERT=None, MAXCELLS=None, LIMIT=None, LIMIT_MIN=None, LIMIT_MAX=None, Verbose=2):
    '''
    Topographic Correction
    ----------
    [imagery_tools.19]\n
    Topographic correction tools for satellite imagery using a digital elevation model (DEM) and the position of the Sun at acquisition time to estimate and remove shading effects.\n
    Arguments
    ----------
    - DEM [`input grid`] : Elevation
    - BANDS [`input grid list`] : Bands
    - CORRECTED [`output grid list`] : Corrected Bands
    - DEM_GRIDSYSTEM [`grid system`] : Grid system
    - AZIMUTH [`floating point number`] : Azimuth. Minimum: 0.000000 Maximum: 360.000000 Default: 180.000000 direction of sun (degree, clockwise from North)
    - HEIGHT [`floating point number`] : Height. Minimum: 0.000000 Maximum: 90.000000 Default: 45.000000 height of sun above horizon (degree)
    - METHOD [`choice`] : Method. Available Choices: [0] Cosine Correction (Teillet et al. 1982) [1] Cosine Correction (Civco 1989) [2] Minnaert Correction [3] Minnaert Correction with Slope (Riano et al. 2003) [4] Minnaert Correction with Slope (Law & Nichol 2004) [5] C Correction [6] Normalization (after Civco, modified by Law & Nichol) Default: 2
    - MINNAERT [`floating point number`] : Minnaert Correction. Minimum: 0.000000 Maximum: 1.000000 Default: 0.500000
    - MAXCELLS [`integer number`] : Maximum Number of Cells. Minimum: 1000 Default: 100000 Maximum number of grid cells used for trend analysis as required by C correction.
    - LIMIT [`boolean`] : Limit Value Range. Default: 0 Avoid over and undershootings specifying value range limits.
    - LIMIT_MIN [`floating point number`] : Minimum. Default: 1.000000
    - LIMIT_MAX [`floating point number`] : Maximum. Default: 255.000000

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('imagery_tools', '19', 'Topographic Correction')
    if Tool.is_Okay():
        Tool.Set_Input ('DEM', DEM)
        Tool.Set_Input ('BANDS', BANDS)
        Tool.Set_Output('CORRECTED', CORRECTED)
        Tool.Set_Option('DEM_GRIDSYSTEM', DEM_GRIDSYSTEM)
        Tool.Set_Option('AZIMUTH', AZIMUTH)
        Tool.Set_Option('HEIGHT', HEIGHT)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('MINNAERT', MINNAERT)
        Tool.Set_Option('MAXCELLS', MAXCELLS)
        Tool.Set_Option('LIMIT', LIMIT)
        Tool.Set_Option('LIMIT_MIN', LIMIT_MIN)
        Tool.Set_Option('LIMIT_MAX', LIMIT_MAX)
        return Tool.Execute(Verbose)
    return False

def run_tool_imagery_tools_19(DEM=None, BANDS=None, CORRECTED=None, DEM_GRIDSYSTEM=None, AZIMUTH=None, HEIGHT=None, METHOD=None, MINNAERT=None, MAXCELLS=None, LIMIT=None, LIMIT_MIN=None, LIMIT_MAX=None, Verbose=2):
    '''
    Topographic Correction
    ----------
    [imagery_tools.19]\n
    Topographic correction tools for satellite imagery using a digital elevation model (DEM) and the position of the Sun at acquisition time to estimate and remove shading effects.\n
    Arguments
    ----------
    - DEM [`input grid`] : Elevation
    - BANDS [`input grid list`] : Bands
    - CORRECTED [`output grid list`] : Corrected Bands
    - DEM_GRIDSYSTEM [`grid system`] : Grid system
    - AZIMUTH [`floating point number`] : Azimuth. Minimum: 0.000000 Maximum: 360.000000 Default: 180.000000 direction of sun (degree, clockwise from North)
    - HEIGHT [`floating point number`] : Height. Minimum: 0.000000 Maximum: 90.000000 Default: 45.000000 height of sun above horizon (degree)
    - METHOD [`choice`] : Method. Available Choices: [0] Cosine Correction (Teillet et al. 1982) [1] Cosine Correction (Civco 1989) [2] Minnaert Correction [3] Minnaert Correction with Slope (Riano et al. 2003) [4] Minnaert Correction with Slope (Law & Nichol 2004) [5] C Correction [6] Normalization (after Civco, modified by Law & Nichol) Default: 2
    - MINNAERT [`floating point number`] : Minnaert Correction. Minimum: 0.000000 Maximum: 1.000000 Default: 0.500000
    - MAXCELLS [`integer number`] : Maximum Number of Cells. Minimum: 1000 Default: 100000 Maximum number of grid cells used for trend analysis as required by C correction.
    - LIMIT [`boolean`] : Limit Value Range. Default: 0 Avoid over and undershootings specifying value range limits.
    - LIMIT_MIN [`floating point number`] : Minimum. Default: 1.000000
    - LIMIT_MAX [`floating point number`] : Maximum. Default: 255.000000

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('imagery_tools', '19', 'Topographic Correction')
    if Tool.is_Okay():
        Tool.Set_Input ('DEM', DEM)
        Tool.Set_Input ('BANDS', BANDS)
        Tool.Set_Output('CORRECTED', CORRECTED)
        Tool.Set_Option('DEM_GRIDSYSTEM', DEM_GRIDSYSTEM)
        Tool.Set_Option('AZIMUTH', AZIMUTH)
        Tool.Set_Option('HEIGHT', HEIGHT)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('MINNAERT', MINNAERT)
        Tool.Set_Option('MAXCELLS', MAXCELLS)
        Tool.Set_Option('LIMIT', LIMIT)
        Tool.Set_Option('LIMIT_MIN', LIMIT_MIN)
        Tool.Set_Option('LIMIT_MAX', LIMIT_MAX)
        return Tool.Execute(Verbose)
    return False

def Cloud_Detection(BAND_BLUE=None, BAND_GREEN=None, BAND_RED=None, BAND_NIR=None, BAND_SWIR1=None, BAND_SWIR2=None, BAND_THERMAL=None, BAND_CIRRUS=None, CLOUDS=None, BANDS_SWIR=None, BANDS_THERMAL=None, BANDS_CIRRUS=None, THERMAL_UNIT=None, ALGORITHM=None, ACCA_B56C=None, ACCA_B45R=None, ACCA_HIST_N=None, ACCA_CSIG=None, ACCA_PASS2=None, ACCA_SHADOW=None, SHADOWS=None, SUN_AZIMUTH=None, SUN_HEIGHT=None, Verbose=2):
    '''
    Cloud Detection
    ----------
    [imagery_tools.20]\n
    This tool implements pass one of the Function of mask (Fmask) algorithm for cloud and cloud shadow detection in Landsat imagery. Landsat Top of Atmosphere (TOA) reflectance and Brightness Temperature (BT) are used as input.\n
    Alternatively you can choose the scene-average automated cloud-cover assessment (ACCA) algorithm as proposed by Irish (2000) and Irish et al. (2006).\n
    This tool can optionally pass the cloud mask to the "Cloud Shadow Detection" tool as well.\n
    Arguments
    ----------
    - BAND_BLUE [`input grid`] : Blue
    - BAND_GREEN [`input grid`] : Green
    - BAND_RED [`input grid`] : Red
    - BAND_NIR [`input grid`] : Near Infrared
    - BAND_SWIR1 [`input grid`] : Shortwave Infrared 1
    - BAND_SWIR2 [`input grid`] : Shortwave Infrared 2
    - BAND_THERMAL [`optional input grid`] : Thermal
    - BAND_CIRRUS [`optional input grid`] : Cirrus
    - CLOUDS [`output grid`] : Clouds
    - BANDS_SWIR [`grid system`] : Grid System
    - BANDS_THERMAL [`grid system`] : Grid System
    - BANDS_CIRRUS [`grid system`] : Grid System
    - THERMAL_UNIT [`choice`] : Unit. Available Choices: [0] Kelvin [1] Celsius Default: 0
    - ALGORITHM [`choice`] : Algorithm. Available Choices: [0] Fmask [1] ACCA Default: 0
    - ACCA_B56C [`floating point number`] : SWIR/Thermal Threshold. Default: 225.000000 Threshold for SWIR/Thermal Composite (step 6).
    - ACCA_B45R [`floating point number`] : Desert Detection Threshold. Default: 1.000000 Threshold for desert detection (step 10,  NIR/SWIR Ratio).
    - ACCA_HIST_N [`integer number`] : Temperature Histogram. Minimum: 10 Default: 100 Number of classes in the cloud temperature histogram.
    - ACCA_CSIG [`boolean`] : Cloud Signature. Default: 1 Always use cloud signature (step 14).
    - ACCA_PASS2 [`boolean`] : Cloud Differentiation. Default: 0 Differentiate between warm (not ambiguous) and cold clouds.
    - ACCA_SHADOW [`boolean`] : Shadows. Default: 0 Include a category for cloud shadows.
    - SHADOWS [`boolean`] : Shadow Detection. Default: 0 Run cloud shadow detection tool with standard settings.
    - SUN_AZIMUTH [`floating point number`] : Sun's Azimuth. Minimum: 0.000000 Maximum: 360.000000 Default: -180.000000 Direction of sun clockwise from North [degree].
    - SUN_HEIGHT [`floating point number`] : Sun's Height. Minimum: 0.000000 Maximum: 90.000000 Default: 45.000000 Height of sun above horizon [degree].

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('imagery_tools', '20', 'Cloud Detection')
    if Tool.is_Okay():
        Tool.Set_Input ('BAND_BLUE', BAND_BLUE)
        Tool.Set_Input ('BAND_GREEN', BAND_GREEN)
        Tool.Set_Input ('BAND_RED', BAND_RED)
        Tool.Set_Input ('BAND_NIR', BAND_NIR)
        Tool.Set_Input ('BAND_SWIR1', BAND_SWIR1)
        Tool.Set_Input ('BAND_SWIR2', BAND_SWIR2)
        Tool.Set_Input ('BAND_THERMAL', BAND_THERMAL)
        Tool.Set_Input ('BAND_CIRRUS', BAND_CIRRUS)
        Tool.Set_Output('CLOUDS', CLOUDS)
        Tool.Set_Option('BANDS_SWIR', BANDS_SWIR)
        Tool.Set_Option('BANDS_THERMAL', BANDS_THERMAL)
        Tool.Set_Option('BANDS_CIRRUS', BANDS_CIRRUS)
        Tool.Set_Option('THERMAL_UNIT', THERMAL_UNIT)
        Tool.Set_Option('ALGORITHM', ALGORITHM)
        Tool.Set_Option('ACCA_B56C', ACCA_B56C)
        Tool.Set_Option('ACCA_B45R', ACCA_B45R)
        Tool.Set_Option('ACCA_HIST_N', ACCA_HIST_N)
        Tool.Set_Option('ACCA_CSIG', ACCA_CSIG)
        Tool.Set_Option('ACCA_PASS2', ACCA_PASS2)
        Tool.Set_Option('ACCA_SHADOW', ACCA_SHADOW)
        Tool.Set_Option('SHADOWS', SHADOWS)
        Tool.Set_Option('SUN_AZIMUTH', SUN_AZIMUTH)
        Tool.Set_Option('SUN_HEIGHT', SUN_HEIGHT)
        return Tool.Execute(Verbose)
    return False

def run_tool_imagery_tools_20(BAND_BLUE=None, BAND_GREEN=None, BAND_RED=None, BAND_NIR=None, BAND_SWIR1=None, BAND_SWIR2=None, BAND_THERMAL=None, BAND_CIRRUS=None, CLOUDS=None, BANDS_SWIR=None, BANDS_THERMAL=None, BANDS_CIRRUS=None, THERMAL_UNIT=None, ALGORITHM=None, ACCA_B56C=None, ACCA_B45R=None, ACCA_HIST_N=None, ACCA_CSIG=None, ACCA_PASS2=None, ACCA_SHADOW=None, SHADOWS=None, SUN_AZIMUTH=None, SUN_HEIGHT=None, Verbose=2):
    '''
    Cloud Detection
    ----------
    [imagery_tools.20]\n
    This tool implements pass one of the Function of mask (Fmask) algorithm for cloud and cloud shadow detection in Landsat imagery. Landsat Top of Atmosphere (TOA) reflectance and Brightness Temperature (BT) are used as input.\n
    Alternatively you can choose the scene-average automated cloud-cover assessment (ACCA) algorithm as proposed by Irish (2000) and Irish et al. (2006).\n
    This tool can optionally pass the cloud mask to the "Cloud Shadow Detection" tool as well.\n
    Arguments
    ----------
    - BAND_BLUE [`input grid`] : Blue
    - BAND_GREEN [`input grid`] : Green
    - BAND_RED [`input grid`] : Red
    - BAND_NIR [`input grid`] : Near Infrared
    - BAND_SWIR1 [`input grid`] : Shortwave Infrared 1
    - BAND_SWIR2 [`input grid`] : Shortwave Infrared 2
    - BAND_THERMAL [`optional input grid`] : Thermal
    - BAND_CIRRUS [`optional input grid`] : Cirrus
    - CLOUDS [`output grid`] : Clouds
    - BANDS_SWIR [`grid system`] : Grid System
    - BANDS_THERMAL [`grid system`] : Grid System
    - BANDS_CIRRUS [`grid system`] : Grid System
    - THERMAL_UNIT [`choice`] : Unit. Available Choices: [0] Kelvin [1] Celsius Default: 0
    - ALGORITHM [`choice`] : Algorithm. Available Choices: [0] Fmask [1] ACCA Default: 0
    - ACCA_B56C [`floating point number`] : SWIR/Thermal Threshold. Default: 225.000000 Threshold for SWIR/Thermal Composite (step 6).
    - ACCA_B45R [`floating point number`] : Desert Detection Threshold. Default: 1.000000 Threshold for desert detection (step 10,  NIR/SWIR Ratio).
    - ACCA_HIST_N [`integer number`] : Temperature Histogram. Minimum: 10 Default: 100 Number of classes in the cloud temperature histogram.
    - ACCA_CSIG [`boolean`] : Cloud Signature. Default: 1 Always use cloud signature (step 14).
    - ACCA_PASS2 [`boolean`] : Cloud Differentiation. Default: 0 Differentiate between warm (not ambiguous) and cold clouds.
    - ACCA_SHADOW [`boolean`] : Shadows. Default: 0 Include a category for cloud shadows.
    - SHADOWS [`boolean`] : Shadow Detection. Default: 0 Run cloud shadow detection tool with standard settings.
    - SUN_AZIMUTH [`floating point number`] : Sun's Azimuth. Minimum: 0.000000 Maximum: 360.000000 Default: -180.000000 Direction of sun clockwise from North [degree].
    - SUN_HEIGHT [`floating point number`] : Sun's Height. Minimum: 0.000000 Maximum: 90.000000 Default: 45.000000 Height of sun above horizon [degree].

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('imagery_tools', '20', 'Cloud Detection')
    if Tool.is_Okay():
        Tool.Set_Input ('BAND_BLUE', BAND_BLUE)
        Tool.Set_Input ('BAND_GREEN', BAND_GREEN)
        Tool.Set_Input ('BAND_RED', BAND_RED)
        Tool.Set_Input ('BAND_NIR', BAND_NIR)
        Tool.Set_Input ('BAND_SWIR1', BAND_SWIR1)
        Tool.Set_Input ('BAND_SWIR2', BAND_SWIR2)
        Tool.Set_Input ('BAND_THERMAL', BAND_THERMAL)
        Tool.Set_Input ('BAND_CIRRUS', BAND_CIRRUS)
        Tool.Set_Output('CLOUDS', CLOUDS)
        Tool.Set_Option('BANDS_SWIR', BANDS_SWIR)
        Tool.Set_Option('BANDS_THERMAL', BANDS_THERMAL)
        Tool.Set_Option('BANDS_CIRRUS', BANDS_CIRRUS)
        Tool.Set_Option('THERMAL_UNIT', THERMAL_UNIT)
        Tool.Set_Option('ALGORITHM', ALGORITHM)
        Tool.Set_Option('ACCA_B56C', ACCA_B56C)
        Tool.Set_Option('ACCA_B45R', ACCA_B45R)
        Tool.Set_Option('ACCA_HIST_N', ACCA_HIST_N)
        Tool.Set_Option('ACCA_CSIG', ACCA_CSIG)
        Tool.Set_Option('ACCA_PASS2', ACCA_PASS2)
        Tool.Set_Option('ACCA_SHADOW', ACCA_SHADOW)
        Tool.Set_Option('SHADOWS', SHADOWS)
        Tool.Set_Option('SUN_AZIMUTH', SUN_AZIMUTH)
        Tool.Set_Option('SUN_HEIGHT', SUN_HEIGHT)
        return Tool.Execute(Verbose)
    return False

def Cloud_Shadow_Detection(CLOUDS=None, CAND_GRID_IN=None, BAND_GREEN=None, BAND_RED=None, BAND_NIR=None, BAND_SWIR=None, BANDS_BRIGHTNESS=None, DEM=None, BAND_THERMAL=None, CLOUD_ID=None, CAND_GRID_OUT=None, SHADOWS=None, CLOUD_INFO=None, BANDS_SWIR=None, BANDS_THERMAL=None, OUTPUT=None, CAND_GRID_VALUE=None, THERMAL_UNIT=None, CANDIDATES=None, BRIGHTNESS=None, PROCESSING=None, SUN_AZIMUTH=None, SUN_HEIGHT=None, CLOUD_HEIGHT=None, Verbose=2):
    '''
    Cloud Shadow Detection
    ----------
    [imagery_tools.21]\n
    This tool derives cloud shadows from detected clouds based on their spectral characteristics as well as terrain, sun position and expected cloud height. The initial cloud mask can be created by the tool "Cloud Detection".\n
    Arguments
    ----------
    - CLOUDS [`input grid`] : Clouds
    - CAND_GRID_IN [`input grid`] : Candidates
    - BAND_GREEN [`input grid`] : Green
    - BAND_RED [`input grid`] : Red
    - BAND_NIR [`input grid`] : Near Infrared
    - BAND_SWIR [`input grid`] : Shortwave Infrared
    - BANDS_BRIGHTNESS [`input grid list`] : Brightness Bands
    - DEM [`optional input grid`] : Elevation
    - BAND_THERMAL [`optional input grid`] : Thermal
    - CLOUD_ID [`output grid`] : Cloud Number
    - CAND_GRID_OUT [`output grid`] : Candidates
    - SHADOWS [`output grid`] : Shadows
    - CLOUD_INFO [`output table`] : Cloud Info
    - BANDS_SWIR [`grid system`] : Grid System
    - BANDS_THERMAL [`grid system`] : Grid System
    - OUTPUT [`choice`] : Output. Available Choices: [0] visible shadow [1] full shadow [2] shadow and clouds Default: 2
    - CAND_GRID_VALUE [`floating point number`] : Value. Default: 0.000000
    - THERMAL_UNIT [`choice`] : Unit. Available Choices: [0] Kelvin [1] Celsius Default: 0
    - CANDIDATES [`choice`] : Candidates. Available Choices: [0] all cells of candidates grid that are not no-data [1] all cells of candidates grid with a specified value [2] average brightness threshold [3] Irish [4] Irish modified by Tizado Default: 0
    - BRIGHTNESS [`floating point number`] : Brightness Threshold. Minimum: 0.000000 Maximum: 1.000000 Default: 0.050000
    - PROCESSING [`choice`] : Processing. Available Choices: [0] all clouds at once [1] cloud by cloud Default: 0
    - SUN_AZIMUTH [`floating point number`] : Azimuth. Minimum: 0.000000 Maximum: 360.000000 Default: -180.000000 Direction of sun clockwise from North [degree].
    - SUN_HEIGHT [`floating point number`] : Height. Minimum: 0.000000 Maximum: 90.000000 Default: 45.000000 Height of sun above horizon [degree].
    - CLOUD_HEIGHT [`value range`] : Cloud Height. The range of cloud height above ground to be tested.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('imagery_tools', '21', 'Cloud Shadow Detection')
    if Tool.is_Okay():
        Tool.Set_Input ('CLOUDS', CLOUDS)
        Tool.Set_Input ('CAND_GRID_IN', CAND_GRID_IN)
        Tool.Set_Input ('BAND_GREEN', BAND_GREEN)
        Tool.Set_Input ('BAND_RED', BAND_RED)
        Tool.Set_Input ('BAND_NIR', BAND_NIR)
        Tool.Set_Input ('BAND_SWIR', BAND_SWIR)
        Tool.Set_Input ('BANDS_BRIGHTNESS', BANDS_BRIGHTNESS)
        Tool.Set_Input ('DEM', DEM)
        Tool.Set_Input ('BAND_THERMAL', BAND_THERMAL)
        Tool.Set_Output('CLOUD_ID', CLOUD_ID)
        Tool.Set_Output('CAND_GRID_OUT', CAND_GRID_OUT)
        Tool.Set_Output('SHADOWS', SHADOWS)
        Tool.Set_Output('CLOUD_INFO', CLOUD_INFO)
        Tool.Set_Option('BANDS_SWIR', BANDS_SWIR)
        Tool.Set_Option('BANDS_THERMAL', BANDS_THERMAL)
        Tool.Set_Option('OUTPUT', OUTPUT)
        Tool.Set_Option('CAND_GRID_VALUE', CAND_GRID_VALUE)
        Tool.Set_Option('THERMAL_UNIT', THERMAL_UNIT)
        Tool.Set_Option('CANDIDATES', CANDIDATES)
        Tool.Set_Option('BRIGHTNESS', BRIGHTNESS)
        Tool.Set_Option('PROCESSING', PROCESSING)
        Tool.Set_Option('SUN_AZIMUTH', SUN_AZIMUTH)
        Tool.Set_Option('SUN_HEIGHT', SUN_HEIGHT)
        Tool.Set_Option('CLOUD_HEIGHT', CLOUD_HEIGHT)
        return Tool.Execute(Verbose)
    return False

def run_tool_imagery_tools_21(CLOUDS=None, CAND_GRID_IN=None, BAND_GREEN=None, BAND_RED=None, BAND_NIR=None, BAND_SWIR=None, BANDS_BRIGHTNESS=None, DEM=None, BAND_THERMAL=None, CLOUD_ID=None, CAND_GRID_OUT=None, SHADOWS=None, CLOUD_INFO=None, BANDS_SWIR=None, BANDS_THERMAL=None, OUTPUT=None, CAND_GRID_VALUE=None, THERMAL_UNIT=None, CANDIDATES=None, BRIGHTNESS=None, PROCESSING=None, SUN_AZIMUTH=None, SUN_HEIGHT=None, CLOUD_HEIGHT=None, Verbose=2):
    '''
    Cloud Shadow Detection
    ----------
    [imagery_tools.21]\n
    This tool derives cloud shadows from detected clouds based on their spectral characteristics as well as terrain, sun position and expected cloud height. The initial cloud mask can be created by the tool "Cloud Detection".\n
    Arguments
    ----------
    - CLOUDS [`input grid`] : Clouds
    - CAND_GRID_IN [`input grid`] : Candidates
    - BAND_GREEN [`input grid`] : Green
    - BAND_RED [`input grid`] : Red
    - BAND_NIR [`input grid`] : Near Infrared
    - BAND_SWIR [`input grid`] : Shortwave Infrared
    - BANDS_BRIGHTNESS [`input grid list`] : Brightness Bands
    - DEM [`optional input grid`] : Elevation
    - BAND_THERMAL [`optional input grid`] : Thermal
    - CLOUD_ID [`output grid`] : Cloud Number
    - CAND_GRID_OUT [`output grid`] : Candidates
    - SHADOWS [`output grid`] : Shadows
    - CLOUD_INFO [`output table`] : Cloud Info
    - BANDS_SWIR [`grid system`] : Grid System
    - BANDS_THERMAL [`grid system`] : Grid System
    - OUTPUT [`choice`] : Output. Available Choices: [0] visible shadow [1] full shadow [2] shadow and clouds Default: 2
    - CAND_GRID_VALUE [`floating point number`] : Value. Default: 0.000000
    - THERMAL_UNIT [`choice`] : Unit. Available Choices: [0] Kelvin [1] Celsius Default: 0
    - CANDIDATES [`choice`] : Candidates. Available Choices: [0] all cells of candidates grid that are not no-data [1] all cells of candidates grid with a specified value [2] average brightness threshold [3] Irish [4] Irish modified by Tizado Default: 0
    - BRIGHTNESS [`floating point number`] : Brightness Threshold. Minimum: 0.000000 Maximum: 1.000000 Default: 0.050000
    - PROCESSING [`choice`] : Processing. Available Choices: [0] all clouds at once [1] cloud by cloud Default: 0
    - SUN_AZIMUTH [`floating point number`] : Azimuth. Minimum: 0.000000 Maximum: 360.000000 Default: -180.000000 Direction of sun clockwise from North [degree].
    - SUN_HEIGHT [`floating point number`] : Height. Minimum: 0.000000 Maximum: 90.000000 Default: 45.000000 Height of sun above horizon [degree].
    - CLOUD_HEIGHT [`value range`] : Cloud Height. The range of cloud height above ground to be tested.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('imagery_tools', '21', 'Cloud Shadow Detection')
    if Tool.is_Okay():
        Tool.Set_Input ('CLOUDS', CLOUDS)
        Tool.Set_Input ('CAND_GRID_IN', CAND_GRID_IN)
        Tool.Set_Input ('BAND_GREEN', BAND_GREEN)
        Tool.Set_Input ('BAND_RED', BAND_RED)
        Tool.Set_Input ('BAND_NIR', BAND_NIR)
        Tool.Set_Input ('BAND_SWIR', BAND_SWIR)
        Tool.Set_Input ('BANDS_BRIGHTNESS', BANDS_BRIGHTNESS)
        Tool.Set_Input ('DEM', DEM)
        Tool.Set_Input ('BAND_THERMAL', BAND_THERMAL)
        Tool.Set_Output('CLOUD_ID', CLOUD_ID)
        Tool.Set_Output('CAND_GRID_OUT', CAND_GRID_OUT)
        Tool.Set_Output('SHADOWS', SHADOWS)
        Tool.Set_Output('CLOUD_INFO', CLOUD_INFO)
        Tool.Set_Option('BANDS_SWIR', BANDS_SWIR)
        Tool.Set_Option('BANDS_THERMAL', BANDS_THERMAL)
        Tool.Set_Option('OUTPUT', OUTPUT)
        Tool.Set_Option('CAND_GRID_VALUE', CAND_GRID_VALUE)
        Tool.Set_Option('THERMAL_UNIT', THERMAL_UNIT)
        Tool.Set_Option('CANDIDATES', CANDIDATES)
        Tool.Set_Option('BRIGHTNESS', BRIGHTNESS)
        Tool.Set_Option('PROCESSING', PROCESSING)
        Tool.Set_Option('SUN_AZIMUTH', SUN_AZIMUTH)
        Tool.Set_Option('SUN_HEIGHT', SUN_HEIGHT)
        Tool.Set_Option('CLOUD_HEIGHT', CLOUD_HEIGHT)
        return Tool.Execute(Verbose)
    return False

def Import_SPOT_Scene(BANDS=None, METAFILE=None, PROJECTION=None, RESAMPLING=None, UTM_ZONE=None, UTM_SOUTH=None, SHIFT_X=None, SHIFT_Y=None, Verbose=2):
    '''
    Import SPOT Scene
    ----------
    [imagery_tools.22]\n
    Imports a SPOT (Satellite Pour l'Observation de la Terre) scene. Currently this is just a simple import support tool for SPOT level 1A data.\n
    Arguments
    ----------
    - BANDS [`output grid list`] : Spectral Bands
    - METAFILE [`file path`] : Metadata File
    - PROJECTION [`choice`] : Coordinate System. Available Choices: [0] Geographic Coordinates [1] Universal Transverse Mercator Default: 1
    - RESAMPLING [`choice`] : Resampling. Available Choices: [0] Nearest Neighbour [1] Bilinear Interpolation [2] Bicubic Spline Interpolation [3] B-Spline Interpolation Default: 3
    - UTM_ZONE [`integer number`] : Zone. Minimum: 1 Maximum: 60 Default: 32
    - UTM_SOUTH [`boolean`] : South. Default: 0
    - SHIFT_X [`floating point number`] : Shift.x. Default: 0.000000
    - SHIFT_Y [`floating point number`] : Shift.y. Default: 0.000000

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('imagery_tools', '22', 'Import SPOT Scene')
    if Tool.is_Okay():
        Tool.Set_Output('BANDS', BANDS)
        Tool.Set_Option('METAFILE', METAFILE)
        Tool.Set_Option('PROJECTION', PROJECTION)
        Tool.Set_Option('RESAMPLING', RESAMPLING)
        Tool.Set_Option('UTM_ZONE', UTM_ZONE)
        Tool.Set_Option('UTM_SOUTH', UTM_SOUTH)
        Tool.Set_Option('SHIFT_X', SHIFT_X)
        Tool.Set_Option('SHIFT_Y', SHIFT_Y)
        return Tool.Execute(Verbose)
    return False

def run_tool_imagery_tools_22(BANDS=None, METAFILE=None, PROJECTION=None, RESAMPLING=None, UTM_ZONE=None, UTM_SOUTH=None, SHIFT_X=None, SHIFT_Y=None, Verbose=2):
    '''
    Import SPOT Scene
    ----------
    [imagery_tools.22]\n
    Imports a SPOT (Satellite Pour l'Observation de la Terre) scene. Currently this is just a simple import support tool for SPOT level 1A data.\n
    Arguments
    ----------
    - BANDS [`output grid list`] : Spectral Bands
    - METAFILE [`file path`] : Metadata File
    - PROJECTION [`choice`] : Coordinate System. Available Choices: [0] Geographic Coordinates [1] Universal Transverse Mercator Default: 1
    - RESAMPLING [`choice`] : Resampling. Available Choices: [0] Nearest Neighbour [1] Bilinear Interpolation [2] Bicubic Spline Interpolation [3] B-Spline Interpolation Default: 3
    - UTM_ZONE [`integer number`] : Zone. Minimum: 1 Maximum: 60 Default: 32
    - UTM_SOUTH [`boolean`] : South. Default: 0
    - SHIFT_X [`floating point number`] : Shift.x. Default: 0.000000
    - SHIFT_Y [`floating point number`] : Shift.y. Default: 0.000000

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('imagery_tools', '22', 'Import SPOT Scene')
    if Tool.is_Okay():
        Tool.Set_Output('BANDS', BANDS)
        Tool.Set_Option('METAFILE', METAFILE)
        Tool.Set_Option('PROJECTION', PROJECTION)
        Tool.Set_Option('RESAMPLING', RESAMPLING)
        Tool.Set_Option('UTM_ZONE', UTM_ZONE)
        Tool.Set_Option('UTM_SOUTH', UTM_SOUTH)
        Tool.Set_Option('SHIFT_X', SHIFT_X)
        Tool.Set_Option('SHIFT_Y', SHIFT_Y)
        return Tool.Execute(Verbose)
    return False

def Decode_Landsat_Quality_Assessment_Bands(IN_QA_PIXEL=None, IN_QA_RADSAT=None, IN_SR_QA_AEROSOL=None, OUTPUT=None, SENSOR=None, IN_QA_PIX_SELECTION=None, IN_QA_RADSAT_SELECTION=None, IN_SR_QA_AEROSOL_SELECTION=None, SELECTION=None, GRIDS=None, SET_LUT=None, Verbose=2):
    '''
    Decode Landsat Quality Assessment Bands
    ----------
    [imagery_tools.23]\n
    This tool decodes Landsat Multispectral Scanner System (MSS), Thematic Mapper (TM), Enhanced Thematic Mapper Plus (ETM+), and Operational Land Imager/Thermal Infrared Sensor (OLI/TIRS) Quality Assessment (QA) bands. It splits these QA bands into individual bands and optionally aggregates them into a Grid Collection. It is also possible to select individual flags for output.\n
    Currently, the tool supports Pixel, Radiometric Saturation and Surface Reflectance Aerosol (only OLI/TIRS) Quality Assessment bands from Collection 2 (Level 1 and 2). It also provides value interpretation for certain sensors and QA bands, which can be optionally added to the input datasets for classified displaying in the GUI.\n
    Arguments
    ----------
    - IN_QA_PIXEL [`optional input grid`] : Pixel QA Band. "QA_PIXEL"-Suffix
    - IN_QA_RADSAT [`optional input grid`] : Radiometric Saturation QA Band. "QA_RADSAT"-Suffix
    - IN_SR_QA_AEROSOL [`optional input grid`] : SR Aerosol QA Band. "SR_QA_AEROSOL"-Suffix
    - OUTPUT [`output grid list`] : Output
    - SENSOR [`choice`] : Spacecraft (Sensor). Available Choices: [0] Landsat 1-5 (MSS) [1] Landsat 4-7 (TM & ETM+) [2] Landsat 8-9 (OLI/TIRS) Default: 2
    - IN_QA_PIX_SELECTION [`choices`] : Flag Selection. Available Choices:
    - IN_QA_RADSAT_SELECTION [`choices`] : Flag Selection. Available Choices:
    - IN_SR_QA_AEROSOL_SELECTION [`choices`] : Flag Selection. Available Choices:
    - SELECTION [`boolean`] : Select individual Bands. Default: 0
    - GRIDS [`boolean`] : Output as Grid Collection. Default: 0
    - SET_LUT [`boolean`] : Classify Colors of Input. Default: 1

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('imagery_tools', '23', 'Decode Landsat Quality Assessment Bands')
    if Tool.is_Okay():
        Tool.Set_Input ('IN_QA_PIXEL', IN_QA_PIXEL)
        Tool.Set_Input ('IN_QA_RADSAT', IN_QA_RADSAT)
        Tool.Set_Input ('IN_SR_QA_AEROSOL', IN_SR_QA_AEROSOL)
        Tool.Set_Output('OUTPUT', OUTPUT)
        Tool.Set_Option('SENSOR', SENSOR)
        Tool.Set_Option('IN_QA_PIX_SELECTION', IN_QA_PIX_SELECTION)
        Tool.Set_Option('IN_QA_RADSAT_SELECTION', IN_QA_RADSAT_SELECTION)
        Tool.Set_Option('IN_SR_QA_AEROSOL_SELECTION', IN_SR_QA_AEROSOL_SELECTION)
        Tool.Set_Option('SELECTION', SELECTION)
        Tool.Set_Option('GRIDS', GRIDS)
        Tool.Set_Option('SET_LUT', SET_LUT)
        return Tool.Execute(Verbose)
    return False

def run_tool_imagery_tools_23(IN_QA_PIXEL=None, IN_QA_RADSAT=None, IN_SR_QA_AEROSOL=None, OUTPUT=None, SENSOR=None, IN_QA_PIX_SELECTION=None, IN_QA_RADSAT_SELECTION=None, IN_SR_QA_AEROSOL_SELECTION=None, SELECTION=None, GRIDS=None, SET_LUT=None, Verbose=2):
    '''
    Decode Landsat Quality Assessment Bands
    ----------
    [imagery_tools.23]\n
    This tool decodes Landsat Multispectral Scanner System (MSS), Thematic Mapper (TM), Enhanced Thematic Mapper Plus (ETM+), and Operational Land Imager/Thermal Infrared Sensor (OLI/TIRS) Quality Assessment (QA) bands. It splits these QA bands into individual bands and optionally aggregates them into a Grid Collection. It is also possible to select individual flags for output.\n
    Currently, the tool supports Pixel, Radiometric Saturation and Surface Reflectance Aerosol (only OLI/TIRS) Quality Assessment bands from Collection 2 (Level 1 and 2). It also provides value interpretation for certain sensors and QA bands, which can be optionally added to the input datasets for classified displaying in the GUI.\n
    Arguments
    ----------
    - IN_QA_PIXEL [`optional input grid`] : Pixel QA Band. "QA_PIXEL"-Suffix
    - IN_QA_RADSAT [`optional input grid`] : Radiometric Saturation QA Band. "QA_RADSAT"-Suffix
    - IN_SR_QA_AEROSOL [`optional input grid`] : SR Aerosol QA Band. "SR_QA_AEROSOL"-Suffix
    - OUTPUT [`output grid list`] : Output
    - SENSOR [`choice`] : Spacecraft (Sensor). Available Choices: [0] Landsat 1-5 (MSS) [1] Landsat 4-7 (TM & ETM+) [2] Landsat 8-9 (OLI/TIRS) Default: 2
    - IN_QA_PIX_SELECTION [`choices`] : Flag Selection. Available Choices:
    - IN_QA_RADSAT_SELECTION [`choices`] : Flag Selection. Available Choices:
    - IN_SR_QA_AEROSOL_SELECTION [`choices`] : Flag Selection. Available Choices:
    - SELECTION [`boolean`] : Select individual Bands. Default: 0
    - GRIDS [`boolean`] : Output as Grid Collection. Default: 0
    - SET_LUT [`boolean`] : Classify Colors of Input. Default: 1

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('imagery_tools', '23', 'Decode Landsat Quality Assessment Bands')
    if Tool.is_Okay():
        Tool.Set_Input ('IN_QA_PIXEL', IN_QA_PIXEL)
        Tool.Set_Input ('IN_QA_RADSAT', IN_QA_RADSAT)
        Tool.Set_Input ('IN_SR_QA_AEROSOL', IN_SR_QA_AEROSOL)
        Tool.Set_Output('OUTPUT', OUTPUT)
        Tool.Set_Option('SENSOR', SENSOR)
        Tool.Set_Option('IN_QA_PIX_SELECTION', IN_QA_PIX_SELECTION)
        Tool.Set_Option('IN_QA_RADSAT_SELECTION', IN_QA_RADSAT_SELECTION)
        Tool.Set_Option('IN_SR_QA_AEROSOL_SELECTION', IN_SR_QA_AEROSOL_SELECTION)
        Tool.Set_Option('SELECTION', SELECTION)
        Tool.Set_Option('GRIDS', GRIDS)
        Tool.Set_Option('SET_LUT', SET_LUT)
        return Tool.Execute(Verbose)
    return False

