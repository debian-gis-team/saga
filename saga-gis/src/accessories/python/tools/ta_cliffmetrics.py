#! /usr/bin/env python

'''
Python Interface to SAGA Tools Library
----------
- Category : Terrain Analysis
- Name     : CliffMetrics
- ID       : ta_cliffmetrics

Description
----------
CliffMetrics (Automatic Cliff Metrics delineation) delineates the location of the coastline, coastline normals, and cliff top and toe location along these normals. 
'''

from PySAGA.helper import Tool_Wrapper

def CliffMetrics(DEM=None, COAST_INITIAL=None, SEDIMENT_TOP=None, RASTER_COAST=None, RASTER_NORMAL=None, COAST=None, NORMALS=None, CLIFF_TOP=None, CLIFF_TOE=None, COAST_POINT=None, INVALID_NORMALS=None, COAST_CURVATURE=None, PROFILES=None, COASTSEAHANDINESS=None, STARTEDGEUSERCOASTLINE=None, ENDEDGEUSERCOASTLINE=None, STILLWATERLEVEL=None, COASTSMOOTH=None, COASTSMOOTHWINDOW=None, SAVGOLCOASTPOLY=None, SCALERASTEROUTPUT=None, RANDOMCOASTEDGESEARCH=None, COASTNORMALLENGTH=None, ELETOLERANCE=None, OUTPATH=None, Verbose=2):
    '''
    CliffMetrics
    ----------
    [ta_cliffmetrics.0]\n
    CliffMetrics (Automatic Cliff Metrics delineation) delineates the location of the coastline, coastline normals, and cliff top and toe location along these normals.\n
    Arguments
    ----------
    - DEM [`input grid`] : Elevation
    - COAST_INITIAL [`optional input shapes`] : User Defined Coastline Points. if non empty, cliffmetric will use the user defined coastal line to draw the transects
    - SEDIMENT_TOP [`output grid`] : Sediment Top Elevation
    - RASTER_COAST [`output grid`] : Coastline
    - RASTER_NORMAL [`output grid`] : Normals
    - COAST [`output shapes`] : Coastline
    - NORMALS [`output shapes`] : Coastline-Normal Profiles
    - CLIFF_TOP [`output shapes`] : Cliff Top Points
    - CLIFF_TOE [`output shapes`] : Cliff Toe Points
    - COAST_POINT [`output shapes`] : Coast Points
    - INVALID_NORMALS [`output shapes`] : Invalid Coastline-Normal Profiles
    - COAST_CURVATURE [`output shapes`] : Coastline Curvature
    - PROFILES [`output table`] : Profile Data
    - COASTSEAHANDINESS [`choice`] : Sea handiness. Available Choices: [0] right [1] left Default: 0 as you traverse the coastline, on which side of shoreline the sea is?
    - STARTEDGEUSERCOASTLINE [`choice`] : Start edge coastline. Available Choices: [0] North [1] East [2] South [3] West Default: 0 on which edge of the DTM the start of coastline is?
    - ENDEDGEUSERCOASTLINE [`choice`] : End edge coastline. Available Choices: [0] North [1] East [2] South [3] West Default: 0 on which edge of the DTM the end of coastline is?
    - STILLWATERLEVEL [`floating point number`] : Still Water Level. Minimum: 0.000000 Default: 1.000000 Still water level (m) used to extract the shoreline.
    - COASTSMOOTH [`choice`] : Coastline Smoothing Algorithm. Available Choices: [0] none [1] running mean [2] Savitsky-Golay Default: 1 Vector coastline smoothing algorithm.
    - COASTSMOOTHWINDOW [`integer number`] : Coastline Smoothing Window Size. Minimum: 1 Default: 30 Size of coastline smoothing window, resulting kernel size will be: 1 + 2 * size.
    - SAVGOLCOASTPOLY [`integer number`] : Polynomial Order for Savitsky-Golay. Minimum: 1 Maximum: 6 Default: 4 Order of coastline profile smoothing polynomial for Savitsky-Golay smoothing: usually 2 or 4, max is 6.
    - SCALERASTEROUTPUT [`boolean`] : Scale Raster Output Values. Default: 1 If needed, scale GIS raster output values.
    - RANDOMCOASTEDGESEARCH [`boolean`] : Random Edge for Coastline Search. Default: 1 Random edge for coastline search.
    - COASTNORMALLENGTH [`floating point number`] : Length of Coastline Normals. Minimum: 0.010000 Default: 500.000000 Length of coastline normals (m).
    - ELETOLERANCE [`floating point number`] : Vertical Tolerance. Minimum: 0.010000 Default: 0.500000 Vertical tolerance to avoid false cliff tops and toes.
    - OUTPATH [`file path`] : Main Output File Directory

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_cliffmetrics', '0', 'CliffMetrics')
    if Tool.is_Okay():
        Tool.Set_Input ('DEM', DEM)
        Tool.Set_Input ('COAST_INITIAL', COAST_INITIAL)
        Tool.Set_Output('SEDIMENT_TOP', SEDIMENT_TOP)
        Tool.Set_Output('RASTER_COAST', RASTER_COAST)
        Tool.Set_Output('RASTER_NORMAL', RASTER_NORMAL)
        Tool.Set_Output('COAST', COAST)
        Tool.Set_Output('NORMALS', NORMALS)
        Tool.Set_Output('CLIFF_TOP', CLIFF_TOP)
        Tool.Set_Output('CLIFF_TOE', CLIFF_TOE)
        Tool.Set_Output('COAST_POINT', COAST_POINT)
        Tool.Set_Output('INVALID_NORMALS', INVALID_NORMALS)
        Tool.Set_Output('COAST_CURVATURE', COAST_CURVATURE)
        Tool.Set_Output('PROFILES', PROFILES)
        Tool.Set_Option('CoastSeaHandiness', COASTSEAHANDINESS)
        Tool.Set_Option('StartEdgeUserCoastLine', STARTEDGEUSERCOASTLINE)
        Tool.Set_Option('EndEdgeUserCoastLine', ENDEDGEUSERCOASTLINE)
        Tool.Set_Option('StillWaterLevel', STILLWATERLEVEL)
        Tool.Set_Option('CoastSmooth', COASTSMOOTH)
        Tool.Set_Option('CoastSmoothWindow', COASTSMOOTHWINDOW)
        Tool.Set_Option('SavGolCoastPoly', SAVGOLCOASTPOLY)
        Tool.Set_Option('ScaleRasterOutput', SCALERASTEROUTPUT)
        Tool.Set_Option('RandomCoastEdgeSearch', RANDOMCOASTEDGESEARCH)
        Tool.Set_Option('CoastNormalLength', COASTNORMALLENGTH)
        Tool.Set_Option('EleTolerance', ELETOLERANCE)
        Tool.Set_Option('OutPath', OUTPATH)
        return Tool.Execute(Verbose)
    return False

def run_tool_ta_cliffmetrics_0(DEM=None, COAST_INITIAL=None, SEDIMENT_TOP=None, RASTER_COAST=None, RASTER_NORMAL=None, COAST=None, NORMALS=None, CLIFF_TOP=None, CLIFF_TOE=None, COAST_POINT=None, INVALID_NORMALS=None, COAST_CURVATURE=None, PROFILES=None, COASTSEAHANDINESS=None, STARTEDGEUSERCOASTLINE=None, ENDEDGEUSERCOASTLINE=None, STILLWATERLEVEL=None, COASTSMOOTH=None, COASTSMOOTHWINDOW=None, SAVGOLCOASTPOLY=None, SCALERASTEROUTPUT=None, RANDOMCOASTEDGESEARCH=None, COASTNORMALLENGTH=None, ELETOLERANCE=None, OUTPATH=None, Verbose=2):
    '''
    CliffMetrics
    ----------
    [ta_cliffmetrics.0]\n
    CliffMetrics (Automatic Cliff Metrics delineation) delineates the location of the coastline, coastline normals, and cliff top and toe location along these normals.\n
    Arguments
    ----------
    - DEM [`input grid`] : Elevation
    - COAST_INITIAL [`optional input shapes`] : User Defined Coastline Points. if non empty, cliffmetric will use the user defined coastal line to draw the transects
    - SEDIMENT_TOP [`output grid`] : Sediment Top Elevation
    - RASTER_COAST [`output grid`] : Coastline
    - RASTER_NORMAL [`output grid`] : Normals
    - COAST [`output shapes`] : Coastline
    - NORMALS [`output shapes`] : Coastline-Normal Profiles
    - CLIFF_TOP [`output shapes`] : Cliff Top Points
    - CLIFF_TOE [`output shapes`] : Cliff Toe Points
    - COAST_POINT [`output shapes`] : Coast Points
    - INVALID_NORMALS [`output shapes`] : Invalid Coastline-Normal Profiles
    - COAST_CURVATURE [`output shapes`] : Coastline Curvature
    - PROFILES [`output table`] : Profile Data
    - COASTSEAHANDINESS [`choice`] : Sea handiness. Available Choices: [0] right [1] left Default: 0 as you traverse the coastline, on which side of shoreline the sea is?
    - STARTEDGEUSERCOASTLINE [`choice`] : Start edge coastline. Available Choices: [0] North [1] East [2] South [3] West Default: 0 on which edge of the DTM the start of coastline is?
    - ENDEDGEUSERCOASTLINE [`choice`] : End edge coastline. Available Choices: [0] North [1] East [2] South [3] West Default: 0 on which edge of the DTM the end of coastline is?
    - STILLWATERLEVEL [`floating point number`] : Still Water Level. Minimum: 0.000000 Default: 1.000000 Still water level (m) used to extract the shoreline.
    - COASTSMOOTH [`choice`] : Coastline Smoothing Algorithm. Available Choices: [0] none [1] running mean [2] Savitsky-Golay Default: 1 Vector coastline smoothing algorithm.
    - COASTSMOOTHWINDOW [`integer number`] : Coastline Smoothing Window Size. Minimum: 1 Default: 30 Size of coastline smoothing window, resulting kernel size will be: 1 + 2 * size.
    - SAVGOLCOASTPOLY [`integer number`] : Polynomial Order for Savitsky-Golay. Minimum: 1 Maximum: 6 Default: 4 Order of coastline profile smoothing polynomial for Savitsky-Golay smoothing: usually 2 or 4, max is 6.
    - SCALERASTEROUTPUT [`boolean`] : Scale Raster Output Values. Default: 1 If needed, scale GIS raster output values.
    - RANDOMCOASTEDGESEARCH [`boolean`] : Random Edge for Coastline Search. Default: 1 Random edge for coastline search.
    - COASTNORMALLENGTH [`floating point number`] : Length of Coastline Normals. Minimum: 0.010000 Default: 500.000000 Length of coastline normals (m).
    - ELETOLERANCE [`floating point number`] : Vertical Tolerance. Minimum: 0.010000 Default: 0.500000 Vertical tolerance to avoid false cliff tops and toes.
    - OUTPATH [`file path`] : Main Output File Directory

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_cliffmetrics', '0', 'CliffMetrics')
    if Tool.is_Okay():
        Tool.Set_Input ('DEM', DEM)
        Tool.Set_Input ('COAST_INITIAL', COAST_INITIAL)
        Tool.Set_Output('SEDIMENT_TOP', SEDIMENT_TOP)
        Tool.Set_Output('RASTER_COAST', RASTER_COAST)
        Tool.Set_Output('RASTER_NORMAL', RASTER_NORMAL)
        Tool.Set_Output('COAST', COAST)
        Tool.Set_Output('NORMALS', NORMALS)
        Tool.Set_Output('CLIFF_TOP', CLIFF_TOP)
        Tool.Set_Output('CLIFF_TOE', CLIFF_TOE)
        Tool.Set_Output('COAST_POINT', COAST_POINT)
        Tool.Set_Output('INVALID_NORMALS', INVALID_NORMALS)
        Tool.Set_Output('COAST_CURVATURE', COAST_CURVATURE)
        Tool.Set_Output('PROFILES', PROFILES)
        Tool.Set_Option('CoastSeaHandiness', COASTSEAHANDINESS)
        Tool.Set_Option('StartEdgeUserCoastLine', STARTEDGEUSERCOASTLINE)
        Tool.Set_Option('EndEdgeUserCoastLine', ENDEDGEUSERCOASTLINE)
        Tool.Set_Option('StillWaterLevel', STILLWATERLEVEL)
        Tool.Set_Option('CoastSmooth', COASTSMOOTH)
        Tool.Set_Option('CoastSmoothWindow', COASTSMOOTHWINDOW)
        Tool.Set_Option('SavGolCoastPoly', SAVGOLCOASTPOLY)
        Tool.Set_Option('ScaleRasterOutput', SCALERASTEROUTPUT)
        Tool.Set_Option('RandomCoastEdgeSearch', RANDOMCOASTEDGESEARCH)
        Tool.Set_Option('CoastNormalLength', COASTNORMALLENGTH)
        Tool.Set_Option('EleTolerance', ELETOLERANCE)
        Tool.Set_Option('OutPath', OUTPATH)
        return Tool.Execute(Verbose)
    return False

def Coastal_Profile_Crossings(LINES_SEASIDE=None, LINES_LANDSIDE=None, LINES_COAST=None, CROSSINGS_SEASIDE=None, CROSSINGS_LANDSIDE=None, DISTANCES=None, ATTRIBUTES=None, Verbose=2):
    '''
    Coastal Profile Crossings
    ----------
    [ta_cliffmetrics.1]\n
    The Coastal Profile Crossings tool identifies the crossing points between coastal profiles along a reference coastline (from CliffMetrics Normal outputs) and any other coast lines and calculates the distance and coastline differences metrics.\n
    Arguments
    ----------
    - LINES_SEASIDE [`input shapes`] : SeaSide Profile Lines Layer
    - LINES_LANDSIDE [`input shapes`] : LandSide Profile Lines Layer
    - LINES_COAST [`input shapes`] : Coast Lines Layer
    - CROSSINGS_SEASIDE [`output shapes`] : Crossings at Sea Side
    - CROSSINGS_LANDSIDE [`output shapes`] : Crossings at Land Side
    - DISTANCES [`output shapes`] : Distances to profile start point. Distances > 0 if towards sea side (acretion) and < 0 if towards landside (erosion)
    - ATTRIBUTES [`choice`] : Parent Attributes. Available Choices: [0] index [1] attributes [2] index and attributes Default: 0 attributes inherited by parent lines layers

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_cliffmetrics', '1', 'Coastal Profile Crossings')
    if Tool.is_Okay():
        Tool.Set_Input ('LINES_SeaSide', LINES_SEASIDE)
        Tool.Set_Input ('LINES_LandSide', LINES_LANDSIDE)
        Tool.Set_Input ('LINES_Coast', LINES_COAST)
        Tool.Set_Output('CROSSINGS_SEASIDE', CROSSINGS_SEASIDE)
        Tool.Set_Output('CROSSINGS_LANDSIDE', CROSSINGS_LANDSIDE)
        Tool.Set_Output('DISTANCES', DISTANCES)
        Tool.Set_Option('ATTRIBUTES', ATTRIBUTES)
        return Tool.Execute(Verbose)
    return False

def run_tool_ta_cliffmetrics_1(LINES_SEASIDE=None, LINES_LANDSIDE=None, LINES_COAST=None, CROSSINGS_SEASIDE=None, CROSSINGS_LANDSIDE=None, DISTANCES=None, ATTRIBUTES=None, Verbose=2):
    '''
    Coastal Profile Crossings
    ----------
    [ta_cliffmetrics.1]\n
    The Coastal Profile Crossings tool identifies the crossing points between coastal profiles along a reference coastline (from CliffMetrics Normal outputs) and any other coast lines and calculates the distance and coastline differences metrics.\n
    Arguments
    ----------
    - LINES_SEASIDE [`input shapes`] : SeaSide Profile Lines Layer
    - LINES_LANDSIDE [`input shapes`] : LandSide Profile Lines Layer
    - LINES_COAST [`input shapes`] : Coast Lines Layer
    - CROSSINGS_SEASIDE [`output shapes`] : Crossings at Sea Side
    - CROSSINGS_LANDSIDE [`output shapes`] : Crossings at Land Side
    - DISTANCES [`output shapes`] : Distances to profile start point. Distances > 0 if towards sea side (acretion) and < 0 if towards landside (erosion)
    - ATTRIBUTES [`choice`] : Parent Attributes. Available Choices: [0] index [1] attributes [2] index and attributes Default: 0 attributes inherited by parent lines layers

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('ta_cliffmetrics', '1', 'Coastal Profile Crossings')
    if Tool.is_Okay():
        Tool.Set_Input ('LINES_SeaSide', LINES_SEASIDE)
        Tool.Set_Input ('LINES_LandSide', LINES_LANDSIDE)
        Tool.Set_Input ('LINES_Coast', LINES_COAST)
        Tool.Set_Output('CROSSINGS_SEASIDE', CROSSINGS_SEASIDE)
        Tool.Set_Output('CROSSINGS_LANDSIDE', CROSSINGS_LANDSIDE)
        Tool.Set_Output('DISTANCES', DISTANCES)
        Tool.Set_Option('ATTRIBUTES', ATTRIBUTES)
        return Tool.Execute(Verbose)
    return False

