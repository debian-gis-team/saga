#! /usr/bin/env python

'''
Python Interface to SAGA Tools Library
----------
- Category : Imagery
- Name     : Maximum Entropy
- ID       : imagery_maxent

Description
----------
Maximum entropy based classification and prediction.
'''

from PySAGA.helper import Tool_Wrapper

def Maximum_Entropy_Classification(TRAINING=None, FEATURES_NUM=None, FEATURES_CAT=None, CLASSES=None, CLASSES_LUT=None, PROB=None, PROBS=None, FIELD=None, PROBS_CREATE=None, METHOD=None, YT_FILE_LOAD=None, YT_FILE_SAVE=None, YT_REGUL=None, YT_REGUL_VAL=None, YT_NUMASREAL=None, DL_ALPHA=None, DL_THRESHOLD=None, DL_ITERATIONS=None, NUM_CLASSES=None, PROB_MIN=None, Verbose=2):
    '''
    Maximum Entropy Classification
    ----------
    [imagery_maxent.0]\n
    Maximum Entropy Classification\n
    Arguments
    ----------
    - TRAINING [`input shapes`] : Training Areas
    - FEATURES_NUM [`optional input grid list`] : Numerical Features
    - FEATURES_CAT [`optional input grid list`] : Categorical Features
    - CLASSES [`output grid`] : Classification
    - CLASSES_LUT [`output table`] : Look-up Table. A reference list of the grid values that have been assigned to the training classes.
    - PROB [`output grid`] : Probability
    - PROBS [`output grid list`] : Probabilities
    - FIELD [`table field`] : Class Name
    - PROBS_CREATE [`boolean`] : Create Probabilities. Default: 0
    - METHOD [`choice`] : Method. Available Choices: [0] Yoshimasa Tsuruoka [1] Dekang Lin Default: 0
    - YT_FILE_LOAD [`file path`] : Load from File...
    - YT_FILE_SAVE [`file path`] : Save to File...
    - YT_REGUL [`choice`] : Regularization. Available Choices: [0] none [1] L1 [2] L2 Default: 0
    - YT_REGUL_VAL [`floating point number`] : Regularization Factor. Minimum: 0.000000 Default: 1.000000
    - YT_NUMASREAL [`boolean`] : Real-valued Numerical Features. Default: 1
    - DL_ALPHA [`floating point number`] : Alpha. Default: 0.100000
    - DL_THRESHOLD [`floating point number`] : Threshold. Minimum: 0.000000 Default: 0.000000
    - DL_ITERATIONS [`integer number`] : Maximum Iterations. Minimum: 1 Default: 100
    - NUM_CLASSES [`integer number`] : Number of Numeric Value Classes. Minimum: 1 Default: 32
    - PROB_MIN [`floating point number`] : Minimum Probability. Minimum: 0.000000 Maximum: 1.000000 Default: 0.000000 Minimum probability to accept a classification result for a cell.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('imagery_maxent', '0', 'Maximum Entropy Classification')
    if Tool.is_Okay():
        Tool.Set_Input ('TRAINING', TRAINING)
        Tool.Set_Input ('FEATURES_NUM', FEATURES_NUM)
        Tool.Set_Input ('FEATURES_CAT', FEATURES_CAT)
        Tool.Set_Output('CLASSES', CLASSES)
        Tool.Set_Output('CLASSES_LUT', CLASSES_LUT)
        Tool.Set_Output('PROB', PROB)
        Tool.Set_Output('PROBS', PROBS)
        Tool.Set_Option('FIELD', FIELD)
        Tool.Set_Option('PROBS_CREATE', PROBS_CREATE)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('YT_FILE_LOAD', YT_FILE_LOAD)
        Tool.Set_Option('YT_FILE_SAVE', YT_FILE_SAVE)
        Tool.Set_Option('YT_REGUL', YT_REGUL)
        Tool.Set_Option('YT_REGUL_VAL', YT_REGUL_VAL)
        Tool.Set_Option('YT_NUMASREAL', YT_NUMASREAL)
        Tool.Set_Option('DL_ALPHA', DL_ALPHA)
        Tool.Set_Option('DL_THRESHOLD', DL_THRESHOLD)
        Tool.Set_Option('DL_ITERATIONS', DL_ITERATIONS)
        Tool.Set_Option('NUM_CLASSES', NUM_CLASSES)
        Tool.Set_Option('PROB_MIN', PROB_MIN)
        return Tool.Execute(Verbose)
    return False

def run_tool_imagery_maxent_0(TRAINING=None, FEATURES_NUM=None, FEATURES_CAT=None, CLASSES=None, CLASSES_LUT=None, PROB=None, PROBS=None, FIELD=None, PROBS_CREATE=None, METHOD=None, YT_FILE_LOAD=None, YT_FILE_SAVE=None, YT_REGUL=None, YT_REGUL_VAL=None, YT_NUMASREAL=None, DL_ALPHA=None, DL_THRESHOLD=None, DL_ITERATIONS=None, NUM_CLASSES=None, PROB_MIN=None, Verbose=2):
    '''
    Maximum Entropy Classification
    ----------
    [imagery_maxent.0]\n
    Maximum Entropy Classification\n
    Arguments
    ----------
    - TRAINING [`input shapes`] : Training Areas
    - FEATURES_NUM [`optional input grid list`] : Numerical Features
    - FEATURES_CAT [`optional input grid list`] : Categorical Features
    - CLASSES [`output grid`] : Classification
    - CLASSES_LUT [`output table`] : Look-up Table. A reference list of the grid values that have been assigned to the training classes.
    - PROB [`output grid`] : Probability
    - PROBS [`output grid list`] : Probabilities
    - FIELD [`table field`] : Class Name
    - PROBS_CREATE [`boolean`] : Create Probabilities. Default: 0
    - METHOD [`choice`] : Method. Available Choices: [0] Yoshimasa Tsuruoka [1] Dekang Lin Default: 0
    - YT_FILE_LOAD [`file path`] : Load from File...
    - YT_FILE_SAVE [`file path`] : Save to File...
    - YT_REGUL [`choice`] : Regularization. Available Choices: [0] none [1] L1 [2] L2 Default: 0
    - YT_REGUL_VAL [`floating point number`] : Regularization Factor. Minimum: 0.000000 Default: 1.000000
    - YT_NUMASREAL [`boolean`] : Real-valued Numerical Features. Default: 1
    - DL_ALPHA [`floating point number`] : Alpha. Default: 0.100000
    - DL_THRESHOLD [`floating point number`] : Threshold. Minimum: 0.000000 Default: 0.000000
    - DL_ITERATIONS [`integer number`] : Maximum Iterations. Minimum: 1 Default: 100
    - NUM_CLASSES [`integer number`] : Number of Numeric Value Classes. Minimum: 1 Default: 32
    - PROB_MIN [`floating point number`] : Minimum Probability. Minimum: 0.000000 Maximum: 1.000000 Default: 0.000000 Minimum probability to accept a classification result for a cell.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('imagery_maxent', '0', 'Maximum Entropy Classification')
    if Tool.is_Okay():
        Tool.Set_Input ('TRAINING', TRAINING)
        Tool.Set_Input ('FEATURES_NUM', FEATURES_NUM)
        Tool.Set_Input ('FEATURES_CAT', FEATURES_CAT)
        Tool.Set_Output('CLASSES', CLASSES)
        Tool.Set_Output('CLASSES_LUT', CLASSES_LUT)
        Tool.Set_Output('PROB', PROB)
        Tool.Set_Output('PROBS', PROBS)
        Tool.Set_Option('FIELD', FIELD)
        Tool.Set_Option('PROBS_CREATE', PROBS_CREATE)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('YT_FILE_LOAD', YT_FILE_LOAD)
        Tool.Set_Option('YT_FILE_SAVE', YT_FILE_SAVE)
        Tool.Set_Option('YT_REGUL', YT_REGUL)
        Tool.Set_Option('YT_REGUL_VAL', YT_REGUL_VAL)
        Tool.Set_Option('YT_NUMASREAL', YT_NUMASREAL)
        Tool.Set_Option('DL_ALPHA', DL_ALPHA)
        Tool.Set_Option('DL_THRESHOLD', DL_THRESHOLD)
        Tool.Set_Option('DL_ITERATIONS', DL_ITERATIONS)
        Tool.Set_Option('NUM_CLASSES', NUM_CLASSES)
        Tool.Set_Option('PROB_MIN', PROB_MIN)
        return Tool.Execute(Verbose)
    return False

def Maximum_Entropy_Presence_Prediction(PRESENCE=None, FEATURES_NUM=None, FEATURES_CAT=None, PREDICTION=None, PROBABILITY=None, BACKGROUND=None, METHOD=None, YT_FILE_LOAD=None, YT_FILE_SAVE=None, YT_REGUL=None, YT_REGUL_VAL=None, YT_NUMASREAL=None, DL_ALPHA=None, DL_THRESHOLD=None, DL_ITERATIONS=None, NUM_CLASSES=None, Verbose=2):
    '''
    Maximum Entropy Presence Prediction
    ----------
    [imagery_maxent.1]\n
    Maximum Entropy Presence Prediction\n
    Arguments
    ----------
    - PRESENCE [`input shapes`] : Presence Data
    - FEATURES_NUM [`optional input grid list`] : Numerical Features
    - FEATURES_CAT [`optional input grid list`] : Categorical Features
    - PREDICTION [`output grid`] : Presence Prediction
    - PROBABILITY [`output grid`] : Presence Probability
    - BACKGROUND [`floating point number`] : Background Sample Density [Percent]. Minimum: 0.000000 Maximum: 100.000000 Default: 1.000000
    - METHOD [`choice`] : Method. Available Choices: [0] Yoshimasa Tsuruoka [1] Dekang Lin Default: 0
    - YT_FILE_LOAD [`file path`] : Load from File...
    - YT_FILE_SAVE [`file path`] : Save to File...
    - YT_REGUL [`choice`] : Regularization. Available Choices: [0] none [1] L1 [2] L2 Default: 1
    - YT_REGUL_VAL [`floating point number`] : Regularization Factor. Minimum: 0.000000 Default: 1.000000
    - YT_NUMASREAL [`boolean`] : Real-valued Numerical Features. Default: 1
    - DL_ALPHA [`floating point number`] : Alpha. Default: 0.100000
    - DL_THRESHOLD [`floating point number`] : Threshold. Minimum: 0.000000 Default: 0.000000
    - DL_ITERATIONS [`integer number`] : Maximum Iterations. Minimum: 1 Default: 100
    - NUM_CLASSES [`integer number`] : Number of Numeric Value Classes. Minimum: 1 Default: 32

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('imagery_maxent', '1', 'Maximum Entropy Presence Prediction')
    if Tool.is_Okay():
        Tool.Set_Input ('PRESENCE', PRESENCE)
        Tool.Set_Input ('FEATURES_NUM', FEATURES_NUM)
        Tool.Set_Input ('FEATURES_CAT', FEATURES_CAT)
        Tool.Set_Output('PREDICTION', PREDICTION)
        Tool.Set_Output('PROBABILITY', PROBABILITY)
        Tool.Set_Option('BACKGROUND', BACKGROUND)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('YT_FILE_LOAD', YT_FILE_LOAD)
        Tool.Set_Option('YT_FILE_SAVE', YT_FILE_SAVE)
        Tool.Set_Option('YT_REGUL', YT_REGUL)
        Tool.Set_Option('YT_REGUL_VAL', YT_REGUL_VAL)
        Tool.Set_Option('YT_NUMASREAL', YT_NUMASREAL)
        Tool.Set_Option('DL_ALPHA', DL_ALPHA)
        Tool.Set_Option('DL_THRESHOLD', DL_THRESHOLD)
        Tool.Set_Option('DL_ITERATIONS', DL_ITERATIONS)
        Tool.Set_Option('NUM_CLASSES', NUM_CLASSES)
        return Tool.Execute(Verbose)
    return False

def run_tool_imagery_maxent_1(PRESENCE=None, FEATURES_NUM=None, FEATURES_CAT=None, PREDICTION=None, PROBABILITY=None, BACKGROUND=None, METHOD=None, YT_FILE_LOAD=None, YT_FILE_SAVE=None, YT_REGUL=None, YT_REGUL_VAL=None, YT_NUMASREAL=None, DL_ALPHA=None, DL_THRESHOLD=None, DL_ITERATIONS=None, NUM_CLASSES=None, Verbose=2):
    '''
    Maximum Entropy Presence Prediction
    ----------
    [imagery_maxent.1]\n
    Maximum Entropy Presence Prediction\n
    Arguments
    ----------
    - PRESENCE [`input shapes`] : Presence Data
    - FEATURES_NUM [`optional input grid list`] : Numerical Features
    - FEATURES_CAT [`optional input grid list`] : Categorical Features
    - PREDICTION [`output grid`] : Presence Prediction
    - PROBABILITY [`output grid`] : Presence Probability
    - BACKGROUND [`floating point number`] : Background Sample Density [Percent]. Minimum: 0.000000 Maximum: 100.000000 Default: 1.000000
    - METHOD [`choice`] : Method. Available Choices: [0] Yoshimasa Tsuruoka [1] Dekang Lin Default: 0
    - YT_FILE_LOAD [`file path`] : Load from File...
    - YT_FILE_SAVE [`file path`] : Save to File...
    - YT_REGUL [`choice`] : Regularization. Available Choices: [0] none [1] L1 [2] L2 Default: 1
    - YT_REGUL_VAL [`floating point number`] : Regularization Factor. Minimum: 0.000000 Default: 1.000000
    - YT_NUMASREAL [`boolean`] : Real-valued Numerical Features. Default: 1
    - DL_ALPHA [`floating point number`] : Alpha. Default: 0.100000
    - DL_THRESHOLD [`floating point number`] : Threshold. Minimum: 0.000000 Default: 0.000000
    - DL_ITERATIONS [`integer number`] : Maximum Iterations. Minimum: 1 Default: 100
    - NUM_CLASSES [`integer number`] : Number of Numeric Value Classes. Minimum: 1 Default: 32

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('imagery_maxent', '1', 'Maximum Entropy Presence Prediction')
    if Tool.is_Okay():
        Tool.Set_Input ('PRESENCE', PRESENCE)
        Tool.Set_Input ('FEATURES_NUM', FEATURES_NUM)
        Tool.Set_Input ('FEATURES_CAT', FEATURES_CAT)
        Tool.Set_Output('PREDICTION', PREDICTION)
        Tool.Set_Output('PROBABILITY', PROBABILITY)
        Tool.Set_Option('BACKGROUND', BACKGROUND)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('YT_FILE_LOAD', YT_FILE_LOAD)
        Tool.Set_Option('YT_FILE_SAVE', YT_FILE_SAVE)
        Tool.Set_Option('YT_REGUL', YT_REGUL)
        Tool.Set_Option('YT_REGUL_VAL', YT_REGUL_VAL)
        Tool.Set_Option('YT_NUMASREAL', YT_NUMASREAL)
        Tool.Set_Option('DL_ALPHA', DL_ALPHA)
        Tool.Set_Option('DL_THRESHOLD', DL_THRESHOLD)
        Tool.Set_Option('DL_ITERATIONS', DL_ITERATIONS)
        Tool.Set_Option('NUM_CLASSES', NUM_CLASSES)
        return Tool.Execute(Verbose)
    return False

