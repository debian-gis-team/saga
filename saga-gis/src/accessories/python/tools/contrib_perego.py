#! /usr/bin/env python

'''
Python Interface to SAGA Tools Library
----------
- Category : Grid
- Name     : Filter (Perego 2009)
- ID       : contrib_perego

Description
----------
Contributions from Alessandro Perego. Go to [www.alspergis.altervista.org](http://www.alspergis.altervista.org/software/software.html#saga) for further information.
'''

from PySAGA.helper import Tool_Wrapper

def Average_With_Threshold_1(INPUT=None, RESULT=None, RX=None, RY=None, THRESH=None, Verbose=2):
    '''
    Average With Threshold 1
    ----------
    [contrib_perego.0]\n
    Average With Threshold for Grids calculates average in X and Y distances using only the values that differ form central pixel less than a specified threshold. It's useful to remove noise with a known maximum reducing the loss of information\n
    Arguments
    ----------
    - INPUT [`input grid`] : Input. This must be your input data of type grid.
    - RESULT [`output grid`] : AWT Grid. New grid filtered with the A1WiTh tool
    - RX [`integer number`] : Radius X. Minimum: 1 Default: 1
    - RY [`integer number`] : Radius Y. Minimum: 1 Default: 1
    - THRESH [`floating point number`] : Threshold. Default: 2.000000 The values in the specified radius is used in the average calculation only if its difference with the central value is lesser or equal to this threshold.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('contrib_perego', '0', 'Average With Threshold 1')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Output('RESULT', RESULT)
        Tool.Set_Option('RX', RX)
        Tool.Set_Option('RY', RY)
        Tool.Set_Option('THRESH', THRESH)
        return Tool.Execute(Verbose)
    return False

def run_tool_contrib_perego_0(INPUT=None, RESULT=None, RX=None, RY=None, THRESH=None, Verbose=2):
    '''
    Average With Threshold 1
    ----------
    [contrib_perego.0]\n
    Average With Threshold for Grids calculates average in X and Y distances using only the values that differ form central pixel less than a specified threshold. It's useful to remove noise with a known maximum reducing the loss of information\n
    Arguments
    ----------
    - INPUT [`input grid`] : Input. This must be your input data of type grid.
    - RESULT [`output grid`] : AWT Grid. New grid filtered with the A1WiTh tool
    - RX [`integer number`] : Radius X. Minimum: 1 Default: 1
    - RY [`integer number`] : Radius Y. Minimum: 1 Default: 1
    - THRESH [`floating point number`] : Threshold. Default: 2.000000 The values in the specified radius is used in the average calculation only if its difference with the central value is lesser or equal to this threshold.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('contrib_perego', '0', 'Average With Threshold 1')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Output('RESULT', RESULT)
        Tool.Set_Option('RX', RX)
        Tool.Set_Option('RY', RY)
        Tool.Set_Option('THRESH', THRESH)
        return Tool.Execute(Verbose)
    return False

def Average_With_Threshold_2(INPUT=None, RESULT=None, RX=None, RY=None, THRESH=None, Verbose=2):
    '''
    Average With Threshold 2
    ----------
    [contrib_perego.1]\n
    Average 2 With Threshold for Grids calculates average in X and Y distances using only the values that differ form central pixel less than a specified threshold. Each value has a weight which is inversely proportional to the distance (method 1).\n
    Arguments
    ----------
    - INPUT [`input grid`] : Input. This must be your input data of type grid.
    - RESULT [`output grid`] : AWT Grid. New grid filtered with the A2WiTh tool
    - RX [`integer number`] : Radius X. Minimum: 1 Default: 1
    - RY [`integer number`] : Radius Y. Minimum: 1 Default: 1
    - THRESH [`floating point number`] : Threshold. Default: 2.000000 The values in the specified radius is used in the average calculation only if its difference with the central value is lesser or equal to this threshold.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('contrib_perego', '1', 'Average With Threshold 2')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Output('RESULT', RESULT)
        Tool.Set_Option('RX', RX)
        Tool.Set_Option('RY', RY)
        Tool.Set_Option('THRESH', THRESH)
        return Tool.Execute(Verbose)
    return False

def run_tool_contrib_perego_1(INPUT=None, RESULT=None, RX=None, RY=None, THRESH=None, Verbose=2):
    '''
    Average With Threshold 2
    ----------
    [contrib_perego.1]\n
    Average 2 With Threshold for Grids calculates average in X and Y distances using only the values that differ form central pixel less than a specified threshold. Each value has a weight which is inversely proportional to the distance (method 1).\n
    Arguments
    ----------
    - INPUT [`input grid`] : Input. This must be your input data of type grid.
    - RESULT [`output grid`] : AWT Grid. New grid filtered with the A2WiTh tool
    - RX [`integer number`] : Radius X. Minimum: 1 Default: 1
    - RY [`integer number`] : Radius Y. Minimum: 1 Default: 1
    - THRESH [`floating point number`] : Threshold. Default: 2.000000 The values in the specified radius is used in the average calculation only if its difference with the central value is lesser or equal to this threshold.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('contrib_perego', '1', 'Average With Threshold 2')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Output('RESULT', RESULT)
        Tool.Set_Option('RX', RX)
        Tool.Set_Option('RY', RY)
        Tool.Set_Option('THRESH', THRESH)
        return Tool.Execute(Verbose)
    return False

def Average_With_Threshold_3(INPUT=None, RESULT=None, RX=None, RY=None, THRESH=None, Verbose=2):
    '''
    Average With Threshold 3
    ----------
    [contrib_perego.2]\n
    Average 3 With Threshold for Grids calculates average in X and Y distances using only the values that differ form central pixel less than a specified threshold. Each value has a weight which is inversely proportional to the distance (method 2).\n
    Arguments
    ----------
    - INPUT [`input grid`] : Input. This must be your input data of type grid.
    - RESULT [`output grid`] : AWT Grid. New grid filtered with the A3WiTh tool
    - RX [`integer number`] : Radius X. Minimum: 1 Default: 1
    - RY [`integer number`] : Radius Y. Minimum: 1 Default: 1
    - THRESH [`floating point number`] : Threshold. Default: 2.000000 The values in the specified radius is used in the average calculation only if its difference with the central value is lesser or equal to this threshold.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('contrib_perego', '2', 'Average With Threshold 3')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Output('RESULT', RESULT)
        Tool.Set_Option('RX', RX)
        Tool.Set_Option('RY', RY)
        Tool.Set_Option('THRESH', THRESH)
        return Tool.Execute(Verbose)
    return False

def run_tool_contrib_perego_2(INPUT=None, RESULT=None, RX=None, RY=None, THRESH=None, Verbose=2):
    '''
    Average With Threshold 3
    ----------
    [contrib_perego.2]\n
    Average 3 With Threshold for Grids calculates average in X and Y distances using only the values that differ form central pixel less than a specified threshold. Each value has a weight which is inversely proportional to the distance (method 2).\n
    Arguments
    ----------
    - INPUT [`input grid`] : Input. This must be your input data of type grid.
    - RESULT [`output grid`] : AWT Grid. New grid filtered with the A3WiTh tool
    - RX [`integer number`] : Radius X. Minimum: 1 Default: 1
    - RY [`integer number`] : Radius Y. Minimum: 1 Default: 1
    - THRESH [`floating point number`] : Threshold. Default: 2.000000 The values in the specified radius is used in the average calculation only if its difference with the central value is lesser or equal to this threshold.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('contrib_perego', '2', 'Average With Threshold 3')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Output('RESULT', RESULT)
        Tool.Set_Option('RX', RX)
        Tool.Set_Option('RY', RY)
        Tool.Set_Option('THRESH', THRESH)
        return Tool.Execute(Verbose)
    return False

def Average_With_Mask_1(INPUT=None, MASK=None, RESULT=None, V=None, RX=None, RY=None, Verbose=2):
    '''
    Average With Mask 1
    ----------
    [contrib_perego.3]\n
    Average With Mask 1 calculates average for cells specified by a mask grid. Cell excluded by the mask grid are NOT used in the average calculation.\n
    Arguments
    ----------
    - INPUT [`input grid`] : Input. This must be your input data of type grid.
    - MASK [`input grid`] : Mask Grid. This grid indicates the cells you want calculate the average.
    - RESULT [`output grid`] : AWM1 Grid. New grid filtered with the AvWiMa1 tool
    - V [`floating point number`] : Mask value. Default: 1.000000 Value of right cells in the Mask Grid
    - RX [`integer number`] : Radius X. Minimum: 1 Default: 1
    - RY [`integer number`] : Radius Y. Minimum: 1 Default: 1

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('contrib_perego', '3', 'Average With Mask 1')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Input ('MASK', MASK)
        Tool.Set_Output('RESULT', RESULT)
        Tool.Set_Option('V', V)
        Tool.Set_Option('RX', RX)
        Tool.Set_Option('RY', RY)
        return Tool.Execute(Verbose)
    return False

def run_tool_contrib_perego_3(INPUT=None, MASK=None, RESULT=None, V=None, RX=None, RY=None, Verbose=2):
    '''
    Average With Mask 1
    ----------
    [contrib_perego.3]\n
    Average With Mask 1 calculates average for cells specified by a mask grid. Cell excluded by the mask grid are NOT used in the average calculation.\n
    Arguments
    ----------
    - INPUT [`input grid`] : Input. This must be your input data of type grid.
    - MASK [`input grid`] : Mask Grid. This grid indicates the cells you want calculate the average.
    - RESULT [`output grid`] : AWM1 Grid. New grid filtered with the AvWiMa1 tool
    - V [`floating point number`] : Mask value. Default: 1.000000 Value of right cells in the Mask Grid
    - RX [`integer number`] : Radius X. Minimum: 1 Default: 1
    - RY [`integer number`] : Radius Y. Minimum: 1 Default: 1

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('contrib_perego', '3', 'Average With Mask 1')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Input ('MASK', MASK)
        Tool.Set_Output('RESULT', RESULT)
        Tool.Set_Option('V', V)
        Tool.Set_Option('RX', RX)
        Tool.Set_Option('RY', RY)
        return Tool.Execute(Verbose)
    return False

def Average_With_Mask_2(INPUT=None, MASK=None, RESULT=None, V=None, RX=None, RY=None, Verbose=2):
    '''
    Average With Mask 2
    ----------
    [contrib_perego.4]\n
    Average With Mask 2 calculates average for cells specified by a mask grid. However cell excluded by the mask grid are used in the average calculation for right pixels.\n
    Arguments
    ----------
    - INPUT [`input grid`] : Input. This must be your input data of type grid.
    - MASK [`input grid`] : Mask Grid. This grid indicates the cells you want calculate the average.
    - RESULT [`output grid`] : AWM2 Grid. New grid filtered with the AvWiMa2 tool
    - V [`floating point number`] : Mask value. Default: 1.000000 Value of right cells in the Mask Grid
    - RX [`integer number`] : Radius X. Minimum: 1 Default: 1
    - RY [`integer number`] : Radius Y. Minimum: 1 Default: 1

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('contrib_perego', '4', 'Average With Mask 2')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Input ('MASK', MASK)
        Tool.Set_Output('RESULT', RESULT)
        Tool.Set_Option('V', V)
        Tool.Set_Option('RX', RX)
        Tool.Set_Option('RY', RY)
        return Tool.Execute(Verbose)
    return False

def run_tool_contrib_perego_4(INPUT=None, MASK=None, RESULT=None, V=None, RX=None, RY=None, Verbose=2):
    '''
    Average With Mask 2
    ----------
    [contrib_perego.4]\n
    Average With Mask 2 calculates average for cells specified by a mask grid. However cell excluded by the mask grid are used in the average calculation for right pixels.\n
    Arguments
    ----------
    - INPUT [`input grid`] : Input. This must be your input data of type grid.
    - MASK [`input grid`] : Mask Grid. This grid indicates the cells you want calculate the average.
    - RESULT [`output grid`] : AWM2 Grid. New grid filtered with the AvWiMa2 tool
    - V [`floating point number`] : Mask value. Default: 1.000000 Value of right cells in the Mask Grid
    - RX [`integer number`] : Radius X. Minimum: 1 Default: 1
    - RY [`integer number`] : Radius Y. Minimum: 1 Default: 1

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('contrib_perego', '4', 'Average With Mask 2')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Input ('MASK', MASK)
        Tool.Set_Output('RESULT', RESULT)
        Tool.Set_Option('V', V)
        Tool.Set_Option('RX', RX)
        Tool.Set_Option('RY', RY)
        return Tool.Execute(Verbose)
    return False

def Destriping(INPUT=None, MASK=None, RESULT3=None, RESULT1=None, RESULT2=None, STRIPES=None, ANG=None, R=None, D=None, Verbose=2):
    '''
    Destriping
    ----------
    [contrib_perego.5]\n
    Destriping filter removes straight parallel stripes in raster data. It uses two low-pass filters elongated in the stripes direction; the first one is 1 pixel unit wide while the second one is wide as the striping wavelength. Their difference is the striping error which is removed from the original data to obtain the destriped DEM. This method is equivalent to that proposed by Oimoen (2000).\n
    Arguments
    ----------
    - INPUT [`input grid`] : Grid
    - MASK [`optional input grid`] : Mask. Mask's no-data cells will be excluded from processing
    - RESULT3 [`output grid`] : Destriped Grid
    - RESULT1 [`output grid`] : Low-pass 1. Step 1: low-pass of stripe
    - RESULT2 [`output grid`] : Low-pass 2. Step 2: low-pass between stripe and its surroundings
    - STRIPES [`output grid`] : Stripes. The difference between destriped and original grid
    - ANG [`floating point number`] : Angle. Default: 0.000000 [Degree], 0 = horizontal, 90 = vertical
    - R [`floating point number`] : Radius. Minimum: 1.000000 Default: 20.000000 [Cells]
    - D [`floating point number`] : Stripes Distance. Minimum: 2.000000 Default: 2.000000 [Cells]

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('contrib_perego', '5', 'Destriping')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Input ('MASK', MASK)
        Tool.Set_Output('RESULT3', RESULT3)
        Tool.Set_Output('RESULT1', RESULT1)
        Tool.Set_Output('RESULT2', RESULT2)
        Tool.Set_Output('STRIPES', STRIPES)
        Tool.Set_Option('ANG', ANG)
        Tool.Set_Option('R', R)
        Tool.Set_Option('D', D)
        return Tool.Execute(Verbose)
    return False

def run_tool_contrib_perego_5(INPUT=None, MASK=None, RESULT3=None, RESULT1=None, RESULT2=None, STRIPES=None, ANG=None, R=None, D=None, Verbose=2):
    '''
    Destriping
    ----------
    [contrib_perego.5]\n
    Destriping filter removes straight parallel stripes in raster data. It uses two low-pass filters elongated in the stripes direction; the first one is 1 pixel unit wide while the second one is wide as the striping wavelength. Their difference is the striping error which is removed from the original data to obtain the destriped DEM. This method is equivalent to that proposed by Oimoen (2000).\n
    Arguments
    ----------
    - INPUT [`input grid`] : Grid
    - MASK [`optional input grid`] : Mask. Mask's no-data cells will be excluded from processing
    - RESULT3 [`output grid`] : Destriped Grid
    - RESULT1 [`output grid`] : Low-pass 1. Step 1: low-pass of stripe
    - RESULT2 [`output grid`] : Low-pass 2. Step 2: low-pass between stripe and its surroundings
    - STRIPES [`output grid`] : Stripes. The difference between destriped and original grid
    - ANG [`floating point number`] : Angle. Default: 0.000000 [Degree], 0 = horizontal, 90 = vertical
    - R [`floating point number`] : Radius. Minimum: 1.000000 Default: 20.000000 [Cells]
    - D [`floating point number`] : Stripes Distance. Minimum: 2.000000 Default: 2.000000 [Cells]

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('contrib_perego', '5', 'Destriping')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Input ('MASK', MASK)
        Tool.Set_Output('RESULT3', RESULT3)
        Tool.Set_Output('RESULT1', RESULT1)
        Tool.Set_Output('RESULT2', RESULT2)
        Tool.Set_Output('STRIPES', STRIPES)
        Tool.Set_Option('ANG', ANG)
        Tool.Set_Option('R', R)
        Tool.Set_Option('D', D)
        return Tool.Execute(Verbose)
    return False

def Destriping_with_Mask(INPUT=None, MASK=None, RESULT3=None, RESULT1=None, RESULT2=None, STRIPES=None, ANG=None, R=None, D=None, MIN=None, MAX=None, MMIN=None, MMAX=None, Verbose=2):
    '''
    Destriping with Mask
    ----------
    [contrib_perego.6]\n
    Destriping filter removes straight parallel stripes in raster data. It uses two low-pass filters elongated in the stripes direction; the first one is 1 pixel unit wide while the second one is wide as the striping wavelength. Their difference is the striping error which is removed from the original data to obtain the destriped DEM. This method is equivalent to that proposed ry[1] Oimoen (2000). With destriping 2 you can choose a range of value (min-max) from the input grid and a range of value (Mask min - Mask max) from a mask grid to select the target cells.\n
    Arguments
    ----------
    - INPUT [`input grid`] : Grid
    - MASK [`input grid`] : Mask. Mask used to select cells for processing
    - RESULT3 [`output grid`] : Destriped Grid
    - RESULT1 [`output grid`] : Low-pass 1. Step 1: low-pass of stripe
    - RESULT2 [`output grid`] : Low-pass 2. Step 2: low-pass between stripe and its surroundings
    - STRIPES [`output grid`] : Stripes. The difference between destriped and original grid
    - ANG [`floating point number`] : Angle. Default: 0.000000 [Degree], 0 = horizontal, 90 = vertical
    - R [`floating point number`] : Radius. Minimum: 1.000000 Default: 20.000000 [Cells]
    - D [`floating point number`] : Stripes Distance. Minimum: 2.000000 Default: 2.000000 [Cells]
    - MIN [`floating point number`] : Minimum. Default: -10.000000
    - MAX [`floating point number`] : Maximum. Default: 10.000000
    - MMIN [`floating point number`] : Mask Minimum. Default: -10000.000000
    - MMAX [`floating point number`] : Mask Maximum. Default: 10000.000000

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('contrib_perego', '6', 'Destriping with Mask')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Input ('MASK', MASK)
        Tool.Set_Output('RESULT3', RESULT3)
        Tool.Set_Output('RESULT1', RESULT1)
        Tool.Set_Output('RESULT2', RESULT2)
        Tool.Set_Output('STRIPES', STRIPES)
        Tool.Set_Option('ANG', ANG)
        Tool.Set_Option('R', R)
        Tool.Set_Option('D', D)
        Tool.Set_Option('MIN', MIN)
        Tool.Set_Option('MAX', MAX)
        Tool.Set_Option('MMIN', MMIN)
        Tool.Set_Option('MMAX', MMAX)
        return Tool.Execute(Verbose)
    return False

def run_tool_contrib_perego_6(INPUT=None, MASK=None, RESULT3=None, RESULT1=None, RESULT2=None, STRIPES=None, ANG=None, R=None, D=None, MIN=None, MAX=None, MMIN=None, MMAX=None, Verbose=2):
    '''
    Destriping with Mask
    ----------
    [contrib_perego.6]\n
    Destriping filter removes straight parallel stripes in raster data. It uses two low-pass filters elongated in the stripes direction; the first one is 1 pixel unit wide while the second one is wide as the striping wavelength. Their difference is the striping error which is removed from the original data to obtain the destriped DEM. This method is equivalent to that proposed ry[1] Oimoen (2000). With destriping 2 you can choose a range of value (min-max) from the input grid and a range of value (Mask min - Mask max) from a mask grid to select the target cells.\n
    Arguments
    ----------
    - INPUT [`input grid`] : Grid
    - MASK [`input grid`] : Mask. Mask used to select cells for processing
    - RESULT3 [`output grid`] : Destriped Grid
    - RESULT1 [`output grid`] : Low-pass 1. Step 1: low-pass of stripe
    - RESULT2 [`output grid`] : Low-pass 2. Step 2: low-pass between stripe and its surroundings
    - STRIPES [`output grid`] : Stripes. The difference between destriped and original grid
    - ANG [`floating point number`] : Angle. Default: 0.000000 [Degree], 0 = horizontal, 90 = vertical
    - R [`floating point number`] : Radius. Minimum: 1.000000 Default: 20.000000 [Cells]
    - D [`floating point number`] : Stripes Distance. Minimum: 2.000000 Default: 2.000000 [Cells]
    - MIN [`floating point number`] : Minimum. Default: -10.000000
    - MAX [`floating point number`] : Maximum. Default: 10.000000
    - MMIN [`floating point number`] : Mask Minimum. Default: -10000.000000
    - MMAX [`floating point number`] : Mask Maximum. Default: 10000.000000

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('contrib_perego', '6', 'Destriping with Mask')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Input ('MASK', MASK)
        Tool.Set_Output('RESULT3', RESULT3)
        Tool.Set_Output('RESULT1', RESULT1)
        Tool.Set_Output('RESULT2', RESULT2)
        Tool.Set_Output('STRIPES', STRIPES)
        Tool.Set_Option('ANG', ANG)
        Tool.Set_Option('R', R)
        Tool.Set_Option('D', D)
        Tool.Set_Option('MIN', MIN)
        Tool.Set_Option('MAX', MAX)
        Tool.Set_Option('MMIN', MMIN)
        Tool.Set_Option('MMAX', MMAX)
        return Tool.Execute(Verbose)
    return False

def Directional_Average(INPUT=None, RESULT=None, ANG=None, R1=None, R2=None, Verbose=2):
    '''
    Directional Average
    ----------
    [contrib_perego.7]\n
    directional1 average for Grids\n
    Arguments
    ----------
    - INPUT [`input grid`] : Input. This must be your input data of type grid.
    - RESULT [`output grid`] : Output Grid. New grid filtered with the directional1 tool
    - ANG [`floating point number`] : Angle (in degrees). Default: 0.000000 0 is horizontal, 90 is vertical.
    - R1 [`floating point number`] : Main Radius. Default: 1.000000
    - R2 [`floating point number`] : Transversal radius. Default: 0.500000

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('contrib_perego', '7', 'Directional Average')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Output('RESULT', RESULT)
        Tool.Set_Option('ANG', ANG)
        Tool.Set_Option('R1', R1)
        Tool.Set_Option('R2', R2)
        return Tool.Execute(Verbose)
    return False

def run_tool_contrib_perego_7(INPUT=None, RESULT=None, ANG=None, R1=None, R2=None, Verbose=2):
    '''
    Directional Average
    ----------
    [contrib_perego.7]\n
    directional1 average for Grids\n
    Arguments
    ----------
    - INPUT [`input grid`] : Input. This must be your input data of type grid.
    - RESULT [`output grid`] : Output Grid. New grid filtered with the directional1 tool
    - ANG [`floating point number`] : Angle (in degrees). Default: 0.000000 0 is horizontal, 90 is vertical.
    - R1 [`floating point number`] : Main Radius. Default: 1.000000
    - R2 [`floating point number`] : Transversal radius. Default: 0.500000

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('contrib_perego', '7', 'Directional Average')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Output('RESULT', RESULT)
        Tool.Set_Option('ANG', ANG)
        Tool.Set_Option('R1', R1)
        Tool.Set_Option('R2', R2)
        return Tool.Execute(Verbose)
    return False

