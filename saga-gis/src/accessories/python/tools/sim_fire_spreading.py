#! /usr/bin/env python

'''
Python Interface to SAGA Tools Library
----------
- Category : Simulation
- Name     : Fire Spreading Analysis
- ID       : sim_fire_spreading

Description
----------
Fire spreading analyses based on the BEHAVE fire modeling system supported by the U.S. Forest Service, Fire and Aviation Management. Find more information on BEHAVE at the 'Public Domain Software for the Wildland Fire Community' at [http://fire.org](http://fire.org)

Reference:
Andrews, P.L. (1986): BEHAVE: Fire Behavior Prediction and Fuel Modeling System - Burn Subsystem, Part 1. U.S. Department of Agriculture, Forest Service General, Technical Report INT-194. 
'''

from PySAGA.helper import Tool_Wrapper

def Fire_Risk_Analysis(DEM=None, FUEL=None, WINDSPD=None, WINDDIR=None, M1H=None, M10H=None, M100H=None, MHERB=None, MWOOD=None, VALUE=None, BASEPROB=None, DANGER=None, COMPPROB=None, PRIORITY=None, MONTECARLO=None, INTERVAL=None, Verbose=2):
    '''
    Fire Risk Analysis
    ----------
    [sim_fire_spreading.0]\n
    Fire risk analysis based on the BEHAVE fire modeling system supported by the U.S. Forest Service, Fire and Aviation Management. Find more information on BEHAVE at the 'Public Domain Software for the Wildland Fire Community' at [http://fire.org](http://fire.org)\n
    Reference:\n
    Andrews, P.L. (1986): BEHAVE: Fire Behavior Prediction and Fuel Modeling System - Burn Subsystem, Part 1. U.S. Department of Agriculture, Forest Service General, Technical Report INT-194.\n
    Arguments
    ----------
    - DEM [`input grid`] : DEM
    - FUEL [`input grid`] : Fuel Model
    - WINDSPD [`input grid`] : Wind Speed. Wind Speed (m/s)
    - WINDDIR [`input grid`] : Wind Direction. Wind Direction (degrees clockwise from north)
    - M1H [`input grid`] : Dead Fuel Moisture 1H. Fraction (weight of water in sample / dry weight of sample)
    - M10H [`input grid`] : Dead Fuel Moisture 10H. Fraction (weight of water in sample / dry weight of sample)
    - M100H [`input grid`] : Dead Fuel Moisture 100H. Fraction (weight of water in sample / dry weight of sample)
    - MHERB [`input grid`] : Herbaceous Fuel Moisture. Fraction (weight of water in sample / dry weight of sample)
    - MWOOD [`input grid`] : Wood Fuel Moisture. Fraction (weight of water in sample / dry weight of sample)
    - VALUE [`optional input grid`] : Value
    - BASEPROB [`optional input grid`] : Base Probability
    - DANGER [`output grid`] : Danger
    - COMPPROB [`output grid`] : Compound Probability
    - PRIORITY [`output grid`] : Priority Index
    - MONTECARLO [`integer number`] : Number of Events. Minimum: 1 Default: 1000 Number of Monte-Carlo events
    - INTERVAL [`floating point number`] : Fire Length. Minimum: 1.000000 Default: 100.000000 Fire Length (min)

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('sim_fire_spreading', '0', 'Fire Risk Analysis')
    if Tool.is_Okay():
        Tool.Set_Input ('DEM', DEM)
        Tool.Set_Input ('FUEL', FUEL)
        Tool.Set_Input ('WINDSPD', WINDSPD)
        Tool.Set_Input ('WINDDIR', WINDDIR)
        Tool.Set_Input ('M1H', M1H)
        Tool.Set_Input ('M10H', M10H)
        Tool.Set_Input ('M100H', M100H)
        Tool.Set_Input ('MHERB', MHERB)
        Tool.Set_Input ('MWOOD', MWOOD)
        Tool.Set_Input ('VALUE', VALUE)
        Tool.Set_Input ('BASEPROB', BASEPROB)
        Tool.Set_Output('DANGER', DANGER)
        Tool.Set_Output('COMPPROB', COMPPROB)
        Tool.Set_Output('PRIORITY', PRIORITY)
        Tool.Set_Option('MONTECARLO', MONTECARLO)
        Tool.Set_Option('INTERVAL', INTERVAL)
        return Tool.Execute(Verbose)
    return False

def run_tool_sim_fire_spreading_0(DEM=None, FUEL=None, WINDSPD=None, WINDDIR=None, M1H=None, M10H=None, M100H=None, MHERB=None, MWOOD=None, VALUE=None, BASEPROB=None, DANGER=None, COMPPROB=None, PRIORITY=None, MONTECARLO=None, INTERVAL=None, Verbose=2):
    '''
    Fire Risk Analysis
    ----------
    [sim_fire_spreading.0]\n
    Fire risk analysis based on the BEHAVE fire modeling system supported by the U.S. Forest Service, Fire and Aviation Management. Find more information on BEHAVE at the 'Public Domain Software for the Wildland Fire Community' at [http://fire.org](http://fire.org)\n
    Reference:\n
    Andrews, P.L. (1986): BEHAVE: Fire Behavior Prediction and Fuel Modeling System - Burn Subsystem, Part 1. U.S. Department of Agriculture, Forest Service General, Technical Report INT-194.\n
    Arguments
    ----------
    - DEM [`input grid`] : DEM
    - FUEL [`input grid`] : Fuel Model
    - WINDSPD [`input grid`] : Wind Speed. Wind Speed (m/s)
    - WINDDIR [`input grid`] : Wind Direction. Wind Direction (degrees clockwise from north)
    - M1H [`input grid`] : Dead Fuel Moisture 1H. Fraction (weight of water in sample / dry weight of sample)
    - M10H [`input grid`] : Dead Fuel Moisture 10H. Fraction (weight of water in sample / dry weight of sample)
    - M100H [`input grid`] : Dead Fuel Moisture 100H. Fraction (weight of water in sample / dry weight of sample)
    - MHERB [`input grid`] : Herbaceous Fuel Moisture. Fraction (weight of water in sample / dry weight of sample)
    - MWOOD [`input grid`] : Wood Fuel Moisture. Fraction (weight of water in sample / dry weight of sample)
    - VALUE [`optional input grid`] : Value
    - BASEPROB [`optional input grid`] : Base Probability
    - DANGER [`output grid`] : Danger
    - COMPPROB [`output grid`] : Compound Probability
    - PRIORITY [`output grid`] : Priority Index
    - MONTECARLO [`integer number`] : Number of Events. Minimum: 1 Default: 1000 Number of Monte-Carlo events
    - INTERVAL [`floating point number`] : Fire Length. Minimum: 1.000000 Default: 100.000000 Fire Length (min)

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('sim_fire_spreading', '0', 'Fire Risk Analysis')
    if Tool.is_Okay():
        Tool.Set_Input ('DEM', DEM)
        Tool.Set_Input ('FUEL', FUEL)
        Tool.Set_Input ('WINDSPD', WINDSPD)
        Tool.Set_Input ('WINDDIR', WINDDIR)
        Tool.Set_Input ('M1H', M1H)
        Tool.Set_Input ('M10H', M10H)
        Tool.Set_Input ('M100H', M100H)
        Tool.Set_Input ('MHERB', MHERB)
        Tool.Set_Input ('MWOOD', MWOOD)
        Tool.Set_Input ('VALUE', VALUE)
        Tool.Set_Input ('BASEPROB', BASEPROB)
        Tool.Set_Output('DANGER', DANGER)
        Tool.Set_Output('COMPPROB', COMPPROB)
        Tool.Set_Output('PRIORITY', PRIORITY)
        Tool.Set_Option('MONTECARLO', MONTECARLO)
        Tool.Set_Option('INTERVAL', INTERVAL)
        return Tool.Execute(Verbose)
    return False

def Simulation(DEM=None, FUEL=None, WINDSPD=None, WINDDIR=None, M1H=None, M10H=None, M100H=None, MHERB=None, MWOOD=None, IGNITION=None, TIME=None, FLAME=None, INTENSITY=None, UPDATEVIEW=None, Verbose=2):
    '''
    Simulation
    ----------
    [sim_fire_spreading.1]\n
    Fire simulation based on the BEHAVE fire modeling system supported by the U.S. Forest Service, Fire and Aviation Management. Find more information on BEHAVE at the 'Public Domain Software for the Wildland Fire Community' at [http://fire.org](http://fire.org)\n
    Reference:\n
    Andrews, P.L. (1986): BEHAVE: Fire Behavior Prediction and Fuel Modeling System - Burn Subsystem, Part 1. U.S. Department of Agriculture, Forest Service General, Technical Report INT-194.\n
    Arguments
    ----------
    - DEM [`input grid`] : DEM
    - FUEL [`input grid`] : Fuel Model
    - WINDSPD [`input grid`] : Wind Speed. Wind speed (m/s)
    - WINDDIR [`input grid`] : Wind Direction. Wind direction (degrees clockwise from north)
    - M1H [`input grid`] : Dead Fuel Moisture 1H. Fraction (weight of water in sample / dry weight of sample)
    - M10H [`input grid`] : Dead Fuel Moisture 10H. Fraction (weight of water in sample / dry weight of sample)
    - M100H [`input grid`] : Dead Fuel Moisture 100H. Fraction (weight of water in sample / dry weight of sample)
    - MHERB [`input grid`] : Herbaceous Fuel Moisture. Fraction (weight of water in sample / dry weight of sample)
    - MWOOD [`input grid`] : Wood Fuel Moisture. Fraction (weight of water in sample / dry weight of sample)
    - IGNITION [`input grid`] : Ignition Points
    - TIME [`output grid`] : Time
    - FLAME [`output grid`] : Flame Length. Flame Length (m)
    - INTENSITY [`output grid`] : Intensity. Intensity (Kcal/m)
    - UPDATEVIEW [`boolean`] : Update View. Default: 1 Update view during simulation.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('sim_fire_spreading', '1', 'Simulation')
    if Tool.is_Okay():
        Tool.Set_Input ('DEM', DEM)
        Tool.Set_Input ('FUEL', FUEL)
        Tool.Set_Input ('WINDSPD', WINDSPD)
        Tool.Set_Input ('WINDDIR', WINDDIR)
        Tool.Set_Input ('M1H', M1H)
        Tool.Set_Input ('M10H', M10H)
        Tool.Set_Input ('M100H', M100H)
        Tool.Set_Input ('MHERB', MHERB)
        Tool.Set_Input ('MWOOD', MWOOD)
        Tool.Set_Input ('IGNITION', IGNITION)
        Tool.Set_Output('TIME', TIME)
        Tool.Set_Output('FLAME', FLAME)
        Tool.Set_Output('INTENSITY', INTENSITY)
        Tool.Set_Option('UPDATEVIEW', UPDATEVIEW)
        return Tool.Execute(Verbose)
    return False

def run_tool_sim_fire_spreading_1(DEM=None, FUEL=None, WINDSPD=None, WINDDIR=None, M1H=None, M10H=None, M100H=None, MHERB=None, MWOOD=None, IGNITION=None, TIME=None, FLAME=None, INTENSITY=None, UPDATEVIEW=None, Verbose=2):
    '''
    Simulation
    ----------
    [sim_fire_spreading.1]\n
    Fire simulation based on the BEHAVE fire modeling system supported by the U.S. Forest Service, Fire and Aviation Management. Find more information on BEHAVE at the 'Public Domain Software for the Wildland Fire Community' at [http://fire.org](http://fire.org)\n
    Reference:\n
    Andrews, P.L. (1986): BEHAVE: Fire Behavior Prediction and Fuel Modeling System - Burn Subsystem, Part 1. U.S. Department of Agriculture, Forest Service General, Technical Report INT-194.\n
    Arguments
    ----------
    - DEM [`input grid`] : DEM
    - FUEL [`input grid`] : Fuel Model
    - WINDSPD [`input grid`] : Wind Speed. Wind speed (m/s)
    - WINDDIR [`input grid`] : Wind Direction. Wind direction (degrees clockwise from north)
    - M1H [`input grid`] : Dead Fuel Moisture 1H. Fraction (weight of water in sample / dry weight of sample)
    - M10H [`input grid`] : Dead Fuel Moisture 10H. Fraction (weight of water in sample / dry weight of sample)
    - M100H [`input grid`] : Dead Fuel Moisture 100H. Fraction (weight of water in sample / dry weight of sample)
    - MHERB [`input grid`] : Herbaceous Fuel Moisture. Fraction (weight of water in sample / dry weight of sample)
    - MWOOD [`input grid`] : Wood Fuel Moisture. Fraction (weight of water in sample / dry weight of sample)
    - IGNITION [`input grid`] : Ignition Points
    - TIME [`output grid`] : Time
    - FLAME [`output grid`] : Flame Length. Flame Length (m)
    - INTENSITY [`output grid`] : Intensity. Intensity (Kcal/m)
    - UPDATEVIEW [`boolean`] : Update View. Default: 1 Update view during simulation.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('sim_fire_spreading', '1', 'Simulation')
    if Tool.is_Okay():
        Tool.Set_Input ('DEM', DEM)
        Tool.Set_Input ('FUEL', FUEL)
        Tool.Set_Input ('WINDSPD', WINDSPD)
        Tool.Set_Input ('WINDDIR', WINDDIR)
        Tool.Set_Input ('M1H', M1H)
        Tool.Set_Input ('M10H', M10H)
        Tool.Set_Input ('M100H', M100H)
        Tool.Set_Input ('MHERB', MHERB)
        Tool.Set_Input ('MWOOD', MWOOD)
        Tool.Set_Input ('IGNITION', IGNITION)
        Tool.Set_Output('TIME', TIME)
        Tool.Set_Output('FLAME', FLAME)
        Tool.Set_Output('INTENSITY', INTENSITY)
        Tool.Set_Option('UPDATEVIEW', UPDATEVIEW)
        return Tool.Execute(Verbose)
    return False

