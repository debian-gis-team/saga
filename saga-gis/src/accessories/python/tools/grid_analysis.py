#! /usr/bin/env python

'''
Python Interface to SAGA Tools Library
----------
- Category : Grid
- Name     : Analysis
- ID       : grid_analysis

Description
----------
Some Grid Analysis Tools.
'''

from PySAGA.helper import Tool_Wrapper

def Accumulated_Cost(DEST_POINTS=None, DEST_GRID=None, COST=None, DIR_MAXCOST=None, ACCUMULATED=None, ALLOCATION=None, DEST_TYPE=None, COST_BMIN=None, COST_MIN=None, DIR_UNIT=None, DIR_K=None, THRESHOLD=None, Verbose=2):
    '''
    Accumulated Cost
    ----------
    [grid_analysis.0]\n
    Calculation of accumulated cost, either isotropic or anisotropic, if direction of maximum cost is specified.\n
    Arguments
    ----------
    - DEST_POINTS [`input shapes`] : Destinations
    - DEST_GRID [`input grid`] : Destinations
    - COST [`input grid`] : Local Cost
    - DIR_MAXCOST [`optional input grid`] : Direction of Maximum Cost
    - ACCUMULATED [`output grid`] : Accumulated Cost
    - ALLOCATION [`output grid`] : Allocation
    - DEST_TYPE [`choice`] : Destinations. Available Choices: [0] Points [1] Grid Default: 0
    - COST_BMIN [`boolean`] : Minimum Cost. Default: 1 Zero cost works like a barrier. Use this option to define a minimum cost applied everywhere where the supplied local cost falls below this value.
    - COST_MIN [`floating point number`] : Threshold. Minimum: 0.000000 Default: 0.010000 Zero cost works like a barrier. Use this option to define a minimum cost applied everywhere where the supplied local cost falls below this value.
    - DIR_UNIT [`choice`] : Units of Direction. Available Choices: [0] radians [1] degree Default: 0
    - DIR_K [`floating point number`] : K Factor. Default: 2.000000 effective friction = stated friction^f , where f = cos(DifAngle)^k.
    - THRESHOLD [`floating point number`] : Threshold for different route. Minimum: 0.000000 Default: 0.000000

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_analysis', '0', 'Accumulated Cost')
    if Tool.is_Okay():
        Tool.Set_Input ('DEST_POINTS', DEST_POINTS)
        Tool.Set_Input ('DEST_GRID', DEST_GRID)
        Tool.Set_Input ('COST', COST)
        Tool.Set_Input ('DIR_MAXCOST', DIR_MAXCOST)
        Tool.Set_Output('ACCUMULATED', ACCUMULATED)
        Tool.Set_Output('ALLOCATION', ALLOCATION)
        Tool.Set_Option('DEST_TYPE', DEST_TYPE)
        Tool.Set_Option('COST_BMIN', COST_BMIN)
        Tool.Set_Option('COST_MIN', COST_MIN)
        Tool.Set_Option('DIR_UNIT', DIR_UNIT)
        Tool.Set_Option('DIR_K', DIR_K)
        Tool.Set_Option('THRESHOLD', THRESHOLD)
        return Tool.Execute(Verbose)
    return False

def run_tool_grid_analysis_0(DEST_POINTS=None, DEST_GRID=None, COST=None, DIR_MAXCOST=None, ACCUMULATED=None, ALLOCATION=None, DEST_TYPE=None, COST_BMIN=None, COST_MIN=None, DIR_UNIT=None, DIR_K=None, THRESHOLD=None, Verbose=2):
    '''
    Accumulated Cost
    ----------
    [grid_analysis.0]\n
    Calculation of accumulated cost, either isotropic or anisotropic, if direction of maximum cost is specified.\n
    Arguments
    ----------
    - DEST_POINTS [`input shapes`] : Destinations
    - DEST_GRID [`input grid`] : Destinations
    - COST [`input grid`] : Local Cost
    - DIR_MAXCOST [`optional input grid`] : Direction of Maximum Cost
    - ACCUMULATED [`output grid`] : Accumulated Cost
    - ALLOCATION [`output grid`] : Allocation
    - DEST_TYPE [`choice`] : Destinations. Available Choices: [0] Points [1] Grid Default: 0
    - COST_BMIN [`boolean`] : Minimum Cost. Default: 1 Zero cost works like a barrier. Use this option to define a minimum cost applied everywhere where the supplied local cost falls below this value.
    - COST_MIN [`floating point number`] : Threshold. Minimum: 0.000000 Default: 0.010000 Zero cost works like a barrier. Use this option to define a minimum cost applied everywhere where the supplied local cost falls below this value.
    - DIR_UNIT [`choice`] : Units of Direction. Available Choices: [0] radians [1] degree Default: 0
    - DIR_K [`floating point number`] : K Factor. Default: 2.000000 effective friction = stated friction^f , where f = cos(DifAngle)^k.
    - THRESHOLD [`floating point number`] : Threshold for different route. Minimum: 0.000000 Default: 0.000000

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_analysis', '0', 'Accumulated Cost')
    if Tool.is_Okay():
        Tool.Set_Input ('DEST_POINTS', DEST_POINTS)
        Tool.Set_Input ('DEST_GRID', DEST_GRID)
        Tool.Set_Input ('COST', COST)
        Tool.Set_Input ('DIR_MAXCOST', DIR_MAXCOST)
        Tool.Set_Output('ACCUMULATED', ACCUMULATED)
        Tool.Set_Output('ALLOCATION', ALLOCATION)
        Tool.Set_Option('DEST_TYPE', DEST_TYPE)
        Tool.Set_Option('COST_BMIN', COST_BMIN)
        Tool.Set_Option('COST_MIN', COST_MIN)
        Tool.Set_Option('DIR_UNIT', DIR_UNIT)
        Tool.Set_Option('DIR_K', DIR_K)
        Tool.Set_Option('THRESHOLD', THRESHOLD)
        return Tool.Execute(Verbose)
    return False

def Least_Cost_Paths(SOURCE=None, DEM=None, VALUES=None, POINTS=None, LINE=None, Verbose=2):
    '''
    Least Cost Paths
    ----------
    [grid_analysis.5]\n
    This tool allows one to compute least cost path profile(s). It takes an accumulated cost surface grid and a point shapefile as input. Each point in the shapefile represents a source for which the least cost path is calculated.\n
    In case the point shapefile has more than one source point defined, a separate least cost path is calculated for each point. The tool outputs a point and a line shapefile for each least cost path.\n
    The tool allows for optional input grids. The cell values of these grids along the least cost path are written to the outputs as additional table fields.\n
    Arguments
    ----------
    - SOURCE [`input shapes`] : Source Point(s). Point shapefile with source point(s)
    - DEM [`input grid`] : Accumulated Cost Surface
    - VALUES [`optional input grid list`] : Values. Allows writing cell values from additional grids to the output
    - POINTS [`output shapes list`] : Profile Points. Least cost path profile points
    - LINE [`output shapes list`] : Profile Lines. Least cost path profile lines

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_analysis', '5', 'Least Cost Paths')
    if Tool.is_Okay():
        Tool.Set_Input ('SOURCE', SOURCE)
        Tool.Set_Input ('DEM', DEM)
        Tool.Set_Input ('VALUES', VALUES)
        Tool.Set_Output('POINTS', POINTS)
        Tool.Set_Output('LINE', LINE)
        return Tool.Execute(Verbose)
    return False

def run_tool_grid_analysis_5(SOURCE=None, DEM=None, VALUES=None, POINTS=None, LINE=None, Verbose=2):
    '''
    Least Cost Paths
    ----------
    [grid_analysis.5]\n
    This tool allows one to compute least cost path profile(s). It takes an accumulated cost surface grid and a point shapefile as input. Each point in the shapefile represents a source for which the least cost path is calculated.\n
    In case the point shapefile has more than one source point defined, a separate least cost path is calculated for each point. The tool outputs a point and a line shapefile for each least cost path.\n
    The tool allows for optional input grids. The cell values of these grids along the least cost path are written to the outputs as additional table fields.\n
    Arguments
    ----------
    - SOURCE [`input shapes`] : Source Point(s). Point shapefile with source point(s)
    - DEM [`input grid`] : Accumulated Cost Surface
    - VALUES [`optional input grid list`] : Values. Allows writing cell values from additional grids to the output
    - POINTS [`output shapes list`] : Profile Points. Least cost path profile points
    - LINE [`output shapes list`] : Profile Lines. Least cost path profile lines

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_analysis', '5', 'Least Cost Paths')
    if Tool.is_Okay():
        Tool.Set_Input ('SOURCE', SOURCE)
        Tool.Set_Input ('DEM', DEM)
        Tool.Set_Input ('VALUES', VALUES)
        Tool.Set_Output('POINTS', POINTS)
        Tool.Set_Output('LINE', LINE)
        return Tool.Execute(Verbose)
    return False

def Change_Vector_Analysis(A=None, B=None, DIST=None, DIR=None, C=None, Verbose=2):
    '''
    Change Vector Analysis
    ----------
    [grid_analysis.6]\n
    This tool performs a change vector analysis (CVA) for the given input features. Input features are supplied as grid lists for initial and final state. In both lists features have to be given in the same order. Distance is measured as Euclidean distance in features space. When analyzing two features direction is calculated as angle (radians) by default. Otherwise direction is coded as the quadrant it points to in terms of feature space.\n
    Arguments
    ----------
    - A [`input grid list`] : Initial State
    - B [`input grid list`] : Final State
    - DIST [`output grid`] : Distance
    - DIR [`output grid`] : Angle
    - C [`output grid collection`] : Change Vector

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_analysis', '6', 'Change Vector Analysis')
    if Tool.is_Okay():
        Tool.Set_Input ('A', A)
        Tool.Set_Input ('B', B)
        Tool.Set_Output('DIST', DIST)
        Tool.Set_Output('DIR', DIR)
        Tool.Set_Output('C', C)
        return Tool.Execute(Verbose)
    return False

def run_tool_grid_analysis_6(A=None, B=None, DIST=None, DIR=None, C=None, Verbose=2):
    '''
    Change Vector Analysis
    ----------
    [grid_analysis.6]\n
    This tool performs a change vector analysis (CVA) for the given input features. Input features are supplied as grid lists for initial and final state. In both lists features have to be given in the same order. Distance is measured as Euclidean distance in features space. When analyzing two features direction is calculated as angle (radians) by default. Otherwise direction is coded as the quadrant it points to in terms of feature space.\n
    Arguments
    ----------
    - A [`input grid list`] : Initial State
    - B [`input grid list`] : Final State
    - DIST [`output grid`] : Distance
    - DIR [`output grid`] : Angle
    - C [`output grid collection`] : Change Vector

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_analysis', '6', 'Change Vector Analysis')
    if Tool.is_Okay():
        Tool.Set_Input ('A', A)
        Tool.Set_Input ('B', B)
        Tool.Set_Output('DIST', DIST)
        Tool.Set_Output('DIR', DIR)
        Tool.Set_Output('C', C)
        return Tool.Execute(Verbose)
    return False

def Covered_Distance(INPUT=None, RESULT=None, Verbose=2):
    '''
    Covered Distance
    ----------
    [grid_analysis.7]\n
    Covered Distance\n
    Arguments
    ----------
    - INPUT [`input grid list`] : Grids
    - RESULT [`output grid`] : Covered Distance

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_analysis', '7', 'Covered Distance')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Output('RESULT', RESULT)
        return Tool.Execute(Verbose)
    return False

def run_tool_grid_analysis_7(INPUT=None, RESULT=None, Verbose=2):
    '''
    Covered Distance
    ----------
    [grid_analysis.7]\n
    Covered Distance\n
    Arguments
    ----------
    - INPUT [`input grid list`] : Grids
    - RESULT [`output grid`] : Covered Distance

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_analysis', '7', 'Covered Distance')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Output('RESULT', RESULT)
        return Tool.Execute(Verbose)
    return False

def Pattern_Analysis(INPUT=None, NDC=None, RELATIVE=None, FRAGMENTATION=None, CVN=None, DIVERSITY=None, DOMINANCE=None, MAXNUMCLASS=None, KERNEL_TYPE=None, KERNEL_RADIUS=None, Verbose=2):
    '''
    Pattern Analysis
    ----------
    [grid_analysis.8]\n
    Pattern Analysis\n
    Arguments
    ----------
    - INPUT [`input grid`] : Input Grid
    - NDC [`output grid`] : Number of Classes
    - RELATIVE [`output grid`] : Relative Richness
    - FRAGMENTATION [`output grid`] : Fragmentation
    - CVN [`output grid`] : Center vs. Neighbours
    - DIVERSITY [`output grid`] : Diversity
    - DOMINANCE [`output grid`] : Dominance
    - MAXNUMCLASS [`integer number`] : Max. Number of Classes. Default: 10 Maximum number of classes in entire grid.
    - KERNEL_TYPE [`choice`] : Kernel Type. Available Choices: [0] Square [1] Circle Default: 1 The kernel's shape.
    - KERNEL_RADIUS [`integer number`] : Radius. Minimum: 0 Default: 2 cells

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_analysis', '8', 'Pattern Analysis')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Output('NDC', NDC)
        Tool.Set_Output('RELATIVE', RELATIVE)
        Tool.Set_Output('FRAGMENTATION', FRAGMENTATION)
        Tool.Set_Output('CVN', CVN)
        Tool.Set_Output('DIVERSITY', DIVERSITY)
        Tool.Set_Output('DOMINANCE', DOMINANCE)
        Tool.Set_Option('MAXNUMCLASS', MAXNUMCLASS)
        Tool.Set_Option('KERNEL_TYPE', KERNEL_TYPE)
        Tool.Set_Option('KERNEL_RADIUS', KERNEL_RADIUS)
        return Tool.Execute(Verbose)
    return False

def run_tool_grid_analysis_8(INPUT=None, NDC=None, RELATIVE=None, FRAGMENTATION=None, CVN=None, DIVERSITY=None, DOMINANCE=None, MAXNUMCLASS=None, KERNEL_TYPE=None, KERNEL_RADIUS=None, Verbose=2):
    '''
    Pattern Analysis
    ----------
    [grid_analysis.8]\n
    Pattern Analysis\n
    Arguments
    ----------
    - INPUT [`input grid`] : Input Grid
    - NDC [`output grid`] : Number of Classes
    - RELATIVE [`output grid`] : Relative Richness
    - FRAGMENTATION [`output grid`] : Fragmentation
    - CVN [`output grid`] : Center vs. Neighbours
    - DIVERSITY [`output grid`] : Diversity
    - DOMINANCE [`output grid`] : Dominance
    - MAXNUMCLASS [`integer number`] : Max. Number of Classes. Default: 10 Maximum number of classes in entire grid.
    - KERNEL_TYPE [`choice`] : Kernel Type. Available Choices: [0] Square [1] Circle Default: 1 The kernel's shape.
    - KERNEL_RADIUS [`integer number`] : Radius. Minimum: 0 Default: 2 cells

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_analysis', '8', 'Pattern Analysis')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Output('NDC', NDC)
        Tool.Set_Output('RELATIVE', RELATIVE)
        Tool.Set_Output('FRAGMENTATION', FRAGMENTATION)
        Tool.Set_Output('CVN', CVN)
        Tool.Set_Output('DIVERSITY', DIVERSITY)
        Tool.Set_Output('DOMINANCE', DOMINANCE)
        Tool.Set_Option('MAXNUMCLASS', MAXNUMCLASS)
        Tool.Set_Option('KERNEL_TYPE', KERNEL_TYPE)
        Tool.Set_Option('KERNEL_RADIUS', KERNEL_RADIUS)
        return Tool.Execute(Verbose)
    return False

def Layer_of_extreme_value(GRIDS=None, RESULT=None, CRITERIA=None, Verbose=2):
    '''
    Layer of extreme value
    ----------
    [grid_analysis.9]\n
    It creates a new grid containing the ID of the grid with the maximum (minimum) value.\n
    Copyright 2005 Victor Olaya: e-mail: volaya@ya.com\n
    Arguments
    ----------
    - GRIDS [`input grid list`] : Grids
    - RESULT [`output grid`] : Result
    - CRITERIA [`choice`] : Method. Available Choices: [0] Maximum [1] Minimum Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_analysis', '9', 'Layer of extreme value')
    if Tool.is_Okay():
        Tool.Set_Input ('GRIDS', GRIDS)
        Tool.Set_Output('RESULT', RESULT)
        Tool.Set_Option('CRITERIA', CRITERIA)
        return Tool.Execute(Verbose)
    return False

def run_tool_grid_analysis_9(GRIDS=None, RESULT=None, CRITERIA=None, Verbose=2):
    '''
    Layer of extreme value
    ----------
    [grid_analysis.9]\n
    It creates a new grid containing the ID of the grid with the maximum (minimum) value.\n
    Copyright 2005 Victor Olaya: e-mail: volaya@ya.com\n
    Arguments
    ----------
    - GRIDS [`input grid list`] : Grids
    - RESULT [`output grid`] : Result
    - CRITERIA [`choice`] : Method. Available Choices: [0] Maximum [1] Minimum Default: 0

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_analysis', '9', 'Layer of extreme value')
    if Tool.is_Okay():
        Tool.Set_Input ('GRIDS', GRIDS)
        Tool.Set_Output('RESULT', RESULT)
        Tool.Set_Option('CRITERIA', CRITERIA)
        return Tool.Execute(Verbose)
    return False

def Analytical_Hierarchy_Process(GRIDS=None, TABLE=None, OUTPUT=None, Verbose=2):
    '''
    Analytical Hierarchy Process
    ----------
    [grid_analysis.10]\n
    (c) 2004 by Victor Olaya. Analytical Hierarchy Process\n
    Arguments
    ----------
    - GRIDS [`input grid list`] : Input Grids. Input Grids
    - TABLE [`input table`] : Pairwise Comparisons Table 
    - OUTPUT [`output grid`] : Output Grid

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_analysis', '10', 'Analytical Hierarchy Process')
    if Tool.is_Okay():
        Tool.Set_Input ('GRIDS', GRIDS)
        Tool.Set_Input ('TABLE', TABLE)
        Tool.Set_Output('OUTPUT', OUTPUT)
        return Tool.Execute(Verbose)
    return False

def run_tool_grid_analysis_10(GRIDS=None, TABLE=None, OUTPUT=None, Verbose=2):
    '''
    Analytical Hierarchy Process
    ----------
    [grid_analysis.10]\n
    (c) 2004 by Victor Olaya. Analytical Hierarchy Process\n
    Arguments
    ----------
    - GRIDS [`input grid list`] : Input Grids. Input Grids
    - TABLE [`input table`] : Pairwise Comparisons Table 
    - OUTPUT [`output grid`] : Output Grid

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_analysis', '10', 'Analytical Hierarchy Process')
    if Tool.is_Okay():
        Tool.Set_Input ('GRIDS', GRIDS)
        Tool.Set_Input ('TABLE', TABLE)
        Tool.Set_Output('OUTPUT', OUTPUT)
        return Tool.Execute(Verbose)
    return False

def Ordered_Weighted_Averaging(GRIDS=None, OUTPUT=None, ORDERED=None, WEIGHTS=None, Verbose=2):
    '''
    Ordered Weighted Averaging
    ----------
    [grid_analysis.11]\n
    The ordered weighted averaging (OWA) tool calculates for each cell the weighted average from the values of the supplied grids. The weighting factor for each grid value is defined with the 'Weights' table. If the 'Ordered' flag is unchecked, the order of the weights correspond to the order of the grids in the input list. If the 'Ordered' flag is checked, the grid values will be sorted and the weights will be assigned to the values in their ascending order, i.e. from the lowest to the highest value.\n
    Arguments
    ----------
    - GRIDS [`input grid list`] : Input Grids
    - OUTPUT [`output grid`] : Output Grid
    - ORDERED [`boolean`] : Ordered. Default: 1
    - WEIGHTS [`static table`] : Weights. 1 Fields: - 1. [8 byte floating point number] Weight 

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_analysis', '11', 'Ordered Weighted Averaging')
    if Tool.is_Okay():
        Tool.Set_Input ('GRIDS', GRIDS)
        Tool.Set_Output('OUTPUT', OUTPUT)
        Tool.Set_Option('ORDERED', ORDERED)
        Tool.Set_Option('WEIGHTS', WEIGHTS)
        return Tool.Execute(Verbose)
    return False

def run_tool_grid_analysis_11(GRIDS=None, OUTPUT=None, ORDERED=None, WEIGHTS=None, Verbose=2):
    '''
    Ordered Weighted Averaging
    ----------
    [grid_analysis.11]\n
    The ordered weighted averaging (OWA) tool calculates for each cell the weighted average from the values of the supplied grids. The weighting factor for each grid value is defined with the 'Weights' table. If the 'Ordered' flag is unchecked, the order of the weights correspond to the order of the grids in the input list. If the 'Ordered' flag is checked, the grid values will be sorted and the weights will be assigned to the values in their ascending order, i.e. from the lowest to the highest value.\n
    Arguments
    ----------
    - GRIDS [`input grid list`] : Input Grids
    - OUTPUT [`output grid`] : Output Grid
    - ORDERED [`boolean`] : Ordered. Default: 1
    - WEIGHTS [`static table`] : Weights. 1 Fields: - 1. [8 byte floating point number] Weight 

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_analysis', '11', 'Ordered Weighted Averaging')
    if Tool.is_Okay():
        Tool.Set_Input ('GRIDS', GRIDS)
        Tool.Set_Output('OUTPUT', OUTPUT)
        Tool.Set_Option('ORDERED', ORDERED)
        Tool.Set_Option('WEIGHTS', WEIGHTS)
        return Tool.Execute(Verbose)
    return False

def Aggregation_Index(INPUT=None, RESULT=None, MAXNUMCLASS=None, Verbose=2):
    '''
    Aggregation Index
    ----------
    [grid_analysis.12]\n
    (c) 2004 by Victor Olaya. Aggregation Index\n
    References:\n
    1. Hong S. He, et al. An aggregation index to quantify spatial patterns of landscapes, Landscape Ecology 15, 591-601,2000\n
    Arguments
    ----------
    - INPUT [`input grid`] : Input Grid
    - RESULT [`output table`] : Result
    - MAXNUMCLASS [`integer number`] : Max. Number of Classes. Default: 10 Maximum number of classes in the entire grid.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_analysis', '12', 'Aggregation Index')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Output('RESULT', RESULT)
        Tool.Set_Option('MAXNUMCLASS', MAXNUMCLASS)
        return Tool.Execute(Verbose)
    return False

def run_tool_grid_analysis_12(INPUT=None, RESULT=None, MAXNUMCLASS=None, Verbose=2):
    '''
    Aggregation Index
    ----------
    [grid_analysis.12]\n
    (c) 2004 by Victor Olaya. Aggregation Index\n
    References:\n
    1. Hong S. He, et al. An aggregation index to quantify spatial patterns of landscapes, Landscape Ecology 15, 591-601,2000\n
    Arguments
    ----------
    - INPUT [`input grid`] : Input Grid
    - RESULT [`output table`] : Result
    - MAXNUMCLASS [`integer number`] : Max. Number of Classes. Default: 10 Maximum number of classes in the entire grid.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_analysis', '12', 'Aggregation Index')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Output('RESULT', RESULT)
        Tool.Set_Option('MAXNUMCLASS', MAXNUMCLASS)
        return Tool.Execute(Verbose)
    return False

def CrossClassification_and_Tabulation(INPUT=None, INPUT2=None, RESULTGRID=None, RESULTTABLE=None, MAXNUMCLASS=None, Verbose=2):
    '''
    Cross-Classification and Tabulation
    ----------
    [grid_analysis.13]\n
    (c) 2004 by Victor Olaya. Cross-Classification and Tabulation\n
    Arguments
    ----------
    - INPUT [`input grid`] : Input Grid 1
    - INPUT2 [`input grid`] : Input Grid 2
    - RESULTGRID [`output grid`] : Cross-Classification Grid
    - RESULTTABLE [`output table`] : Cross-Tabulation Table
    - MAXNUMCLASS [`integer number`] : Max. Number of Classes. Default: 10 Maximum number of classes in the entire grid.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_analysis', '13', 'Cross-Classification and Tabulation')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Input ('INPUT2', INPUT2)
        Tool.Set_Output('RESULTGRID', RESULTGRID)
        Tool.Set_Output('RESULTTABLE', RESULTTABLE)
        Tool.Set_Option('MAXNUMCLASS', MAXNUMCLASS)
        return Tool.Execute(Verbose)
    return False

def run_tool_grid_analysis_13(INPUT=None, INPUT2=None, RESULTGRID=None, RESULTTABLE=None, MAXNUMCLASS=None, Verbose=2):
    '''
    Cross-Classification and Tabulation
    ----------
    [grid_analysis.13]\n
    (c) 2004 by Victor Olaya. Cross-Classification and Tabulation\n
    Arguments
    ----------
    - INPUT [`input grid`] : Input Grid 1
    - INPUT2 [`input grid`] : Input Grid 2
    - RESULTGRID [`output grid`] : Cross-Classification Grid
    - RESULTTABLE [`output table`] : Cross-Tabulation Table
    - MAXNUMCLASS [`integer number`] : Max. Number of Classes. Default: 10 Maximum number of classes in the entire grid.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_analysis', '13', 'Cross-Classification and Tabulation')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Input ('INPUT2', INPUT2)
        Tool.Set_Output('RESULTGRID', RESULTGRID)
        Tool.Set_Output('RESULTTABLE', RESULTTABLE)
        Tool.Set_Option('MAXNUMCLASS', MAXNUMCLASS)
        return Tool.Execute(Verbose)
    return False

def Soil_Texture_Classification(SAND=None, SILT=None, CLAY=None, TEXTURE=None, SUM=None, POLYGONS=None, SCHEME=None, COLORS=None, USER=None, XY_AXES=None, TRIANGLE=None, Verbose=2):
    '''
    Soil Texture Classification
    ----------
    [grid_analysis.14]\n
    Derive soil texture classes from sand, silt and clay contents. Currently supported schemes are USDA and German Kartieranleitung 5.\n
    Arguments
    ----------
    - SAND [`optional input grid`] : Sand. sand content given as percentage
    - SILT [`optional input grid`] : Silt. silt content given as percentage
    - CLAY [`optional input grid`] : Clay. clay content given as percentage
    - TEXTURE [`output grid`] : Soil Texture. soil texture
    - SUM [`output grid`] : Sum. Sum of percentages
    - POLYGONS [`output shapes`] : Scheme as Polygons
    - SCHEME [`choice`] : Classification. Available Choices: [0] USDA [1] Germany KA5 [2] Belgium/France [3] user defined Default: 0
    - COLORS [`choice`] : Default Colour Scheme. Available Choices: [0] Scheme 1 [1] Scheme 2 [2] Scheme 3 Default: 0
    - USER [`static table`] : User Definition. 4 Fields: - 1. [string] COLOR - 2. [string] KEY - 3. [string] NAME - 4. [string] POLYGON  The colour is defined as comma separated red, green and blue values (in the range 0 to 255). If the colour field is empty it will be generated from the chosen default colour scheme. Key and name are simple text labels specifying each class. The polygon is defined as pairs of sand (=x) and clay (=y) separated by a blank and separated from the next pair by a comma. 
    - XY_AXES [`choice`] : X/Y Axes. Available Choices: [0] Sand and Clay [1] Sand and Silt [2] Silt and Sand [3] Silt and Clay [4] Clay and Sand [5] Clay and Silt Default: 3
    - TRIANGLE [`choice`] : Triangle. Available Choices: [0] right-angled [1] isosceles Default: 1

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_analysis', '14', 'Soil Texture Classification')
    if Tool.is_Okay():
        Tool.Set_Input ('SAND', SAND)
        Tool.Set_Input ('SILT', SILT)
        Tool.Set_Input ('CLAY', CLAY)
        Tool.Set_Output('TEXTURE', TEXTURE)
        Tool.Set_Output('SUM', SUM)
        Tool.Set_Output('POLYGONS', POLYGONS)
        Tool.Set_Option('SCHEME', SCHEME)
        Tool.Set_Option('COLORS', COLORS)
        Tool.Set_Option('USER', USER)
        Tool.Set_Option('XY_AXES', XY_AXES)
        Tool.Set_Option('TRIANGLE', TRIANGLE)
        return Tool.Execute(Verbose)
    return False

def run_tool_grid_analysis_14(SAND=None, SILT=None, CLAY=None, TEXTURE=None, SUM=None, POLYGONS=None, SCHEME=None, COLORS=None, USER=None, XY_AXES=None, TRIANGLE=None, Verbose=2):
    '''
    Soil Texture Classification
    ----------
    [grid_analysis.14]\n
    Derive soil texture classes from sand, silt and clay contents. Currently supported schemes are USDA and German Kartieranleitung 5.\n
    Arguments
    ----------
    - SAND [`optional input grid`] : Sand. sand content given as percentage
    - SILT [`optional input grid`] : Silt. silt content given as percentage
    - CLAY [`optional input grid`] : Clay. clay content given as percentage
    - TEXTURE [`output grid`] : Soil Texture. soil texture
    - SUM [`output grid`] : Sum. Sum of percentages
    - POLYGONS [`output shapes`] : Scheme as Polygons
    - SCHEME [`choice`] : Classification. Available Choices: [0] USDA [1] Germany KA5 [2] Belgium/France [3] user defined Default: 0
    - COLORS [`choice`] : Default Colour Scheme. Available Choices: [0] Scheme 1 [1] Scheme 2 [2] Scheme 3 Default: 0
    - USER [`static table`] : User Definition. 4 Fields: - 1. [string] COLOR - 2. [string] KEY - 3. [string] NAME - 4. [string] POLYGON  The colour is defined as comma separated red, green and blue values (in the range 0 to 255). If the colour field is empty it will be generated from the chosen default colour scheme. Key and name are simple text labels specifying each class. The polygon is defined as pairs of sand (=x) and clay (=y) separated by a blank and separated from the next pair by a comma. 
    - XY_AXES [`choice`] : X/Y Axes. Available Choices: [0] Sand and Clay [1] Sand and Silt [2] Silt and Sand [3] Silt and Clay [4] Clay and Sand [5] Clay and Silt Default: 3
    - TRIANGLE [`choice`] : Triangle. Available Choices: [0] right-angled [1] isosceles Default: 1

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_analysis', '14', 'Soil Texture Classification')
    if Tool.is_Okay():
        Tool.Set_Input ('SAND', SAND)
        Tool.Set_Input ('SILT', SILT)
        Tool.Set_Input ('CLAY', CLAY)
        Tool.Set_Output('TEXTURE', TEXTURE)
        Tool.Set_Output('SUM', SUM)
        Tool.Set_Output('POLYGONS', POLYGONS)
        Tool.Set_Option('SCHEME', SCHEME)
        Tool.Set_Option('COLORS', COLORS)
        Tool.Set_Option('USER', USER)
        Tool.Set_Option('XY_AXES', XY_AXES)
        Tool.Set_Option('TRIANGLE', TRIANGLE)
        return Tool.Execute(Verbose)
    return False

def Fragmentation_Standard(CLASSES=None, DENSITY=None, CONNECTIVITY=None, FRAGMENTATION=None, FRAGSTATS=None, CLASS=None, NEIGHBORHOOD=None, AGGREGATION=None, BORDER=None, WEIGHT=None, DENSITY_MIN=None, DENSITY_INT=None, CIRCULAR=None, DIAGONAL=None, Verbose=2):
    '''
    Fragmentation (Standard)
    ----------
    [grid_analysis.15]\n
    Grid based fragmentation analysis after Riitters et al. (2000).\n
    (1) interior, if Density = 1.0\n
    (2) undetermined, if Density > 0.6 and Density = Connectivity\n
    (3) perforated, if Density > 0.6 and Density - Connectivity > 0\n
    (4) edge, if Density > 0.6 and Density - Connectivity < 0\n
    (5) transitional, if 0.4 < Density < 0.6\n
    (6) patch, if Density < 0.4\n
    Arguments
    ----------
    - CLASSES [`input grid`] : Classification
    - DENSITY [`output grid`] : Density [Percent]. Density Index (Pf).
    - CONNECTIVITY [`output grid`] : Connectivity [Percent]. Connectivity Index (Pff).
    - FRAGMENTATION [`output grid`] : Fragmentation. Fragmentation Index
    - FRAGSTATS [`output table`] : Summary
    - CLASS [`integer number`] : Class Identifier. Default: 1
    - NEIGHBORHOOD [`value range`] : Neighborhood. Moving window size = 1 + 2 * Neighborhood.
    - AGGREGATION [`choice`] : Level Aggregation. Available Choices: [0] average [1] multiplicative Default: 0
    - BORDER [`boolean`] : Add Border. Default: 0
    - WEIGHT [`floating point number`] : Connectivity Weighting. Minimum: 0.000000 Default: 1.100000
    - DENSITY_MIN [`floating point number`] : Minimum Density [Percent]. Minimum: 0.000000 Maximum: 100.000000 Default: 10.000000
    - DENSITY_INT [`floating point number`] : Minimum Density for Interior Forest [Percent]. Minimum: 0.000000 Maximum: 100.000000 Default: 99.000000 if less than 100, it is distinguished between interior and core forest
    - CIRCULAR [`choice`] : Neighbourhood. Available Choices: [0] square [1] circle Default: 1
    - DIAGONAL [`boolean`] : Include diagonal neighbour relations. Default: 1

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_analysis', '15', 'Fragmentation (Standard)')
    if Tool.is_Okay():
        Tool.Set_Input ('CLASSES', CLASSES)
        Tool.Set_Output('DENSITY', DENSITY)
        Tool.Set_Output('CONNECTIVITY', CONNECTIVITY)
        Tool.Set_Output('FRAGMENTATION', FRAGMENTATION)
        Tool.Set_Output('FRAGSTATS', FRAGSTATS)
        Tool.Set_Option('CLASS', CLASS)
        Tool.Set_Option('NEIGHBORHOOD', NEIGHBORHOOD)
        Tool.Set_Option('AGGREGATION', AGGREGATION)
        Tool.Set_Option('BORDER', BORDER)
        Tool.Set_Option('WEIGHT', WEIGHT)
        Tool.Set_Option('DENSITY_MIN', DENSITY_MIN)
        Tool.Set_Option('DENSITY_INT', DENSITY_INT)
        Tool.Set_Option('CIRCULAR', CIRCULAR)
        Tool.Set_Option('DIAGONAL', DIAGONAL)
        return Tool.Execute(Verbose)
    return False

def run_tool_grid_analysis_15(CLASSES=None, DENSITY=None, CONNECTIVITY=None, FRAGMENTATION=None, FRAGSTATS=None, CLASS=None, NEIGHBORHOOD=None, AGGREGATION=None, BORDER=None, WEIGHT=None, DENSITY_MIN=None, DENSITY_INT=None, CIRCULAR=None, DIAGONAL=None, Verbose=2):
    '''
    Fragmentation (Standard)
    ----------
    [grid_analysis.15]\n
    Grid based fragmentation analysis after Riitters et al. (2000).\n
    (1) interior, if Density = 1.0\n
    (2) undetermined, if Density > 0.6 and Density = Connectivity\n
    (3) perforated, if Density > 0.6 and Density - Connectivity > 0\n
    (4) edge, if Density > 0.6 and Density - Connectivity < 0\n
    (5) transitional, if 0.4 < Density < 0.6\n
    (6) patch, if Density < 0.4\n
    Arguments
    ----------
    - CLASSES [`input grid`] : Classification
    - DENSITY [`output grid`] : Density [Percent]. Density Index (Pf).
    - CONNECTIVITY [`output grid`] : Connectivity [Percent]. Connectivity Index (Pff).
    - FRAGMENTATION [`output grid`] : Fragmentation. Fragmentation Index
    - FRAGSTATS [`output table`] : Summary
    - CLASS [`integer number`] : Class Identifier. Default: 1
    - NEIGHBORHOOD [`value range`] : Neighborhood. Moving window size = 1 + 2 * Neighborhood.
    - AGGREGATION [`choice`] : Level Aggregation. Available Choices: [0] average [1] multiplicative Default: 0
    - BORDER [`boolean`] : Add Border. Default: 0
    - WEIGHT [`floating point number`] : Connectivity Weighting. Minimum: 0.000000 Default: 1.100000
    - DENSITY_MIN [`floating point number`] : Minimum Density [Percent]. Minimum: 0.000000 Maximum: 100.000000 Default: 10.000000
    - DENSITY_INT [`floating point number`] : Minimum Density for Interior Forest [Percent]. Minimum: 0.000000 Maximum: 100.000000 Default: 99.000000 if less than 100, it is distinguished between interior and core forest
    - CIRCULAR [`choice`] : Neighbourhood. Available Choices: [0] square [1] circle Default: 1
    - DIAGONAL [`boolean`] : Include diagonal neighbour relations. Default: 1

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_analysis', '15', 'Fragmentation (Standard)')
    if Tool.is_Okay():
        Tool.Set_Input ('CLASSES', CLASSES)
        Tool.Set_Output('DENSITY', DENSITY)
        Tool.Set_Output('CONNECTIVITY', CONNECTIVITY)
        Tool.Set_Output('FRAGMENTATION', FRAGMENTATION)
        Tool.Set_Output('FRAGSTATS', FRAGSTATS)
        Tool.Set_Option('CLASS', CLASS)
        Tool.Set_Option('NEIGHBORHOOD', NEIGHBORHOOD)
        Tool.Set_Option('AGGREGATION', AGGREGATION)
        Tool.Set_Option('BORDER', BORDER)
        Tool.Set_Option('WEIGHT', WEIGHT)
        Tool.Set_Option('DENSITY_MIN', DENSITY_MIN)
        Tool.Set_Option('DENSITY_INT', DENSITY_INT)
        Tool.Set_Option('CIRCULAR', CIRCULAR)
        Tool.Set_Option('DIAGONAL', DIAGONAL)
        return Tool.Execute(Verbose)
    return False

def Fragmentation_Alternative(CLASSES=None, DENSITY=None, CONNECTIVITY=None, FRAGMENTATION=None, FRAGSTATS=None, CLASS=None, NEIGHBORHOOD=None, AGGREGATION=None, BORDER=None, WEIGHT=None, DENSITY_MIN=None, DENSITY_INT=None, LEVEL_GROW=None, DENSITY_MEAN=None, Verbose=2):
    '''
    Fragmentation (Alternative)
    ----------
    [grid_analysis.16]\n
    (1) interior, if Density = 1.0\n
    (2) undetermined, if Density > 0.6 and Density = Connectivity\n
    (3) perforated, if Density > 0.6 and Density - Connectivity > 0\n
    (4) edge, if Density > 0.6 and Density - Connectivity < 0\n
    (5) transitional, if 0.4 < Density < 0.6\n
    (6) patch, if Density < 0.4\n
    Arguments
    ----------
    - CLASSES [`input grid`] : Classification
    - DENSITY [`output grid`] : Density [Percent]. Density Index (Pf).
    - CONNECTIVITY [`output grid`] : Connectivity [Percent]. Connectivity Index (Pff).
    - FRAGMENTATION [`output grid`] : Fragmentation. Fragmentation Index
    - FRAGSTATS [`output table`] : Summary
    - CLASS [`integer number`] : Class Identifier. Default: 1
    - NEIGHBORHOOD [`value range`] : Neighborhood. Moving window size = 1 + 2 * Neighborhood.
    - AGGREGATION [`choice`] : Level Aggregation. Available Choices: [0] average [1] multiplicative Default: 0
    - BORDER [`boolean`] : Add Border. Default: 0
    - WEIGHT [`floating point number`] : Connectivity Weighting. Minimum: 0.000000 Default: 1.100000
    - DENSITY_MIN [`floating point number`] : Minimum Density [Percent]. Minimum: 0.000000 Maximum: 100.000000 Default: 10.000000
    - DENSITY_INT [`floating point number`] : Minimum Density for Interior Forest [Percent]. Minimum: 0.000000 Maximum: 100.000000 Default: 99.000000 if less than 100, it is distinguished between interior and core forest
    - LEVEL_GROW [`floating point number`] : Search Distance Increment. Minimum: 0.000000 Default: 1.000000
    - DENSITY_MEAN [`boolean`] : Density from Neighbourhood. Default: 1

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_analysis', '16', 'Fragmentation (Alternative)')
    if Tool.is_Okay():
        Tool.Set_Input ('CLASSES', CLASSES)
        Tool.Set_Output('DENSITY', DENSITY)
        Tool.Set_Output('CONNECTIVITY', CONNECTIVITY)
        Tool.Set_Output('FRAGMENTATION', FRAGMENTATION)
        Tool.Set_Output('FRAGSTATS', FRAGSTATS)
        Tool.Set_Option('CLASS', CLASS)
        Tool.Set_Option('NEIGHBORHOOD', NEIGHBORHOOD)
        Tool.Set_Option('AGGREGATION', AGGREGATION)
        Tool.Set_Option('BORDER', BORDER)
        Tool.Set_Option('WEIGHT', WEIGHT)
        Tool.Set_Option('DENSITY_MIN', DENSITY_MIN)
        Tool.Set_Option('DENSITY_INT', DENSITY_INT)
        Tool.Set_Option('LEVEL_GROW', LEVEL_GROW)
        Tool.Set_Option('DENSITY_MEAN', DENSITY_MEAN)
        return Tool.Execute(Verbose)
    return False

def run_tool_grid_analysis_16(CLASSES=None, DENSITY=None, CONNECTIVITY=None, FRAGMENTATION=None, FRAGSTATS=None, CLASS=None, NEIGHBORHOOD=None, AGGREGATION=None, BORDER=None, WEIGHT=None, DENSITY_MIN=None, DENSITY_INT=None, LEVEL_GROW=None, DENSITY_MEAN=None, Verbose=2):
    '''
    Fragmentation (Alternative)
    ----------
    [grid_analysis.16]\n
    (1) interior, if Density = 1.0\n
    (2) undetermined, if Density > 0.6 and Density = Connectivity\n
    (3) perforated, if Density > 0.6 and Density - Connectivity > 0\n
    (4) edge, if Density > 0.6 and Density - Connectivity < 0\n
    (5) transitional, if 0.4 < Density < 0.6\n
    (6) patch, if Density < 0.4\n
    Arguments
    ----------
    - CLASSES [`input grid`] : Classification
    - DENSITY [`output grid`] : Density [Percent]. Density Index (Pf).
    - CONNECTIVITY [`output grid`] : Connectivity [Percent]. Connectivity Index (Pff).
    - FRAGMENTATION [`output grid`] : Fragmentation. Fragmentation Index
    - FRAGSTATS [`output table`] : Summary
    - CLASS [`integer number`] : Class Identifier. Default: 1
    - NEIGHBORHOOD [`value range`] : Neighborhood. Moving window size = 1 + 2 * Neighborhood.
    - AGGREGATION [`choice`] : Level Aggregation. Available Choices: [0] average [1] multiplicative Default: 0
    - BORDER [`boolean`] : Add Border. Default: 0
    - WEIGHT [`floating point number`] : Connectivity Weighting. Minimum: 0.000000 Default: 1.100000
    - DENSITY_MIN [`floating point number`] : Minimum Density [Percent]. Minimum: 0.000000 Maximum: 100.000000 Default: 10.000000
    - DENSITY_INT [`floating point number`] : Minimum Density for Interior Forest [Percent]. Minimum: 0.000000 Maximum: 100.000000 Default: 99.000000 if less than 100, it is distinguished between interior and core forest
    - LEVEL_GROW [`floating point number`] : Search Distance Increment. Minimum: 0.000000 Default: 1.000000
    - DENSITY_MEAN [`boolean`] : Density from Neighbourhood. Default: 1

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_analysis', '16', 'Fragmentation (Alternative)')
    if Tool.is_Okay():
        Tool.Set_Input ('CLASSES', CLASSES)
        Tool.Set_Output('DENSITY', DENSITY)
        Tool.Set_Output('CONNECTIVITY', CONNECTIVITY)
        Tool.Set_Output('FRAGMENTATION', FRAGMENTATION)
        Tool.Set_Output('FRAGSTATS', FRAGSTATS)
        Tool.Set_Option('CLASS', CLASS)
        Tool.Set_Option('NEIGHBORHOOD', NEIGHBORHOOD)
        Tool.Set_Option('AGGREGATION', AGGREGATION)
        Tool.Set_Option('BORDER', BORDER)
        Tool.Set_Option('WEIGHT', WEIGHT)
        Tool.Set_Option('DENSITY_MIN', DENSITY_MIN)
        Tool.Set_Option('DENSITY_INT', DENSITY_INT)
        Tool.Set_Option('LEVEL_GROW', LEVEL_GROW)
        Tool.Set_Option('DENSITY_MEAN', DENSITY_MEAN)
        return Tool.Execute(Verbose)
    return False

def Fragmentation_Classes_from_Density_and_Connectivity(DENSITY=None, CONNECTIVITY=None, FRAGMENTATION=None, BORDER=None, WEIGHT=None, DENSITY_MIN=None, DENSITY_INT=None, Verbose=2):
    '''
    Fragmentation Classes from Density and Connectivity
    ----------
    [grid_analysis.17]\n
    Fragmentation classes:\n
    (1) interior, if Density = 1.0\n
    (2) undetermined, if Density > 0.6 and Density = Connectivity\n
    (3) perforated, if Density > 0.6 and Density - Connectivity > 0\n
    (4) edge, if Density > 0.6 and Density - Connectivity < 0\n
    (5) transitional, if 0.4 < Density < 0.6\n
    (6) patch, if Density < 0.4\n
    Arguments
    ----------
    - DENSITY [`input grid`] : Density [Percent]. Density Index (Pf).
    - CONNECTIVITY [`input grid`] : Connectivity [Percent]. Connectivity Index (Pff).
    - FRAGMENTATION [`output grid`] : Fragmentation. Fragmentation Index
    - BORDER [`boolean`] : Add Border. Default: 0
    - WEIGHT [`floating point number`] : Connectivity Weighting. Minimum: 0.000000 Default: 1.100000
    - DENSITY_MIN [`floating point number`] : Minimum Density [Percent]. Minimum: 0.000000 Maximum: 100.000000 Default: 10.000000
    - DENSITY_INT [`floating point number`] : Minimum Density for Interior Forest [Percent]. Minimum: 0.000000 Maximum: 100.000000 Default: 99.000000 if less than 100, it is distinguished between interior and core forest

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_analysis', '17', 'Fragmentation Classes from Density and Connectivity')
    if Tool.is_Okay():
        Tool.Set_Input ('DENSITY', DENSITY)
        Tool.Set_Input ('CONNECTIVITY', CONNECTIVITY)
        Tool.Set_Output('FRAGMENTATION', FRAGMENTATION)
        Tool.Set_Option('BORDER', BORDER)
        Tool.Set_Option('WEIGHT', WEIGHT)
        Tool.Set_Option('DENSITY_MIN', DENSITY_MIN)
        Tool.Set_Option('DENSITY_INT', DENSITY_INT)
        return Tool.Execute(Verbose)
    return False

def run_tool_grid_analysis_17(DENSITY=None, CONNECTIVITY=None, FRAGMENTATION=None, BORDER=None, WEIGHT=None, DENSITY_MIN=None, DENSITY_INT=None, Verbose=2):
    '''
    Fragmentation Classes from Density and Connectivity
    ----------
    [grid_analysis.17]\n
    Fragmentation classes:\n
    (1) interior, if Density = 1.0\n
    (2) undetermined, if Density > 0.6 and Density = Connectivity\n
    (3) perforated, if Density > 0.6 and Density - Connectivity > 0\n
    (4) edge, if Density > 0.6 and Density - Connectivity < 0\n
    (5) transitional, if 0.4 < Density < 0.6\n
    (6) patch, if Density < 0.4\n
    Arguments
    ----------
    - DENSITY [`input grid`] : Density [Percent]. Density Index (Pf).
    - CONNECTIVITY [`input grid`] : Connectivity [Percent]. Connectivity Index (Pff).
    - FRAGMENTATION [`output grid`] : Fragmentation. Fragmentation Index
    - BORDER [`boolean`] : Add Border. Default: 0
    - WEIGHT [`floating point number`] : Connectivity Weighting. Minimum: 0.000000 Default: 1.100000
    - DENSITY_MIN [`floating point number`] : Minimum Density [Percent]. Minimum: 0.000000 Maximum: 100.000000 Default: 10.000000
    - DENSITY_INT [`floating point number`] : Minimum Density for Interior Forest [Percent]. Minimum: 0.000000 Maximum: 100.000000 Default: 99.000000 if less than 100, it is distinguished between interior and core forest

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_analysis', '17', 'Fragmentation Classes from Density and Connectivity')
    if Tool.is_Okay():
        Tool.Set_Input ('DENSITY', DENSITY)
        Tool.Set_Input ('CONNECTIVITY', CONNECTIVITY)
        Tool.Set_Output('FRAGMENTATION', FRAGMENTATION)
        Tool.Set_Option('BORDER', BORDER)
        Tool.Set_Option('WEIGHT', WEIGHT)
        Tool.Set_Option('DENSITY_MIN', DENSITY_MIN)
        Tool.Set_Option('DENSITY_INT', DENSITY_INT)
        return Tool.Execute(Verbose)
    return False

def Accumulation_Functions(SURFACE=None, INPUT=None, STATE_IN=None, OPERATION_GRID=None, CONTROL=None, CTRL_LINEAR=None, FLUX=None, STATE_OUT=None, OPERATION=None, LINEAR=None, THRES_LINEAR=None, Verbose=2):
    '''
    Accumulation Functions
    ----------
    [grid_analysis.18]\n
    The tool allows one to use different "accumulation functions" to, e.g., move material over a "local drain direction" (LDD) network. The LDD net is computed for the supplied surface by MFD and D8 flow-routing algorithms. It is possible to switch from MFD to D8 as soon as a flow threshold is exceeded.\n
    The input to each cell on the grid can be supplied from e.g. time series and the material can be moved over the net in several ways. All of these, except the "accuflux" operation, compute both the flux and the state for a given cell. For time series modelling (batch processing), the state of each cell at time t can be initialized with the previous state t - 1.\n
    The capacity, fraction, threshold and trigger operations compute the fluxes and cell states at time t + 1 according to cell-specific parameters that control the way the flux is computed.\n
    The capacity function limits the cell-to-cell flux by a (channel) capacity control; the fraction function transports only a given proportion of material from cell to cell, the threshold function transports material only once a given threshold has been exceeded, and the trigger function transports nothing until a trigger value has been exceeded (at which point all accumulated material in the state of the cell is discharged to its downstream neighbour(s)).\n
    The following operations are supported:\n
    * ACCUFLUX: The accuflux function computes the new state of the attributes for the cell as the sum of the input cell values plus the cumulative sum of all upstream elements draining through the cell.\n
    * ACCUCAPACITYFLUX / STATE: The operation modifies the accumulation of flow over the network by a limiting transport capacity given in absolute values.\n
    * ACCUFRACTIONFLUX / STATE: The operation limits the flow over the network by a parameter which controls the proportion (0-1) of the material that can flow through each cell.\n
    * ACCUTHRESHOLDFLUX / STATE: The operation modifies the accumulation of flow over the network by limiting transport to values greater than a minimum threshold value per cell. No flow occurs if the threshold is not exceeded.\n
    * ACCUTRIGGERFLUX / STATE: The operation only allows transport (flux) to occur if a trigger value is exceeded, otherwise no transport occurs and storage accumulates.\n
    Instead of choosing a single global operation with the "Operation" choice parameter, an input grid can be provided which encodes the operation per grid cell. This makes it possible to use different operations across the LDD (e.g. for different land use classes). The cell values used to encode the operation in the grid are the index numbers of the "Operation" choice parameter:\n
    0: accuflux\n
    1: accucapacityflux / state\n
    2: accufractionflux / state\n
    3: accuthresholdflux / state\n
    4: accutriggerflux / state\n
    Arguments
    ----------
    - SURFACE [`input grid`] : Surface. Surface used to derive the LDD network, e.g. a DTM.
    - INPUT [`input grid`] : Input. Grid with the input values to accumulate.
    - STATE_IN [`optional input grid`] : State t. Grid describing the state of each cell at timestep t.
    - OPERATION_GRID [`optional input grid`] : Operation Grid. Grid encoding the mode of operation per grid cell. Can be used instead of a global setting ("Operation" choice). Operations use the same identifiers as the "Operation" choice parameter [0-4].
    - CONTROL [`optional input grid`] : Operation Control. Depending on the mode of operation either transport capacity, transport fraction, threshold value or trigger value.
    - CTRL_LINEAR [`optional input grid`] : Linear Flow Control Grid. The values of this grid are checked against the linear flow threshold to decide on the flow-routing algorithm.
    - FLUX [`output grid`] : Flux. Flux out of each cell, i.e. everything accumulated so far.
    - STATE_OUT [`output grid`] : State t + 1. Grid describing the state of each cell at timestep t + 1.
    - OPERATION [`choice`] : Operation. Available Choices: [0] accuflux [1] accucapacityflux / state [2] accufractionflux / state [3] accuthresholdflux / state [4] accutriggerflux / state Default: 0 Select a mode of operation.
    - LINEAR [`boolean`] : Switch to Linear Flow. Default: 1 Switch from MFD8 to D8 if the linear flow threshold is crossed.
    - THRES_LINEAR [`floating point number`] : Threshold Linear Flow. Minimum: 0.000000 Default: 0.000000 Threshold for linear flow, if exceeded D8 is used.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_analysis', '18', 'Accumulation Functions')
    if Tool.is_Okay():
        Tool.Set_Input ('SURFACE', SURFACE)
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Input ('STATE_IN', STATE_IN)
        Tool.Set_Input ('OPERATION_GRID', OPERATION_GRID)
        Tool.Set_Input ('CONTROL', CONTROL)
        Tool.Set_Input ('CTRL_LINEAR', CTRL_LINEAR)
        Tool.Set_Output('FLUX', FLUX)
        Tool.Set_Output('STATE_OUT', STATE_OUT)
        Tool.Set_Option('OPERATION', OPERATION)
        Tool.Set_Option('LINEAR', LINEAR)
        Tool.Set_Option('THRES_LINEAR', THRES_LINEAR)
        return Tool.Execute(Verbose)
    return False

def run_tool_grid_analysis_18(SURFACE=None, INPUT=None, STATE_IN=None, OPERATION_GRID=None, CONTROL=None, CTRL_LINEAR=None, FLUX=None, STATE_OUT=None, OPERATION=None, LINEAR=None, THRES_LINEAR=None, Verbose=2):
    '''
    Accumulation Functions
    ----------
    [grid_analysis.18]\n
    The tool allows one to use different "accumulation functions" to, e.g., move material over a "local drain direction" (LDD) network. The LDD net is computed for the supplied surface by MFD and D8 flow-routing algorithms. It is possible to switch from MFD to D8 as soon as a flow threshold is exceeded.\n
    The input to each cell on the grid can be supplied from e.g. time series and the material can be moved over the net in several ways. All of these, except the "accuflux" operation, compute both the flux and the state for a given cell. For time series modelling (batch processing), the state of each cell at time t can be initialized with the previous state t - 1.\n
    The capacity, fraction, threshold and trigger operations compute the fluxes and cell states at time t + 1 according to cell-specific parameters that control the way the flux is computed.\n
    The capacity function limits the cell-to-cell flux by a (channel) capacity control; the fraction function transports only a given proportion of material from cell to cell, the threshold function transports material only once a given threshold has been exceeded, and the trigger function transports nothing until a trigger value has been exceeded (at which point all accumulated material in the state of the cell is discharged to its downstream neighbour(s)).\n
    The following operations are supported:\n
    * ACCUFLUX: The accuflux function computes the new state of the attributes for the cell as the sum of the input cell values plus the cumulative sum of all upstream elements draining through the cell.\n
    * ACCUCAPACITYFLUX / STATE: The operation modifies the accumulation of flow over the network by a limiting transport capacity given in absolute values.\n
    * ACCUFRACTIONFLUX / STATE: The operation limits the flow over the network by a parameter which controls the proportion (0-1) of the material that can flow through each cell.\n
    * ACCUTHRESHOLDFLUX / STATE: The operation modifies the accumulation of flow over the network by limiting transport to values greater than a minimum threshold value per cell. No flow occurs if the threshold is not exceeded.\n
    * ACCUTRIGGERFLUX / STATE: The operation only allows transport (flux) to occur if a trigger value is exceeded, otherwise no transport occurs and storage accumulates.\n
    Instead of choosing a single global operation with the "Operation" choice parameter, an input grid can be provided which encodes the operation per grid cell. This makes it possible to use different operations across the LDD (e.g. for different land use classes). The cell values used to encode the operation in the grid are the index numbers of the "Operation" choice parameter:\n
    0: accuflux\n
    1: accucapacityflux / state\n
    2: accufractionflux / state\n
    3: accuthresholdflux / state\n
    4: accutriggerflux / state\n
    Arguments
    ----------
    - SURFACE [`input grid`] : Surface. Surface used to derive the LDD network, e.g. a DTM.
    - INPUT [`input grid`] : Input. Grid with the input values to accumulate.
    - STATE_IN [`optional input grid`] : State t. Grid describing the state of each cell at timestep t.
    - OPERATION_GRID [`optional input grid`] : Operation Grid. Grid encoding the mode of operation per grid cell. Can be used instead of a global setting ("Operation" choice). Operations use the same identifiers as the "Operation" choice parameter [0-4].
    - CONTROL [`optional input grid`] : Operation Control. Depending on the mode of operation either transport capacity, transport fraction, threshold value or trigger value.
    - CTRL_LINEAR [`optional input grid`] : Linear Flow Control Grid. The values of this grid are checked against the linear flow threshold to decide on the flow-routing algorithm.
    - FLUX [`output grid`] : Flux. Flux out of each cell, i.e. everything accumulated so far.
    - STATE_OUT [`output grid`] : State t + 1. Grid describing the state of each cell at timestep t + 1.
    - OPERATION [`choice`] : Operation. Available Choices: [0] accuflux [1] accucapacityflux / state [2] accufractionflux / state [3] accuthresholdflux / state [4] accutriggerflux / state Default: 0 Select a mode of operation.
    - LINEAR [`boolean`] : Switch to Linear Flow. Default: 1 Switch from MFD8 to D8 if the linear flow threshold is crossed.
    - THRES_LINEAR [`floating point number`] : Threshold Linear Flow. Minimum: 0.000000 Default: 0.000000 Threshold for linear flow, if exceeded D8 is used.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_analysis', '18', 'Accumulation Functions')
    if Tool.is_Okay():
        Tool.Set_Input ('SURFACE', SURFACE)
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Input ('STATE_IN', STATE_IN)
        Tool.Set_Input ('OPERATION_GRID', OPERATION_GRID)
        Tool.Set_Input ('CONTROL', CONTROL)
        Tool.Set_Input ('CTRL_LINEAR', CTRL_LINEAR)
        Tool.Set_Output('FLUX', FLUX)
        Tool.Set_Output('STATE_OUT', STATE_OUT)
        Tool.Set_Option('OPERATION', OPERATION)
        Tool.Set_Option('LINEAR', LINEAR)
        Tool.Set_Option('THRES_LINEAR', THRES_LINEAR)
        return Tool.Execute(Verbose)
    return False

def IMCORR__Feature_Tracking(GRID_1=None, GRID_2=None, DTM_1=None, DTM_2=None, CORRPOINTS=None, CORRLINES=None, SEARCH_CHIPSIZE=None, REF_CHIPSIZE=None, GRID_SPACING=None, Verbose=2):
    '''
    IMCORR - Feature Tracking
    ----------
    [grid_analysis.19]\n
    The tool performs an image correlation based on two raster data sets. Additionally, two DTMs can be given and used to obtain 3D displacement vectors.\n
    This is a SAGA implementation of the standalone IMCORR software provided by the National Snow and Ice Data Center in Boulder, Colorado / US.\n
    Arguments
    ----------
    - GRID_1 [`input grid`] : Grid 1. The first grid to correlate
    - GRID_2 [`input grid`] : Grid 2. The second grid to correlate
    - DTM_1 [`optional input grid`] : DTM 1. The first DTM used to assign height information to grid 1
    - DTM_2 [`optional input grid`] : DTM 2. The second DTM used to assign height information to grid 2
    - CORRPOINTS [`output shapes`] : Correlated Points. Correlated points with displacement and correlation information
    - CORRLINES [`output shapes`] : Displacement Vector. Displacement vectors between correlated points
    - SEARCH_CHIPSIZE [`choice`] : Search Chip Size (Cells). Available Choices: [0] 16x16 [1] 32x32 [2] 64x64 [3] 128x128 [4] 256x256 Default: 2 Chip size of search chip, used to find correlating reference chip
    - REF_CHIPSIZE [`choice`] : Reference Chip Size (Cells). Available Choices: [0] 16x16 [1] 32x32 [2] 64x64 [3] 128x128 Default: 1 Chip size of reference chip to be found in search chip
    - GRID_SPACING [`floating point number`] : Grid Spacing (Map Units). Minimum: 0.100000 Maximum: 256.000000 Default: 10.000000 Grid spacing used for the construction of correlated points [map units]

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_analysis', '19', 'IMCORR - Feature Tracking')
    if Tool.is_Okay():
        Tool.Set_Input ('GRID_1', GRID_1)
        Tool.Set_Input ('GRID_2', GRID_2)
        Tool.Set_Input ('DTM_1', DTM_1)
        Tool.Set_Input ('DTM_2', DTM_2)
        Tool.Set_Output('CORRPOINTS', CORRPOINTS)
        Tool.Set_Output('CORRLINES', CORRLINES)
        Tool.Set_Option('SEARCH_CHIPSIZE', SEARCH_CHIPSIZE)
        Tool.Set_Option('REF_CHIPSIZE', REF_CHIPSIZE)
        Tool.Set_Option('GRID_SPACING', GRID_SPACING)
        return Tool.Execute(Verbose)
    return False

def run_tool_grid_analysis_19(GRID_1=None, GRID_2=None, DTM_1=None, DTM_2=None, CORRPOINTS=None, CORRLINES=None, SEARCH_CHIPSIZE=None, REF_CHIPSIZE=None, GRID_SPACING=None, Verbose=2):
    '''
    IMCORR - Feature Tracking
    ----------
    [grid_analysis.19]\n
    The tool performs an image correlation based on two raster data sets. Additionally, two DTMs can be given and used to obtain 3D displacement vectors.\n
    This is a SAGA implementation of the standalone IMCORR software provided by the National Snow and Ice Data Center in Boulder, Colorado / US.\n
    Arguments
    ----------
    - GRID_1 [`input grid`] : Grid 1. The first grid to correlate
    - GRID_2 [`input grid`] : Grid 2. The second grid to correlate
    - DTM_1 [`optional input grid`] : DTM 1. The first DTM used to assign height information to grid 1
    - DTM_2 [`optional input grid`] : DTM 2. The second DTM used to assign height information to grid 2
    - CORRPOINTS [`output shapes`] : Correlated Points. Correlated points with displacement and correlation information
    - CORRLINES [`output shapes`] : Displacement Vector. Displacement vectors between correlated points
    - SEARCH_CHIPSIZE [`choice`] : Search Chip Size (Cells). Available Choices: [0] 16x16 [1] 32x32 [2] 64x64 [3] 128x128 [4] 256x256 Default: 2 Chip size of search chip, used to find correlating reference chip
    - REF_CHIPSIZE [`choice`] : Reference Chip Size (Cells). Available Choices: [0] 16x16 [1] 32x32 [2] 64x64 [3] 128x128 Default: 1 Chip size of reference chip to be found in search chip
    - GRID_SPACING [`floating point number`] : Grid Spacing (Map Units). Minimum: 0.100000 Maximum: 256.000000 Default: 10.000000 Grid spacing used for the construction of correlated points [map units]

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_analysis', '19', 'IMCORR - Feature Tracking')
    if Tool.is_Okay():
        Tool.Set_Input ('GRID_1', GRID_1)
        Tool.Set_Input ('GRID_2', GRID_2)
        Tool.Set_Input ('DTM_1', DTM_1)
        Tool.Set_Input ('DTM_2', DTM_2)
        Tool.Set_Output('CORRPOINTS', CORRPOINTS)
        Tool.Set_Output('CORRLINES', CORRLINES)
        Tool.Set_Option('SEARCH_CHIPSIZE', SEARCH_CHIPSIZE)
        Tool.Set_Option('REF_CHIPSIZE', REF_CHIPSIZE)
        Tool.Set_Option('GRID_SPACING', GRID_SPACING)
        return Tool.Execute(Verbose)
    return False

def Soil_Texture_Classification_for_Tables(TABLE=None, POLYGONS=None, SAND=None, SILT=None, CLAY=None, TEXTURE=None, SCHEME=None, COLORS=None, USER=None, XY_AXES=None, TRIANGLE=None, Verbose=2):
    '''
    Soil Texture Classification for Tables
    ----------
    [grid_analysis.20]\n
    Derive soil texture classes from sand, silt and clay contents. Currently supported schemes are USDA and German Kartieranleitung 5.\n
    Arguments
    ----------
    - TABLE [`input table`] : Table
    - POLYGONS [`output shapes`] : Scheme as Polygons
    - SAND [`table field`] : Sand. sand content given as percentage
    - SILT [`table field`] : Silt. silt content given as percentage
    - CLAY [`table field`] : Clay. clay content given as percentage
    - TEXTURE [`table field`] : Texture. soil texture
    - SCHEME [`choice`] : Classification. Available Choices: [0] USDA [1] Germany KA5 [2] Belgium/France [3] user defined Default: 0
    - COLORS [`choice`] : Default Colour Scheme. Available Choices: [0] Scheme 1 [1] Scheme 2 [2] Scheme 3 Default: 0
    - USER [`static table`] : User Definition. 4 Fields: - 1. [string] COLOR - 2. [string] KEY - 3. [string] NAME - 4. [string] POLYGON  The colour is defined as comma separated red, green and blue values (in the range 0 to 255). If the colour field is empty it will be generated from the chosen default colour scheme. Key and name are simple text labels specifying each class. The polygon is defined as pairs of sand (=x) and clay (=y) separated by a blank and separated from the next pair by a comma. 
    - XY_AXES [`choice`] : X/Y Axes. Available Choices: [0] Sand and Clay [1] Sand and Silt [2] Silt and Sand [3] Silt and Clay [4] Clay and Sand [5] Clay and Silt Default: 3
    - TRIANGLE [`choice`] : Triangle. Available Choices: [0] right-angled [1] isosceles Default: 1

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_analysis', '20', 'Soil Texture Classification for Tables')
    if Tool.is_Okay():
        Tool.Set_Input ('TABLE', TABLE)
        Tool.Set_Output('POLYGONS', POLYGONS)
        Tool.Set_Option('SAND', SAND)
        Tool.Set_Option('SILT', SILT)
        Tool.Set_Option('CLAY', CLAY)
        Tool.Set_Option('TEXTURE', TEXTURE)
        Tool.Set_Option('SCHEME', SCHEME)
        Tool.Set_Option('COLORS', COLORS)
        Tool.Set_Option('USER', USER)
        Tool.Set_Option('XY_AXES', XY_AXES)
        Tool.Set_Option('TRIANGLE', TRIANGLE)
        return Tool.Execute(Verbose)
    return False

def run_tool_grid_analysis_20(TABLE=None, POLYGONS=None, SAND=None, SILT=None, CLAY=None, TEXTURE=None, SCHEME=None, COLORS=None, USER=None, XY_AXES=None, TRIANGLE=None, Verbose=2):
    '''
    Soil Texture Classification for Tables
    ----------
    [grid_analysis.20]\n
    Derive soil texture classes from sand, silt and clay contents. Currently supported schemes are USDA and German Kartieranleitung 5.\n
    Arguments
    ----------
    - TABLE [`input table`] : Table
    - POLYGONS [`output shapes`] : Scheme as Polygons
    - SAND [`table field`] : Sand. sand content given as percentage
    - SILT [`table field`] : Silt. silt content given as percentage
    - CLAY [`table field`] : Clay. clay content given as percentage
    - TEXTURE [`table field`] : Texture. soil texture
    - SCHEME [`choice`] : Classification. Available Choices: [0] USDA [1] Germany KA5 [2] Belgium/France [3] user defined Default: 0
    - COLORS [`choice`] : Default Colour Scheme. Available Choices: [0] Scheme 1 [1] Scheme 2 [2] Scheme 3 Default: 0
    - USER [`static table`] : User Definition. 4 Fields: - 1. [string] COLOR - 2. [string] KEY - 3. [string] NAME - 4. [string] POLYGON  The colour is defined as comma separated red, green and blue values (in the range 0 to 255). If the colour field is empty it will be generated from the chosen default colour scheme. Key and name are simple text labels specifying each class. The polygon is defined as pairs of sand (=x) and clay (=y) separated by a blank and separated from the next pair by a comma. 
    - XY_AXES [`choice`] : X/Y Axes. Available Choices: [0] Sand and Clay [1] Sand and Silt [2] Silt and Sand [3] Silt and Clay [4] Clay and Sand [5] Clay and Silt Default: 3
    - TRIANGLE [`choice`] : Triangle. Available Choices: [0] right-angled [1] isosceles Default: 1

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_analysis', '20', 'Soil Texture Classification for Tables')
    if Tool.is_Okay():
        Tool.Set_Input ('TABLE', TABLE)
        Tool.Set_Output('POLYGONS', POLYGONS)
        Tool.Set_Option('SAND', SAND)
        Tool.Set_Option('SILT', SILT)
        Tool.Set_Option('CLAY', CLAY)
        Tool.Set_Option('TEXTURE', TEXTURE)
        Tool.Set_Option('SCHEME', SCHEME)
        Tool.Set_Option('COLORS', COLORS)
        Tool.Set_Option('USER', USER)
        Tool.Set_Option('XY_AXES', XY_AXES)
        Tool.Set_Option('TRIANGLE', TRIANGLE)
        return Tool.Execute(Verbose)
    return False

def Diversity_of_Categories(CATEGORIES=None, COUNT=None, DIVERSITY=None, CONNECTIVITY=None, CONNECTEDAVG=None, NB_CASE=None, NORMALIZE=None, KERNEL_TYPE=None, KERNEL_RADIUS=None, DW_WEIGHTING=None, DW_IDW_POWER=None, DW_BANDWIDTH=None, Verbose=2):
    '''
    Diversity of Categories
    ----------
    [grid_analysis.21]\n
    Grid based analysis of diversity. It is assumed that the input grid provides a classification (i.e. not a continuous field). For each cell it counts the number of different categories (classes) as well as the connectivity within the chosen search window.\n
    Arguments
    ----------
    - CATEGORIES [`input grid`] : Categories
    - COUNT [`output grid`] : Number of Categories. number of different categories (unique values) within search area
    - DIVERSITY [`output grid`] : Diversity. distance weighted average of the number of different categories for distance classes
    - CONNECTIVITY [`output grid`] : Connectivity
    - CONNECTEDAVG [`output grid`] : Averaged Connectivity. average size of the area covered by each category that occurs within search area
    - NB_CASE [`choice`] : Connectivity Neighbourhood. Available Choices: [0] Rook's case [1] Queen's case Default: 1
    - NORMALIZE [`choice`] : Normalize. Available Choices: [0] no [1] by number of cells [2] by area size Default: 0
    - KERNEL_TYPE [`choice`] : Kernel Type. Available Choices: [0] Square [1] Circle Default: 1 The kernel's shape.
    - KERNEL_RADIUS [`integer number`] : Radius. Minimum: 0 Default: 2 cells
    - DW_WEIGHTING [`choice`] : Weighting Function. Available Choices: [0] no distance weighting [1] inverse distance to a power [2] exponential [3] gaussian Default: 3
    - DW_IDW_POWER [`floating point number`] : Power. Minimum: 0.000000 Default: 2.000000
    - DW_BANDWIDTH [`floating point number`] : Bandwidth. Minimum: 0.000000 Default: 0.700000 Bandwidth for exponential and Gaussian weighting

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_analysis', '21', 'Diversity of Categories')
    if Tool.is_Okay():
        Tool.Set_Input ('CATEGORIES', CATEGORIES)
        Tool.Set_Output('COUNT', COUNT)
        Tool.Set_Output('DIVERSITY', DIVERSITY)
        Tool.Set_Output('CONNECTIVITY', CONNECTIVITY)
        Tool.Set_Output('CONNECTEDAVG', CONNECTEDAVG)
        Tool.Set_Option('NB_CASE', NB_CASE)
        Tool.Set_Option('NORMALIZE', NORMALIZE)
        Tool.Set_Option('KERNEL_TYPE', KERNEL_TYPE)
        Tool.Set_Option('KERNEL_RADIUS', KERNEL_RADIUS)
        Tool.Set_Option('DW_WEIGHTING', DW_WEIGHTING)
        Tool.Set_Option('DW_IDW_POWER', DW_IDW_POWER)
        Tool.Set_Option('DW_BANDWIDTH', DW_BANDWIDTH)
        return Tool.Execute(Verbose)
    return False

def run_tool_grid_analysis_21(CATEGORIES=None, COUNT=None, DIVERSITY=None, CONNECTIVITY=None, CONNECTEDAVG=None, NB_CASE=None, NORMALIZE=None, KERNEL_TYPE=None, KERNEL_RADIUS=None, DW_WEIGHTING=None, DW_IDW_POWER=None, DW_BANDWIDTH=None, Verbose=2):
    '''
    Diversity of Categories
    ----------
    [grid_analysis.21]\n
    Grid based analysis of diversity. It is assumed that the input grid provides a classification (i.e. not a continuous field). For each cell it counts the number of different categories (classes) as well as the connectivity within the chosen search window.\n
    Arguments
    ----------
    - CATEGORIES [`input grid`] : Categories
    - COUNT [`output grid`] : Number of Categories. number of different categories (unique values) within search area
    - DIVERSITY [`output grid`] : Diversity. distance weighted average of the number of different categories for distance classes
    - CONNECTIVITY [`output grid`] : Connectivity
    - CONNECTEDAVG [`output grid`] : Averaged Connectivity. average size of the area covered by each category that occurs within search area
    - NB_CASE [`choice`] : Connectivity Neighbourhood. Available Choices: [0] Rook's case [1] Queen's case Default: 1
    - NORMALIZE [`choice`] : Normalize. Available Choices: [0] no [1] by number of cells [2] by area size Default: 0
    - KERNEL_TYPE [`choice`] : Kernel Type. Available Choices: [0] Square [1] Circle Default: 1 The kernel's shape.
    - KERNEL_RADIUS [`integer number`] : Radius. Minimum: 0 Default: 2 cells
    - DW_WEIGHTING [`choice`] : Weighting Function. Available Choices: [0] no distance weighting [1] inverse distance to a power [2] exponential [3] gaussian Default: 3
    - DW_IDW_POWER [`floating point number`] : Power. Minimum: 0.000000 Default: 2.000000
    - DW_BANDWIDTH [`floating point number`] : Bandwidth. Minimum: 0.000000 Default: 0.700000 Bandwidth for exponential and Gaussian weighting

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_analysis', '21', 'Diversity of Categories')
    if Tool.is_Okay():
        Tool.Set_Input ('CATEGORIES', CATEGORIES)
        Tool.Set_Output('COUNT', COUNT)
        Tool.Set_Output('DIVERSITY', DIVERSITY)
        Tool.Set_Output('CONNECTIVITY', CONNECTIVITY)
        Tool.Set_Output('CONNECTEDAVG', CONNECTEDAVG)
        Tool.Set_Option('NB_CASE', NB_CASE)
        Tool.Set_Option('NORMALIZE', NORMALIZE)
        Tool.Set_Option('KERNEL_TYPE', KERNEL_TYPE)
        Tool.Set_Option('KERNEL_RADIUS', KERNEL_RADIUS)
        Tool.Set_Option('DW_WEIGHTING', DW_WEIGHTING)
        Tool.Set_Option('DW_IDW_POWER', DW_IDW_POWER)
        Tool.Set_Option('DW_BANDWIDTH', DW_BANDWIDTH)
        return Tool.Execute(Verbose)
    return False

def Shannon_Index(CATEGORIES=None, COUNT=None, INDEX=None, EVENNESS=None, KERNEL_TYPE=None, KERNEL_RADIUS=None, Verbose=2):
    '''
    Shannon Index
    ----------
    [grid_analysis.22]\n
    Grid based analysis of diversity with the Shannon Index. The index is calculated locally for each grid cell using the specified kernel (aka 'moving window'). It is assumed that the grid cell values represent a classification.\n
    Arguments
    ----------
    - CATEGORIES [`input grid`] : Categories
    - COUNT [`output grid`] : Number of Categories. number of different categories (unique values) within search area
    - INDEX [`output grid`] : Shannon Index
    - EVENNESS [`output grid`] : Evenness
    - KERNEL_TYPE [`choice`] : Kernel Type. Available Choices: [0] Square [1] Circle Default: 1 The kernel's shape.
    - KERNEL_RADIUS [`integer number`] : Radius. Minimum: 0 Default: 2 cells

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_analysis', '22', 'Shannon Index')
    if Tool.is_Okay():
        Tool.Set_Input ('CATEGORIES', CATEGORIES)
        Tool.Set_Output('COUNT', COUNT)
        Tool.Set_Output('INDEX', INDEX)
        Tool.Set_Output('EVENNESS', EVENNESS)
        Tool.Set_Option('KERNEL_TYPE', KERNEL_TYPE)
        Tool.Set_Option('KERNEL_RADIUS', KERNEL_RADIUS)
        return Tool.Execute(Verbose)
    return False

def run_tool_grid_analysis_22(CATEGORIES=None, COUNT=None, INDEX=None, EVENNESS=None, KERNEL_TYPE=None, KERNEL_RADIUS=None, Verbose=2):
    '''
    Shannon Index
    ----------
    [grid_analysis.22]\n
    Grid based analysis of diversity with the Shannon Index. The index is calculated locally for each grid cell using the specified kernel (aka 'moving window'). It is assumed that the grid cell values represent a classification.\n
    Arguments
    ----------
    - CATEGORIES [`input grid`] : Categories
    - COUNT [`output grid`] : Number of Categories. number of different categories (unique values) within search area
    - INDEX [`output grid`] : Shannon Index
    - EVENNESS [`output grid`] : Evenness
    - KERNEL_TYPE [`choice`] : Kernel Type. Available Choices: [0] Square [1] Circle Default: 1 The kernel's shape.
    - KERNEL_RADIUS [`integer number`] : Radius. Minimum: 0 Default: 2 cells

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_analysis', '22', 'Shannon Index')
    if Tool.is_Okay():
        Tool.Set_Input ('CATEGORIES', CATEGORIES)
        Tool.Set_Output('COUNT', COUNT)
        Tool.Set_Output('INDEX', INDEX)
        Tool.Set_Output('EVENNESS', EVENNESS)
        Tool.Set_Option('KERNEL_TYPE', KERNEL_TYPE)
        Tool.Set_Option('KERNEL_RADIUS', KERNEL_RADIUS)
        return Tool.Execute(Verbose)
    return False

def Simpson_Index(CATEGORIES=None, COUNT=None, INDEX=None, KERNEL_TYPE=None, KERNEL_RADIUS=None, Verbose=2):
    '''
    Simpson Index
    ----------
    [grid_analysis.23]\n
    Grid based analysis of diversity with the Simpson Index. The index is calculated locally for each grid cell using the specified kernel (aka 'moving window'). It is assumed that the grid cell values represent a classification.\n
    Arguments
    ----------
    - CATEGORIES [`input grid`] : Categories
    - COUNT [`output grid`] : Number of Categories. number of different categories (unique values) within search area
    - INDEX [`output grid`] : Simpson Index
    - KERNEL_TYPE [`choice`] : Kernel Type. Available Choices: [0] Square [1] Circle Default: 1 The kernel's shape.
    - KERNEL_RADIUS [`integer number`] : Radius. Minimum: 0 Default: 2 cells

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_analysis', '23', 'Simpson Index')
    if Tool.is_Okay():
        Tool.Set_Input ('CATEGORIES', CATEGORIES)
        Tool.Set_Output('COUNT', COUNT)
        Tool.Set_Output('INDEX', INDEX)
        Tool.Set_Option('KERNEL_TYPE', KERNEL_TYPE)
        Tool.Set_Option('KERNEL_RADIUS', KERNEL_RADIUS)
        return Tool.Execute(Verbose)
    return False

def run_tool_grid_analysis_23(CATEGORIES=None, COUNT=None, INDEX=None, KERNEL_TYPE=None, KERNEL_RADIUS=None, Verbose=2):
    '''
    Simpson Index
    ----------
    [grid_analysis.23]\n
    Grid based analysis of diversity with the Simpson Index. The index is calculated locally for each grid cell using the specified kernel (aka 'moving window'). It is assumed that the grid cell values represent a classification.\n
    Arguments
    ----------
    - CATEGORIES [`input grid`] : Categories
    - COUNT [`output grid`] : Number of Categories. number of different categories (unique values) within search area
    - INDEX [`output grid`] : Simpson Index
    - KERNEL_TYPE [`choice`] : Kernel Type. Available Choices: [0] Square [1] Circle Default: 1 The kernel's shape.
    - KERNEL_RADIUS [`integer number`] : Radius. Minimum: 0 Default: 2 cells

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_analysis', '23', 'Simpson Index')
    if Tool.is_Okay():
        Tool.Set_Input ('CATEGORIES', CATEGORIES)
        Tool.Set_Output('COUNT', COUNT)
        Tool.Set_Output('INDEX', INDEX)
        Tool.Set_Option('KERNEL_TYPE', KERNEL_TYPE)
        Tool.Set_Option('KERNEL_RADIUS', KERNEL_RADIUS)
        return Tool.Execute(Verbose)
    return False

def Raos_Q_Diversity_Index_Classic(VALUES=None, COUNT=None, INDEX=None, KERNEL_TYPE=None, KERNEL_RADIUS=None, Verbose=2):
    '''
    Rao's Q Diversity Index (Classic)
    ----------
    [grid_analysis.24]\n
    Grid based analysis of diversity with Rao's Q Index. Rao's Q diversity index is calculated locally for each grid cell using the specified kernel (aka 'moving window'). It is assumed that the grid cell values represent quantities.\n
    Arguments
    ----------
    - VALUES [`input grid`] : Values
    - COUNT [`output grid`] : Number of Categories. number of different categories (unique values) within search area
    - INDEX [`output grid`] : Rao's Q
    - KERNEL_TYPE [`choice`] : Kernel Type. Available Choices: [0] Square [1] Circle Default: 1 The kernel's shape.
    - KERNEL_RADIUS [`integer number`] : Radius. Minimum: 0 Default: 2 cells

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_analysis', '24', 'Rao\'s Q Diversity Index (Classic)')
    if Tool.is_Okay():
        Tool.Set_Input ('VALUES', VALUES)
        Tool.Set_Output('COUNT', COUNT)
        Tool.Set_Output('INDEX', INDEX)
        Tool.Set_Option('KERNEL_TYPE', KERNEL_TYPE)
        Tool.Set_Option('KERNEL_RADIUS', KERNEL_RADIUS)
        return Tool.Execute(Verbose)
    return False

def run_tool_grid_analysis_24(VALUES=None, COUNT=None, INDEX=None, KERNEL_TYPE=None, KERNEL_RADIUS=None, Verbose=2):
    '''
    Rao's Q Diversity Index (Classic)
    ----------
    [grid_analysis.24]\n
    Grid based analysis of diversity with Rao's Q Index. Rao's Q diversity index is calculated locally for each grid cell using the specified kernel (aka 'moving window'). It is assumed that the grid cell values represent quantities.\n
    Arguments
    ----------
    - VALUES [`input grid`] : Values
    - COUNT [`output grid`] : Number of Categories. number of different categories (unique values) within search area
    - INDEX [`output grid`] : Rao's Q
    - KERNEL_TYPE [`choice`] : Kernel Type. Available Choices: [0] Square [1] Circle Default: 1 The kernel's shape.
    - KERNEL_RADIUS [`integer number`] : Radius. Minimum: 0 Default: 2 cells

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_analysis', '24', 'Rao\'s Q Diversity Index (Classic)')
    if Tool.is_Okay():
        Tool.Set_Input ('VALUES', VALUES)
        Tool.Set_Output('COUNT', COUNT)
        Tool.Set_Output('INDEX', INDEX)
        Tool.Set_Option('KERNEL_TYPE', KERNEL_TYPE)
        Tool.Set_Option('KERNEL_RADIUS', KERNEL_RADIUS)
        return Tool.Execute(Verbose)
    return False

def Raos_Q_Diversity_Index(VALUES=None, INDEX=None, NORMALIZE=None, DISTANCE=None, LAMBDA=None, KERNEL_TYPE=None, KERNEL_RADIUS=None, Verbose=2):
    '''
    Rao's Q Diversity Index
    ----------
    [grid_analysis.25]\n
    Grid based analysis of diversity with Rao's Q Index. Rao's Q diversity index is calculated locally for each grid cell using the specified kernel (aka 'moving window'). It is assumed that the grid cell values represent quantities.\n
    Arguments
    ----------
    - VALUES [`input grid list`] : Values
    - INDEX [`output grid`] : Rao's Q
    - NORMALIZE [`boolean`] : Normalize. Default: 0
    - DISTANCE [`choice`] : Distance. Available Choices: [0] Euclidean [1] Manhatten [2] Canberra [3] Minkowski Default: 0
    - LAMBDA [`floating point number`] : Lambda. Minimum: 0.000100 Default: 1.000000 Lambda for Minkowski distance calculation.
    - KERNEL_TYPE [`choice`] : Kernel Type. Available Choices: [0] Square [1] Circle Default: 1 The kernel's shape.
    - KERNEL_RADIUS [`integer number`] : Radius. Minimum: 0 Default: 2 cells

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_analysis', '25', 'Rao\'s Q Diversity Index')
    if Tool.is_Okay():
        Tool.Set_Input ('VALUES', VALUES)
        Tool.Set_Output('INDEX', INDEX)
        Tool.Set_Option('NORMALIZE', NORMALIZE)
        Tool.Set_Option('DISTANCE', DISTANCE)
        Tool.Set_Option('LAMBDA', LAMBDA)
        Tool.Set_Option('KERNEL_TYPE', KERNEL_TYPE)
        Tool.Set_Option('KERNEL_RADIUS', KERNEL_RADIUS)
        return Tool.Execute(Verbose)
    return False

def run_tool_grid_analysis_25(VALUES=None, INDEX=None, NORMALIZE=None, DISTANCE=None, LAMBDA=None, KERNEL_TYPE=None, KERNEL_RADIUS=None, Verbose=2):
    '''
    Rao's Q Diversity Index
    ----------
    [grid_analysis.25]\n
    Grid based analysis of diversity with Rao's Q Index. Rao's Q diversity index is calculated locally for each grid cell using the specified kernel (aka 'moving window'). It is assumed that the grid cell values represent quantities.\n
    Arguments
    ----------
    - VALUES [`input grid list`] : Values
    - INDEX [`output grid`] : Rao's Q
    - NORMALIZE [`boolean`] : Normalize. Default: 0
    - DISTANCE [`choice`] : Distance. Available Choices: [0] Euclidean [1] Manhatten [2] Canberra [3] Minkowski Default: 0
    - LAMBDA [`floating point number`] : Lambda. Minimum: 0.000100 Default: 1.000000 Lambda for Minkowski distance calculation.
    - KERNEL_TYPE [`choice`] : Kernel Type. Available Choices: [0] Square [1] Circle Default: 1 The kernel's shape.
    - KERNEL_RADIUS [`integer number`] : Radius. Minimum: 0 Default: 2 cells

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_analysis', '25', 'Rao\'s Q Diversity Index')
    if Tool.is_Okay():
        Tool.Set_Input ('VALUES', VALUES)
        Tool.Set_Output('INDEX', INDEX)
        Tool.Set_Option('NORMALIZE', NORMALIZE)
        Tool.Set_Option('DISTANCE', DISTANCE)
        Tool.Set_Option('LAMBDA', LAMBDA)
        Tool.Set_Option('KERNEL_TYPE', KERNEL_TYPE)
        Tool.Set_Option('KERNEL_RADIUS', KERNEL_RADIUS)
        return Tool.Execute(Verbose)
    return False

def Coverage_of_Categories(CLASSES=None, LUT=None, TARGET_TEMPLATE=None, COVERAGES=None, LUT_VAL=None, LUT_MAX=None, LUT_NAME=None, NO_DATA=None, DATADEPTH=None, UNIT=None, TARGET_DEFINITION=None, TARGET_USER_SIZE=None, TARGET_USER_XMIN=None, TARGET_USER_XMAX=None, TARGET_USER_YMIN=None, TARGET_USER_YMAX=None, TARGET_USER_COLS=None, TARGET_USER_ROWS=None, TARGET_USER_FLAT=None, TARGET_USER_FITS=None, TARGET_SYSTEM=None, Verbose=2):
    '''
    Coverage of Categories
    ----------
    [grid_analysis.26]\n
    The Coverage of Categories tool calculates for each category of the categories input grid the percentage it covers in each cell of the target grid system.\n
    Arguments
    ----------
    - CLASSES [`input grid`] : Categories
    - LUT [`optional input table`] : Classification
    - TARGET_TEMPLATE [`optional input grid`] : Target System. use this grid's system for output grids
    - COVERAGES [`output grid list`] : Coverages
    - LUT_VAL [`table field`] : Value. The class value or - in combination with value 2 - the minimum/maximum value specifying a value range.
    - LUT_MAX [`table field`] : Maximum Value. Use this option to specify a value range equal or greater than previous value and less than this (maximum) value.
    - LUT_NAME [`table field`] : Class name. Optional, a class name used for the naming of the target coverage rasters.
    - NO_DATA [`boolean`] : Mark No Coverage as No-Data. Default: 1
    - DATADEPTH [`data type`] : Data Depth. Available Choices: [0] unsigned 1 byte integer [1] unsigned 2 byte integer [2] 4 byte floating point number [3] 8 byte floating point number Default: 1
    - UNIT [`choice`] : Unit. Available Choices: [0] fraction [1] percent Default: 0
    - TARGET_DEFINITION [`choice`] : Target Grid System. Available Choices: [0] user defined [1] grid or grid system Default: 0
    - TARGET_USER_SIZE [`floating point number`] : Cellsize. Minimum: 0.000000 Default: 1.000000
    - TARGET_USER_XMIN [`floating point number`] : West. Default: 0.000000
    - TARGET_USER_XMAX [`floating point number`] : East. Default: 100.000000
    - TARGET_USER_YMIN [`floating point number`] : South. Default: 0.000000
    - TARGET_USER_YMAX [`floating point number`] : North. Default: 100.000000
    - TARGET_USER_COLS [`integer number`] : Columns. Minimum: 1 Default: 101 Number of cells in East-West direction.
    - TARGET_USER_ROWS [`integer number`] : Rows. Minimum: 1 Default: 101 Number of cells in North-South direction.
    - TARGET_USER_FLAT [`boolean`] : Rounding. Default: 1 Round bounding coordinates to multiples of cell size. Ignored if cell size has decimal places.
    - TARGET_USER_FITS [`choice`] : Fit. Available Choices: [0] nodes [1] cells Default: 0
    - TARGET_SYSTEM [`grid system`] : Grid System

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_analysis', '26', 'Coverage of Categories')
    if Tool.is_Okay():
        Tool.Set_Input ('CLASSES', CLASSES)
        Tool.Set_Input ('LUT', LUT)
        Tool.Set_Input ('TARGET_TEMPLATE', TARGET_TEMPLATE)
        Tool.Set_Output('COVERAGES', COVERAGES)
        Tool.Set_Option('LUT_VAL', LUT_VAL)
        Tool.Set_Option('LUT_MAX', LUT_MAX)
        Tool.Set_Option('LUT_NAME', LUT_NAME)
        Tool.Set_Option('NO_DATA', NO_DATA)
        Tool.Set_Option('DATADEPTH', DATADEPTH)
        Tool.Set_Option('UNIT', UNIT)
        Tool.Set_Option('TARGET_DEFINITION', TARGET_DEFINITION)
        Tool.Set_Option('TARGET_USER_SIZE', TARGET_USER_SIZE)
        Tool.Set_Option('TARGET_USER_XMIN', TARGET_USER_XMIN)
        Tool.Set_Option('TARGET_USER_XMAX', TARGET_USER_XMAX)
        Tool.Set_Option('TARGET_USER_YMIN', TARGET_USER_YMIN)
        Tool.Set_Option('TARGET_USER_YMAX', TARGET_USER_YMAX)
        Tool.Set_Option('TARGET_USER_COLS', TARGET_USER_COLS)
        Tool.Set_Option('TARGET_USER_ROWS', TARGET_USER_ROWS)
        Tool.Set_Option('TARGET_USER_FLAT', TARGET_USER_FLAT)
        Tool.Set_Option('TARGET_USER_FITS', TARGET_USER_FITS)
        Tool.Set_Option('TARGET_SYSTEM', TARGET_SYSTEM)
        return Tool.Execute(Verbose)
    return False

def run_tool_grid_analysis_26(CLASSES=None, LUT=None, TARGET_TEMPLATE=None, COVERAGES=None, LUT_VAL=None, LUT_MAX=None, LUT_NAME=None, NO_DATA=None, DATADEPTH=None, UNIT=None, TARGET_DEFINITION=None, TARGET_USER_SIZE=None, TARGET_USER_XMIN=None, TARGET_USER_XMAX=None, TARGET_USER_YMIN=None, TARGET_USER_YMAX=None, TARGET_USER_COLS=None, TARGET_USER_ROWS=None, TARGET_USER_FLAT=None, TARGET_USER_FITS=None, TARGET_SYSTEM=None, Verbose=2):
    '''
    Coverage of Categories
    ----------
    [grid_analysis.26]\n
    The Coverage of Categories tool calculates for each category of the categories input grid the percentage it covers in each cell of the target grid system.\n
    Arguments
    ----------
    - CLASSES [`input grid`] : Categories
    - LUT [`optional input table`] : Classification
    - TARGET_TEMPLATE [`optional input grid`] : Target System. use this grid's system for output grids
    - COVERAGES [`output grid list`] : Coverages
    - LUT_VAL [`table field`] : Value. The class value or - in combination with value 2 - the minimum/maximum value specifying a value range.
    - LUT_MAX [`table field`] : Maximum Value. Use this option to specify a value range equal or greater than previous value and less than this (maximum) value.
    - LUT_NAME [`table field`] : Class name. Optional, a class name used for the naming of the target coverage rasters.
    - NO_DATA [`boolean`] : Mark No Coverage as No-Data. Default: 1
    - DATADEPTH [`data type`] : Data Depth. Available Choices: [0] unsigned 1 byte integer [1] unsigned 2 byte integer [2] 4 byte floating point number [3] 8 byte floating point number Default: 1
    - UNIT [`choice`] : Unit. Available Choices: [0] fraction [1] percent Default: 0
    - TARGET_DEFINITION [`choice`] : Target Grid System. Available Choices: [0] user defined [1] grid or grid system Default: 0
    - TARGET_USER_SIZE [`floating point number`] : Cellsize. Minimum: 0.000000 Default: 1.000000
    - TARGET_USER_XMIN [`floating point number`] : West. Default: 0.000000
    - TARGET_USER_XMAX [`floating point number`] : East. Default: 100.000000
    - TARGET_USER_YMIN [`floating point number`] : South. Default: 0.000000
    - TARGET_USER_YMAX [`floating point number`] : North. Default: 100.000000
    - TARGET_USER_COLS [`integer number`] : Columns. Minimum: 1 Default: 101 Number of cells in East-West direction.
    - TARGET_USER_ROWS [`integer number`] : Rows. Minimum: 1 Default: 101 Number of cells in North-South direction.
    - TARGET_USER_FLAT [`boolean`] : Rounding. Default: 1 Round bounding coordinates to multiples of cell size. Ignored if cell size has decimal places.
    - TARGET_USER_FITS [`choice`] : Fit. Available Choices: [0] nodes [1] cells Default: 0
    - TARGET_SYSTEM [`grid system`] : Grid System

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_analysis', '26', 'Coverage of Categories')
    if Tool.is_Okay():
        Tool.Set_Input ('CLASSES', CLASSES)
        Tool.Set_Input ('LUT', LUT)
        Tool.Set_Input ('TARGET_TEMPLATE', TARGET_TEMPLATE)
        Tool.Set_Output('COVERAGES', COVERAGES)
        Tool.Set_Option('LUT_VAL', LUT_VAL)
        Tool.Set_Option('LUT_MAX', LUT_MAX)
        Tool.Set_Option('LUT_NAME', LUT_NAME)
        Tool.Set_Option('NO_DATA', NO_DATA)
        Tool.Set_Option('DATADEPTH', DATADEPTH)
        Tool.Set_Option('UNIT', UNIT)
        Tool.Set_Option('TARGET_DEFINITION', TARGET_DEFINITION)
        Tool.Set_Option('TARGET_USER_SIZE', TARGET_USER_SIZE)
        Tool.Set_Option('TARGET_USER_XMIN', TARGET_USER_XMIN)
        Tool.Set_Option('TARGET_USER_XMAX', TARGET_USER_XMAX)
        Tool.Set_Option('TARGET_USER_YMIN', TARGET_USER_YMIN)
        Tool.Set_Option('TARGET_USER_YMAX', TARGET_USER_YMAX)
        Tool.Set_Option('TARGET_USER_COLS', TARGET_USER_COLS)
        Tool.Set_Option('TARGET_USER_ROWS', TARGET_USER_ROWS)
        Tool.Set_Option('TARGET_USER_FLAT', TARGET_USER_FLAT)
        Tool.Set_Option('TARGET_USER_FITS', TARGET_USER_FITS)
        Tool.Set_Option('TARGET_SYSTEM', TARGET_SYSTEM)
        return Tool.Execute(Verbose)
    return False

def Soil_Water_Capacity(SAND=None, SILT=None, CLAY=None, CORG=None, BULK=None, CEC=None, PH=None, FC=None, PWP=None, THETA_S=None, SAND_DEFAULT=None, SILT_DEFAULT=None, CLAY_DEFAULT=None, CORG_DEFAULT=None, BULK_DEFAULT=None, CEC_DEFAULT=None, PH_DEFAULT=None, UNIT=None, FUNCTION=None, PSI_FC=None, PSI_PWP=None, ADJUST=None, USERDEF=None, COEFFICIENTS=None, Verbose=2):
    '''
    Soil Water Capacity
    ----------
    [grid_analysis.27]\n
    This tool derives the soil water capacity for the given soil moisture potentials (psi) based on pedo-transfer functions.\n
    Suggested psi values for field capacity estimation range between 60 hPa (pF=1.8) and 316 hPa (pF=2.5). For permanent wilting point estimation take a psi value of about 15850 hPa (pF=4.2). This tool re-implements the R-script AWCPTF by Hengl as well as the regression approach by Toth et al. (2015). See Hengl et al. (2017), Woesten & Verzandvoort (2013) and Toth et al. (2015) for more details.\n
    Arguments
    ----------
    - SAND [`optional input grid`] : Sand. [%]
    - SILT [`optional input grid`] : Silt. [%]
    - CLAY [`optional input grid`] : Clay. [%]
    - CORG [`optional input grid`] : Soil Organic Carbon. [permille]
    - BULK [`optional input grid`] : Bulk Density. [kg/m³]
    - CEC [`optional input grid`] : Cation Exchange Capacity. [cmol/kg]
    - PH [`optional input grid`] : pH
    - FC [`output grid`] : Field Capacity
    - PWP [`output grid`] : Permanent Wilting Point
    - THETA_S [`output grid`] : Water Capacity at Saturation
    - SAND_DEFAULT [`floating point number`] : Default. Minimum: 0.000000 Maximum: 100.000000 Default: 15.000000 default value if no grid has been selected
    - SILT_DEFAULT [`floating point number`] : Default. Minimum: 0.000000 Maximum: 100.000000 Default: 37.000000 default value if no grid has been selected
    - CLAY_DEFAULT [`floating point number`] : Default. Minimum: 0.000000 Maximum: 100.000000 Default: 48.000000 default value if no grid has been selected
    - CORG_DEFAULT [`floating point number`] : Default. Minimum: 0.000000 Maximum: 1000.000000 Default: 15.000000 default value if no grid has been selected
    - BULK_DEFAULT [`floating point number`] : Default. Minimum: 100.000000 Maximum: 2650.000000 Default: 1350.000000 default value if no grid has been selected
    - CEC_DEFAULT [`floating point number`] : Default. Minimum: 0.000000 Default: 45.000000 default value if no grid has been selected
    - PH_DEFAULT [`floating point number`] : Default. Minimum: 0.000000 Maximum: 14.000000 Default: 6.400000 default value if no grid has been selected
    - UNIT [`choice`] : Output Unit. Available Choices: [0] cubic-meter per cubic-meter [1] percentage of volume Default: 1
    - FUNCTION [`choice`] : Pedo-Transfer Function. Available Choices: [0] Hodnett & Tomasella 2002 [1] Toth et al. 2015 Default: 0
    - PSI_FC [`floating point number`] : Soil Moisture Potential. Minimum: 0.000000 Default: 316.000000 [hPa]
    - PSI_PWP [`floating point number`] : Soil Moisture Potential. Minimum: 0.000000 Default: 15850.000000 [hPa]
    - ADJUST [`boolean`] : Adjustments. Default: 1 Specifies whether to correct values of textures and bulk density to avoid creating nonsensical values.
    - USERDEF [`boolean`] : User Defined Coefficients. Default: 0
    - COEFFICIENTS [`static table`] : User Defined Coefficients. 4 Fields: - 1. [8 byte floating point number] ln(alpha) - 2. [8 byte floating point number] ln(n) - 3. [8 byte floating point number] theta s - 4. [8 byte floating point number] theta r 

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_analysis', '27', 'Soil Water Capacity')
    if Tool.is_Okay():
        Tool.Set_Input ('SAND', SAND)
        Tool.Set_Input ('SILT', SILT)
        Tool.Set_Input ('CLAY', CLAY)
        Tool.Set_Input ('CORG', CORG)
        Tool.Set_Input ('BULK', BULK)
        Tool.Set_Input ('CEC', CEC)
        Tool.Set_Input ('PH', PH)
        Tool.Set_Output('FC', FC)
        Tool.Set_Output('PWP', PWP)
        Tool.Set_Output('THETA_S', THETA_S)
        Tool.Set_Option('SAND_DEFAULT', SAND_DEFAULT)
        Tool.Set_Option('SILT_DEFAULT', SILT_DEFAULT)
        Tool.Set_Option('CLAY_DEFAULT', CLAY_DEFAULT)
        Tool.Set_Option('CORG_DEFAULT', CORG_DEFAULT)
        Tool.Set_Option('BULK_DEFAULT', BULK_DEFAULT)
        Tool.Set_Option('CEC_DEFAULT', CEC_DEFAULT)
        Tool.Set_Option('PH_DEFAULT', PH_DEFAULT)
        Tool.Set_Option('UNIT', UNIT)
        Tool.Set_Option('FUNCTION', FUNCTION)
        Tool.Set_Option('PSI_FC', PSI_FC)
        Tool.Set_Option('PSI_PWP', PSI_PWP)
        Tool.Set_Option('ADJUST', ADJUST)
        Tool.Set_Option('USERDEF', USERDEF)
        Tool.Set_Option('COEFFICIENTS', COEFFICIENTS)
        return Tool.Execute(Verbose)
    return False

def run_tool_grid_analysis_27(SAND=None, SILT=None, CLAY=None, CORG=None, BULK=None, CEC=None, PH=None, FC=None, PWP=None, THETA_S=None, SAND_DEFAULT=None, SILT_DEFAULT=None, CLAY_DEFAULT=None, CORG_DEFAULT=None, BULK_DEFAULT=None, CEC_DEFAULT=None, PH_DEFAULT=None, UNIT=None, FUNCTION=None, PSI_FC=None, PSI_PWP=None, ADJUST=None, USERDEF=None, COEFFICIENTS=None, Verbose=2):
    '''
    Soil Water Capacity
    ----------
    [grid_analysis.27]\n
    This tool derives the soil water capacity for the given soil moisture potentials (psi) based on pedo-transfer functions.\n
    Suggested psi values for field capacity estimation range between 60 hPa (pF=1.8) and 316 hPa (pF=2.5). For permanent wilting point estimation take a psi value of about 15850 hPa (pF=4.2). This tool re-implements the R-script AWCPTF by Hengl as well as the regression approach by Toth et al. (2015). See Hengl et al. (2017), Woesten & Verzandvoort (2013) and Toth et al. (2015) for more details.\n
    Arguments
    ----------
    - SAND [`optional input grid`] : Sand. [%]
    - SILT [`optional input grid`] : Silt. [%]
    - CLAY [`optional input grid`] : Clay. [%]
    - CORG [`optional input grid`] : Soil Organic Carbon. [permille]
    - BULK [`optional input grid`] : Bulk Density. [kg/m³]
    - CEC [`optional input grid`] : Cation Exchange Capacity. [cmol/kg]
    - PH [`optional input grid`] : pH
    - FC [`output grid`] : Field Capacity
    - PWP [`output grid`] : Permanent Wilting Point
    - THETA_S [`output grid`] : Water Capacity at Saturation
    - SAND_DEFAULT [`floating point number`] : Default. Minimum: 0.000000 Maximum: 100.000000 Default: 15.000000 default value if no grid has been selected
    - SILT_DEFAULT [`floating point number`] : Default. Minimum: 0.000000 Maximum: 100.000000 Default: 37.000000 default value if no grid has been selected
    - CLAY_DEFAULT [`floating point number`] : Default. Minimum: 0.000000 Maximum: 100.000000 Default: 48.000000 default value if no grid has been selected
    - CORG_DEFAULT [`floating point number`] : Default. Minimum: 0.000000 Maximum: 1000.000000 Default: 15.000000 default value if no grid has been selected
    - BULK_DEFAULT [`floating point number`] : Default. Minimum: 100.000000 Maximum: 2650.000000 Default: 1350.000000 default value if no grid has been selected
    - CEC_DEFAULT [`floating point number`] : Default. Minimum: 0.000000 Default: 45.000000 default value if no grid has been selected
    - PH_DEFAULT [`floating point number`] : Default. Minimum: 0.000000 Maximum: 14.000000 Default: 6.400000 default value if no grid has been selected
    - UNIT [`choice`] : Output Unit. Available Choices: [0] cubic-meter per cubic-meter [1] percentage of volume Default: 1
    - FUNCTION [`choice`] : Pedo-Transfer Function. Available Choices: [0] Hodnett & Tomasella 2002 [1] Toth et al. 2015 Default: 0
    - PSI_FC [`floating point number`] : Soil Moisture Potential. Minimum: 0.000000 Default: 316.000000 [hPa]
    - PSI_PWP [`floating point number`] : Soil Moisture Potential. Minimum: 0.000000 Default: 15850.000000 [hPa]
    - ADJUST [`boolean`] : Adjustments. Default: 1 Specifies whether to correct values of textures and bulk density to avoid creating nonsensical values.
    - USERDEF [`boolean`] : User Defined Coefficients. Default: 0
    - COEFFICIENTS [`static table`] : User Defined Coefficients. 4 Fields: - 1. [8 byte floating point number] ln(alpha) - 2. [8 byte floating point number] ln(n) - 3. [8 byte floating point number] theta s - 4. [8 byte floating point number] theta r 

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_analysis', '27', 'Soil Water Capacity')
    if Tool.is_Okay():
        Tool.Set_Input ('SAND', SAND)
        Tool.Set_Input ('SILT', SILT)
        Tool.Set_Input ('CLAY', CLAY)
        Tool.Set_Input ('CORG', CORG)
        Tool.Set_Input ('BULK', BULK)
        Tool.Set_Input ('CEC', CEC)
        Tool.Set_Input ('PH', PH)
        Tool.Set_Output('FC', FC)
        Tool.Set_Output('PWP', PWP)
        Tool.Set_Output('THETA_S', THETA_S)
        Tool.Set_Option('SAND_DEFAULT', SAND_DEFAULT)
        Tool.Set_Option('SILT_DEFAULT', SILT_DEFAULT)
        Tool.Set_Option('CLAY_DEFAULT', CLAY_DEFAULT)
        Tool.Set_Option('CORG_DEFAULT', CORG_DEFAULT)
        Tool.Set_Option('BULK_DEFAULT', BULK_DEFAULT)
        Tool.Set_Option('CEC_DEFAULT', CEC_DEFAULT)
        Tool.Set_Option('PH_DEFAULT', PH_DEFAULT)
        Tool.Set_Option('UNIT', UNIT)
        Tool.Set_Option('FUNCTION', FUNCTION)
        Tool.Set_Option('PSI_FC', PSI_FC)
        Tool.Set_Option('PSI_PWP', PSI_PWP)
        Tool.Set_Option('ADJUST', ADJUST)
        Tool.Set_Option('USERDEF', USERDEF)
        Tool.Set_Option('COEFFICIENTS', COEFFICIENTS)
        return Tool.Execute(Verbose)
    return False

def Soil_Water_Capacity_Grid_Collections(SAND=None, SILT=None, CLAY=None, CORG=None, BULK=None, CEC=None, PH=None, FC=None, PWP=None, THETA_S=None, UNIT=None, FUNCTION=None, PSI_FC=None, PSI_PWP=None, ADJUST=None, USERDEF=None, COEFFICIENTS=None, Verbose=2):
    '''
    Soil Water Capacity (Grid Collections)
    ----------
    [grid_analysis.28]\n
    This tool derives the soil water capacity for the given soil moisture potentials (psi) based on pedo-transfer functions.\n
    Suggested psi values for field capacity estimation range between 60 hPa (pF=1.8) and 316 hPa (pF=2.5). For permanent wilting point estimation take a psi value of about 15850 hPa (pF=4.2). This tool re-implements the R-script AWCPTF by Hengl as well as the regression approach by Toth et al. (2015). See Hengl et al. (2017), Woesten & Verzandvoort (2013) and Toth et al. (2015) for more details.\n
    Arguments
    ----------
    - SAND [`input grid collection`] : Sand. [%]
    - SILT [`input grid collection`] : Silt. [%]
    - CLAY [`input grid collection`] : Clay. [%]
    - CORG [`input grid collection`] : Soil Organic Carbon. [permille]
    - BULK [`input grid collection`] : Bulk Density. [kg/m³]
    - CEC [`input grid collection`] : Cation Exchange Capacity. [cmol/kg]
    - PH [`input grid collection`] : pH
    - FC [`output grid collection`] : Field Capacity
    - PWP [`output grid collection`] : Permanent Wilting Point
    - THETA_S [`output grid collection`] : Water Capacity at Saturation
    - UNIT [`choice`] : Output Unit. Available Choices: [0] cubic-meter per cubic-meter [1] percentage of volume Default: 1
    - FUNCTION [`choice`] : Pedo-Transfer Function. Available Choices: [0] Hodnett & Tomasella 2002 [1] Toth et al. 2015 Default: 0
    - PSI_FC [`floating point number`] : Soil Moisture Potential. Minimum: 0.000000 Default: 316.000000 [hPa]
    - PSI_PWP [`floating point number`] : Soil Moisture Potential. Minimum: 0.000000 Default: 15850.000000 [hPa]
    - ADJUST [`boolean`] : Adjustments. Default: 1 Specifies whether to correct values of textures and bulk density to avoid creating nonsensical values.
    - USERDEF [`boolean`] : User Defined Coefficients. Default: 0
    - COEFFICIENTS [`static table`] : User Defined Coefficients. 4 Fields: - 1. [8 byte floating point number] ln(alpha) - 2. [8 byte floating point number] ln(n) - 3. [8 byte floating point number] theta s - 4. [8 byte floating point number] theta r 

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_analysis', '28', 'Soil Water Capacity (Grid Collections)')
    if Tool.is_Okay():
        Tool.Set_Input ('SAND', SAND)
        Tool.Set_Input ('SILT', SILT)
        Tool.Set_Input ('CLAY', CLAY)
        Tool.Set_Input ('CORG', CORG)
        Tool.Set_Input ('BULK', BULK)
        Tool.Set_Input ('CEC', CEC)
        Tool.Set_Input ('PH', PH)
        Tool.Set_Output('FC', FC)
        Tool.Set_Output('PWP', PWP)
        Tool.Set_Output('THETA_S', THETA_S)
        Tool.Set_Option('UNIT', UNIT)
        Tool.Set_Option('FUNCTION', FUNCTION)
        Tool.Set_Option('PSI_FC', PSI_FC)
        Tool.Set_Option('PSI_PWP', PSI_PWP)
        Tool.Set_Option('ADJUST', ADJUST)
        Tool.Set_Option('USERDEF', USERDEF)
        Tool.Set_Option('COEFFICIENTS', COEFFICIENTS)
        return Tool.Execute(Verbose)
    return False

def run_tool_grid_analysis_28(SAND=None, SILT=None, CLAY=None, CORG=None, BULK=None, CEC=None, PH=None, FC=None, PWP=None, THETA_S=None, UNIT=None, FUNCTION=None, PSI_FC=None, PSI_PWP=None, ADJUST=None, USERDEF=None, COEFFICIENTS=None, Verbose=2):
    '''
    Soil Water Capacity (Grid Collections)
    ----------
    [grid_analysis.28]\n
    This tool derives the soil water capacity for the given soil moisture potentials (psi) based on pedo-transfer functions.\n
    Suggested psi values for field capacity estimation range between 60 hPa (pF=1.8) and 316 hPa (pF=2.5). For permanent wilting point estimation take a psi value of about 15850 hPa (pF=4.2). This tool re-implements the R-script AWCPTF by Hengl as well as the regression approach by Toth et al. (2015). See Hengl et al. (2017), Woesten & Verzandvoort (2013) and Toth et al. (2015) for more details.\n
    Arguments
    ----------
    - SAND [`input grid collection`] : Sand. [%]
    - SILT [`input grid collection`] : Silt. [%]
    - CLAY [`input grid collection`] : Clay. [%]
    - CORG [`input grid collection`] : Soil Organic Carbon. [permille]
    - BULK [`input grid collection`] : Bulk Density. [kg/m³]
    - CEC [`input grid collection`] : Cation Exchange Capacity. [cmol/kg]
    - PH [`input grid collection`] : pH
    - FC [`output grid collection`] : Field Capacity
    - PWP [`output grid collection`] : Permanent Wilting Point
    - THETA_S [`output grid collection`] : Water Capacity at Saturation
    - UNIT [`choice`] : Output Unit. Available Choices: [0] cubic-meter per cubic-meter [1] percentage of volume Default: 1
    - FUNCTION [`choice`] : Pedo-Transfer Function. Available Choices: [0] Hodnett & Tomasella 2002 [1] Toth et al. 2015 Default: 0
    - PSI_FC [`floating point number`] : Soil Moisture Potential. Minimum: 0.000000 Default: 316.000000 [hPa]
    - PSI_PWP [`floating point number`] : Soil Moisture Potential. Minimum: 0.000000 Default: 15850.000000 [hPa]
    - ADJUST [`boolean`] : Adjustments. Default: 1 Specifies whether to correct values of textures and bulk density to avoid creating nonsensical values.
    - USERDEF [`boolean`] : User Defined Coefficients. Default: 0
    - COEFFICIENTS [`static table`] : User Defined Coefficients. 4 Fields: - 1. [8 byte floating point number] ln(alpha) - 2. [8 byte floating point number] ln(n) - 3. [8 byte floating point number] theta s - 4. [8 byte floating point number] theta r 

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_analysis', '28', 'Soil Water Capacity (Grid Collections)')
    if Tool.is_Okay():
        Tool.Set_Input ('SAND', SAND)
        Tool.Set_Input ('SILT', SILT)
        Tool.Set_Input ('CLAY', CLAY)
        Tool.Set_Input ('CORG', CORG)
        Tool.Set_Input ('BULK', BULK)
        Tool.Set_Input ('CEC', CEC)
        Tool.Set_Input ('PH', PH)
        Tool.Set_Output('FC', FC)
        Tool.Set_Output('PWP', PWP)
        Tool.Set_Output('THETA_S', THETA_S)
        Tool.Set_Option('UNIT', UNIT)
        Tool.Set_Option('FUNCTION', FUNCTION)
        Tool.Set_Option('PSI_FC', PSI_FC)
        Tool.Set_Option('PSI_PWP', PSI_PWP)
        Tool.Set_Option('ADJUST', ADJUST)
        Tool.Set_Option('USERDEF', USERDEF)
        Tool.Set_Option('COEFFICIENTS', COEFFICIENTS)
        return Tool.Execute(Verbose)
    return False

def Iterative_Truncation(INPUT=None, REMOVED=None, OUTPUT=None, TARGET=None, METHOD=None, SUBSTITUTE=None, Verbose=2):
    '''
    Iterative Truncation
    ----------
    [grid_analysis.29]\n
    The tool allows one to perform an iterative truncation to a target average. This operation iteratively removes the highest values from the input grid until the average of all grid values matches the user-specified target average. Instead of simply removing the highest cell values, these values can also be replaced by a substitute value.\n
    An example application is surface soil cleanup, where the highest soil contaminant concentrations are removed until targeted post-remediation concentrations are reached. In this case, the substitute value would be set to the concentration of clean fill.\n
    Arguments
    ----------
    - INPUT [`input grid`] : Input. Grid to analyse.
    - REMOVED [`output grid`] : Removed Cells. Output grid showing the removed cells (1/NoData).
    - OUTPUT [`output grid`] : Output. The modified input grid.
    - TARGET [`floating point number`] : Target Average. Default: 100.000000 The target average.
    - METHOD [`choice`] : Method. Available Choices: [0] remove cell values [1] replace cell values Default: 0 Choose a mode of operation.
    - SUBSTITUTE [`floating point number`] : Substitute Value. Default: 0.000000 The value with which the removed cell values are replaced.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_analysis', '29', 'Iterative Truncation')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Output('REMOVED', REMOVED)
        Tool.Set_Output('OUTPUT', OUTPUT)
        Tool.Set_Option('TARGET', TARGET)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('SUBSTITUTE', SUBSTITUTE)
        return Tool.Execute(Verbose)
    return False

def run_tool_grid_analysis_29(INPUT=None, REMOVED=None, OUTPUT=None, TARGET=None, METHOD=None, SUBSTITUTE=None, Verbose=2):
    '''
    Iterative Truncation
    ----------
    [grid_analysis.29]\n
    The tool allows one to perform an iterative truncation to a target average. This operation iteratively removes the highest values from the input grid until the average of all grid values matches the user-specified target average. Instead of simply removing the highest cell values, these values can also be replaced by a substitute value.\n
    An example application is surface soil cleanup, where the highest soil contaminant concentrations are removed until targeted post-remediation concentrations are reached. In this case, the substitute value would be set to the concentration of clean fill.\n
    Arguments
    ----------
    - INPUT [`input grid`] : Input. Grid to analyse.
    - REMOVED [`output grid`] : Removed Cells. Output grid showing the removed cells (1/NoData).
    - OUTPUT [`output grid`] : Output. The modified input grid.
    - TARGET [`floating point number`] : Target Average. Default: 100.000000 The target average.
    - METHOD [`choice`] : Method. Available Choices: [0] remove cell values [1] replace cell values Default: 0 Choose a mode of operation.
    - SUBSTITUTE [`floating point number`] : Substitute Value. Default: 0.000000 The value with which the removed cell values are replaced.

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_analysis', '29', 'Iterative Truncation')
    if Tool.is_Okay():
        Tool.Set_Input ('INPUT', INPUT)
        Tool.Set_Output('REMOVED', REMOVED)
        Tool.Set_Output('OUTPUT', OUTPUT)
        Tool.Set_Option('TARGET', TARGET)
        Tool.Set_Option('METHOD', METHOD)
        Tool.Set_Option('SUBSTITUTE', SUBSTITUTE)
        return Tool.Execute(Verbose)
    return False

def Object_Enumeration(GRID=None, OBJECTS=None, SUMMARY=None, EXTENTS=None, NEIGHBOURHOOD=None, BOUNDARY_CELLS=None, BOUNDARY_VALUE=None, Verbose=2):
    '''
    Object Enumeration
    ----------
    [grid_analysis.30]\n
    The 'Object Enumeration' tool to identifies objects as connected cells taking all no-data cells (or those cells having a specified value) as potential boundary cells surrounding each object.\n
    Arguments
    ----------
    - GRID [`input grid`] : Grid
    - OBJECTS [`output grid`] : Objects
    - SUMMARY [`output table`] : Summary
    - EXTENTS [`output shapes`] : Extents
    - NEIGHBOURHOOD [`choice`] : Neighbourhood. Available Choices: [0] Neumann [1] Moore Default: 0
    - BOUNDARY_CELLS [`choice`] : Boundary Cells. Available Choices: [0] no data [1] value Default: 0
    - BOUNDARY_VALUE [`floating point number`] : Value. Default: 0.000000

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_analysis', '30', 'Object Enumeration')
    if Tool.is_Okay():
        Tool.Set_Input ('GRID', GRID)
        Tool.Set_Output('OBJECTS', OBJECTS)
        Tool.Set_Output('SUMMARY', SUMMARY)
        Tool.Set_Output('EXTENTS', EXTENTS)
        Tool.Set_Option('NEIGHBOURHOOD', NEIGHBOURHOOD)
        Tool.Set_Option('BOUNDARY_CELLS', BOUNDARY_CELLS)
        Tool.Set_Option('BOUNDARY_VALUE', BOUNDARY_VALUE)
        return Tool.Execute(Verbose)
    return False

def run_tool_grid_analysis_30(GRID=None, OBJECTS=None, SUMMARY=None, EXTENTS=None, NEIGHBOURHOOD=None, BOUNDARY_CELLS=None, BOUNDARY_VALUE=None, Verbose=2):
    '''
    Object Enumeration
    ----------
    [grid_analysis.30]\n
    The 'Object Enumeration' tool to identifies objects as connected cells taking all no-data cells (or those cells having a specified value) as potential boundary cells surrounding each object.\n
    Arguments
    ----------
    - GRID [`input grid`] : Grid
    - OBJECTS [`output grid`] : Objects
    - SUMMARY [`output table`] : Summary
    - EXTENTS [`output shapes`] : Extents
    - NEIGHBOURHOOD [`choice`] : Neighbourhood. Available Choices: [0] Neumann [1] Moore Default: 0
    - BOUNDARY_CELLS [`choice`] : Boundary Cells. Available Choices: [0] no data [1] value Default: 0
    - BOUNDARY_VALUE [`floating point number`] : Value. Default: 0.000000

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('grid_analysis', '30', 'Object Enumeration')
    if Tool.is_Okay():
        Tool.Set_Input ('GRID', GRID)
        Tool.Set_Output('OBJECTS', OBJECTS)
        Tool.Set_Output('SUMMARY', SUMMARY)
        Tool.Set_Output('EXTENTS', EXTENTS)
        Tool.Set_Option('NEIGHBOURHOOD', NEIGHBOURHOOD)
        Tool.Set_Option('BOUNDARY_CELLS', BOUNDARY_CELLS)
        Tool.Set_Option('BOUNDARY_VALUE', BOUNDARY_VALUE)
        return Tool.Execute(Verbose)
    return False

