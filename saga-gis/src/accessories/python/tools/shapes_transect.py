#! /usr/bin/env python

'''
Python Interface to SAGA Tools Library
----------
- Category : Shapes
- Name     : Transects
- ID       : shapes_transect

Description
----------
A SAGA tool to create transects through polygon files.
'''

from PySAGA.helper import Tool_Wrapper

def Transect_through_polygon_shapefile(TRANSECT=None, THEME=None, TRANSECT_RESULT=None, THEME_FIELD=None, Verbose=2):
    '''
    Transect through polygon shapefile
    ----------
    [shapes_transect.0]\n
    Transect for lines and polygon shapefiles\n
    The goal of this tool is to create a transect along a line through a polygon map.\n
    Eg\n
    |____ST1_____!_ST2_!__ST1__!_______ST#_____|\n
    (Soil type 1 etc...)\n
    This is done by creating a table with the ID of each line, the distance\n
    to the starting point and the different transects:\n
    |  line_id  |  start  |  end  |  code/field  |\n
    |    0      |    0    |  124  |     ST1      |\n
    |    0      |   124   |  300  |     ST2      |\n
    |    0      |   300   | 1223  |     ST1      |\n
    |    0      |  1223   | 2504  |     ST3      |\n
    |    1      |    0    |  200  |     ST4      |\n
    |   ...     |   ...   |  ...  |     ...      |</pre>\n
    The tool requires an input shape with all the line transects [Transect_Line]\n
    and a polygon theme [Theme]. You also have to select which field you want to have in\n
    the resulting table [Transect_Result]. This can be an ID of the polygon theme if you\n
    want to link the tables later on, or any other field [Theme_Field].\n
    Arguments
    ----------
    - TRANSECT [`input shapes`] : Line Transect(s)
    - THEME [`input shapes`] : Theme
    - TRANSECT_RESULT [`output table`] : Result table
    - THEME_FIELD [`table field`] : Theme Field

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_transect', '0', 'Transect through polygon shapefile')
    if Tool.is_Okay():
        Tool.Set_Input ('TRANSECT', TRANSECT)
        Tool.Set_Input ('THEME', THEME)
        Tool.Set_Output('TRANSECT_RESULT', TRANSECT_RESULT)
        Tool.Set_Option('THEME_FIELD', THEME_FIELD)
        return Tool.Execute(Verbose)
    return False

def run_tool_shapes_transect_0(TRANSECT=None, THEME=None, TRANSECT_RESULT=None, THEME_FIELD=None, Verbose=2):
    '''
    Transect through polygon shapefile
    ----------
    [shapes_transect.0]\n
    Transect for lines and polygon shapefiles\n
    The goal of this tool is to create a transect along a line through a polygon map.\n
    Eg\n
    |____ST1_____!_ST2_!__ST1__!_______ST#_____|\n
    (Soil type 1 etc...)\n
    This is done by creating a table with the ID of each line, the distance\n
    to the starting point and the different transects:\n
    |  line_id  |  start  |  end  |  code/field  |\n
    |    0      |    0    |  124  |     ST1      |\n
    |    0      |   124   |  300  |     ST2      |\n
    |    0      |   300   | 1223  |     ST1      |\n
    |    0      |  1223   | 2504  |     ST3      |\n
    |    1      |    0    |  200  |     ST4      |\n
    |   ...     |   ...   |  ...  |     ...      |</pre>\n
    The tool requires an input shape with all the line transects [Transect_Line]\n
    and a polygon theme [Theme]. You also have to select which field you want to have in\n
    the resulting table [Transect_Result]. This can be an ID of the polygon theme if you\n
    want to link the tables later on, or any other field [Theme_Field].\n
    Arguments
    ----------
    - TRANSECT [`input shapes`] : Line Transect(s)
    - THEME [`input shapes`] : Theme
    - TRANSECT_RESULT [`output table`] : Result table
    - THEME_FIELD [`table field`] : Theme Field

    - Verbose [`integer number`] : Verbosity level, 0=silent, 1=tool name and success notification, 2=complete tool output.\n
    Returns
    ----------
    `boolean` : `True` on success, `False` on failure.
    '''
    Tool = Tool_Wrapper('shapes_transect', '0', 'Transect through polygon shapefile')
    if Tool.is_Okay():
        Tool.Set_Input ('TRANSECT', TRANSECT)
        Tool.Set_Input ('THEME', THEME)
        Tool.Set_Output('TRANSECT_RESULT', TRANSECT_RESULT)
        Tool.Set_Option('THEME_FIELD', THEME_FIELD)
        return Tool.Execute(Verbose)
    return False

